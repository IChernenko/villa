<?php

ini_set('default_charset', 'UTF-8');

$ctype = false;
if (isset($_REQUEST['ctype'])) {
    $ctype = $_REQUEST['ctype'];
}

header("HTTP/1.0 200 OK");
header('Content-type: application/json; charset=utf8');
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

define('ROOT_PATH', '../');

include_once(ROOT_PATH.'dante/bootstrap.php');

include_once(ROOT_PATH.'dante/lib/json.php');



//$_POST = Dante_Lib_Strings::processString($_POST);

try {
	$content = new Dante_Controller_Content();
    $result = $content->run();
    if ($ctype == 'html') echo $result;
    else echo json_encode($result);
} catch (Exception $e) {
	$result = array(
		'result' => 0,
		'message' => $e->getMessage()
	);
	echo json_encode($result);
}
?>