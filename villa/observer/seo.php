<?php
/**
 * Created by PhpStorm.
 * User: dorian
 * Date: 5/16/16
 * Time: 12:24 PM
 */

//namespace observer;


/**
 * Обработка сео
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Villa_Observer_Seo implements \Dante_Lib_Observer_Iobserver
{
    protected function _loadCategory($categoryId, $object) {
        $storedObject = Dante_Helper_App::getCache()->get('seo.c'.$categoryId);
        if ($storedObject) {
            $object->title = $storedObject->title;
            $object->keywords = $storedObject->keywords;
            $object->description = $storedObject->description;
            return;
        }

        $mapper = new Lascala_Module_Shop_Mapper_Category();
        $category = $mapper->get($categoryId);
        if (!$category) return false;
        $object->title = $category->title;
        $object->keywords = $category->keywords;
        $object->description = $category->description;

        Dante_Helper_App::getCache()->set('seo.c'.$categoryId, $object);
    }

    protected function _loadProduct($productCode, $object) {
        $storedObject = Dante_Helper_App::getCache()->get('seo.p'.$productCode);
        if ($storedObject) {
            $object->title = $storedObject->title;
            $object->keywords = $storedObject->keywords;
            $object->description = $storedObject->description;
            return;
        }

        $mapper = new Lascala_Module_Shop_Mapper_Catalog();
        $product = $mapper->getByLink($productCode);
        if (!$product) return false;

        if(!$product->seoTitle && !$product->seoKeywords && !$product->seoDescription) return false;

        $object->title = $product->seoTitle;
        $object->keywords = $product->seoKeywords;
        $object->description = $product->seoDescription;

        Dante_Helper_App::getCache()->set('seo.p'.$productCode, $object);
    }

    protected function _getContacts($object) {
        $langId = Module_Lang_Helper::getCurrent();

        $sql = "select seo_title, seo_keywords, seo_description from [%%]villa_contacts where lang_id=$langId";

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        if (!$r || Dante_Lib_SQL_DB::get_instance()->getNumRows($r) == 0) {
            return false;
        }


        $f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r);
        $object->title = $f['seo_title'];
        $object->keywords = $f['seo_keywords'];
        $object->description = $f['seo_description'];
    }

    protected function _getAboutPage($object) {
        $langId = Module_Lang_Helper::getCurrent();

        $sql = "select seo_title, seo_keywords, seo_description from [%%]villa_about_page where lang_id=$langId";
        //echo $sql; die();
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        if (!$r || Dante_Lib_SQL_DB::get_instance()->getNumRows($r) == 0) {
            return false;
        }


        $f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r);
        $object->title = $f['seo_title'];
        $object->keywords = $f['seo_keywords'];
        $object->description = $f['seo_description'];
        //print($object); die();
    }

    protected function _getHomePage($object) {
        $langId = Module_Lang_Helper::getCurrent();

        $sql = "select seo_title, seo_keywords, seo_description from [%%]villa_homepage where lang_id=$langId";
        //echo $sql;
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        if (!$r || Dante_Lib_SQL_DB::get_instance()->getNumRows($r) == 0) {
            return false;
        }


        $f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r);
        $object->title = $f['seo_title'];
        $object->keywords = $f['seo_keywords'];
        $object->description = $f['seo_description'];

    }

    protected function _loadSeoData($object) {
        $url = Dante_Controller_Front::getRequest()->get_requested_url();

        if ($url == '/contacts/') {
            return $this->_getContacts($object);
        }

        if ($url == '/about-page/') {
            return $this->_getAboutPage($object);
        }

        if ($url == '/') {
            return $this->_getHomePage($object);
        }


        return;

        $categoryId = Lascala_Module_Shop_Helper_Url::getCategoryIdFromUrl($url);
        if ($categoryId > 0) $this->_loadCategory($categoryId, $object);

        $productCode = Lascala_Module_Shop_Helper_Url::getProductCodeFromUrl($url);
        if ($productCode != '') $this->_loadProduct($productCode, $object);
    }

    public function notify($object, $eventType ) {
        $seoEvents = array(
            'seo.draw.title',
            'seo.draw.keywords',
            'seo.draw.description'
        );

        if (in_array($eventType, $seoEvents)) {
            $this->_loadSeoData($object);
        }
    }
}