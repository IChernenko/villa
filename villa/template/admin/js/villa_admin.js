villa_admin = {
    interval: false,
    
    init: function()
    {
        villa_admin.callReminder(); 
        villa_admin.setCheckInterval();
        
        $(document).ajaxSuccess(function(e, xhr, settings){
            var commentCtr = 'villa.module.comment.manage.controller.comment';
            var questionCtr = 'villa.module.comment.manage.controller.questions';
            var bookingCtr = booking.controller;
            if(typeof(settings.data) == 'string')
            {    
                var start = settings.data.indexOf('controller');
                if(settings.data.indexOf(bookingCtr, start) > -1 || settings.data.indexOf(commentCtr, start) > -1 || settings.data.indexOf(questionCtr, start) > -1)
                {
                    villa_admin.clearCheckInterval();
                    villa_admin.callReminder();
                    villa_admin.setCheckInterval();
                }
            }
        });
    },
    setCheckInterval: function()
    {
        villa_admin.interval = setInterval(function(){
            villa_admin.callReminder()
        }, 30000);
    },
    clearCheckInterval: function()
    {
        clearInterval(villa_admin.interval);
    },
    callReminder: function()
    {
        $.ajax({
            type: "POST",
            url: '/ajax.php',
            data: {
                controller: "villa.controller.manage.reminder"
            },
            success: function(data) 
            {
                villa_admin.removeHighlight();
                
                if(data.result == 1)
                {                    
                    if(data.questions > 0)
                        villa_admin.highlightItem('/manage/questions/');
                    
                    if(data.bookings > 0)
                        villa_admin.highlightItem('/manage/booking/');
                    
                    if(data.comments > 0)
                        villa_admin.highlightItem('/manage/comment/');
                    
                    if(data.transfers > 0)
                        villa_admin.highlightItem('/manage/booking-transfer/');
                }
            }
        });
    },
    highlightItem: function(itemLink)
    {
        var obj = $('a[href="'+itemLink+'"]');
        if($.support.opacity)
        {
            obj.addClass('attention');
        }
        else
        {
            obj.addClass('attention-ie');
        }
    },
    removeHighlight: function()
    {
        $('a.attention').removeClass('attention');
        $('a.attention-ie').removeClass('attention-ie');
    }
}