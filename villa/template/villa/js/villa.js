villa = {
    labels: {}

    //todo: перенести методы в объект
};
/**
 * Отображает компонент выбора даты.
 */
function showDatepicker(elemId) {
    $(elemId).datepicker("show");
}




function hotelShowDetailAppartment(id, container) {
    dante.ajaxContentLoadStart(container);
    $.ajax({
        type: "POST",
        url: '/ajax.php',
        data: {
            controller: "villa.module.hotel.controller.appartment",
            action: 'get',
            id: id
        },
        success: function(data) {
            $('#'+container).html(data.template);


            $('.lightbox a').lightBox();
            // поддержка скроллинга
            // $(".scrollable").smoothDivScroll({
            //     mousewheelScrolling: true,
            //     manualContinuousScrolling: true,
            //     visibleHotSpotBackgrounds: "always",
            //     hotSpotScrollingInterval: 40
            // });

        }
    });
}

function collageHide()
{
    $('#collage').animate({height: 'hide'}, 500);
}

function collageShow()
{
    $('#collage').animate({height: 'show'}, 500);
}

function changeDataToggle()
{
    $('#btnChangeData').toggle(
        function(){collageShow();},
        function(){collageHide();}
    );
}

function villaInitComments() {

    $('#btnFeedback').click(function() {
            $.arcticmodal({
                    type: 'ajax',
                    url: '/ajax.php',
                    ajax: {
                            type: 'POST',
                            cache: false,
                            dataType: 'json',
                            data: {
                                controller: "module.comment.controller.comment",
                                action: 'drawForm'
                            },
                            success: function(data, el, responce) {

                                var h = $('<div class="b-modal"><div class="b-modal_close arcticmodal-close">X</div><div id="feedbackContent">'+responce.template+'</div></div>');
                                //$('B', h).html(responce.title);
                                //$('P:last', h).html(responce.text);
                                data.body.html(h);

                                $('#commentBtn').click(function(){
                                    comment.save({
                                        success: function(data) {
                                            var message = "Неудалось сохранить отзыв";
                                            if (data.result = 1) message = "Спасибо за ваш отзыв!";

                                            $('#feedbackContent').html(message);
                                        }
                                    });
                                });
                            }
                    }
            });
    });

}

/**
 * Инициализация обработчиков
 */
function villaInit()
{
    var msecDay = 1000 * 3600 * 24; //один день в миллисекундах
    var now = new Date();
    var maxArrDate = new Date(now.getTime() + msecDay * 30 * 3);



    if($('body').width() > 1100) {
        jQuery("#arriveDate").datepicker({
            minDate: 0,
            maxDate: maxArrDate,
            numberOfMonths: [1, 3],
            dateFormat: 'dd.mm.yy',
            onSelect: function(dateText, inst)
            {
                jQuery('#departureDate').removeClass('hasDatepicker');
                // Дата выезда
                var arriveDate = dateText.match(/\d+/g);
                var departureDate = jQuery('#departureDate').val();
                var minDepDate = new Date(arriveDate[2], arriveDate[1]-1, arriveDate[0]);
                var departureYear = minDepDate.getFullYear();
                var departureMonth = minDepDate.getMonth();
                var departureDay = minDepDate.getDate()+1;
                minDepDate = new Date(departureYear, departureMonth, departureDay);
                var maxDepDate = new Date(minDepDate.getTime() + msecDay * 30 * 6);
                jQuery("#departureDate").datepicker({
                    minDate: minDepDate,
                    maxDate: maxDepDate,
                    numberOfMonths: [1, 3],
                    onClose: function()
                    {
                        var arriveDate = jQuery('#arriveDate').val();
                        var departureDate = jQuery('#departureDate').val();
                        if(arriveDate && departureDate)
                        {
                            jQuery('#btnRoomSearch').addClass('green');
                        }
                        else
                        {
                            jQuery('#btnRoomSearch').removeClass('green');
                        }
                    }

                });

                jQuery('#departureDate').datepicker( "option", "dateFormat", "dd.mm.yy" );

                var arrDate = new Date(arriveDate[2], arriveDate[1]-1, arriveDate[0]);
                departureDate = departureDate.match(/\d+/g);
                var depDate = new Date(departureDate[2], departureDate[1]-1, departureDate[0]);
                if(depDate.getTime() <= arrDate.getTime())
                {
                    jQuery('#departureDate').datepicker('setDate', minDepDate);
                }
                else
                {
                    jQuery('#departureDate').datepicker('setDate', depDate);
                }

            },
            onClose: function()
            {
                var arriveDate = jQuery('#arriveDate').val();
                var departureDate = jQuery('#departureDate').val();
                if(arriveDate && departureDate)
                {
                    jQuery('#btnRoomSearch').addClass('green');
                }
                else
                {
                    jQuery('#btnRoomSearch').removeClass('green');
                }
            }
        });
    }

    if($('body').width() < 600) {
        jQuery("#arriveDate").datepicker({
            minDate: 0,
            maxDate: maxArrDate,
            numberOfMonths: [1, 1],
            dateFormat: 'dd.mm.yy',
            onSelect: function(dateText, inst)
            {
                jQuery('#departureDate').removeClass('hasDatepicker');
                // Дата выезда
                var arriveDate = dateText.match(/\d+/g);
                var departureDate = jQuery('#departureDate').val();
                var minDepDate = new Date(arriveDate[2], arriveDate[1]-1, arriveDate[0]);
                var departureYear = minDepDate.getFullYear();
                var departureMonth = minDepDate.getMonth();
                var departureDay = minDepDate.getDate()+1;
                minDepDate = new Date(departureYear, departureMonth, departureDay);
                var maxDepDate = new Date(minDepDate.getTime() + msecDay * 30 * 6);
                jQuery("#departureDate").datepicker({
                    minDate: minDepDate,
                    maxDate: maxDepDate,
                    numberOfMonths: [1, 3],
                    onClose: function()
                    {
                        var arriveDate = jQuery('#arriveDate').val();
                        var departureDate = jQuery('#departureDate').val();
                        if(arriveDate && departureDate)
                        {
                            jQuery('#btnRoomSearch').addClass('green');
                        }
                        else
                        {
                            jQuery('#btnRoomSearch').removeClass('green');
                        }
                    }

                });

                jQuery('#departureDate').datepicker( "option", "dateFormat", "dd.mm.yy" );

                var arrDate = new Date(arriveDate[2], arriveDate[1]-1, arriveDate[0]);
                departureDate = departureDate.match(/\d+/g);
                var depDate = new Date(departureDate[2], departureDate[1]-1, departureDate[0]);
                if(depDate.getTime() <= arrDate.getTime())
                {
                    jQuery('#departureDate').datepicker('setDate', minDepDate);
                }
                else
                {
                    jQuery('#departureDate').datepicker('setDate', depDate);
                }

            },
            onClose: function()
            {
                var arriveDate = jQuery('#arriveDate').val();
                var departureDate = jQuery('#departureDate').val();
                if(arriveDate && departureDate)
                {
                    jQuery('#btnRoomSearch').addClass('green');
                }
                else
                {
                    jQuery('#btnRoomSearch').removeClass('green');
                }
            }
        });
    }

    if($('body').width() < 1100) {
        jQuery("#arriveDate").datepicker({
            minDate: 0,
            maxDate: maxArrDate,
            numberOfMonths: [1, 2],
            dateFormat: 'dd.mm.yy',
            onSelect: function(dateText, inst)
            {
                jQuery('#departureDate').removeClass('hasDatepicker');
                // Дата выезда
                var arriveDate = dateText.match(/\d+/g);
                var departureDate = jQuery('#departureDate').val();
                var minDepDate = new Date(arriveDate[2], arriveDate[1]-1, arriveDate[0]);
                var departureYear = minDepDate.getFullYear();
                var departureMonth = minDepDate.getMonth();
                var departureDay = minDepDate.getDate()+1;
                minDepDate = new Date(departureYear, departureMonth, departureDay);
                var maxDepDate = new Date(minDepDate.getTime() + msecDay * 30 * 6);
                jQuery("#departureDate").datepicker({
                    minDate: minDepDate,
                    maxDate: maxDepDate,
                    numberOfMonths: [1, 3],
                    onClose: function()
                    {
                        var arriveDate = jQuery('#arriveDate').val();
                        var departureDate = jQuery('#departureDate').val();
                        if(arriveDate && departureDate)
                        {
                            jQuery('#btnRoomSearch').addClass('green');
                        }
                        else
                        {
                            jQuery('#btnRoomSearch').removeClass('green');
                        }
                    }

                });

                jQuery('#departureDate').datepicker( "option", "dateFormat", "dd.mm.yy" );

                var arrDate = new Date(arriveDate[2], arriveDate[1]-1, arriveDate[0]);
                departureDate = departureDate.match(/\d+/g);
                var depDate = new Date(departureDate[2], departureDate[1]-1, departureDate[0]);
                if(depDate.getTime() <= arrDate.getTime())
                {
                    jQuery('#departureDate').datepicker('setDate', minDepDate);
                }
                else
                {
                    jQuery('#departureDate').datepicker('setDate', depDate);
                }

            },
            onClose: function()
            {
                var arriveDate = jQuery('#arriveDate').val();
                var departureDate = jQuery('#departureDate').val();
                if(arriveDate && departureDate)
                {
                    jQuery('#btnRoomSearch').addClass('green');
                }
                else
                {
                    jQuery('#btnRoomSearch').removeClass('green');
                }
            }
        });
    }


    if($('body').width() > 1100){
        jQuery("#departureDate").datepicker({
            minDate: 1,
            maxDate: '+6m',
            numberOfMonths: [1, 3],
            dateFormat: 'dd.mm.yy',
            onClose: function()
            {
                var arriveDate = jQuery('#arriveDate').val();
                var departureDate = jQuery('#departureDate').val();
                if(arriveDate && departureDate)
                {
                    jQuery('#btnRoomSearch').addClass('green');
                }
                else
                {
                    jQuery('#btnRoomSearch').removeClass('green');
                }
            }
        });
    }


    if($('body').width() < 600) {
        jQuery("#departureDate").datepicker({
            minDate: 1,
            maxDate: '+6m',
            numberOfMonths: [1, 1],
            dateFormat: 'dd.mm.yy',
            onClose: function () {
                var arriveDate = jQuery('#arriveDate').val();
                var departureDate = jQuery('#departureDate').val();
                if (arriveDate && departureDate) {
                    jQuery('#btnRoomSearch').addClass('green');
                }
                else {
                    jQuery('#btnRoomSearch').removeClass('green');
                }
            }
        });
    }

    if($('body').width() < 1100){
        jQuery("#departureDate").datepicker({
            minDate: 1,
            maxDate: '+6m',
            numberOfMonths: [1, 2],
            dateFormat: 'dd.mm.yy',
            onClose: function()
            {
                var arriveDate = jQuery('#arriveDate').val();
                var departureDate = jQuery('#departureDate').val();
                if(arriveDate && departureDate)
                {
                    jQuery('#btnRoomSearch').addClass('green');
                }
                else
                {
                    jQuery('#btnRoomSearch').removeClass('green');
                }
            }
        });
    }

    /**
     * Функция, отображающая (скрывающая) полное описание типа номера на форме выбора брони,
     * выполняется при наведении курсора на блок с кратким описанием типа номера.
     */
    jQuery('.reserv_description').css('cursor', 'pointer');
    jQuery('.reserv_description').hover(
        function()
        {
            jQuery(this).css('opacity', '0.0');
            jQuery(this).find('.bg').hide();
            jQuery(this).css(
                {
                    'border': '1px solid grey',
                    'border-radius': '3px',
                    'background-color': '#fef7ea',
                    'z-index': '500',
                    'width': '50%',
                    'height': "100%",
                    'display': 'inline-table',
                    '-webkit-box-shadow': '0px 0px 14px 0px rgba(50, 50, 50, 0.75)',
                    '-moz-box-shadow': '0px 0px 14px 0px rgba(50, 50, 50, 0.75)',
                    'box-shadow': '0px 0px 14px 0px rgba(50, 50, 50, 0.75)'
                }
            );
            jQuery(this).animate({'opacity': '1.0'}, 500);
        },

        function()
        {
            jQuery(this).css('opacity', '0.0');
            jQuery(this).css(
                {
                    'border': '0px',
                    'border-radius': '0px',
                    'background-color': '#fef7ea',
                    'z-index': '100',
                    'width': '277px',
                    'height': "103px",
                    'display': 'block',
                    '-webkit-box-shadow': 'none',
                    '-moz-box-shadow': 'none',
                    'box-shadow': 'none'
                }
            );
            jQuery(this).find('.bg').show();
            jQuery(this).animate({'opacity': '1.0'}, 500);
        }
    );
    /**************************************************************************************/


    $('#langSwitcher').click(function(){
        $('#langList').toggle();
    });

    $("#langSwitcher").change(function(){
        $.ajax({
            type: "POST",
            url: '/ajax.php',
            data: {
                controller: "module.lang.controller",
                action: 'change',
                langCode: $('#langSwitcher').val()
            },
            success: function(data) {
                if (data.result == 1) {
                    //$("#content").html(data.template);
                }
            }
        });
    });

    //обработчик "холостого" клика, языковой блок
    var langEnter = false;
    $('#langSwitcher').mouseenter(function() {
        langEnter = true;
    });
    $('#langSwitcher').mouseleave(function() {
        langEnter = false;
    });
    $(document).click(function() {
        if (!langEnter) {
            $('#langList').hide();
        }
    });
    //

    $('#currencySwitcher').click(function(){
        $('#currencyList').toggle();
    });

    //обработчик "холостого" клика, валютный блок
    var currencyEnter = false;
    $('#currencySwitcher').mouseenter(function() {
        currencyEnter = true;
    });
    $('#currencySwitcher').mouseleave(function() {
        currencyEnter = false;
    });
    $(document).click(function() {
        if (!currencyEnter) {
            $('#currencyList').hide();
        }
    });

    $('#b-profilemenu p').click(function(){
        $('#profile_menu_wrap').toggle();
    });

    //обработчик "холостого" клика, меню профиля
    var profileEnter = false;
    $('#b-profilemenu').mouseenter(function() {
        profileEnter = true;
    });
    $('#b-profilemenu').mouseleave(function() {
        profileEnter = false;
    });
    $(document).click(function() {
        if (!profileEnter) {
            $('#profile_menu_wrap').hide();
        }
    });

    $('.lightbox a').lightBox();


    // поддержка скроллинга
    // $(".scrollable").smoothDivScroll({
    //     mousewheelScrolling: true,
    //     manualContinuousScrolling: true,
    //     visibleHotSpotBackgrounds: "always"
    //     /*autoScrollingMode: "onstart"*/
    // });

    $("#postCallback").click(function() {postCallback();});

    // позволяем делать поиск
    $("#searchTrigger").click(doSearch);

    // включаем слайдшоу
    //$('#slideshow').jqFancyTransitions({width: 572, height: 367});

    //обработчик кнопки "задать вопрос"
    $('#btnAddQuestion').click(function(){location.assign('/questions/');});

    //обработчик кнопки "оставить отзыв"
    $('#btnFeedback').click(function(){location.assign('/comments/');});

    // повесим обработчик для комментов
//    villaInitComments();

    bookingFormInit();
    $('#b_collage').orbit({
        animation: 'horizontal-slide',
        animationSpeed: 800,
        timer: true
    });
    $('#b_collage').css({'width':'574px'});
//    slideshow.insert();
}

function viewHomepageImages(event)
{
    var imgObjects = $(event.target).closest('ul').find('img');
    var location = $(event.target).closest('td');
    var activeIndex = $(imgObjects).index(event.target);

    var params = [];

    params['images'] = [];
    $(imgObjects).each(function(){
        params['images'].push($(this).attr('src'));
    });
    params['thumbs'] = 0;
    params['title'] = $(location).parent('tr').prev('tr').find('h3').html();
    params['descript'] = $(location).parent('tr').next('tr').find('.full-descript').html();

    showViewer(params, location, activeIndex);
}

function viewImages(event)
{
    var imgObjects = $(event.target).parents('ul').find('img');
    var location = $(event.target).parents('#homePageItems');
    var activeIndex = $(imgObjects).index(event.target);

    var params = [];
    params['images'] = [];

    $(imgObjects).each(function(){
        params['images'].push($(this).attr('src'));
    });

    showViewer(params, location, activeIndex);

}

/**
 * Инициализация обработки наведения курсора на изображения в галерее
 */
function galleryImgHoverInit()
{
    jQuery('.iconImg').mouseover(function()
    {
        if(jQuery('.zoomImg').length > 0){zoomOut();}
        var imgSrc = jQuery(this).attr('src');
        var winWidth = jQuery(window).outerWidth();
        var winHeight = jQuery(window).outerHeight();

        jQuery('body').append('<div class="zoomImg" onmouseout="zoomOut();"><img src="'+imgSrc+'"/>' +
            '<div class="closeZoom">&#935; Закрыть</div></div>');
        var zoomWidth = jQuery('.zoomImg').outerWidth();
        var zoomHeight = jQuery('.zoomImg').outerHeight();

        jQuery('.zoomImg').css({
            left: ((winWidth - zoomWidth) / 2 + 'px'),
            top: ((winHeight - zoomHeight) / 2 + 'px'),
            position: 'fixed',
            height: (zoomHeight - 10 + 'px')
        });
        jQuery('.zoomImg').fadeIn('slow');
    });


}

function zoomOut()
{
    jQuery('.zoomImg').fadeOut();
    jQuery('.zoomImg').empty();
    jQuery('.zoomImg').remove();
}
