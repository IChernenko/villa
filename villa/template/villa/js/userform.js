userform = {
    /**
     * Шаблон
     * @private
     */
    template: '<div id="userForm" class="b-modal">  \
               <div class="b-modal_close arcticmodal-close"></div>\
               <h3>{{header}}</h3>  \
               <ul> \
               {{#inputs}}  \
                    <li><input type="text" id="{{id}}" placeholder="{{placeholder}}" value="{{value}}"/></li>    \
               {{/inputs}}  \
               {{#passlabels}}  \
                    <li><input type="password" id="{{id}}" placeholder="{{placeholder}}"/></li> \
               {{/passlabels}}  \
               </ul>    \
               <div class="error"></div>    \
               <button id="userFormContinue">Продолжить</button>  \
               </div>',
    
    /**
     * Данные по умолчанию
     * @private
     */    
    data: {
        header: 'fillYourData',
        inputs: [
            {id: 'userFormName', placeholder: 'name'},
            {id: 'userFormSurname', placeholder: 'lastName'},
            {id: 'userFormEmail', placeholder: 'E-mail'},
            {id: 'userFormPhone', placeholder: 'phone'}
        ],
        passlabels: [
            {id: 'userFormPass', placeholder: 'password7chars'}, 
            {id: 'userFormPassRepeat', placeholder: 'repeatPassword'}
        ]
    },
    
    /**
     * Заполнение формы данными, переданными сервером
     * @param {array}
     * @return {json}
     */
    fillForm: function(params)
    {
        var newData = {
            header: 'fillYourData',
            inputs: [
                {id: 'userFormName', placeholder: 'name', value: params['name']},
                {id: 'userFormSurname', placeholder: 'lastName', value: params['surname']},
                {id: 'userFormEmail', placeholder: 'E-mail', value: params['email']},
                {id: 'userFormPhone', placeholder: 'phone', value: params['phone']}
            ],
            passlabels: [
                {id: 'userFormPass', placeholder: 'password7chars'}, 
                {id: 'userFormPassRepeat', placeholder: 'repeatPassword'}
            ]
        };
        
        return newData;
    },
    
    /**
     * Генерация формы
     * @param {json}
     * @this {string} шаблон
     * @this {json} данные
     * @return {string} - html
     */
    renderForm: function(newData)
    {
        if(newData) return Mustache.to_html(this.template, newData);
        else return Mustache.to_html(this.template, this.data);
    },
    
    /**
     * Валидация
     */
    validateForm: function()
    {
        $('div.error').empty();
        if(!$('#userFormName').val())
        {
            $('div.error').html('enterName')+'!';
            return false;
        }
        if(!$('#userFormSurname').val())
        {
            $('div.error').html('enterSurname')+'!';
            return false;
        }
        if(!$('#userFormEmail').val())
        {
            $('div.error').html('enterEmail')+'!';
            return false;
        }
        if(!$('#userFormPhone').val())
        {
            $('div.error').html('enterPhone')+'!';
            return false;
        }
        if(!$('#userFormPass').val())
        {
            $('div.error').html('enterPassword')+'!';
            return false;
        }
        if(!$('#userFormPassRepeat').val())
        {
            $('div.error').html('repeatPassword')+'!';
            return false;
        }
        if($('#userFormPass').val() != $('#userFormPassRepeat').val())
        {
            $('div.error').html('passwordsNotEqual')+'!';
            return false;
        }
        return true;
    }
}

/**
 * Активация формы
 */
function activateForm()
{
    $('#userForm').arcticmodal({
        afterClose: (function(){        
            $('#userForm').remove();
        })
    });
    
    $('.b-modal_close arcticmodal-close').click(function(){
        $('#userForm').arcticmodal('close');
    });
    $('#userFormContinue').click(function(){
        if(userform.validateForm()) $('#userForm').arcticmodal('close');
    });
}

/**
 * Отправка запроса, вызов генерации и вывод формы
 */
function getForm()
{
    $.ajax({
        type: 'POST',
        url: '/ajax.php',
        data: {
            controller: 'villa.controller.userform',
            action: 'callback'
        },
        success: function(data){
            if(data['result'] == 1)
            {
                if(data['data'])
                {
                    var params = data['data'];
                    var newData = userform.fillForm(params);
                    $('#userFormTest').append(userform.renderForm(newData));                    
                }
                else
                {
                    $('#userFormTest').append(userform.renderForm(false));
                }
                activateForm();
            }
        }
    });
}
                
