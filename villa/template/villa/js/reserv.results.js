function viewAppartment(event)
{
    var imgObjects = $(event.target).parent().children('img');
    var location = $(event.target).parents('li');
    var params = [];
    params['images'] = [];
    $(imgObjects).each(function(){
        params['images'].push($(this).attr('src'));
    });
    params['thumbs'] = 0;
    params['title'] = $(location).find('.number_name').text();
    params['descript'] = $(location).find('.reserv_description').text();
    showViewer(params, location);

}
