customtour = {
    transferValidation: function(){
        $("div.error").empty();
        var trans = $("#tourExtraService").find("td.transfer");  
        $(trans).each(function(){
            var time = $(this).find("input.time").val();
            if(!/\d\d\:\d\d/.test(time)) 
                {                    
                    $(this).find("div.error").text('Укажите время в формате "чч:мм"!');
                }
            else
                {
                    var wagon = $(this).find("input.wagon");
                    var wagonNum = $(wagon).val();
                    if(wagon['length'] != 0 && !/^\d+$/.test(wagonNum))
                        {
                            $(this).find("div.error").text('Укажите номер вагона!');
                        }
                }
        });
    }
}

function tourValidate()
{
    customtour.transferValidation();
}

function bookingFormInit()
{
    $("#railTransferDate").datepicker({minDate: 0, numberOfMonths: [1, 3]});
    $("#airTransferDate").datepicker({minDate: 0, numberOfMonths: [1, 3]});
}
