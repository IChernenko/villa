slideshow = {
    insert: function()
    {
        var images = $('#content img');
        if(images.length)
        {
            $(images).each(function(){
                $(this).clone().appendTo('#b_collage');
            });
            this.init();
        }
        else
        {
            $.ajax({
                type: 'POST',
                url: '/ajax.php',
                data: {
                    controller: 'villa.controller.homepage',
                    action: 'default',
                    gallery: true
                },
                success: function(data){ //console.log(data);
//                    for(var i=0; i<data.length; i++)
//                    {
//                        var img = '<img src="'+data[i]+'">';
//                        $('#b_collage').append(img);
//                    }
//                    slideshow.init();
                }
            });
        }
        
    },
    init: function()
    {

        $('#b_collage').orbit({
            animation: 'horizontal-slide',
            animationSpeed: 800,
            timer: true
        });
        $('#b_collage').css({'width':'574px'});
    }
}