/**
 * Класс авторизации пользователя
 * @author Valkyria
 */
villa_auth = {
    /**
     * Маркер успешно выполненной авторизации
     * @private
     */
    authDone: false,
    lastForm: false,
    
    /**
     * Базовый шаблон, не изменяется за время существования окна
     * @private
     */
    base: '<div id="authForm" class="b-modal villa-modal-form">  \
                <div class="b-modal_close arcticmodal-close"></div>   \
                <div id="authFormBlock">    \
                {{>form}}   \
                </div>  \
                </div>',
    /**
     * Шаблон формы, может быть изменен 
     * @private
     */
    form:  '<div class="strong">  \
            {{#links}}  {{{.}}} {{/links}}    \
            </div>    \
            <ul id="{{ul}}"> \
            {{#inputs}}  \
                    <li><input type="{{type}}" autocomplete="{{acomplete}}" name="{{name}}" placeholder="{{placeholder}}"/></li>    \
            {{/inputs}}  \
            {{#remember}}  \
                    <li><input type="checkbox" name="{{id}}" id="{{id}}" />\
                    <label for="{{id}}">'+villa.labels.saveEmailAndPass+'</label></li> \
            {{/remember}}  \
            </ul>    \
            {{#button}}    \
            <button id="{{id}}" onclick="villa_auth.{{funct}};"><span>{{value}}</span></button>  \
            {{/button}}    \
            {{{passRecover}}}',
    /**
     * Шаблон сообщения об ошибке
     * @private
     */
    error: '<p class="error">{{{message}}}</p>',
    
    /**
     * Шаблон сообщения об успешно проведенной операции.
     * Выводится вместо шаблона формы
     * @private
     */
    success: '<p class="success">   \
                {{{message}}}<br><br>     \
                <button id="authFormContinue"><span>'+villa.labels.continueLbl+'</span></button>    \
             </p>',
    
    /**
     * Контекст формы входа в систему
     * @private
     */
    enter: {
        links: [
            '<span>'+villa.labels.enterSystem+'</span>',
            '<a href="javascript:villa_auth.changeForm(2);" >'+villa.labels.registration+'</a>'
        ],
        ul: 'entList',
        inputs: [
            {type: 'email', name: 'email', acomplete: 'on', placeholder: villa.labels.yourEmail},
            {type: 'password', name: 'password', placeholder: villa.labels.yourPassword} 
        ],
        remember: [
            {id: 'authFormRemember'}
        ],
        button: [
            {id: 'authFormEnterBtn', funct: 'login()', value: villa.labels.doEnter}
        ],
        passRecover: '<a href="javascript:villa_auth.changeForm(3);" class="passRecover">'+villa.labels.forgotPassword+'</a>'
    },
    
    /**
     * Контекст формы регистрации
     * @private
     */
    register: {
        links: [
            '<a href="javascript:villa_auth.changeForm(1);">'+villa.labels.enterSystem+'</a>',
            '<span>'+villa.labels.registration+'</span>'
        ],
        ul: 'regList',
        inputs: [
            {type: 'email', name: 'email', acomplete: 'on', placeholder: villa.labels.yourEmail},
            {type: 'password', name: 'password', placeholder: villa.labels.yourPassword7},
            {type: 'password', name: 'password_repeat', placeholder: villa.labels.repeatPassword} 
        ],
        button: [
            {id: 'authFormRegBtn', funct: 'registration()', value: villa.labels.doRegister}
        ]
    },
    
    /**
     * Контекст формы восстановления пароля
     * @private
     */
    recover: {
        links: [
            '<a href="javascript:villa_auth.changeForm(1);">'+villa.labels.enterSystem+'</a>',
            '<a href="javascript:villa_auth.changeForm(2);">'+villa.labels.registration+'</a>'
        ],
        inputs: [
            {type: 'email', name: 'email', acomplete: 'on', placeholder: villa.labels.registeredEmail}
        ],
        button: [
            {id: 'authFormRecoverBtn', funct: 'passRecover()', value: villa.labels.sendNewPass}
        ]
    },
    
    /**
     * Генерация формы
     * @param {boolean} full - маркер полной(с базовым шаблоном) или неполной генерации
     * @param {json} formData - переменная контекста
     * @return {string}
     */
    renderForm: function(full, formData)
    {
        if(!full) return Mustache.to_html(this.form, formData);
        else 
        {
            var template = {form: this.form};
            return Mustache.to_html(this.base, formData, template);
        }
    },
    
    /**
     * Генерация блока ошибки
     * @param {json} errorText - текст ошибки
     * @return {string}
     */
    renderError: function(errorText)
    {
        return Mustache.to_html(this.error, errorText);
    },
    
    /**
     * Вывод блока успешно проведенной операции
     * @param {json} successText - сообщение системы
     */
    showSuccess: function(successText)
    {
        $('#authForm').html(Mustache.to_html(this.success, successText));
    },
    
    /**
     * Вызов генерации и вывод формы
     */
    showAuthForm: function()
    {
        $('#content').append(this.renderForm(true, this.enter));
        this.activateForm();
        this.lastForm = 1;

        $.ajax({
            type: 'POST',
            url: '/ajax.php',
            data: {
                controller: 'module.auth.controller',
                action: 'getCookies'
            },
            success: function(data){
                if(data.result == 1)
                {
                    jQuery('#authFormBlock input[name=email]').val(data.userLogin);
                    jQuery('#authFormBlock input[name=password]').val(data.userPassword);

                    if(data.userRemember == "true")
                    {
                        jQuery('#authFormRemember').attr('checked', true);
                    }
                    else
                    {
                        jQuery('#authFormRemember').attr('checked', false);
                    }
                    console.log(data);
                }
                else
                {

                }
            }
        });


    },
    
    /**
     * Активация формы
     */
    activateForm: function()
    {
        $('#authForm').arcticmodal({
            afterClose: (function(){        
                $('#authForm').remove();
                if(villa_auth.authDone) location.reload();
            })
        });

        $('.b-modal_close arcticmodal-close').click(function(){
            $('#authForm').arcticmodal('close');
        });
    },
    
    /**
     * Вызов смены контекста формы
     * @param {int} form - номер формы
     */
    changeForm: function(form)
    {
        var newForm = false;
        switch(form)
        {
            case 1:
                newForm = this.renderForm(false, this.enter);
                this.lastForm = 1;
                break;
            case 2:
                newForm = this.renderForm(false, this.register);
                this.lastForm = 2;
                break;
            case 3:
                newForm = this.renderForm(false, this.recover);
                this.lastForm = 3;
                break;
        }
        $('#authFormBlock').html(newForm);
    },
    
    /**
     * Валидация полей формы входа
     * @return {boolean}
     */
    validateEnter: function()
    {
        var text = false;
        if(!$('#authFormBlock input[name=email]').val())
        {
            $('p.error').remove();
            text = {message: villa.labels.enterEmail};
            $('#authFormBlock').append(this.renderError(text));
            return false;
        }
        if(!$('#authFormBlock input[name=password]').val())
        {
            $('p.error').remove();
            text = {message: villa.labels.enterPassword};
            $('#authFormBlock').append(this.renderError(text));
            return false;
        }
        return true;
    },
    
    /**
     * Авторизация пользователя
     */
    login: function()
    {
        if(!this.validateEnter()) return false;
        var login = $('#authFormBlock input[name=email]').val();
        var password = $('#authFormBlock input[name=password]').val();
        var remember = jQuery("#authFormRemember").prop("checked");
        dante.ajaxContentLoadStart('#authFormBlock');
        $.ajax({
            type: 'POST',
            url: '/ajax.php',
            data: {
                controller: 'module.auth.controller',
                action: 'login',
                login: login,
                password: password,
                remember: remember
            },
            success: function(data){
                if(data.result == 1) villa_auth.authDone = true;
                villa_auth.callbackProcess(data, true);
                //console.log(villa_auth.authDone);
            }
        });
    },
    
    /**
     * Валидация полей формы регистрации
     * @return {boolean}
     */
    validateRegister: function()
    {
        var text = false;
        if(!$('#authFormBlock input[name=email]').val())
        {
            $('p.error').remove();
            text = {message: villa.labels.enterEmail};
            $('#authFormBlock').append(this.renderError(text));
            return false;
        }
        var pattern = /\b^[\w\.\-]+\@[a-z]+\.[a-z]{2,3}\b/i;
        if(!pattern.test($('#authFormBlock input[name=email]').val()))
        {
            $('p.error').remove();
            text = {message: villa.labels.wrongEmail};
            $('#authFormBlock').append(this.renderError(text));
            return false;
        }
        if(!$('#authFormBlock input[name=password]').val())
        {            
            $('p.error').remove();
            text = {message: villa.labels.enterPassword};
            $('#authFormBlock').append(this.renderError(text));
            return false;
        }
        if(!$('#authFormBlock input[name=password_repeat]').val())
        {
            $('p.error').remove();
            text = {message: villa.labels.repeatPassword};
            $('#authFormBlock').append(this.renderError(text));
            return false;
        }
        if($('#authFormBlock input[name=password]').val() != $('#authFormBlock input[name=password_repeat]').val())
        {
            $('p.error').remove();
            text = {message: villa.labels.passNotEqual};
            $('#authFormBlock').append(this.renderError(text));
            return false;
        }
        return true;
    },
    
    /**
     * Регистрация пользователя
     */
    registration: function()
    {
        if(!this.validateRegister()) return false;
        var email = $('#authFormBlock input[name=email]').val();
        var password = $('#authFormBlock input[name=password]').val();
        dante.ajaxContentLoadStart('#authFormBlock');
        $.ajax({
            type: 'POST',
            url: '/ajax.php',
            data: {
                controller: 'villa.app.registration.controller',
                action: 'register',
                email: email,
                password: password
            },
            success: function(data){
                villa_auth.callbackProcess(data, true);
            }
        });
    },
    
    /**
     * Валидация поля формы восстановления пароля
     * @return {boolean}
     */
    validateRecover: function()
    {
        var text = false;
        if(!$('#authFormBlock input[name=email]').val())
        {
            $('p.error').remove();
            text = {message: villa.labels.enterEmail};
            $('#authFormBlock').append(this.renderError(text));
            return false;
        }
        return true;
    },
    
    /**
     * Восстановление пароля
     */
    passRecover: function()
    {
        if(!this.validateRecover()) return false;
        var email = $('#authFormBlock input[name=email]').val();
        dante.ajaxContentLoadStart('#authFormBlock');
        $.ajax({
            type: 'POST',
            url: '/ajax.php',
            data: {
                controller: 'module.passwordchange.controller',
                action: 'send',
                email: email,
                json: 1
            },
            success: function(data){
                //console.log(data);
                villa_auth.callbackProcess(data);
            }
        });
    },
    
    /**
     * Обработка ответов системы
     */
    callbackProcess: function(data, reload)
    {
        if(reload === undefined) reload = false;
        
        $('p.error').remove();
        var text = {message: data.message};
        if(data.result == 1) 
        {
            //console.log(data);   
            this.showSuccess(text);     
            if(reload)
            {
                $('#authFormContinue').hide();
                setTimeout(function(){location.reload();}, 100);
            }
            else
            {                
                $('#authFormContinue').click(function(){
                    $('#authForm').arcticmodal('close');
                });
            }
        }
        else 
        {
            //console.log(data);
            this.changeForm(this.lastForm);
            $('#authFormBlock').append(this.renderError(text));
        }
    }
}