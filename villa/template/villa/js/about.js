function viewHotelImages(event)
{
    var imgObjects = $(event.target).parents('.img-list').find('img');
    var location = $('#aboutHotel');
    var activeIndex = $(imgObjects).index(event.target);
    
    var params = [];
    
    params['images'] = [];
    $(imgObjects).each(function(){
        params['images'].push($(this).attr('src'));
    });
    params['thumbs'] = 0;
    params['title'] = $(location).find('h2').text();
    params['descript'] = $(location).find('.descript').text();    
    
    showViewer(params, location, activeIndex);
}