<?php
global $css;

$css = array(
    '/template/villa/css/reset.css',
    '/template/villa/css/main.css',
    '/template/villa/css/default.css',
    '/template/villa/css/customtour.css',
    '/template/villa/css/userform.css',
    '/template/villa/css/villa_modal_form.css'
);

?>