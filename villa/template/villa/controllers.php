<?php

global $wsControllers;
$wsControllers = array();
$wsControllers['title'] = 'Module_Seo_Controller_Title';
$wsControllers['seo_keywords'] = 'Module_Seo_Controller_Keywords';
$wsControllers['seo_description'] = 'Module_Seo_Controller_Description';
$wsControllers['css'] = 'Dante_Controller_Css';
$wsControllers['js'] = 'Dante_Controller_Js';
$wsControllers['jslabels'] = 'Villa_Controller_Jslabels';
$wsControllers['content'] = 'Dante_Controller_Content';
$wsControllers['lang'] = 'Module_Lang_Controller';
$wsControllers['roomSearch'] = 'Villa_Module_Hotel_Controller_Roomsearch';
$wsControllers['slider'] = 'Module_Slider_Controller';
$wsControllers['profilemenu'] = 'Villa_Module_User_Controller';
$wsControllers['auth_label'] = 'Module_Auth_Label';
$wsControllers['currency'] = 'Villa_Controller_Currency';
$wsControllers['phone'] = 'Villa_Controller_Phone';

?>