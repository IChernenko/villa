UPDATE `[%%]languages` SET `sys_name` = 'en' WHERE `sys_name` = 'eng';

SELECT @en:=`id` FROM `[%%]languages` WHERE `sys_name` = 'en';

INSERT INTO `[%%]labels_lang` (`id`, `lang_id`, `name`)
    SELECT `id`, @en, CONCAT(`sys_name`, '_en') FROM `[%%]labels`;

SELECT @de:=`id` FROM `[%%]languages` WHERE `sys_name` = 'de';

INSERT INTO `[%%]labels_lang` (`id`, `lang_id`, `name`)
    SELECT `id`, @de, CONCAT(`sys_name`, '_de') FROM `[%%]labels`;