INSERT INTO `[%%]division_groups` (`division_id`, `group_id`)
    SELECT `d`.`id`, `g`.`group_id` FROM `[%%]division` AS `d`, `[%%]groups` AS `g`
    WHERE `d`.`link` = '/manage/slider/' AND `g`.`group_name` = 'gendir';

UPDATE `db_version` SET `version` = 2 WHERE `package` = 'module.slider';