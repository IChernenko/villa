<?php

$countryMapper = new Module_Currencycountries_Manage_Mapper();
$countryMapper->paginator = false;
$countries = $countryMapper->getRowsByParams();
$countryMapper->paginator = true;

$langMapper = new Module_Lang_Mapper();
$langs = $langMapper->getList();

foreach($countries['rows'] as $country)
{
    foreach($langs as $lang)
    {
        $table = new Dante_Lib_Orm_Table('[%%]currencycountries_lang');
        $cond = array(
            'country_id' => $country['id'],
            'set_lang' => $lang->id
        );
        if(!$table->exist($cond) || !$table->select($cond)->disp_name)
        {
            $table->country_id = $country['id'];
            $table->set_lang = $lang->id;
            $table->disp_name = $country['title'];
            
            $table->apply($cond);
        }
    }
}

?>
