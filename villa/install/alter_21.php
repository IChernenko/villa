<?php

$labels = array(
    'continueLbl' => 'Продолжить',
    'enterSystem' => 'Вход в систему',
    'registration' => 'Регистрация',
    'yourPassword' => 'Ваш пароль',
    'doEnter' => 'Войти',
    'forgotPassword' => 'Забыли пароль?',
    'yourPassword7' => 'Ваш пароль (7 знаков)',
    'repeatPassword' => 'Повторите пароль',
    'doRegister' => 'Зарегистрировать',
    'registeredEmail' => 'Зарегистрированный E-mail',
    'sendNewPass' => 'Отправить новый пароль',
    'saveEmailAndPass' => 'Сохранить e-mail и пароль',
    'passNotEqual' => 'Пароли не совпадают',
    'yourQuestionSentToAdmin' => 'Ваш вопрос поступил администратору.',
    'yourCommentSentToAdmin' => 'Ваш отзыв поступил администратору.',
    'answerWillBeSentToEmail' => 'Ответ Вы получите на указанный E-mail'
);

$langMapper = new Module_Lang_Mapper();
$lang = $langMapper->getByCode('ru');

$sql = "SELECT `id` FROM `[%%]labels` ORDER BY `id` DESC";
$i = Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'id');
foreach($labels as $label => $value)
{
    $i++;
    $sql = "INSERT INTO `[%%]labels` SET `id` = {$i}, `sys_name` = '{$label}'";
    Dante_Lib_SQL_DB::get_instance()->exec($sql);
    
    $sql = "INSERT INTO `[%%]labels_lang` SET `id` = {$i}, `lang_id` = {$lang}, `name` = '{$value}'";
    Dante_Lib_SQL_DB::get_instance()->exec($sql);
}

?>
