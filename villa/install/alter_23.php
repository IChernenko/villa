<?php

$labels = array(
    'myBookings' => 'Мои бронирования',
    'personalData' => 'Личные данные',
    'discounts' => 'Скидки',
    'passportScan' => 'Скан паспорта',
    'changeEmail' => 'Изменить E-mail',
    'changePassword' => 'Изменить пароль',
    'tourAgentData' => 'Данные турагента',
    'noFileForUpload' => 'Нет файла для загрузки',
    'userAccessOnly' => 'Страница доступна только зарегистированным пользователям',
    'wrongUrl' => 'Неверный url',
    'accessDenied' => 'Отказано в доступе',
    'bookingCalcelled' => 'Бронирование #{id} отменено',
    'bookingHistory' => 'История бронирования',
    'creationDate' => 'Дата создания',
    'changeDate' => 'Дата изменения',
    'roomType' => 'Тип номера',
    'status' => 'Статус',
    'edit' => 'Редактировать',
    'changeArrivalDeparture' => 'Изменить дату заезда / выезда',
    'close' => 'Закрыть',
    'change' => 'Изменить',
    'uploadFile' => 'Загрузить файл',
    'delCurrent' => 'Удалить текущий',
    'payRequisites' => 'Платежные реквизиты',
    'logo' => 'Логотип',
    'postalIndex' => 'Почтовый индекс',
    'address' => 'Адрес',
    'workCompany' => 'Компания (место работы)',
    'curEmail' => 'Текущий E-mail',
    'newEmail' => 'Новый E-mail',
    'curPass' => 'Текущий пароль',
    'newPass' => 'Новый пароль',
    'enterEmail' => 'Введите E-mail',
    'wrongEmail' => 'Неверный E-mail',
    'enterCurPassword' => 'Введите текущий пароль',
    'enterNewPassword' => 'Введите новый пароль',
    'repeatNewPassword' => 'Повторите новый пароль',
    'youCantChangeArrival' => 'Вы не можете изменить дату заезда при текущих условиях',
    'pressToChangeArrival' => 'Нажмите "Сохранить" для обновления даты заезда',
    'enterAnotherArrival' => 'Задайте другую дату заезда',
    'noFreeRooms' => 'Нет свободных номеров для выбранного периода',
    'roomSearchSuccess' => 'Поиск номеров успешно завершен',
    'hours' => 'Часы',
    'minutes' => 'Минуты',
    'saving' => 'Сохранение...',
    'recalc' => 'Пересчёт...',
    'dontForgetToResetTransfers' => 'Не забудьте изменить даты и время трансферов',
    'saveAndExit' => 'Сохранить и выйти',
);

$langMapper = new Module_Lang_Mapper();
$ru = $langMapper->getByCode('ru');
$langList = $langMapper->getList();

$sql = "SELECT `id` FROM `[%%]labels` ORDER BY `id` DESC";
$i = Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'id');
foreach($labels as $label => $value)
{
    $i++;
    $sql = "INSERT INTO `[%%]labels` SET `id` = {$i}, `sys_name` = '{$label}'";
    Dante_Lib_SQL_DB::get_instance()->exec($sql);

    foreach($langList as $id => $lang)
    {
        if($id == $ru) $langLabel = $value;
        else $langLabel = $label.'_'.$lang->sys_name;

        $sql = "INSERT INTO `[%%]labels_lang` SET `id` = {$i}, `lang_id` = {$id}, `name` = '{$langLabel}'";
        Dante_Lib_SQL_DB::get_instance()->exec($sql);
    }
}

?>

