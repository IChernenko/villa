INSERT INTO `[%%]groups` (`group_id`, `group_name`, `group_caption`) VALUES
(1, 'moder', 'Модератор'),
(2, 'admin', 'Администратор'),
(3, 'gendir', 'Ген Директор'),
(4, 'user', 'Пользователь'),
(5, 'guest', 'Гость');

INSERT INTO [%%]user_groups (`user_id`, `group_id`) VALUES ('18', '3');
UPDATE [%%]users SET `rating` = '100' WHERE id =18;