<?php

$labels = array(
    'wrongGuestData' => 'Неверно заполнены личные данные гостей',
    'addBookingError' => 'Ошибка при добавлении бронирования',
    'serverError' => 'Ошибка на сервере',
    'message' => 'Сообщение',
    'error' => 'Ошибка',
    'yourBookingAdded' => 'Поздравляем! Ваше бронирование сохранено. Номер брони: ',
    'fillAllTransferFields' => 'Заполните все поля, необходимые для сохранения трансфера!',
    'noArriveDate' => 'Не задана дата заезда',
    'noDepartureDate' => 'Не задана дата выезда',
    'chooseRoomAndPrepayment' => 'Выберите тип номера и тип предоплаты!',
    'fileDeleteSuccess' => 'Файл успешно удален',
    'changesApplied' => 'Изменения сохранены',
    'confirmCancel' => 'Подтвердите отмену',
    'bookingCancelEnterPass' => 'Для подтверждения отмены бронирования введите свой пароль:',
    'servicesInVilla' => 'Услуги в Villa LaScala'
);

$langMapper = new Module_Lang_Mapper();
$lang = $langMapper->getByCode('ru');

$sql = "SELECT `id` FROM `[%%]labels` ORDER BY `id` DESC";
$i = Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'id');
foreach($labels as $label => $value)
{
    $i++;
    $sql = "INSERT INTO `[%%]labels` SET `id` = {$i}, `sys_name` = '{$label}'";
    Dante_Lib_SQL_DB::get_instance()->exec($sql);
    
    $sql = "INSERT INTO `[%%]labels_lang` SET `id` = {$i}, `lang_id` = {$lang}, `name` = '{$value}'";
    Dante_Lib_SQL_DB::get_instance()->exec($sql);
}

?>
