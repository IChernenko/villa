<?php

$labels = array(
    'endBooking' => 'Завершить бронирование',
    'save' => 'Сохранить',
    'question' => 'Вопрос',
    'sendYourQuestion' => 'Отправить ваш вопрос',
    'answer' => 'Ответ',
    'leaveComment' => 'Оставить отзыв',
    'comment' => 'Отзыв',
    'yourName' => 'Ваше имя',
    'yourEmail' => 'Ваш email',
    'yourComment' => 'Ваш отзыв',
    'textFromPicture' => 'Текст с картинки',
    'sendYourComment' => 'Отправить ваш отзыв'
);

$langMapper = new Module_Lang_Mapper();
$lang = $langMapper->getByCode('ru');

$sql = "SELECT `id` FROM `[%%]labels` ORDER BY `id` DESC";
$i = Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'id');
foreach($labels as $label => $value)
{
    $i++;
    $sql = "INSERT INTO `[%%]labels` SET `id` = {$i}, `sys_name` = '{$label}'";
    Dante_Lib_SQL_DB::get_instance()->exec($sql);
    
    $sql = "INSERT INTO `[%%]labels_lang` SET `id` = {$i}, `lang_id` = {$lang}, `name` = '{$value}'";
    Dante_Lib_SQL_DB::get_instance()->exec($sql);
}

?>
