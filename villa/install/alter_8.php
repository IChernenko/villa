<?php

$label = new Module_Labels_Mapper();

$modelFillData = new Module_Labels_Model();
$modelFillData->sys_name = 'fillYourData';
$modelFillData->lang_id = 1;
$modelFillData->name = 'Заполните ваши данные';

$label->add($modelFillData);

$modelPhPass = new Module_Labels_Model();
$modelPhPass->sys_name = 'password7chars';
$modelPhPass->lang_id = 1;
$modelPhPass->name = 'Пароль (7 знаков)';

$label->add($modelPhPass);

$modelPhPassRepeat = new Module_Labels_Model();
$modelPhPassRepeat->sys_name = 'repeatPassword';
$modelPhPassRepeat->lang_id = 1;
$modelPhPassRepeat->name = 'Повторите пароль';

$label->add($modelPhPassRepeat);

$modelErrName = new Module_Labels_Model();
$modelErrName->sys_name = 'enterName';
$modelErrName->lang_id = 1;
$modelErrName->name = 'Введите имя';

$label->add($modelErrName);

$modelErrSurname = new Module_Labels_Model();
$modelErrSurname->sys_name = 'enterSurname';
$modelErrSurname->lang_id = 1;
$modelErrSurname->name = 'Введите фамилию';

$label->add($modelErrSurname);

$modelErrPhone = new Module_Labels_Model();
$modelErrPhone->sys_name = 'enterPhone';
$modelErrPhone->lang_id = 1;
$modelErrPhone->name = 'Введите телефон';

$label->add($modelErrPhone);

$modelErrPassEqual = new Module_Labels_Model();
$modelErrPassEqual->sys_name = 'passwordsNotEqual';
$modelErrPassEqual->lang_id = 1;
$modelErrPassEqual->name = 'Пароли не совпадают';

$label->add($modelErrPassEqual);

?>