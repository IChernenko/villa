<?php

$sql = "SELECT COUNT(*) AS `count`, `sys_name` FROM [%%]labels GROUP BY `sys_name`";

$r = Dante_Lib_SQL_DB::get_instance()->open($sql);

$labels = array();
while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
{
    $labels[$f['sys_name']] = $f['count'];
}

foreach($labels as $name => $count)
{
    if($count > 1)
    {
        $limit = $count - 1;
        $sql = "DELETE FROM [%%]labels WHERE `sys_name` = '{$name}' LIMIT {$limit}";
        Dante_Lib_SQL_DB::get_instance()->exec($sql);
    }
}

$sql = "SELECT COUNT(*) AS `count`, `id` FROM [%%]labels_lang GROUP BY `id`";

$r = Dante_Lib_SQL_DB::get_instance()->open($sql);

$labelsLang = array();
while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
{
    $labelsLang[$f['id']] = $f['count'];
}

foreach($labelsLang as $id => $count)
{
    if($count > 1)
    {
        $limit = $count - 1;
        $sql = "DELETE FROM [%%]labels_lang WHERE `id` = {$id} LIMIT {$limit}";
        Dante_Lib_SQL_DB::get_instance()->exec($sql);
    }
}

?>
