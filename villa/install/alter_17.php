<?php

$labels = array(
    'airlineSearch' => 'Поиск Авиакомпании',
    'country' => 'Страна',
    'town' => 'Город',
    'enter' => 'Вход',
    'exit' => 'Выход',
    'myProfile' => 'Мой профиль',
    'priceCalendar' => 'Календарь цен',
    'roomEquipment' => 'Оборудование номера',
    'roomTypesNote' => 'цены приведены на текущее время, для просмотра цен в интересующие вас даты воспользуйтесь календарем цен или заполните ',
    'roomTypesNoteLink' => 'форму бронирования',
    'generalInfo' => 'Общая информация',
    'numberOfNights' => 'Количество ночей',
    'chooseRoomAndPrepayment' => 'Выберите тип номера и вид предоплаты',
    'choosePrepayment' => 'Выберите вид предоплаты',
    'price' => 'Цена',
    'priceComment' => 'Цены приведены за номер в сутки на текущее время',
    'bookingCost' => 'Стоимость бронирования',
    //bookingform
    'createTourByYourself' => 'Создайте тур своими руками',
    'roomDayPrice' => 'Стоимость номера (в сутки)',
    'adultsInRoom' => 'Взрослых в номере',
    'childrenInRoom' => 'Детей в номере',
    'guestsInRoom' => 'Общее количество проживающих в номере',
    'checkGuestData' => 'Пожалуйста, проверьте данные проживающих в номере',
    'checkChildrenData' => 'Пожалуйста, проверьте данные о детях, проживающих в номере',
    'guestN' => 'Гость №',
    'childN' => 'Ребенок №',
    'showGuestN' => 'Показать гостя №',
    'hideGuestN' => 'Скрыть гостя №',
    'showChildN' => 'Показать ребенка №',
    'hideChildN' => 'Скрыть ребенка №',
    'phoneNum' => 'Номер телефона',
    'countryCode' => 'Код страны',
    'operCode' => 'Код оператора',
    'chooseAdditionalServices' => 'Для комфортного отдыха выберите дополнительные услуги',
    'personDay' => 'с человека в день',
    'days' => 'дней',
    'add' => 'Добавить',
    'excursions' => 'Экскурсии',
    'monday' => 'Понедельник',
    'tuesday' => 'Вторник',
    'wednesday' => 'Среда',
    'thursday' => 'Четверг',
    'friday' => 'Пятница',
    'saturday' => 'Суббота',
    'sunday' => 'Воскресенье',
    'transferArrival' => 'Трансфер - прибытие',
    'transferDeparture' => 'Трансфер - отправление',
    'optionsCost' => 'Стоимость услуг',
    'residenceOptionsCost' => 'Стоимость проживания с услугами',
    'roomPrepayment' => 'Предоплата за номер',
    'totalCost' => 'Общая стоимость',
    'services' => 'Услуги',
    'residence' => 'Проживание',
    'discount' => 'Скидка',
    'payable' => 'К оплате',
    'paid' => 'Оплачено',
    'restToPay' => 'Остаток к оплате',
    'payTotalCost' => 'Оплатить общую стоимость',
    'payServices' => 'Оплатить услуги',
    'payResidence' => 'Оплатить проживание',
    'payAllResidence' => 'Оплатить проживание за весь период',
    'allResidence' => 'Весь период',
    'payFirstNight' => 'Оплатить стоимость первой ночи',
    'firstNight' => 'Первая ночь',
    'printAll' => 'Печатать все',
    'printOffer' => 'Печатать предложение',
    'cancel' => 'Отменить',
    'date' => 'Дата',
    'time' => 'Время',
    'race' => 'Рейс',
    'wagon' => 'Вагон',
    'train' => 'Поезд',
    'back' => 'Назад',
    'specifyAdditionalWishes' => 'Укажите свои дополнительные пожелания'
);

$langMapper = new Module_Lang_Mapper();
$lang = $langMapper->getByCode('ru');

$sql = "SELECT `id` FROM `[%%]labels` ORDER BY `id` DESC";
$i = Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'id');
foreach($labels as $label => $value)
{
    $i++;
    $sql = "INSERT INTO `[%%]labels` SET `id` = {$i}, `sys_name` = '{$label}'";
    Dante_Lib_SQL_DB::get_instance()->exec($sql);
    
    $sql = "INSERT INTO `[%%]labels_lang` SET `id` = {$i}, `lang_id` = {$lang}, `name` = '{$value}'";
    Dante_Lib_SQL_DB::get_instance()->exec($sql);
}

?>
