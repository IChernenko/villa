UPDATE `[%%]division` SET `is_active` = 0 WHERE `link` = '/manage/division/';

DELETE FROM `[%%]division` WHERE `link` IN('/manage/commentextended/', '/manage/discount/', '/manage/transportcompanies/', '/manage/callback/');

UPDATE `[%%]division` SET `name` = 'Инфо-блок (Страница "Номера")' WHERE `link` = '/manage/infoblock/';
UPDATE `[%%]division` SET `name` = 'Отзывы' WHERE `link` = '/manage/comment/';
UPDATE `[%%]division` SET `name` = 'Вопросы' WHERE `link` = '/manage/questions/';
UPDATE `[%%]division` SET `name` = 'Клиенты (Пользователи)' WHERE `link` = '/manage/villauser/';

DROP TABLE IF EXISTS `[%%]commentextended`;
DROP TABLE IF EXISTS `[%%]hotel_discount`;
DROP TABLE IF EXISTS `[%%]hotel_callback`;
DROP TABLE IF EXISTS `[%%]transportcompanies_towns`;
DROP TABLE IF EXISTS `[%%]transportcompanies`;
