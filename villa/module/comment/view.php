<?php
/**
 * Description of view
 *
 * @author Valkyria
 */
class Villa_Module_Comment_View
{
    public $type;
    
    public function draw($list) 
    {
        $tplFileName = '../villa/module/comment/template/list.html';
        
        $tpl = new Dante_Lib_Template();
        $tpl->comments = $list;
        $tpl->entityId = $this->type;
        $tpl->entityTypeId = 1;
        
        $uid = Dante_Helper_App::getUid();
        if($uid > 0) $tpl->uid = $uid;
        
        return $tpl->draw($tplFileName);
    }
    
    public function drawForm($captcha, $user) 
    { 
        $tplFileName = '../villa/module/comment/template/form.html';

        $tpl = new Dante_Lib_Template();
        $tpl->captcha = $captcha;
        $tpl->user = $user;
        $tpl->isQuestion = $this->type == COMMENT_TYPE_QUESTION;
        
        return $tpl->draw($tplFileName);
    }
}

?>
