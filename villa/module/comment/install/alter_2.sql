ALTER TABLE `[%%]comment` DROP COLUMN `approved`;

INSERT INTO `[%%]comment` 
    (`entity_type_id`, `entity_id`, `date`, `name`, `email`, `message`, `is_read`, `answer`)
SELECT 
    1, 2, `t1`.`date`, `t1`.`name`, `t1`.`email`, `t1`.`message`, 
    IF(`t2`.`message` IS NULL, 0, 1) AS `is_read`, `t2`.`message` AS `answer` 
FROM  `[%%]questions` AS  `t1` 
LEFT JOIN  `[%%]questions` AS  `t2` ON  `t2`.`parent_id` =  `t1`.`id` 
WHERE  `t1`.`parent_id` = 0;

DROP TABLE IF EXISTS `[%%]questions`;

DELETE FROM `db_version` WHERE `package` = 'module.questions';

DELETE FROM `[%%]division` WHERE `link` = '/manage/questions/';