ALTER TABLE `[%%]comment` ADD COLUMN `lang_id` INT(11) UNSIGNED NOT NULL AFTER `email`;

UPDATE `[%%]comment` SET `lang_id` = 1;

ALTER TABLE `[%%]comment` ADD CONSTRAINT `fk_comment_lang_id`
    FOREIGN KEY(`lang_id`) REFERENCES `[%%]languages`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;