<?php
/**
 * Description of mapper
 *
 * @author Valkyria
 */
class Villa_Module_Comment_Mapper extends Module_Comment_Mapper_Comment
{
    public function add(Module_Comment_Model_Comment $comment) 
    {
        $table = new Dante_Lib_Orm_Table('[%%]comment');
        $table->date = time();
        $table->lang_id = Module_Lang_Helper::getCurrent();
        $table->name = $comment->name;
        $table->email = $comment->email;
        $table->message = $comment->message;
        $table->entity_id = $comment->entity_id;
        $table->entity_type_id = $comment->entity_type_id;
        $comment->id = (int)$table->insert();
        return true;
    }
    
    public function getList($entityId, $entityTypeId, $conditions = array()) 
    {
        $table = new Dante_Lib_Orm_Table('[%%]comment');
        $conditions['entity_type_id']   = $entityTypeId;
        $conditions['entity_id']        = $entityId;
        $table->get()->where($conditions)->orderby('date desc');
        $table->open();

        $list = array();
        while($f = $table->fetchRecord()) {
            $comment = new Villa_Module_Comment_Model();
            $comment->id = (int)$f['id'];
            $comment->entity_type_id = (int)$f['entity_type_id'];
            $comment->entity_id = (int)$f['entity_id'];
            $comment->date = Dante_Helper_Date::convertToDateTime($f['date']);
            $comment->name = $f['name'];
            $comment->email = $f['email'];
            $comment->message = $f['message'];
            $comment->answer = $f['answer'];

            $list[] = $comment;
        }

        return $list;
    }
}

?>
