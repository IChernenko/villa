<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of comment
 *
 * @author Valkyria
 */
class Villa_Module_Comment_Controller extends Module_Comment_Controller_Comment
{


    protected function _init() 
    {
        parent::_init();
        $this->_mapper = new Villa_Module_Comment_Mapper();
        $this->_view = new Villa_Module_Comment_View();
    }

    protected function _defaultAction() 
    {
        return $this->draw($this->_getParam('type'), 1);
    }

    public function draw($entityId, $entityTypeId) 
    {
        $this->_view->type = $entityId;
        
        $list = $this->_getCommentsList($entityId, $entityTypeId);               
        $listHtml = $this->_view->draw($list);

        $captcha = '';
        
        $uid = Dante_Helper_App::getUid();
        $userMapper = new Villa_Module_User_Manage_Mapper();
        $user = $userMapper->getUser($uid); 
        
        if(!$uid && Dante_Lib_Config::get('comment.allowCaptcha'))
        {
            $service = new Module_Captcha_Service();
            $captcha = $service->showCaptchaImage();
        }
        
        $form = $this->_view->drawForm($captcha, $user);
        
        return $form.$listHtml;
    }
    
    protected function _addAction() 
    {
        $result = parent::_addAction();
        
        if(!$result['result']) return $result;
        
        switch ($this->_getRequest()->get('comment_entity_id')) 
        {
            case COMMENT_TYPE_DEFAULT:
                $result['html'] = Module_Lang_Helper::translate('yourCommentSentToAdmin').'<br/>'.
                    Module_Lang_Helper::translate('answerWillBeSentToEmail');
                break;

            case COMMENT_TYPE_QUESTION:
                $result['html'] = Module_Lang_Helper::translate('yourQuestionSentToAdmin').'<br/>'.
                    Module_Lang_Helper::translate('answerWillBeSentToEmail');
                break;
        }
        
        return $result;
    }
}

?>
