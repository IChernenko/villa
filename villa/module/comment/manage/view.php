<?php

/**
 * Description of view
 *
 * @author Valkyria
 */
class Villa_Module_Comment_Manage_View
{
    public $type;

    public function drawForm($model)
    {
        $tpl = new Dante_Lib_Template();
        $filePath = '../villa/module/comment/manage/template/form.html';
        
        $tpl->comment = $model;
        $controller = 'villa.module.comment.manage.controller';
        $formHead = 'Редактирование ';
        $commentLabel = '';
        
        switch($this->type)
        {
            case COMMENT_TYPE_DEFAULT:
                $controller .= '.comment';
                $formHead .= 'отзыва';
                $commentLabel = 'Отзыв';
                break;
            
            case COMMENT_TYPE_QUESTION:
                $controller .= '.questions';
                $formHead .= 'вопроса';
                $commentLabel = 'Вопрос';
                break;
        }
        
        $tpl->controller = $controller;
        $tpl->formHead = $formHead;
        $tpl->commentLabel = $commentLabel;   
        
        return $tpl->draw($filePath);
    }
    
    public function drawMail($text)
    {
        $tpl = new Dante_Lib_Template();
        $filePath = '../villa/module/comment/manage/template/mail.html';
        
        $tpl->text = $text;
        $tpl->domain = Dante_Lib_Config::get('url.domain_main');     
        return $tpl->draw($filePath);
    }
}

?>
