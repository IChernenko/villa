<?php

/**
 * Description of service
 *
 * @author Valkyria
 */
class Villa_Module_Comment_Manage_Service 
{
    public static function sendLetter($commentType, Villa_Module_Comment_Model $comment)
    {
        $mailType = false;
                
        $data = array(
            'date' => Dante_Helper_Date::converToDateType($comment->date, 3),
            'answer' => $comment->answer
        );
        
        switch($commentType)
        {
            case COMMENT_TYPE_DEFAULT:
                $mailType = MAIL_TYPE_COMMENT;
                $data['comment'] = $comment->message;
                break;
            case COMMENT_TYPE_QUESTION:
                $mailType = MAIL_TYPE_QUESTION;
                $data['question'] = $comment->message;
                break;
        }
        
        if(!$mailType) throw new Exception('wrong comment type');
        
        $builder = Villa_Module_Mail_Builder_Factory::create($mailType);
        $builder->setLang($comment->lang_id);
        $builder->setModel();
        $builder->setData($data);
        $mailModel = $builder->buildMessage();
                
        $service = new Module_Mail_Service_Sender();
        $service->send($comment->email, $mailModel->subject, $mailModel->message);
    }
}

?>
