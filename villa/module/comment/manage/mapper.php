<?php

/**
 * Description of mapper
 *
 * @author Valkyria
 */
class Villa_Module_Comment_Manage_Mapper extends Module_Comment_Mapper_Manage_Comment
{
    protected $_commentType;
    
    public function setCommentType($type)
    {
        $this->_commentType = $type;
    }
    
    public function getRowsByParams($params = false, $cols = "*", $join = false) 
    {
        if($this->_commentType)
        {
            if(!$params) $params = array();
            if(!isset($params['where'])) $params['where'] = array();

            $params['where'][] = '`entity_id`='.$this->_commentType;
        }
        
        return parent::getRowsByParams($params, $cols, $join);
    }

    protected function _instanceModel() 
    {
        return new Villa_Module_Comment_Model();
    }
    
    protected function _arrayToModel($fields)
    {        
        $model = parent::_arrayToModel($fields);
        $model->answer = $fields['answer'];
        $model->lang_id = $fields['lang_id'];
        
        return $model;
    }
    
    public function apply($params) 
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->date = $params->date;
        $table->name = $params->name;
        $table->email = $params->email;
        $table->message = $params->message;
        $table->answer = $params->answer;
        
        if($params->id) $table->update(array('id'=>$params->id));
        else 
        {
            $table->entity_id = 1;
            $table->entity_type_id = 1;
            $table->insert();
        }
    }
    
    public function getNewCommentsCount()
    {
        $sql = "SELECT COUNT(*) AS `count` FROM {$this->_tableName} 
            WHERE `entity_id` = ".COMMENT_TYPE_DEFAULT." AND `is_read` = 0";
        return Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'count');
    }
    
    public function getNewQuestionsCount() 
    {
        $sql = "SELECT COUNT(*) AS `count` FROM {$this->_tableName} 
            WHERE `entity_id` = ".COMMENT_TYPE_QUESTION." AND `is_read` = 0";
        return Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'count');
    }
}

?>
