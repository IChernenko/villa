<?php
/**
 * Description of questions
 *
 * @author Valkyria
 */
class Villa_Module_Comment_Manage_Controller_Questions extends Villa_Module_Comment_Manage_Controller_Base
{
    protected function _setCommentType() 
    {
        $this->_commentType = COMMENT_TYPE_QUESTION;
    }
    
    protected function _setAlias() 
    {
        parent::_setAlias();
        $this->_alias .= '.questions';
    }
    
    protected function _setHead() 
    {
        parent::_setHead();
        
        $this->thead[0][4]['content'] = 'Вопрос';
                
        $this->caption = 'Вопросы';
    }
}

?>
