<?php

/**
 * Description of base
 *
 * @author Valkyria
 */
abstract class Villa_Module_Comment_Manage_Controller_Base extends Module_Comment_Controller_Manage_Comment
{
    protected $_commentType;

    protected function _instanceView() 
    {
        $view = new Villa_Module_Comment_Manage_View();
        $view->type = $this->_commentType;
        return $view;
    }
    
    protected function _instanceModel() 
    {
        return new Villa_Module_Comment_Model();
    }
    
    protected function _init() 
    {
        parent::_init();
        $this->_setAlias();
        $this->_setCommentType();
        $this->_mapper = new Villa_Module_Comment_Manage_Mapper();
        $this->_mapper->setCommentType($this->_commentType);
    }
    
    protected function _setAlias()
    {
        $this->_alias = 'villa.module.comment.manage.controller';
    }
    
    protected abstract function _setCommentType();

    protected function _setHead() 
    {
        parent::_setHead();
        
        $funcItem = array_pop($this->thead[0]);
        
        $this->thead[0][] = array(
            'content' => 'Ответ',
            'sortable' => 'answer'
        );
        
        $this->thead[0][] = $funcItem;
    }
    
    protected function _setBody($rows) 
    {
        $table = array();
        
        foreach($rows as $k => $cur)
        {
            $row = array();
            $row['id'] = $cur['id'];
            $row['name'] = $cur['name'];
            $row['email'] = $cur['email'];
            $row['date'] = Dante_Helper_Date::converToDateType($cur['date']);
            $row['message'] = $cur['message'];
            $row['is_read'] = $this->_isRead[$cur['is_read']];
            $row['answer'] = $cur['answer'];
            $row['buttons'] = array(
                'id'=>$cur['id'],
                'type'=>Dante_Lib_Config::get('comment.allowDel')?array('edit', 'del'):array('edit'),                
            );  
                        
            $table[]=array(
                'content' => $row,
            );
        }
        
        $this->tbody = $table;
    }
    
    protected function _editAction() 
    {
        $result = parent::_editAction();
        
        $model = $this->_getModel();
        if($model->answer)
            Villa_Module_Comment_Manage_Service::sendLetter($this->_commentType, $model);
        
        return $result;
    }

    protected function _getModel() 
    {
        $model = parent::_getModel();
        $model->answer = $this->_getRequest()->get('answer');
        $model->lang_id = $this->_getRequest()->get('lang_id');
        $model->entity_id = $this->_commentType;
        
        return $model;
    }
}

?>
