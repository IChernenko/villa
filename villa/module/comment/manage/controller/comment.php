<?php
/**
 * Description of comment
 *
 * @author Valkyria
 */
class Villa_Module_Comment_Manage_Controller_Comment extends Villa_Module_Comment_Manage_Controller_Base
{
    protected function _setCommentType() 
    {
        $this->_commentType = COMMENT_TYPE_DEFAULT;
    }
    
    protected function _setAlias() 
    {
        parent::_setAlias();
        $this->_alias .= '.comment';
    }


    protected function _setHead() 
    {
        parent::_setHead();
        
        $this->thead[0][4]['content'] = 'Отзыв';
        
        $this->caption = 'Отзывы';
    }
}

?>
