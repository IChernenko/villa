<?php

/**
 * Description of controller
 *
 * @author Valkyria
 */
class Villa_Module_Comment_Manage_Controller extends Module_Comment_Controller_Manage_Comment
{
    protected $_alias = 'villa.module.comment.manage.controller';
    
    protected function _instanceView() 
    {
        return new Villa_Module_Comment_Manage_View();
    }
    
    protected function _instanceModel() 
    {
        return new Villa_Module_Comment_Model();
    }
    
    protected function _init() 
    {
        parent::_init();
        $this->_mapper = new Villa_Module_Comment_Manage_Mapper();
    }

    protected function _setHead() 
    {
        parent::_setHead();
        
        $funcItem = array_pop($this->thead[0]);
        
        $this->thead[0][] = array(
            'content' => 'Ответ',
            'sortable' => 'answer'
        );
        
        $this->thead[0][] = $funcItem;
    }
    
    protected function _setBody($rows) 
    {
        $table = array();
        
        foreach($rows as $k => $cur)
        {
            $row = array();
            $row['id'] = $cur['id'];
            $row['name'] = $cur['name'];
            $row['email'] = $cur['email'];
            $row['date'] = Dante_Helper_Date::converToDateType($cur['date']);
            $row['message'] = $cur['message'];
            $row['is_read'] = $this->_isRead[$cur['is_read']];
            $row['answer'] = $cur['answer'];
            $row['buttons'] = array(
                'id'=>$cur['id'],
                'type'=>Dante_Lib_Config::get('comment.allowDel')?array('edit', 'del'):array('edit'),                
            );  
                        
            $table[]=array(
                'content' => $row,
            );
        }
        
        $this->tbody = $table;
    }
    
    protected function _editAction() 
    {
        $result = parent::_editAction();
        
        if($answer = $this->_getRequest()->get('answer'))
        {
            $to = $this->_getRequest()->get('email');
            Villa_Module_Comment_Manage_Service::sendLetter($to, $answer);
        }
        
        return $result;
    }

    protected function _getModel() 
    {
        $model = parent::_getModel();
        $model->answer = $this->_getRequest()->get('answer');
        
        return $model;
    }
}

?>
