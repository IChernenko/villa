villa_manage_admins = {
    controller: 'villa.module.admins.controller',
    
    formSubmit: function()
    {        
        var data = $('#villaAdminForm').serializeArray();
        
        $.ajax({
            data: data,
            url: '/ajax.php',
            dataType : "json",
            success: function (data){ 
                if(data.result == 1)
                {
                    if(data.html) $('.modal-body').html(data.html);
                    jAlert('Изменения сохранены', 'Сообщение');
                }
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    },
    saveDivisions: function()
    {       
        var data = $('#villaAdminDivisionsForm').serializeArray();
        
        $.ajax({
            url: '/ajax.php',
            dataType : "json",
            data: data,
            success: function (data)
            {
                if(data.result == 1)
                {
                    if(data.html) $('.tab-pane.active').html(data.html);
                    jAlert('Изменения сохранены', 'Сообщение');
                }
                else
                    jAlert(data.message, 'Ошибка');
                
            }
        });
    },  
    loadContent: function(obj)
    {        
        var event_id = $(obj).attr('href');
        var data = {
            controller: villa_manage_admins.controller
        };
        if(!(data.action = villa_manage_admins.getAction(event_id)))           
            return false;
        
        data.group_id = $('#groupId').val();
        
        $(event_id).html(villa_manage_admins.loaderHtml);
        
        $.ajax({
            type: 'POST',
            data: data,
            url: '/ajax.php',
            dataType : "json",
            success: function (data){ 
                if(data.result==1){
                    $(event_id).html(data.html);
                }else{
                    $(event_id).html(data.message);
                }
            }
        })
        
    },
    getAction: function(id)
    {        
        var action = false;
        switch(id)
        {
            case '#edit':
                break;
            case '#divisions':
                action = 'drawDivisions';
                break;
            default:
                break;
        }
        
        return action;
    },
    loaderHtml:'<h1>Loading...</h1>'+
        '<div class="progress progress-striped active" style="width:150px; margin:0 auto;">'+
        '<div class="bar" style="width: 100%;"></div></div>'
}