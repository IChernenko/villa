<?php

/**
 * Description of mapper
 *
 * @author Nika
 */
class Villa_Module_Admins_Mapper 
{
    protected $_tableName = '[%%]division_groups';

    public function saveAdminDivisions($group, $divisions)
    {
        $sql = "DELETE FROM {$this->_tableName} WHERE `group_id` = {$group}";
        Dante_Lib_SQL_DB::get_instance()->exec($sql);
        
        foreach($divisions as $divId)
        {
            $sql = "INSERT INTO {$this->_tableName} SET `division_id` = {$divId}, `group_id` = {$group}";            
            Dante_Lib_SQL_DB::get_instance()->exec($sql);
            
            Dante_Lib_Log_Factory::getLogger()->debug($sql, 'logs/my.log');
        }
    }
    
    public function getSelectedDivisions($groupId)
    {
        $sql = "SELECT `division_id` FROM {$this->_tableName} WHERE `group_id` = {$groupId}";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);        
        
        $list = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) 
        {
            $list[] = $f['division_id'];
        }
        
        return $list;
    }
    
    public function getDivisionsNames()
    {
        $sql = "SELECT `id`, `name` FROM [%%]division WHERE `parent_id` = 2 AND `is_active` = 1";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);        
        
        $list = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) 
        {
            $list[$f['id']] = $f['name'];
        }
        
        return $list;
    }
}
