<?php
/**
 * Description of view
 *
 * @author Nika
 */
class Villa_Module_Admins_View 
{
    public function drawForm($model)
    {
        $tplFileName = '../villa/module/admins/template/form.html';       
        
        $tpl = new Dante_Lib_Template();
        $tpl->model = $model;
        
        return $tpl->draw($tplFileName);
    }
    
    public function drawDivisions($groupId, $divisions, $selected)
    {        
        $tplFileName = '../villa/module/admins/template/divisions.html';       
        
        $tpl = new Dante_Lib_Template();
        $tpl->divisions = $divisions;
        $tpl->selected = $selected;
        $tpl->groupId = $groupId;
        
        return $tpl->draw($tplFileName);
    }
}
