<?php

/**
 * Description of admins
 *
 * @author Valkyria
 */
class Villa_Module_Admins_Controller extends Villa_Module_User_Manage_Controller
{
    protected $_alias = 'villa.module.admins.controller';
    
    protected function _getDefaultFilter() 
    {        
        return '`t1`.`rating` = 100';
    }
    
    protected function _instanceView() 
    {
        return new Villa_Module_Admins_View();
    }

    protected function _genTitles()
    {          
        $titles = array(
            array(
                'content'=>"id",
                'sortable'  =>'id',
                'sortable_def' =>'desc'
            ),
            array(
                'content'   =>'Логин',
                'sortable'  =>'email'
            ),
            array(
                'content'   =>'Функции',
                'params'=>array(
                    'style'=>array(
                        'width'=>'130px'
                    )
                )                
            ),
        );
        
        return $titles;
    }
    
    protected function _genFilters()
    {        
        $request = $this->_getRequest();  
        $filters = array(
            null,            
            "email"=>array(
                "type"=>"text",
                "value"=>$request->get_validate("email", "text"),
            ),           
            array(
                "type"=>"button_filter"
            ),            
        );
        
        return $filters;
    }


    protected function _genRows($rows)
    {               
        $table = array();
       
        foreach($rows as $row)
        {           
            if($row['email'] == 'admin') continue;
            
            $cur = array();
            $cur['id'] = $row['id'];          
            $cur['email'] = $row['email'];               
            $cur['buttons'] = array(
                'id'=>$row['id'],
                'type'=>array('edit', 'del'),
                'content'=>$this->_buttonActive($row['id'],$row['is_active'])
            );            
            
            if($row['is_active'])
                $params['class']='success';
            else
                $params['class']='error';            
            
            $table[$row["id"]]= array(
                'content'=>$cur,
                'params'=>$params
            );
        }
        
        return $table;
    }

    protected function _populateModel()
    {    
        $model = new Villa_Module_User_Model(); 
        $request = $this->_getRequest();
        $model->id      = $request->get_validate('id', 'int', false);
        $model->email   = $request->get_validate('email', 'text', false);
                
        if($password = $request->get_validate('password', 'password'))
            $model->password = $password;
        
        $model->rating = 100;
        $model->group_id = $request->get('group_id', 'int'); 
        
        if(!$model->group_id) $model->group_id = $this->_createGroup($model->email);
                
        return $model;
    }
    
    protected function _validate($model) 
    {   
        $required = array(
            'password'=> 'Пароль',
            'email'=> 'Логин',    
        );        

        if($model->id) unset($required['password']);        
        foreach($required as $k=>$v)
        {
            if(!$model->$k){
                throw new Exception("Поле '{$v}' не заполнено");
            }
        } 
                
        if(!preg_match('/^\w+$/i', $model->email))
                throw new Exception("Поле 'Логин' введено не корректно");
    }

    protected function _createGroup($login)
    {
        $mapper = new Module_Group_Mapper();
        $model = new Module_Group_Model();
        
        $model->name = $model->caption = 'adm_'.$login;
        $mapper->add($model);
        
        return $model->id;
    }
    
    protected function _drawDivisionsAction()
    {
        $groupId = $this->_getRequest()->get('group_id');
        $mapper = new Villa_Module_Admins_Mapper();
        
        $selected = $mapper->getSelectedDivisions($groupId);
        $allDivisions = $mapper->getDivisionsNames();
        
        return array(
            'result' => 1,
            'html' => $this->_instanceView()->drawDivisions($groupId, $allDivisions, $selected)
        );
    }
    
    protected function _saveDivisionsAction()
    {        
        $groupId = $this->_getRequest()->get('group_id');
        $divisions = array_keys($this->_getRequest()->get('divisions'));
        
        $mapper = new Villa_Module_Admins_Mapper();
        $mapper->saveAdminDivisions($groupId, $divisions);
        
        return array(
            'result' => 1
        );
    }

    public function _getSeoAjaxAction() {
        $sql = 'SELECT * FROM tst_room_types_seo WHERE lang_id = ' . $this->_getRequest()->get('lang_id');
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $fields = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r);

//        $table = new Dante_Lib_Orm_Table('tst_room_types_seo');
//        $exist = $table->exist(array('lang_id' => $this->_getRequest()->get('lang_id')));
//
//        if ( $exist ) {
//            $table = $table->select_array(array('lang_id' => $this->_getRequest()->get('lang_id')));
//        }
        return $fields;
    }

    public function _seoAjaxAction() {

        $table = new Dante_Lib_Orm_Table('tst_room_types_seo');
        $exist = $table->exist(array('lang_id' => $this->_getRequest()->get('lang_id')));


        $table->title = $this->_getRequest()->get('title');
        $table->description = $this->_getRequest()->get('description');
        $table->keywords = $this->_getRequest()->get('keywords');
//
        if ( $exist ) {
            $table->update(array('lang_id' => $this->_getRequest()->get('lang_id')));
        } else {
            $table->lang_id = $this->_getRequest()->get('lang_id');
            $table->insert();
        }
        return $table;
    }


}
