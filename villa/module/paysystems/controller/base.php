<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of base
 *
 * @author Valkyria
 */
abstract class Villa_Module_Paysystems_Controller_Base extends Dante_Controller_Base
{    
    protected abstract function _instanceModel();
    protected abstract function _instanceMapper();

    protected final function _payByFormAction()
    {
        if($this->_setSessionStatus()) 
        {
            $bookingId = Villa_Module_Hotel_Helper_Session::getBookingId(); 
            $this->_afterPaymentByForm($bookingId);
            
            header('Location: /mybookings/edit-'.$bookingId);
        }
    }
    
    protected final function _afterPaymentByForm($bookingId)
    {
        if(!Villa_Module_Hotel_Helper_Session::getBookingEditMode())
        {
            $userMapper = new Villa_Module_User_Manage_Mapper();
            $user = $userMapper->getUser(Dante_Helper_App::getUid());

            $user->password = Villa_Module_Hotel_Helper_Session::getNewUser();

            $service = new Villa_Module_Hotel_Service_Booking();
            if(Villa_Module_User_Helper::checkTourAgent())
            {            
                $service->setMode(BOOKING_MAIL_MODE_TOURAGENT);
            }
            $service->notifyUser($user, $bookingId);
            
            Villa_Module_Hotel_Helper_Session::setNewBookingAfterPayment();
        }  
                
        Villa_Module_Hotel_Helper_Session::resetAll();
    }


    protected final function _payByProfileAction()
    {
        if($this->_setSessionStatus()) header('Location: /mybookings/');
    }  
    
    protected final function _setSessionStatus()
    {
        $bookingId = Villa_Module_Hotel_Helper_Session::getBookingId();
        if(!$bookingId) return false;
        
        $status = $this->_getStatus($bookingId);
        if(!$status) return false;
        
        Villa_Module_Hotel_Helper_Session::setPaymentStatus($status);
        return true;
    }

    protected abstract function _getStatus($bookingId);
}

?>
