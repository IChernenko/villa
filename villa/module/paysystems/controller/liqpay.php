<?php
/**
 * Description of liqpay
 *
 * @author Valkyria
 */
class Villa_Module_Paysystems_Controller_Liqpay extends Villa_Module_Paysystems_Controller_Base
{
    protected function _instanceModel()
    {
        return new Module_Liqpay2_Manage_Model();
    }
    protected function _instanceMapper()
    {
        return new Module_Liqpay2_Mapper();
    }

    protected function _defaultAction()
    {
        $liqpayModel = $this->_instanceModel();
                
        $liqpayModel->public_key = Module_Liqpay2_Helper::getPublicKey();
        $liqpayModel->private_key = Module_Liqpay2_Helper::getPrivateKey();
        
        $liqpayModel->data = $this->_getRequest()->get('data');
        $liqpayModel->signature = $this->_getRequest()->get('signature');
        
        $liqpayModel = Module_Liqpay2_Helper::parseData($liqpayModel);    
        
        if(Module_Liqpay2_Helper::checkSignature($liqpayModel))
        {   
            $stateList = Module_Liqpay2_Helper::getStatusList();
            $liqpayModel->status = (int)array_search($liqpayModel->status, $stateList);
            $liqpayModel->time = time();
            
            $mapper = $this->_instanceMapper();
            $mapper->apply($liqpayModel);
            
            Dante_Lib_Observer_Helper::fireEvent('internet_payment', $liqpayModel);
            
            if(Module_Liqpay2_Helper::getStatusType($liqpayModel->status) == LIQPAY_STATUS_SUCCESS)
            {
                $customData = Villa_Module_Paysystems_Helper_Liqpay::parseDescription($liqpayModel->data);

                $bookingMapper = new Villa_Module_Hotel_Mapper_Manage_Booking();
                $paymentModel = new Villa_Module_Hotel_Model_Manage_Payment();
                $paymentModel->booking_id = $liqpayModel->order_id;
                $paymentModel->user_id = $customData['uid'];
                $paymentModel->payment_id = $liqpayModel->id;
                $paymentModel->type = PAYMENT_TYPE_INCOME;
                $paymentModel->subject = $customData['subject'];
                $paymentModel->date = $liqpayModel->time;
                $paymentModel->amount = Module_Currency_Helper::deconvertFrom($liqpayModel->amount, $liqpayModel->currency);
                $paymentModel->pay_way = Villa_Module_Hotel_Helper_Manage_Payment::getPayWay('L');
                $paymentModel->description = $liqpayModel->description != NULL ? $liqpayModel->description : '-';

                $bookingMapper->createPayment($paymentModel);
            }
        }
    }
    
    protected function _getStatus($bookingId) 
    {        
        $statusLiqpay = $this->_instanceMapper()->getStatusByOrder($bookingId);
        $statusType = Module_Liqpay2_Helper::getStatusType($statusLiqpay);
        
        switch($statusType) 
        {
            case LIQPAY_STATUS_SUCCESS:
                $status = PAYMENT_STATUS_SUCCESS;
                break;
            
            case LIQPAY_STATUS_WAIT:
                $status = PAYMENT_STATUS_WAIT;
                break;
            
            case LIQPAY_STATUS_ERROR:
                $status = PAYMENT_STATUS_ERROR;
                break;
        }
        
        return $status;
    }
}

?>
