<?php

class Villa_Module_Paysystems_Controller_Paypal extends Villa_Module_Paysystems_Controller_Base
{
    protected function _instanceModel()
    {
        return new Module_Paypal_Model();
    }
    protected function _instanceMapper()
    {
        return new Module_Paypal_Mapper();
    }

    protected function _defaultAction()
    {
        $paypalModel = $this->_instanceModel();
        $paypalModel->fillFromRequest($this->_getRequest());
        
        $mapper = $this->_instanceMapper();
        $mapper->add($paypalModel);
                
        Dante_Lib_Observer_Helper::fireEvent('internet_payment', $paypalModel);
        
        if($paypalModel->payment_status == 'Completed')
        {
            $customData = Villa_Module_Paysystems_Helper_Paypal::parseCustomData($paypalModel->custom);
            
            $bookingMapper = new Villa_Module_Hotel_Mapper_Manage_Booking();
            $paymentModel = new Villa_Module_Hotel_Model_Manage_Payment();
            $paymentModel->booking_id = $paypalModel->item_number;
            $paymentModel->user_id = $customData['uid'];
            $paymentModel->type = PAYMENT_TYPE_INCOME;
            $paymentModel->subject = $customData['subject'];
            $paymentModel->date = time();
            $paymentModel->amount = Module_Currency_Helper::deconvertFrom($paypalModel->mc_gross, $paypalModel->mc_currency);
            $paymentModel->pay_way = Villa_Module_Hotel_Helper_Manage_Payment::getPayWay('P');
            $paymentModel->description = $paypalModel->item_name != NULL ? $paypalModel->item_name : '-';
            
            $bookingMapper->createPayment($paymentModel);
        }
    }
    
    protected function _getStatus($bookingId) 
    {
        $status = $this->_instanceMapper()->getStatusByItemNum($bookingId);
        
        if($status == 'Completed') return PAYMENT_STATUS_SUCCESS;
        else return PAYMENT_STATUS_ERROR;
    }

    protected function _testAction()
    {
        $params = array(
            'controller' => 'villa.module.paysystems.controller.paypal',
            'payment_type' => 'echeck',
            'payment_date' => '16:45:48 May 28, 2012 PDT',
            'payment_status' => 'Completed',
            'address_status' => 'confirmed',
            'payer_status' => 'verified',
            'first_name' => 'John',
            'last_name' => 'Smith',
            'payer_email' => 'buyer@paypalsandbox.com',
            'payer_id' => 'TESTBUYERID01',
            'address_name' => 'John Smith',
            'address_country' => 'United States',
            'address_country_code' => 'US',
            'address_zip' => '95131',
            'address_state' => 'CA',
            'address_city' => 'San Jose',
            'address_street' => '123, any street',
            'business' => 'seller@paypalsandbox.com',
            'receiver_email' => 'seller@paypalsandbox.com',
            'receiver_id' => 'TESTSELLERID1',
            'residence_country' => 'US',
            'item_name' => 'something',
            'item_number' => $this->_getParam('%1'),
            'quantity' => '1',
            'shipping' => '0',
            'tax' => '0',
            'mc_currency' => $this->_getParam('%4'),
            'mc_fee' => '0',
            'mc_gross' => $this->_getParam('%3'),
            'txn_type' => 'web_accept',
            'txn_id' => '485282345',
            'notify_version' => '2.1',
            'custom' => 's:'.$this->_getParam('%2').';u:'.$this->_getParam('%5'),
            'invoice' => 'abc1234',
            'charset' => 'utf-8',
            'verify_sign' => 'AVGUa04bUtMm3j7yed7qpzQsUMbEAjf-R-uwAtkl4Rw-kxjQ07pNYaXE',
        );
        
        var_dump($params);
        
        $url = Dante_Lib_Config::get('url.domain_main').'/ajax.php';
        echo $url.'<br/>';
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        
        curl_exec($ch);
        curl_close($ch);
        
        echo '<br/>test complete. check user profile, db and admin<br/>';
    }
}

?>