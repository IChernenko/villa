<?php
/**
 * Description of factory
 *
 * @author Valkyria
 */
class Villa_Module_Paysystems_Factory 
{
    protected static $_systemId;
    
    public static function getSystemId()
    {
        if(!self::$_systemId) self::$_systemId = Villa_Module_Countries_Helper::getPaymentSystem();
        return self::$_systemId;
    }

    public static function getPaySystemHelper()
    {
        $system = self::getSystemId();
        
        $helper = null;
        switch($system)
        {
            case PAYMENT_SYSTEM_PAYPAL:
                $helper = new Villa_Module_Paysystems_Helper_Paypal();
                break;
            
            case PAYMENT_SYSTEM_LIQPAY:
                $helper = new Villa_Module_Paysystems_Helper_Liqpay();
                break;
            
            default:
                $helper = PAYMENT_SYSTEM_PAYPAL;
                break;
        }
        
        return $helper;
    }
}

?>
