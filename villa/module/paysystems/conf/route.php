<?php

Dante_Lib_Router::addRule('/^\/paypal\/notify\/$/',array(
    'controller' => 'Villa_Module_Paysystems_Controller_Paypal',
));

Dante_Lib_Router::addRule('/^\/paypal\/pay-by-form\/$/', array(
    'controller' => 'Villa_Module_Paysystems_Controller_Paypal',
    'action' => 'payByForm'
));
Dante_Lib_Router::addRule('/^\/paypal\/pay-by-profile\/$/', array(
    'controller' => 'Villa_Module_Paysystems_Controller_Paypal',
    'action' => 'payByProfile'
));

Dante_Lib_Router::addRule('/^\/paypal-test\/b=(\d+)_s=(\d+)_amt=([\d\.]+)_ccy=(\w+)_u=(\d+)$/', array(
    'controller' => 'Villa_Module_Paysystems_Controller_Paypal',
    'action' => 'test'
));


Dante_Lib_Router::addRule('/^\/liqpay\/notify\/$/',array(
    'controller' => 'Villa_Module_Paysystems_Controller_Liqpay',
));

Dante_Lib_Router::addRule('/^\/liqpay\/pay\-by\-form\/$/', array(
    'controller' => 'Villa_Module_Paysystems_Controller_Liqpay',
    'action' => 'payByForm'
));
Dante_Lib_Router::addRule('/^\/liqpay\/pay-by-profile\/$/', array(
    'controller' => 'Villa_Module_Paysystems_Controller_Liqpay',
    'action' => 'payByProfile'
));

//Dante_Lib_Router::addRule('/^\/liqpay-test\/b=(\d+)_s=(\d+)_amt=([\d\.]+)_ccy=(\w+)_u=(\d+)$/', array(
//    'controller' => 'Villa_Module_Paysystems_Controller_Paypal',
//    'action' => 'test'
//));

?>
