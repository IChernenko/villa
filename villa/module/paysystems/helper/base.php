<?php
/**
 * Description of base
 *
 * @author Valkyria
 */
abstract class Villa_Module_Paysystems_Helper_Base 
{
    protected $_urlPrefix = '';

    protected $_notifyUrl;
    protected $_returnUrl;
    
    protected $_model;

    public $bookingId;
    
    public $prepayment;
    
    public $prepaymentId;
    
    public $roomType;
    
    public $currencyCode;
    
    public $uid;
    
    protected $_buttonTitle = '';
    protected $_description = '';
    protected $_formId = 'pay';
    protected $_onSubmitHtml;
    
    public function setNotifyUrl($url)
    {
        $this->_notifyUrl = Dante_Lib_Config::get('url.domain_main').$this->_urlPrefix.$url;
    }
    
    public function setReturnUrl($url)
    {
        $this->_returnUrl = Dante_Lib_Config::get('url.domain_main').$this->_urlPrefix.$url;
    }

    protected final function _setBtnFormParams($subject)
    {
        switch($subject)
        {
            case PAYMENT_SUBJECT_ALL:
                $this->_formId .= 'TotalCost';
                $this->_buttonTitle = Module_Lang_Helper::translate('payTotalCost');
                $this->_description = Module_Lang_Helper::translate('totalCost');
                break;

            case PAYMENT_SUBJECT_SERVICE:
                $this->_buttonTitle = Module_Lang_Helper::translate('payServices');
                $this->_formId .= 'Services';
                $this->_description = Module_Lang_Helper::translate('services');
                break;

            case PAYMENT_SUBJECT_RESIDENCE:
                $this->_buttonTitle = Module_Lang_Helper::translate('payResidence');
                $this->_formId .= 'Residence';
                $this->_description = Module_Lang_Helper::translate('residence');
                break;

            default:
                break;
        }        
        
        $this->_description .= " ({$this->prepayment})";
    }
    
    protected final function _setEndBtnFormParams($amount)
    {
        switch($this->prepaymentId)
        {
            case PREPAYMENT_TYPE_ALL:
                $this->_buttonTitle = Module_Lang_Helper::translate('payAllResidence');
                $this->_formId = 'pay_all_residence';
                $this->_description = Module_Lang_Helper::translate('allResidence');
                break;

            case PREPAYMENT_TYPE_FIRST:
                $this->_buttonTitle = Module_Lang_Helper::translate('payFirstNight');
                $this->_formId = 'pay_first_night';
                $this->_description = Module_Lang_Helper::translate('firstNight');
                break;
            
            default:
                throw new Exception("can't generate end btn: no prepayment id");
        }
        
        $this->_description .= " ({$this->prepayment})";
        $this->_buttonTitle .= " {$amount} {$this->currencyCode}";
    }
    
    protected function _setOnsubmitHtml($buttonOpt)
    {
        switch($buttonOpt) 
        {
            case PAYOPT_SAVE_BOOKING:
                $this->_onSubmitHtml = 'onsubmit="bookingform.saveBooking('.$this->bookingId.', this); return false;"';
                break;

            case PAYOPT_REMEMBER_ID:
                $this->_onSubmitHtml = 'onsubmit="bookingform.rememberBookingId('.$this->bookingId.', this); return false;"';
                break;
            
            default:
                $this->_onSubmitHtml = '';
                break;
        }
        
    }

    public function getPayBtn($subject, $amount, $buttonOpt = false)
    {        
        $this->_setBtnFormParams($subject);  
        $this->_setOnsubmitHtml($buttonOpt);
        $this->_setModel($amount, $subject);
        return $this->_drawButton();
    }

    public function getEndBookingBtn($amount)
    {
        $this->_setEndBtnFormParams($amount);
        $this->_setOnsubmitHtml(PAYOPT_SAVE_BOOKING);
        $this->_setModel($amount, PAYMENT_SUBJECT_RESIDENCE);
        return $this->_drawButton(true);
    }
            
    protected abstract function _setModel($amount, $subject);
    
    protected function _drawButton($end = false)
    {
        $fileName = $this->_getTemplateFile();
        $tpl = new Dante_Lib_Template();
        
        $tpl->formId = $this->_formId;
        $tpl->buttonTitle = $this->_buttonTitle;
        $tpl->onSubmitHtml = $this->_onSubmitHtml;
        $tpl->params = $this->_model;
        
        if($end) $tpl->withEndBtn = true;
        
        return $tpl->draw($fileName);
    }
    
    protected abstract function _getTemplateFile();
}

?>
