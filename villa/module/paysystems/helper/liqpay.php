<?php
/**
 * Description of liqpay
 *
 * @author Valkyria
 */
class Villa_Module_Paysystems_Helper_Liqpay extends Villa_Module_Paysystems_Helper_Base 
{
    protected $_urlPrefix = '/liqpay/';

    protected function _setModel($amount, $subject) 
    {
        $this->_model = new Module_Liqpay2_Model();
        $this->_model->public_key = Module_Liqpay2_Helper::getPublicKey();
        $this->_model->private_key = Module_Liqpay2_Helper::getPrivateKey();
        $this->_model->result_url = $this->_returnUrl;
        $this->_model->server_url = $this->_notifyUrl;
        $this->_model->currency = $this->currencyCode;
        $this->_model->amount = $amount;
        $this->_model->order_id = $this->bookingId;
        $this->_model->description = self::genDescription($this->_description, $subject, $this->uid);
        $this->_model->sandbox = Dante_Lib_Config::get('liqpay2.test_mode');
        $this->_model->language = self::getLang();
        $this->_model->data = Module_Liqpay2_Helper::data($this->_model);
        $this->_model->signature = Module_Liqpay2_Helper::signature($this->_model);
    }
        
    protected function _getTemplateFile()
    {
        return '../villa/module/paysystems/template/liqpay.html';
    }
    
    public static function genDescription($descrBase, $subject, $uid)
    {
        return "{$descrBase} [S:{$subject};U:{$uid}]";
    }
    
    public static function parseDescription($data)
    {
        $params = json_decode(base64_decode($data));        
        $descr = $params->description;
        
        $matches = array();
        $result = array();
        if(preg_match('/S\:(\d+)/', $descr, $matches))
        {
            $result['subject'] = $matches[1];
        }
        if(preg_match('/U\:(\d+)/', $descr, $matches))
        {
            $result['uid'] = $matches[1];
        }
        $result['descr'] = substr($descr, 0, strpos($descr, "["));
        
        return $result;
    }
    
    public static function updateDataField($currentData, $bookingId, $descr)
    {
        $dataObj = Module_Liqpay2_Helper::dataDecode($currentData);
        if($bookingId) $dataObj->order_id = Module_Liqpay2_Helper::uniqueValue($bookingId);
        if($descr) $dataObj->description = $descr;
        
        return Module_Liqpay2_Helper::dataEncode($dataObj);
    }
    
    public static function updateSignature($newData)
    {
        $model = new Module_Liqpay2_Model();
        $model->private_key = Module_Liqpay2_Helper::getPrivateKey();
        $model->data = $newData;
        return Module_Liqpay2_Helper::signature($model);
    }
    
    public static function getLang()
    {
        $liqpayLangs = array('ru', 'en');
        $langCode = strtolower(Module_Lang_Helper::getCurrentCode());
        
        if(in_array($langCode, $liqpayLangs)) return $langCode;
        if($langCode == 'ua' || $langCode == 'uk') return 'ru';
        
        return 'en';
    }
}

?>
