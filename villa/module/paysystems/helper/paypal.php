<?php
/**
 * Description of paypal
 *
 * @author Valkyria
 */
class Villa_Module_Paysystems_Helper_Paypal extends Villa_Module_Paysystems_Helper_Base
{   
    protected $_urlPrefix = '/paypal/';

    protected function _setModel($amount, $subject) 
    {
        $this->_model = new Module_Paypal_Model();
        $this->_model->business = Dante_Lib_Config::get('paypal.business');
        $this->_model->return_url = $this->_returnUrl;
        $this->_model->notify_url = $this->_notifyUrl;
        $this->_model->mc_currency = $this->currencyCode;
        $this->_model->mc_gross = $amount;
        $this->_model->item_number = $this->bookingId;
        $this->_model->item_name = $this->_description;
        
        $this->_model->custom = self::genCustomData($subject, $this->uid);
    }
    
    protected function _getTemplateFile()
    {
        return '../villa/module/paysystems/template/paypal.html';
    }

    public static function genCustomData($subject, $uid)
    {
        $customData = 's:'.$subject;        
        if($uid) $customData .= ';u:'.$uid;
        
        return $customData;
    }

    public static function parseCustomData($customData)
    {
        $matches = array();
        $result = array();
        if(preg_match('/s\:(\d+)/', $customData, $matches))
        {
            $result['subject'] = $matches[1];
        }
        if(preg_match('/u\:(\d+)/', $customData, $matches))
        {
            $result['uid'] = $matches[1];
        }
        
        return $result;
    }
}

?>
