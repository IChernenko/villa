villa_mail = {
    controller: 'villa.module.mail.manage.controller',
    changeLang: function(obj)
    {
        if(this.checkDisabled(obj)) return;
        
        villa_mail.disableBtn();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'ajax.php',
            data: {                
                controller: villa_mail.controller,
                action: 'drawForm',
                id: $('#typeId').val(), 
                lang_id: $(obj).val()
            },
            success: function(data){
                if(data.result == 1)
                    $('#modalWindow .modal-body').html(data.html);
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    },
    preview: function(obj)
    {
        if(this.checkDisabled(obj)) return;
        
        villa_mail.disableBtn();
        
        var data = $('#editMailForm').serializeArray();
        data.push(
            {name: 'controller', value: villa_mail.controller}, 
            {name: 'action', value: 'preview'}
        );
        
        $('#previewModal').arcticmodal({
            type: 'ajax',
            url: 'ajax.php',
            ajax: {
                type: 'POST',
                dataType: 'JSON',
                data: data,
                success: function(data, el, response) 
                {
                    data.body.html(el);                    
                    $(el).find('.modal-body').html(response.html);
                }
            },
            afterClose: function()
            {
                 $('#secondModal').attr('style', '').find('.modal-body').html('');
                 villa_mail.enableBtn();
            }
        });
    },
    save: function(obj)
    {
        if(this.checkDisabled(obj)) return;
        
        villa_mail.disableBtn();
        
        var data = $('#editMailForm').serializeArray();
        data.push(
            {name: 'controller', value: villa_mail.controller}, 
            {name: 'action', value: 'edit'}
        );
            
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'ajax.php',
            data: data,
            success: function(data)
            {
                villa_mail.enableBtn();
                if(data.result == 1)
                    jAlert('Шаблон сохранен', 'Сообщение', function(){
                        $('#modalWindow .modal-body').html(data.html);
                    });
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
        
    },
    disableBtn: function()
    {
        $('a.btn, select').addClass('disabled');
    },    
    enableBtn: function()
    {
        $('a.btn, select').removeClass('disabled');
    },
    checkDisabled: function(obj)
    {
        if($(obj).hasClass('disabled')) return true;
        else return false;
    }   
}


