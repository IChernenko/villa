<?php
/**
 * Description of client
 *
 * @author Valkyria
 */
class Villa_Module_Mail_View_Client extends Villa_Module_Mail_View_Base
{
    public function draw($mailParts, $userData)
    {
        $tpl = new Dante_Lib_Template();
        
        $tpl->parts = $mailParts;
        $tpl->data = $userData;
        $tpl->link = $this->_getDomainLink();

        return $tpl->draw($this->_templateName);
    }
}

?>
