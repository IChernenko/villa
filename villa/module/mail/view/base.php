<?php
/**
 * Description of base
 *
 * @author Valkyria
 */
class Villa_Module_Mail_View_Base 
{    
    protected $_templateName;
    
    public function setTemplate($fileName)
    {
        $this->_templateName = $fileName;
    }   
    
    protected function _getDomainLink()
    {
        return '<a href="'.Dante_Helper_App::getAppDomain().'">Villa LaScala</a>';
    }
}

?>
