<?php

/**
 * Description of manage
 *
 * @author Valkyria
 */
class Villa_Module_Mail_View_Manage extends Villa_Module_Mail_View_Base
{    
    protected $_fileNameMain = '../villa/module/mail/manage/template/form.html';
    
    public $subject2Enabled = false;

    public $langList;
    
    public function draw(Villa_Module_Mail_Model $model)
    {
        $tpl = new Dante_Lib_Template();
        
        $tpl->langListHtml = $this->_getLangListHtml($model->lang_id);
        $tpl->id = $model->id;
        $tpl->type = $model->type_id;
        $tpl->subject = $model->subject;
        $tpl->subject_2_enabled = $this->subject2Enabled;
        $tpl->subject_2 = $model->subject_2;
        $tpl->credentials = $model->credentials;
//        $model->parts['credentials'] = 'tsr';
        $tpl->custom = $this->_drawCustomTemplateForm($model->parts);




//        ob_start();
//        var_dump($model->parts);
//        $result = ob_get_clean();




        return $tpl->draw($this->_fileNameMain);
    }
    
    protected function _drawCustomTemplateForm($parts)
    {        
        $tpl = new Dante_Lib_Template();
        $tpl->parts = $parts;
        $tpl->link = $this->_getDomainLink();
        
        return $tpl->draw($this->_templateName);
    }
    
    protected function _getLangListHtml($curLang)
    {
        $html = '<select id="langId" name="lang_id" onchange="villa_mail.changeLang(this)">';
        
        foreach($this->langList as $id => $lang)
        {
            $selected = $id == $curLang ? 'selected' : '';
            $html .= '<option value="'.$id.'" '.$selected.'>'.$lang->name.'</option>';
        }
        
        $html .= '</select>';
        
        return $html;
    }
}

?>
