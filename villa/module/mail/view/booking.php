<?php
/**
 * Description of booking
 *
 * @author Valkyria
 */
class Villa_Module_Mail_View_Booking extends Villa_Module_Mail_View_Base
{
    public $mode;

    public function draw($mailParts, $userData)
    {
        $tpl = new Dante_Lib_Template();
        //print_r($userData['contacts']);
        $tpl->parts = $mailParts;
        $tpl->model = $userData['booking'];
        $tpl->room_type_name = $userData['room_type_name'];
        $tpl->prepayment_name = $userData['prepayment_name'];
        $tpl->ccy = $userData['ccy'];
        $tpl->services = $userData['services'];
        $tpl->excursions = $userData['excursions'];
        $tpl->transfers = $userData['transfers'];
        $country = '';
        if (isset($userData['contacts']->country)) {
            $country = $userData['contacts']->country;
        }

        $city = '';
        if (isset($userData['contacts']->city)) {
            $city = $userData['contacts']->city;
        }

        $address = '';
        if (isset($userData['contacts']->address)) {
            $address = $userData['contacts']->address;
        }

        $tpl->contacts = $country.', '.$city.',<br/>'.$address;

        // если у нас турагент проставляем контакты димы
        if ($this->mode == BOOKING_MAIL_MODE_TOURAGENT) {
            $tpl->contacts = $userData['contacts']->contacts;
        }


        $tpl->phone = $userData['phone'];   
        $tpl->info = $userData['info'];
        
        $tpl->domain = Dante_Helper_App::getAppDomain();
                        
        $tpl->user_info = $this->_buildUserInfo($userData['user_info']);
        $newUser = (boolean)$userData['user_info']->password;
        
        if($newUser)
        {
            $tpl->new_user = 1;
            $tpl->user_login = $userData['user_info']->email;
            $tpl->user_password = $userData['user_info']->password;
        }

        return $tpl->draw($this->_templateName);
    }
    
    protected function _buildUserInfo($userModel)
    {    
        $userInfo = '';
        if($userModel->group_id == Villa_Module_User_Helper::getTourAgentGroupId())
        {
            $userInfo = '';
            $textWidth = '100%';
            if($userModel->logo)
            {
                $src = Dante_Helper_App::getAppDomain().'/'.Villa_Module_User_Helper::getTouragentLogoDir().$userModel->logo;
                $userInfo .= '<img style="width: 15%; float: left; '.
                        'margin-right: 3%;" src="'.$src.'">';
                $textWidth = '78%';
            }
            
            $userInfo .= '<p style="float: left; margin: 0; overflow: hidden; width: '.$textWidth.';">'.$userModel->requisites.'<br/>'.
                    $userModel->first_name.' '.$userModel->last_name.
                    '<br/><a style="color: inherit; text-decoration: none;">'.$userModel->phone.'</a></p>';
        }
//        else
//        {
//            $userInfo = $userModel->first_name.' '.$userModel->last_name.'<br/>'.
//                        '<a style="color: inherit; text-decoration: none;">'.$userModel->email.'</a><br/>'.
//                        '<a style="color: inherit; text-decoration: none;">'.$userModel->phone.'</a>';
//        }
        
        return $userInfo;
    }
}

?>
