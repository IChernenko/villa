<?php
/**
 * Description of mapper
 *
 * @author Valkyria
 */
class Villa_Module_Mail_Mapper 
{
    protected $_tableName = '[%%]villa_mail';    
    protected $_tableParts = '[%%]villa_mail_parts';
    protected $_tableTypes = '[%%]villa_mail_types';
    
    public function get($type, $lang)
    {
        $model = new Villa_Module_Mail_Model();
        
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->select(array('type_id' => $type, 'lang_id' => $lang));
        
        $model->id = $table->id;
        $model->type_id = $type;
        $model->lang_id = $lang;
        $model->subject = $table->subject;
        $model->subject_2 = $table->subject_2;
        $model->credentials = $table->credentials;

        if(!$model->id) return $model;
        
        $tableParts = new Dante_Lib_Orm_Table($this->_tableParts);
        $tableParts->getByAttribute(array('mail_id' => $model->id))->open();
        
        while($tableParts->fetchRecord())
        {
            $model->parts[$tableParts->part_id] = $tableParts->text;
        }
        
        return $model;
    }
}

?>
