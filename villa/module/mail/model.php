<?php
/**
 * Description of model
 *
 * @author Valkyria
 */
class Villa_Module_Mail_Model 
{
    public $id;
    
    public $lang_id;
    
    public $type_id;
    
    public $subject;
    
    public $subject_2;

    public $credentials;

    public $parts = array();
    
    public $message;
    
    public function removeEmptyParts()
    {
        if(!is_array($this->parts)) return;
        
        foreach($this->parts as $id => $part)
        {
            if(!$part) unset($this->parts[$id]);
        }
    }
    
    public function fillEmptyParts($partsCondition)
    {
        if(is_int($partsCondition))
        {
            for($i = 1; $i <= $partsCondition; $i++)
            {
                $this->fillPart($i);
            }
        }
        elseif(is_array($partsCondition))
        {
            foreach($partsCondition as $index)
            {
                $this->fillPart($index);
            }
        }
        else
            throw new Exception('mail model: wrong part condition');
    }
    
    private function fillPart($index)
    {
        if(!isset($this->parts[$index])) $this->parts[$index] = "";
    }
}

?>
