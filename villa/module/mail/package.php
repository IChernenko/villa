<?php

global $package;
$package = array(
    'version' => '5',
    'name' => 'villa.module.mail',
    'dependence' => array(),
    'js' => array(
        '../villa/module/mail/js/villa_mail.js',
    ),
    'css' => array(
        '../villa/module/mail/css/manage.css',
    )
);

?>
