CREATE TABLE `[%%]villa_mail_types` (
    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(50),
    PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT = 1;

INSERT INTO `[%%]villa_mail_types`(`id`, `name`)
    VALUES (1, 'Регистрация' ), (2, 'Бронирование'), (3, 'Ответ на вопрос'), (4, 'Ответ на отзыв');

CREATE TABLE `[%%]villa_mail` (
    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `type_id` INT(11) UNSIGNED NOT NULL,
    `lang_id` INT(11) UNSIGNED NOT NULL,
    `subject` VARCHAR(100) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_villa_mails_type_id`(`type_id`),
    CONSTRAINT `fk_villa_mail_type_id` FOREIGN KEY (`type_id`) REFERENCES `[%%]villa_mail_types`(`id`) 
        ON DELETE CASCADE ON UPDATE CASCADE,    
    KEY `fk_villa_mails_lang_id`(`lang_id`),
    CONSTRAINT `fk_villa_mail_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `[%%]languages`(`id`) 
        ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `[%%]villa_mail_parts` (
    `mail_id` INT(11) UNSIGNED NOT NULL,
    `part_id` INT(11) UNSIGNED NOT NULL DEFAULT 1,
    `text` VARCHAR(200),
    KEY `fk_villa_mail_parts_mail_id`(`mail_id`),
    CONSTRAINT `fk_villa_mail_parts_mail_id` FOREIGN KEY (`mail_id`) REFERENCES `[%%]villa_mail`(`id`) 
        ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `[%%]villa_mail`(`id`, `type_id`, `lang_id`, `subject`)
    VALUES 
(1, 1, 1, 'Регистрация'), 
(2, 2, 1, 'Бронирование'), 
(3, 3, 1, 'Ответ на вопрос'), 
(4, 4, 1, 'Ответ на отзыв');

INSERT INTO `[%%]villa_mail_parts`(`mail_id`, `part_id`, `text`)
    VALUES 

(1, 1, 'Вы успешно зарегистрированы на сайте'), 
(1, 2, 'Данные для входа в Ваш профиль'), 
(1, 3, 'Email'), 
(1, 4, 'Пароль'), 

(2, 1, 'Поздравляем!'), 
(2, 2, 'Вы оформили бронирование на сайте отеля'), 
(2, 3, 'Номер Вашей брони'), 
(2, 4, 'Подробности Вы можете просмотреть в Вашем профиле'), 
(2, 5, 'Данные для входа'), 
(2, 6, 'Email'), 
(2, 7, 'Пароль'), 

(3, 1, 'Вы задали вопрос на сайте'), 
(3, 2, 'Текст вопроса'), 
(3, 3, 'Ответ на Ваш вопрос'), 

(4, 1, 'Вы оставили отзыв на сайте'),
(4, 2, 'Текст отзыва'),
(4, 3, 'Ответ на Ваш отзыв');
