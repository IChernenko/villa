<?php

$sql = 'DELETE FROM `[%%]villa_mail_parts` WHERE `mail_id` = 2;';
Dante_Lib_SQL_DB::get_instance()->exec($sql);

$sql = 'INSERT INTO `[%%]villa_mail_parts` (`mail_id`, `part_id`, `text`) VALUES ';

$parts = array(
    'Бронирование №', 
    'Дата заезда', 
    'Дата отъезда', 
    'Количество ночей', 
    'Взрослых',
    'Детей', 
    'Стоимость за ночь',
    'Общая стоимость', 
    'Данные гостей',
    'Имя', 
    'Фамилия',
    'Телефон', 
    'E-mail',
    'Услуги', 
    'Наименование',
    'Цена для взрослых', 
    'Кол-во',
    'Цена для детей', 
    'Кол-во дней',
    'Сумма', 
    'Общая стоимость проживания и услуг',
    'Стоимость', 
    'Скидка',
    'К оплате',
    'Оплачено', 
    'Остаток к оплате',
    'Данные для входа в Ваш профиль', 
    'Email', 
    'Пароль'
);

foreach($parts as $k => $v)
{
    $partId = $k+1;
    $sql .= "(2, {$partId}, '{$v}')";
    
    if($partId < count($parts)) $sql .= ', ';
}

Dante_Lib_SQL_DB::get_instance()->exec($sql);

?>
