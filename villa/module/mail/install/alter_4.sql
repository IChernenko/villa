ALTER TABLE `[%%]villa_mail_parts` MODIFY `part_id` VARCHAR(50) NOT NULL;

ALTER TABLE `[%%]villa_mail` ADD COLUMN `subject_2` VARCHAR(100) DEFAULT NULL;
UPDATE `[%%]villa_mail` SET `subject_2` = 'Изменение бронирования' WHERE `id` = 2;