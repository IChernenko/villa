<?php

$sql = 'DELETE FROM `[%%]villa_mail_parts` WHERE `mail_id` = 2;';
Dante_Lib_SQL_DB::get_instance()->exec($sql);

$sql = 'INSERT INTO `[%%]villa_mail_parts` (`mail_id`, `part_id`, `text`) VALUES ';

$parts = array(
    'bookingNum' => 'Бронирование №',
    'arriveDate' => 'Дата заезда',
    'departureDate' => 'Дата отъезда',
    'numOfNights' => 'Количество ночей',
    'adults' => 'Взрослых',
    'children' => 'Детей',
    'costOneNight' => 'Стоимость за ночь',
    'totalCost' => 'Общая стоимость',
    'guestData' => 'Данные гостей',
    'name' => 'Имя',
    'surname' => 'Фамилия',
    'phone' => 'Телефон',
    'email' => 'E-mail',
    'services' => 'Услуги',
    'denomination' => 'Наименование',
    'adultPrice' => 'Цена для взрослых',
    'quantity' => 'Кол-во',
    'childrenPrice' => 'Цена для детей',
    'numDays' => 'Кол-во дней',
    'sum' => 'Сумма',
    'excursion' => 'Экскурсия',
    'transfer' => 'Трансфер',
    'transferType' => 'Вид трансфера',
    'dateAndTime' => 'Дата и время',
    'raceOrTrainNum' => 'Рейс / Поезд и вагон',
    'city' => 'Город',
    'race' => 'Рейс',
    'train' => 'Поезд',
    'wagon' => 'Вагон',
    'totalResidServCost' => 'Общая стоимость проживания и услуг',
    'cost' => 'Стоимость',
    'discount' => 'Скидка',
    'payable' => 'К оплате',
    'paid' => 'Оплачено',
    'leftPayable' => 'Остаток к оплате',
    'loginData' => 'Данные для входа в Ваш профиль',
    'password' => 'Пароль'
);

foreach($parts as $partId => $v)
{
    $sql .= "(2, '{$partId}', '{$v}'), ";
}

$sql = rtrim($sql, ", ");

Dante_Lib_SQL_DB::get_instance()->exec($sql);

?>
