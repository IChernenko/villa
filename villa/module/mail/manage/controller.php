<?php

/**
 * Description of controller
 *
 * @author Valkyria
 */
class Villa_Module_Mail_Manage_Controller extends Module_Handbooksgenerator_Controller_Base
{
    protected function _instanceMapper()
    {
        return new Villa_Module_Mail_Manage_Mapper();
    }

    protected function _drawAction()
    {
        $list = $this->_instanceMapper()->getMailTypesList();
        
        $this->formEnable('villa.module.mail.manage.controller');
        $this->tableParams['style'] = array(
            'width' => '455px'
        );
        
        $this->tableParams['class'] = array('table', 'handbook', 'table-bordered', 'table-hover');
        
        $this->caption = 'Шаблоны писем';
        
        $this->thead = array(
            array(
                'Тип письма',
                array(
                    'content' => 'Функции',
                    'params' => array(
                        'style' => array(
                            'width' => '125px'
                        )
                    )
                )
            )
        );
        
        foreach($list as $row)
        {
            $this->tbody[] = array(
                'type' => $row['name'],
                'buttons' => '<a class="btn btn-info" onclick="handbooks.edit('.$row['id'].')">Редактировать</a>'
            );
        }
        
        $this->modal = true;
        
        return $this->generate();
    }
    
    protected function _drawFormAction($model = false)
    {
        if(!$model)
        {
            $lang = $this->_getRequest()->get_validate('lang_id', 'int', 1);
            $type = $this->_getRequest()->get('id');

            $model = $this->_instanceMapper()->get($type, $lang);
        }
        
        $builder = Villa_Module_Mail_Builder_Factory::create($model->type_id);
        $builder->setModel($model);
        $form = $builder->getEditForm();
        
        return array(
            'result' => 1,
            'html' => $form
        );
    }
    
    protected function _previewAction()
    {
        $model = $this->_populateModel();
        
        $builder = Villa_Module_Mail_Builder_Factory::create($model->type_id);
        $builder->setModel($model);
        $builder->setPreviewData();

        $model->parts['credentials'] = $builder->_model->credentials;
        $model = $builder->buildMessage();
        
        return array(
            'result' => 1,
            'html' => $model->message
        );
    }

    protected function _editAction()
    {
        $model = $this->_populateModel();
        $model->removeEmptyParts();
        
        $model->id = $this->_instanceMapper()->apply($model);
        
        return $this->_drawFormAction($model);
    }

    protected function _populateModel()
    {
        $request = $this->_getRequest();
        
        $model = new Villa_Module_Mail_Model();
        $model->id = $request->get('id');
        $model->type_id = $request->get('type_id');
        $model->lang_id = $request->get('lang_id');
        $model->subject = $request->get('subject');
        $model->credentials = $request->get('credentials');
        $model->parts = $request->get('parts');
        
        return $model;
    }

    protected function _testAction()
    {
//        $tpl = new Dante_Lib_Template();
//        $fileName = '../villa/logs/mail_1430915613_test@test.bbb.html';
//        
//        return $tpl->draw($fileName);
        
        $model = $this->_instanceMapper()->get(MAIL_TYPE_BOOKING, 1);
        
        $builder = Villa_Module_Mail_Builder_Factory::create(MAIL_TYPE_BOOKING);
        $builder->setModel($model);
        $builder->setPreviewData();
        
        $model = $builder->buildMessage();
        
        return $model->message;
        
    }
}

?>
