<?php
/**
 * Description of mapper
 *
 * @author Valkyria
 */
class Villa_Module_Mail_Manage_Mapper extends Villa_Module_Mail_Mapper
{
    public function getMailTypesList()
    {
        $table = new Dante_Lib_Orm_Table($this->_tableTypes);
        $table->select_array('WHERE 1');
        return $table->getFields();
    }

    public function apply(Villa_Module_Mail_Model $model)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        
        $table->type_id = $model->type_id;
        $table->lang_id = $model->lang_id;
        $table->subject = $model->subject;
        $table->credentials = $model->credentials;

        if($model->id)
        {
            $table->update(array('id' => $model->id));
            
            $tableParts = new Dante_Lib_Orm_Table($this->_tableParts);
            $tableParts->delete(array('mail_id' => $model->id));
        }
        else 
            $model->id = $table->insert();
        
        if(!is_array($model->parts)) return $model->id;
        
        foreach($model->parts as $id => $part)
        {
            $tableParts = new Dante_Lib_Orm_Table($this->_tableParts);
            $tableParts->mail_id = $model->id;
            $tableParts->part_id = $id;
            $tableParts->text = $part;
            
            $tableParts->insert();
        }
        
        return $model->id;
    }
}

?>
