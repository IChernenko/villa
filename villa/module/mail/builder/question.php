<?php
/**
 * Description of question
 *
 * @author Valkyria
 */
class Villa_Module_Mail_Builder_Question extends Villa_Module_Mail_Builder_Base
{
    protected function _setMailType() 
    {
        $this->_mailType = MAIL_TYPE_QUESTION;
    }
    
    protected function _setTemplates() 
    {
        parent::_setTemplates();
        $this->_viewTemplate .= 'question.html';
        $this->_viewTemplateManage .= 'question.html';
    }
    
    protected function _setPartsCondition() 
    {
        $this->_partsCondition = 3;
    }
    
    protected function _setKeys() 
    {
        $this->_dataKeys = array(
            'date',
            'question',
            'answer'
        );
    }
}

?>
