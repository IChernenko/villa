<?php
/**
 * Description of registration
 *
 * @author Valkyria
 */
class Villa_Module_Mail_Builder_Registration extends Villa_Module_Mail_Builder_Base
{
    protected function _setMailType() 
    {
        $this->_mailType = MAIL_TYPE_REGISTRATION;
    }
    
    protected function _setTemplates() 
    {
        parent::_setTemplates();
        $this->_viewTemplate .= 'reqistration.html';
        $this->_viewTemplateManage .= 'reqistration.html';
    }
    
    protected function _setPartsCondition() 
    {
        $this->_partsCondition = 4;
    }
    
    protected function _setKeys() 
    {
        $this->_dataKeys = array(
            'email',
            'password'
        );
    }
}

?>
