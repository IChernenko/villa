<?php
/**
 * Description of base
 *
 * @author Valkyria
 */
abstract class Villa_Module_Mail_Builder_Base 
{
    /**
     * Вид письма
     * @var int 
     */
    protected $_mailType;
    
    /**
     * Язык
     * @var type 
     */
    protected $_lang;
    /**
     * Модель, полученная из базы
     * @var Villa_Module_Mail_Model 
     */
    public $_model;
    
    /**
     * Флаг возможности использования доп. темы (не в конкретном случае, а в рамках объекта)
     * @var boolean 
     */
    protected $_subject2Available;
    /**
     * Флаг использования дополнительной темы вместо основной
     * @var boolean 
     */
    protected $_useSubject2;
    /**
     * Переменная класса отображения
     * @var Villa_Module_Mail_View_Manage || Villa_Module_Mail_View_Client
     */
    protected $_view;
        
    /**
     * Шаблон для переменной вида
     * @var string 
     */
    protected $_viewTemplate;
    
    /**
     * Шаблон для переменной вида (админка)
     * @var string 
     */
    protected $_viewTemplateManage;
        
    /**
     * Данные пользователя для вставки в письмо
     * @var array 
     */
    protected $_mailData;

    public $credentials;
        
    /**
     * Нужное количество частей или ключи частей (для писем с большим кол-вом частей и возможными повторами)
     * @var int | array
     */
    protected $_partsCondition;
    
    /**
     * Ключи для валидации данных для вставки
     * @var array 
     */
    protected $_dataKeys;

    public $mode;

    function __construct() {
        $this->_init();
    }
    
    protected function _init()
    {
        $this->_setMailType();
        $this->_setPartsCondition();
        $this->_setKeys();
        $this->_setTemplates();
        $this->setLang(Module_Lang_Helper::getCurrent());
        $this->_setSubject2Availability();
        $this->setSubject2Usage();
    }
    
    /**
     * Установка переменной ключей 
     */
    protected abstract function _setKeys();
    
    /**
     * Установка кол-ва частей 
     */
    protected abstract function _setPartsCondition();
    
    /**
     * Установка типа письма 
     */
    protected abstract function _setMailType();

    /**
     * Установка возможности использования доп. темы (по умолчанию - false) 
     */
    protected function _setSubject2Availability()
    {
        $this->_subject2Available = false;
    }

    /**
     * Установка флага использования доп. темы 
     */
    public function setSubject2Usage($usage = false)
    {
        if(!$this->_subject2Available) 
            $this->_useSubject2 = false;
        else
            $this->_useSubject2 = $usage;
    }

    /**
     * Установка шаблона для формы редактирования 
     */
    protected function _setTemplates()
    {
        $this->_viewTemplate = '../villa/module/mail/template/';  
        $this->_viewTemplateManage = '../villa/module/mail/manage/template/';  
    }
    
    protected function _instanceClientView()
    {
        return new Villa_Module_Mail_View_Client();
    }
    
    protected function _instanceManageView()
    {
        return new Villa_Module_Mail_View_Manage();
    }

    /**
     * Установка переменной вида 
     */
    protected final function _setManageView()
    {
        $this->_view = $this->_instanceManageView();
        $this->_view->setTemplate($this->_viewTemplateManage);
    }
    
    protected final function _setClientView()
    {
        $this->_view = $this->_instanceClientView();
        //echo($this->_viewTemplate."\n");
        $this->_view->setTemplate($this->_viewTemplate);
    }
    
    public final function setLang($lang)
    {
        $this->_lang = $lang;
    }
    
    /**
     * Установка данных для вставки
     * @param type $data 
     */
    public final function setData($data)
    {
        $this->_validate($data);
        $this->_mailData = $data;
    }

    /**
     * Установка модели письма
     * @param Villa_Module_Mail_Model $model 
     */
    public final function setModel(Villa_Module_Mail_Model $model = null)
    {
        if(!$model) $model = $this->_instanceMapper()->get($this->_mailType, $this->_lang);
        
        $this->_model = $model;
        $this->_model->fillEmptyParts($this->_partsCondition);
    }
    
    protected function _instanceMapper()
    {
        return new Villa_Module_Mail_Mapper();
    }
    
    /**
     * Возвращает готовую форму для редактирования
     * @return string 
     */
    public final function getEditForm()
    {
        if(!isset($this->_model))
            throw new Exception('cant draw edit form: model is missing');
        
        $this->_setManageView();
        $this->_view->langList = $this->_getLangList();
        $this->_view->subject2Enabled = $this->_subject2Available;
        
        return $this->_view->draw($this->_model);
    }
    
    /**
     * Генерирует сообщение для отправки
     * @return Villa_Module_Mail_Model 
     */
    public final function buildMessage()
    {
        if(!isset($this->_model))
            throw new Exception('cant build message: model is missing');
        
        if(!isset($this->_mailData))
            throw new Exception('cant build message: data is missing');
        
        $this->_setClientView();
        
        // Создаем тему
        if($this->_useSubject2)
            $this->_model->subject = $this->_createSubject($this->_model->subject_2);
        else
            $this->_model->subject = $this->_createSubject($this->_model->subject);

        $this->_view->mode = $this->mode;
        $this->_model->parts['credentials'] = $this->_model->credentials;

        // Создаем сообщение
//        ob_start();
//        var_dump($this->_model);
//        $result = ob_get_clean();
        $this->_model->message = $this->_view->draw($this->_model->parts, $this->_mailData);
        
        return $this->_model;
    }
    
    /**
     * Функция модификации темы (если в ней должны присутствовать данные пользователя)
     * По умолчанию тема остается без изменений
     * @param string $subjectBase - тема, полученная из базы
     * @return string 
     */
    protected function _createSubject($subjectBase)
    {
        return $subjectBase;
    }

    /**
     * Валидация данных
     * @param type $data
     * @throws Exception 
     */
    protected final function _validate($data)
    {        
        foreach($this->_dataKeys as $key)
        {
            if(!array_key_exists($key, $data))
                throw new Exception('cant build mail: key "'.$key.'" is missing');
        }
    }
    
    protected final function _getLangList()
    {
        $langMapper = new Module_Lang_Mapper();
        return $langMapper->getList();
    }
    
    public function setPreviewData()
    {
        foreach($this->_dataKeys as $key)
        {
            $this->_mailData[$key] = '<span style="color: red">X</span>';
        }
    }
}

?>
