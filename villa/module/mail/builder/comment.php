<?php
/**
 * Description of comment
 *
 * @author Valkyria
 */
class Villa_Module_Mail_Builder_Comment extends Villa_Module_Mail_Builder_Base
{
    protected function _setMailType() 
    {
        $this->_mailType = MAIL_TYPE_COMMENT;
    }
    
    protected function _setTemplates() 
    {
        parent::_setTemplates();
        $this->_viewTemplate .= 'comment.html';
        $this->_viewTemplateManage .= 'comment.html';
    }
    
    protected function _setPartsCondition() 
    {
        $this->_partsCondition = 3;
    }
    
    protected function _setKeys() 
    {
        $this->_dataKeys = array(
            'date',
            'comment',
            'answer'
        );
    }
}

?>
