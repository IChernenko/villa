<?php

/**
 * Description of factory
 *
 * @author Valkyria
 */
class Villa_Module_Mail_Builder_Factory 
{
    /**
     * Фабрика построителей шаблонов письма
     * @param int $type
     * @return \Villa_Module_Mail_Builder_Base 
     */
    public static function create($type)
    {
        $builder = null;
        switch($type)
        {
            case MAIL_TYPE_REGISTRATION:
                $builder = new Villa_Module_Mail_Builder_Registration();
                break;
            
            case MAIL_TYPE_BOOKING:
                $builder = new Villa_Module_Mail_Builder_Booking();
                break;
            
            case MAIL_TYPE_QUESTION:
                $builder = new Villa_Module_Mail_Builder_Question();
                break;
            
            case MAIL_TYPE_COMMENT:
                $builder = new Villa_Module_Mail_Builder_Comment();
                break;
        }
        
        return $builder;
    }
}

?>
