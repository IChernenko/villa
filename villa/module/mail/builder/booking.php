<?php
/**
 * Description of booking
 *
 * @author Valkyria
 */
class Villa_Module_Mail_Builder_Booking extends Villa_Module_Mail_Builder_Base
{
    protected function _instanceClientView() 
    {
        return new Villa_Module_Mail_View_Booking();
    }
    
    protected function _setSubject2Availability() 
    {
        $this->_subject2Available = true;
    }

    protected function _setMailType() 
    {
        $this->_mailType = MAIL_TYPE_BOOKING;
    }
    
    protected function _setTemplates() 
    {
        parent::_setTemplates();
        $this->_viewTemplate .= 'booking.html';
        $this->_viewTemplateManage .= 'booking.html';
    }
    
    protected function _setPartsCondition() 
    {
        $this->_partsCondition = array(
            'bookingNum', 
            'arriveDate', 
            'departureDate', 
            'numOfNights', 
            'adults',
            'children', 
            'costOneNight',
            'totalCost', 
            'guestData',
            'name', 
            'surname',
            'phone', 
            'email',
            'services', 
            'denomination',
            'adultPrice', 
            'quantity',
            'childrenPrice', 
            'numDays',
            'sum', 
            'excursion',
            'transfer',
            'transferType',
            'dateAndTime',
            'raceOrTrainNum',
            'city',
            'race',
            'train',
            'wagon',
            'totalResidServCost',
            'cost', 
            'discount',
            'payable',
            'paid', 
            'leftPayable',
            'loginData', 
            'password',
            'credentials'
        );
    }
    
    protected function _setKeys() 
    {
        $this->_dataKeys = array(
            'booking',
            'user_info',
            'room_type_name',
            'prepayment_name',
            'ccy',
            'services',
            'excursions',
            'transfers',
            'contacts',
            'phone',
            'info'
        );
    }
    
    protected function _createSubject($subjectBase) 
    {
        $subject = parent::_createSubject($subjectBase);
        $bookingId = $this->_mailData['booking']->id;
        return $subject.' #'.$bookingId;
    }


    public function setPreviewData() 
    {
        parent::setPreviewData();
                  
        $optionsMapper = new Villa_Module_Hotel_Mapper_Bookingoptions();
        $this->_mailData['services'] = $optionsMapper->getServices();
        $this->_mailData['transfers'] = $optionsMapper->getTransfers();
        
        $user = new Villa_Module_User_Model();
        $user->first_name = 'Иван';
        $user->last_name = 'Петров';
        $user->email = 'ipetrov@mail.ua';
        $user->password = '123456';
        
        $model = $this->_getPreviewBookingModel();
        $this->_mailData['booking'] = $model;        
        
        $this->_mailData['excursions'] = Villa_Module_Hotel_Helper_Excursion::getListForPeriod(
                $model->basicData->arrivalToInt(), $model->days + 1, 0
            );
        $this->_mailData['ccy'] = 'UAH';
        
        $contactsMapper = new Villa_Module_Contacts_Mapper();
        $this->_mailData['contacts'] = $contactsMapper->get(Module_Lang_Helper::getCurrent());
        $this->_mailData['phone'] = Villa_Module_Countries_Helper::getPhone();
        $this->_mailData['user_info'] = $user;
        $bookingInfoMapper = new Villa_Module_Bookinginfo_Mapper();
        $this->_mailData['info'] = $bookingInfoMapper->get(Module_Lang_Helper::getCurrent());
    }
    
    private function _getPreviewBookingModel()
    {        
        $model = new Villa_Module_Hotel_Model_Manage_Booking();
        $model->id = 123;
        $model->basicData->arrival = '12/12/1970';
        $model->basicData->departure = '24/12/1970';
        $model->basicData->adults = 1;
        $model->basicData->children = 1;
        $model->days = 12;
        
        $model->roomData->type_id = 3;
        $model->roomData->prepayment = 1;
        $model->roomData->max_adults = 1;
        $model->roomData->max_children = 1;
        $model->roomData->setPrice(100);
        $model->init();      
        
        $adultModel = new Villa_Module_Hotel_Model_Booking_Adult();
        $adultModel->enabled = 1;
        $adultModel->first_name = 'Иван';
        $adultModel->last_name = 'Петров';
        $adultModel->phone = '713491549';
        $adultModel->email = 'ipetrov@mail.ua';                    
        $model->adultsInfo[1] = $adultModel;   
        
        $childModel = new Villa_Module_Hotel_Model_Booking_Child();
        $childModel->enabled = 1;
        $childModel->first_name = 'Василий';
        $childModel->last_name = 'Петров';
        $model->childrenInfo[1] = $childModel;
          
        $sampleServiceId = current(array_keys($this->_mailData['services']));
        $service = array('guest_type' => GUEST_TYPE_ADULT, 'guest_num' => 1, 'count' => 3, 'cost' => 12);
        $service = array('guest_type' => GUEST_TYPE_CHILD, 'guest_num' => 1, 'count' => 3, 'cost' => 12);
        $model->addService($sampleServiceId, $service, false);
        
        $excursion = array('guest_type' => GUEST_TYPE_ADULT, 'guest_num' => 1, 'cost' => 10);
        $model->addExcursion(mktime(0, 0, 0, 12, 15, 1970), $excursion, false);
       
        $sampleTransfer = current($this->_mailData['transfers']);
        switch ($sampleTransfer['type']) 
        {
            case TRANSFER_TYPE_ARRIVAL:
                $transferDate = $model->basicData->arrival;
                break;

            case TRANSFER_TYPE_DEPARTURE:
                $transferDate = $model->basicData->departure;
                break;
        }
        $transfer = array('transfer_id' => $sampleTransfer['id'], 'date' => $transferDate, 'time' => '12:35', 'location' => 'London');
        $model->addTransfer($sampleTransfer['type'], $transfer, false);

        $model->basicData->dateType = 0;
        $model->totalsData->setDiscountsByValues(10, 10);
        $model->totalsData->addResidencePayment(10);
        $model->totalsData->recalc();
        
        return $model;
    }
}

?>
