<?php
/**
 * Description of service
 *
 * @author Valkyria
 */
class Villa_Module_User_Service 
{
    /**
     * Регистрируем пользователя после создания бронирования.
     * Если юзер залогинен или уже существует просто достаем его данные
     * @param Villa_Module_Hotel_Model_Booking_Adult $guest
     * @param boolean $returnUser - если true возврат модели юзера после преобразования (без проверки, регистрации и т.д.)
     * @return \Villa_Module_User_Model 
     */
    public static function guestToUser(Villa_Module_Hotel_Model_Booking_Adult $guest, $returnUser = false)
    {        
        $userMapper = new Villa_Module_User_Manage_Mapper();
        
        $user = new Villa_Module_User_Model();
        // Делаем из модели гостя модель юзера
        $user->first_name = $guest->first_name;
        $user->last_name = $guest->last_name;
        $user->email = $guest->email;
        $user->phone = $guest->phone;
        $user->rating = 1;
        
        // Если нам нужно только вернуть модель юзера - делаем
        if($returnUser) return $user;
        
        //Смотрим, нет ли у нас такого юзера. Если есть - возвращаем модель
        $uid = Dante_Helper_App::getUid();        
        if(!$uid) $uid = $userMapper->emailExists($user->email);
        if($uid)
        {
            $user->id = $uid;
            return $user;
        }
        
        // Если все-таки нет - генерим пароль и сохраняем
        $user->password = Module_Passwordchange_Helper::codeGenerator();
        
        $user->id = $userMapper->apply($user);
        
        // Авторизуем нового юзера, чтобы он смог сразу войти в свой профиль
        self::autoLogin($user);
        
        return $user;
    }
    
    public static function autoLogin($user)
    {
        $session = new Module_Session_Service();
        $session->start($user);
    }
}

?>
