<?php

$url = Dante_Controller_Front::getRequest()->get_requested_url();

Dante_Lib_Router::addRule("/manage\/villauser\/clients/", array(
    'controller' => 'Villa_Module_User_Manage_Controller_Clients'
));

Dante_Lib_Router::addRule("/manage\/villauser\/touragents/", array(
    'controller' => 'Villa_Module_User_Manage_Controller_Touragents'
));

Dante_Lib_Router::addRule("/manage\/villauser\/admins/", array(
    'controller' => 'Villa_Module_Admins_Controller'
));

Dante_Lib_Router::addRule("/^(\/[a-z]{2,3})?\/mybookings\/$/", array(
    'controller' => 'Villa_Module_User_Controller',
    'action' => 'drawBookingHistory'
));

Dante_Lib_Router::addRule("/^(\/[a-z]{2,3})?\/passport-upload\/$/", array(
    'controller' => 'Villa_Module_User_Controller',
    'action' => 'drawScanForm'
));

Dante_Lib_Router::addRule("/^(\/[a-z]{2,3})?\/touragent-menu\/$/", array(
    'controller' => 'Villa_Module_User_Controller',
    'action' => 'drawTouragentForm'
));

Dante_Lib_Router::addRule("/^(\/[a-z]{2,3})?\/mybookings\/edit-(\d+)$/", array(
    'controller' => 'Villa_Module_Hotel_Controller_Bookingform',
    'action' => 'editBooking'
));

?>