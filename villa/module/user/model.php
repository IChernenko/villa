<?php

/**
 * Description of manage
 *
 * @author Mort
 */
class Villa_Module_User_Model extends Module_User_Model
{    
    public $user_id;
    
    public $first_name;
    
    public $last_name;

    public $company = '';
    
    public $phone = '';
    
    public $discount_services = '';
    
    public $discount_residence = '';
    
    public $scan = '';
    
    public $address = '';
    
    public $town = '';
    
    public $country = '';
    
    public $index = '';
    
    public $logo;
    
    public $requisites;
}

?>
