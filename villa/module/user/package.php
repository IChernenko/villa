<?php
global $package;
$package = array(
    'version' => '7',
    'name' => 'villa.module.user',
    'dependence' => array(
        'module.group',
        'module.auth',
        'module.lang',
        'lib.jqueryUpload',
    ),
    'js' => array(
        '../villa/module/user/js/manage/villauser.js',
        '../villa/module/user/js/profile.js'
    )
);