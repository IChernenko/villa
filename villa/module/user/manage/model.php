<?php

/**
 * Description of manage
 *
 * @author Mort
 */
class Villa_Module_User_Manage_Model extends Module_User_Model_Manage_Usergrid
{
    public $filter_rules = array(        
        array(
            'name'=>'id',
            'format'=>'`t1`.`%1$s` = "%2$s"'
        ),
        array(
            'name'=>'email',
            'format'=>'`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ),      
        array(
            'name'=>'discount_residence',
            'format'=>'`t2`.`%1$s` = "%2$s"'
        ),        
        array(
            'name'=>'discount_options',
            'format'=>'`t2`.`%1$s` = "%2$s"'
        ),
        array(
            'name'=>'group_id',
            'format'=>'`t3`.`group_id` = "%2$s"'
        ),
        array(
            'name' => 'rating',
            'format'=>'`t1`.`%1$s` = "%2$s"'
        )
    );
}

?>
