<?php
/**
 * Description of view
 *
 * @author Valkyria
 */
class Villa_Module_User_Manage_View
{
    public function drawForm($model)
    {
        $tpl = new Dante_Lib_Template();
        $tpl->model = $model;
        $tpl->groupList = $this->groupList;
        
        if($model->scan)
            $tpl->scanPath = Villa_Module_User_Helper::getPassportScanDir().$model->scan;
        else
           $tpl->scanPath = Lib_Image_Helper::getNoImage(); 
        
        if($model->logo)
            $tpl->logoPath = Villa_Module_User_Helper::getTouragentLogoDir().$model->logo;
        else
           $tpl->logoPath = Lib_Image_Helper::getNoImage(); 
        
        $tpl->tourAgentId = Villa_Module_User_Helper::getTourAgentGroupId();
        
        $fileName = '../villa/module/user/manage/template/form.html';
        
        return $tpl->draw($fileName);
    }
}

?>
