<?php

/**
 * Description of controller
 *
 * @author Valkyria
 */

class Villa_Module_User_Manage_Controller extends Module_User_Controller_Manage_User 
{
    protected $_alias = 'villa.module.user.manage.controller';
    
    protected function _init() {
        parent::_init();
        $this->_mapper = new Villa_Module_User_Manage_Mapper();
    }
    
    protected function _instanceView() 
    {
        return new Villa_Module_User_Manage_View();
    }    
    
    protected function _instanceModel() 
    {
        return new Villa_Module_User_Manage_Model();
    }
    
    protected function _instanceGroupMapper()
    {
        return new Module_Group_Mapper();
    }

    protected function _getDefaultFilter()
    {}

    public function getParams($model = false) 
    {
        $params = parent::getParams($model);
        $params['where'][] = $this->_getDefaultFilter();
        return $params;
    }    
    
    protected function _genTitles()
    {          
        $titles = array(
            array(
                'content'=>"id",
                'sortable'  =>'id',
                'sortable_def' =>'desc'
            ),
            array(
                'content' => 'Пользователь'
            ),
            array(
                'content'   =>'Email',
                'sortable'  =>'email'
            ),
            array(
                'content'   =>'Скидка (проживание)',
                'sortable'  =>'discount_residence',
                'params'=>array(
                    'style'=>array(
                        'width'=>'100px'
                    )
                )  
            ),
            array(
                'content'   =>'Скидка (услуги)',
                'sortable'  =>'discount_options',
                'params'=>array(
                    'style'=>array(
                        'width'=>'100px'
                    )
                )  
            ),
            array(
                'content'   =>'Функции',
                'params'=>array(
                    'style'=>array(
                        'width'=>'130px'
                    )
                )                
            ),
        );
        
        return $titles;
    }
    
    protected function _genFilters()
    {        
        $request = $this->_getRequest();  
        $filters = array(
            null,
            null,            
            "email"=>array(
                "type"=>"text",
                "value"=>$request->get_validate("email", "text"),
            ),            
            "discount_residence"=>array(
                "type"=>"text",
                "value"=>$request->get_validate("discount_residence", "int"),
            ),         
            "discount_options"=>array(
                "type"=>"text",
                "value"=>$request->get_validate("discount_options", "int"),
            ),         
            array(
                "type"=>"button_filter"
            ),            
        );
        
        return $filters;
    }


    protected function _genRows($rows)
    {               
        $table = array();
       
        foreach($rows as $row)
        {           
            $cur = array();
            $cur['id'] = $row['id'];
            $cur['user'] = $row['name'].' '.$row['surname'];            
            $cur['email'] = $row['email'];            
            $cur['discount_residence'] = $row['discount_residence']; 
            $cur['discount_options'] = $row['discount_options'];       
            $cur['buttons'] = array(
                'id'=>$row['id'],
                'type'=>array('edit', 'del'),
                'content'=>$this->_buttonActive($row['id'],$row['is_active'])
            );            
            
            if($row['is_active'])
                $params['class']='success';
            else
                $params['class']='error';            
            
            $table[$row["id"]]= array(
                'content'=>$cur,
                'params'=>$params
            );
        }
        
        return $table;
    }

    protected function _populateModel()
    {    
        $model = new Villa_Module_User_Model(); 
        $request = $this->_getRequest();
        $model->id      = $request->get_validate('id', 'int', false);
        $model->email   = $request->get_validate('email', 'email', false);
        $model->login   = $request->get_validate('login', 'text', false);
                
        if($password = $request->get_validate('password', 'password'))
            $model->password = $password;
        
        $model->rating = $request->get_validate('rating', 'int', false);
        
        $model->company  = $request->get_validate('company', 'text', '');
        $model->first_name  = $request->get_validate('first_name', 'text', '');
        $model->last_name  = $request->get_validate('last_name', 'text', '');
        $model->phone  = $request->get_validate('phone', 'text', '');
        $model->scan  = $request->get_validate('scan', 'text', '');
        $model->address  = $request->get_validate('address', 'text', '');
        $model->town  = $request->get_validate('town', 'text', '');
        $model->country  = $request->get_validate('country', 'text', '');
        $model->index  = $request->get_validate('index', 'text', '');
        $model->discount_services  = $request->get_validate('discount_services', 'int', 0);
        $model->discount_residence  = $request->get_validate('discount_residence', 'int', 0);
        
        $model->requisites = $request->get_validate('requisites', 'text', '');
        $model->logo = $request->get_validate('logo', 'text', '');
        
        return $model;
    }
    
    protected function _scanUploadAction()
    {
        if(!isset($_FILES['file']))
            return array(
                'result' => 0,
                'message' => 'Нет файла для загрузки'
            );
                    
        $uploadDir = Villa_Module_User_Helper::getPassportScanDir();
        if (!is_dir($uploadDir)) mkdir($uploadDir, 0755, true);             

        $newName = 'scan_'.time();  

        $uploader = new Lib_JqueryUpload_Uploader();
        $uploader->globalKey = 'file';
        
        $image = $uploader->upload($uploadDir, $newName);  
         
        $uid = $this->_getRequest()->get('id');
        if(!$uid)
            return array(
                'result' => 0,
                'message' => 'wrong uid'
            );
        
        $mapper = new Villa_Module_User_Manage_Mapper();
        
        $oldScan = $mapper->getPassportScan($uid);
        if(is_file($uploadDir.$oldScan)) unlink($uploadDir.$oldScan);
        
        $mapper->updatePassportScan($uid, $image);
        
        return array(
            'result' => 1,
            'dir' => $uploadDir,
            'image' => $image
        );
    }
    
    protected function _delScanAction()
    {
        $uid = $this->_getRequest()->get('id');
        if(!$uid)
            return array(
                'result' => 0,
                'message' => 'wrong uid'
            );
        
        $uploadDir = Villa_Module_User_Helper::getPassportScanDir();
        $mapper = new Villa_Module_User_Manage_Mapper();
        
        $oldScan = $mapper->getPassportScan($uid);
        if(is_file($uploadDir.$oldScan)) unlink($uploadDir.$oldScan);
        
        $mapper->delPassportScan($uid);
        
        return array(
            'result' => 1,
            'noimage' => Lib_Image_Helper::getNoImage()
        );
    }
    
    protected function _logoUploadAction()
    {
        if(!isset($_FILES['file']))
            return array(
                'result' => 0,
                'message' => 'Нет файла для загрузки'
            );
                    
        $uploadDir = Villa_Module_User_Helper::getTouragentLogoDir();
        if (!is_dir($uploadDir)) mkdir($uploadDir, 0755, true);             

        $newName = 'logo_'.time();  

        $uploader = new Lib_JqueryUpload_Uploader();
        $uploader->globalKey = 'file';
        
        $image = $uploader->upload($uploadDir, $newName);  
         
        $uid = $this->_getRequest()->get('id');
        if(!$uid)
            return array(
                'result' => 0,
                'message' => 'wrong uid'
            );
        
        $mapper = new Villa_Module_User_Manage_Mapper();
        
        $oldScan = $mapper->getTouragentLogo($uid);
        if(is_file($uploadDir.$oldScan)) unlink($uploadDir.$oldScan);
        
        $mapper->updateTouragentLogo($uid, $image);
        
        return array(
            'result' => 1,
            'dir' => $uploadDir,
            'image' => $image
        );
    }
    
    protected function _delLogoAction()
    {
        $uid = $this->_getRequest()->get('id');
        if(!$uid)
            return array(
                'result' => 0,
                'message' => 'wrong uid'
            );
        
        $uploadDir = Villa_Module_User_Helper::getPassportScanDir();
        $mapper = new Villa_Module_User_Manage_Mapper();
        
        $oldScan = $mapper->getTouragentLogo($uid);
        if(is_file($uploadDir.$oldScan)) unlink($uploadDir.$oldScan);
        
        $mapper->delTouragentLogo($uid);
        
        return array(
            'result' => 1,
            'noimage' => Lib_Image_Helper::getNoImage()
        );
    }
}

?>