<?php

/**
 * Description of manage
 *
 * @author Mort
 */
class Villa_Module_User_Manage_Mapper extends Module_User_Mapper_Manage_User
{    
    protected function _instanceModel()
    {
        return new Villa_Module_User_Model();
    }
    
    protected function _getProfile($uid, $model) 
    {
        $table = new Dante_Lib_Orm_Table($this->_userProfileTableName);
        $table->select(array('user_id' => $uid));
        
        if($table->getNumRows() == 0) return $model;
        $model->company = $table->company;
        $model->first_name = $table->name;
        $model->last_name = $table->surname;
        $model->phone = $table->phone;
        $model->address = $table->adress;
        $model->town = $table->town;
        $model->country = $table->country;
        $model->index = $table->index;
        $model->scan = $table->scan;
        $model->discount_services = $table->discount_options;
        $model->discount_residence = $table->discount_residence;
        
        $model->logo = $table->logo;
        $model->requisites = $table->requisites;
        
        return $model;
    }

    protected function _applyProfile($model)
    {
        $tableObj = new Dante_Lib_Orm_Table($this->_userProfileTableName);
        $cond = array('user_id'=>$model->id);
        $tableObj->user_id = $model->id;
        $tableObj->company = $model->company;
        $tableObj->name = $model->first_name;
        $tableObj->surname = $model->last_name;
        $tableObj->phone = $model->phone;
        $tableObj->adress = $model->address;
        $tableObj->town = $model->town;
        $tableObj->country = $model->country;
        $tableObj->index = $model->index;
        $tableObj->scan = $model->scan;
        $tableObj->discount_options = $model->discount_services;
        $tableObj->discount_residence = $model->discount_residence;
                
        $tableObj->logo = $model->logo;
        $tableObj->requisites = $model->requisites;
        
        $tableObj->apply($cond);
    }
    
    public function getEmailsList()
    {
        $sql = "SELECT * FROM ".$this->_tableName; 
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        
        $users = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) 
        {
            $users[$f['id']] = $f['email'];
        }
        
        return $users;
    }
    
    public function getPassportScan($uid)
    {
        $table = new Dante_Lib_Orm_Table($this->_userProfileTableName);
        $table->select(array('user_id' => $uid));
        
        if($table->getNumRows() == 0) return '';
        return $table->scan;
    }
    
    public function updatePassportScan($uid, $scan)
    {
        $table = new Dante_Lib_Orm_Table($this->_userProfileTableName);
        $table->scan = $scan;
        
        if($table->exist(array('user_id' => $uid)))
            $table->update(array('user_id' => $uid));
        else
        {
            $table->user_id = $uid;
            $table->insert();
        }
    }
    
    public function delPassportScan($uid)
    {
        $this->updatePassportScan($uid, '');
    }
    
    public function getTouragentLogo($uid)
    {
        $table = new Dante_Lib_Orm_Table($this->_userProfileTableName);
        $table->select(array('user_id' => $uid));
        
        if($table->getNumRows() == 0) return '';
        return $table->logo;
    }
    
    public function updateTouragentLogo($uid, $logo)
    {
        $table = new Dante_Lib_Orm_Table($this->_userProfileTableName);
        $table->logo = $logo;
        
        if($table->exist(array('user_id' => $uid)))
            $table->update(array('user_id' => $uid));
        else
        {
            $table->user_id = $uid;
            $table->insert();
        }
    }
    
    public function updateRequisitesAction($uid, $requisites)
    {
        $table = new Dante_Lib_Orm_Table($this->_userProfileTableName);
        $table->requisites = $requisites;
        
        if($table->exist(array('user_id' => $uid)))
            $table->update(array('user_id' => $uid));
        else
        {
            $table->user_id = $uid;
            $table->insert();
        }
    }
    
    public function delTouragentLogo($uid)
    {
        $this->updateTouragentLogo($uid, '');
    }
    
    public function profileExists($email)
    {
        $uid = $this->emailExists($email);
        if(!$uid) return false;
        
        $table = new Dante_Lib_Orm_Table($this->_userProfileTableName);
        $table->select(array(
            'user_id' => $uid
        ));
        
        if($table->getNumRows() == 0) 
            return false;
        else
            return $uid;
    }
    
    public function emailExists($email)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->select(array(
            'email' => $email
        ));
        
        if($table->getNumRows() == 0) 
            return false;
        else
            return $table->id;
    }
}

?>
