<?php

/**
 * Description of clients
 *
 * @author Valkyria
 */
class Villa_Module_User_Manage_Controller_Clients extends Villa_Module_User_Manage_Controller
{
    protected $_alias = 'villa.module.user.manage.controller.clients';
    
    protected function _getUserGroupId()
    {
        return $this->_instanceGroupMapper()->getByName('user');
    }

    protected function _getDefaultFilter() 
    {        
        $groupId = $this->_getUserGroupId();
        return '`t3`.`group_id` = '.$groupId;
    }
    
    protected function _populateModel() 
    {
        $model = parent::_populateModel();
        $model->group_id = $this->_getUserGroupId();
        
        return $model;
    }
}
