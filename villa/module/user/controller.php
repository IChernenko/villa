<?php
/**
 * Description of controller
 *
 * @author Valkyria
 */
class Villa_Module_User_Controller extends Module_User_Controller
{

    protected function _defaultAction() 
    {
        $uid = Dante_Helper_App::getUid();
        if (!$uid) return '';
        
        $items = $this->_getMenuItems();
        if (count($items)==0) return '';        
        
        $html = '<p>'.Module_Lang_Helper::translate('myProfile').'<span></span></p>';
        $html .= '<div id="profile_menu_wrap"><div class="popup-detail"></div>';
        $html .= "<ul id='profile_menu'>";
        foreach($items as $item) {
            $html .= "<li><a href='{$item['url']}'>{$item['name']}</a></li>";
        }
        $html .= '</ul></div>';
        
        $login = $this->_instanceMapper()->getEmailById($uid);
        
        $html .= '<div id="loginDisp">'.$login.'</div>';
        
        return $html;
    }
    
    protected function _getMenuItems() 
    {
        $items = array();
        $items[] = array(
            'url' => '/mybookings/',
            'name' => Module_Lang_Helper::translate('myBookings')
        );
        
        $items[] = array(
            'url' => 'javascript:profile.showProfileForm(1);',
            'name' => Module_Lang_Helper::translate('personalData')
        );
        
        $items[] = array(
            'url' => 'javascript:profile.showDiscounts();',
            'name' => Module_Lang_Helper::translate('discounts')
        );
        
        $items[] = array(
            'url' => '/passport-upload/',
            'name' => Module_Lang_Helper::translate('passportScan')
        );
        
        $items[] = array(
            'url' => 'javascript:profile.showProfileForm(2);',
            'name' => Module_Lang_Helper::translate('changeEmail')
        );
        
        $items[] = array(
            'url' => 'javascript:profile.showProfileForm(3);',
            'name' => Module_Lang_Helper::translate('changePassword')
        );
        
        if(Villa_Module_User_Helper::checkTourAgent())
        {
            $items[] = array(
                'url' => '/touragent-menu/',
                'name' => Module_Lang_Helper::translate('tourAgentData')
            );
        }
        
        
        return $items;
    }
    
    protected function _drawDiscountsAction()
    {
        $view = new Villa_Module_User_View();
        $mapper = new Villa_Module_User_Manage_Mapper();
        $profile = $mapper->getUser(Dante_Helper_App::getUid());
        
        return array(
            'result' => 1,
            'html' => $view->drawDiscounts($profile)
        );
    }


    protected function _drawScanFormAction()
    {
        $uid = Dante_Helper_App::getUid();
        
        $mapper = new Villa_Module_User_Manage_Mapper();
        $scanImage = $mapper->getPassportScan($uid);
        
        $view = new Villa_Module_User_View();
        return $view->drawScanForm($scanImage);
    }
    
    protected function _drawTouragentFormAction()
    {
        $uid = Dante_Helper_App::getUid();
        
        $mapper = new Villa_Module_User_Manage_Mapper();
        $model = $mapper->getUser($uid);
        
        $view = new Villa_Module_User_View();
        return $view->drawTouragentForm($model->logo, $model->requisites);
    }

    protected function _scanUploadAction()
    {
        if(!isset($_FILES['file']))
            return array(
                'result' => 0,
                'message' => Module_Lang_Helper::translate('noFileForUpload')
            );
                    
        $uploadDir = Villa_Module_User_Helper::getPassportScanDir();
        if (!is_dir($uploadDir)) mkdir($uploadDir, 0755, true);             

        $newName = 'scan_'.time();  

        $uploader = new Lib_JqueryUpload_Uploader();
        $uploader->globalKey = 'file';
        
        $image = $uploader->upload($uploadDir, $newName);  
         
        $uid = Dante_Helper_App::getUid();
        
        $oldScan = $mapper->getPassportScan($uid);
        if(is_file($uploadDir.$oldScan)) unlink($uploadDir.$oldScan);
        
        $mapper = new Villa_Module_User_Manage_Mapper();        
        $mapper->updatePassportScan($uid, $image);
        
        return array(
            'result' => 1,
            'image' => $uploadDir.$image
        );
    }
    
    protected function _delScanAction()
    {
        $uid = Dante_Helper_App::getUid();
        $uploadDir = Villa_Module_User_Helper::getPassportScanDir();
        $mapper = new Villa_Module_User_Manage_Mapper();
        
        $oldScan = $mapper->getPassportScan($uid);
        if(is_file($uploadDir.$oldScan)) unlink($uploadDir.$oldScan);
        
        $mapper->delPassportScan($uid);
        
        return array(
            'result' => 1,
            'noimage' => Lib_Image_Helper::getNoImage()
        );
    }
    
     protected function _logoUploadAction()
    {
        if(!isset($_FILES['file']))
            return array(
                'result' => 0,
                'message' => Module_Lang_Helper::translate('noFileForUpload')
            );
                    
        $uploadDir = Villa_Module_User_Helper::getTouragentLogoDir();
        if (!is_dir($uploadDir)) mkdir($uploadDir, 0755, true);             

        $newName = 'logo_'.time();  

        $uploader = new Lib_JqueryUpload_Uploader();
        $uploader->globalKey = 'file';
        
        $image = $uploader->upload($uploadDir, $newName);  
         
        $uid = Dante_Helper_App::getUid();
        if(!$uid)
            return array(
                'result' => 0,
                'message' => 'wrong uid'
            );
        
        $mapper = new Villa_Module_User_Manage_Mapper();
        
        $oldScan = $mapper->getTouragentLogo($uid);
        if(is_file($uploadDir.$oldScan)) unlink($uploadDir.$oldScan);
        
        $mapper->updateTouragentLogo($uid, $image);
        
        return array(
            'result' => 1,
            'dir' => $uploadDir,
            'image' => $image
        );
    }
    
    protected function _delLogoAction()
    {
        $uid = Dante_Helper_App::getUid();
        $uploadDir = Villa_Module_User_Helper::getPassportScanDir();
        $mapper = new Villa_Module_User_Manage_Mapper();
        
        $oldScan = $mapper->getTouragentLogo($uid);
        if(is_file($uploadDir.$oldScan)) unlink($uploadDir.$oldScan);
        
        $mapper->delTouragentLogo($uid);
        
        return array(
            'result' => 1,
            'noimage' => Lib_Image_Helper::getNoImage()
        );
    }
    
    protected function _saveRequisitesAction()
    {
        $uid = Dante_Helper_App::getUid();
        $requisites = $this->_getRequest()->get('requisites');
        
        $mapper = new Villa_Module_User_Manage_Mapper();
        $mapper->updateRequisitesAction($uid, $requisites);
        
        return array(
            'result' => 1
        );
    }


    protected function _drawBookingHistoryAction()
    {
        $uid = (int)Dante_Helper_App::getUid();
        if ($uid == 0) return Module_Lang_Helper::translate('userAccessOnly');
        
        $lang = Module_Lang_Helper::getCurrent(); 
        
        $roomTypesMapper = new Villa_Module_Hotel_Mapper_Roomtypes();
        $roomTypes = $roomTypesMapper->getAllByLang($lang);
        $prepTypesMapper = new Villa_Module_Hotel_Mapper_Prepayment();
        $prepTypes = $prepTypesMapper->getNamesList();
                
        $mapper = new Villa_Module_Hotel_Mapper_Manage_Booking();
        $list = $mapper->getHistory($uid);
        $statuses = $mapper->getStatuses();        
        
        $currencyInfo = Module_Currency_Helper::getCurrentCurrency();
        
        $payButtons = array();
        $payButtonsView = new Villa_Module_Hotel_View_Bookingform();
        $payButtonsView->setPaymentReturnUrl('pay-by-profile/');
        $payButtonsView->setCurrencyCode($currencyInfo['reduction']);
        
        foreach($list as $model) 
        {
            if($model->totalsData->getTotalPayable() == 0)
            {
                $payButtons[$model->id] = false;
                continue;
            }
            
            $payButtonsView->setRoomTypeName($roomTypes[$model->roomData->type_id]);
            $payButtonsView->setPrepaymentName($prepTypes[$model->roomData->prepayment]);
            $payButtonsView->setPaymentBookingId($model->id);
            $payButtonsView->setPaymentUid(Dante_Helper_App::getUid());
            $buttons = $payButtonsView->drawButtons($model->totalsData, PAYOPT_REMEMBER_ID);                
            $payButtons[$model->id] = $buttons;
        }      
        
        $view = new Villa_Module_User_View();
        $view->statusList = $statuses;
        $view->roomTypesList = $roomTypes;
        $view->currencyName = $currencyInfo['reduction'];
        return $view->drawHistory($list, $payButtons); 
    }
    
    protected function _changeDatesAction()
    {
        $request = $this->_getRequest();
        $bookingId = $request->get('booking_id');
        $basicData = new Villa_Module_Hotel_Model_Booking_Basic();
        $basicData->arrival = $request->get('arrival');
        $basicData->departure = $request->get('departure');
        $newDays = $basicData->getDays();
        
        $roomId = $request->get('room_id');
        
        $mapper = new Villa_Module_Hotel_Mapper_Booking();
        $mapper->changeDates($basicData, $newDays, $bookingId, $roomId);
        
        return array(
            'result' => 1
        );
    }

    protected function _cancelBookingAction()
    {
        $uid = Dante_Helper_App::getUid();
        if(!$uid)
            throw new Exception(Module_Lang_Helper::translate('accessDenied'));
        
        $mapper = new Villa_Module_User_Manage_Mapper();
        $user = $mapper->getUser($uid);
        
        $password = $this->_getRequest()->get('password');
        
        $service = new Module_Auth_Service();
        $service->checkPassword($user, $password);
        
        $bookingMapper = new Villa_Module_Hotel_Mapper_Manage_Booking();
        
        $bookingId = $this->_getRequest()->get('booking_id');        
        $bookingMapper->changeStatus($bookingId, BOOKING_STATUS_CANCEL);
                
        return array(
            'result' => 1,
            'message' => str_ireplace('{id}', $bookingId, Module_Lang_Helper::translate('bookingCalcelled'))
        );
    }
}

?>
