ALTER TABLE [%%]user_profile DROP `fio`;

ALTER TABLE `[%%]user_profile` ADD `company` varchar(100) NOT NULL;
ALTER TABLE `[%%]user_profile` ADD `name` varchar(100) NOT NULL;
ALTER TABLE `[%%]user_profile` ADD `surname` varchar(100) NOT NULL;
ALTER TABLE `[%%]user_profile` ADD `phone` varchar(15) NOT NULL;

ALTER TABLE `[%%]user_profile` ADD `discount_options` int(2) NOT NULL;
ALTER TABLE `[%%]user_profile` ADD `discount_residence` int(2) NOT NULL;

ALTER TABLE `[%%]user_profile` ADD `scan` varchar(100) NOT NULL;
ALTER TABLE `[%%]user_profile` ADD `description` text NOT NULL;

ALTER TABLE `[%%]user_profile` ADD `adress` varchar(100) NOT NULL;
ALTER TABLE `[%%]user_profile` ADD `town` varchar(100) NOT NULL;

ALTER TABLE `[%%]user_profile` ADD `country` varchar(100) NOT NULL;
ALTER TABLE `[%%]user_profile` ADD `index` varchar(50) NOT NULL;