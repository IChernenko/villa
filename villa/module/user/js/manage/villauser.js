villauser = {
    controller: 'villa.module.user.manage.controller',
    formSubmit: function(obj)
    {
        var data = $('#editUserForm').serializeArray();
        data.push(
            {name: 'controller', value: villauser.controller},
            {name: 'action', value: 'edit'}
        );
            
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'ajax.php',
            data: data,
            success: function(data){
                if(data.result == 1)
                {
                    jAlert('Изменения сохранены', 'Сообщение', function(){
                        $('#modalWindow .modal-body').html(data.html);
                    });
                }
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    }
}