profile = {
    
    button: false,
    
    base: '<div id="profileForm" class="b-modal villa-modal-form">  \
                <div class="b-modal_close arcticmodal-close"></div>   \
                <div id="profileFormBlock">    \
                {{>form}}   \
                </div>  \
                </div>',
    
    form:  '<p class="strong">{{{head}}}</p>  \
            <input type="hidden" id="user_id" value="{{user_id}}"/>    \
            <ul id="{{ul}}"> \
            {{#inputs}}  \
                    <li><input type="{{type}}" id="{{id}}" placeholder="{{title}}" title="{{title}}" value="{{value}}" {{{other}}}/>{{{after}}}</li>    \
            {{/inputs}}  \
            </ul>    \
            {{#button}}    \
            <button id="{{id}}" onclick="profile.{{funct}};"><span>{{value}}</span></button>  \
            {{/button}} \
            <p class="strong bottom">  \
            {{#links}}  {{{.}}}{{/links}}    \
            </p>',
    
    message: '<p class="message">{{{message}}}</p>',
    
    profile: {
        head: '<span>'+villa.labels.personalData+'</span>',
        links: [
            '<a href="javascript:profile.changeForm(2);" >'+villa.labels.changeEmail+'</a>',
            '<a href="javascript:profile.changeForm(3);" >'+villa.labels.changePassword+'</a>'
        ],
        ul: 'userProfileList',
        inputs: [
            {type: 'name', id: 'profile_name', title: villa.labels.name},
            {type: 'name', id: 'profile_surname', title: villa.labels.lastName},
            {type: 'number', id: 'profile_phone', title: villa.labels.phone, other: 'onchange="profile.phoneValidate(this)"', after: '<span class="phone-plus">+</span>'},
            {type: 'text', id: 'profile_index', title: villa.labels.postalIndex},
            {type: 'text', id: 'profile_country', title: villa.labels.country},
            {type: 'text', id: 'profile_town', title: villa.labels.town},
            {type: 'text', id: 'profile_address', title: villa.labels.address},
            {type: 'hidden', id: 'profile_company', title: villa.labels.workCompany},
            
        ],
        button: [
            {id: 'profileSaveBtn', funct: 'saveProfile(this)', value: villa.labels.save}
        ]
    },
    
    email: {
        head: '<span>'+villa.labels.changeEmail+'</span>',
        links: [
            '<a href="javascript:profile.changeForm(1);" >'+villa.labels.personalData+'</a>',
            '<a href="javascript:profile.changeForm(3);" >'+villa.labels.changePassword+'</a>'
        ],
        ul: 'changeEmailList',
        inputs: [
            {type: 'email', id: 'curEmail', title: villa.labels.curEmail, other: 'disabled'},
            {type: 'email', id: 'newEmail', title: villa.labels.newEmail},
            
        ],
        button: [
            {id: 'changeEmailBtn', funct: 'changeEmail(this)', value: villa.labels.save}
        ]
    },
    
    password: {
        head: '<span>'+villa.labels.changePassword+'</span>',
        links: [
            '<a href="javascript:profile.changeForm(1);" >'+villa.labels.personalData+'</a>',
            '<a href="javascript:profile.changeForm(2);" >'+villa.labels.changeEmail+'</a>'
        ],
        ul: 'changePassList',
        inputs: [
            {type: 'password', id: 'curPass', title: villa.labels.curPass, other: 'maxlength="10"'},
            {type: 'password', id: 'newPass', title: villa.labels.newPass, other: 'maxlength="10"'},
            {type: 'password', id: 'repeatPass', title: villa.labels.repeatPassword, other: 'maxlength="10"'},
        ],
        button: [
            {id: 'changePassBtn', funct: 'changePass(this)', value: villa.labels.changePassword}
        ]
    },
    
    renderForm: function(full, formData)
    {
        if(!full) return Mustache.to_html(this.form, formData);
        else 
        {
            var template = {form: this.form};
            return Mustache.to_html(this.base, formData, template);
        }
    },
    
    renderMessage: function(text)
    {
        return Mustache.to_html(this.message, text);
    },
    
    showProfileForm: function(id)
    {
        $.ajax({
            type: 'POST',
            url: '/ajax.php',
            data: {
                controller: 'villa.module.user.controller',
                action: 'getProfile',
                full: 1,
                json: 1
            },
            success: function(data){
                if(data.result == 1)
                {
                    var inputs = profile.profile.inputs;
                    var count = inputs.length;
                    for(var i=0; i<count; i++)
                    {
                        var input = inputs[i];
                        var index = input.id.substr(8);
                        profile.profile.inputs[i].value = data.profile[index];
                    }
                    profile.email.inputs[0].value = data.profile.email;
                    
                    profile.profile.user_id = data.profile.user_id;
                    profile.email.user_id = data.profile.user_id;
                    profile.password.user_id = data.profile.user_id;
                    
                    profile.selectForm(id);
                }
                else
                {
                    jAlert(data.message, villa.labels.error);
                }
            }
        });
    },
    
    selectForm: function(id)
    {
        var form = false;
        switch(id)
        {
            case 1:
                form = this.profile;
                this.lastForm = 1;
                break;
            case 2:
                form = this.email;
                this.lastForm = 2;
                break;
            case 3:
                form = this.password;
                this.lastForm = 3;
                break;
        }
        $('#content').append(this.renderForm(true, form));
        this.activateForm();
    },
    
    /**
     * Активация формы
     */
    activateForm: function()
    {
        $('#profileForm').arcticmodal({
            afterClose: (function(){        
                $('#profileForm').remove();
            })
        });

        $('.b-modal_close arcticmodal-close').click(function(){
            $('#profileForm').arcticmodal('close');
        });
    },
    
    changeForm: function(form)
    {
        var newForm = false;
        switch(form)
        {
            case 1:
                newForm = this.profile;
                break;
            case 2:
                newForm = this.email;
                break;
            case 3:
                newForm = this.password;
                break;
        }
        $('#profileFormBlock').html(this.renderForm(false, newForm));
    },
    
    saveProfile: function(btn)
    {
        var data = {
            user_id: $('#user_id').val(),
            name: $('#profile_name').val(),
            surname: $('#profile_surname').val(),
            phone: $('#profile_phone').val(),
            index: $('#profile_index').val(),
            country: $('#profile_country').val(),
            town: $('#profile_town').val(),
            adress: $('#profile_address').val(),
            company: $('#profile_company').val()
        }
        this.preload(btn);
        $.ajax({
            type: 'POST',
            url: '/ajax.php',
            data: {
                controller: 'villa.module.user.controller',
                action: 'saveProfile',
                json: 1,
                profile: data
            },
            success: function(data){
                profile.callbackProcess(data, btn);
            }
        });
    },
    
    validateEmail: function()
    {
        var pattern = /\b^[\w\.\-]+\@[a-z]+\.[a-z]{2,3}\b/i;
        if(!$('#newEmail').val())
        {
            $('.message').remove();
            var text = {message: villa.labels.enterEmail};
            $('.strong.bottom').before(this.renderMessage(text));
            return false;
        }
        if(!pattern.test($('#newEmail').val()))
        {
            $('.message').remove();
            text = {message: villa.labels.wrongEmail};
            $('.strong.bottom').before(this.renderMessage(text));
            return false;
        }
        return true;
    },
    
    phoneValidate: function(obj)
    {        
        var pattern = /^\d+$/;
        $('.message').remove();
        if(!pattern.test($(obj).val()))
        {
            text = {message: 'В поле "Телефон" возможны только цифры'};
            $('.strong.bottom').before(this.renderMessage(text));
        }
    },
    
    changeEmail: function(btn)
    {   
        if(!this.validateEmail()) return false;
        this.preload(btn);
        $.ajax({
            type: 'POST',
            url: '/ajax.php',
            data: {
                controller: 'villa.module.user.controller',
                action: 'changeEmail',
                json: 1,
                user_id: $('#user_id').val(),
                email: $('#newEmail').val()
            },
            success: function(data){
                if(data.result == 1) 
                {
                    $('#curEmail').val(data.email);
                    $('#newEmail').val('');
                }
                profile.callbackProcess(data, btn);
            }
        });
    },
    
    validatePass: function()
    {
        if(!$('#curPass').val())
        {            
            $('.message').remove();
            var text = {message: villa.labels.enterCurPassword};
            $('.strong.bottom').before(this.renderMessage(text));
            return false;
        }
        if(!$('#newPass').val())
        {
            $('.message').remove();
            text = {message: villa.labels.enterNewPassword};
            $('.strong.bottom').before(this.renderMessage(text));
            return false;
        }
        if(!$('#repeatPass').val())
        {
            $('.message').remove();
            text = {message: villa.labels.repeatNewPassword};
            $('.strong.bottom').before(this.renderMessage(text));
            return false;
        }
        if($('#newPass').val() != $('#repeatPass').val())
        {
            $('.message').remove();
            text = {message: villa.labels.passwordsNotEqual};
            $('.strong.bottom').before(this.renderMessage(text));
            return false;
        }
        return true;
    },
    
    changePass: function(btn)
    {   
        if(!this.validatePass()) return false;
        this.preload(btn);
        $.ajax({
            type: 'POST',
            url: '/ajax.php',
            data: {
                controller: 'villa.module.user.controller',
                action: 'changePassword',
                json: 1,
                user_id: $('#user_id').val(),
                curPass: $('#curPass').val(),
                newPass: $('#newPass').val()
            },
            success: function(data){
                $('input[type=password]').each(function(){
                    $(this).val('');
                });
                profile.callbackProcess(data, btn);
            }
        });
    },
    
    preload: function(btn)
    {
        $('.message').remove();
        $(btn).prop('disabled', true);
        $('.strong.bottom').before('<div class="preloader"></div>');
        dante.ajaxContentLoadStart('.preloader');
    },
    
    callbackProcess: function(data, btn)
    {   
        $('.preloader').remove();
        var text = {message: data.message};        
        $(btn).prop('disabled', false);
        $('.strong.bottom').before(this.renderMessage(text));
    },
        
    delScan: function()
    {
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'ajax.php',
            data: {
                controller: 'villa.module.user.controller',
                action: 'delScan'
            },
            success: function(data)
            {
                if(data.result == 1)
                {
                    $('#scanPreview').attr('src', data.noimage);
                    jAlert(villa.labels.fileDeleteSuccess, villa.labels.message);
                }                    
                else
                    jAlert(data.message, villa.labels.error);
            }
        });
    },
        
    delLogo: function()
    {
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'ajax.php',
            data: {
                controller: 'villa.module.user.controller',
                action: 'delLogo'
            },
            success: function(data)
            {
                if(data.result == 1)
                {
                    $('#scanPreview').attr('src', data.noimage);
                    jAlert(villa.labels.fileDeleteSuccess, villa.labels.message);
                }                    
                else
                    jAlert(data.message, villa.labels.error);
            }
        });
    },
    
    saveRequisites: function()
    {
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'ajax.php',
            data: {
                controller: 'villa.module.user.controller',
                action: 'saveRequisites',
                requisites: $('#requisites').val()
            },
            success: function(data)
            {
                if(data.result == 1)
                    jAlert(villa.labels.changesApplied, villa.labels.message);                  
                else
                    jAlert(data.message, villa.labels.error);
            }
        });
    },

    showDiscounts: function()
    {
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'ajax.php',
            data: {
                controller: 'villa.module.user.controller',
                action: 'drawDiscounts'
            },
            success: function(data)
            {
                if(data.result == 1)
                {
                    $('#content').append(data.html);
                    profile.activateForm();
                }                    
                else
                    jAlert(data.message, villa.labels.error);
            }
        });
    },
    
    cancelBooking: function(bookingId)
    {
        jPrompt(villa.labels.bookingCancelEnterPass, '', villa.labels.confirmCancel, function(r){
            if(!r) return;
            
            $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: 'ajax.php',
                data: {
                    controller: 'villa.module.user.controller',
                    action: 'cancelBooking',
                    booking_id: bookingId,
                    password: r
                },
                success: function(data)
                {
                    if(data.result == 1)
                        jAlert(data.message, villa.labels.message, function(){location.reload()});
                    else
                        jAlert(data.message, villa.labels.error);
                }
            });
        });
        $('#popup_prompt').replaceWith('<input type="password" size="30" id="popup_prompt"/>');
        $("#popup_prompt").width( $("#popup_message").width() );
    },
    
    openDatesWin: function(bookingId, roomType, obj)
    {
        var inputArrival = $(obj).siblings('span.dates').find('input.arrival');
        var inputDeparture = $(obj).siblings('span.dates').find('input.departure');
        
        var curArrival = inputArrival.val();
        var curDeparture = inputDeparture.val();
        var days = $(obj).siblings('span.dates').find('input.days').val();
        
        var msecDay = 1000 * 3600 * 24; //один день в миллисекундах        
        var now = new Date();
        
        var depDateArray = curDeparture.match(/\d+/g);
        var departureDate = new Date(depDateArray[2], depDateArray[1]-1, depDateArray[0]); 
        
        var arriveDateArray = curArrival.match(/\d+/g);
        var arriveDate = new Date(arriveDateArray[2], arriveDateArray[1]-1, arriveDateArray[0]);
        
        $('#changeArrivalWin').arcticmodal({
            beforeOpen: function(data, el){                
                $(el).find('input[name=booking_id]').val(bookingId);
                $(el).find('input[name=room_type]').val(roomType);
                
                $(el).find('input[name=arrival]').val(curArrival);
                $(el).find('input[name=departure]').val(curDeparture);
                 
                $(el).find('input[name=arrival]').datepicker({
                    dateFormat: 'dd/mm/yy',
                    minDate: 0,
                    numberOfMonths: [1, 3],
                    onSelect: function(dateText, inst){
                        $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');
                        var arriveDate = dateText.match(/\d+/g);
                        var newDepTmp = new Date(arriveDate[2], arriveDate[1]-1, arriveDate[0]);
                        var newDep = new Date(newDepTmp.getTime() + msecDay * days); 
                        
                        $('#changeArrivalWin').find('input[name=departure]').datepicker('setDate', newDep);
                    }
                });
                
                $(el).find('input[name=departure]').datepicker({
                    dateFormat: 'dd/mm/yy',
                    minDate: 1,
                    maxDate: '+6m',
                    numberOfMonths: [1, 3],
                    onSelect: function(dateText, inst){
                        $(this).datepicker('option', 'dateFormat', 'dd/mm/yy');
                    }
                });
            },
            afterClose: function(data, el)
            {
                $(el).find('input').datepicker('destroy').val('');
            }
        });
    },
    
    newRoomSearch: function()
    {
        var data = $('#changeArrivalForm').serializeArray();
        
        data.push(
            {name: 'controller', value: 'villa.module.hotel.controller.roomsearch'},
            {name: 'action', value: 'searchAjax'}
        );
        
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'ajax.php',
            data: data,
            success: function(data)
            {
                if(data.result == 1)
                {
                    data.message += '<br/> '+villa.labels.pressToChangeArrival;
                    $('#changeArrivalWin button').hide();
                    $('#changeArrivalWin #changeArrivalBtn').show();
                    
                    jAlert(data.message, villa.labels.message, function(){
                        $('#changeArrivalWin').find('input[name=room_id]').val(data.room);
                    });
                }
                else
                {
                    data.message += '<br/> '+villa.labels.enterAnotherArrival;
                    jAlert(data.message, villa.labels.error);
                }
            }
        });
    },
    
    changeDates: function()
    {
        var data = $('#changeArrivalForm').serializeArray();
        
        data.push(
            {name: 'controller', value: 'villa.module.user.controller'},
            {name: 'action', value: 'changeDates'}
        );
        
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'ajax.php',
            data: data,
            success: function(data)
            {
                if(data.result == 1)
                {
                    var html = '<p class="success">'+villa.labels.dontForgetToResetTransfers+
                            '<button><span>'+villa.labels.continueLbl+'</span></button></p>';
                    $('#changeArrivalWin .modal-body').html(html);
                    $('#changeArrivalWin button').click(function(){
                        $('#changeArrivalWin').arcticmodal('close');
                        location.reload();
                    });                   
                }
                else
                {
                    jAlert(data.message, villa.labels.error);
                }
            }
        });
    }
}