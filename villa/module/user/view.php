<?php
/**
 * Description of view
 *
 * @author Valkyria
 */
class Villa_Module_User_View 
{
    public $statusList;
    public $roomTypesList;
    
    public $currencyName;
    
    public function drawDiscounts(Villa_Module_User_Model $model)
    {
        $html = '<div id="profileForm" class="b-modal villa-modal-form discounts">
                <div class="b-modal_close arcticmodal-close"></div>
                <div id="profileFormBlock">
                <p class="strong">Скидки</p>
                <ul id="profileDiscounts">
                    <li>
                    <label>Проживание&nbsp;</label>
                    <label>
                    <input type="text" title="{{title}}" value="'.$model->discount_residence.'" disabled/>&nbsp;%</label>
                    </li>
                    <li>
                    <label>Услуги&nbsp;</label>
                    <label><input type="text" title="{{title}}" value="'.$model->discount_services.'" disabled/>&nbsp;%</label>
                    </li>
                </ul>
                </div>
                </div>';
        
        return $html;
    }
    
    public function drawScanForm($scan)
    {
        $tplFileName = '../villa/module/user/template/scan_form.html';
        
        $tpl = new Dante_Lib_Template();
        if($scan)
            $tpl->scanPath = Villa_Module_User_Helper::getPassportScanDir().$scan;
        else
            $tpl->scanPath = Lib_Image_Helper::getNoImage();
                
        return $tpl->draw($tplFileName);
    }
    
    public function drawTouragentForm($logo, $requisites)
    {
        $tplFileName = '../villa/module/user/template/touragent.html';
        
        $tpl = new Dante_Lib_Template();
        if($logo)
            $tpl->logoPath = Villa_Module_User_Helper::getTouragentLogoDir().$logo;
        else
            $tpl->logoPath = Lib_Image_Helper::getNoImage();
        
        $tpl->requisites = $requisites;
                
        return $tpl->draw($tplFileName);
    }
    
    public function drawHistory($list, $payButtons)
    {        
        $tplFileName = '../villa/module/user/template/booking_history.html';
        
        $tpl = new Dante_Lib_Template();
        $tpl->list = $list;
        $tpl->buttons = $payButtons;
        $tpl->statusList = $this->statusList;
        $tpl->roomTypes = $this->roomTypesList;
        $tpl->currencyName = $this->currencyName;
        
        $bookingId = Villa_Module_Hotel_Helper_Session::getBookingId();
        $paymentStatus = Villa_Module_Hotel_Helper_Session::getPaymentStatus();
        if($bookingId && $paymentStatus)
        {
            $bfView = new Villa_Module_Hotel_View_Bookingform();
            $tpl->payMessage = "<strong>Бронирование №{$bookingId}:</strong><br/>".
                                $bfView->getPaymentStatusMessage($paymentStatus);
            
            Villa_Module_Hotel_Helper_Session::unsetBookingId();
            Villa_Module_Hotel_Helper_Session::unsetPaymentStatus();
        }
        else
            $tpl->payMessage = false;
        
        return $tpl->draw($tplFileName);
    }
}

?>
