<?php
/**
 * Description of helper
 *
 * @author Valkyria
 */
class Villa_Module_User_Helper 
{
    public static function getTourAgentGroupId()
    {
        $dmo = new Module_Group_Dmo();
        return $dmo->byName('tour_agent')->fetch()->group_id;
    }
    
    public static function checkTourAgent()
    {
        if(!Dante_Helper_App::getUid()) return false;
        
        $helper = new Module_Division_Helper();
        $groupId = $helper->getGroupId();
        
        return $groupId == self::getTourAgentGroupId();
    }
    
    public static function getPassportScanDir()
    {
        $app = Dante_Lib_Config::get('app.uploadFolder');
        $dir = Dante_Lib_Config::get('hotel.passportScanDir');
        return "{$app}/{$dir}/";
    }
    
    public static function getTouragentLogoDir()
    {
        $app = Dante_Lib_Config::get('app.uploadFolder');
        $dir = Dante_Lib_Config::get('hotel.touragentLogoDir');
        return "{$app}/{$dir}/";
    }
}

?>
