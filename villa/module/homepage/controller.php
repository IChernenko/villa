<?php
    /**
    * Главный контроллер модуля Homepage (/villa/module/homepage/)
    */

class Villa_Module_Homepage_Controller extends Dante_Controller_Base
{
    protected function _defaultAction()
    {
        $userHelper = new Module_Division_Helper();
        $groupId = (int)$userHelper->getGroupId();
        if ($groupId == 3) return '';

        $homePageMapper = new Villa_Module_Homepage_Mapper();
        $servicesMapper = new Villa_Module_Hotel_Mapper_Bookingoptions();
        $view = new Villa_Module_Homepage_View();
        $lang = Module_Lang_Helper::getCurrent();

        /************** Формирование массива типов ноиеров *******************/

        $params = array(
            'lang' => $lang
        );

        $room_prices = array();

        $room_prices_sql = "select * from [%%]hotel_prices WHERE prep_type = 1";

        $r = Dante_Lib_SQL_DB::get_instance()->open($room_prices_sql);
//        $room_prices = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r);
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            array_push( $room_prices, $f['standardPrice'] );
        }

        $mapper = new Villa_Module_Hotel_Mapper_Roomtypes();
        $items = $mapper->getList($params);
        if (!$items) $items = array();

        /************** Формирование массива авиакомпаний *******************/
        $airMapper = new Module_Airlines_Mapper();
        $countryMapper = new Module_Currencycountries_Mapper();
        $townMapper = new Module_Currencytowns_Mapper();
        $airCompanies = array();

        $geoipHelper = new Lib_Geoip_Helper();
        $countryCode = $geoipHelper->getCountryCode($_SERVER['REMOTE_ADDR']);
        if($countryCode == false || $countryCode == '')
        {
            $countryCode = 'UA';
        }
        // Id страны вылета
        $departureCountryId = $countryMapper->getIdByCode($countryCode);

        $airCompanies = $airMapper->getListByCountry($departureCountryId);

        $airCompaniesGallery = $airCompanies;
        /************ Формирование массива стран и массива городов ********************/
        $airCountries = $airMapper->getCountries();
        if($airCountries)
        {
            $airTowns = $townMapper->getList($departureCountryId, $lang);
        }
        else
        {
            $airTowns = null;
        }

        /************** Формирование массива авиакомпаний END *******************/


        // Получение данных страницы с учетом текущей языковой версии $lang
        $homePageData = $homePageMapper->getEntry($lang);
        $homePageGallery = $homePageMapper->getImages();

        // Получение данных блока услуг с учетом текущей языковой версии $lang
        $servicesData = $servicesMapper->getServices();
        $servicesGallery = $servicesMapper->getServicesImages();

        // Получение данных блока экскурсий с учетом текущей языковой версии $lang
        $excursData = $homePageMapper->getExcursionsEntry($lang);
        $excursGallery = $homePageMapper->getExcursionsImages();

        // Получение данных блока трансферов с учетом текущей языковой версии $lang
        $transferData = $homePageMapper->getTransferEntry($lang);
        $transferGallery = $homePageMapper->getTransferImages();




        return $view->drawHomepage(
            $homePageData,          // Информация стартовой страницы
            $homePageGallery,       // Галерея стартовой страницы
            $servicesData, // Дополнительные услуги
            $servicesGallery,
            $excursData, // Экскурсии
            $excursGallery,
            $transferData, // Трансферы
            $transferGallery,
            $airCompanies, // Авиакомпании
            $airCountries, // Страны
            $airTowns, // Города
            $airCompaniesGallery, // Изображения для галереи авиакомпаний
            $departureCountryId,
            $items,
            $room_prices
        );

    }

}
