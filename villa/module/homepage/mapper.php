<?php
/**
 * Class Villa_Module_Homepage_Mapper
 * Маппер модуля Homepage (для работы с базой данных)
 */

class Villa_Module_Homepage_Mapper
{
    protected $_tableName = '[%%]villa_homepage';
    protected $_excursionsTable = '[%%]villa_homepage_excursions';
    protected $_transferTable = '[%%]villa_homepage_transfer';

    protected $_tableGallery = '[%%]villa_homepage_gallery';

    /**
     * Функция, получающая запись из таблицы $_tableName
     * @param $langId - ID языка
     * @return array - данные записи
     */

    public function getEntry($langId)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->select(array('lang_id' => $langId));
        $pageInfo = array();
        $pageInfo['title'] = $table->title;
        $pageInfo['html'] = $table->html;
        $pageInfo['full_descr'] = $table->full_descr;
        $pageInfo['seoTitle'] = $table->seo_title;
        $pageInfo['seoDescription'] = $table->seo_description;
        $pageInfo['seoKeywords'] = $table->seo_keywords;
        $pageInfo['ex_total_title'] = $table->ex_total_title;
        $pageInfo['ex_total_desc'] = $table->ex_total_desc;

        return $pageInfo;
    }

    /**
     * Функция, получающая запись из таблицы $_transferTableName
     * @param $langId - ID языка
     * @return array - данные записи
     */

    public function getExcursionsEntry($langId)
    {
        $table = new Dante_Lib_Orm_Table($this->_excursionsTable);
        $excursInfo = array();
        $excursInfo['title'] = $table->select(array('lang_id' => $langId))->title;
        $excursInfo['info_list'] = array();
        
        $sql = "SELECT `day`, `html`, `full_descr`, `title`,  `ex_show` FROM {$this->_excursionsTable} WHERE `lang_id` = {$langId}";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
            $excursInfo['info_list'][$f['day']] = $f;

        return $excursInfo;
    }

    /**
     * Функция, получающая запись из таблицы $_transferTableName
     * @param $langId - ID языка
     * @return array - данные записи
     */

    public function getTransferEntry($langId)
    {
        $table = new Dante_Lib_Orm_Table($this->_transferTable);
        $table->select(array('lang_id' => $langId));
        $transferInfo = array();
        $transferInfo['title'] = $table->title;
        $transferInfo['html'] = $table->html;
        $transferInfo['full_descr'] = $table->full_descr;

        return $transferInfo;
    }

    /**
     * Функция, получающая изображения из таблицы $_imagesTableName
     */

    public function getImages()
    {
        $entity = Villa_Helper_Entity::getHomepage();
        return $this->_getGallery($entity, true);
    }

    public function getExcursionsImages()
    {
        $entity = Villa_Helper_Entity::getExcursion();
        return $this->_getGallery($entity);
    }

    public function getTransferImages()
    {
        $entity = Villa_Helper_Entity::getTransfer();
        return $this->_getGallery($entity, true);
    }
    
    protected function _getGallery($entityType, $oneEntity = false)
    {
        $sql = "SELECT * FROM {$this->_tableGallery} WHERE `entity_type_id` = {$entityType} ORDER BY draw_order";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);

        $gallery = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            if($oneEntity)
            {
                $gallery[] = Villa_Module_Homepage_Helper::getImageDir().$f['image'];
            }
            else
            {
                if(!isset($gallery[$f['entity_id']]))
                    $gallery[$f['entity_id']] = array();

                $gallery[$f['entity_id']][] = Villa_Module_Homepage_Helper::getImageDir().$f['image'];
            }
        }

        return $gallery;
    }

}
