<?php

class Villa_Module_Homepage_View
{
    public $publication;    
    public $gallery;
    public $options;
    public $transfer;
    
    public function drawAbout()
    {
        $tplFileName = '../villa/template/villa/about.html';
        $tpl = new Dante_Lib_Template();
        
        $tpl->publication = $this->publication;
        $tpl->gallery = $this->gallery;
        $tpl->options = $this->options;
        $tpl->transfer = $this->transfer;
        
        return $tpl->draw($tplFileName);
    }
    
    public function drawHomepage(
        $homePageData,          // Информация стартовой страницы
        $homePageGallery,       // Галерея стартовой страницы
        $servicesData,           // Дополнительные услуги
        $servicesGallery,         
        $excursData, // Экскурсии
        $excursGallery,
        $transferData, // Трансферы
        $transferGallery,
        $airCompanies,          // Авиакомпании
        $airCountries,          // Страны
        $airTowns,              // Города
        $airCompaniesGallery,    // Изображения для галереи авиакомпаний
        $countryId,
        $items,
        $room_prices

    )
    {
        
        $tplFileName = '../villa/template/villa/homepage.html';
        $tpl = new Dante_Lib_Template();
        
        //$tpl->roomsTypesList = $roomsTypesList;
        $tpl->homePageData = $homePageData;
        $tpl->homePageGallery = $homePageGallery;
        $tpl->servicesData = $servicesData;
        $tpl->servicesGallery = $servicesGallery;
        $tpl->excursData = $excursData;
        $tpl->excursGallery = $excursGallery;
        $tpl->transferData = $transferData;
        $tpl->transferGallery = $transferGallery;
        $tpl->airCompanies = $airCompanies;
        $tpl->airCountries = $airCountries;
        $tpl->airTowns = $airTowns;
        $tpl->airCompaniesGallery = $airCompaniesGallery;
        $tpl->countryId = $countryId; // Текущая страна
        $tpl->items = $items; // Текущая страна
        $tpl->days = Villa_Module_Hotel_Helper_Excursion::getDays();
        $tpl->room_prices = $room_prices;



        return $tpl->draw($tplFileName);
    }
}

?>