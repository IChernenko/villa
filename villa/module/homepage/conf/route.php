<?php
/**
 * Правило маршрутизации для url раздела редактирования
 * стартовой страницы в административной части сайта.
 * (Модуль /villa/module/manage/homepage/)
 */
Dante_Lib_Router::addRule('/^\/manage\/home\-page\/$/', array(
    'controller' => 'Villa_Module_Homepage_Manage_Controller'
));

Dante_Lib_Router::addRule('/^\/manage\/excursions\/$/', array(
    'controller' => 'Villa_Module_Homepage_Manage_Excursion_Controller'
));

?>
