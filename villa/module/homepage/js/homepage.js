homepage =
{
    setMetaSeo: function(seoTitle, seoDescription, seoKeywords)
    {
        jQuery('meta[name=title]').attr('content', seoTitle);
        jQuery('meta[name=description]').attr('content', seoDescription);
        jQuery('meta[name=keywords]').attr('content', seoKeywords);
    }
}