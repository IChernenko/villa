<?php
/**
* Главный контроллер модуля Manage-Homepage (/villa/module/manage/homepage/)
*/

class Villa_Module_Homepage_Manage_Excursion_Controller extends Dante_Controller_Base
{
    /**
     * @var $_mapper - маппер модуля Manage_Homepage
     */
    protected $_mapper;

    /**
     * Инициализация переменных класса контроллера
     */
    protected function _init()
    {
        parent::_init();
        $helper = new Module_Division_Helper();
        $helper->checkAccess();
        $this->_mapper = new Villa_Module_Homepage_Manage_Mapper();
    }

    /**
     * Action выполняющийся по умолчанию
     */
    protected function _defaultAction()
    {
        // Создание маппера для языковой локали
        $langMapper = new Module_Lang_Mapper();

        // Определение ID текущей языковой локали
        $lang = $this->_getRequest()->get_validate('lang_id', 'int', 1);

        // Получение данных страницы с учетом текущей языковой версии $lang (страница, услуги, трансферы)
        $pageData = $this->_mapper->getExcursions($lang);

        /*echo('<pre>');
        print_r($pageData);
        echo('</pre>');
        die();*/
        /**
         * Создание представления (view) для раздела редактирования
         * страницы "Об Отеле" в административной части
         */
        $view = new Villa_Module_Homepage_Manage_Excursion_View();
        $view->langList = $langMapper->getList();

        return $view->drawEditor($pageData);
    }

    /**
     * Получение данных страницы из БД
     */
    protected function _drawAjaxAction()
    {
        // Возврат данных
        return array(
            'result' => 1,
            'html' => $this->_defaultAction()
        );
    }

    /**
     * Сохранение данных страницы в БД
     */
    protected function _saveAction()
    {

        $request = new Dante_Lib_Request();

        $model = new Villa_Module_Homepage_Model();
        $model->lang_id = $request->get('lang_id');
        $model->excursions_title = $request->get('excursions_title');
        $model->excursions_list = $request->get('excursions_list');
        
        return array(
            'result' => $this->_mapper->applyExcursions($model)
        );
    }

    /**
     * Отобразить изображения для галереи
     * @return array
     */
    protected function _drawImagesAction($entityType = false, $entityId = false)
    {
        if(!$entityType)
            $entityType = $this->_getRequest()->get('entity_type_id');
        
        if(!$entityId)
            $entityId = $this->_getRequest()->get_validate('entity_id', 'int', 1);
        
        $images = $this->_mapper->getImages($entityType, $entityId);
        $view = new Villa_Module_Homepage_Manage_Excursion_View();
        
        return array(
            'result' => 1,
            'html' => $view->drawImages($images, $entityType, $entityId)
        );
    }

    /**
     * Функция для загрузки изображения для галереи
     * @return array
     *
     */
    protected function _imageUploadAction()
    {
        if(!isset($_FILES['file']))
            return array(
                'result' => 0,
                'message' => 'Нет файла для загрузки'
            );
        
        $uploadDir = Villa_Module_Homepage_Helper::getImageDir();
        if (!is_dir($uploadDir)) mkdir($uploadDir, 0755, true);

        $newName = time();
        $uploader = new Lib_JqueryUpload_Uploader();
        $uploader->globalKey = 'file';
        $image = $uploader->upload($uploadDir, $newName);
        
        $entityType = $this->_getRequest()->get('entity_type_id');
        $entityId = $this->_getRequest()->get('entity_id');
        
        $this->_mapper->addImage($image, $entityType, $entityId);

        $html = $this->_drawImagesAction($entityType, $entityId);
        return $html;
    }

    /**
     * Функция для удаления изображения для галереи
     * @return array
     */
    protected function _delImageAction()
    {
        $imageId = $this->_getRequest()->get('id');
        $image = $this->_mapper->delImage($imageId);
        
        $filename = Villa_Module_Homepage_Helper::getImageDir().$image;
        if(is_file($filename)) unlink($filename);

        return array(
            'result' => 1
        );
    }

    /**
     * Функция для изменения порядка отображения изображений для галереи
     * @return array
     */
    protected function _changeImgOrderAction()
    {
        $request = $this->_getRequest();
        $entityType = $request->get('entity_type_id');
        $entityId = $request->get('entity_id');
        $imageId = $request->get('id');
        
        $drawOrder = $request->get('draw_order');
        $orderFactor = $request->get('order_factor');

        $this->_mapper->changeImgOrder($imageId, $drawOrder, $orderFactor, $entityType, $entityId);
        return $this->_drawImagesAction($entityType, $entityId);
    }
}