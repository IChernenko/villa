manage_homepage =
{
    // Главный контроллер модуля Villa_Module_Manage_About_Controller
    controller: 'villa.module.homepage.manage.controller',
    
    editors: [],

    /**
     * Обработчик изменения селектора выбора языка
     * @param object - объект селектора
     */
    changeLang: function(object)
    {
        // если объект селектора неактивный - return;
        // Отключить все элементы управления

        manage_homepage.setEditorData(jQuery(object).val());
    },

    /**
     * Вывод данных Стартовой страницы в редактор
     * @param langId - ID языковой локали
     */
    setEditorData: function(langId)
    {
        jQuery.ajax({
            type: 'POST',
            url: 'ajax.php',
            data:
            {
                controller: manage_homepage.controller,
                action: 'drawAjax',
                lang_id: langId
            },
            success: function(data)
            {
                if(data.result == 1)
                {
                    $('#homePageEditorMain').replaceWith(data.html);
                }
                else
                {
                    jAlert(data.message, 'Ошибка');
                }
            }
        });
    },

    /**
     * Инициализация редактора CKEditor
     * @param language - код языковой локали (ru, en, de ...)
     */
    initCKEditor: function(language)
    {
        for(var i=0; i<manage_homepage.editors.length; i++)
        {
            console.log(manage_homepage.editors[i]);
            CKEDITOR.replace(manage_homepage.editors[i],
            {
                language: language
            });
        }
    },

    /**
     * Фунция - обработчик нажатия кнопки "Сохранить" в разделе редактирования
     * Стартовой страницы в административной части
     */

    saveEditorData: function(object)
    {
        // Если кнопка "Сохранить" неактивна
        // return false;
        // else
        
        for(var i=0; i<manage_homepage.editors.length; i++)
        {
            var name = manage_homepage.editors[i];
            var html = CKEDITOR.instances[name].getData();
            $('textarea#'+name).val(html);
        }
        
        var data = $('#homePageEditor').serializeArray();
        console.log(data);
        data.push(
            {name: 'controller', value: manage_homepage.controller},
            {name: 'action', value: 'save'}
        );
        
        jQuery.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: data,
            success: function(data)
            {
                if(data.result == 1)
                {
                    jAlert("Версия страницы успешно сохранена", "Сообщение");
                }
                else
                {
                    jAlert(data.message, "Ошибка");
                }
            }
        });
    },



    /**
     * Вывод окна для управления изображениями галереи
     */
    editImages: function(entityType, entityId)
    {
        jQuery('#editImagesModal').modal({show: true});
        var data = {
            controller: manage_homepage.controller,
            action: 'drawImages',
            entity_type_id: entityType
        };
        
        if(entityId !== undefined) data.entity_id = entityId;
        
        jQuery.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: data,
            success: function(data)
            {
                if(data.result == 1)
                {
                    jQuery('#editImagesModal .modal-body').html(data.html);
                }
                else
                {
                    jAlert(data.message, "Ошибка");
                }
            }
        });
    },

    /**
     * Удаление изображения
     * @param object
     * @param imageId
     */
    delImage: function(object, imageId)
    {
        $.ajax({
            url: '/ajax.php',
            type: 'post',
            dataType : "json",
            data:{
                controller: manage_homepage.controller,
                action: 'delImage',
                id: imageId
            },
            success: function (data){
                if(data.result == 1)
                    $(object).closest('tr').remove();
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    },

    /**
     * Изменение очередности отображенияя изображений
     * @param imageId
     * @param drawOrder
     * @param orderFactor
     */
    changeImgOrder: function(imageId, drawOrder, orderFactor)
    {
        $.ajax({
            url: '/ajax.php',
            dataType: "json",
            type: 'post',
            data:{
                controller: manage_homepage.controller,
                action: 'changeImgOrder',
                entity_type_id: $('#imageEntityType').val(),
                entity_id: $('#imageEntityId').val(),
                id: imageId,
                draw_order: drawOrder,
                order_factor: orderFactor
            },
            success: function (data){
                if(data.result == 1)
                {
                    jQuery('#editImagesModal .modal-body').html(data.html);
                }
                else
                {
                    jAlert(data.message, 'Ошибка');
                }

            }
        });
    }
}