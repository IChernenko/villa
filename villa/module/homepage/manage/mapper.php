<?php
/**
 * Class Villa_Module_Manage_Homepage_Mapper
 * Маппер модуля Homepage (для работы с базой данных)
 */
class Villa_Module_Homepage_Manage_Mapper
{
    protected $_tableName = '[%%]villa_homepage';
    protected $_servicesTable = '[%%]villa_homepage_services';
    protected $_excursionsTable = '[%%]villa_homepage_excursions';
    protected $_transferTable = '[%%]villa_homepage_transfer';

    protected $_tableGallery = '[%%]villa_homepage_gallery';

    /**
     * Функция, получающая запись из таблицы $_tableName, $_optionsTableName, $_transferTableName
     * @param $langId - ID языка
     * @return array - данные записи
     */
    public function get($langId)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->select(array('lang_id' => $langId));
        
        $pageInfo = new Villa_Module_Homepage_Model();
        $pageInfo->lang_id = $langId;
        $pageInfo->title = $table->title;
        $pageInfo->html = $table->html;
        $pageInfo->full_descr = $table->full_descr;
        $pageInfo->seo_title = $table->seo_title;
        $pageInfo->seo_keywords = $table->seo_keywords;
        $pageInfo->seo_description = $table->seo_description;
        $pageInfo->ex_total_title = $table->ex_total_title;
        $pageInfo->ex_total_desc = $table->ex_total_desc;

        
        $tableExcursions = new Dante_Lib_Orm_Table($this->_excursionsTable);
        $tableExcursions->get()->where(array('lang_id' => $langId))->limit(array(0, 1));
        $pageInfo->excursions_title = $tableExcursions->fetch()->title;
        
        $sql = "SELECT `day`, `html`, `full_descr` FROM {$this->_excursionsTable} WHERE `lang_id` = {$langId}";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $excursList = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
            $excursList[$f['day']] = $f;
        
        $days = Villa_Module_Hotel_Helper_Excursion::getDays();
        foreach(array_keys($days) as $day)
            $pageInfo->excursions_list[$day] = isset($excursList[$day])?$excursList[$day]:array('html' => '', 'full_descr' => '');
        
        $tableTransfer = new Dante_Lib_Orm_Table($this->_transferTable);
        $tableTransfer->select(array('lang_id' => $langId));
        $pageInfo->transfer_title = $tableTransfer->title;
        $pageInfo->transfer_html = $tableTransfer->html;
        $pageInfo->transfer_full_descr = $tableTransfer->full_descr;
        
        return $pageInfo;
    }
    
    public function getExcursions($langId)
    {
        $pageInfo = new Villa_Module_Homepage_Model();
        $pageInfo->lang_id = $langId;
        
        $sql = "SELECT `day`, `html`, `full_descr`, `max_guests`, `outer_id`, `title`, `ex_show` FROM {$this->_excursionsTable} WHERE `lang_id` = {$langId}";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $excursList = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
            $excursList[$f['day']] = $f;
        
        $days = Villa_Module_Hotel_Helper_Excursion::getDays();
        foreach(array_keys($days) as $day)
            $pageInfo->excursions_list[$day] = isset($excursList[$day])?$excursList[$day]:array('html' => '', 'full_descr' => '', 'max_guests' => '', 'outer_id' => '', 'title'=>'');
        return $pageInfo;
    }

    /**
     * Функция, изменяющая запись в таблицах $_tableName и $_transferTableName
     * @param $langId - ID языка
     * @param $pageTitle - заголовок стартовой страницы
     * @param $pageHtml - html стартовой страницы
     * @param $transferTitle - заголовок блока трансферов
     * @param $transferHtml - html содержимое блока трансферов
     * @return mixed - результат выполнения запроса к DB
     */
    public function apply(Villa_Module_Homepage_Model $model)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->lang_id = $model->lang_id;
        $table->title = $model->title;
        $table->html = $model->html;
        $table->full_descr = $model->full_descr;
        $table->seo_title = $model->seo_title;
        $table->seo_description = $model->seo_description;
        $table->seo_keywords = $model->seo_keywords;
        $table->ex_total_title = $model->ex_total_title;
        $table->ex_total_desc = $model->ex_total_desc;

        if($table->exist(array('lang_id' => $model->lang_id)))
            $table->update(array('lang_id' => $model->lang_id));
        else
            $table->insert();
        
        $table = new Dante_Lib_Orm_Table($this->_transferTable);
        $table->lang_id = $model->lang_id;
        $table->title = $model->transfer_title;
        $table->html = $model->transfer_html;
        $table->full_descr = $model->transfer_full_descr;


        if($table->exist(array('lang_id' => $model->lang_id)))
            $table->update(array('lang_id' => $model->lang_id));
        else
            $table->insert();

        return true;
    }
    
    public function applyExcursions(Villa_Module_Homepage_Model $model)
    {
        //print_r($model); die();
        foreach($model->excursions_list as $day => $info)
        {
            $table = new Dante_Lib_Orm_Table($this->_excursionsTable);
            $table->lang_id = $model->lang_id;
            $table->day = $day;
            $table->title = $model->excursions_title[$day];

            $table->html = $info['html'];
            $table->full_descr = $info['full_descr'];
            $table->max_guests = (int)$info['max_guests'];
            $table->outer_id = (int)$info['outer_id'];
            if ( isset($info['ex_show']) ) {
                $table->ex_show = 1;
            } else {
                $table->ex_show = 0;
            }
            $tableTest = new Dante_Lib_Orm_Table($this->_excursionsTable);
            for ( $i = 0; $i < 13;  $i++ ) {
                $tableTest->lang_id = $i;
                $tableTest->day = $day;
                $tableTest->max_guests = (int)$info['max_guests'];
                $tableTest->outer_id = (int)$info['outer_id'];
                $tableTest->ex_show = $table->ex_show;

                if($tableTest->exist(array('lang_id' => $i, 'day' => $day)))
                    $tableTest->update(array('lang_id' => $i, 'day' => $day));
                else
                    $tableTest->insert();


            }
            if($table->exist(array('lang_id' => $model->lang_id, 'day' => $day)))
                $table->update(array('lang_id' => $model->lang_id, 'day' => $day));
            else
                $table->insert();
        }

        return true;
    }

    /**
     * Выбрать изображения для галереи
     * @return Dante_Lib_Orm_Table
     */
    public function getImages($entityType, $entityId = 1)
    {      
        $sql = "SELECT * FROM {$this->_tableGallery} 
                WHERE `entity_type_id` = {$entityType} AND `entity_id` = {$entityId} 
                ORDER BY draw_order ASC";

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $images = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $images[] = $f;
        }
        return $images;
    }

    public function addImage($image, $entityType, $entityId = 1)
    { 
        $table = new Dante_Lib_Orm_Table($this->_tableGallery);
        
        $drawOrder = $this->_getLastImgOrder($entityType, $entityId)+1;
        
        $table->entity_type_id = $entityType;
        $table->entity_id = $entityId;
        $table->image = $image;
        $table->draw_order = $drawOrder;
        
        $table->insert();
    }

    public function delImage($imageId)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableGallery);  
        $image = $table->select(array('id' => $imageId));
        $table->delete(array('id' => $imageId));
        
        return $image;
    }

    public function changeImgOrder($imageId, $drawOrder, $orderFactor, $entityType, $entityId = 1)
    {
        $switchRowId = $this->_getImageIdByDrawOrder($drawOrder+$orderFactor, $entityType, $entityId);
        
        if(!$switchRowId) return;
        
        $table = new Dante_Lib_Orm_Table($this->_tableGallery);
        $table->draw_order = $drawOrder+$orderFactor;
        $table->apply(array(
            'id' => $imageId,
        ));

        $table = new Dante_Lib_Orm_Table($this->_tableGallery);
        $table->draw_order = $drawOrder;        
        $table->apply(array(
            'id' => $switchRowId,
        ));
    }

    protected function _getImageIdByDrawOrder($drawOrder, $entityType, $entityId = 1)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableGallery);
        $table->get()->where(array(
                'draw_order'=>$drawOrder,
                'entity_type_id' => $entityType,
                'entity_id' => $entityId
            ))->limit(array(0, 1))->orderby('draw_order desc');
        
        $table->fetch();

        return $table->id;
    }

    protected function _getLastImgOrder($entityType, $entityId = 1)
    {
        $sql = "SELECT `draw_order` FROM {$this->_tableGallery} 
                WHERE `entity_type_id` = {$entityType} AND `entity_id` = {$entityId}
                ORDER BY `draw_order` DESC LIMIT 1";
        return Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'draw_order');
    }

    protected function _getExcursionLastImgOrder($day)
    {
        $sql = "SELECT `draw_order` FROM {$this->_excursionsImagesTable} WHERE `day` = {$day}
                ORDER BY `draw_order` DESC LIMIT 1";
        
        return Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'draw_order');
    }
}