<?php
/**
    * Class Villa_Module_Manage_Homepage_View
    * Класс представления модуля Manage_Homepage
    */

class Villa_Module_Homepage_Manage_View
{
    public $langList = array();
    public $dayList = array();

    public function drawEditor($pageData)
    {
        $tpl = new Dante_Lib_Template();
        $filePath = '../villa/module/homepage/manage/template/form.html';
        $tpl->model = $pageData;
        $tpl->langList = $this->langList;
        $tpl->dayList = Villa_Module_Hotel_Helper_Excursion::getDays();
        //constants
        $tpl->mainEntity = Villa_Helper_Entity::getHomepage();
        $tpl->transferEntity = Villa_Helper_Entity::getTransfer();
        $tpl->excursionEntity = Villa_Helper_Entity::getExcursion();
        return $tpl->draw($filePath);
    }

    public function drawImages($images, $entityType, $entityId)
    {         
        $filePath = '../villa/module/homepage/manage/template/form_img.html';
        $tpl = new Dante_Lib_Template();
        $tpl->images = $images;
        $tpl->entityType = $entityType;
        $tpl->entityId = $entityId;

        return $tpl->draw($filePath);
    }
}

?>
