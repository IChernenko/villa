RENAME TABLE `[%%]villa_home_page` TO `[%%]villa_homepage`;
RENAME TABLE `[%%]villa_home_page_images` TO `[%%]villa_homepage_images`;
RENAME TABLE `[%%]villa_home_page_options` TO `[%%]villa_homepage_services`;
RENAME TABLE `[%%]villa_home_page_options_images` TO `[%%]villa_homepage_services_images`;
RENAME TABLE `[%%]villa_home_page_transfer` TO `[%%]villa_homepage_transfer`;
RENAME TABLE `[%%]villa_home_page_transfer_images` TO `[%%]villa_homepage_transfer_images`;

CREATE TABLE `[%%]villa_homepage_excursions`(
    `lang_id` INT(11) UNSIGNED NOT NULL,
    `day` INT(11) NOT NULL,
    `title` VARCHAR(255),
    `html` TEXT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `[%%]villa_homepage_excursions` ADD CONSTRAINT `fk_villa_home_page_excursions_lang_id`
    FOREIGN KEY (`lang_id`) REFERENCES `[%%]languages`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE `[%%]villa_homepage_excursions_images`(
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `day` INT(11) NOT NULL,
    `image` CHAR(30) NOT NULL,
    `draw_order` TINYINT(2) NOT NULL,
    PRIMARY KEY(`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;