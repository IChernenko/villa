CREATE TABLE
    `[%%]villa_home_page_transfer_images`
    (
        `id` INT(11) NOT NULL AUTO_INCREMENT,
        `image` CHAR(30) NOT NULL,
        `draw_order` TINYINT(2) NOT NULL,
        PRIMARY KEY(`id`)
    )
    ENGINE=InnoDB DEFAULT CHARSET=utf8;