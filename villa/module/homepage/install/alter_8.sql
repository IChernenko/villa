CREATE TABLE `[%%]villa_home_page_options`(
    `lang_id` INT(11) UNSIGNED NOT NULL,
    `title` VARCHAR(255),
    `html` TEXT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `[%%]villa_home_page_options` ADD CONSTRAINT `fk_villa_home_page_options_lang_id`
    FOREIGN KEY (`lang_id`) REFERENCES `[%%]languages`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE `[%%]villa_home_page_options_images`(
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `image` CHAR(30) NOT NULL,
    `draw_order` TINYINT(2) NOT NULL,
    PRIMARY KEY(`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;