SELECT @version:=`version` FROM `db_version` WHERE `package` = 'villa.module.manage.homepage';
SELECT @res:=IF(@version, @version, 1);
UPDATE `db_version` SET `version` = @res WHERE `package` = 'villa.module.homepage';
DELETE FROM `db_version` WHERE `package` = 'villa.module.manage.homepage';

/*
 * Создание таблицы tst_villa_home_page для хранения данных о странице
 * (Заголовок, html-код, изображения для галереи) на всех существующих
 * на сайте языках. (По одной версии для каждого языка)
*/
CREATE TABLE `[%%]villa_home_page`(
    `lang_id` INT(11) UNSIGNED NOT NULL,
    `title` VARCHAR(255),
    `html` TEXT,
    `seo_title` TEXT,
    `seo_description` TEXT,
    `seo_keywords` TEXT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `[%%]villa_home_page` ADD CONSTRAINT `fk_villa_home_page_lang_id`
    FOREIGN KEY (`lang_id`) REFERENCES `[%%]languages`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;