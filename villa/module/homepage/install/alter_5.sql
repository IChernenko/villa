/*
 * Создание таблицы tst_villa_home_page_transfer для хранения данных о nhfycathf[
 * (Заголовок, html-код, изображения для галереи) на всех существующих
 * на сайте языках. (По одной версии для каждого языка)
*/
CREATE TABLE `[%%]villa_home_page_transfer`(
    `lang_id` INT(11) UNSIGNED NOT NULL,
    `title` VARCHAR(255),
    `html` TEXT

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `[%%]villa_home_page_transfer` ADD CONSTRAINT `fk_villa_home_page_transfer_lang_id`
    FOREIGN KEY (`lang_id`) REFERENCES `[%%]languages`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;