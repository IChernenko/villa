<?php
/**
 * Description of model
 *
 * @author Valkyria
 */
class Villa_Module_Homepage_Model 
{
    public $lang_id;
    
    public $title;
    
    public $html;
    
    public $full_descr;
    
    public $seo_title;
    
    public $seo_keywords;
    
    public $seo_description;

    public $services_title;
    
    public $services_html;
    
    public $services_full_descr;

    public $ex_total_title;

    public $ex_total_desc;

    public $excursions_title;

    public $excursions_list;
    
    public $transfer_title;
    
    public $transfer_html;
    
    public $transfer_full_descr;
}

?>
