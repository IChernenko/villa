<?php
/**
 * Description of helper
 *
 * @author Valkyria
 */
class Villa_Module_Homepage_Helper 
{
    public static function getImageDir()
    {
        $upload = Dante_Lib_Config::get('app.uploadFolder');
        $dir = Dante_Lib_Config::get('hotel.homePageFolder');
        return "{$upload}/{$dir}/";
    }
}

?>
