<?php
/**
 * Модуль, реализующий вывод Стартовой страницы
 * в клиентской части сайта
 * Path: /villa/module/homepage/
 * Name: villa.module.homepage
 * Current version: 1
 */
global $package;
$package = array(
    'version' => '10',
    'name' => 'villa.module.homepage',
    'dependence' => array(),
    'js' => array(
        '../villa/module/homepage/js/homepage.js',
        '../villa/module/homepage/manage/js/manage_homepage.js'
    ),
    'css' => array(
        'ws' => array(
            'admin' => array(
                '../villa/module/homepage/manage/css/manage_homepage.css'
            )
        )
    )
);
?>