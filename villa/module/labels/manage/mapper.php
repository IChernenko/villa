<?php

/**
 * Description of mapper
 *
 * @author Valkyria
 */
class Villa_Module_Labels_Manage_Mapper extends Module_Labels_Manage_Mapper {

    public function getAll($langList) {
        $this->paginator = false;
        $this->hasPrimary = false;
        $list = array();
        if (!is_array($langList) || count($langList) == 0)
            return $list;

        $join = "LEFT JOIN {$this->_tableNameLang} AS `t2` ON(`t1`.`id` = `t2`.`id`)";
        $tbl = $this->getRowsByParams(false, "*", $join);

        foreach ($tbl['rows'] as $row) {
            if (!isset($list[$row['id']])) {
                $list[$row['id']] = array(
                    'id' => $row['id'],
                    'sys' => $row['sys_name']
                );
            }

            $list[$row['id']][$langList[$row['lang_id']]] = $row['name'];
        }

        return $list;
    }

    public function updateAll($langList) {
        foreach ($langList as $p => $labelsItem) {
            if ($labelsItem[0] == 0) { // пропуск заголовков

                continue;
            }
            $id = $labelsItem[0];
            foreach ($labelsItem as $p1 => $labelsItemText) {
                if ($p1 < 2) { // пропуск id и sys_name
                    continue;
                }
                $lang_id = $p1 - 1;
//                $lang_id = $p1;

                self::_updateLabel($id, $lang_id, $labelsItemText);
            }
        }

        return true;
    }

    private function _updateLabel($id, $lang_id, $text) {
        $table = new Dante_Lib_Orm_Table($this->_tableNameLang);
        $table->id = $id;
        $table->lang_id = $lang_id;
        $table->name = $text;


        $cond = array(
            'id' => $id,
            'lang_id' => $lang_id,
            'name' => $text
        );

        if($table->exist($cond)) {
            $table->update($cond);
        } else {
            $table->insert();
        }




//        $table->update(array('id' => $id, 'lang_id' => $lang_id));
//        $table->insert();
    }

}
