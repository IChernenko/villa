<?php

/**
 * Description of controller
 *
 * @author Valkyria
 */
class Villa_Module_Labels_Manage_Controller extends Module_Labels_Manage_Controller {

    protected function _init() {
        parent::_init();
        $this->_mapper = new Villa_Module_Labels_Manage_Mapper();
    }

    protected function _instanceView() {
        return new Villa_Module_Labels_Manage_Veiw();
    }

    protected function _drawAction() {
        return $this->_instanceView()->draw() . parent::_drawAction();
    }

    protected function _exportAction() {
        $langs = $this->_getLangList();
        $list = $this->_mapper->getAll($langs);

        Villa_Module_Labels_Manage_Service::getCSVFile($langs, $list);
    }

    protected function _getLangList() {
        $langMapper = new Module_Lang_Mapper();
        return $langMapper->getCodesList();
    }

    protected function _importAction() {
//        $fileDataArr = Villa_Module_Labels_Manage_Service::parseCSV();
        $fileDataArr = Villa_Module_Labels_Manage_Service::parseXLS();


        $result = $this->_mapper->updateAll($fileDataArr);

        if (!$result) {
            return array(
                'result' => 0,
                'message' => 'Нет файла для загрузки'
            );
        }

        return array(
            'result' => 1,
            'message' => 'Метки (' . count($fileDataArr) . ')  были обновлены'
        );
    }

}
