<?php

/**
 * Description of service
 *
 * @author Valkyria
 */
class Villa_Module_Labels_Manage_Service {

    private static $_dir = 'tmp/';
    private static $_filename = 'labels.csv';

    private static function _getDir() {
        if (!is_dir(self::$_dir))
            mkdir(self::$_dir, 0755);
        return self::$_dir;
    }

    private static function _getFileName() {
        return self::_getDir() . self::$_filename;
    }

    private static function _uploadFile() {
        if (!isset($_FILES['file']))
            return false;

        $uploader = new Lib_JqueryUpload_Uploader();
        $uploader->globalKey = 'file';

        $file = $uploader->upload(self::_getDir());
        return self::_getDir() . $file;
    }

    public static function parseCSV() {
        $file = self::_uploadFile();
        $parsed = array();

        /* $handle = fopen($file, "r");
          while (($data = fgetcsv($handle, 1000, ";")) !== false) {
          $parsed[] = $data;
          }
          fclose($handle); */

        header('Content-type: text/html; charset=utf-8');
        if (!setlocale(LC_ALL, 'ru_RU.utf8'))
            setlocale(LC_ALL, 'en_US.utf8');
        if (setlocale(LC_ALL, 0) == 'C')
            die('Не поддерживается ни одна из перечисленных локалей (ru_RU.utf8, en_US.utf8)');

        $handle = fopen('php://memory', 'w+');
        fwrite($handle, iconv('CP1251', 'UTF-8', file_get_contents($file)));
        rewind($handle);
        while (($data = fgetcsv($handle, 1000, ';')) !== false)
            $parsed[] = $data;
        fclose($handle);

        return $parsed;
    }

    public static function parseXLS() {
        $file = self::_uploadFile();


        require_once "PHPExcel.php";

        $ar=array();
        $inputFileType = PHPExcel_IOFactory::identify($file);  // узнаем тип файла, excel может хранить файлы в разных форматах, xls, xlsx и другие
        $objReader = PHPExcel_IOFactory::createReader($inputFileType); // создаем объект для чтения файла
        $objPHPExcel = $objReader->load($file); // загружаем данные файла в объект
        $ar = $objPHPExcel->getActiveSheet()->toArray(); // выгружаем данные из объекта в массив
        return $ar; //возвращаем массив


    }

    public static function getCSVFile($langs, $labels) {
        self::_prepareLabelsArr($labels, $langs);

        //$dir = self::_getDir();

        foreach ($labels as $p => $labelsItem) {
            foreach ($labelsItem as $p1 => $labelsItemText) {
                $labels[$p][$p1] = self::_toWindowString($labelsItemText);
            }
        }

        $handle = fopen(self::_getFileName(), 'w');
        foreach ($labels as $row) {
            fputcsv($handle, $row, ";");
        }

        fclose($handle);
        self::_send();
    }

    private static function _toWindowString($ii) {
        return iconv("utf-8", "windows-1251", $ii);
    }

    private static function _prepareLabelsArr(&$labels, $langs) {
        foreach ($labels as &$labelRow) {
            $labelRowTmp = array(
                $labelRow['id'],
                $labelRow['sys']
            );

            foreach ($langs as $code) {
                $labelRowTmp[] = isset($labelRow[$code]) ? $labelRow[$code] : '';
            }

            $labelRow = $labelRowTmp;
        }

        // формируем заголовок файла - добавляем 2 значения
        array_unshift($langs, 'sys');
        array_unshift($langs, 'id');

        // добавляем заголовки в общий массив
        array_unshift($labels, $langs);
    }

    private static function _send() {
        $file = self::_getFileName();

        if (ob_get_level()) {
            ob_end_clean();
        }
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename($file));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        readfile($file);

        exit;
    }

}
