<?php

/**
 * Description of controller
 *
 * @author Nika
 */
class Villa_Module_Division_Controller extends Dante_Controller_Base
{
    protected function _instanceMapper()
    {
        return new Villa_App_Division_Mapper();
    }

    protected function _defaultAction()
    {
        $userHelper = new Module_Division_Helper();
        $groupId = $userHelper->getGroupId();
        if (is_null($groupId) ) 
        {
            if(!Dante_Helper_App::getUid()) return '';
            return 'Не задана группа для пользователя';
        }
        
        $divisionsMapper = new Module_Division_Manage_Mapper();
        $divisions = $divisionsMapper->getAvailableDivisions($groupId);
        
        $url = Dante_Controller_Front::getRequest()->get_requested_url();
        
        $view = new Villa_Module_Division_View();
        return $view->draw($divisions, $url);
    }
}
