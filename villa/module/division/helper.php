<?php

/**
 * Description of helper
 *
 * @author Nika
 */
class Villa_Module_Division_Helper 
{
    public static function getDivisionClassName($classId)
    {
        $names = array(
            DIVISION_CLASS_MAIN => 'ОСНОВНОЕ',
            DIVISION_CLASS_FIN => 'ФИНАНСОВОЕ УПРАВЛЕНИЕ',
            DIVISION_CLASS_INFO => 'ИНФОРМАЦИОННОЕ НАПОЛНЕНИЕ'
        );
        
        return $names[$classId];
    }
}
