<?php

$divisions = array(    
    '/manage/loading/',
    '/manage/booking/',
    '/manage/booking-transfer/',
    '/manage/comment/',
    '/manage/questions/',
    '/manage/villauser/clients',
    '/manage/villauser/touragents',
    '/manage/villauser/admins',   
    '/manage/currency/',
    '/manage/hotelprices/',
    '/manage/bookingprepayment/',
    '/manage/liqpay2-settings/',
    '/manage/paypal-logs/',
    '/manage/liqpay2/',    
    '/manage/rooms/',
    '/manage/roomtypes/',
    '/manage/infoblock/',
    '/manage/equipment/',
    '/manage/home-page/',
    '/manage/about-page/',
    '/manage/bookinginfo/',
    '/manage/mail-templates/',
    '/manage/contacts/',    
    '/manage/services/',
    '/manage/transfer/',    
    '/manage/countries/',
    '/manage/currencytowns/',
    '/manage/airlines/',    
    '/manage/labels/',
    '/manage/languages/',
    '/manage/slider/',
    '/manage/settings/'
);

foreach($divisions as $k => $link)
{
    $order = $k+1;
    $sql = "UPDATE `[%%]division` SET `draw_order` = {$order} WHERE `link` = '{$link}'";
    Dante_Lib_SQL_DB::get_instance()->exec($sql);
}

