ALTER TABLE  `[%%]division` ADD  `d_class` INT( 4 ) NULL DEFAULT NULL;

UPDATE `[%%]division` SET `d_class` = 1 WHERE `link` IN (
    '/manage/booking/',
    '/manage/loading/',
    '/manage/comment/',
    '/manage/questions/',
    '/manage/booking-transfer/',
    '/manage/villauser/clients',
    '/manage/villauser/touragents',
    '/manage/villauser/admins'
);

UPDATE `[%%]division` SET `d_class` = 2 WHERE `link` IN (
    '/manage/bookingprepayment/',
    '/manage/currency/',
    '/manage/hotelprices/',
    '/manage/paypal-logs/',
    '/manage/liqpay2/',
    '/manage/liqpay2-settings/'
);

UPDATE `[%%]division` SET `d_class` = 3 WHERE `link` IN (
    '/manage/rooms/',
    '/manage/roomtypes/',
    '/manage/services/',
    '/manage/equipment/',
    '/manage/countries/',
    '/manage/labels/',
    '/manage/languages/',
    '/manage/currencytowns/',
    '/manage/slider/',
    '/manage/transfer/',
    '/manage/airlines/',
    '/manage/settings/',
    '/manage/infoblock/',
    '/manage/about-page/',
    '/manage/home-page/',
    '/manage/bookinginfo/',
    '/manage/mail-templates/',
    '/manage/contacts/'
);

UPDATE `[%%]division` SET `is_active` = 0 WHERE `link` IN(
    '/manage/cashcontrol/',
    '/manage/cashgendir/'
);
