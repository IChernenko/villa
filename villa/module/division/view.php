<?php

/**
 * Description of view
 *
 * @author Nika
 */
class Villa_Module_Division_View 
{
    public function draw($divisions, $url)
    {
        $list = array();
        
        foreach($divisions as $div)
        {
            if(empty($div['d_class'])) continue;
            
            if(!isset($list[$div['d_class']]))
            {
                $list[$div['d_class']] = array(
                    'caption' => Villa_Module_Division_Helper::getDivisionClassName($div['d_class']),
                    'items' => array()
                );
            }
                
            $list[$div['d_class']]['items'][$div['id']] = $div;
        }
        
        ksort($list);        
        $tplFileName = '../villa/module/division/template/menu.html';   
        
        $tpl = new Dante_Lib_Template();
        $tpl->list = $list;
        
        return $tpl->draw($tplFileName);
    }
}
