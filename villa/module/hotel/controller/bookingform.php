﻿<?php

class Villa_Module_Hotel_Controller_Bookingform extends Dante_Controller_Base
{    
    protected $_bookingId;
    
    protected $_editMode = false;

    protected $_formUpdate = false;
    
    protected $test = '';

    protected $_skipModel = false;
    /**
     * Модель бронирования
     * @var Villa_Module_Hotel_Model_Booking 
     */
    protected $_model;
    
    /**
     * Параметры опций бронирования, полученные от пользователя
     * (напр. кол-во дней, для трансферов - даты и время и т.д.)
     */
    protected $_serviceParams = array();
    protected $_excursParams = array();
    protected $_transferParams = array();


    /**
     * Модель списка цен
     * @var Villa_Module_Hotel_Model_Manage_Priceslist 
     */
    protected $_priceList;

    protected function _init() 
    {
        parent::_init();
        $this->_checkSkipModel();
        if(!$this->_skipModel)
        {        
            $this->_setId();
            $this->_setModel();
            $this->_setPrices();
        }
    }
    
    protected function _checkSkipModel()
    {        
        $url = Dante_Lib_Request::get_requested_url();  
        if(strpos($url, 'ajax.php?skip_model=1')) $this->_skipModel = true;
    }

    protected function _setId($id = false)
    {  
        if(!$id)
        {     
            $url = Dante_Lib_Request::get_requested_url();    
            $matches = array();
            if(preg_match('/mybookings\/edit\-(\d+)/', $url, $matches) || preg_match('/ajax\.php\?booking_id\=(\d+)\&edit_mode\=1/', $url, $matches))
            {
                $id = $matches[1];
                $this->_editMode = true;
            }
        }
        if(!$id) $id = Villa_Module_Hotel_Helper_Session::getBookingId();
        
        $this->_bookingId = $id;
    }

    protected function _setModel()
    {
        if($this->_bookingId)
        {
            $this->_model = $this->_instanceMapper()->getById($this->_bookingId, false);
        }
        else
        {
            $this->_model = new Villa_Module_Hotel_Model_Booking();
            $this->_model->basicData = Villa_Module_Hotel_Helper_Session::getBookingBase();
            $this->_model->roomData = Villa_Module_Hotel_Helper_Session::getBookingRoom();
            $this->_model->init();
        }
    }
    
    protected function _instanceMapper()
    {
        return new Villa_Module_Hotel_Mapper_Booking();
    }
    
    protected function _setPrices()
    {        
        $pricesMapper = new Villa_Module_Hotel_Mapper_Manage_Prices();
        $this->_priceList = $pricesMapper->getCurrentPriceList($this->_model->basicData->arrivalToInt());
    }

    protected function _updateFormAction()
    {
        $this->_formUpdate = true;

        $this->_fillModelFromRequest();

        return array(
            'result' => 1,
            'html' => $this->_defaultAction(true)
        );
    }
    
    protected function _fillModelFromRequest()
    {
        $this->_setOptionParams();

        $request = $this->_getRequest();
        $this->_model->comment = $request->get('comment');

        $this->_model->basicData->adults = 0;
        $this->_model->basicData->children = 0;

        $adultsInfo = $request->get('adultsInfo');
        //print_r($adultsInfo); die();
        foreach($adultsInfo as $num => $adult)
        {
            $adultModel = new Villa_Module_Hotel_Model_Booking_Adult();
            $adultModel->enabled = $adult['enabled'];
            if (isset($adult['first_name'])) {
                $adultModel->first_name = $adult['first_name'];
            }

            if (isset($adult['last_name'])) {
                $adultModel->last_name = $adult['last_name'];
            }

            if (isset($adult['phone'])) {
                $adultModel->phone = $adult['phone'];
            }

            if (isset($adult['email'])) {
                $adultModel->email = $adult['email'];
            }


            if($adult['enabled']) $this->_model->basicData->adults++;

            $this->_model->adultsInfo[$num] = $adultModel;
        }

        $childrenInfo = $request->get('childrenInfo');
        if(!$childrenInfo) $childrenInfo = array();

        foreach($childrenInfo as $num => $child)
        {
            $childModel = new Villa_Module_Hotel_Model_Booking_Child();
            $childModel->enabled = $child['enabled'];
            $childModel->first_name = $child['first_name'];
            $childModel->last_name = $child['last_name'];

            if($child['enabled']) $this->_model->basicData->children++;

            $this->_model->childrenInfo[$num] = $childModel;
        }

        foreach($this->_serviceParams as $id => &$service)
        {
            $this->_model->addService($id, $service, false);
        }


        if(is_array($this->_excursParams) || is_object($this->_excursParams)) {
            foreach ($this->_excursParams as $date => &$excursion) {
                $this->_model->addExcursion($date, $excursion, false);
            }
        }
        foreach($this->_transferParams as $type => $transfer)
        {
            $this->_model->addTransfer($type, $transfer, false);
        }

        $this->_model->totalsData->recalc();
    }

    /**
     * Устанавливает параметры опций бронирования, полученные от пользователя
     */
    protected function _setOptionParams()
    {
        $this->_serviceParams = $this->_getRequest()->get('services');
        $this->_excursParams = $this->_getRequest()->get('excursions');
        $this->_transferParams = $this->_getRequest()->get('transfers');
    }

    protected function _defaultAction($update = false)
    {
        if(!$this->_model->basicData || !$this->_model->roomData)
        {
            header('Location: /');
        }

        if($this->_bookingId && !$this->_formUpdate)
        {
            $this->_model = $this->_instanceMapper()->getAdditinalModelData($this->_model);
        }

        $currency = Module_Currency_Helper::getCurrentCurrency();

        $mapper = new Villa_Module_Hotel_Mapper_Roomtypes();
        $prepaymentMapper = new Villa_Module_Hotel_Mapper_Prepayment();
        $optionsMapper = new Villa_Module_Hotel_Mapper_Bookingoptions();

        $view = $this->_instanceView();

        $userMapper = new Villa_Module_User_Manage_Mapper();
        $uid = (int)Dante_Helper_App::getUid();
        if($uid)
        {
            $user = $userMapper->getUser($uid);

            if($user->group_id != Villa_Module_User_Helper::getTourAgentGroupId() && (!$this->_getRequest() || !$this->_getRequest()->get('adultsInfo')))
            {
                $this->_model->adultsInfo[1]->first_name = $user->first_name;
                $this->_model->adultsInfo[1]->last_name = $user->last_name;
                $this->_model->adultsInfo[1]->email = $user->email;
                $this->_model->adultsInfo[1]->phone = $user->phone;
            }
        }
        else $user = false;

        $roomTypes = $mapper->getNamesList();
        $prepaymentTypes = $prepaymentMapper->getNamesList();

        // Список доступных услуг
        $services = $optionsMapper->getServices();

        // Список экскурсий по дням
        $excursions = Villa_Module_Hotel_Helper_Excursion::getListForPeriod(
                $this->_model->basicData->arrivalToInt(),
                $this->_model->days + 1,
                $this->_model->id
        );

        // Трансферы
        $transfers = $optionsMapper->getTransfers();

        foreach($transfers as $id => &$transfer)
        {
            $transfer['price'] = Module_Currency_Helper::convert($this->_priceList->getTransferPrice($id));
        }

        if(!$update)
            $this->_fillModelDefault($services, $excursions);

        $this->_model->totalsData->recalc();

        $view->setRoomTypeName($roomTypes[$this->_model->roomData->type_id]);
        $view->setPrepaymentName($prepaymentTypes[$this->_model->roomData->prepayment]);

        $view->user = $user;
        //$currency['reduction'] = 'UAH';
        $view->setCurrencyCode($currency['reduction']);

        $view->services = $services;
        $view->excursions = $excursions;
        $view->transfers = $transfers;

        $viewMode = BF_VIEW_MODE_NEW;
        if($this->_editMode)
        {
            $viewMode = BF_VIEW_MODE_EDIT;
        }
        if(Villa_Module_Hotel_Helper_Session::getPaymentStatus())
        {
            $viewMode = BF_VIEW_MODE_AFTER_PAYMENT;
        }

        $view->setMode($viewMode);
        $view->gallery = $this->_getBookingformGallery($services);
        $view->bottomText = $this->_getBookingInfo();

        return $view->draw($this->_model);
    }
    
    protected function _instanceView()
    {
        return new Villa_Module_Hotel_View_Bookingform();
    }

    protected function _fillModelDefault($services, $excursions)
    {
        foreach($services as $id => $service)
        {
            if($this->_model->serviceExists($id)) continue;

            $service['enabled'] = 0;
            $service['days'] = $this->_model->days;
            $service['adults'] = $this->_model->basicData->adults;
            $service['children'] = $this->_model->basicData->children;

            $this->_model->addService($id, $service, false);
        }

        foreach($excursions as $date => $excursion)
        {
            if($this->_model->excursionExists($date)) continue;

            $excursion['enabled'] = 0;
            $excursion['adults'] = $this->_model->basicData->adults;
            $excursion['children'] = $this->_model->basicData->children;

            $this->_model->addExcursion($date, $excursion, false);
        }
    }
    
    protected function _getBookingformGallery($servicesList = false)
    {
        $homepageMapper = new Villa_Module_Homepage_Mapper();

        $excursImgs = $homepageMapper->getExcursionsImages();
        $firstExcursImage = Lib_Image_Helper::getNoImage();
        if(is_array($excursImgs))
        {
            foreach($excursImgs as $daySet)
            {
                if(is_array($daySet) && isset($daySet[0]))
                {
                    $firstExcursImage = $daySet[0];
                    break;
                }
            }
        }

        $transferImgs = $homepageMapper->getTransferImages();
        $firstTransferImg = Lib_Image_Helper::getNoImage();
        if(is_array($transferImgs) && isset($transferImgs[0]))
        {
            $firstTransferImg = $transferImgs[0];
        }

        $optionsMapper = new Villa_Module_Hotel_Mapper_Bookingoptions();
        $serviceImgs = $optionsMapper->getServicesImages();

        if(!$servicesList) $servicesList = $optionsMapper->getServices();
        $servicesIdList = array_keys($servicesList);

        $firstServicesImgs = array();
        foreach($servicesIdList as $id)
        {
            $firstServicesImgs[$id] = isset($serviceImgs[$id]) && isset($serviceImgs[$id][0]) ?
                                    $serviceImgs[$id][0] : Lib_Image_Helper::getNoImage();
        }

        $roomMapper = new Villa_Module_Hotel_Mapper_Roomtypes();
        $roomImages = $roomMapper->getFirstImage($this->_model->roomData->type_id);
        $roomFirstImage = isset($roomImages[$this->_model->roomData->type_id]) ?
                    Villa_Module_Hotel_Helper_Media::getApartmentTypesDir().$roomImages[$this->_model->roomData->type_id] :
                    Lib_Image_Helper::getNoImage();

        $gallery = array(
            'room' => $roomFirstImage,
            'services' => $firstServicesImgs,
            'excursions' => $firstExcursImage,
            'transfer' => $firstTransferImg
        );

        return $gallery;
    }
    
    protected function _getBookingInfo()
    {
        $ctr = new Villa_Module_Bookinginfo_Controller();
        return $ctr->run();
    }

    protected function _editBookingAction()
    {
        $uid = Dante_Helper_App::getUid();
        if(!$uid)
            return Module_Lang_Helper::translate('userAccessOnly');

        if(!$this->_bookingId || $this->_model->uid != $uid) return Module_Lang_Helper::translate('wrongUrl');

        $this->_editMode = true;

        return $this->_defaultAction();
    }
    
    protected function _getExcursionInfoAction()
    {
        $day = $this->_getRequest()->get('day');

        $mapper = new Villa_Module_Hotel_Mapper_Excursion();
        $excursion = $mapper->getByDay($day);

        $days = Villa_Module_Hotel_Helper_Excursion::getDays();
        $excursion['title'] = $excursion['title'].' - '.$days[$day];
        $excursion['descript'] = $excursion['full_descr'];
        unset ($excursion['html']);

        if(!count($excursion['images']))
            $excursion['images'][] = Lib_Image_Helper::getNoImage();

        return array(
            'result' => 1,
            'excursion' => $excursion
        );
    }
    
    protected function _getServiceInfoAction()
    {
        $id = $this->_getRequest()->get('id');

        $mapper = new Villa_Module_Hotel_Mapper_Bookingoptions();
        $serviceItem = $mapper->getService($id);

        $service = array();
        $service['title'] = $serviceItem['name'];
        $service['descript'] = $serviceItem['full_descr'];
        $service['images'] = $serviceItem['images'];

        if(!count($service['images']))
            $service['images'][] = Lib_Image_Helper::getNoImage();

        return array(
            'result' => 1,
            'service' => $service
        );
    }
    
    protected function _saveBookingAction()
    {
        $this->_fillModelFromRequest();
        $this->_model->removeEmptyValues();
        //print_r($this->_model->adultsInfo);
        if(!$this->_model->adultsInfo[1]->first_name || !$this->_model->adultsInfo[1]->last_name || !$this->_model->adultsInfo[1]->email || !$this->_model->adultsInfo[1]->phone)
            return array(
                'result' => 0,
                'message' => Module_Lang_Helper::translate('wrongGuestData')
            );

        // Если у нас есть пользователи, указанные в "гостях", заполняем недостающие данные
        $userMapper = new Villa_Module_User_Manage_Mapper();
        foreach($this->_model->adultsInfo as $adult)
        {
            if($uid = $userMapper->profileExists($adult->email))
            {
                $profile = $userMapper->getUser($uid);
                $adult->scan = $profile->scan;
                $adult->address = $profile->address;
            }
        }

        if(!$this->_model->roomData->room_id)
        {
            // Ищем свободную комнату
            $roomMapper = new Villa_Module_Hotel_Mapper_Manage_Rooms();
            $this->_model->roomData->room_id = $roomMapper->getRoomForBooking(
                    $this->_model->roomData->type_id,
                    $this->_model->basicData->arrivalToInt(),
                    $this->_model->basicData->departureToInt()
                );
        }

        // Сохраняем бронь
        $this->_model->id = $this->_instanceMapper()->saveBooking($this->_model);

        if(!$this->_model->id)
            return array(
                'result' => 0,
                'message' => Module_Lang_Helper::translate('addBookingError')
            );

        $beforePayment = $this->_getRequest()->get('payment_form');

        $user = $this->_getUser($beforePayment);
        $this->_notifyUser($user);
        $customData = '';
        $sign = '';
        if($beforePayment)
        {
            if($this->_editMode) Villa_Module_Hotel_Helper_Session::setBookingEditMode();
            list($customData, $sign) = $this->_updatePaymentFormData($user->id);
        }
        else
        {
            $this->_notifyUser($user);
        }

        $this->_model->paymentsHistory = array();
        if($this->_model->id)
        {
            $mapper = new Villa_Module_Hotel_Mapper_Manage_Booking();
            $list = $mapper->getPaymentHistory($this->_model->id);
            $this->_model->paymentsHistory = $list;
        }

        //print_r($this->_model); die();
        Dante_Lib_Observer_Helper::fireEvent('new_booking', $this->_model);

        return array(
            'result' => 1,
            'id' => $this->_model->id,
            'sign' => $sign,
            'custom' => $customData,
            'edit_mode' => $this->_editMode
        );
    }

    protected function _getUser($beforePayment = false)
    {
        $currentUid = Dante_Helper_App::getUid();
        $userMapper = new Villa_Module_User_Manage_Mapper();

        if($this->_editMode || Villa_Module_User_Helper::checkTourAgent())
        {
            // Если турагент или мы в ред. режиме - достаем юзера из базы
            $user = $userMapper->getUser($currentUid);
            $user->password = false;
        }
        else
        {
            // Регистрируем юзера (все проверки внутри)
            $user = Villa_Module_User_Service::guestToUser($this->_model->adultsInfo[1]);
            if($user->password) Villa_Module_Hotel_Helper_Session::setNewUser($user->password);
        }

        // Если юзер не залогинен, но мы его зарегистрировали/нашли в базе - апдейтим uid
        if(!$currentUid)
        {
            $this->_instanceMapper()->updateBookingUid($this->_model->id, $user->id);
            if($beforePayment) Villa_Module_User_Service::autoLogin($user);
        }

        $this->_model->uid = $user->id;
        return $user;
    }

    protected function _updatePaymentFormData($uid)
    {
        $customData = '';
        $sign = '';

        Villa_Module_Hotel_Helper_Session::setBookingId($this->_model->id);

        $customFormData = $this->_getRequest()->get('custom_form_data');
        $formType = $this->_getRequest()->get('payment_form_type');
        $customDataParsed = array();
        switch($formType)
        {
            case 'liqpay':
                $customDataParsed = Villa_Module_Paysystems_Helper_Liqpay::parseDescription($customFormData);

                if(isset($customDataParsed['uid']) && $customDataParsed['uid'] == $uid)
                    $descr = NULL;
                else
                    $descr = Villa_Module_Paysystems_Helper_Liqpay::genDescription($customDataParsed['descr'], $customDataParsed['subject'], $uid);

                $customData = Villa_Module_Paysystems_Helper_Liqpay::updateDataField($customFormData, $this->_model->id, $descr);
                $sign = Villa_Module_Paysystems_Helper_Liqpay::updateSignature($customData);
                break;
            case 'paypal':
                $customDataParsed = Villa_Module_Paysystems_Helper_Paypal::parseCustomData($customFormData);

                if(isset($customDataParsed['uid']) && $customDataParsed['uid'] == $uid)
                    $customData = $customFormData;
                else
                    $customData = Villa_Module_Paysystems_Helper_Paypal::genCustomData($customDataParsed['subject'], $uid);

                break;
        }

        return array($customData, $sign);
    }
    
    protected function _notifyUser($user)
    {
        $service = new Villa_Module_Hotel_Service_Booking();
        if($this->_editMode)
        {
            $service->setMode(BOOKING_MAIL_MODE_EDIT);
        }
        elseif(Villa_Module_User_Helper::checkTourAgent())
        {
            $service->setMode(BOOKING_MAIL_MODE_TOURAGENT);
        }

        $service->notifyUser($user, $this->_model->id);
    }
}
?>