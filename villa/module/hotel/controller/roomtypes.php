<?php

class Villa_Module_Hotel_Controller_Roomtypes extends Dante_Controller_Base
{
    protected function _defaultAction() 
    {
        $params = array(
            'lang' => Module_Lang_Helper::getCurrent()
        );

        $mapper = new Villa_Module_Hotel_Mapper_Roomtypes();

        $items = $mapper->getAll($params);

        if (!$items) $items = array();
                
        $params = array();
        $prepaymentMapper = new Villa_Module_Hotel_Mapper_Prepayment();
        $prepayment = $prepaymentMapper->getList();        
        
        $pricesMapper = new Villa_Module_Hotel_Mapper_Manage_Prices();
        $priceList = $pricesMapper->getCurrentPriceList()->roomTypesPrices;
        $curPrices = array();
        
        foreach($priceList as $price)
        {
            if(!isset($curPrices[$price['id']]))
                $curPrices[$price['id']] = array();
            
            $curPrices[$price['id']][$price['prep_type']] = array(
                'standard' => Module_Currency_Helper::convert($price['price']),
                'displayed' => Module_Currency_Helper::convert($price['displayedPrice'])
            );
        }
        
        foreach(array_keys($items) as $apptId)
        {
            foreach(array_keys($prepayment) as $typeId)
            {
                if(!isset($curPrices[$apptId])) $curPrices[$apptId] = array();
                
                if(!isset($curPrices[$apptId][$typeId]))
                    $curPrices[$apptId][$typeId] = array(
                        'standard' => 0,
                        'displayed' => 0
                    );
            }
        }
               
        $currency = Module_Currency_Helper::getCurrentCurrency();
        
        $params['prepayment'] = $prepayment;
        $params['items'] = $items;
        $params['currency'] = $currency;
        
        $params['infoblock'] = $this->_getInfoblock();
        
        $params['curPrices'] = $curPrices;
        $params['prices'] = $this->_getPriceCalendarData();
        
        $view = new Villa_Module_Hotel_View_Roomtypes();
        return $view->draw($params);
    }
    
    protected function _getInfoblock()
    {        
        $infoblockCtr = new Villa_Module_Infoblock_Controller();
        return $infoblockCtr->run();
    }
    
    protected function _getPriceCalendarData()
    {
        $priceMapper = new Villa_Module_Hotel_Mapper_Manage_Prices();
        return $priceMapper->getAppartmentTypesPrices();
    }
    
    protected function _getEquipmentAction()
    {
        $typeId = $this->_getRequest()->get('type_id');
        
        $mapper = new Villa_Module_Hotel_Mapper_Roomtypes();
        $equipList = $mapper->getEquipment($typeId);
        
        $view = new Villa_Module_Hotel_View_Roomtypes();
        return array(
            'result' => 1,
            'html' => $view->drawEquipment($equipList)
        );
    }
}

?>