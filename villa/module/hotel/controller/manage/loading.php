<?php

/**
 * Description of loading
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Controller_Manage_Loading extends Dante_Controller_Base
{       
    protected function _init() 
    {
        parent::_init();        
        
        $helper = new Module_Division_Helper();
        $helper->checkAccess();        
    }
    /**
     * отрисовка стартового html
     * @return string 
     */
    protected function _defaultAction($dateFrom = false, $dateTo = false)
    {        
        if(!$dateFrom)
        {
            $dateFrom = Dante_Helper_Date::mktimeToday() -7*24*60*60;            
            if(date("I", $dateFrom)) $dateFrom -= 3600;
        }
        if(!$dateTo)
        {
            $dateTo = Dante_Helper_Date::mktimeToday() +61*24*60*60;         
            if(date("I", $dateTo)) $dateTo -= 3600;
        }
        
        $roomMapper = new Villa_Module_Hotel_Mapper_Manage_Rooms();
        
        $mapper = new Villa_Module_Hotel_Mapper_Manage_Loading();
        $service = (int)$this->_getRequest()->get("service");

        $bookings = $mapper->getBookings($dateFrom, $dateTo, $service);
        
        $view = new Villa_Module_Hotel_View_Manage_Loading();
        
        $view->dateFrom  = Dante_Helper_Date::converToDateType($dateFrom, 0);
        $view->dateTo = Dante_Helper_Date::converToDateType($dateTo, 0);
        $view->roomList = array_values($roomMapper->getApartmentsNumbers());
        $view->dates = $this->_getDatesList($dateFrom, $dateTo);
        $optionsMapper = new Villa_Module_Hotel_Mapper_Bookingoptions();
        $bookingTotals = new Villa_Module_Hotel_Model_Booking_Totals();


        $view->selectedService = $service;
        $view->services = $optionsMapper->getServices();
        $view->serviceCount = $mapper->getLastServices();
        $view->serviceChild = $mapper->getServiceChild();
        $view->payRemain = $mapper->getPayRemain();

//        ob_start();
//        var_dump($bookings);
//        $result = ob_get_clean();

        return $view->draw($bookings);
    }
    
    protected function _getDatesList($dateFrom, $dateTo)
    {
        $list = array();
        
        $oneDay = 24*3600; // один день в секундах
        $currentDate = $dateFrom;
        while($currentDate <= $dateTo)
        {
            $list[] = $currentDate;
            
            $prevDate = $currentDate;
            $currentDate += $oneDay;
            
            // Исправление ошибок с летним/зимним временем
            if(date("I", $prevDate) && !date("I", $currentDate))
                    $currentDate += 3600;
            
            if(!date("I", $prevDate) && date("I", $currentDate))
                    $currentDate -= 3600;
        }
        
        return $list;
    }
    
    protected function _drawAjaxAction()
    {
        $request = $this->_getRequest();
        $dateFromStr = $request->get('dateFrom');
        $dateToStr = $request->get('dateTo');
        
        $dateFrom = Dante_Helper_Date::strtotimef($dateFromStr, 0);
        $dateTo = Dante_Helper_Date::strtotimef($dateToStr, 0);
        
        return array(
            'result' => 1,
            'html' => $this->_defaultAction($dateFrom, $dateTo)
        );
    }
}

?>