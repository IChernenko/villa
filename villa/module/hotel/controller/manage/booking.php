<?php

/**
 * Description of booking
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Controller_Manage_Booking extends Module_Handbooksgenerator_Controller_Base {

    protected function _init() {
        parent::_init();
        $this->_mapper = new Villa_Module_Hotel_Mapper_Manage_Booking();

        $currencyCtr = new Module_Currency_Controller();
        $request = new Dante_Lib_Request();
        $request->set('code', 'UAH');
        $currencyCtr->setRequest($request);
        $currencyCtr->run('change');
    }

    /**
     * 
     * @return \Villa_Module_Hotel_View_Manage_Booking 
     */
    protected function _instanceView() {
        $view = new Villa_Module_Hotel_View_Manage_Booking();
        $view->currencyCode = 'UAH';

        return $view;
    }

    /**
     * отрисовка стартового html
     * @return string
     */
    protected function _drawAction() {
        $hotelHelper = new Module_Division_Helper();
        $hotelHelper->checkAccess();

        $prepaymentMapper = new Villa_Module_Hotel_Mapper_Prepayment();
        $roomMapper = new Villa_Module_Hotel_Mapper_Roomtypes();
        $currencyMapper = new Module_Currency_Mapper();

        $roomTypes = $roomMapper->getNamesList();
        $prepaymentTypes = $prepaymentMapper->getNamesList();
        $statuses = $this->_mapper->getStatuses();
        $ccyList = $currencyMapper->getCodesList();

        $model = new Villa_Module_Hotel_Model_Manage_Bookinggrid();
        $params = $this->getParams($model);
        $list = $this->_mapper->getList($params);
        $this->formEnable('villa.module.hotel.controller.manage.booking');

        $this->tableParams['style'] = array(
            'width' => '2000px'
        );
        $this->_setHead($roomTypes, $prepaymentTypes, $statuses, $ccyList);
        $this->_setBody($list['rows'], $roomTypes, $prepaymentTypes, $statuses, $ccyList);

        $this->tfoot = array(
            array(
                '<a class="btn btn-success pull-left" onclick="handbooks.edit(0)">Добавить</a>' .
                '<a class="btn btn-success pull-right" onclick="handbooks.edit(0)">Добавить</a>'
            ),
            array(
                $this->paginator($list['rowCount'], $params)
            ),
        );

        $modalStyle = '<style> #modalWindow {width: 1210px !important;} </style>';

        return $modalStyle . $this->generate();
    }

    protected function _setHead($roomTypes, $prepaymentTypes, $statuses, $currencyList) {
        $titles = array(
            array(
                'content' => 'Функции',
                'params' => array(
                    'style' => array(
                        'width' => '100px'
                    )
                )
            ),
            array(
                'content' => 'ID',
                'sortable' => 'id'
            ),
            array(
                'content' => 'Статус',
                'sortable' => 'status'
            ),
            array(
                'content' => 'Валюта'
            ),
            array(
                'content' => 'Дата заезда',
                'sortable' => 'arrival'
            ),
            array(
                'content' => 'Дата выезда',
                'sortable' => 'departure'
            ),
            array(
                'content' => 'Кол-во ночей',
                'sortable' => 'days'
            ),
            array(
                'content' => 'Кол-во гостей',
            ),
            array(
                'content' => 'Номер',
                'sortable' => 'room'
            ),
            array(
                'content' => 'Тип номера',
                'sortable' => 'room_type'
            ),
            array(
                'content' => 'Предоплата',
                'sortable' => 'prepayment_type'
            ),
            array(
                'content' => 'Стоимость',
                'sortable' => 'cost'
            ),
            array(
                'content' => 'Имя',
                'sortable' => 'first_name'
            ),
            array(
                'content' => 'Фамилия',
                'sortable' => 'second_name'
            ),
            array(
                'content' => 'Почта',
                'sortable' => 'email'
            ),
            array(
                'content' => 'Телефон',
                'sortable' => 'phone'
            ),
            array(
                'content' => 'Комментарий',
            ),
            array(
                'content' => 'Функции',
                'params' => array(
                    'style' => array(
                        'width' => '100px'
                    )
                )
            ),
        );

        $filters = array(
            'button_filter_1' => array(
                'type' => 'button_filter'
            ),
            NULL,
            'status' => array(
                'type' => 'combobox',
                'items' => $statuses,
                'value' => $this->_getRequest()->get_validate('status', 'int')
            ),
            'currency_id' => array(
                'type' => 'combobox',
                'items' => $currencyList,
                'value' => $this->_getRequest()->get_validate('currency_id', 'int')
            ),
            'arrival' => array(
                'type' => 'datepick',
                'value' => $this->_getRequest()->get_validate('arrival', 'text')
            ),
            'departure' => array(
                'type' => 'datepick',
                'value' => $this->_getRequest()->get_validate('departure', 'text')
            ),
            'days' => array(
                'type' => 'text',
                'value' => $this->_getRequest()->get_validate('days', 'text')
            ),
            NULL,
            'room' => array(
                'type' => 'text',
                'value' => $this->_getRequest()->get_validate('room', 'text')
            ),
            'room_type' => array(
                'type' => 'combobox',
                'items' => $roomTypes,
                'value' => $this->_getRequest()->get_validate('room_type', 'int')
            ),
            'prepayment_type' => array(
                'type' => 'combobox',
                'items' => $prepaymentTypes,
                'value' => $this->_getRequest()->get_validate('prepayment_type', 'int')
            ),
            NULL,
            'first_name' => array(
                'type' => 'text',
                'value' => $this->_getRequest()->get_validate('first_name', 'text')
            ),
            'last_name' => array(
                'type' => 'text',
                'value' => $this->_getRequest()->get_validate('last_name', 'text')
            ),
            'email' => array(
                'type' => 'text',
                'value' => $this->_getRequest()->get_validate('email', 'text')
            ),
            'phone' => array(
                'type' => 'text',
                'value' => $this->_getRequest()->get_validate('phone', 'text')
            ),
            NULL,
            'button_filter_2' => array(
                'type' => 'button_filter'
            )
        );

        $this->thead = array($titles, $filters);
    }

    protected function _setBody($rows, $roomTypes, $prepaymentTypes, $statuses, $currencyList) {
        foreach ($rows as $cur) {
            $row = array();
            $row['buttons_f'] = array(
                'id' => $cur['id'],
                'type' => array('edit')//$cur['status'] == BOOKING_STATUS_RESIDENCE ?  array('edit') : array('edit', 'del'),                
            );
            $row['id'] = $cur['id'];
            $row['status'] = $statuses[$cur['status']];
            $row['currency'] = $currencyList[$cur['currency_id']];
            $row['arrival'] = Dante_Helper_Date::converToDateType($cur['arrival']);
            $row['departure'] = Dante_Helper_Date::converToDateType($cur['departure']);
            $row['days'] = $cur['days'];
            $row['guests'] = $cur['adults'] + $cur['children'];
            $row['room'] = $cur['room'];
            $row['room_type'] = $roomTypes[$cur['room_type']];
            $row['prepayment_type'] = $prepaymentTypes[$cur['prepayment_type']];
            $row['cost'] = $cur['cost'];
            $row['first_name'] = $cur['first_name'];
            $row['last_name'] = $cur['last_name'];
            $row['email'] = $cur['email'];
            $row['phone'] = $cur['phone'];
            $row['comment'] = $cur['comment'];
            $row['buttons'] = array(
                'id' => $cur['id'],
                'type' => array('edit')//$cur['status'] == BOOKING_STATUS_RESIDENCE ?  array('edit') : array('edit', 'del'),                
            );

            $this->tbody[] = $row;
        }
    }

    protected function _drawFormAction($model = false, $versionMode = false) {
        if (!$model) {
            $id = $this->_getRequest()->get_validate('id', 'int', false);
            if (!$id)
                return $this->_drawAddForm(); //если новое бронирование - рисуем другую форму

            $model = $this->_mapper->getById($id);
        }

        $roomMapper = new Villa_Module_Hotel_Mapper_Manage_Rooms();
        $optionsMapper = new Villa_Module_Hotel_Mapper_Bookingoptions();
        $prepaymentMapper = new Villa_Module_Hotel_Mapper_Prepayment();
        $roomTypesMapper = new Villa_Module_Hotel_Mapper_Manage_Roomtypes();
        $ccyMapper = new Module_Currency_Mapper();

        $priceList = $model->getPriceList();

        $roomInfo = $roomTypesMapper->getOne($model->roomData->type_id, 1);
        $prepaymentTypes = $prepaymentMapper->getNamesList();

        $services = $optionsMapper->getServices();
        foreach ($services as $id => &$service) {
            $service['price'] = Module_Currency_Helper::convert($priceList->getServicePrice($id));
        }

        $transfers = $optionsMapper->getTransfers();
        foreach ($transfers as $id => &$transfer) {
            $transfer['price'] = Module_Currency_Helper::convert($priceList->getTransferPrice($id));
        }

        $excursions = Villa_Module_Hotel_Helper_Excursion::getListForPeriod($model->basicData->arrivalToInt(), $model->days + 1, $model->id);
        foreach ($excursions as $date => &$excursion) {
            $excursion['price'] = Module_Currency_Helper::convert($priceList->getExcursionPrice($excursion['dayId']));
        }

        $view = $this->_instanceView();
        $roomList = $roomMapper->getFreeRooms(
                $model->roomData->type_id, $model->basicData->arrivalToInt(), $model->basicData->departureToInt(), $model->id
        );

        if (!$roomList)
            return array(
                'result' => 0,
                'message' => 'Свободные комнаты для выбранного периода отсутствуют'
            );


        $view->roomList = $roomList;
        $view->roomTypeName = $roomInfo->title;
        $view->prepaymentName = $prepaymentTypes[$model->roomData->prepayment];
        $userCurrency = $ccyMapper->getById($model->user_currency);
        $view->userCurrencyCode = $userCurrency['reduction'];

        $view->services = $services;
        $view->excursions = $excursions;
        $view->transfers = $transfers;

        $view->statusList = $this->_mapper->getStatuses();
        return array(
            'result' => 1,
            'html' => $view->drawForm($model, $versionMode),
            'model' => $model
        );
    }

    protected function _drawAddForm() {
        $roomTypesMapper = new Villa_Module_Hotel_Mapper_Roomtypes();
        $prepaymentMapper = new Villa_Module_Hotel_Mapper_Prepayment();
        $maxGuests = $roomTypesMapper->getMaxGuests();

        $view = $this->_instanceView();
        $view->prepaymentTypes = $prepaymentMapper->getNamesList();

        return array(
            'result' => 1,
            'html' => $view->drawAddForm($maxGuests['max_adults'], $maxGuests['max_children'])
        );
    }

    protected function _getRoomDataAction() {
        $mapper = new Villa_Module_Hotel_Mapper_Roomsearch();

        $request = $this->_getRequest();
        $model = new Villa_Module_Hotel_Model_Booking_Basic();
        $model->dateType = 0;
        $model->arrival = $request->get('arrival');
        $model->departure = $request->get('departure');
        $model->adults = $request->get('adults');
        $model->children = $request->get('children');

        if ($model->departureToInt() <= $model->arrivalToInt())
            return array(
                'result' => 0,
                'message' => 'Дата выезда не может быть меньше или равняться дате заезда'
            );

        try {
            $roomTypesFull = $mapper->search($model);
        } catch (Exception $e) {
            return array(
                'result' => 0,
                'message' => $e->getMessage()
            );
        }

        $roomTypes = array();
        foreach ($roomTypesFull as $id => $model) {
            $roomTypes[$id] = $model->title;
        }

        $view = $this->_instanceView();
        return array(
            'result' => 1,
            'html' => $view->drawRoomTypesSelect($roomTypes),
        );
    }

    protected function _getResidenceCostAction() {
        $pricesMapper = new Villa_Module_Hotel_Mapper_Manage_Prices();

        $request = $this->_getRequest();
        $model = new Villa_Module_Hotel_Model_Booking_Basic();
        $model->dateType = 0;
        $model->arrival = $request->get('arrival');
        $model->departure = $request->get('departure');

        $roomType = $request->get('room_type');
        $prepayment = $request->get('prepayment');

        $roomPrice = $pricesMapper->getPrice(
                Villa_Helper_Entity::getRoomType(), $roomType, $prepayment, $model->arrivalToInt()
        );

        $days = round(($model->departureToInt() - $model->arrivalToInt()) / 24 / 3600);

        $allPrice = $roomPrice * $days;

        return array(
            'result' => 1,
            'dayPrice' => Module_Currency_Helper::convert($roomPrice),
            'allPrice' => Module_Currency_Helper::convert($allPrice)
        );
    }

    protected function _addBookingAction() {
        $pricesMapper = new Villa_Module_Hotel_Mapper_Manage_Prices();
        $model = new Villa_Module_Hotel_Model_Manage_Booking();

        $request = $this->_getRequest();

        $model->status = BOOKING_STATUS_NEW;
        $model->uid = Dante_Helper_App::getUid();

        $model->basicData->adults = $request->get_validate('adults', 'int', 1);
        $model->basicData->children = $request->get_validate('children', 'int', 0);
        $model->basicData->arrival = $request->get('arrival');
        $model->basicData->departure = $request->get('departure');
        $model->days = round(($model->basicData->departureToInt() - $model->basicData->arrivalToInt()) / 24 / 3600);

        $model->roomData->type_id = $request->get('room_type');
        $model->roomData->prepayment = $request->get('prepayment');

        $roomPrice = $pricesMapper->getPrice(
                Villa_Helper_Entity::getRoomType(), $model->roomData->type_id, $model->roomData->prepayment, $model->basicData->arrivalToInt()
        );
        $model->roomData->setPrice($roomPrice);
        $model->init();

        $adult = new Villa_Module_Hotel_Model_Booking_Adult();
        $adult->first_name = $request->get('first_name');
        $adult->last_name = $request->get('last_name');
        $adult->phone = $request->get('phone');
        $adult->email = $request->get('email');

        $model->adultsInfo[1] = $adult;

        if ($model->basicData->adults > 1) {
            for ($i = 2; $i <= $model->basicData->adults; $i++) {
                $defText = '-';
                $adult = new Villa_Module_Hotel_Model_Booking_Adult();
                $adult->first_name = $defText;
                $adult->last_name = $defText;
                $adult->phone = $defText;
                $adult->email = $defText;

                $model->adultsInfo[$i] = $adult;
            }
        }

        if ($model->basicData->children > 0) {
            for ($i = 1; $i <= $model->basicData->children; $i++) {
                $defText = '-';
                $child = new Villa_Module_Hotel_Model_Booking_Child();
                $child->first_name = $defText;
                $child->last_name = $defText;

                $model->childrenInfo[$i] = $child;
            }
        }

        $model->creation_time = time();

        // Ищем свободную комнату
        $roomMapper = new Villa_Module_Hotel_Mapper_Manage_Rooms();
        $model->roomData->room_id = $roomMapper->getRoomForBooking(
                $model->roomData->type_id, $model->basicData->arrivalToInt(), $model->basicData->departureToInt()
        );

        $model->totalsData->recalc();

        $model->id = $this->_mapper->addBooking($model);

        $this->_getRequest()->set('id', $model->id);

        return $this->_drawFormAction();
    }

    protected function _updateFormAction() {
        $model = $this->_getModelFromRequest();

        return $this->_drawFormAction($model);
    }

    protected function _getModelFromRequest() {
        $model = new Villa_Module_Hotel_Model_Manage_Booking();
        $request = $this->_getRequest();

        $model->id = $request->get('id');
        $model->status = $request->get('status');
        $model->user_currency = $request->get('currency_id');
        $model->roomData->room_id = $request->get_validate('room', 'int', NULL);
        $model->roomData->type_id = $request->get('room_type');
        $model->roomData->setPrice($request->get('room_price'));
        $model->roomData->prepayment = $request->get('prepayment');
        $model->roomData->max_adults = $request->get('max_adults');
        $model->roomData->max_children = $request->get('max_children');
        $model->basicData->arrival = $request->get('arrival');
        $model->basicData->departure = $request->get('departure');
        $model->uid = $request->get('uid');
        $model->days = $request->get('nights');
        $model->comment = $request->get('comment');
        $model->creation_time = Dante_Helper_Date::strtotimef($request->get('booking_date'), 0);
        $model->basicData->dateType = 0;

        $model->init();

        $discounts = $this->_mapper->getBookingDiscounts($model->id);
        $model->totalsData->setDiscountsByValues($discounts['dc_residence'], $discounts['dc_options']);

        $model->totalsData->addResidencePayment($request->get('residencePaid'));
        $model->totalsData->addOptionsPayment($request->get('servicePaid'));

        $model->basicData->adults = 0;
        $model->basicData->children = 0;

        $adultsInfo = $request->get('adultsInfo');
        foreach ($adultsInfo as $num => $adult) {
            $adultModel = new Villa_Module_Hotel_Model_Booking_Adult();
            $adultModel->enabled = $adult['enabled'];
            $adultModel->first_name = $adult['first_name'];
            $adultModel->last_name = $adult['last_name'];
            $adultModel->scan = $adult['scan'];
            $adultModel->address = $adult['adress'];
            $adultModel->phone = $adult['phone'];
            $adultModel->email = $adult['email'];

            if ($adult['enabled'])
                $model->basicData->adults++;

            $model->adultsInfo[$num] = $adultModel;
        }

        $childrenInfo = $request->get('childrenInfo');
        if (!$childrenInfo)
            $childrenInfo = array();

        foreach ($childrenInfo as $num => $child) {
            $childModel = new Villa_Module_Hotel_Model_Booking_Child();
            $childModel->enabled = $child['enabled'];
            $childModel->first_name = $child['first_name'];
            $childModel->last_name = $child['last_name'];

            if ($child['enabled'])
                $model->basicData->children++;

            $model->childrenInfo[$num] = $childModel;
        }


        $services = $request->get('services');
        $excursions = $request->get('excursions');
        $transfers = $request->get('transfers');
        $extraServices = $request->get('extra_services');

        if (!is_array($services))
            $services = array();
        if (!is_array($excursions))
            $excursions = array();
        if (!is_array($transfers))
            $transfers = array();
        if (!is_array($extraServices))
            $extraServices = array();

        foreach ($services as $id => $service) {
            foreach ($service['adults'] as $num => &$item) {
                if (!$item['enabled'])
                    continue;

                $item['guest_type'] = GUEST_TYPE_ADULT;
                $item['guest_num'] = $num;
                $model->addService($id, $item, false);
            }

            if (isset($service['children'])) {
                foreach ($service['children'] as $num => &$item) {
                    if (!$item['enabled'])
                        continue;

                    $item['guest_type'] = GUEST_TYPE_CHILD;
                    $item['guest_num'] = $num;
                    $model->addService($id, $item, false);
                }
            }
        }

        foreach ($transfers as $type => $transfer) {
            $model->addTransfer($type, $transfer, false);
        }

        foreach ($extraServices as $extraService) {
            $model->addExtraService($extraService, false);
        }

        foreach ($excursions as $date => $excursion) {
            foreach ($excursion['adults'] as $num => &$item) {
                if (!$item['enabled'])
                    continue;

                $item['guest_type'] = GUEST_TYPE_ADULT;
                $item['guest_num'] = $num;
                $model->addExcursion($date, $item, false);
            }

            if (isset($excursion['children'])) {
                foreach ($excursion['children'] as $num => &$item) {
                    if (!$item['enabled'])
                        continue;

                    $item['guest_type'] = GUEST_TYPE_CHILD;
                    $item['guest_num'] = $num;
                    $model->addExcursion($date, $item, false);
                }
            }
        }

        $model->totalsData->recalc();

        return $model;
    }

    protected function _addServiceAction() {
        $model = $this->_getModelFromRequest();

        $newService = $this->_getRequest()->get('new_service');
        if (!$newService)
            return $this->_drawFormAction($model);

        $id = isset($newService['id']) ? $newService['id'] : false;
        if (!$id)
            return array(
                'result' => 0,
                'message' => 'Ошибка при добавлении услуги'
            );

        $days = isset($newService['days']) ? $newService['days'] : $model->days;

        $model->addNewService($id, $days);
        return $this->_drawFormAction($model);
    }

    protected function _addExcursionAction() {
        $model = $this->_getModelFromRequest();

        $newExcursion = $this->_getRequest()->get('new_excursion');
        if (!$newExcursion)
            return $this->_drawFormAction($model);

        $date = isset($newExcursion['date']) ? $newExcursion['date'] : false;
        if (!$date)
            return array(
                'result' => 0,
                'message' => 'Ошибка при добавлении экскурсии'
            );

        $model->addNewExcursion($date);
        return $this->_drawFormAction($model);
    }

    protected function _addTransferAction() {
        $model = $this->_getModelFromRequest();

        $newTransfer = $this->_getRequest()->get('new_transfer');
        if (!$newTransfer)
            return $this->_drawFormAction($model);

        $type = isset($newTransfer['type']) ? $newTransfer['type'] : false;
        if (!$type)
            return array(
                'result' => 0,
                'message' => 'Ошибка при добавлении трансфера'
            );

        $model->addTransfer($type, $newTransfer);
        return $this->_drawFormAction($model);
    }

    protected function _addExtraServiceAction() {
        $model = $this->_getModelFromRequest();

        $newService = $this->_getRequest()->get('new_extra_service');
        if (!$newService)
            return $this->_drawFormAction($model);

        $newService['cost'] = Module_Currency_Helper::deconvert($newService['cost']);

        $model->addExtraService($newService);
        return $this->_drawFormAction($model);
    }

    protected function _changeStatusAction($bookingId = false, $statusId = false) {
        if (!$bookingId)
            $bookingId = $this->_getRequest()->get('booking_id');

        if (!$statusId)
            $statusId = $this->_getRequest()->get('status_id');

        $statusName = $this->_mapper->changeStatus($bookingId, $statusId);

        // отмена брони
        if ($statusId >= 6 && $statusId <= 7) {
            // найти комнату в брони
            $mapper = new Villa_Module_Hotel_Mapper_Booking();
            $booking = $mapper->getById($bookingId);
            $room = $booking->roomData->room_id;
            // удалить комнату в брони
            $mapper->setRoom($bookingId, null);
        }

        if (!$statusName)
            return array(
                'result' => 0,
                'message' => 'wrong status id'
            );

        return array(
            'result' => 1,
            'status' => $statusName
        );
    }

    protected function _drawScanFormAction() {
        $bookingId = $this->_getRequest()->get('booking_id');
        $guestNum = $this->_getRequest()->get('guest_num');
        $scanImage = $this->_getRequest()->get('scan');

        $view = $this->_instanceView();
        return array(
            'result' => 1,
            'html' => $view->drawScanForm($bookingId, $guestNum, $scanImage)
        );
    }

    protected function _scanUploadAction() {
        if (!isset($_FILES['file']))
            return array(
                'result' => 0,
                'message' => 'Нет файла для загрузки'
            );

        $uploadDir = Villa_Module_User_Helper::getPassportScanDir();
        if (!is_dir($uploadDir))
            mkdir($uploadDir, 0755, true);

        $newName = 'scan_' . time();

        $uploader = new Lib_JqueryUpload_Uploader();
        $uploader->globalKey = 'file';

        $image = $uploader->upload($uploadDir, $newName);

        return array(
            'result' => 1,
            'dir' => $uploadDir,
            'image' => $image
        );
    }

    protected function _editAction() {
        $model = $this->_getModelFromRequest();

        foreach ($model->adultsInfo as $num => $item) {
            if ($item->scan) {
                $curScan = $this->_mapper->getScan($model->id, $num);
                $dir = Villa_Module_User_Helper::getPassportScanDir();

                if (is_file($dir . $curScan) && $curScan != $item->scan)
                    unlink($dir . $curScan);
            }
        }

        $model->removeEmptyValues();

        $this->_mapper->saveBooking($model);
//
        $mapper = new Villa_Module_Hotel_Mapper_Manage_Booking();
        $list = $mapper->getPaymentHistory($model->id);
        $model->paymentsHistory = $list;
//
//        Dante_Lib_Observer_Helper::fireEvent('edit_booking', $model);

        return $this->_drawFormAction();
    }

    protected function _drawPaymentFormAction() {
        $id = $this->_getRequest()->get('id');
        $type = $this->_getRequest()->get('type');
        $totals = $this->_getRequest()->get('totals');

        $view = $this->_instanceView();
        return array(
            'result' => 1,
            'html' => $view->drawPaymentForm($id, $type, $totals)
        );
    }

    protected function _getPaymentModel() {
        $request = $this->_getRequest();

        $model = new Villa_Module_Hotel_Model_Manage_Payment();
        $model->booking_id = $request->get('booking_id');
        $model->user_id = Dante_Helper_App::getUid();
        $model->type = $request->get_validate('type', 'int', 1);
        $model->subject = $request->get_validate('subject', 'int', 1);
        $model->date = time();
        $model->amount = $request->get('amount');
        $model->pay_way = $request->get('pay_way');
        $model->description = $request->get('description');

        return $model;
    }

    /**
     * Добавление / возврат оплаты
     * @return type 
     */
    protected function _createPaymentAction() {
        $model = $this->_getPaymentModel();
        $residenceMax = $this->_getRequest()->get('residenceMax');
        $serviceMax = $this->_getRequest()->get('serviceMax');

        $error = false;
        switch ($model->subject) {
            case PAYMENT_SUBJECT_RESIDENCE:
                if ($model->amount > $residenceMax)
                    $error = 'проживание';
                break;

            case PAYMENT_SUBJECT_SERVICE:
                if ($model->amount > $serviceMax)
                    $error = 'услуги';
                break;
        }

        if ($error) {
            $message = '<b>Невозможно создать платеж.</b><br/>';
            switch ($model->type) {
                case PAYMENT_TYPE_INCOME:
                    $message .= 'Введенная сумма оплаты больше остатка к оплате <u>за ' . $error . '</u>';
                    break;

                case PAYMENT_TYPE_REFUND:
                    $message .= 'Введенная сумма возврата больше суммы внесенных оплат <u>за ' . $error . '</u>';
                    break;
            }
            $message .= '<br/><b>Введите меньшее число</b>';

            return array(
                'result' => 0,
                'message' => $message
            );
        }

        $model->amount = Module_Currency_Helper::deconvert($model->amount);

        $mapper = new Villa_Module_Hotel_Mapper_Manage_Booking();
        $id = $mapper->createPayment($model);

        if (!$id)
            return array(
                'result' => 0,
                'message' => 'Ошибка создания платежа'
            );

        return array(
            'result' => 1
        );
    }

    protected function _drawPaymentHistoryAction() {
        $id = $this->_getRequest()->get('id');

        $mapper = new Villa_Module_Hotel_Mapper_Manage_Booking();
        $list = $mapper->getPaymentHistory($id);

        $view = $this->_instanceView();

        //теперь выбираем имена юзеров
        $userMapper = new Villa_Module_User_Manage_Mapper();
        $view->userList = $userMapper->getEmailsList();
        $a = 1;

        return array(
            'result' => 1,
            'html' => $view->drawPaymentHistory($id, $list)
        );
    }

    protected function _drawVersionsAction() {
        $bookingId = $this->_getRequest()->get('id');
        $versions = $this->_mapper->getVersions($bookingId);

        $userMapper = new Villa_Module_User_Manage_Mapper();
        $users = $userMapper->getEmailsList();

        $view = $this->_instanceView();
        $html = $view->drawVersions($versions, $users);

        return array(
            'result' => 1,
            'html' => $html
        );
    }

    protected function _showVersionAction() {
        $bookingId = $this->_getRequest()->get('id');
        $version = $this->_getRequest()->get('version');

        $model = $this->_mapper->getVersion($bookingId, $version);

        return $this->_drawFormAction($model, true);
    }

    protected function _restoreVersionAction() {
        $bookingId = $this->_getRequest()->get('id');
        $version = $this->_getRequest()->get('version');

        $this->_mapper->restoreVersion($bookingId, $version);

        return array(
            'result' => 1
        );
    }

    protected function _deleteVersionAction() {
        $bookingId = $this->_getRequest()->get('id');
        $version = $this->_getRequest()->get('version');

        $this->_mapper->deleteVersion($bookingId, $version);

        return array(
            'result' => 1
        );
    }

}

?>
