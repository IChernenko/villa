<?php

/**
 * Description of bookingtransfer
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Controller_Manage_Bookingtransfer extends Module_Handbooksgenerator_Controller_Base
{
    protected function _init()
    {
        parent::_init();
        $this->_mapper = new Villa_Module_Hotel_Mapper_Manage_Bookingtransfer();
        $this->_mapper->hasPrimary = false;
        $this->modal = true;
    }

    protected function _instanceView()
    {
        return new Villa_Module_Hotel_View_Manage_Bookingtransfer();
    }

    protected function _drawAction() 
    {
        $params = $this->getParams(new Villa_Module_Hotel_Model_Manage_Bookingtransfer());
        $list = $this->_mapper->getList($params);
        
        $types = Villa_Module_Hotel_Helper_Manage_Booking::getTransferTypes();
        $transportTypes = Villa_Module_Hotel_Helper_Manage_Booking::getTransferTransportTypes();
                
        $this->formEnable('villa.module.hotel.controller.manage.bookingtransfer');
        $this->caption = 'Трансфер бронирования';
        
        $statusList = Villa_Module_Hotel_Helper_Manage_Booking::getTransferStatuses();
        
        $titles = array(
            array(
                'content' => 'ID брони',
                'sortable' => 'booking_id'
            ),
            array(
                'content' => 'Название',
                'sortable' => 'sys_name',
            ),
            array(
                'content' => 'Тип',
                'sortable' => 'type',
            ),
            array(
                'content' => 'Вид транспорта',
                'sortable' => 'transport_type',
            ),
            array(
                'content' => 'Дата',
                'sortable' => 'date',
            ),
            array(
                'content' => 'Время',
                'sortable' => 'time',
            ),
            array(
                'content' => 'Рейс',
                'sortable' => 'race',
            ),
            array(
                'content' => 'Поезд',
                'sortable' => 'train',
            ),
            array(
                'content' => 'Вагон',
                'sortable' => 'wagon',
            ),
            array(
                'content' => 'Стоимость',
                'sortable' => 'cost',
            ),
            array(
                'content' => 'Статус',
                'sortable' => 'status',                
            ),
            'Функции'
        );   
        
        $filters = array(
            'status' => array(
                'type' => 'combobox',
                'items' => $statusList,
                'value' => $this->_getRequest()->get('status')
            ),
            NULL
        );
        
        $emptyFilters = array_fill(0, 10, NULL);
        $filters = array_merge($emptyFilters, $filters);
        
        $this->thead = array($titles, $filters);
                   
        foreach($list['rows'] as $cur)
        {
            $rowCont = array();
            
            $lnk = '<a href="javascript:booking.editFromOutside('.$cur['booking_id'].', handbooks.formSubmit)"'.
                    ' title="Открыть форму бронирования">'.$cur['booking_id'].'</a>';
            $rowCont['booking_id'] = $lnk;
            
            $rowCont['sys_name'] = $cur['sys_name'];
            $rowCont['type'] = $types[$cur['type']];
            $rowCont['transport_type'] = $transportTypes[$cur['transport_type']];
            $rowCont['date'] = Dante_Helper_Date::converToDateType($cur['date'], 0);
            $rowCont['time'] = Dante_Helper_Date::converToDateType($cur['time'], 10);
            $rowCont['race'] = $cur['race'] ? $cur['race'] : '-';
            $rowCont['train'] = $cur['train'] ? $cur['train'] : '-';
            $rowCont['wagon'] = $cur['wagon'] ? $cur['wagon'] : '-';
            $rowCont['cost'] = $cur['cost'];
            $rowCont['status'] = $statusList[$cur['status']];
            $rowCont['btn'] = '<a class="btn btn-mini btn-info optional" onclick="bookingtransfer.edit('.
                    $cur['booking_id'].', '.$cur['transfer_id'].')">Ред.</a>';
            
            $row = array('content' => $rowCont);
            
            switch($cur['status'])
            {
                case TRANSFER_STATUS_ORDER:
                    $bgColor = '#ED5353';
                    break;
                case TRANSFER_STATUS_ORDERED:
                    $bgColor = '#A0ED53';
                    break;
                case TRANSFER_STATUS_PROCESSED:
                    $bgColor = '#FDEC72';
                    break;
                case TRANSFER_STATUS_END:
                    $bgColor = '#BBB';
                    break;
                default:
                    $bgColor = '#FFF';
            }
            $row['params'] = array(
                'style' => array(
                    'background' => $bgColor
                )
            );
            $this->tbody[] = $row;
        }
        
        $this->tfoot=array(
            array(
                $this->paginator($list['rowCount'], $params)                              
            ),
        );
        
        $this->modal = true;
        
        $modalStyle = '<style>#modalWindow{width: 1210px !important;}</style>';
        return $modalStyle.$this->generate();
    }
    
    protected function _drawFormAction()
    {
        $bookingId = $this->_getRequest()->get('booking_id');
        $transferId = $this->_getRequest()->get('transfer_id');
        
        $model = $this->_mapper->get($bookingId, $transferId);
        
        return array(
            'result' => 1,
            'html' => $this->_instanceView()->drawForm($model)
        );
    }
    
    protected function _editAction()
    {
        $model = new Villa_Module_Hotel_Model_Manage_Bookingtransfer();
        $request = $this->_getRequest();
        
        $model->booking_id = $request->get('booking_id');
        $model->transfer_id = $request->get('transfer_id');
        $model->status = $request->get('status');
        $model->setDate($request->get('date'));
        $model->setTime($request->get('time'));
        
        $this->_mapper->update($model);
        
        return array(
            'result' => 1
        );
    }
}

?>
