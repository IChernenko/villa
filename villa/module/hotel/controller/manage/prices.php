<?php

/**
 * Description of controller
 *
 * @author Mort
 */

class Villa_Module_Hotel_Controller_Manage_Prices extends Dante_Controller_Base 
{
    protected $_mapper;

    protected function _init() 
    {
        parent::_init();
        
        $helper = new Module_Division_Helper();
        $helper->checkAccess();
        
        $this->_mapper = new Villa_Module_Hotel_Mapper_Manage_Prices();
    }
    
    protected function _instanceView()
    {
        return new Villa_Module_Hotel_View_Manage_Prices();
    }

    /**
     * отрисовка стартового html
     * @return string 
     */
    protected function _defaultAction()
    {
        $listModel = $this->_mapper->getAllPrices();
        
        $roomMapper = new Villa_Module_Hotel_Mapper_Manage_Roomtypes();
        $roomTypes = $roomMapper->getNamesList();
        
        $prepMapper = new Villa_Module_Hotel_Mapper_Manage_Prepayment();
        $prepayment = $prepMapper->getNamesList();
        
        $serviceMapper = new Villa_Module_Hotel_Mapper_Manage_Services();
        $services = $serviceMapper->getNamesList();
        
        $transferMapper = new Villa_Module_Hotel_Mapper_Manage_Transfer();
        $transfers = $transferMapper->getNamesList();
        
        $view = $this->_instanceView();
        $view->roomTypes = $roomTypes;
        $view->prepTypes = $prepayment;
        $view->servicesList = $services;
        $view->transferList = $transfers;
        $view->daysList = Villa_Module_Hotel_Helper_Excursion::getDays();
        
        return $view->drawPrices($listModel);
    }
    
    protected function _drawAjaxAction()
    {
        return array(
            'result' => 1,
            'html' => $this->_defaultAction()
        );
    }
    
    protected function _editAction()
    {
        $requset = $this->_getRequest();
        $model = new Villa_Module_Hotel_Model_Manage_Prices();
        $model->id = $requset->get_validate('id', 'int', false);
        $model->entity_type_id = $requset->get_validate('entity_type_id', 'int', false);
        $model->entity_id = $requset->get_validate('entity_id', 'int', false);
        $model->prep_type = $requset->get_validate('prep_type', 'int', NULL);
        $model->standardPrice = $requset->get('standardPrice');
        $model->displayedPrice = $requset->get('displayedPrice');
        
        if(!$model->entity_type_id || !$model->entity_id)
            return array(
                'result' => 0,
                'message' => 'wrong entity params'
            );
        
        if($model->entity_type_id == Villa_Helper_Entity::getRoomType() && !$model->prep_type)
            return array(
                'result' => 0,
                'message' => 'wrong prepayment param for appartment type price'
            );
                    
        $this->_mapper->apply($model);
        
        return $this->_drawAjaxAction();
    }
    
    protected function _delAction()
    {
        $id = $this->_getRequest()->get('id');        
        $this->_mapper->del($id);
        
        return $this->_drawAjaxAction();
    }


    protected function _drawDatesAction($priceId = false)
    {
        if(!$priceId)
            $priceId = $this->_getRequest()->get('price_id');
        
        $model = $this->_mapper->getById($priceId);
        switch($model->entity_type_id)
        {
            case Villa_Helper_Entity::getRoomType():
                $roomMapper = new Villa_Module_Hotel_Mapper_Manage_Roomtypes();
                $title = $roomMapper->getOne($model->entity_id, 1)->title;
                
                $prepMapper = new Villa_Module_Hotel_Mapper_Manage_Prepayment();
                $prepayment = $prepMapper->getNamesList();
                $title .= ', '.$prepayment[$model->prep_type];
                break;
            
            case Villa_Helper_Entity::getService():        
                $serviceMapper = new Villa_Module_Hotel_Mapper_Manage_Services();
                $title = $serviceMapper->getOne($model->entity_id)->sys_name;
                break;
            
            case Villa_Helper_Entity::getTransfer():
                $transferMapper = new Villa_Module_Hotel_Mapper_Manage_Transfer();
                $transfer = $transferMapper->getRow($model->entity_id, 1);
                $title = $transfer->sys_name.' - '.$transfer->type_name;
                break;
            
            default:
                $title = '';
                break;
        }
        
        $model->title = $title;
        
        $dates = $this->_mapper->getDates($priceId);
        $view = $this->_instanceView();
        
        return array(
            'result' => 1,
            'html' => $view->drawDates($model, $dates)
        );
    }
    
    protected function _editDateAction()
    {
        $requset = $this->_getRequest();
        $model = new Villa_Module_Hotel_Model_Manage_Pricesdate();
        $model->id = $requset->get_validate('id', 'int', false);
        $model->price_id = $requset->get_validate('price_id', 'int', false);
        $model->dateFrom = $requset->get('dateFrom');
        $model->dateTo = $requset->get('dateTo');
        $model->price = $requset->get('price');
        $model->disabled = $requset->get('disabled');
        
        $model->dateFrom = Dante_Helper_Date::strtotimef($model->dateFrom, 0);
        $model->dateTo = Dante_Helper_Date::strtotimef($model->dateTo, 0);
        
        $this->_mapper->applyDate($model);
        
        return $this->_drawDatesAction($model->price_id);
    }
    
    protected function _delDateAction()
    {
        $id = $this->_getRequest()->get('id');        
        $this->_mapper->delDate($id);
        
        $priceId = $this->_getRequest()->get('price_id');
        return $this->_drawDatesAction($priceId);
    }
}

?>