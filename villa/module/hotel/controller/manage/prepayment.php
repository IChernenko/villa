<?php

/**
 * Description of prepayment
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Controller_Manage_Prepayment extends Module_Handbooksgenerator_Controller_Base
{    
    protected function _init()
    {
        parent::_init();
        $this->_mapper = new Villa_Module_Hotel_Mapper_Manage_Prepayment();
    }
    
    protected function _drawAction()
    {        
        $params = $this->getParams(new Villa_Module_Hotel_Model_Manage_Prepayment());        
        $list = $this->_mapper->getRowsByParams($params);
        
        $this->formEnable('villa.module.hotel.controller.manage.prepayment');
        $this->caption = 'Типы предоплат';
        $this->tableParams['style'] = array(
            'width' => '600px'
        );
        
        $titles = array(
            array(
                'content' => 'ID',
                'sortable' => 'id'
            ),
            array(
                'content' => 'Систем. название',
                'sortable' => 'title',
            ),
            array(
                'content' => 'Функции',
                'params' => array(
                    'style' => array(
                        'width' => '100px'
                    )
                )
            )
        );   
        
        $this->thead = array($titles);
                   
        foreach($list['rows'] as $cur)
        {
            $row = array();
            
            $row['id'] = $cur['id'];
            $row['title'] = $cur['title'];
            $row['buttons'] = array(
                'id'=>$cur['id'],
                'type'=>array('edit'),                
            );
            
            $this->tbody[] = $row;
        }
        
        $html = $this->generate();
        
        return $html;
    }

    
    protected function _drawFormAction()
    {
        $id = $this->_getRequest()->get_validate('id', 'int', 0);
        $lang = $this->_getRequest()->get_validate('lang_id', 'int', 1);
        $model = $this->_mapper->getById($id, $lang);
        
        $langMapper = new Module_Lang_Mapper();
        $view = new Villa_Module_Hotel_View_Manage_Prepayment();
        
        $view->langList = $langMapper->getList();
        
        return array(
            'result' => 1,
            'html' => $view->drawForm($model)
        );
    }

    /**
     * редактирование записей грида
     */
    protected function _editAction() 
    {
        $model = new Villa_Module_Hotel_Model_Manage_Prepayment();
        $model->id = $this->_getRequest()->get('id');
        $model->sys_name = $this->_getRequest()->get('title');
        $model->lang_id = $this->_getRequest()->get('lang_id');
        $model->name = $this->_getRequest()->get('name');
        
        $this->_mapper->apply($model);
        
        return array(
            'result' => 1
        );
    }    
}

?>
