<?php

/**
 * Description of controller
 *
 * @author Mort
 */

class Villa_Module_Hotel_Controller_Manage_Roomtypes extends Module_Handbooksgenerator_Controller_Base 
{    
    protected function _init() 
    {
        parent::_init();
        $this->_mapper = new Villa_Module_Hotel_Mapper_Manage_Roomtypes();
    }
    
    protected function _drawAction() 
    {
        $model = new Villa_Module_Hotel_Model_Manage_Roomtypes();
        
        $params = $this->getParams($model);
        
        $list = $this->_mapper->getRowsByParams($params);
        
        $this->formEnable('villa.module.hotel.controller.manage.roomtypes');
        $this->caption = 'Типы номеров';
                
        $this->tableParams['style'] = array(
            'width' => '1000px'
        );
        
        $titles=array(
            array(
                'content'=>'ID',
                'sortable'=>'id'
            ),
            array(
                'content' => 'Название',
                'sortable' => 'title',
            ),
            array(
                'content' => 'Взрослых',
                'sortable' => 'max_adults',
            ),
            array(
                'content' => 'Детей',
                'sortable' => 'max_children',
            ),
            array(
                'content' => 'Ссылка',
                'sortable' => 'link',
            ),
            array(
                'content' => 'Функции',
                'params' => array(
                    'style' => array(
                        'width' => '100px'
                    )
                )
            )
        );   
        
        $this->thead = array($titles);
        
        $this->tbody = $this->_genRows($list['rows']);        
                
        $html = $this->generate();
        
        return $html;
    }
    
    protected function _genRows($rows)
    {
        $table = array();       
        foreach($rows as $cur)
        {
            $row = array();
            
            $row['id'] = $cur['id'];
            $row['title'] = $cur['title'];
            $row['max_adults'] = $cur['max_adults'];
            $row['max_children'] = $cur['max_children'];
            $row['link'] = $cur['link'];  
            $row['buttons'] = array(
                'id'=>$cur['id'],
                'type'=>array('edit'),                
            );
            
            $table[] = $row;
        }
        
        return $table;
    }
    
    protected function _drawFormAction($id = false)
    {
        $langMapper = new Module_Lang_Mapper();
        
        if(!$id) $id = $this->_getRequest()->get_validate('id', 'int', 0);
        $lang = $this->_getRequest()->get_validate('lang_id', 'int', 1);
        
        $model = $this->_mapper->getOne($id, $lang);
        $view = new Villa_Module_Hotel_View_Manage_Roomtypes();
        $view->langList = $langMapper->getList();
        
        return array(
            'result' => 1,
            'html' => $view->drawForm($model)
        );
    }
    
    protected function _editAction() 
    {                
        $model = new Villa_Module_Hotel_Model_Manage_Roomtypes();
        $model->id = $this->_getRequest()->get('id');
        $model->lang_id = $this->_getRequest()->get('lang_id');
        $model->title = $this->_getRequest()->get('title');
        $model->max_adults = $this->_getRequest()->get('max_adults');
        $model->max_children = $this->_getRequest()->get('max_children');
        $model->cost = $this->_getRequest()->get('cost');
        $model->link = $this->_getRequest()->get('link');
        $model->standart_cost = $this->_getRequest()->get('standart_cost');
        $model->description = $this->_getRequest()->get('description');
        Dante_Lib_Log_Factory::getLogger()->debug('$sql');
        $insert = $this->_mapper->apply($model);
        
        if($insert)
            return $this->_drawFormAction($model->id);
        else
            return array(
                'result' => 1
            );
    }

    /**
     * формируем массив картинок
     * @return array 
     */
    protected function _drawImagesAction($typeId = false) 
    {
        if(!$typeId) $typeId = $this->_getRequest()->get('typeId');
        
        if(!$typeId)
            return array(
                'result' => 0,
                'message' => 'incorrect id'
            );
        
        $mapper = new Villa_Module_Hotel_Mapper_Manage_Roomtypes();
        $images = $mapper->getImages($typeId);    
        
        $view = new Villa_Module_Hotel_View_Manage_Roomtypes();
        $view->typeId = $typeId;
        
        return array(
            'result' => 1,
            'html' => $view->drawImages($images)
        );
    }
    
    
    /**
     * загрузка картинки
     * @return boolean 
     */
    protected function _imageUploadAction() 
    {
         if(!isset($_FILES['file']))
            return array(
                'result' => 0,
                'message' => 'Нет файла для загрузки'
            );
                    
        $uploadDir = Villa_Module_Hotel_Helper_Media::getApartmentTypesDir();
        if (!is_dir($uploadDir)) mkdir($uploadDir, 0755, true);  
        
        $typeId = $this->_getRequest()->get('typeId');
        if(!$typeId)
            return array(
                'result' => 0,
                'message' => 'Неверный ID'
            );
        
        $newName = $typeId.'-'.time();  

        $uploader = new Lib_JqueryUpload_Uploader();
        $uploader->globalKey = 'file';
        
        $image = $uploader->upload($uploadDir, $newName);   
        $this->_mapper->addImage($typeId, $image);
                        
        return $this->_drawImagesAction($typeId);
    }
    
    /**
     * меняем порядок отображения картинки
     * @return array 
     */
    protected function _changeOrderAction() 
    {
        $typeId = $this->_getRequest()->get('typeId');
        $drawOrder = $this->_getRequest()->get('drawOrder');
        $orderFactor = $this->_getRequest()->get('orderFactor');
        
        $mapper = new Villa_Module_Hotel_Mapper_Manage_Roomtypes();
        $mapper->changeOrder($typeId, $drawOrder, $orderFactor);
        
        return $this->_drawImagesAction($typeId);
    }
    
    /**
     * удаляем картинку
     * @return boolean 
     */
    protected function _delImageAction() 
    {
        $typeId = $this->_getRequest()->get('typeId');
        $image = $this->_getRequest()->get('image');
        
        $mapper = new Villa_Module_Hotel_Mapper_Manage_Roomtypes();
        $mapper->delImage($typeId, $image);

        return array(
            'result' => 1
        );
    }
    
    /**
     * формируем массив оборудования
     * @return array 
     */
    protected function _drawEquipmentAction($typeId = false) 
    {
        if(!$typeId) $typeId = $this->_getRequest()->get('typeId');
        
        $mapper = new Villa_Module_Hotel_Mapper_Manage_Roomtypes();
        list($defEquip, $aptEquip) = $mapper->getEquipment($typeId);
                
        $view = new Villa_Module_Hotel_View_Manage_Roomtypes();
        
        return array(
            'result' => 1,
            'html' => $view->drawEquipment($defEquip, $aptEquip)
        );
    }
    
    /**
     * сохраняем оборудование
     * @return array 
     */
    protected function _saveEquipmentAction() {
        $typeId = $this->_getRequest()->get('typeId');
        $equipment = $this->_getRequest()->get('equipment');
        $bold = $this->_getRequest()->get('bold');
        $fontsize = $this->_getRequest()->get('fontsize');
        
        $equipmentArr = explode(',', $equipment);
        $boldArr = explode(',', $bold);
        $fontsizeArr = explode(',', $fontsize);
        
        $mapper = new Villa_Module_Hotel_Mapper_Manage_Roomtypes();
        $equipment = $mapper->saveEquipment($typeId, $equipmentArr, $boldArr, $fontsizeArr);
        
        return $this->_drawEquipmentAction($typeId);
    }
}

?>