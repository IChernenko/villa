<?php

/**
 * Description of cashcontrol
 *
 * @author Mort
 */
class Villa_Module_Hotel_Controller_Manage_Cashgendir extends Lib_Jgrid_Controller_Base{
    
    /**
     * отрисовка стартового html
     * @return string 
     */
    protected function _defaultAction()
    {
        $hotelHelper = new Module_Division_Helper();
        $hotelHelper->checkAccess();
        
        $mapper = new Villa_Module_Hotel_Mapper_Manage_Cashgendir();
        $cashHistory = $mapper->getCashHistory();
        $cash = $mapper->getCash();
        
        $user_id = Dante_Helper_App::getUid();
        
        $html = '';
        
        $html .= '<div style="margin: 0 auto; width:800px;">';
        $html .= '<table cellpadding="0" cellspacing="0" border="1" class="table table-bordered table-condensed">';
        $html .= '<tr>';
            $html .= '<td>';
                $html .= 'Номер';
            $html .= '</td>';
            $html .= '<td>';
                $html .= 'Пользователь';
            $html .= '</td>';
            $html .= '<td>';
                $html .= 'Балланс';
            $html .= '</td>';
            
        $html .= '</tr>';
        foreach ($cash as $value) {
            $html .= '<tr id="cashRow_'.$value['user_id'].'" onclick="cashcontrol.selectRow('.$value['user_id'].');" style="cursor:pointer;">';
            $html .= '<td>';
                $html .= '&nbsp;'.$value['user_id'].'&nbsp;';
            $html .= '</td>';
            $html .= '<td>';
                $html .= $value['fio'];
            $html .= '</td>';
            $html .= '<td>';
                $html .= $value['balance'];
            $html .= '</td>';
            $html .= '</tr>';
        }
        $html .= '</table>';
        
        $html .= '<br><br><br>';
        
        $html .= '<table cellpadding="0" cellspacing="0" border="1" class="table table-striped table-bordered table-condensed">';
        $html .= '<tr>';
            $html .= '<td>';
                $html .= 'Номер';
            $html .= '</td>';
            $html .= '<td>';
                $html .= 'Администратор от';
            $html .= '</td>';
            $html .= '<td>';
                $html .= 'Администратор кому';
            $html .= '</td>';
            $html .= '<td>';
                $html .= 'Сумма';
            $html .= '</td>';
            $html .= '<td>';
                $html .= 'Дата';
            $html .= '</td>';
            $html .= '<td>';
                $html .= 'Описание';
            $html .= '</td>';
            
        $html .= '</tr>';
        foreach ($cashHistory as $value) {
            $html .= '<tr>';
            $html .= '<td>';
                $html .= '&nbsp;'.$value['id'].'&nbsp;';
            $html .= '</td>';
            $html .= '<td>';
                $html .= $value['user_from_name'];
            $html .= '</td>';
            $html .= '<td>';
                $html .= $value['user_to_name'];
            $html .= '</td>';
            $html .= '<td>';
                $html .= $value['summ'];
            $html .= '</td>';
            $html .= '<td>';
                $html .= Dante_Helper_Date::converToDateType($value['date'],2);
            $html .= '</td>';
            $html .= '<td>';
                $html .= $value['description'];
            $html .= '</td>';
            $html .= '</tr>';
        }
        $html .= '</table>';
        
        $html .='
        <br>
        <div id="navMenu">
                <input type="button" value ="Списать средства с пользователя" onclick="cashcontrol.showDepositForm();" class="btn">
            </div>
<br><br>
            <div id="depositForm">
                <b>Форма списания средств</b><br>
                <label for="summ">Сумма для списания</label> <input type="text" id="summ" name="summ" style="width:150px;"><br>
                <label for="description">Комментарий</label> <textarea id="description" name="description" rows="10" cols="80" style="width: 40%"></textarea><br>
                <input type="button" value ="Снять средства" onclick="cashcontrol.doDepositAction();" class="btn">
            </div>
        ';
        $html .= '</div>';
        
        $html .= '<script>
            var curRow = 0;
            var curUser = '.$user_id.';
            $("#depositForm").hide();
            </script>';
        
        return $html;
    }
    
}

?>
