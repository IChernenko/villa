<?php

/**
 * Description of cashcontrol
 *
 * @author Mort
 */
class Villa_Module_Hotel_Controller_Manage_Cashcontrol extends Lib_Jgrid_Controller_Base{
    
    /**
     * отрисовка стартового html
     * @return string 
     */
    protected function _defaultAction()
    {
        $hotelHelper = new Module_Division_Helper();
        $hotelHelper->checkAccess();
        
        $user_id = Dante_Helper_App::getUid();
        
        $mapper = new Villa_Module_Hotel_Mapper_Manage_Cashcontrol();
        $cash = $mapper->getCash();
        
        $html = '';

        $html .= '<div style="margin: 0 auto; width:800px;">';
        $html .= '<table cellpadding="0" cellspacing="0" border="1" class="table table-bordered table-condensed">';
        $html .= '<tr>';
            $html .= '<td>';
                $html .= 'Номер';
            $html .= '</td>';
            $html .= '<td>';
                $html .= 'Модератор';
            $html .= '</td>';
            $html .= '<td>';
                $html .= 'Балланс';
            $html .= '</td>';
            
        $html .= '</tr>';
        foreach ($cash as $value) {
            $html .= '<tr id="cashRow_'.$value['user_id'].'" onclick="cashcontrol.selectRow('.$value['user_id'].');" style="cursor:pointer;">';
            $html .= '<td>';
                $html .= '&nbsp;'.$value['user_id'].'&nbsp;';
            $html .= '</td>';
            $html .= '<td>';
                $html .= $value['fio'];
            $html .= '</td>';
            $html .= '<td>';
                $html .= $value['balance'];
            $html .= '</td>';
            $html .= '</tr>';
        }
        $html .= '</table>';
        
        $html .='
        <br>
        <div id="navMenu">
                <input type="button" value ="Списать средства с Модератора" onclick="cashcontrol.showDepositForm();" class="btn">
                <input type="button" value ="Списать средства с себя" onclick="cashcontrol.selfSelect();" class="btn">
            </div>
<br><br>
            <div id="depositForm">
                <b>Форма списания средств</b><br>
                <label for="summ">Сумма для списания<font color=red>*</font></label> <input type="text" id="summ" name="summ" style="width:150px;"><br>
                <label for="description">Комментарий</label> <textarea id="description" name="description" rows="10" cols="80" style="width: 40%"></textarea><br>
                <input type="button" value ="Снять средства" onclick="cashcontrol.doDepositAction();" class="btn">
                <input type="button" value ="Закрыть" onclick="cashcontrol.hideDepositForm();" class="btn">
            </div>
        ';
        $html .= '</div>';
        
        $html .= '<script>
            var curRow = 0;
            var curUser = '.$user_id.';
            $("#depositForm").hide();
            </script>';
        
        return $html;
    }
    
    /**
     * создание проводки
     * @return array 
     */
    protected function _addDepositAction()
    {
        $hotelHelper = new Module_Division_Helper();
        $hotelHelper->checkAccess();
        
        $userIdFrom = $this->_getRequest()->get('userId');
        $summ = $this->_getRequest()->get('summ');
        $description = $this->_getRequest()->get('description');
        
        $helper = new Dante_Helper_App();
        $userIdTo = $helper->getUid();
        
        $mapper = new Villa_Module_Hotel_Mapper_Manage_Cashcontrol();
        $mapper->addDeposit($userIdFrom, $userIdTo, $summ, $description);
        
        return array(
            'result' => 1
        );
    }
    
}

?>
