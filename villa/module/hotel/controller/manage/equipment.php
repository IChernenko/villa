<?php

/**
 * Description of equipment
 *
 * @author Admin
 */

class Villa_Module_Hotel_Controller_Manage_Equipment extends Module_Handbooksgenerator_Controller_Base 
{    
    protected function _init() 
    {
        parent::_init();
        $this->_mapper = new Villa_Module_Hotel_Mapper_Manage_Equipment();
    }

    /**
     * отрисовка стартового html
     * @return string 
     */
    protected function _drawAction()
    {
        $params = $this->getParams(new Villa_Module_Hotel_Model_Manage_Equipment());        
        $list = $this->_mapper->getRowsByParams($params);
        
        $this->formEnable('villa.module.hotel.controller.manage.equipment');
        $this->caption = 'Оборудование';
        $this->tableParams['style'] = array(
            'width' => '700px'
        );
        
        $titles = array(
            array(
                'content' => 'ID',
                'sortable' => 'id'
            ),
            array(
                'content' => 'Систем. название',
                'sortable' => 'sys_name',
            ),
            array(
                'content' => 'Функции',
                'params' => array(
                    'style' => array(
                        'width' => '100px'
                    )
                )
            )
        );   
        
        $this->thead = array($titles);
                   
        foreach($list['rows'] as $cur)
        {
            $row = array();
            
            $row['id'] = $cur['id'];
            $row['sys_name'] = $cur['sys_name'];
            $row['buttons'] = array(
                'id'=>$cur['id'],
                'type'=>array('edit', 'del'),                
            );
            
            $this->tbody[] = $row;
        }        
        
        $this->tfoot=array(
            array(
                $this->paginator($list['rowCount'], $params)                              
            ),
            array(
                'button_filter' => array(
                    'type' => array(
                        'edit'
                    ),                    
                )
             )       
        );
        
        $html = $this->generate();
        
        return $html;
        
    }
    
    protected function _drawFormAction()
    {
        $id = $this->_getRequest()->get_validate('id', 'int', 0);
        $lang = $this->_getRequest()->get_validate('lang_id', 'int', 1);
        $model = $this->_mapper->getById($id, $lang);
        
        $langMapper = new Module_Lang_Mapper();
        $view = new Villa_Module_Hotel_View_Manage_Equipment();
        
        $view->langList = $langMapper->getList();
        
        return array(
            'result' => 1,
            'html' => $view->drawForm($model)
        );
    }


    /**
     * редактирование записей грида
     */
    protected function _editAction() 
    {        
        $model = new Villa_Module_Hotel_Model_Manage_Equipment();
        $model->id = $this->_getRequest()->get('id');
        $model->sys_name = $this->_getRequest()->get('sys_name');
        $model->lang_id = $this->_getRequest()->get('lang_id');
        $model->name = $this->_getRequest()->get('name');
        
        $this->_mapper->apply($model);
        
        return array(
            'result' => 1
        );
    }
    
}

?>
