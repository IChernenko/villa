<?php

/**
 * Description of controller
 *
 * @author Valkyria
 */

class Villa_Module_Hotel_Controller_Manage_Rooms extends Module_Handbooksgenerator_Controller_Base 
{
    protected function _init()
    {
        parent::_init();
        $this->_mapper = new Villa_Module_Hotel_Mapper_Manage_Rooms();
    }
    
    protected function _drawAction()
    {
        $roomTypesMapper = new Villa_Module_Hotel_Mapper_Manage_Roomtypes();
        $typesList = $roomTypesMapper->getNamesList();
        
        $model = new Villa_Module_Hotel_Model_Manage_Rooms();
        
        $params = $this->getParams($model);
        
        $list = $this->_mapper->getRowsByParams($params);
        
        $this->formEnable('villa.module.hotel.controller.manage.rooms');
        $this->tableParams['style'] = array(
            'width' => '900px'
        );
        
        $titles = array(
            array(
                'content' => '№',
                'sortable' => 'id'
            ),
            array(
                'content' => 'Тип номера',
                'sortable' => 'type',
            ),
            array(
                'content' => 'Приоритет',
                'sortable' => 'priority',
            ),
            array(
                'content' => 'Функции',
                'params' => array(
                    'style' => array(
                        'width' => '100px'
                    )
                )
            )
        );   
        
        $filters = array(
            'id' => array(
                'type' => 'text',
                'value' => $this->_getRequest()->get_validate('id', 'int')
            ),
            'type' => array(
                'type' => 'combobox',
                'items' => $typesList,
                'value' => $this->_getRequest()->get_validate('type', 'int')
            ),
            'priority' => array(
                'type' => 'text',
                'value' => $this->_getRequest()->get_validate('priority', 'int')
            ),
            'button_filter'=>array(
                'type'=>'button_filter'
            )
        );
        
        $this->thead = array($titles, $filters);
                   
        foreach($list['rows'] as $cur)
        {
            $row = array();
            
            $row['id'] = $cur['id'];
            $row['type'] = $typesList[$cur['type']];
            $row['priority'] = $cur['priority'];
            $row['buttons'] = array(
                'id'=>$cur['id'],
                'type'=>array('edit', 'del'),                
            );
            
            $this->tbody[] = $row;
        }
        
        $this->tfoot=array(
            array(
                $this->paginator($list['rowCount'], $params)                              
            ),
            array(
                'button_filter' => array(
                    'type' => array(
                        'edit'
                    ),                    
                )
             )       
        );
        
        $html = $this->generate();
        
        return $html;
    }
    
    protected function _drawFormAction()
    {
        $id = $this->_getRequest()->get_validate('id', 'int', 0);
        $model = $this->_mapper->getById($id);
        
        $roomTypesMapper = new Villa_Module_Hotel_Mapper_Manage_Roomtypes();
        $view = new Villa_Module_Hotel_View_Manage_Rooms();
        
        $view->typesList = $roomTypesMapper->getNamesList();
        
        return array(
            'result' => 1,
            'html' => $view->drawForm($model)
        );
    }
    
    protected function _editAction()
    {
        $prevId = $this->_getRequest()->get_validate('prev_id', 'int', 0);
        
        $model = new Villa_Module_Hotel_Model_Manage_Rooms();
        $model->id = $this->_getRequest()->get_validate('id', 'int', 0);
        $model->type = $this->_getRequest()->get_validate('type', 'int', 0);
        $model->priority = $this->_getRequest()->get_validate('priority', 'int', 0);
        
        $res = $this->_mapper->apply($model, $prevId);
        
        if(!$res)
            return array(
                'result' => 0,
                'message' => 'Такой номер комнаты уже существует!'
            );
        
        return array(
            'result' => 1
        );
    }
}

?>