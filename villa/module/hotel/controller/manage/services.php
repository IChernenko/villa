<?php

/**
 * Description of bookingoptions
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Controller_Manage_Services extends Module_Handbooksgenerator_Controller_Base
{
    /**
     * Инициализация контроллера
     */
    protected function _init()
    {
        parent::_init();
        $this->_mapper = new Villa_Module_Hotel_Mapper_Manage_Services();
    }
    /**
     * формируем модель грида
     * @return Lib_Jgrid_Model_Base 
     */
    protected function _drawAction() 
    {
        $hotelHelper = new Module_Division_Helper();
        $hotelHelper->checkAccess();
        
        $list = $this->_mapper->getRowsByParams();
        
        $this->formEnable('villa.module.hotel.controller.manage.services');
        
        $titles = array(
            array(
                'content' => "#",
                'params' => array(
                    'style' => array(
                        'width'=>'15px'
                    )
                )
            ),
            array(
                'content' =>'Системное название',
                'params' => array(
                    'style' => array(
                        'width' => '400px'
                    )
                )
            ),
            array(
                'content' =>'Функции',
                'params' => array(
                    'style' => array(
                        'width' => '120px'
                    )
                )                
            ),
        );
        
        $this->thead = array(
            $titles
        );
        $this->_setBody($list['rows']);
        
        $this->tfoot=array(
//            array(
//                $this->paginator($list['rowCount'], $params)                              
//            ),
            array(
                'button_filter'=>array(
                    'type' => array('edit')
                )
            )
        );
        $this->tableParams['style'] = array(
            'width' => 'auto'
        );
        
        return $this->generate();
    }
    
    protected function _setBody($rows)
    {          
        foreach($rows as $cur){  
            $row = array();
            $row['id'] = $cur['id'];
            $row['sys_name'] = $cur['sys_name'];
            
            $row['buttons'] = array(
                'id'=>$cur['id'],
                'type'=>array('edit', 'del'),                
            );
            $this->tbody[$cur['id']] = $row;
        }
    }
    
    protected function _drawFormAction($model = false)
    {
        $langMapper = new Module_Lang_Mapper();
        
        if(!$model)
        {
            $lang = $this->_getRequest()->get_validate('lang_id', 'int', 1);
            $id = $this->_getRequest()->get_validate('id', 'int', 0);
            $model = $this->_mapper->getOne($id, $lang);     
        }
        
        $view = new Villa_Module_Hotel_View_Manage_Services();  
        
        $view->langList = $langMapper->getList();
              
        $html = $view->drawForm($model);

        return array(
            'result' => 1,
            'html' => $html
        );
    }

    /**
     * редактирование/добавление/удаление записей грида
     */
    protected function _editAction() 
    {
        $hotelHelper = new Module_Division_Helper();
        $hotelHelper->checkAccess();
        
        $model = new Villa_Module_Hotel_Model_Manage_Service();
        $model->id = $this->_getRequest()->get_validate('id', 'int', 0);
        $model->sys_name = $this->_getRequest()->get('sys_name');
//        $model->image = $this->_getRequest()->get('image');
        $model->lang_id = $this->_getRequest()->get('lang_id');
        $model->name = $this->_getRequest()->get('name');
        $model->description = $this->_getRequest()->get('description');
        $model->full_descr = $this->_getRequest()->get('full_descr');
        $model->outer_id = $this->_getRequest()->get('outer_id');
                
        if($model->id)
        {
            $curImage = $this->_mapper->getCurrentImage($model->id);
            $imgDir = Villa_Module_Hotel_Helper_Media::getServicesDir();
            if(is_file($imgDir.$curImage) && $curImage !== $model->image) 
                unlink($imgDir.$curImage);
        }
        
        $this->_mapper->apply($model);
        return $this->_drawFormAction($model);
    }

    /**
     * Отобразить изображения для опции
     * @param bool $serviceId - ID опции
     * @return array
     */
    protected function _drawImagesAction($serviceId = false)
    {
        if(!$serviceId) $serviceId = $this->_getRequest()->get('serviceId');
        if(!$serviceId)
            return array(
                'result' => 0,
                'message' => 'incorrect id'
            );

        $images = $this->_mapper->getImages($serviceId);

        $view = new Villa_Module_Hotel_View_Manage_Services();
        $view->serviceId = $serviceId;

        return array(
            'result' => 1,
            'html' => $view->drawImages($images)
        );
    }

    /**
     * загрузка картинки
     * @return boolean
     */
    protected function _imageUploadAction()
    {
        if(!isset($_FILES['file']))
            return array(
                'result' => 0,
                'message' => 'Нет файла для загрузки'
            );
        $uploadDir = Villa_Module_Hotel_Helper_Media::getServicesDir();
        if (!is_dir($uploadDir)) mkdir($uploadDir, 0755, true);

        $serviceId = $this->_getRequest()->get('serviceId');

        if(!$serviceId)
            return array(
                'result' => 0,
                'message' => 'Неверный ID'
            );

        $newName = $serviceId.'-'.time();
        $uploader = new Lib_JqueryUpload_Uploader();
        $uploader->globalKey = 'file';
        $image = $uploader->upload($uploadDir, $newName);
        $this->_mapper->addImage($serviceId, $image);

        return $this->_drawImagesAction($serviceId);
    }

    /**
     * удаляем картинку
     * @return boolean
     */
    protected function _delImageAction()
    {
        $imageId = $this->_getRequest()->get('id');
        $imageFile = Villa_Module_Hotel_Helper_Media::getServicesDir().$this->_getRequest()->get('image');

        $this->_mapper->delImage($imageId);
        
        if(is_file($imageFile)) unlink($imageFile);

        return array(
            'result' => 1
        );
    }

    /**
     * меняем порядок отображения картинки
     * @return array
     */
    protected function _changeOrderAction()
    {
        $serviceId = $this->_getRequest()->get('service_id');
        $imageId = $this->_getRequest()->get('image_id');
        $drawOrder = $this->_getRequest()->get('draw_order');
        $orderFactor = $this->_getRequest()->get('order_factor');

        $this->_mapper->changeOrder($serviceId, $imageId, $drawOrder, $orderFactor);

        return $this->_drawImagesAction($serviceId);
    }
}

?>
