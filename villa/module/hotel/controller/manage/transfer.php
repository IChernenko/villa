<?php
/**
 * Description of transfer
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Controller_Manage_Transfer extends Module_Handbooksgenerator_Controller_Base
{
    /**
     * Инициализация контроллера
     */
    protected function _init()
    {
        parent::_init();
        $this->_mapper = new Villa_Module_Hotel_Mapper_Manage_Transfer();
    }



    protected function _drawAction()
    {
        $mapper = new Villa_Module_Hotel_Mapper_Manage_Transfer();
        $transfers = $mapper->getRowsByParams();
        
        $this->formEnable('villa.module.hotel.controller.manage.transfer');
        $this->caption = 'Трансфер';
        
        $titles = array(
            'ID',
            array(
                'content' => 'Системное название',
                'params' => array(
                    'style' => array(
                        'width' => '200px'
                    )
                )
            ),
            array(
                'content' => 'Тип',
                'params' => array(
                    'style' => array(
                        'width' => '150px'
                    )
                )
            ), 
            'Функции'
        );        
                
        /**
         * параметры формы
         */
        $this->thead=array($titles);
        
        $types = Villa_Module_Hotel_Helper_Manage_Booking::getTransferTypes();
        foreach($transfers['rows'] as $cur)
        {
            $row = array();            
            $row['id'] = $cur['id'];
            $row['sys_name'] = $cur['sys_name'];
            $row['type'] = $types[$cur['type']];
            $row['buttons'] = array(
                'id'=>$cur['id'],
                'type'=>array('edit', 'del'),                
            );
            
            $this->tbody[] = $row;
        }
        
        $this->tfoot=array(
            array(
                'button_filter'=>array(
                    'type' => array('edit')
                )
            )
        );
        
        $this->tableParams['style'] = array(
            'width' => 'auto'
        );
        
        return $this->generate();
    }


    /**
     * Формирование формы редактирования трансфера в админке
     * @return array
     */
    protected function _drawFormAction()
    {
        $mapper = new Villa_Module_Hotel_Mapper_Manage_Transfer();
        $langMapper = new Module_Lang_Mapper();
        
        $id = $this->_getRequest()->get_validate('id', 'int', 0);
        $lang = $this->_getRequest()->get_validate('lang', 'int', 1);
        $model = $mapper->getRow($id, $lang);
        
        $view = new Villa_Module_Hotel_View_Manage_Transfer();
        $view->types = Villa_Module_Hotel_Helper_Manage_Booking::getTransferTypes();
        $view->transport = Villa_Module_Hotel_Helper_Manage_Booking::getTransferTransportTypes();
        $view->langList = $langMapper->getList();
        return array(
            'result' => 1,
            'html' => $view->draw($model)
        );
    }
    
    protected function _editAction()
    {
        $mapper = new Villa_Module_Hotel_Mapper_Manage_Transfer();
        $model = new Villa_Module_Hotel_Model_Manage_Transfer();
        $model->id = $this->_getRequest()->get_validate('transferId', 'int', 0);
        $model->sys_name = $this->_getRequest()->get('sys_name');
        $model->transport_type = $this->_getRequest()->get('transport_type');
        $model->description = $this->_getRequest()->get('description');
        $model->lang_id = $this->_getRequest()->get('lang');
        $model->type = $this->_getRequest()->get('type');
        $model->name = $this->_getRequest()->get('name');
        $model->type_name = $this->_getRequest()->get('type_name');
        $model->outer_id = $this->_getRequest()->get('outer_id');
        
        $mapper->edit($model);

        return array(
            'result' => 1
        );
    }
    
    protected function _delAction()
    {
        $mapper = new Villa_Module_Hotel_Mapper_Manage_Transfer();
        $id = $this->_getRequest()->get('id');
        $mapper->del($id);
        return array(
            'result' => 1
        );        
    }

    /**
     * Отобразить изображения для конкретного трансфера
     * @param bool $optionId - ID опции
     * @return array
     */
    protected function _drawImagesAction($transferId = false)
    {
        if(!$transferId) $transferId = $this->_getRequest()->get('transferId');
        if(!$transferId)
            return array(
                'result' => 0,
                'message' => 'incorrect Transfer ID'
            );
        $mapper = new Villa_Module_Hotel_Mapper_Manage_Transfer();
        $images = $mapper->getImages($transferId);
        $view = new Villa_Module_Hotel_View_Manage_Transfer();
        $view->transferId = $transferId;

        return array(
            'result' => 1,
            'html' => $view->drawImages($images)
        );
    }


    protected function _imageUploadAction()
    {
        if(!isset($_FILES['file']))
            return array(
                'result' => 0,
                'message' => 'Нет файла для загрузки'
            );
        $uploadDir = Villa_Module_Hotel_Helper_Media::getTransfersDir();
        if (!is_dir($uploadDir)) mkdir($uploadDir, 0755, true);
        $transferId = $this->_getRequest()->get('transferId');
        if(!$transferId)
            return array(
                'result' => 0,
                'message' => 'Неверный ID'
            );
        $newName = $transferId.'-'.time();
        $uploader = new Lib_JqueryUpload_Uploader();
        $uploader->globalKey = 'file';
        $image = $uploader->upload($uploadDir, $newName);



        $this->_mapper->addImage($transferId, $image);

        return $this->_drawImagesAction($transferId);
    }

    /**
     * Удаление изображения
     * @return boolean
     */
    protected function _delImageAction()
    {
        $transferId = $this->_getRequest()->get('transferId');
        $image = $this->_getRequest()->get('image');

        $mapper = $this->_mapper;
        $mapper->delImage($transferId, $image);

        return array(
            'result' => 1
        );
    }

    /**
     * Изменение порядка отображения картинки
     * @return array
     */
    protected function _changeOrderAction()
    {
        $transferId = $this->_getRequest()->get('transferId');
        $drawOrder = $this->_getRequest()->get('drawOrder');
        $orderFactor = $this->_getRequest()->get('orderFactor');

        $mapper = new Villa_Module_Hotel_Mapper_Manage_Transfer();
        $mapper->changeOrder($transferId, $drawOrder, $orderFactor);

        return $this->_drawImagesAction($transferId);
    }

}

?>
