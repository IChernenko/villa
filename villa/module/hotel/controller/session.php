<?php

class Villa_Module_Hotel_Controller_Session extends Dante_Controller_Base
{    
    protected function _setBookingBaseAction()
    {
        Villa_Module_Hotel_Helper_Session::unsetBookingId();
        Villa_Module_Hotel_Helper_Session::unsetBookingRoom();
        
        $request = $this->_getRequest();
        $model = new Villa_Module_Hotel_Model_Booking_Basic();
        $model->arrival = $request->get('arriveDate');
        $model->departure = $request->get('departureDate');
        $model->adults = $request->get('adults');
        $model->children = $request->get('children');

        Villa_Module_Hotel_Helper_Session::setBookingBase($model);
        
        unset($model);
        
        return array(
            'result' => 1
        );
    }

    protected function _setBookingRoomDataAction()
    {
        $base = Villa_Module_Hotel_Helper_Session::getBookingBase();
        
        $request = $this->_getRequest();
        $model = new Villa_Module_Hotel_Model_Booking_Room();
        $model->type_id = $request->get('type_id');
        $model->prepayment = $request->get('prepayment');
        
        $roomMapper = new Villa_Module_Hotel_Mapper_Manage_Roomtypes();
        $room = $roomMapper->getOne($model->type_id, 1);
        
        $model->max_adults = $room->max_adults;
        $model->max_children = $room->max_children;
        
        $pricesMapper = new Villa_Module_Hotel_Mapper_Manage_Prices();
        $priceList = $pricesMapper->getCurrentPriceList($base->arrivalToInt());
        
        $price = $priceList->getRoomTypePrice($model->type_id, $model->prepayment);
        $model->setPrice($price);
        
        Villa_Module_Hotel_Helper_Session::setBookingRoom($model);

        unset($model);
        
        return array(
            'result' => 1
        );
    }
    
    protected function _setBookingIdAction()
    {
        $bookingId = $this->_getRequest()->get('booking_id');
        Villa_Module_Hotel_Helper_Session::setBookingId($bookingId);
        
        return array(
            'result' => 1
        );
    }

    protected function _resetAllAction()
    {
        Villa_Module_Hotel_Helper_Session::unsetBookingBase();
        Villa_Module_Hotel_Helper_Session::unsetBookingRoom();
        Villa_Module_Hotel_Helper_Session::unsetBookingId();  
        
        return array(
            'result' => 1
        );
    }
}