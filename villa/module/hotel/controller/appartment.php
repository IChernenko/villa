<?php
/**
 * Контроллер отдельного номера
 * User: dorian
 * Date: 24.05.12
 * Time: 16:28
 * To change this template use File | Settings | File Templates.
 */
class Villa_Module_Hotel_Controller_Appartment extends Dante_Controller_Base
{
    protected function _defaultAction() {
        $typeUrl = $this->_getParam('%2');
        if (!$typeUrl) throw new Exception('некорректный вызов');

        $mapper = new Villa_Module_Hotel_Mapper_Roomtypes();
        $model = $mapper->getByLink($typeUrl, Module_Lang_Helper::getCurrent());
        
        // Учитываем валюту
        $currencyInfo = Module_Currency_Helper::getInfo();
        
        $model->cost = $model->cost*$currencyInfo['factor'];
        
        //var_dump($model);
        $view = new Villa_Module_Hotel_View_Appartment();
        $view->currencyName = $currencyInfo['name'];
        return $view->draw($model);
    }
    
    protected function _getAction() {
        $id = (int)$this->_getRequest()->get('id');

        $mapper = new Villa_Module_Hotel_Mapper_Roomtypes();
        $model = $mapper->get($id, Module_Lang_Helper::getCurrent());

        
        // Учитываем валюту
        $currencyInfo = Module_Currency_Helper::getInfo();
        
        $model->cost = $model->cost*$currencyInfo['factor'];
        
        //var_dump($model);
        $view = new Villa_Module_Hotel_View_Appartment();
        $view->currencyName = $currencyInfo['name'];
        return array(
            'result' => 1,
            'template' => $view->draw($model)
        );
    }
}
