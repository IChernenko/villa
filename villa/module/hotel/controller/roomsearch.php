<?php

class Villa_Module_Hotel_Controller_Roomsearch extends Dante_Controller_Base
{
    protected $_dateType = 3;
    
    /**
     * @var Villa_Module_Hotel_Model_Booking_Basic
     */
    protected $_basicData;
    
    protected function _init() 
    {
        parent::_init();
        $this->_basicData = Villa_Module_Hotel_Helper_Session::getBookingBase();
    }

    protected function _defaultAction()
    {                
        $mapper = new Villa_Module_Hotel_Mapper_Roomtypes();
        $maxGuests = $mapper->getMaxGuests();
        
        $view = new Villa_Module_Hotel_View_Roomsearch();
        if($maxGuests)
        {
            $view->maxAdults = $maxGuests['max_adults'];
            $view->maxChildren = $maxGuests['max_children'];
        }
        
        if(!$this->_basicData)
            $this->_basicData = new Villa_Module_Hotel_Model_Booking_Basic();
        
        $view->basicData = $this->_basicData;
        return $view->drawForm();
    }


    /**
     * Отображение результата поиска номеров
     * @return array
     */
    protected function _showResultAction()
    {    
        if(!$this->_basicData)
        {
            header('Location: /');
        }
        
        $mapper = new Villa_Module_Hotel_Mapper_Roomsearch();
        $prepaymentTypesMapper = new Villa_Module_Hotel_Mapper_Prepayment();
        $prepayment = $prepaymentTypesMapper->getList();

        $currency = Module_Currency_Helper::getCurrentCurrency();

        try
        {
            $items = $mapper->search($this->_basicData);
        }
        catch(Exception $e)
        {
            return $e->getMessage();
        }
        
        $pricesMapper = new Villa_Module_Hotel_Mapper_Manage_Prices();
        $priceList = $pricesMapper->getCurrentPriceList($this->_basicData->arrivalToInt())->roomTypesPrices;
        $curPrices = array();
        
        foreach($priceList as $price)
        {
            if(!isset($curPrices[$price['id']]))
                $curPrices[$price['id']] = array();
            
            $curPrices[$price['id']][$price['prep_type']] = array(
                'standard' => Module_Currency_Helper::convert($price['price']),
                'displayed' => Module_Currency_Helper::convert($price['displayedPrice']),
                'disabled' => $price['disabled']
            );
        }
        
        foreach(array_keys($items) as $apptId)
        {
            foreach(array_keys($prepayment) as $typeId)
            {
                if(!isset($curPrices[$apptId])) $curPrices[$apptId] = array();
                
                if(!isset($curPrices[$apptId][$typeId]))
                    $curPrices[$apptId][$typeId] = array(
                        'standard' => 0,
                        'displayed' => 0,
                        'disabled' => 0
                    );
            }
        }
        
        $view = new Villa_Module_Hotel_View_Roomsearch();
        $view->basicData = $this->_basicData;
        $view->roomData = Villa_Module_Hotel_Helper_Session::getBookingRoom();
        $view->days = $this->_getDays();
        $view->items = $items;
        $view->prepaymentTypes = $prepayment;
        $view->prices = $curPrices;
        $view->currency = $currency;
        
        return $view->drawResult();
    }

    protected function _getDays()
    {
        $validator = new Dante_Lib_Validator();
        $arrive         = $validator->dateToInt($this->_basicData->arrival, $this->_dateType);
        $departure      = $validator->dateToInt($this->_basicData->departure, $this->_dateType);
        return round(($departure-$arrive)/24/3600);
    }
    
    protected function _searchAjaxAction()
    {        
        $request = $this->_getRequest();
        
        $this->_basicData = new Villa_Module_Hotel_Model_Booking_Basic();
        $this->_basicData->arrival = $request->get('arrival');
        $this->_basicData->departure = $request->get('departure');
             
        $typeId = $request->get('room_type');
        $bookingId = $request->get('booking_id');
        
        // Ищем свободную комнату
        $roomMapper = new Villa_Module_Hotel_Mapper_Manage_Rooms();
        $newRoomId = $roomMapper->getRoomForBooking(
                $typeId, 
                $this->_basicData->arrivalToInt(), 
                $this->_basicData->departureToInt(),
                $bookingId
            );
        
        if(!$newRoomId)
        {
            return array(
                'result' => 0,
                'message' => Module_Lang_Helper::translate('noFreeRooms').'.'
            );
        }
        else
        {
            return array(
                'result' => 1,
                'message' => Module_Lang_Helper::translate('roomSearchSuccess').'.',
                'room' => $newRoomId
            );
        }
    }
}
?>