/* global villa */

bookingform = {
    booking_id: 0,
    edit_mode: false,
    controller: 'villa.module.hotel.controller.bookingform',
    guestSelectInit: function ()
    {
        $('.guest-num-select').each(function ()
        {
            var staticIndex = $('.guest-num-select').index(this);
            $(this).data('index', staticIndex);

            $(this).click(function () {
                $(this).find('ul').toggle();
                var index = $(this).data('index');
                var wrapperId = 'guestNumSelectWrap_' + index;
                if ($(this).parents('.carousel-wrap').length > 0)
                {
                    var wrapper = '<div id="' + wrapperId + '"></div>';
                    $(this).wrap(wrapper);
                    var offset = $(this).offset();
                    $(this).appendTo($(this).parents('.carousel-wrap').parent());
                    $(this).css('position', 'absolute').offset({top: offset.top, left: offset.left});
                } else if ($('div').is('#' + wrapperId) && $(this).parents('#' + wrapperId).length === 0)
                {
                    $(this).appendTo('#' + wrapperId);
                    $(this).removeAttr('style').unwrap();
                }
            });

            $(this).find('li').click(function () {
                var adults = $(this).find('.adult').length;
                var children = $(this).find('.child').length;
                $(this).parents('.guest-num-select').find('input.guest-select-adults').val(adults);
                $(this).parents('.guest-num-select').find('input.guest-select-children').val(children);

                var item = $(this).html();
                $(this).parents('.guest-num-select').find('.selected').html(item);

                bookingform.updateForm();
            });

            $(this).data('enter', false);

            $(this).mouseenter(function () {
                $(this).data('enter', true);
            });
            $(this).mouseleave(function () {
                $(this).data('enter', false);
            });
        });

        //обработчик "холостого" клика
        $(document).click(function ()
        {
            $('.guest-num-select').each(function () {
                if (!$(this).data('enter'))
                {
                    $(this).find('ul').hide();
                    var index = $(this).data('index');
                    var wrapperId = 'guestNumSelectWrap_' + index;
                    if ($('div').is('#' + wrapperId) && $(this).parents('#' + wrapperId).length === 0)
                    {
                        $(this).appendTo('#' + wrapperId);
                        $(this).removeAttr('style').unwrap();
                    }
                }
            });
        });
    },
    updateForm: function ()
    {
        this.beforeFormSend();

        var data = this.getFormData('updateForm');
        this.showOverlay();

        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'ajax.php?booking_id=' + bookingform.booking_id + '&edit_mode=' + bookingform.edit_mode,
            data: data,
            success: function (data) {
                if (data.result == 1)
                {
                    var textareaVal =  $('#tourExtraService textarea').val();
                    localStorage.setItem('textareaVal', JSON.stringify(textareaVal));
                    var retrievedObject = localStorage.getItem('textareaVal');
                    $('#content').html(data.html);
                    $('#tourExtraService textarea').val(JSON.parse(retrievedObject));

                } else
                    jAlert(data.message, villa.labels.error);
            }
        });
    },
    getFormData: function (action)
    {
        var data = $('#mainBookingForm').serializeArray();

        data.push({
            name: 'controller', value: this.controller
        });

        if (action != undefined)
        {
            data.push({
                name: 'action', value: action
            });
        }

        return data;
    },
    showAdultItem: function (num, obj)
    {
        this.showOverlay();
        $('.guest-info#adult_' + num).find('input.guest-enabled').val('1');
        $('.guest-info#adult_' + num).slideDown(300, function () {
            $(obj).hide();
            $(obj).next('.hide-guest').fadeIn(300, function () {
                bookingform.updateForm();
            });
        });
    },
    hideAdultItem: function (num, obj)
    {
        this.showOverlay();
        $('.guest-info#adult_' + num).find('input.guest-enabled').val('0');
        $('.guest-info#adult_' + num).slideUp(300, function () {
            $(obj).hide();
            $(obj).prev('.show-guest').fadeIn(300, function () {
                bookingform.updateForm();
            });
        });
    },
    showChildItem: function (num, obj)
    {
        this.showOverlay();
        $('.guest-info#child_' + num).find('input.guest-enabled').val('1');
        $('.guest-info#child_' + num).slideDown(300, function () {
            $(obj).hide();
            $(obj).next('.hide-guest').fadeIn(300, function () {
                bookingform.updateForm();
            });
        });
    },
    hideChildItem: function (num, obj)
    {
        this.showOverlay();
        $('.guest-info#child_' + num).find('input.guest-enabled').val('0');
        $('.guest-info#child_' + num).slideUp(300, function () {
            $(obj).hide();
            $(obj).prev('.show-guest').fadeIn(300, function () {
                bookingform.updateForm();
            });
        });
    },
    addService: function (btnObj)
    {
        var inputObj = $(btnObj).closest('tr').find('input.option-enabled');
        this.addOption(btnObj, inputObj);
    },
    delService: function (btnObj)
    {
        var inputObj = $(btnObj).closest('tr').find('input.option-enabled');
        this.delOption(btnObj, inputObj);
    },
    addExcursion: function (btnObj)
    {
        var inputObj = $(btnObj).closest('li').find('input.option-enabled');
        this.addOption(btnObj, inputObj);
    },
    delExcursion: function (btnObj)
    {
        var inputObj = $(btnObj).closest('li').find('input.option-enabled');
        this.delOption(btnObj, inputObj);
    },
    addTransfer: function (btnObj)
    {
        var tr = $(btnObj).closest('tr');
        if (!this.validateTransfer(tr))
            return;

        var inputObj = $(btnObj).closest('td').find('input.option-enabled');
        this.addOption(btnObj, inputObj);
    },
    delTransfer: function (btnObj)
    {
        var inputObj = $(btnObj).closest('td').find('input.option-enabled');
        this.delOption(btnObj, inputObj);
    },
    /**
     * Функция-обработчик нажатия кнопок Добавить(Удалить) для услуги
     */
    addOption: function (btnObj, inputObj)
    {
        this.showOverlay();
        $(inputObj).val(1);
        $(btnObj).hide();

        if ($(btnObj).siblings('.del-opt').hasClass('always-hidden'))
        {
            bookingform.updateForm();
        } else
        {
            $(btnObj).siblings('.del-opt').fadeIn('slow', function () {
                bookingform.updateForm();
            });
        }
    },
    delOption: function (btnObj, inputObj)
    {
        this.showOverlay();
        $(inputObj).val(0);
        $(btnObj).hide();
        $(btnObj).siblings('.add-opt').fadeIn('slow', function () {
            bookingform.updateForm();
        });
    },
    changeTransferTime: function (object)
    {
        var time = $(object).val();
        if(time.length === 2){
            time += ':';
            $(object).val(time);
        }
    },
    /**
     * Функция - обработчик изменения значения селекторов выбора трансфера
     * @param object - объект селектора
     */
    changeTransferType: function (object)
    {
        var name = $(object).attr('id');
        var price = $(object).children(":selected").attr("data-transferprice");
        $('span[id=' + name + 'Price]').text(price);

        var transportType = $(object).children(":selected").attr("data-transporttype");
        $(object).closest('tr').find('input.option-price').val(price);
        $(object).siblings('span.transport-type').hide();
        $(object).siblings('span.transport-type.type-' + transportType).show();
    },
    copyTransferCity: function (obj)
    {
        var city = $(obj).val();
        $('input.city').each(function () {
            if (!$(this).val())
                $(this).val(city);
        });
    },
    /**
     * Функция для отображения всплывающей подсказки
     * @param items - элементы для которых нужно добавить всплывающую подсказку
     * @param name - имя для элемента-подсказки
     */
    showTooltip: function (items, name)
    {
        $(items).each(function (i)
        {
            var text = $(this).siblings('.info-button-descr').html();
            $("body").append("<div class='" + name + "' id='" + name + i + "'>" + text + "</div>");

            var info_button_tooltip = $("#" + name + i);
            $(this).removeAttr("title").mouseover(function (kmouse)
            {
                info_button_tooltip.css({left: kmouse.pageX + 15, top: kmouse.pageY + 15});
                info_button_tooltip.css({opacity: 0.8, display: "none"}).fadeIn(400);
            }).mouseout(function ()
            {
                info_button_tooltip.fadeOut(400);
            });
        });
    },
    showExcursionsInfo: function (day, elem)
    {
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'ajax.php?skip_model=1',
            data: {
                controller: 'villa.module.hotel.controller.bookingform',
                action: 'getExcursionInfo',
                day: day
            },
            success: function (data)
            {
                if (data.result == 1)
                {
                    var params = data.excursion;
                    params['thumbs'] = 0;
                    var location = $(elem).closest('td');

                    showViewer(params, location);
                } else
                {
                    jAlert(data.message, villa.labels.error);
                }
            }
        });
    },
    showServiceInfo: function (id, elem)
    {
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'ajax.php?skip_model=1',
            data: {
                controller: 'villa.module.hotel.controller.bookingform',
                action: 'getServiceInfo',
                id: id
            },
            success: function (data)
            {
                if (data.result == 1)
                {
                    var params = data.service;
                    params['thumbs'] = 0;
                    var location = $(elem).closest('td');

                    showViewer(params, location);
                } else
                {
                    jAlert(data.message, villa.labels.error);
                }
            }
        });
    },
    beforeFormSend: function ()
    {
        $('.guest-info').each(function () {
            var namePrefix = $(this).hasClass('adult') ? 'adults' : 'children';
            var num = parseInt($(this).index('.guest-info' + ($(this).hasClass('adult') ? '.adult' : '.child'))) + 1;
            $(this).find('input').not('[type=hidden]').each(function ()
            {
                var name = namePrefix + 'Info[' + num + '][' + $(this).attr('name') + ']';
                $(this).attr('name', name);
            });
        });
    },
    saveBooking: function (id, form)
    {
        if (!this.validate())
            return;
        this.beforeFormSend();

        var data = this.getFormData('saveBooking');

        if (form != undefined)
        {
            data.push(
                    {name: 'payment_form', value: 1},
                    {name: 'payment_form_type', value: $(form).attr('class')},
                    {name: 'custom_form_data', value: $(form).find('input#customData').val()}
            );
        }

        this.showOverlay(villa.labels.saving);

        $.ajax({
            async: false,
            type: 'POST',
            dataType: 'JSON',
            url: 'ajax.php?booking_id=' + bookingform.booking_id + '&edit_mode=' + bookingform.edit_mode,
            data: data,
            success: function (data)
            {
                if (data && data.result == 1)
                {
                    if (form == undefined)
                        bookingform.endBooking(data.id, data.edit_mode);
                    else
                    {
                        $(form).find('input#bookingId').val(data.id);
                        $(form).find('input#customData').val(data.custom);

                        if (data.sign != undefined && data.sign != false)
                            $(form).find('input#sign').val(data.sign);

                        form.submit();
                    }
                } else if (data == null)
                {
                    bookingform.hideOverlay();
                    jAlert(villa.labels.serverError, villa.labels.error);
                } else
                {
                    bookingform.hideOverlay();
                    jAlert(data.message, villa.labels.error);
                }
            }
        });
    },
    rememberBookingId: function (id, form)
    {
        $.ajax({
            async: false,
            type: 'POST',
            dataType: 'JSON',
            url: 'ajax.php',
            data: {
                controller: 'villa.module.hotel.controller.session',
                action: 'setBookingId',
                booking_id: id
            },
            success: function (data)
            {
                if (data.result == 1)
                    form.submit();
                else
                    jAlert(data.message, villa.labels.error);
            }
        });
    },
    endBooking: function (id, editMode)
    {
        jAlert(villa.labels.yourBookingAdded + id, villa.labels.message, function () {
            if (editMode == undefined || editMode == false)
                bookingform.resetBookingData();
            else
                location.assign('/mybookings/');//bookingform.hideOverlay();
        });
    },
    validateTransfer: function (parentTr)
    {
        var inputs = $(parentTr).find('.transfer-required:visible');

        for (var i = 0; i < inputs.length; i++)
        {
            if (!inputs.eq(i).val())
            {
                jAlert(villa.labels.fillAllTransferFields, villa.labels.error, function () {
                    inputs[i].focus();
                })
                return false;
            }
        }

        return true;
    },
    validate: function ()
    {
        var inputs = $('.guest-info.adult').eq(0).find('input:visible');
        console.log(inputs);
        for (var i = 0; i < inputs.length; i++)
        {
            if (!inputs.eq(i).val())
            {
                jAlert(villa.labels.wrongGuestData, villa.labels.error, function () {
                    inputs[i].focus();
                })
                return false;
            }
        }

        var transferEnabledInputs = $('.transfer-enabled');
        for (i = 0; i < transferEnabledInputs.length; i++)
        {
            if (transferEnabledInputs.eq(i).val() == 1)
            {
                var tr = transferEnabledInputs.eq(i).closest('tr');
                if (!this.validateTransfer(tr))
                    return false;
            }
        }

        return true;
    },
    resetBookingData: function ()
    {
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'ajax.php',
            data: {
                controller: 'villa.module.hotel.controller.session',
                action: 'resetAll'
            },
            success: function (data)
            {
                if (data.result == 1)
                {
                    location.assign('/mybookings/');
                } else
                {
                    jAlert(data.message, villa.labels.error);
                }
            }
        });
    },
    showOverlay: function (message)
    {
        if (message !== undefined)
            $('#bookingFormOverlay:hidden').find('div').text(message);

        $('#bookingFormOverlay:hidden').show();
    },
    hideOverlay: function ()
    {
        $('#bookingFormOverlay:visible').hide();
    }
}