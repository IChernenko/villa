$(document).ready(function(){
    questionClient.setDefaults();
});

questionClient = {
    defEmail: 0,
    defName: 0,
    defMessage: 0,
    
    //Вытаскиваем станд. значения инпутов("введите имя" и т.д.)
    setDefaults: function()
    {
        this.defName = $("#questions_name").val();
        this.defEmail = $("#questions_email").val();
        this.defMessage = $("#questions_message").text();
    },
    
    //Проверяем на (не)соответсвие стандартным значениям
    validate: function()
    {
        var name = $("#questions_name").val();
        var email = $("#questions_email").val();
        var message = $("#questions_message").val();
                
        if(name == this.defName)
            {
                alert('Enter name!');
                return false;
            }
        if(email == this.defEmail)
        {
            alert('Enter email!');
            return false;
        }
        if(message == this.defMessage)
            {
                alert('Enter question!');
                return false;
            }
            
        return true;
    },
    
    //Добавляем стили в вывод
    addStyles: function()
    {
        var rows = $("#prevQuestions").find("div");
        $(rows).each(function(){
            if(!$(this).hasClass("parent_0"))
                {
                    var name = $(this).find(".row-name");
                    $(name).css({"color":"#d28a24"});
                    var label = $(this).find(".row-message-label");
                    $(label).css({"display":"inline-block", "margin-left":"15px"});
                    $(label).html("Ответ:&nbsp;");
                }
        });
    },
    
    //Возвращаем инпутам станд. значения
    restoreDefaults: function()
    {
        $("#questions_name").val(this.defName);
        $("#questions_email").val(this.defEmail);
        $("#questions_message").val(this.defMessage);
    },
    
    //Отправляем запрос на сервер
    sendQuestion: function()
    {
        var check = this.validate();
        if(!check) return false;
        
        $.ajax({
            type: "POST",
            url: '/ajax.php',
            data: {
                controller: "module.questions.controller.questions",
                action: 'add',
                name: $("#questions_name").val(),
                email: $("#questions_email").val(),
                message: $("#questions_message").val()
            },
            success: function(data){
              $("#prevQuestions").html(data['result']);
              questionClient.addStyles();
              questionClient.restoreDefaults();
            }
        });
    }
}

//Вызов кнопкой
function sendQuestion()
{
    questionClient.sendQuestion();
}