rooms = {
    formSubmit: function()
    {        
        if(!this.validate()) return;
        
        var data = $('#editRoom').serializeArray();
        
        $.ajax({
            data: data,
            url: '/ajax.php',
            dataType : "json",
            success: function (data){ 
                if(data.result == 1)
                    jAlert('Изменения сохранены', 'Сообщение');
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    },
    
    validate: function()
    {
        var inputs = $('#editRoom').find('input[type=text], select');
        
        for(var i = 0; i < inputs.length; i++)
        {
            if(!inputs.eq(i).val() || inputs.eq(i).val() == 0)
            {
                jAlert('Не заполнено обязательное поле', 'Ошибка', function(){
                    inputs.eq(i).focus();
                });
                return false;
            }
        }
        
        return true;
    }
}


