prepayment = {
    controller: 'villa.module.hotel.controller.manage.prepayment',
    
    changeLang: function(obj)
    {        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'ajax.php',
            data: {                
                controller: prepayment.controller,
                action: 'drawForm',
                id: $('#id').val(),
                lang_id: $(obj).val()
            },
            success: function(data){
                if(data.result == 1)
                    $('.modal-body').html(data.html);
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    }, 
    formSubmit: function()
    {        
        var data = $('#editPrepType').serializeArray();
        
        $.ajax({
            data: data,
            url: '/ajax.php',
            dataType : "json",
            success: function (data){ 
                if(data.result == 1)
                    jAlert('Изменения сохранены', 'Сообщение');
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    }
}


