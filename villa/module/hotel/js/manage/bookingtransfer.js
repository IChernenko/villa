bookingtransfer = {
    controller: 'villa.module.hotel.controller.manage.bookingtransfer',
    edit: function(bId, tId)
    {
        $('#modalWindow').arcticmodal({
            type: 'ajax',
            url: 'ajax.php',
            ajax: {
                type: 'POST',
                dataType: 'JSON',
                data: {                
                    controller: bookingtransfer.controller,
                    action: 'drawForm',
                    booking_id: bId,
                    transfer_id: tId
                },
                success: function(data, el, response) 
                {
                    data.body.html(el);                    
                    $(el).find('.modal-body').html(response.html);
                }
            },
            afterClose: function()
            {
                 handbooks.formSubmit();
            }
        });
    },
    formSubmit: function()
    {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'ajax.php',
            data: $('#editBookTrans').serializeArray(),
            success: function(data){
                if(data.result == 1)
                {
                    jAlert('Изменения сохранены', 'Сообщение', function(){
                        $('#modalWindow').arcticmodal('close');
                    });
                }                    
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    }
}


