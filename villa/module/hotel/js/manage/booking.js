booking = {
    saved: true,
    paymentResidenceMax: 0,
    paymentServiceMax: 0,
    controller: 'villa.module.hotel.controller.manage.booking',
    arrival: false,
    departure: false,
    initDates: function ()
    {

        var msecDay = 1000 * 3600 * 24; //один день в миллисекундах
        var days = $('#nights').val();
        $('.date').datepicker({dateFormat: 'dd/mm/yy', minDate: 0});
        $('.time').timepicker();
        $('#arrivalDate, #departureDate').prop('disabled', false);

        $('#arrivalDate').datepicker({
            dateFormat: 'dd/mm/yy',
            minDate: new Date(),
            numberOfMonths: [1, 3],
            onSelect: function (dateText, inst)
            {
                var arriveDate = dateText.match(/\d+/g);
                // alert(arriveDate);
                var newDepTmp = new Date(arriveDate[2], arriveDate[1], arriveDate[0]);
                var newDep = new Date(newDepTmp.getTime() + msecDay * days);
                $('#departureDate').datepicker('setDate', newDep);

                booking.changeDates();
            }
        });

        $('#departureDate').datepicker({
            dateFormat: 'dd/mm/yy',
            minDate: 0,
            maxDate: '+6m',
            numberOfMonths: [1, 3],
            onSelect: function (dateText, inst)
            {
                booking.changeDates();
            }
        });
    },
    changeDates: function ()
    {
        booking.arrival = $('#formArrival').val();
        booking.departure = $('#formDeparture').val();

        $('#formArrival').val($('#arrivalDate').val());
        $('#formDeparture').val($('#departureDate').val());

        booking.formSubmit(true);
    },
    restoreDates: function ()
    {
        $('#formArrival').val(booking.arrival);
        $('#arrivalDate').val(booking.arrival);

        $('#formDeparture').val(booking.departure);
        $('#departureDate').val(booking.departure);
    },
    getFormData: function (action)
    {
        var data = $('#bookingEditForm').serializeArray();
        // console.log(data);
        data.push({
            name: 'controller', value: this.controller
        });

        if (action != undefined)
        {
            data.push({
                name: 'action', value: action
            });
        }

        return data;
    },
    enableForm: function ()
    {
        $('#bookingEditForm').find('input, select, textarea').not('.new-option').filter(':disabled').prop('disabled', false).addClass('temp-enabled');
        $('#bookingEditForm').find('a.btn').removeClass('disabled');
    },
    disableForm: function ()
    {
        $('#bookingEditForm').find('input, select, textarea').prop('disabled', true).addClass('form-disabled');
        $('#bookingEditForm').find('a.btn').addClass('disabled');
    },
    enableFormAfter: function ()
    {
        $('#bookingEditForm').find('input, select, textarea').filter('.form-disabled').not('.new-option').not('.temp-enabled').prop('disabled', false);
        $('#bookingEditForm').find('a.btn').removeClass('disabled');
    },
    changeStatus: function (bookingId, statusId)
    {
        var successStatus = 1;
        if ( statusId == 4 ) {

            var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).getTime();
            var selected = new Date($('#arrivalDate').datepicker('getDate')).getTime();
            if ( today < selected ) {
                successStatus = 0;
            }
        }
        if ( successStatus == 1 ) {
            $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: 'ajax.php',
                data: {
                    controller: booking.controller,
                    action: 'changeStatus',
                    booking_id: bookingId,
                    status_id: statusId
                },
                success: function (data)
                {
                    if (data.result == 1)
                        jAlert('Установлен статус: ' + data.status, 'Сообщение', function () {
                            booking.formRedraw();
                        });
                    else
                        jAlert(data.message, 'Ошибка');
                }
            });
        } else {
            jAlert('Стутус "проживания" можно установить только начиная с  даты проживания');
        }
    },
    cancel: function ()
    {
        if (!this.saved)
        {
            jConfirm('Вы действительно хотите отменить все изменения?', 'Подтвердите действие', function (r) {
                if (r)
                    booking.formRedraw();
            });
        }
    },
    close: function ()
    {
        if (this.saved)
            $('#modalWindow').arcticmodal('close');
        else
        {
            jConfirm('Вы действительно хотите закрыть форму без сохранения?', 'Подтвердите действие', function (r) {
                if (r)
                    $('#modalWindow').arcticmodal('close');
            });
        }
    },
    save: function (closeAfter)
    {
        if (closeAfter == undefined)
            closeAfter = false;

        this.enableForm();
        var data = this.getFormData('edit');
        this.disableForm();

        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'ajax.php',
            data: data,
            success: function (data)
            {
                if (data.result == 1)
                {
                    if (closeAfter)
                        $('#modalWindow').arcticmodal('close');
                    else
                    {
                        jAlert('Изменения сохранены', 'Сообщение', function () {
                            if (data.html)
                                $('#modalWindow .modal-body').html(data.html);
                                booking.initDates();
                            this.saved = true;
                        });
                    }
                } else
                {
                    jAlert(data.message, 'Ошибка', function () {
                        booking.enableFormAfter();
                    });
                }
            }
        });
    },
    formRedraw: function ()
    {
        var id = $('#bookingId').val();
        if (!id)
            return;

        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'ajax.php',
            data: {
                controller: booking.controller,
                action: 'drawForm',
                id: id
            },
            success: function (data)
            {
                if (data.result == 1)
                {
                    $('#modalWindow .modal-body').html(data.html);
                    booking.saved = true;
                    booking.initDates();
                } else
                {
                    jAlert(data.message, 'Ошибка');
                }
            }
        });
    },
    formSubmit: function (datesChanged)
    {
        if (datesChanged == undefined)
            datesChanged = false;

        this.enableForm();
        var data = this.getFormData('updateForm');
        this.disableForm();

        if (datesChanged)
            data.push({
                name: 'new_search', value: 1
            });

        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'ajax.php',
            data: data,
            success: function (data)
            {
                if (data.result == 1)
                {

                    $('#modalWindow .modal-body').html(data.html);
                    booking.saved = false;
                    booking.initDates();
                } else
                {
                    jAlert(data.message, 'Ошибка', function () {
                        if (datesChanged)
                            booking.restoreDates();
                        booking.enableFormAfter();
                    });
                }
            }
        });
    },
    changeAdult: function (num, obj)
    {
        if ($(obj).hasClass('first'))
        {
            $(obj).prop('checked', true);
            jAlert('Вы не можете удалить этого гостя, т.к. он является заказчиком', 'Ошибка');
            return;
        }

        var enabled = $(obj).prop('checked') ? 1 : 0;
        $(obj).siblings('input.enable.hidden').val(enabled);

        if (!enabled)
        {
            $('.adult-block-' + num).find('input[type=checkbox]').prop('checked', false);
            $('.adult-block-' + num).find('input.enable.hidden').val(0);
        }

        this.formSubmit();
    },
    changeChild: function (num, obj)
    {
        var enabled = $(obj).prop('checked') ? 1 : 0;
        $(obj).siblings('input.enable.hidden').val(enabled);

        if (!enabled)
        {
            $('.child-block-' + num).find('input[type="checkbox"]').prop('checked', false);
            $('.child-block-' + num).find('input.enable.hidden').val(0);
        }

        this.formSubmit();
    },
    changeOptionCheckbox: function (obj)
    {
        var enabled = $(obj).prop('checked') ? 1 : 0;
        $(obj).siblings('input.enable.hidden').val(enabled);

        this.formSubmit();
    },
    changeTransfer: function (object)
    {
        var type = $(object).children(":selected").attr("type");
        $(object).closest('label').siblings('.transfer-type').val(type);

        var cost = $(object).children(":selected").attr("cost");
        $(object).closest('label').siblings('.transfer-cost').val(cost);

        var transportType = $(object).children(":selected").attr("transport-type");
        $(object).closest('label').siblings('span.transport-type').hide();
        $(object).closest('label').siblings('span.transport-type.type-' + transportType).show();
    },
    showAddOptionRow: function (obj)
    {
        $('#bookingEditForm .cancel-btn').click();

        $(obj).hide();
        $(obj).siblings('.add-option-title').show();
        $(obj).closest('table').find('tr.add-option').show().find('input, select, textarea').prop('disabled', false);
    },
    hideAddOptionRow: function (obj)
    {
        $(obj).closest('table').find('.add-option-title').hide();
        $(obj).closest('table').find('.add-btn').show();
        $(obj).closest('table').find('tr.add-option').hide().find('input, select, textarea').prop('disabled', true);
    },
    addOption: function (optionType)
    {
        if (optionType == undefined)
            return;

        var action = false;
        switch (optionType)
        {
            case 'service':
                action = 'addService';
                break;

            case 'excursion':
                action = 'addExcursion';
                break;

            case 'transfer':
                action = 'addTransfer';
                break;

            case 'extra_service':
                action = 'addExtraService';
                break;
        }

        if (!action)
            return;

        this.enableForm();
        var data = this.getFormData(action);
        this.disableForm();


        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'ajax.php',
            data: data,
            success: function (data)
            {
                if (data.result == 1) {
                    $('#modalWindow .modal-body').html(data.html);
                    booking.initDates();
                } else {
                    jAlert(data.message, 'Ошибка');
                }

            }
        });
    },
    delOption: function (obj)
    {
        $(obj).closest('tr').remove();

        this.formSubmit();
    },
    drawScanForm: function (bookingId, guestId)
    {
        if (!bookingId || !guestId)
            return;

        var data = {
            controller: booking.controller,
            action: 'drawScanForm',
            booking_id: bookingId,
            guest_num: guestId,
            scan: $('#scan_' + guestId).val()
        };

        this.secondModal(data);
    },
    drawPaymentForm: function (id, type)
    {
        if (!id)
            return;

        if (!this.saved)
        {
            jAlert('<b>Найдены несохраненные данные.</b> <br/> Сохраните бронирование или ' +
                    'отмените изменения прежде, чем добавлять оплату', 'Сообщение');
            return;
        }

        var data = {
            controller: booking.controller,
            action: 'drawPaymentForm',
            id: id,
            type: type,
            totals: {
                servicePayable: $('#servicePayable').val(),
                servicePaid: $('#servicePaid').val(),
                residencePayable: $('#residencePayable').val(),
                residencePaid: $('#residencePaid').val()
            }
        };

        $('#secondModal').css({'width': '520px'});
        this.secondModal(data);
    },
    submitPayment: function ()
    {
        var data = $('#bookingPaymentForm').serializeArray();

        data.push(
                {name: 'residenceMax', value: this.paymentResidenceMax},
                {name: 'serviceMax', value: this.paymentServiceMax}
        );

        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'ajax.php',
            data: data,
            success: function (data)
            {
                if (data.result == 1)
                    $('#secondModal').arcticmodal('close');
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    },
    drawPaymentHistory: function (id)
    {
        if (!id)
            return;

        var data = {
            controller: booking.controller,
            action: 'drawPaymentHistory',
            id: id
        };

        this.secondModal(data);
    },
    secondModal: function (data)
    {
        $('#secondModal').arcticmodal({
            type: 'ajax',
            url: 'ajax.php',
            ajax: {
                type: 'POST',
                dataType: 'JSON',
                data: data,
                success: function (data, el, response)
                {
                    data.body.html(el);
                    $(el).find('.modal-body').html(response.html);
                }
            },
            afterClose: function ()
            {
                $('#secondModal').attr('style', '').find('.modal-body').html('');
                booking.paymentResidenceMax = 0;
                booking.paymentServiceMax = 0;
                booking.formRedraw();
            }
        });
    },
    addFormInitBasic: function (onchange)
    {
        var inputs = $('#bookingAddForm').find('input, select').filter('.basic-data');

        if (onchange == undefined)
        {
            $('#basicContinueBtn').click(function () {
                if (booking.addFormValidate(inputs, true))
                    booking.getRoomParams();
            });

            $("#bookingAddForm .datepick").datepicker({
                numberOfMonths: [1, 3],
                dateFormat: 'dd/mm/yy'
            });
        } else if (onchange)
        {
            $('#basicContinueBtn').closest('tr').hide();
            $('#roomContinueBtn').closest('tr').show();

            inputs.unbind('change');
            inputs.change(function () {
                if (booking.addFormValidate(inputs, true))
                    booking.getRoomParams();
            });
        }
    },
    addFormInitRoom: function (onchange)
    {
        var inputs = $('#bookingAddForm').find('input, select').filter('.room-data');

        if (onchange == undefined)
        {
            $('#roomContinueBtn').unbind('click');
            $('#roomContinueBtn').click(function () {
                if (booking.addFormValidate(inputs, true))
                    booking.calcRoomPrice();
            });
        } else if (onchange)
        {
            $('#roomContinueBtn').closest('tr').hide();

            inputs.unbind('change');
            inputs.change(function () {
                if (booking.addFormValidate(inputs, true))
                    booking.calcRoomPrice();
            });
        }
    },
    addFormValidate: function (inputs, allowAlert)
    {
        console.log('validate');
        if (allowAlert == undefined)
            allowAlert = false;

        for (var i = 0; i < inputs.length; i++)
        {
            if (!inputs.eq(i).val())
            {
                if (allowAlert)
                    jAlert('Заполните все поля на форме', 'Ошибка', function () {
                        inputs.eq(i).focus();
                    });

                return false;
            }
        }

        return true;
    },
    getRoomParams: function ()
    {
        $('tr.add-end').hide();
        var data = $('#bookingAddForm').serializeArray();
        data.push(
                {name: 'controller', value: booking.controller},
                {name: 'action', value: 'getRoomData'}
        );
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'ajax.php',
            data: data,
            success: function (data)
            {
                if (data.result == 1)
                {
                    $('#roomSelectTd').html(data.html);
                    $('tr.room-data').show();

                    booking.addFormInitBasic(true);
                    booking.addFormInitRoom();
                } else
                {
                    jAlert(data.message, 'Ошибка');
                }
            }
        });
    },
    calcRoomPrice: function ()
    {
        var data = $('#bookingAddForm').serializeArray();
        data.push(
                {name: 'controller', value: booking.controller},
                {name: 'action', value: 'getResidenceCost'}
        );
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'ajax.php',
            data: data,
            success: function (data)
            {
                if (data.result == 1)
                {
                    $('#roomCostTd').html(data.dayPrice);
                    $('#residenceCostTd').html(data.allPrice);
                    $('tr.add-end').show();

                    booking.addFormInitRoom(true);
                } else
                {
                    jAlert(data.message, 'Ошибка');
                }
            }
        });
    },
    addFormValidateAll: function ()
    {
        var inputs = $('#bookingAddForm').find('input, select');
        return this.addFormValidate(inputs, true);
    },
    addBooking: function ()
    {
        if (!this.addFormValidateAll())
            return;

        var data = $('#bookingAddForm').serializeArray();
        data.push(
                {name: 'controller', value: booking.controller},
                {name: 'action', value: 'addBooking'}
        );

        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'ajax.php',
            data: data,
            success: function (data)
            {
                if (data.result == 1)
                {
                    jAlert('Бронирование добавлено', 'Сообщение', function () {
                        $('#modalWindow .modal-body').html(data.html);
                    });
                } else
                {
                    jAlert(data.message, 'Ошибка');
                }
            }
        });
    },
    openVersions: function (id)
    {
        $.ajax({
            url: 'ajax.php',
            type: 'POST',
            dataType: 'JSON',
            data: {
                controller: booking.controller,
                action: 'drawVersions',
                id: id
            },
            success: function (data) {
                if (data.result == 1)
                {
                    $('#bookingVersionsList').html(data.html).slideDown(300);
                    $('#bookingVersions > a.btn-inverse').hide();
                    $('#bookingVersions > a.btn-danger').show();
                } else
                {
                    jAlert(data.message, 'Ошибка');
                }
            }
        });
    },
    closeVersions: function ()
    {
        $('#bookingVersionsList').empty().slideUp(300);
        $('#bookingVersions > a.btn-inverse').show();
        $('#bookingVersions > a.btn-danger').hide();
        booking.formRedraw();
    },
    showVersion: function (id, version)
    {
        if (!this.saved)
        {
            jAlert('<b>Найдены несохраненные данные.</b> <br/> Сохраните бронирование или ' +
                    'отмените изменения прежде, чем просмативать версии', 'Сообщение');
            return;
        }

        $.ajax({
            url: 'ajax.php',
            type: 'POST',
            dataType: 'JSON',
            data: {
                controller: booking.controller,
                action: 'showVersion',
                id: id,
                version: version
            },
            success: function (data) {
                if (data.result == 1)
                {
                    $('#bookingEditForm').replaceWith(data.html);
                    booking.initDates();
                } else
                {
                    jAlert(data.message, 'Ошибка');
                }
            }
        });
    },
    restoreVersion: function (id, version)
    {
        jConfirm('Вы действительно хотите восстановить версию #' + version + ' ?', 'Подтвердите действие', function (r) {
            if (r)
            {
                $.ajax({
                    url: 'ajax.php',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        controller: booking.controller,
                        action: 'restoreVersion',
                        id: id,
                        version: version
                    },
                    success: function (data) {
                        if (data.result == 1)
                        {
                            jAlert('Версия #' + version + ' восстановлена. ', 'Сообщение', function () {
                                booking.formRedraw();
                            });
                        } else
                        {
                            jAlert(data.message, 'Ошибка');
                        }
                    }
                });
            }
        });
    },
    deleteVersion: function (obj, id, version)
    {
        jConfirm('Вы действительно хотите удалить версию #' + version + ' ?', 'Подтвердите действие', function (r) {
            if (r)
            {
                $.ajax({
                    url: 'ajax.php',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        controller: booking.controller,
                        action: 'deleteVersion',
                        id: id,
                        version: version
                    },
                    success: function (data) {
                        if (data.result == 1)
                        {
                            jAlert('Версия удалена', 'Сообщение', function () {
                                $(obj).closest('tr').remove();
                            });
                        } else
                        {
                            jAlert(data.message, 'Ошибка');
                        }
                    }
                });
            }
        });
    },
    editFromOutside: function (id, callback)
    {
        if (!id)
            return false;

        var data = {
            controller: 'villa.module.hotel.controller.manage.booking',
            action: 'drawForm',
            id: id
        };

        $('#modalWindow').arcticmodal({
            type: 'ajax',
            url: 'ajax.php',
            ajax: {
                type: 'POST',
                dataType: 'JSON',
                data: data,
                success: function (data, el, response)
                {
                    data.body.html(el);
                    //var html = (response.html) ? response.html : response;
                    $(el).find('.modal-body').html(response.html);
                }
            },
            afterOpen: function ()
            {
                $('#modalWindow').css('width', '1210px');
            },
            afterClose: function ()
            {
                if (callback !== undefined)
                    callback();
            }
        });
    }
};
