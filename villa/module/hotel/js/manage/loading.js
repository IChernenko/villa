loading = {
    enter: false,
    scrollStartX: 0,
    scrollStartY: 0,
    tableOffsetLeft: 0,
    tableOffsetTop: 0,
    init: function ()
    {
        $("#loadingDatesDiv .datepick").datepicker({
            dateFormat: "dd/mm/yy",
            numberOfMonths: [1, 3]
        });

        this.scrollStartX = $(document).scrollLeft();
        this.scrollStartY = $(document).scrollTop();

        this.tableOffsetLeft = $('#hotelLoadingTable').offset().left;
        this.tableOffsetTop = $('#hotelLoadingTable').offset().top;

        // if ($(document).scrollTop() > this.tableOffsetTop)
        // {
        //     $('#dateList').show();
        //     $('#loadingDatesDiv').addClass('position_fix');
        //     $('#hotelLoadingTable').css('margin-top' , '-14px');
        // }

        var numbersTableTop = this.tableOffsetTop - this.scrollStartY;
        $('#roomNumbers').css({'top': numbersTableTop + 'px', 'left': '0px'});

        var datesTableLeft = this.tableOffsetLeft - this.scrollStartX;
        $('#dateList').css({'left': datesTableLeft + 'px' , 'top': '140px'});
        var temp = 0;
        $(document).scroll(function () {

            if ($(document).scrollTop() > loading.tableOffsetTop)
            {
                $('#dateList').show();
                $('#loadingDatesDiv').addClass('position_fix');
                $('#loadingDatesDiv1').show();
                if ( temp == 0 ) {
                    $('#hotelLoadingTable').css('margin-top' , '-14px');
                    temp = 1;
                }


            }

            if ($(document).scrollTop() < loading.tableOffsetTop)
            {
                $('#loadingDatesDiv1').hide();
                $('#dateList').hide();
                $('#loadingDatesDiv').removeClass('position_fix');

                if ( temp == 1 ) {
                    $('#hotelLoadingTable').css('margin-top' , '0');
                    temp = 0;
                }

            }

            var diffY = loading.scrollStartY - $(document).scrollTop();
            if (diffY !== 0)
            {
                var left = $('#roomNumbers').offset().left;
                var top = loading.tableOffsetTop;
                $('#roomNumbers').offset({top: top, left: left});

                loading.scrollStartY = $(document).scrollTop();
            }

            var diffX = loading.scrollStartX - $(document).scrollLeft();
            if (diffX !== 0 && $('#dateList').is(':visible'))
            {
                $('#dateList').offset({left: loading.tableOffsetLeft});

                loading.scrollStartX = $(document).scrollLeft();
            }
        });
    },
    filter: function ()
    {
        $.ajax({
            type: 'POST',
            url: '/ajax.php',
            dataType: "json",
            data: {
                controller: 'villa.module.hotel.controller.manage.loading',
                action: 'drawAjax',
                dateFrom: $('#dateFrom').val(),
                dateTo: $('#dateTo').val(),
                service: $("#service").find(":selected").val()
            },
            success: function (data) {
                if (data.result === 1)
                    $('#loadingMain').replaceWith(data.html);
                else
                    console.log(data);
            }
        });
    },
    initialRequest: function(){
        if(
                !$('#dateFrom').val() ||
                !$('#dateTo').val() ||
                !$("#service").find(":selected").val()
        ){
            setTimeout(function () {loading.initialRequest();}, 200);
            return false;
        }

        loading.filter();
    }
};

//initial ajax req
setTimeout(function () {loading.initialRequest();}, 200);
