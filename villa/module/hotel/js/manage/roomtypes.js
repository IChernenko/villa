roomtypes = {
    controller: 'villa.module.hotel.controller.manage.roomtypes',
    
    changeLang: function(obj)
    {        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'ajax.php',
            data: {                
                controller: roomtypes.controller,
                action: 'drawForm',
                id: $('#typeId').val(),
                lang_id: $(obj).val()
            },
            success: function(data){
                if(data.result == 1)
                    $('.modal-body').html(data.html);
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    }, 
    formSubmit: function()
    {        
        $("textarea[name=description]").elrte('updateSource');
        var data = $('#apTypeForm').serializeArray();
        
        $.ajax({
            data: data,
            url: '/ajax.php',
            dataType : "json",
            success: function (data){ 
                if(data.result == 1)
                {
                    if(data.html) $('.modal-body').html(data.html);
                    jAlert('Изменения сохранены', 'Сообщение');
                }
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    },
    delImage: function(obj, id, image)
    {
        $.ajax({
            url: '/ajax.php',
            type: 'post',
            dataType : "json",
            data:{
                controller: roomtypes.controller,
                action: 'delImage',
                typeId: id, 
                image: image
            },
            success: function (data){ 
                if(data.result == 1)
                    $(obj).closest('tr').remove();
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    },
    changeImageOrder: function(typeId, drawOrder, orderFactor)
    {
        $.ajax({
            url: '/ajax.php',
            dataType: "json",
            type: 'post',
            data:{
                controller: roomtypes.controller,
                action: 'changeOrder',
                typeId: typeId, 
                drawOrder: drawOrder, 
                orderFactor: orderFactor
            },
            success: function (data){
                if(data.result == 1)
                    $('.tab-pane.active').html(data.html);
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    },
    saveEquipment: function()
    {       
        var values = new Array();
        var bold = new Array();
        var fontsize = new Array();
        
        $.each($("input[name='equipment[]']:checked"), function() {
            values.push($(this).val());
            var curId = $(this).get(0).id;
            var curIdArr = curId.split('_');
            var curIdNum = curIdArr[1];

            var boldT = $("#equipmentBold_"+curIdNum).attr('checked');
            if(boldT == 'checked'){
                bold.push(1);
            }else{
                bold.push(0);
            }

            fontsize.push( $("#equipmentFontsize_"+curIdNum).val() );
        });
        
        var valuesStr = values.join(',');
        var boldStr = bold.join(',');
        var fontsizeStr = fontsize.join(',');
        
        $.ajax({
            url: '/ajax.php',
            dataType : "json",
            data:{
                controller: roomtypes.controller,
                action: 'saveEquipment', 
                typeId: $('#typeId').val(), 
                equipment: valuesStr, 
                bold: boldStr, 
                fontsize: fontsizeStr
            },
            success: function (data){
                if(data.result == 1)
                    $('.tab-pane.active').html(data.html);
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    },
    changeSortOrderEquipment: function(id, k)
    {
        if( typeof( $('#aptEquipmentLabel_'+(id+k)).get(0) ) == 'undefined'){return;}
        
        var curHtml = $('#aptEquipmentLabel_'+id).html();
        var toHtml = $('#aptEquipmentLabel_'+(id+k)).html();
        
        $('#aptEquipmentLabel_'+(id+k)).html(curHtml);
        $('#aptEquipmentLabel_'+id).html(toHtml);
    },    
    loadContent: function(obj)
    {        
        var event_id = $(obj).attr('href');
        var data = {
            controller: roomtypes.controller
        };
        if(!(data.action = roomtypes.getAction(event_id)))           
            return false;
        
        data.typeId = $('#typeId').val();
        
        $(event_id).html(roomtypes.loaderHtml);
        
        $.ajax({
            type: 'POST',
            data: data,
            url: '/ajax.php',
            dataType : "json",
            success: function (data){ 
                if(data.result==1){
                    $(event_id).html(data.html);
                }else{
                    $(event_id).html(data.message);
                }
            }
        })
        
    },
    getAction: function(id)
    {        
        var action = false;
        switch(id)
        {
            case '#edit':
                break;
            case '#images':
                action = 'drawImages';
                break;
            case '#equipment':
                action = 'drawEquipment';
                break;
            default:
                break;
        }
        
        return action;
    },
    loaderHtml:'<h1>Loading...</h1>'+
        '<div class="progress progress-striped active" style="width:150px; margin:0 auto;">'+
        '<div class="bar" style="width: 100%;"></div></div>'
}