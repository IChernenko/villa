transfer = {
    controller: 'villa.module.hotel.controller.manage.transfer',

    editSubmit: function(obj)
    {
        if($(obj).hasClass('disabled')) return false;
        //$('#editBookingTransfer a, #editBookingTransfer select').addClass('disabled');

        $.ajax({
            url: 'ajax.php',
            type: 'POST',
            dataType: 'json',
            data: $('#editBookingTransfer').serializeArray(),
            success: function(data){
                if(data.result == 1)
                {
                    jAlert('Изменения сохранены', 'Сообщение', function(){
                        $('#editBookingTransfer a, #editBookingTransfer select').removeClass('disabled');
                    });
                }
                else
                    console.log(data);
            }
        });
    },
    changeLang:function()
    {
        $.ajax({
            type: 'POST',
            url: '/ajax.php',
            data: {
                lang: $('#lang').val(),
                id: $('#transferId').val(),
                controller: "villa.module.hotel.controller.manage.transfer",
                action: 'drawForm'
            },
            dataType: 'JSON',
            success: function (data){
                if(data.result==1)                    
                    $('#modalWindow .modal-body').html(data.html);                 
                else
                    console.log(data);
            }
        });
    },

    loadContent: function(object)
    {
        var event_id = $(object).attr('href');
        var data = {
            controller: this.controller
        };

        if(!(data.action = transfer.getAction(event_id)))
            return false;
        data.transferId = $('#transferId').val();
        jQuery(event_id).html(transfer.loaderHtml);

        jQuery.ajax({
            type: 'POST',
            data: data,
            url: '/ajax.php',
            dataType : "json",
            success: function (data){
                if(data.result==1){
                    $(event_id).html(data.html);
                }else{
                    $(event_id).html(data.message);
                }
            }
        });
    },

    getAction: function(id)
    {
        var action = false;
        switch(id)
        {
            case '#edit':
                break;
            case '#images':
                action = 'drawImages';
                break;
            default:
                break;
        }
        return action;
    },

    delImage: function(obj, id, image)
    {
        $.ajax({
            url: '/ajax.php',
            type: 'post',
            dataType : "json",
            data:{
                controller: 'villa.module.hotel.controller.manage.transfer',
                action: 'delImage',
                transferId: id,
                image: image
            },
            success: function (data){
                if(data.result == 1)

                    $(obj).closest('tr').remove();
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    },

    changeImageOrder: function(transferId, drawOrder, orderFactor)
    {
        $.ajax({
            url: '/ajax.php',
            dataType: "json",
            type: 'post',
            data:{
                controller: transfer.controller,
                action: 'changeOrder',
                transferId: transferId,
                drawOrder: drawOrder,
                orderFactor: orderFactor
            },
            success: function (data){
                if(data.result == 1)
                    $('.tab-pane.active').html(data.html);
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    },


    loaderHtml:'<h1>Loading...</h1>'+
        '<div class="progress progress-striped active" style="width:150px; margin:0 auto;">'+
        '<div class="bar" style="width: 100%;"></div></div>'
}

