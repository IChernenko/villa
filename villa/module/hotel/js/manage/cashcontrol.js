cashcontrol = {
    selectRow:function(id){
        $("#cashRow_"+curRow).css('background-color', '#FFFFFF');
        $("#cashRow_"+id).css('background-color', '#3366CC');
        curRow = id;
        
    },
    selfSelect: function(){
        this.selectRow(curUser);
        this.showDepositForm();
    },
    showDepositForm: function(){
        if(curRow!=0){
            $("#depositForm").show();
        }else{
            jAlert('Запись не выбрана', 'Ошибка');
        }
    },
    hideDepositForm: function(){
       $("#depositForm").hide();
    },
    hideDepositForm: function(){
        $("#depositForm").hide();
    },
    doDepositAction :function(){
        var userId = curRow;
        var summ = $("#summ").val();
        var description = $("#description").val();
        
        if(summ==''){
            jAlert('Заполните обязательные поля', 'Ошибка');
            return;
        }
        
        $.ajax({
            data:{userId:userId, summ:summ, description:description},
            url: '/ajax.php?action=addDeposit&controller=villa.module.hotel.controller.manage.cashcontrol',
            dataType : "json",
            success: function (data){
                if(data['result'] && data['result']==1){
                    location.reload();
                }
            }
        });
    }
}