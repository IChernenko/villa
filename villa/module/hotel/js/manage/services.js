services = {
    controller: 'villa.module.hotel.controller.manage.services',
    langName: '',
    editors: [],
    
    formInit: function()
    {
        this.langName = '';
        this.editors = [];
        
        $('#editTabs a').click(function(e) {
            e.preventDefault();
            $(this).tab('show');
            services.loadContent(this);
        });
        
        $('textarea.editor').each(function(elem){        
            services.editors.push($(this).attr('id'));
        });
        
        services.initCKEditor(services.langName);
    },
    
    initCKEditor: function(language)
    {
        for(var i=0; i<services.editors.length; i++)
        {            
            CKEDITOR.replace(services.editors[i],
            {
                language: language
            });
        }
    },
    
    changeLang: function(obj)
    {
        if($(obj).hasClass('disabled')) return false;
        $('#editBookingoption a, #editBookingoption select').addClass('disabled');
        
        var data = {
            controller: 'villa.module.hotel.controller.manage.services',
            action: 'drawForm',
            lang_id: $(obj).val(),
            id: $('#serviceId').val()
        }
        
        $.ajax({
            url: 'ajax.php',
            type: 'POST',
            dataType: 'JSON',
            data: data,
            success:function(data)
            {
                if(data.result == 1)
                {
                    $('#modalWindow .modal-body').html(data.html);
                }
                else console.log(data);
            }
        });
    },
    
    editSubmit: function(obj)
    {        
        if($(obj).hasClass('disabled')) return false;
        $('#editBookingoption a, #editBookingoption select').addClass('disabled');
        
        for(var i=0; i<services.editors.length; i++)
        {
            var name = services.editors[i];
            var html = CKEDITOR.instances[name].getData();
            $('textarea#'+name).val(html);
        }
        
        $.ajax({
            url: 'ajax.php',
            type: 'POST',
            dataType: 'json',
            data: $('#editBookingoption').serializeArray(),
            success: function(data){
                if(data.result == 1)
                {
                    jAlert('Изменения сохранены', 'Сообщение', function(){
                        $('#modalWindow .modal-body').html(data.html);
                    });                    
                }
                else
                    console.log(data);
            }
        });
    },

    loadContent: function(object)
    {
        var event_id = $(object).attr('href');
        var data = {
            controller: 'villa.module.hotel.controller.manage.services'
        };

        if(!(data.action = services.getAction(event_id))) return false;
        data.serviceId = $('#serviceId').val();

        data.reusable = 0;
        if($('input[name="reusable"]').is(":checked"))
        {
            data.reusable = 1;
        }

        $(event_id).html(services.loaderHtml);

        $.ajax({
            type: 'POST',
            data: data,
            url: '/ajax.php',
            dataType : "json",
            success: function (data){
                if(data.result==1){
                    $(event_id).html(data.html);
                }else{
                    $(event_id).html(data.message);
                }
            }
        });
    },

    getAction: function(id)
    {
        var action = false;

        switch(id)
        {
            case '#edit':
                break;
            case '#images':
                action = 'drawImages';
                break;
            default:
                break;
        }
        return action;
    },

    changeImageOrder: function(serviceId, imageId, drawOrder, orderFactor)
    {
        $.ajax({
            url: '/ajax.php',
            dataType: "json",
            type: 'post',
            data:{
                controller: services.controller,
                action: 'changeOrder',
                service_id: serviceId,
                image_id: imageId,
                draw_order: drawOrder,
                order_factor: orderFactor
            },
            success: function (data){
                if(data.result == 1)
                    $('.tab-pane.active').html(data.html);
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    },

    delImage: function(obj, id, image)
    {
        $.ajax({
            url: '/ajax.php',
            type: 'post',
            dataType : "json",
            data:{
                controller: services.controller,
                action: 'delImage',
                id: id,
                image: image
            },
            success: function (data){
                if(data.result == 1)
                    $(obj).closest('tr').remove();
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    },

    editEnd: function()
    {
        $('#modalWindow').arcticmodal('close');
    },

    loaderHtml:'<h1>Loading...</h1>'+
        '<div class="progress progress-striped active" style="width:150px; margin:0 auto;">'+
        '<div class="bar" style="width: 100%;"></div></div>'

}


