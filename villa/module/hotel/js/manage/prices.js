prices = {
    controller: 'villa.module.hotel.controller.manage.prices',
    edit: function(obj)
    {
        $(obj).closest('tr').find('input[type=text]').prop('disabled', false);
        $(obj).siblings().andSelf().hide();
        $(obj).siblings('.save-btn, .cancel-btn').show();
    },
    cancel: function()
    {
        this.refresh();
    },
    save: function(id, obj)
    {
        this.btnDisable();
        
        var tr = $(obj).closest('tr');
        var data = {
            controller: this.controller,
            action: 'edit',
            id: id,
            entity_type_id: $(tr).find('input.entity-type').val(),
            entity_id: $(tr).find('input.entity-id').val(),
            prep_type: $(tr).find('input.prep-type').val(),
            standardPrice: $(tr).find('input.standard-price').val(),
            displayedPrice: $(tr).find('input.displayed-price').val()
        };
        
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'ajax.php',
            data: data,
            success: function(data)
            {
                if(data.result == 1)
                    $('#pricesManageMain').replaceWith(data.html);
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    },
    del: function(id)
    {
        this.btnDisable();
        
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'ajax.php',
            data: {
                controller: prices.controller,
                action: 'del',
                id: id
            },
            success: function(data)
            {
                if(data.result == 1)
                    $('#pricesManageMain').replaceWith(data.html);
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    },
    openDates: function(id)
    {
        $('#modalWindow').arcticmodal({
            type: 'ajax',
            url: 'ajax.php',
            ajax: {
                type: 'GET',
                dataType: 'JSON',
                data: {
                    controller: prices.controller,
                    action: 'drawDates',
                    price_id: id
                },
                success: function(data, el, response)
                {
                    data.body.html(el);
                    $(el).find('.modal-body').html(response.html);
                }
            },
            afterClose: function(data, el) {                
                prices.refresh();
            }
        });
    },
    addDate: function()
    {
        $('#pricesDates tr.new').show();
    },
    editDate: function(obj)
    {
        $(obj).closest('tr').find('input[type=text], input[type=checkbox]').prop('disabled', false);
        $(obj).siblings().andSelf().hide();
        $(obj).siblings('.save-btn, .cancel-btn').show();
    },
    dateSave: function(id, obj)
    {
        this.btnDisable();
        
        var tr = $(obj).closest('tr');
        var data = {
            controller: this.controller,
            action: 'editDate',
            id: id,
            price_id: $('#priceId').val(),
            dateFrom: $(tr).find('input.date-from').val(),
            dateTo: $(tr).find('input.date-to').val(),
            price: $(tr).find('input.price').val(),
            disabled: $(tr).find('input.is_disabled:checked').length ? 1 : 0
        };
        
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'ajax.php',
            data: data,
            success: function(data)
            {
                if(data.result == 1)
                    $('.modal-body').html(data.html);
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    },
    dateCancel: function()
    {
        this.refreshDates();
    },
    delDate: function(id)
    {
        this.btnDisable();
        
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'ajax.php',
            data: {
                controller: prices.controller,
                action: 'delDate',
                id: id,
                price_id: $('#priceId').val()
            },
            success: function(data)
            {
                if(data.result == 1)
                    $('.modal-body').html(data.html);
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    },
    refresh: function()
    {
        this.btnDisable();
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'ajax.php',
            data: {
                controller: prices.controller,
                action: 'drawAjax'
            },
            success: function(data)
            {
                if(data.result == 1)
                    $('#pricesManageMain').replaceWith(data.html);
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    },
    refreshDates: function()
    {
        this.btnDisable();
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'ajax.php',
            data: {
                controller: prices.controller,
                action: 'drawDates',
                price_id: $('#priceId').val()
            },
            success: function(data)
            {
                if(data.result == 1)
                    $('.modal-body').html(data.html);
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    },
    btnDisable: function()
    {
        $('#pricesManageMain').find('a').addClass('disabled');        
    }
};