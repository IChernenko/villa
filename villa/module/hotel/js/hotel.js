hotel = {
    roomSearch: function()
    {
        var arriveDate = $('#arriveDate').val();
        if (arriveDate == '')
        {
            jAlert(villa.labels.noArriveDate, villa.labels.error);
            return;
        }

        var departureDate = $('#departureDate').val();
        if (departureDate == '')
        {
            jAlert(villa.labels.noDepartureDate, villa.labels.error);
            return;
        }

        var adults = $('#adults').val();
        var children = $('#children').val();

        $.ajax({
            type: 'POST',
            url: '/ajax.php',
            data: {
                controller: 'villa.module.hotel.controller.session',
                action: 'setBookingBase',
                arriveDate: arriveDate,
                departureDate: departureDate,
                adults: adults,
                children: children
            },
            success: function(data)
            {
                if(data.result == 1)
                {
                    location.assign('/room-search-result/');
                }
                else
                {
                    console.log(data);
                    jAlert(data.errorMessage);
                }
            }
        });
    }
}

/**
 * Отмена бронирования
 */
function hotelCancelBooking(id, btn) {
    btn.disabled = true;

    $.ajax({
        type: "POST",
        url: '/ajax.php',
        data: {
            controller: "villa.module.hotel.controller.booking",
            action: 'cancel',
            id: id
        },
        success: function(data) {
            if (data.result == 1) {
                $("#b_booking_button_"+data.id).html(data.message);
            }
        }
    });
}
/**
 * Восстановление отменённого бронирования
 */
function hotelResumeBooking(id, btn) {
    if(!confirm("Возобновить бронь ?")) return false;
    btn.disabled = true;

    $.ajax({
        type: "POST",
        url: '/ajax.php',
        data: {
            controller: "villa.module.hotel.controller.booking",
            action: 'resume',
            id: id
        },
        success: function(data) {
            if (data.result == 1) {
                $("#b_booking_button_"+data.id).html(data.message);
            }
        }
    });
}

/**
 * Редактирование бронирования
 */
function hotelEditBooking(id, btn)
{
    location.assign('/edit/'+id);
}


/**
 * Инициализация формы бронирования
 */
function hotelBookingFormInit() {
    if(Number($('#totalPaid b').text()) != 0)
    {
        $('#remainderPaid :submit').attr('disabled', 'true');
    }

    if(Number($('#totalServicesPaid b').text()) != 0)
    {
        $('#remainderServicesPaid :submit').attr('disabled', 'true');
    }

    if(Number($('#totalResidencePaid b').text()) != 0)
    {
        $('#remainderResidencePaid :submit').attr('disabled', 'true');
    }

    /**
     * Функция - обработчик нажатия кнопки "Печатать предложение"
     */
    $('#printOfferButton').click(function()
    {
        printPage(false);
    });

    /**
     * Функция - обработчик нажатия кнопки "Печатать все"
     */
    $('#printAllButton').click(function()
    {
        printPage(true);
    });
}

/**
 * Функция реализующая подготовку и распечатку документа
 */

function printPage(printStatus)
{
    $('body').append('<div id="printModal">Подготовка и печать документа</div>');
    $('#printModal').css("opacity", 0);
    $('#printModal').animate({opacity:1}, 300, function()
    {
        $('body').append('<iframe width="1" height="1" id="printFrame" />');
        var printFrame = document.getElementById('printFrame');
        if (!printFrame)
        {
            jAlert("Ошибка: Документ для печати не сформирован.");
            return;
        }

        // Применение стилей к фрейму с документом
        var printFrameHead = $('#printFrame').contents().find('head');
        if(printFrameHead != null)
        {
            printFrameHead.append($('style, link[rel=stylesheet]').clone());
        }

        // Добавление содержимого к фрейму с документом для печати
        var printFrameBody = $('#printFrame').contents().find('body');
        if(printFrameBody != null)
        {
            var printFrameContent = $('#mainBookingForm').clone();

            if(!printStatus)
            {
                printFrameContent.find('#wpPrepaymentTable').remove();
            }
            printFrameContent.find('#backwardButton').remove();
            printFrameContent.find('#bookingFormFooter .tourPrepaymentOptions').remove();

            printFrameBody.append('' +
                '<div id="printHeader">' +
                '<img src="../template/villa/i/printlogo.png">' +
                '<table>' +
                '    <tr>' +
                '        <td>' +
                '            Адрес:<br />' +
                '            Украина г. Киев, 03061 <br />' +
                '            проспект Отрадный 7 <br />' +
                '            тел. +38 044 000 00 00 <br />' +
                '            факс +38 044 000 00 00 <br />' +
                '        </td>' +
                '        <td>' +
                '            Address:<br />' +
                '            Ukraine, Kiev, 0.061 <br />' +
                '            Prospect Otradnyj 7 <br />' +
                '            tel. +38 044 000 00 00 <br />' +
                '            fax +38 044 000 00 00 <br />' +
                '        </td>' +
                '    </tr>' +
                '</table>' +
                '</div>');
            printFrameBody.append(printFrameContent.html()).css('background', 'white');
        }

        // Печать содержимого фрейма
        printFrame = printFrame.contentWindow;
        printFrame.focus();
        printFrame.print();
        $("#printModal").delay(1000).animate({opacity:0}, 700, function(){
            $('#printFrame').remove();
            $("#printModal").remove();
        });
    });
}

/**
 * Валидация формы бронирования номера
 */
function hotelBookingValidate() {
    var arriveDate = $('#bookingArriveDate').val();
    if (arriveDate == '') {
        jAlert('Не задана дата заезда', villa.labels.error);
        return false;
    }

    var departureDate = $('#bookingDepartureDate').val();
    if (departureDate == '') {
        jAlert('Не задана дата выезда', villa.labels.error);
        return false;
    }

    if ($('#bookingAdults').val() == '') {
        jAlert('Не задано количество человек', villa.labels.error);
        return false;
    }

    if ($('#bookingEmail').val() == ''){
        jAlert('Не задан email', villa.labels.error);
        return false;
    }

    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if (!emailReg.test($('#bookingEmail').val())) {
        jAlert('Неверный формат email', villa.labels.error);
        return false;
    }

    if ($('#bookingPhone').val() == ''){
        jAlert('Не задан телефон', villa.labels.error);
        return false;
    }

    if (!phoneNumberValidate($('#bookingPhone').val())) {
        jAlert('Неверный формат телефонного номера', villa.labels.error);
        return false;
    }

    return true;
}

/**
 * Получение опций бронирования
 * @return array
 */
function hotelBookingGetOptions() {
    // получение опций бронирования
    var inputs = $('#booking-form input:checkbox[name=bookingOption]:checked');
    var options = [];
    for(var i=0; i<inputs.length; i++) {
        options.push(inputs[i].value);
    }

    return options;
}