room_search = {
    drawBookingForm: function()
    {
        if($('#reserv_form').find('div.radio_button.selected').length == 0)
        {
            jAlert(villa.labels.chooseRoomAndPrepayment, villa.labels.message);
            return;
        }

        var data = $('#reserv_form').serializeArray();
        data.push(
            {name: 'controller', value: 'villa.module.hotel.controller.session'},
            {name: 'action', value: 'setBookingRoomData'}
        );
        
        $.ajax({
            type: 'POST',
            url: '/ajax.php',
            data: data,
            success: function(data)
            {
                if(data.result == 1)
                {
                    location.assign('/bookingform/');
                }
                else
                {
                    console.log(data);
                    jAlert(data.errorMessage);
                }
            }
        });
    },
    
    /**
     * Функция - обработчик выбора типа брони на форме выбора номеров
     * для бронирования
     */
    changeBookingRadio : function(object)
    {
        $('#reserv_form').find('.radio_button.selected').removeClass('selected');
        $('.price_item_num, .price_item_foot').text('0');
        
        $(object).addClass('selected');
        
        $('.btn-check-reserve').addClass('active');
                
        $(object).find('input[name="prepayment"]').prop('checked', true);
        $(object).closest('.booking_item').find('input[name="type_id"]').prop('checked', true);
        
        var price = parseFloat($(object).find('.room-price-day').val());
        // Общая стоимость брони
        var cost = price * this.days;
        cost = cost.toFixed(2);
        // Отображение общей стоимости брони
        $(object).closest('.booking_item').find('.price_item_num').text(cost);
        $(object).closest('.booking_item').find('.price_item_foot').text(cost);
    }
}


