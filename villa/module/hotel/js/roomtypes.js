roomtypes = {
    curTypeId: 0,
    
    prices: false,
    prepTypes: false,
    
    priceCalendarInit: function()
    {
        if($(document).width() < 768){
            $(".calendar-input").datepicker({
                minDate: 0,
                numberOfMonths: 1,
                onClose: function(){
                    this.curTypeId = 0;
                },
                beforeShowDay: function(dateObj){
                    var timestamp = dateObj.getTime()/1000;
                    var title = roomtypes.setPricesForDate(timestamp);
                    return [true, '', title];
                }
            });
        }else{
        $(".calendar-input").datepicker({
            minDate: 0,
            numberOfMonths: [1, 3],
            onClose: function(){
                this.curTypeId = 0;
            },
            beforeShowDay: function(dateObj){
                var timestamp = dateObj.getTime()/1000;
                var title = roomtypes.setPricesForDate(timestamp);
                return [true, '', title];
            }
        });
        }
    },
    
    showPriceCalendar: function(typeId)
    {
        this.curTypeId = typeId;
        $("#priceCalendar_"+typeId).datepicker("show");
    },
    
    setPricesForDate: function(timestamp)
    {
        if(this.prices == false || this.prepTypes == false || this.curTypeId == 0) return '';

        var dateTitle = '';
        for(var i=0; i < this.prepTypes.length; i++)
        {
            var prepTypeId = this.prepTypes[i]['id'];
            var curPricesSet = this.prices[this.curTypeId][prepTypeId];
            var dates = curPricesSet['dates'];
            var price = false;
            if(dates !== undefined)
            {
                for(var j=0; j < dates.length; j++)
                {
                    var item = dates[j];
                    if(timestamp >= item['dateFrom'] && timestamp <= item['dateTo'])
                        price = item['price'];
                }            
            }

            if(price === false) price = curPricesSet['main']['standardPrice'];

            dateTitle += this.prepTypes[i]['name']+': '+price+'\n';
        }

        return dateTitle;
    },
    
    showEquipment: function(typeId)
    {
        $('#roomEquipment').arcticmodal({
            type: 'ajax',
            url: 'ajax.php',
            ajax: {
                type: 'POST',
                dataType: 'JSON',
                data: {
                    controller: 'villa.module.hotel.controller.roomtypes',
                    action: 'getEquipment',
                    type_id: typeId
                },
                success: function(data, el, response) 
                {
                    data.body.html(el);                    
                    // $(el).find('.modal-body').html(response.html);
                    // $('.room_list_features').html(response.html);
                    // alert('tesd');
                }
            },
            afterClose: function()
            {
                 $('#roomEquipment').attr('style', '').find('.modal-body').html('');
            }
        });
    },
    
    viewImages: function(event)
    {
        var imgObjects = $(event.target).parents('.img-list').find('img');
        var location = $(event.target).parents('.number-item');    
        var activeIndex = $(imgObjects).index(event.target);

        var params = [];

        params['images'] = [];
        $(imgObjects).each(function(){
            params['images'].push($(this).attr('src'));
        });
        params['thumbs'] = 0;
        params['title'] = $(location).find('h3').text();
        params['descript'] = $(location).find('.descript').text();    

        showViewer(params, location, activeIndex);
    },

    goToTop: function()
    {
        $("body").animate({
            scrollTop: 0
        }, 300);
    }
}


