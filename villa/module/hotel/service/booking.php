<?php
/**
 * Description of booking
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Service_Booking 
{    
    protected $_mode = 0;
    
    public function setMode($mode)
    {
        $this->_mode = $mode;
    }
    
    /**
     * Оповещаем клиента о созданной брони
     * @param Villa_Module_Hotel_Model_Booking_Adult $guest
     * @param int $bookingId 
     */
    public function notifyUser(Villa_Module_User_Model $user, $bookingId)
    {

        $service = new Module_Mail_Service_Sender();
        $model = $this->_getBookingModel($bookingId);
        
        $mailModelMain = $this->_getBookingMsg($user, $model);

        Dante_Lib_Log_Factory::getLogger()->debug("mail model:".var_export($mailModelMain, true));

        // Доп. письмо для турагента
        if($this->_mode == BOOKING_MAIL_MODE_TOURAGENT)
        {
            $model->resetDiscountAndPayment();
            $mailModelTA = $this->_getBookingMsg($user, $model);
            //file_put_contents('mail.html', $mailModelTA->message);
            $service->send($user->email, $mailModelTA->subject, $mailModelTA->message);
        }        
        
        $service->send($user->email, $mailModelMain->subject, $mailModelMain->message);
    }
    
    private function _getBookingMsg(Villa_Module_User_Model $user, Villa_Module_Hotel_Model_Manage_Booking $bookingModel)
    {
        $mailBuilder = Villa_Module_Mail_Builder_Factory::create(MAIL_TYPE_BOOKING);
        $mailBuilder->setModel();
        
        // Если режим редактирования - используем вторую тему
        if($this->_mode == BOOKING_MAIL_MODE_EDIT) $mailBuilder->setSubject2Usage(true);
        
        $prepaymentMapper = new Villa_Module_Hotel_Mapper_Prepayment();
        $roomTypesMapper = new Villa_Module_Hotel_Mapper_Manage_Roomtypes();
        $optionsMapper = new Villa_Module_Hotel_Mapper_Bookingoptions();
        $ccyMapper = new Module_Currency_Mapper();
        $contactsMapper = new Villa_Module_Contacts_Mapper();        
        $bookingInfoMapper = new Villa_Module_Bookinginfo_Mapper();   
                                
        $roomInfo = $roomTypesMapper->getOne($bookingModel->roomData->type_id, Module_Lang_Helper::getCurrent());               
        $prepaymentTypes = $prepaymentMapper->getNamesList();
        $userCurrency = $ccyMapper->getById($bookingModel->user_currency);
        $contacts = $contactsMapper->get(Module_Lang_Helper::getCurrent());

        $data = array(
            'booking' => $bookingModel,
            'user_info' => $user,
            'room_type_name' => $roomInfo->title,
            'prepayment_name' => $prepaymentTypes[$bookingModel->roomData->prepayment],
            'ccy' => $userCurrency['reduction'],
            'services' => $optionsMapper->getServices(),
            'excursions' => Villa_Module_Hotel_Helper_Excursion::getListForPeriod($bookingModel->basicData->arrivalToInt(), $bookingModel->days + 1, $bookingModel->id),
            'transfers' => $optionsMapper->getTransfers(),
            'contacts' => $contacts,
            'phone' => Villa_Module_Countries_Helper::getPhone(),
            'info' => $bookingInfoMapper->get(Module_Lang_Helper::getCurrent()),
            'credentials' => '1'
        );

        //print_r($data);

        $mailBuilder->setData($data);
        $mailBuilder->mode = $this->_mode;
        $mailModel = $mailBuilder->buildMessage();
        
        return $mailModel;
    }
    
    private function _getBookingModel($bookingId)
    {        
        $mapper = new Villa_Module_Hotel_Mapper_Manage_Booking();
                
        $model = $mapper->getById($bookingId);
        $model->removeEmptyValues();
        
        return $model;
    }
}

?>
