<?php

$url = Dante_Controller_Front::getRequest()->get_requested_url();


Dante_Lib_Router::addRule('/^(\/[a-z]{2,3})?\/room\-search\-result\/$/', array(
    'controller' => 'Villa_Module_Hotel_Controller_Roomsearch',
    'action' => 'showResult'
));

Dante_Lib_Router::addRule("/^(\/[a-z]{2,3})?\/numbers\/$/", array(
    'controller' => 'Villa_Module_Hotel_Controller_Roomtypes'
));

Dante_Lib_Router::addRule("/^(\/[a-z]{2,3})?\/bookingform\/$/", array(
    'controller' => 'Villa_Module_Hotel_Controller_Bookingform'
));

Dante_Lib_Router::addRule("/manage\/booking\//", array(
    'controller' => 'Villa_Module_Hotel_Controller_Manage_Booking'
));

Dante_Lib_Router::addRule("/manage\/bookings\//", array(
    'controller' => 'Villa_Module_Hotel_Controller_Manage_Booking'
));

Dante_Lib_Router::addRule("/manage\/services\//", array(
    'controller' => 'Villa_Module_Hotel_Controller_Manage_Services'
));

Dante_Lib_Router::addRule("/manage\/rooms\//", array(
    'controller' => 'Villa_Module_Hotel_Controller_Manage_Rooms'
));

Dante_Lib_Router::addRule("/manage\/roomtypes\//", array(
    'controller' => 'Villa_Module_Hotel_Controller_Manage_Roomtypes'
));

Dante_Lib_Router::addRule("/manage\/loading\//", array(
    'controller' => 'Villa_Module_Hotel_Controller_Manage_Loading'
));

Dante_Lib_Router::addRule("/manage\/cashcontrol\//", array(
    'controller' => 'Villa_Module_Hotel_Controller_Manage_Cashcontrol'
));

Dante_Lib_Router::addRule("/manage\/cashgendir\//", array(
    'controller' => 'Villa_Module_Hotel_Controller_Manage_Cashgendir'
));

Dante_Lib_Router::addRule("/manage\/equipment\//", array(
    'controller' => 'Villa_Module_Hotel_Controller_Manage_Equipment'
));

Dante_Lib_Router::addRule("/manage\/bookingprepayment\//", array(
    'controller' => 'Villa_Module_Hotel_Controller_Manage_Prepayment'
));

Dante_Lib_Router::addRule("/manage\/discount\//", array(
    'controller' => 'Villa_Module_Hotel_Controller_Manage_Discount'
));

Dante_Lib_Router::addRule("/manage\/hotelprices\//", array(
    'controller' => 'Villa_Module_Hotel_Controller_Manage_Prices'
));

Dante_Lib_Router::addRule("/manage\/transfer\//", array(
    'controller' => 'Villa_Module_Hotel_Controller_Manage_Transfer'
));

Dante_Lib_Router::addRule("/manage\/booking\-transfer\//", array(
    'controller' => 'Villa_Module_Hotel_Controller_Manage_Bookingtransfer'
));

?>