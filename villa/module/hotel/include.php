<?php

define("GUEST_TYPE_ADULT", 1);    
define("GUEST_TYPE_CHILD", 2);

define("PREPAYMENT_TYPE_NONE", 1);    
define("PREPAYMENT_TYPE_FIRST", 2);    
define("PREPAYMENT_TYPE_ALL", 3);

define("TRANSFER_TYPE_ARRIVAL", 1);
define("TRANSFER_TYPE_DEPARTURE", 2);

define("TRANSPORT_PLANE", 1);
define("TRANSPORT_TRAIN", 2);

define("BF_VIEW_MODE_NEW", 1);
define("BF_VIEW_MODE_AFTER_PAYMENT", 2);
define("BF_VIEW_MODE_EDIT", 3);

define("PAYMENT_STATUS_SUCCESS", 1);
define("PAYMENT_STATUS_WAIT", 2);
define("PAYMENT_STATUS_ERROR", 3);

define("BOOKING_STATUS_NEW", 1);
define("BOOKING_STATUS_PROCESSED", 2);
define("BOOKING_STATUS_ARRIVAL", 3);
define("BOOKING_STATUS_RESIDENCE", 4);
define("BOOKING_STATUS_DEPARTURE", 5);
define("BOOKING_STATUS_END", 6);
define("BOOKING_STATUS_CANCEL", 7);

define("TRANSFER_STATUS_ORDER", 1);
define("TRANSFER_STATUS_ORDERED", 2);
define("TRANSFER_STATUS_PROCESSED", 3);
define("TRANSFER_STATUS_END", 4);

define("BOOKING_MAIL_MODE_TOURAGENT", 1);
define("BOOKING_MAIL_MODE_EDIT", 2);

define("PAYMENT_TYPE_INCOME", 1);
define("PAYMENT_TYPE_REFUND", 2);

define("PAYMENT_SUBJECT_RESIDENCE", 1);
define("PAYMENT_SUBJECT_SERVICE", 2);
define("PAYMENT_SUBJECT_ALL", 3);

Dante_Lib_Config::set('app.uploadFolder', 'media');
Dante_Lib_Config::set('hotel.servicesFolder', 'bookingoptions');
Dante_Lib_Config::set('hotel.transfersFolder', 'transfers');
Dante_Lib_Config::set('hotel.aptTypesFolder', 'roomtypes');
Dante_Lib_Config::set('hotel.aboutPageFolder', 'aboutpage');
Dante_Lib_Config::set('hotel.homePageFolder', 'homepage');
Dante_Lib_Config::set('hotel.passportScanDir', 'scan');
Dante_Lib_Config::set('hotel.touragentLogoDir', 'touragent_logo');

?>
