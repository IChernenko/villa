DROP TABLE IF EXISTS [%%]hotel_booking_options_samples_lang;

CREATE TABLE IF NOT EXISTS [%%]hotel_booking_options_samples_lang (
  `id` int(11) NOT NULL,
    `lang_id` int(11) unsigned NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE [%%]hotel_booking_options_samples_lang ADD
      CONSTRAINT fk_hotel_booking_options_samples_lang_id
      FOREIGN KEY (`id`)
      REFERENCES [%%]hotel_booking_options_samples(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE [%%]hotel_booking_options_samples_lang ADD
      CONSTRAINT fk_hotel_booking_options_samples_lang_lang_id
      FOREIGN KEY (`lang_id`)
      REFERENCES [%%]languages(id) ON DELETE CASCADE ON UPDATE CASCADE;