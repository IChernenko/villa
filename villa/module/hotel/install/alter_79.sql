CREATE TABLE [%%]hotel_booking_transfer_images (
  `transfer_id` int(11) UNSIGNED NOT NULL,
  `image` VARCHAR(50) NOT NULL,
  `draw_order` TINYINT( 2 ) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE [%%]hotel_booking_transfer_images ADD
    CONSTRAINT fk_hotel_booking_transfer_images_transfer_id
    FOREIGN KEY (transfer_id)
    REFERENCES [%%]hotel_booking_transfer_samples(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE [%%]hotel_booking_transfer_samples_lang ADD
    COLUMN description TEXT NULL AFTER type_name;