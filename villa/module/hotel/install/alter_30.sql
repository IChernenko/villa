DROP TABLE IF EXISTS [%%]hotel_equipment;

CREATE TABLE IF NOT EXISTS [%%]hotel_equipment (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS [%%]hotel_equipment_lang;

CREATE TABLE IF NOT EXISTS [%%]hotel_equipment_lang (
  `id` int(11) NOT NULL,
    `lang_id` int(11) unsigned NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE [%%]hotel_equipment_lang ADD
      CONSTRAINT fk_hotel_equipment_lang_id
      FOREIGN KEY (`id`)
      REFERENCES [%%]hotel_equipment(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE [%%]hotel_equipment_lang ADD
      CONSTRAINT fk_hotel_equipment_lang_lang_id
      FOREIGN KEY (`lang_id`)
      REFERENCES [%%]languages(id) ON DELETE CASCADE ON UPDATE CASCADE;