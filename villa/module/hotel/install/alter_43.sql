alter table [%%]hotel_booking ADD column uid int(11) not null;

alter table [%%]hotel_booking ADD
      CONSTRAINT fk_hotel_booking_uid
      FOREIGN KEY (`uid`)
      REFERENCES [%%]users(id) ON DELETE CASCADE ON UPDATE CASCADE;

alter table [%%]hotel_booking ADD column creation_time int(11);