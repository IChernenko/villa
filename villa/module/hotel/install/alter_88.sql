DELETE FROM `[%%]hotel_booking`;

ALTER TABLE `[%%]hotel_booking_guests` CHANGE `guest_id` `guest_num` INT(11) NOT NULL DEFAULT 1;

ALTER TABLE `[%%]hotel_booking_services` ADD COLUMN `guest_num` INT(11) NOT NULL AFTER `service_id`;
ALTER TABLE `[%%]hotel_booking_services` ADD COLUMN `guest_type` INT(11) NOT NULL AFTER `guest_num`;

ALTER TABLE `[%%]hotel_booking_excursions` ADD COLUMN `guest_num` INT(11) NOT NULL AFTER `date`;
ALTER TABLE `[%%]hotel_booking_excursions` ADD COLUMN `guest_type` INT(11) NOT NULL AFTER `guest_num`;



