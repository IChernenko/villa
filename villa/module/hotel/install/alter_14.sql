ALTER TABLE [%%]hotel_apartmentstypes_lang CHANGE `lang_id` `lang_id` INT( 11 ) UNSIGNED NOT NULL ;

ALTER TABLE [%%]hotel_apartmentstypes_lang ADD
      CONSTRAINT fk_hotel_apartmentstypes_lang_lang_id
      FOREIGN KEY (`lang_id`)
      REFERENCES [%%]languages(id) ON DELETE CASCADE ON UPDATE CASCADE;