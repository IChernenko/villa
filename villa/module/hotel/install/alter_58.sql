ALTER TABLE [%%]hotel_booking ADD `edition` INT( 11 ) NOT NULL AFTER `id`;

ALTER TABLE [%%]hotel_booking_options ADD `count` INT( 11 ) NOT NULL;
ALTER TABLE [%%]hotel_booking_options ADD `day` varchar( 50 ) NOT NULL;