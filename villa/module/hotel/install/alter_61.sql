ALTER TABLE [%%]hotel_booking ADD `description` text NOT NULL;
ALTER TABLE [%%]hotel_booking ADD `r_description` text NOT NULL;

DROP TABLE IF EXISTS [%%]hotel_booking_people;

CREATE TABLE IF NOT EXISTS [%%]hotel_booking_people (
  `hotel_booking_id` int(11) NOT NULL,
    `client_type` varchar(50) NOT NULL,
    `first_name` varchar(50) NOT NULL,
    `last_name` varchar(50) NOT NULL,
    `scan` varchar(50) NOT NULL,
    `adress` varchar(150) NOT NULL,
    `email` varchar(100) NOT NULL,
    `phone` varchar(50) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE [%%]hotel_booking_people ADD
      CONSTRAINT fk_hotel_booking_people_hotel_booking_id
      FOREIGN KEY (`hotel_booking_id`)
      REFERENCES [%%]hotel_booking(id) ON DELETE CASCADE ON UPDATE CASCADE;