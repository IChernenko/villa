ALTER TABLE [%%]hotel_booking_deposit_history ADD `user_id` INT( 11 ) NOT NULL AFTER `booking_id`;

ALTER TABLE [%%]hotel_booking_deposit_history ADD
      CONSTRAINT fk_hotel_booking_deposit_history_user_id
      FOREIGN KEY (`user_id`)
      REFERENCES [%%]users(id) ON DELETE CASCADE ON UPDATE CASCADE;