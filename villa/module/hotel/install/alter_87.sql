ALTER TABLE `[%%]hotel_booking_deposit_history` CHANGE `summ` `amount` FLOAT(10, 2) NOT NULL;
ALTER TABLE `[%%]hotel_booking_deposit_history` CHANGE `summ_type` `pay_way` INT(11) NOT NULL;
ALTER TABLE `[%%]hotel_booking_deposit_history` CHANGE `status` `status` TINYINT(1) NOT NULL;

ALTER TABLE `[%%]hotel_booking_deposit_history` ADD COLUMN `type` TINYINT(1) NOT NULL DEFAULT 1 AFTER `date`;

RENAME TABLE `[%%]hotel_booking_deposit_history` TO `[%%]hotel_booking_payment_history`;