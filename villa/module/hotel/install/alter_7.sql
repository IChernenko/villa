DROP TABLE IF EXISTS [%%]hotel_apartments;

CREATE TABLE [%%]hotel_apartments (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;