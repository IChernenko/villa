DROP TABLE IF EXISTS [%%]hotel_booking_options_transfer;

CREATE TABLE IF NOT EXISTS [%%]hotel_booking_options_transfer (
  `hotel_booking_id` int(11) NOT NULL,
    `date` int(11) NOT NULL,
`time` int(5) NOT NULL,
`location` varchar(50) NOT NULL,
`add1` varchar(50) NOT NULL,
`add2` varchar(50) NOT NULL,
`from` varchar(50) NOT NULL,
`cost` float(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE [%%]hotel_booking_options_transfer ADD
      CONSTRAINT fk_hotel_booking_options_transfer_hotel_booking_id
      FOREIGN KEY (`hotel_booking_id`)
      REFERENCES [%%]hotel_booking(id) ON DELETE CASCADE ON UPDATE CASCADE;