DROP TABLE IF EXISTS [%%]hotel_apartmentstypes_lang;

CREATE TABLE IF NOT EXISTS [%%]hotel_apartmentstypes_lang (
  `id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE [%%]hotel_apartmentstypes DROP `description`