<?
Module_Division_Manage_Service::add(array(
        'Номера', 
        '/manage/rooms/',
        array(
            3,
            2,
            1
        ), 
        'adminka'
    ));

Module_Division_Manage_Service::add(array(
        'Типы номеров', 
        '/manage/roomtypes/',
        array(
            3,
            2,
            1
        ), 
        'adminka'
    ));

Module_Division_Manage_Service::add(array(
        'Бронирование', 
        '/manage/booking/',
        array(
            3,
            2,
            1
        ), 
        'adminka'
    ));

Module_Division_Manage_Service::add(array(
        'Услуги', 
        '/manage/services/',
        array(
            3,
            2,
            1
        ), 
        'adminka'
    ));

Module_Division_Manage_Service::add(array(
        'Загрузка отеля', 
        '/manage/loading/',
        array(
            3,
            2,
            1
        ), 
        'adminka'
    ));
Module_Division_Manage_Service::add(array(
        'Оборудование', 
        '/manage/equipment/',
        array(
            3
        ), 
        'adminka'
    ));

Module_Division_Manage_Service::add(array(
        'Типы предоплат', 
        '/manage/bookingprepayment/',
        array(
            3
        ), 
        'adminka'
    ));
Module_Division_Manage_Service::add(array(
        'Кабинет', 
        '/manage/cashcontrol/',
        array(
            3
        ), 
        'adminka'
    ));
Module_Division_Manage_Service::add(array(
        'Кабинет Ген Директора', 
        '/manage/cashgendir/',
        array(
            3
        ), 
        'adminka'
    ));
?>