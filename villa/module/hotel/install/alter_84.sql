ALTER TABLE `[%%]hotel_discount` DROP FOREIGN KEY `fk_hotel_discount_option_id`;
ALTER TABLE `[%%]hotel_discount` DROP COLUMN `option_id`;

ALTER TABLE `[%%]hotel_discount` CHANGE `discount` `discount_residence` INT(11) NOT NULL DEFAULT 0;
ALTER TABLE `[%%]hotel_discount` CHANGE `discount_percent` `discount_services` INT(11) NOT NULL DEFAULT 0;

INSERT INTO `[%%]entity` SET `name` = 'Экскурсия', `sys_name` = 'excursion';