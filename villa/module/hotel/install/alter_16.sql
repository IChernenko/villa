DROP TABLE IF EXISTS [%%]hotel_booking_deposit_history;

CREATE TABLE IF NOT EXISTS [%%]hotel_booking_deposit_history (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `summ` int(11) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


ALTER TABLE [%%]hotel_booking_deposit_history ADD
      CONSTRAINT fk_hotel_booking_deposit_history_booking_id
      FOREIGN KEY (`booking_id`)
      REFERENCES [%%]hotel_booking(id) ON DELETE CASCADE ON UPDATE CASCADE;