DROP TABLE IF EXISTS `[%%]hotel_booking_transfer_samples`;

CREATE TABLE `[%%]hotel_booking_transfer_samples`(
    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `sys_name` VARCHAR(100) NOT NULL,
    `transport_type` INT(11) NOT NULL DEFAULT 1,
    `type` INT(11) NOT NULL DEFAULT 1,
    PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

DROP TABLE IF EXISTS `[%%]hotel_booking_transfer_samples_lang`;

CREATE TABLE `[%%]hotel_booking_transfer_samples_lang`(
    `transfer_id` INT(11) UNSIGNED NOT NULL,
    `lang_id` INT(11) UNSIGNED NOT NULL,
    `name` VARCHAR(100) NOT NULL,
    `type_name` VARCHAR(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `[%%]hotel_booking_transfer_samples` 
    SET `id` = 1, `sys_name` = 'Аэропорт', `transport_type` = 1, `type` = 1;
INSERT INTO `[%%]hotel_booking_transfer_samples_lang` 
    SET `transfer_id` = 1, `lang_id` = 1, `name` = 'Аэропорт', `type_name` = 'Прибытие';
INSERT INTO `[%%]hotel_booking_transfer_samples` 
    SET `id` = 2, `sys_name` = 'Аэропорт', `transport_type` = 1, `type` = 2;
INSERT INTO `[%%]hotel_booking_transfer_samples_lang` 
    SET `transfer_id` = 2, `lang_id` = 1, `name` = 'Аэропорт', `type_name` = 'Отправление';
INSERT INTO `[%%]hotel_booking_transfer_samples` 
    SET `id` = 3, `sys_name` = 'ЖД вокзал', `transport_type` = 2, `type` = 1;
INSERT INTO `[%%]hotel_booking_transfer_samples_lang` 
    SET `transfer_id` = 3, `lang_id` = 1, `name` = 'ЖД вокзал', `type_name` = 'Прибытие';
INSERT INTO `[%%]hotel_booking_transfer_samples` 
    SET `id` = 4, `sys_name` = 'ЖД вокзал', `transport_type` = 2, `type` = 2;
INSERT INTO `[%%]hotel_booking_transfer_samples_lang` 
    SET `transfer_id` = 4, `lang_id` = 1, `name` = 'ЖД вокзал', `type_name` = 'Отправление';


ALTER TABLE `[%%]hotel_booking_transfer_samples_lang` ADD CONSTRAINT
    FOREIGN KEY(`transfer_id`) REFERENCES `[%%]hotel_booking_transfer_samples`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `[%%]hotel_booking_transfer_samples_lang` ADD CONSTRAINT
    FOREIGN KEY(`lang_id`) REFERENCES `[%%]languages`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `[%%]hotel_booking_options_samples` DROP COLUMN `cost`;