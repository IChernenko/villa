UPDATE `[%%]currency` SET `is_basic` = 0;
UPDATE `[%%]currency` SET `is_basic` = 1 WHERE `reduction` = 'EUR';

ALTER TABLE `[%%]hotel_booking` ADD COLUMN `currency_id` INT(11) NOT NULL AFTER `status`;
UPDATE `[%%]hotel_booking` SET `currency_id` = 1;

ALTER TABLE `[%%]hotel_booking` ADD CONSTRAINT `fk_booking_currency_id`
    FOREIGN KEY (`currency_id`) REFERENCES `[%%]currency`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;