CREATE TABLE `[%%]hotel_services_images` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `service_id` INT(11) NOT NULL,
    `image` VARCHAR(50) NOT NULL,
    `draw_order` INT(11) NOT NULL DEFAULT 0,
    PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

ALTER TABLE `[%%]hotel_services_images` ADD CONSTRAINT `fk_hotel_services_images_service_id`
    FOREIGN KEY(`service_id`) REFERENCES `[%%]hotel_services`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `[%%]hotel_booking_payment_history` ADD `subject` TINYINT(1) NOT NULL DEFAULT 1 AFTER `type`;