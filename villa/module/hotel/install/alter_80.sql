CREATE TABLE [%%]hotel_booking_options_galleries (
  `id` int(11) AUTO_INCREMENT NOT NULL,
  `option_id` int(11) NOT NULL,
  `lang_id` int(11) UNSIGNED NOT NULL,
  `title` VARCHAR(15) NOT NULL,
  `description` TEXT NULL,
  `draw_order` TINYINT(2) NULL,
  PRIMARY KEY(`id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE [%%]hotel_booking_options_galleries ADD
    CONSTRAINT fk_hotel_booking_options_galleries_option_id
    FOREIGN KEY (`option_id`)
    REFERENCES [%%]hotel_booking_options_samples(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE [%%]hotel_booking_options_galleries ADD
    CONSTRAINT fk_hotel_booking_options_galleries_lang_id
    FOREIGN KEY (`lang_id`)
    REFERENCES [%%]languages(id) ON DELETE CASCADE ON UPDATE CASCADE;