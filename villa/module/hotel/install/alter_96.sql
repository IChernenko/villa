ALTER TABLE `[%%]hotel_booking` ADD `dc_residence` INT(11) NOT NULL DEFAULT 0 AFTER `uid`;
ALTER TABLE `[%%]hotel_booking` ADD `dc_options` INT(11) NOT NULL DEFAULT 0 AFTER `uid`;

ALTER TABLE `[%%]hotel_booking` CHANGE `paid_services` `paid_options` FLOAT(10,2) NOT NULL DEFAULT 0;