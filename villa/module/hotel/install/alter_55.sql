DROP TABLE IF EXISTS [%%]hotel_discount;

CREATE TABLE IF NOT EXISTS [%%]hotel_discount (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL,
    `user_id` int(11) NOT NULL,
    `discount` int(2) NOT NULL,
`discount_percent` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


ALTER TABLE [%%]hotel_discount ADD
      CONSTRAINT fk_hotel_discount_option_id
      FOREIGN KEY (`option_id`)
      REFERENCES [%%]hotel_booking_options_samples(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE [%%]hotel_discount ADD
      CONSTRAINT fk_hotel_discount_user_id
      FOREIGN KEY (`user_id`)
      REFERENCES [%%]users(id) ON DELETE CASCADE ON UPDATE CASCADE;