ALTER TABLE `[%%]hotel_booking` DROP COLUMN `first_name`;
ALTER TABLE `[%%]hotel_booking` DROP COLUMN `last_name`;
ALTER TABLE `[%%]hotel_booking` DROP COLUMN `email`;
ALTER TABLE `[%%]hotel_booking` DROP COLUMN `phone`;

ALTER TABLE `[%%]hotel_booking` ADD COLUMN `modified` INT(11) NOT NULL AFTER `creation_time`;
ALTER TABLE `[%%]hotel_booking` ADD COLUMN `version` INT(11) NOT NULL DEFAULT 1 AFTER `creation_time`;

ALTER TABLE `[%%]hotel_booking` ADD COLUMN `modified_by` INT(11) DEFAULT NULL AFTER `modified`;
SELECT @admin:=`id` FROM `[%%]users` WHERE `email` = 'admin';
UPDATE `[%%]hotel_booking` SET `modified_by` = @admin;
UPDATE `[%%]hotel_booking` SET `modified` = UNIX_TIMESTAMP();
ALTER TABLE `[%%]hotel_booking` ADD CONSTRAINT `fk_hotel_booking_modified_by_user` 
    FOREIGN KEY (`modified_by`) REFERENCES `[%%]users`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE `[%%]hotel_booking_versions` (
    `booking_id` INT(11) NOT NULL,
    `version` INT(11) NOT NULL,
    `time` INT(11) NOT NULL,
    `uid` INT(11) DEFAULT NULL,
    `arrival` INT(11) NOT NULL,
    `departure` INT(11) NOT NULL,
    `days` INT(11) NOT NULL DEFAULT '1',
    `adults` INT(11) NOT NULL DEFAULT '1',
    `children` INT(11) NOT NULL DEFAULT '0',
    `prepayment_type` INT(11) NOT NULL,
    `room` INT(11) DEFAULT NULL,
    `room_type` INT(11) NOT NULL,
    `room_price` float(10,2) NOT NULL DEFAULT '0.00',
    `cost` float(10,2) NOT NULL DEFAULT '0.00',
    `comment` text,
    KEY `fk_hotel_booking_uid` (`uid`),
    CONSTRAINT `fk_hotel_booking_versions_uid` FOREIGN KEY (`uid`) REFERENCES `[%%]users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `[%%]hotel_booking_guests` ADD COLUMN `version` INT(11) NOT NULL DEFAULT 1 AFTER `booking_id`;
ALTER TABLE `[%%]hotel_booking_services` ADD COLUMN `version` INT(11) NOT NULL DEFAULT 1 AFTER `booking_id`;
ALTER TABLE `[%%]hotel_booking_excursions` ADD COLUMN `version` INT(11) NOT NULL DEFAULT 1 AFTER `booking_id`;
ALTER TABLE `[%%]hotel_booking_transfer` ADD COLUMN `version` INT(11) NOT NULL DEFAULT 1 AFTER `booking_id`;
ALTER TABLE `[%%]hotel_booking_extra_services` ADD COLUMN `version` INT(11) NOT NULL DEFAULT 1 AFTER `booking_id`;