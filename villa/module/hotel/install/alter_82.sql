DELETE FROM `[%%]hotel_prices`;
DELETE FROM `[%%]hotel_prices_dates`;

ALTER TABLE `[%%]hotel_prices` DROP `key`;
ALTER TABLE `[%%]hotel_prices_dates` DROP `key`;

ALTER TABLE `[%%]hotel_prices` ADD `entity_type_id` INT(11) UNSIGNED NOT NULL AFTER `id`;
ALTER TABLE `[%%]hotel_prices` ADD `entity_id` INT(11) UNSIGNED NOT NULL AFTER `entity_type_id`;
ALTER TABLE `[%%]hotel_prices` ADD `prep_type` INT(11) DEFAULT NULL AFTER `entity_id`;

ALTER TABLE `[%%]hotel_prices` ADD CONSTRAINT `fk_hotel_prices_entity_type`
    FOREIGN KEY(`entity_type_id`) REFERENCES `[%%]entity`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `[%%]hotel_prices` ADD CONSTRAINT `fk_hotel_prices_prep_type`
    FOREIGN KEY(`prep_type`) REFERENCES `[%%]hotel_booking_prepayment_types`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `[%%]hotel_prices_dates` ADD `price_id` INT(11) NOT NULL AFTER `id`;

ALTER TABLE `[%%]hotel_prices_dates` ADD CONSTRAINT `fk_hotel_prices_dates_price_id`
    FOREIGN KEY(`price_id`) REFERENCES `[%%]hotel_prices`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;

INSERT INTO `[%%]entity` SET `name` = 'Тип номера', `sys_name` = 'room_type';
INSERT INTO `[%%]entity` SET `name` = 'Услуга', `sys_name` = 'service';
INSERT INTO `[%%]entity` SET `name` = 'Трансфер', `sys_name` = 'transfer';