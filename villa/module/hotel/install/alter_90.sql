CREATE TABLE `[%%]hotel_booking_extra_services` (
    `booking_id` INT(11) NOT NULL,
    `date` INT(11) NOT NULL,
    `cost` FLOAT(10,2) NOT NULL DEFAULT 0,
    `descr` VARCHAR(300) NOT NULL 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `[%%]hotel_booking_extra_services` ADD CONSTRAINT `fk_booking_extra_services_booking_id`
    FOREIGN KEY(`booking_id`) REFERENCES `[%%]hotel_booking`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `[%%]hotel_apartments` ADD COLUMN `priority` INT(11) NOT NULL DEFAULT 0 AFTER `type`;