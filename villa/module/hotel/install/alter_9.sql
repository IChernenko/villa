DROP TABLE IF EXISTS [%%]hotel_apartmentstypes;

CREATE TABLE [%%]hotel_apartmentstypes (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS [%%]hotel_apartmentstypes_images;

CREATE TABLE [%%]hotel_apartmentstypes_images (
  `type_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS [%%]hotel_apartmentstypes_images_samples;

CREATE TABLE [%%]hotel_apartmentstypes_images_samples (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;