delete FROM [%%]hotel_booking;

ALTER TABLE [%%]hotel_booking ADD `edited_by` INT( 11 ) NOT NULL AFTER `edition`;

ALTER TABLE [%%]hotel_booking ADD
      CONSTRAINT fk_hotel_booking_edited_by
      FOREIGN KEY (`edited_by`)
      REFERENCES [%%]users(id) ON DELETE CASCADE ON UPDATE CASCADE;