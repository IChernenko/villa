CREATE TABLE [%%]hotel_booking_options_images (
  `option_id` int(11) NOT NULL,
  `image` VARCHAR(50) NOT NULL,
  `draw_order` TINYINT( 2 ) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE [%%]hotel_booking_options_images ADD
    CONSTRAINT fk_hotel_booking_options_images_option_id
    FOREIGN KEY (`option_id`)
    REFERENCES [%%]hotel_booking_options_samples(id) ON DELETE CASCADE ON UPDATE CASCADE;