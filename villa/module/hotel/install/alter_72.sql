DROP TABLE IF EXISTS `[%%]hotel_booking_options_transfer`;

CREATE TABLE `[%%]hotel_booking_options_transfer`(
    `hotel_booking_id` INT(11) NOT NULL,
    `sample_id` INT(11) UNSIGNED NOT NULL,
    `date` INT(11) NOT NULL,
    `time` INT(11) NOT NULL,
    `location` VARCHAR(100) NOT NULL,
    `add1` VARCHAR(50) NOT NULL,
    `add2` INT(11) DEFAULT NULL,
    `from` VARCHAR(100) NOT NULL,
    `cost` FLOAT(10,2) NOT NULL DEFAULT 0,
    `flag_discount` TINYINT(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `[%%]hotel_booking_options_transfer` ADD CONSTRAINT
    FOREIGN KEY(`hotel_booking_id`) REFERENCES `[%%]hotel_booking`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `[%%]hotel_booking_options_transfer` ADD CONSTRAINT
    FOREIGN KEY(`sample_id`) REFERENCES `[%%]hotel_booking_transfer_samples`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;