DROP TABLE IF EXISTS [%%]hotel_booking_options;

CREATE TABLE IF NOT EXISTS [%%]hotel_booking_options (
  `hotel_booking_id` int(11) NOT NULL,
  `sample_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS [%%]hotel_booking_options_samples;

CREATE TABLE IF NOT EXISTS [%%]hotel_booking_options_samples (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `cost` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;