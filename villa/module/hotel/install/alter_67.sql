DROP TABLE IF EXISTS [%%]hotel_prices;

CREATE TABLE IF NOT EXISTS [%%]hotel_prices (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(50) NOT NULL,
  `standardPrice` float(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS [%%]hotel_prices_dates;

CREATE TABLE IF NOT EXISTS [%%]hotel_prices_dates (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(50) NOT NULL,
  `dateFrom` int(11) NOT NULL,
  `dateTo` int(11) NOT NULL,
  `price` float(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;