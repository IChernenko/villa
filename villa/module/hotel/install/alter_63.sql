ALTER TABLE [%%]hotel_booking ADD `edition_of` INT( 11 ) NOT NULL AFTER `edition`;
ALTER TABLE [%%]hotel_booking_options ADD `description` TEXT NOT NULL;
ALTER TABLE [%%]hotel_booking_options ADD `cost` float(10,2) NOT NULL;
ALTER TABLE [%%]hotel_booking_options CHANGE `sample_id` `sample_id` INT( 11 ) NULL DEFAULT NULL;
ALTER TABLE [%%]hotel_booking_options_transfer CHANGE `date` `date` VARCHAR( 50 ) NOT NULL;
ALTER TABLE [%%]hotel_booking_options_transfer CHANGE `time` `time` VARCHAR( 50 ) NOT NULL;