DELETE FROM `[%%]hotel_booking`;

ALTER TABLE `[%%]hotel_booking` DROP COLUMN `edition`;
ALTER TABLE `[%%]hotel_booking` DROP COLUMN `edition_of`;
ALTER TABLE `[%%]hotel_booking` DROP FOREIGN KEY `fk_hotel_booking_edited_by`;
ALTER TABLE `[%%]hotel_booking` DROP COLUMN `edited_by`;
ALTER TABLE `[%%]hotel_booking` DROP COLUMN `people`;
ALTER TABLE `[%%]hotel_booking` DROP FOREIGN KEY fk_hotel_booking_parent_id;
ALTER TABLE `[%%]hotel_booking` DROP COLUMN `parent_id`;

ALTER TABLE `[%%]hotel_booking` ADD `children` INT(11) NOT NULL DEFAULT 0 AFTER `departure`;
ALTER TABLE `[%%]hotel_booking` ADD `adults` INT(11) NOT NULL DEFAULT 1 AFTER `departure`;

DROP TABLE `[%%]hotel_booking_options`;
DROP TABLE `[%%]hotel_booking_options_transfer`;

CREATE TABLE `[%%]hotel_services` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `sys_name` varchar(100) NOT NULL,
    `image` varchar(100) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `[%%]hotel_services` (`id`, `sys_name`, `image`)
    SELECT `id`, `title`, `image` FROM `[%%]hotel_booking_options_samples`;

CREATE TABLE `[%%]hotel_services_lang` (
    `service_id` int(11) NOT NULL,
    `lang_id` int(11) unsigned NOT NULL,
    `name` varchar(50) NOT NULL,
    `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `[%%]hotel_services_lang` (`service_id`, `lang_id`, `name`, `description`)
    SELECT `id`, `lang_id`, `name`, `description` FROM `[%%]hotel_booking_options_samples_lang`;

DROP TABLE `[%%]hotel_booking_options_samples_lang`;
DROP TABLE `[%%]hotel_booking_options_images`;
DROP TABLE `[%%]hotel_booking_options_galleries`;
DROP TABLE `[%%]hotel_booking_options_samples`;

ALTER TABLE `[%%]hotel_services_lang` ADD CONSTRAINT `fk_hotel_services_lang_id` 
    FOREIGN KEY (`service_id`) REFERENCES `[%%]hotel_services` (`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `[%%]hotel_services_lang` ADD CONSTRAINT `fk_hotel_services_lang_lang_id` 
    FOREIGN KEY (`lang_id`) REFERENCES `[%%]languages` (`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE `[%%]hotel_booking_services`(
    `booking_id` INT(11) NOT NULL,
    `service_id` INT(11) NOT NULL,
    `count` INT(11) NOT NULL DEFAULT 1,
    `cost` FLOAT(10,2) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `[%%]hotel_booking_services` ADD CONSTRAINT `fk_hotel_booking_services_booking_id` 
    FOREIGN KEY (`booking_id`) REFERENCES `[%%]hotel_booking` (`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `[%%]hotel_booking_services` ADD CONSTRAINT `fk_hotel_booking_services_service_id` 
    FOREIGN KEY (`service_id`) REFERENCES `[%%]hotel_services` (`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE `[%%]hotel_transfer` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `sys_name` varchar(100) NOT NULL,
    `transport_type` int(11) NOT NULL DEFAULT '1',
    `type` int(11) NOT NULL DEFAULT '1',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `[%%]hotel_transfer` (`id`, `sys_name`, `transport_type`, `type`)
    SELECT `id`, `sys_name`, `transport_type`, `type` FROM `[%%]hotel_booking_transfer_samples`;

CREATE TABLE `[%%]hotel_transfer_lang` (
    `transfer_id` int(11) NOT NULL,
    `lang_id` int(11) unsigned NOT NULL,
    `name` varchar(100) NOT NULL,
    `type_name` varchar(100) DEFAULT NULL,
    `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `[%%]hotel_transfer_lang` (`transfer_id`, `lang_id`, `name`, `type_name`, `description`)
    SELECT `transfer_id`, `lang_id`, `name`, `type_name`, `description` FROM `[%%]hotel_booking_transfer_samples_lang`;

DROP TABLE `[%%]hotel_booking_transfer_images`;
DROP TABLE `[%%]hotel_booking_transfer_samples_lang`;
DROP TABLE `[%%]hotel_booking_transfer_samples`;

ALTER TABLE `[%%]hotel_transfer_lang` ADD CONSTRAINT `fk_hotel_transfer_lang_id` 
    FOREIGN KEY (`transfer_id`) REFERENCES `[%%]hotel_transfer` (`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `[%%]hotel_transfer_lang` ADD CONSTRAINT `fk_hotel_transfer_lang_lang_id` 
    FOREIGN KEY (`lang_id`) REFERENCES `[%%]languages` (`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE `[%%]hotel_booking_transfer`(
    `booking_id` INT(11) NOT NULL,
    `transfer_id` INT(11) NOT NULL,
    `date` INT(11) NOT NULL,
    `time` INT(11) NOT NULL,
    `location` VARCHAR(100) NOT NULL,
    `race` VARCHAR(10) DEFAULT NULL,
    `train` VARCHAR(10) DEFAULT NULL,
    `wagon` VARCHAR(10) DEFAULT NULL,
    `cost` FLOAT(10,2) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `[%%]hotel_booking_transfer` ADD CONSTRAINT `fk_hotel_booking_transfer_booking_id` 
    FOREIGN KEY (`booking_id`) REFERENCES `[%%]hotel_booking` (`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `[%%]hotel_booking_transfer` ADD CONSTRAINT `fk_hotel_booking_transfer_transfer_id` 
    FOREIGN KEY (`transfer_id`) REFERENCES `[%%]hotel_transfer` (`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;