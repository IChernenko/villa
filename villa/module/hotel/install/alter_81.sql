ALTER TABLE [%%]hotel_booking_options_images
    ADD gallery_id INT(11) NULL AFTER option_id,
    ADD title VARCHAR(255) AFTER gallery_id,
    ADD description TEXT AFTER title;

ALTER TABLE [%%]hotel_booking_options_images ADD
    CONSTRAINT fk_hotel_booking_options_images_gallery_id
    FOREIGN KEY (`gallery_id`)
    REFERENCES [%%]hotel_booking_options_galleries(id) ON DELETE CASCADE ON UPDATE CASCADE;