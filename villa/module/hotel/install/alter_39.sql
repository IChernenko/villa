ALTER TABLE [%%]hotel_booking CHANGE `cost_total` `costTotal` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ;
ALTER TABLE [%%]hotel_booking CHANGE `first_name` `firstName` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ;
ALTER TABLE [%%]hotel_booking CHANGE `second_name` `secondName` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ;