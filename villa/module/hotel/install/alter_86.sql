ALTER TABLE `[%%]hotel_booking_people` DROP FOREIGN KEY fk_hotel_booking_people_hotel_booking_id;

ALTER TABLE `[%%]hotel_booking_people` CHANGE `hotel_booking_id` `booking_id` INT(11) NOT NULL;
ALTER TABLE `[%%]hotel_booking_people` CHANGE `client_type` `guest_type` INT(11) NOT NULL DEFAULT 1;

RENAME TABLE `[%%]hotel_booking_people` TO `[%%]hotel_booking_guests`;

ALTER TABLE `[%%]hotel_booking_guests` ADD `guest_id` INT(11) NOT NULL DEFAULT 1 AFTER `booking_id`;

ALTER TABLE `[%%]hotel_booking_guests` ADD CONSTRAINT fk_hotel_booking_guests_booking_id
    FOREIGN KEY (`booking_id`) REFERENCES `[%%]hotel_booking`(`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE `[%%]hotel_booking_excursions` (
    `booking_id` INT(11) NOT NULL,
    `date` INT(11) NOT NULL,
    `cost` FLOAT(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `[%%]hotel_booking_excursions` ADD CONSTRAINT fk_hotel_booking_excursions_booking_id
    FOREIGN KEY (`booking_id`) REFERENCES `[%%]hotel_booking`(`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `[%%]hotel_booking` ADD `paid` FLOAT(8,2) NOT NULL DEFAULT 0 AFTER `cost`;
ALTER TABLE `[%%]hotel_booking` DROP COLUMN `costTotal`;
ALTER TABLE `[%%]hotel_booking` CHANGE `uid` `uid` INT(11) NULL DEFAULT NULL;

