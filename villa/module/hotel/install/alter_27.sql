ALTER TABLE [%%]hotel_apartmentstypes_images ADD
      CONSTRAINT fk_hotel_apartmentstypes_images_type_id
      FOREIGN KEY (`type_id`)
      REFERENCES [%%]hotel_apartmentstypes(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE [%%]hotel_apartmentstypes_lang ADD
      CONSTRAINT fk_hotel_apartmentstypes_lang_id
      FOREIGN KEY (`id`)
      REFERENCES [%%]hotel_apartmentstypes(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE [%%]hotel_booking_options ADD
      CONSTRAINT fk_hotel_booking_options_hotel_booking_id
      FOREIGN KEY (`hotel_booking_id`)
      REFERENCES [%%]hotel_booking(id) ON DELETE CASCADE ON UPDATE CASCADE;