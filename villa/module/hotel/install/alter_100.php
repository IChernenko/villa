<?php

$sql = 'ALTER TABLE `[%%]hotel_booking_payment_history` ADD `payment_id` INT(11) DEFAULT NULL AFTER `booking_id`';
Dante_Lib_SQL_DB::get_instance()->exec($sql);

$sql = "SELECT `booking_id`, `description` FROM `[%%]hotel_booking_payment_history` WHERE  `description` LIKE '%S%U%'";
$bookingTable = array();
$r = Dante_Lib_SQL_DB::get_instance()->open($sql);
while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
{
    $bookingTable[$f['booking_id']] = $f['description'];
}

$idStr = implode(',', array_keys($bookingTable));
$sql = "SELECT `id`, `order_id`, `description` FROM `[%%]liqpay_log` WHERE `order_id` IN ({$idStr})";
$paymentTable = array();
$r = Dante_Lib_SQL_DB::get_instance()->open($sql);
while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
{
    $paymentTable[$f['id']] = $f;
}

foreach($paymentTable as $paymentId => $v)
{
    $sql = "UPDATE `[%%]hotel_booking_payment_history` SET `payment_id` = {$paymentId}, `pay_way` = ".Villa_Module_Hotel_Helper_Manage_Payment::getPayWay('L')
            ." WHERE `booking_id` = {$v['order_id']} AND `description` = '{$v['description']}'";
            
    Dante_Lib_SQL_DB::get_instance()->exec($sql);
}

