ALTER TABLE `[%%]hotel_apartmentstypes`
ADD `max_children` INT(11) NOT NULL DEFAULT 1 AFTER `title`,
ADD `max_adults` INT(11) NOT NULL DEFAULT 1 AFTER `title`;