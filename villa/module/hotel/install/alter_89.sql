UPDATE `[%%]hotel_booking` SET `status` = 1;

DELETE FROM `[%%]hotel_booking_statuses` WHERE `id` != 1;
UPDATE `[%%]hotel_booking_statuses` SET `name` = 'Новое' WHERE `id` = 1;

INSERT INTO `[%%]hotel_booking_statuses` 
    (`id`, `name`)
VALUES
    (2, 'Обработано'),
    (3, 'Ожидание прибытия'),
    (4, 'Проживание'),
    (5, 'Ожидание выезда'),
    (6, 'Завершено'),
    (7, 'Отмена');
    