ALTER TABLE [%%]hotel_apartments CHANGE `type` `type` INT( 11 ) NOT NULL;

ALTER TABLE [%%]hotel_apartments ADD
      CONSTRAINT fk_hotel_apartments_type
      FOREIGN KEY (`type`)
      REFERENCES [%%]hotel_apartmentstypes(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE [%%]hotel_booking CHANGE `type` `type` INT( 11 ) NOT NULL;

ALTER TABLE [%%]hotel_booking ADD
      CONSTRAINT fk_hotel_booking_type
      FOREIGN KEY (`type`)
      REFERENCES [%%]hotel_apartmentstypes(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE [%%]hotel_booking ADD
      CONSTRAINT fk_hotel_booking_apartment
      FOREIGN KEY (`apartment`)
      REFERENCES [%%]hotel_apartments(id) ON DELETE CASCADE ON UPDATE CASCADE;