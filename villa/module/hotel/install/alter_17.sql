DROP TABLE IF EXISTS [%%]hotel_cash;

CREATE TABLE IF NOT EXISTS [%%]hotel_cash (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL,
  `balance` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS [%%]hotel_cash_transactions;

CREATE TABLE IF NOT EXISTS [%%]hotel_cash_transactions (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_from` int(11) NOT NULL,
  `user_to` int(11) NOT NULL,
  `summ` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE [%%]hotel_cash_transactions ADD
      CONSTRAINT fk_hotel_cash_transactions_user_from
      FOREIGN KEY (`user_from`)
      REFERENCES [%%]hotel_cash(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE [%%]hotel_cash_transactions ADD
      CONSTRAINT fk_hotel_cash_transactions_user_to
      FOREIGN KEY (`user_to`)
      REFERENCES [%%]hotel_cash(id) ON DELETE CASCADE ON UPDATE CASCADE;