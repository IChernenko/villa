DROP TABLE IF EXISTS [%%]hotel_apartmentstypes_equipment;

CREATE TABLE IF NOT EXISTS [%%]hotel_apartmentstypes_equipment (
  `apt_id` int(11) NOT NULL,
  `eq_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE [%%]hotel_apartmentstypes_equipment ADD
      CONSTRAINT fk_hotel_apartmentstypes_equipment_apt_id
      FOREIGN KEY (`apt_id`)
      REFERENCES [%%]hotel_apartmentstypes(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE [%%]hotel_apartmentstypes_equipment ADD
      CONSTRAINT fk_hotel_apartmentstypes_equipment_eq_id
      FOREIGN KEY (`eq_id`)
      REFERENCES [%%]hotel_equipment(id) ON DELETE CASCADE ON UPDATE CASCADE;