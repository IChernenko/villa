ALTER TABLE `[%%]hotel_services_lang` ADD `full_descr` TEXT DEFAULT NULL;

ALTER TABLE `[%%]villa_homepage` ADD `full_descr` TEXT DEFAULT NULL;
ALTER TABLE `[%%]villa_homepage_excursions` ADD `full_descr` TEXT DEFAULT NULL;
ALTER TABLE `[%%]villa_homepage_transfer` ADD `full_descr` TEXT DEFAULT NULL;

CREATE TABLE `[%%]villa_homepage_gallery` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `entity_type_id` INT(11) UNSIGNED NOT NULL,
    `entity_id` INT(11) NOT NULL,
    `image` VARCHAR(50) NOT NULL,
    `draw_order` INT(11) NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

ALTER TABLE `[%%]villa_homepage_gallery` ADD CONSTRAINT `fk_villa_homepage_gallery_entity_type_id`
    FOREIGN KEY (`entity_type_id`) REFERENCES `[%%]entity`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;

INSERT INTO `[%%]entity` SET `name` = 'Стартовая страница', `sys_name` = 'homepage';

SELECT @entity_homepage := `id` FROM `[%%]entity` WHERE `sys_name` = 'homepage';

INSERT INTO `[%%]villa_homepage_gallery`(`entity_type_id`, `entity_id`, `image`, `draw_order`)
    SELECT @entity_homepage, 1, `image`, `draw_order` FROM `[%%]villa_homepage_images`;

SELECT @entity_excursion := `id` FROM `[%%]entity` WHERE `sys_name` = 'excursion';

INSERT INTO `[%%]villa_homepage_gallery`(`entity_type_id`, `entity_id`, `image`, `draw_order`)
    SELECT @entity_excursion, `day`, `image`, `draw_order` FROM `[%%]villa_homepage_excursions_images`;

SELECT @entity_transfer := `id` FROM `[%%]entity` WHERE `sys_name` = 'transfer';

INSERT INTO `[%%]villa_homepage_gallery`(`entity_type_id`, `entity_id`, `image`, `draw_order`)
    SELECT @entity_transfer, 1, `image`, `draw_order` FROM `[%%]villa_homepage_transfer_images`;

DROP TABLE `[%%]villa_homepage_images`;
DROP TABLE `[%%]villa_homepage_excursions_images`;
DROP TABLE `[%%]villa_homepage_transfer_images`;
DROP TABLE `[%%]villa_homepage_services_images`;
DROP TABLE `[%%]villa_homepage_services`;