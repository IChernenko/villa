ALTER TABLE [%%]hotel_booking CHANGE `status` `status` INT( 11 ) NOT NULL ;

DROP TABLE IF EXISTS [%%]hotel_booking_statuses;

CREATE TABLE IF NOT EXISTS [%%]hotel_booking_statuses (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DELETE FROM [%%]hotel_booking;

INSERT INTO [%%]hotel_booking_statuses (`id`, `name`) VALUES
(1, 'Не обработано'),
(2, 'В обработке'),
(3, 'Обработано');

ALTER TABLE [%%]hotel_booking ADD
      CONSTRAINT fk_hotel_booking_status
      FOREIGN KEY (`status`)
      REFERENCES [%%]hotel_booking_statuses(id) ON DELETE CASCADE ON UPDATE CASCADE;