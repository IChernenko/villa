UPDATE `db_version` SET `package` = 'villa.module.hotel' WHERE `package` = 'module.hotel';

DROP TABLE IF EXISTS [%%]hotel_booking;

CREATE TABLE [%%]hotel_booking (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `arrival` int(11) NOT NULL,
  `departure` int(11) NOT NULL,
  `people` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;