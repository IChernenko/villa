ALTER TABLE `[%%]hotel_booking_guests` MODIFY `first_name` VARCHAR(50) DEFAULT NULL;
ALTER TABLE `[%%]hotel_booking_guests` MODIFY `last_name` VARCHAR(50) DEFAULT NULL;
ALTER TABLE `[%%]hotel_booking_guests` MODIFY `email` VARCHAR(100) DEFAULT NULL;
ALTER TABLE `[%%]hotel_booking_guests` MODIFY `phone` VARCHAR(50) DEFAULT NULL;

UPDATE `[%%]hotel_booking_guests` SET `first_name` = NULL WHERE `first_name` IN('-', '-- Заполнить! --', '');
UPDATE `[%%]hotel_booking_guests` SET `last_name` = NULL WHERE `last_name` IN('-', '-- Заполнить! --', '');
UPDATE `[%%]hotel_booking_guests` SET `email` = NULL WHERE `email` IN('-', '-- Заполнить! --', '');
UPDATE `[%%]hotel_booking_guests` SET `phone` = NULL WHERE `phone` IN('-', '-- Заполнить! --', '');
