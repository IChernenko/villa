CREATE TABLE `[%%]hotel_room_types` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(50) NOT NULL,
    `max_adults` INT(11) NOT NULL DEFAULT 1,
    `max_children` INT(11) NOT NULL DEFAULT 1,
    `link` VARCHAR(50) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `[%%]hotel_room_types` (`id`, `title`, `max_adults`, `max_children`, `link`)
    SELECT `id`, `title`, `max_adults`, `max_children`, `link` FROM `[%%]hotel_apartmentstypes`;

CREATE TABLE `[%%]hotel_room_types_lang` (
    `type_id` INT(11) NOT NULL,
    `lang_id` INT(11) UNSIGNED NOT NULL,
    `title` VARCHAR(50) NOT NULL,
    `description` TEXT NOT NULL,
    KEY `fk_hotel_room_types_lang_id` (`lang_id`),
    KEY `fk_hotel_room_types_type_id` (`type_id`),
    CONSTRAINT `fk_hotel_room_types_type_id` FOREIGN KEY (`type_id`) 
        REFERENCES `[%%]hotel_room_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `fk_hotel_room_types_lang_id` FOREIGN KEY (`lang_id`) 
        REFERENCES `[%%]languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `[%%]hotel_room_types_lang` (`type_id`, `lang_id`, `title`, `description`)
    SELECT `id`, `lang_id`, `title`, `description` FROM `[%%]hotel_apartmentstypes_lang`;

CREATE TABLE `[%%]hotel_room_types_images` (
    `type_id` INT(11) NOT NULL,
    `image` VARCHAR(50) NOT NULL,
    `draw_order` INT(11) NOT NULL,
    KEY `fk_hotel_room_types_images_type_id` (`type_id`),
    CONSTRAINT `fk_hotel_room_types_images_type_id` FOREIGN KEY (`type_id`) 
        REFERENCES `[%%]hotel_room_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `[%%]hotel_room_types_images` (`type_id`, `image`, `draw_order`)
    SELECT `type_id`, `image`, `draw_order` FROM `[%%]hotel_apartmentstypes_images`;

CREATE TABLE `[%%]hotel_room_types_equipment` (
    `type_id` INT(11) NOT NULL,
    `eq_id` INT(11) NOT NULL,
    `draw_order` INT(11) NOT NULL,
    `bold` TINYINT(1) NOT NULL,
    `font_size` TINYINT(2) NOT NULL,
    KEY `fk_hotel_room_types_equipment_type_id` (`type_id`),
    KEY `fk_hotel_room_types_equipment_eq_id` (`eq_id`),
    CONSTRAINT `fk_hotel_room_types_equipment_type_id` FOREIGN KEY (`type_id`) 
        REFERENCES `[%%]hotel_room_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `fk_hotel_room_types_equipment_eq_id` FOREIGN KEY (`eq_id`) 
        REFERENCES `[%%]hotel_equipment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `[%%]hotel_room_types_equipment` (`type_id`, `eq_id`, `draw_order`, `bold`, `font_size`)
    SELECT `apt_id`, `eq_id`, `sort_order`, `bold`, `fontsize` FROM `[%%]hotel_apartmentstypes_equipment`;

CREATE TABLE `[%%]hotel_rooms` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `type` int(11) NOT NULL,
    `priority` int(11) NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    KEY `fk_hotel_rooms_type` (`type`),
    CONSTRAINT `fk_hotel_rooms_type` FOREIGN KEY (`type`) 
        REFERENCES `[%%]hotel_room_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `[%%]hotel_rooms` (`id`, `type`, `priority`)
    SELECT `id`, `type`, `priority` FROM `[%%]hotel_apartments`;

DROP TABLE `[%%]hotel_apartments`;
DROP TABLE `[%%]hotel_apartmentstypes_equipment`;
DROP TABLE `[%%]hotel_apartmentstypes_images`;
DROP TABLE `[%%]hotel_apartmentstypes_lang`;
DROP TABLE `[%%]hotel_apartmentstypes`;

ALTER TABLE `[%%]hotel_booking` ADD CONSTRAINT `fk_hotel_booking_room_type_id`
    FOREIGN KEY (`room_type`) REFERENCES `[%%]hotel_room_types`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `[%%]hotel_booking` ADD CONSTRAINT `fk_hotel_booking_room_id`
    FOREIGN KEY (`room`) REFERENCES `[%%]hotel_rooms`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `[%%]hotel_prices` DROP FOREIGN KEY `fk_hotel_prices_prep_type`;

CREATE TABLE `[%%]hotel_prepayment_types` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(50) NOT NULL,
    PRIMARY KEY(`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `[%%]hotel_prepayment_types` (`id`, `title`)
    SELECT `id`, `title` FROM `[%%]hotel_booking_prepayment_types`;

CREATE TABLE `[%%]hotel_prepayment_types_lang` (
    `type_id` int(11) NOT NULL,
    `lang_id` int(11) unsigned NOT NULL,
    `name` varchar(50) NOT NULL,
    KEY `fk_hotel_prepayment_types_lang_type_id` (`type_id`),
    KEY `fk_hotel_prepayment_types_lang_lang_id` (`lang_id`),
    CONSTRAINT `fk_hotel_prepayment_types_lang_type_id` FOREIGN KEY (`type_id`) 
        REFERENCES `[%%]hotel_prepayment_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `fk_hotel_prepayment_types_lang_lang_id` FOREIGN KEY (`lang_id`) 
        REFERENCES `[%%]languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `[%%]hotel_prepayment_types_lang` (`type_id`, `lang_id`, `name`)
    SELECT `id`, `lang_id`, `name` FROM `[%%]hotel_booking_prepayment_types_lang`;

DROP TABLE `[%%]hotel_booking_prepayment_types_lang`;
DROP TABLE `[%%]hotel_booking_prepayment_types`;

ALTER TABLE `[%%]hotel_booking` ADD CONSTRAINT `fk_hotel_booking_prepayment_type`
    FOREIGN KEY (`prepayment_type`) REFERENCES `[%%]hotel_prepayment_types`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `[%%]hotel_prices` ADD CONSTRAINT `fk_hotel_prices_prep_type`
    FOREIGN KEY (`prep_type`) REFERENCES `[%%]hotel_prepayment_types`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;