CREATE TABLE `[%%]hotel_booking_new` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `creation_time` INT(11) NOT NULL, 
    `status` INT(11) NOT NULL, 
    `arrival` INT(11) NOT NULL,
    `departure` INT(11) NOT NULL,
    `days` INT(11) NOT NULL DEFAULT 1,
    `adults` INT(11) NOT NULL DEFAULT 1,
    `children` INT(11) NOT NULL DEFAULT 0,
    `prepayment_type` INT(11) NOT NULL,
    `room` INT(11) DEFAULT NULL,
    `room_type` INT(11) NOT NULL,
    `room_price` FLOAT(10,2) NOT NULL DEFAULT 0,
    `cost` FLOAT(10,2) NOT NULL DEFAULT 0,
    `paid_services` FLOAT(10,2) NOT NULL DEFAULT 0,
    `paid_residence` FLOAT(10,2) NOT NULL DEFAULT 0,
    `uid` INT(11) DEFAULT NULL,
    `first_name` VARCHAR(100) NOT NULL,
    `last_name` VARCHAR(100) NOT NULL,
    `email` VARCHAR(50) NOT NULL,
    `phone` VARCHAR(50) NOT NULL,
    `comment` TEXT,
    PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT = 1;

INSERT INTO `[%%]hotel_booking_new` (
    `id`,
    `creation_time`, 
    `status`, 
    `arrival`,
    `departure`,
    `days`,
    `adults`,
    `children`,
    `prepayment_type`,
    `room`,
    `room_type`,
    `room_price`,
    `cost`,
    `paid_services`,
    `paid_residence`,
    `uid`,
    `first_name`,
    `last_name`,
    `email`,
    `phone`,
    `comment`
) SELECT 
    `id`,
    `creation_time`,
    `status`,
    `arrival`,
    `departure`,
    `nights`,
    `adults`,
    `children`,
    `prepayment`,
    `apartment`,
    `type`,
    `apartmentCost`,
    `cost`,
    0,
    0,
    `uid`,
    `firstName`,
    `secondName`,
    `email`,
    `phone`,
    `comment`
    FROM `[%%]hotel_booking`;

ALTER TABLE `[%%]hotel_booking_guests` DROP FOREIGN KEY `fk_hotel_booking_guests_booking_id`;
ALTER TABLE `[%%]hotel_booking_services` DROP FOREIGN KEY `fk_hotel_booking_services_booking_id`;
ALTER TABLE `[%%]hotel_booking_excursions` DROP FOREIGN KEY `fk_hotel_booking_excursions_booking_id`;
ALTER TABLE `[%%]hotel_booking_transfer` DROP FOREIGN KEY `fk_hotel_booking_transfer_booking_id`;
ALTER TABLE `[%%]hotel_booking_payment_history` DROP FOREIGN KEY `fk_hotel_booking_deposit_history_booking_id`;
ALTER TABLE `[%%]hotel_booking_extra_services` DROP FOREIGN KEY `fk_booking_extra_services_booking_id`;

DROP TABLE `[%%]hotel_booking`;

RENAME TABLE `[%%]hotel_booking_new` TO `[%%]hotel_booking`;

ALTER TABLE `[%%]hotel_booking_guests` ADD CONSTRAINT fk_hotel_booking_guests_booking_id
    FOREIGN KEY (`booking_id`) REFERENCES `[%%]hotel_booking`(`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `[%%]hotel_booking_services` ADD CONSTRAINT `fk_hotel_booking_services_booking_id` 
    FOREIGN KEY (`booking_id`) REFERENCES `[%%]hotel_booking` (`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `[%%]hotel_booking_excursions` ADD CONSTRAINT fk_hotel_booking_excursions_booking_id
    FOREIGN KEY (`booking_id`) REFERENCES `[%%]hotel_booking`(`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `[%%]hotel_booking_transfer` ADD CONSTRAINT `fk_hotel_booking_transfer_booking_id` 
    FOREIGN KEY (`booking_id`) REFERENCES `[%%]hotel_booking` (`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `[%%]hotel_booking_payment_history` ADD CONSTRAINT `fk_hotel_booking_payment_history_booking_id` 
    FOREIGN KEY (`booking_id`) REFERENCES `tst_hotel_booking` (`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `[%%]hotel_booking_extra_services` ADD CONSTRAINT `fk_booking_extra_services_booking_id`
    FOREIGN KEY(`booking_id`) REFERENCES `[%%]hotel_booking`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;

-- ALTER TABLE `[%%]hotel_booking` ADD CONSTRAINT `fk_hotel_booking_apartment` 
--     FOREIGN KEY (`room`) REFERENCES `[%%]hotel_apartments` (`id`) 
--     ON DELETE CASCADE ON UPDATE CASCADE;
-- ALTER TABLE `[%%]hotel_booking` ADD CONSTRAINT `fk_hotel_booking_type` 
--     FOREIGN KEY (`room_type`) REFERENCES `[%%]hotel_apartmentstypes` (`id`) 
--     ON DELETE CASCADE ON UPDATE CASCADE;
-- ALTER TABLE `[%%]hotel_booking` ADD CONSTRAINT `fk_hotel_booking_prepayment` 
--     FOREIGN KEY (`prepayment_type`) REFERENCES `[%%]hotel_booking_prepayment_types` (`id`) 
--     ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `[%%]hotel_booking` ADD CONSTRAINT `fk_hotel_booking_status` 
    FOREIGN KEY (`status`) REFERENCES `[%%]hotel_booking_statuses` (`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `[%%]hotel_booking` ADD CONSTRAINT `fk_hotel_booking_uid` 
    FOREIGN KEY (`uid`) REFERENCES `[%%]users` (`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;