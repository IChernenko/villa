DROP TABLE IF EXISTS [%%]hotel_booking_prepayment_types;

CREATE TABLE [%%]hotel_booking_prepayment_types (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

UPDATE [%%]hotel_booking SET `prepayment` = '1';

INSERT INTO [%%]hotel_booking_prepayment_types (`id`, `title`) VALUES
(1, 'Без предоплаты'),
(2, 'Предоплата за первую ночь'),
(3, 'Предоплата за весь период проживания');

ALTER TABLE [%%]hotel_booking ADD
      CONSTRAINT fk_hotel_booking_prepayment
      FOREIGN KEY (`prepayment`)
      REFERENCES [%%]hotel_booking_prepayment_types(id) ON DELETE CASCADE ON UPDATE CASCADE;