<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 23.05.12
 * Time: 14:29
 * To change this template use File | Settings | File Templates.
 */
class Villa_Module_Hotel_Mapper_Roomtypes
{
    protected $_tableName = '[%%]hotel_room_types';
    protected $_tableNameLang = '[%%]hotel_room_types_lang';
    
    protected $_tableEquipment = '[%%]hotel_room_types_equipment';
    protected $_tableEquipmentLang = '[%%]hotel_equipment_lang';

    /**
     * Загрузка информации о типах аппартаментов
     * @param array $params
     */
    public function getList($params) {

        $sql = "select
            t.id, a.title, a.description, t.max_adults, t.max_children, t.link
        from [%%]hotel_room_types as t
        left join [%%]hotel_room_types_lang as a on (t.id=a.type_id and `a`.`lang_id` = {$params['lang']})";
        if (isset($params['types'])) {
            $typesList = implode(',', $params['types']);
            $sql .= " WHERE t.id IN ($typesList) ";
        }

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $list = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $model =  new Villa_Module_Hotel_Model_Manage_Roomtypes();
            $model->id              = (int)$f['id'];
            $model->title           = $f['title'];
            $model->description     = $f['description'];
            $model->link            = 'appartments/'.$f['link'].'.html';
            $model->max_adults = $f['max_adults'];
            $model->max_children = $f['max_children'];
            $model->images          = $this->getImages((int)$f['id']);
            $list[$model->id] = $model;
        }
        if (count($list)==0) return array();
        
        // загружаем картинки
        if (isset($params['types'])) {
            $images = $this->getFirstImage($params['types']);
            foreach($images as $typeId => $image) {
                $list[$typeId]->image = '/media/hotel/roomtypes/'.$image;
            }
        }


        return $list;
    }
    
    public function getMaxGuests()
    {
        $sql = "SELECT 
                    MAX(`max_adults`) AS `max_adults`, 
                    MAX(`max_children`) AS `max_children`
                FROM {$this->_tableName}";
                
        $f = Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
        
        return $f;
    }

    /**
     * Получить весь список аппартаментов
     */
    public function getAllByLang($lang) {
        $sql = "select t.id, a.title 
                from [%%]hotel_room_types as t
                left join [%%]hotel_room_types_lang as a on (a.type_id = t.id and a.lang_id = {$lang})";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);        
        $list = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $list[$f['id']] = $f['title'];
        }
        return $list;
    }
    
    /**
     * Получение модели типа номера по ссылке
     * @param string $link
     * @param int $lang
     * @return Villa_Module_Hotel_Model_Manage_Roomtypes
     */
    public function getByLink($link, $lang) {
        $sql = "select id
                from [%%]hotel_room_types as t
                where t.link='{$link}'";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        if (Dante_Lib_SQL_DB::get_instance()->getNumRows($r) == 0) return false;
        $f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r);
        $types = array((int)$f['id']);

        $list = $this->getList(array(
            'types' => $types,
            'lang' => $lang
        ));
        $model = array_pop($list);

        // загружаем картинки
        $model->images = $this->getImages($f['id']);

        // загружаем оборудование
        $equipmentMapper = new Villa_Module_Hotel_Mapper_Equipment();
        $model->equipment = $equipmentMapper->get($f['id'], $lang);

        return $model;
    }
    
    public function get($id, $lang) {
        $sql = "select
            a.id, a.title, a.description, t.link, t.cost
        from [%%]hotel_room_types as t
        left join [%%]hotel_room_types_lang as a on (t.id=a.type_id)
        where a.lang_id = {$lang} and t.id = $id ";
        
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r);
        
        $model =  new Villa_Module_Hotel_Model_Manage_Roomtypes();
        $model->id              = (int)$f['id'];
        $model->title           = $f['title'];
        $model->description     = $f['description'];
        $model->link            = 'appartments/'.$f['link'].'.html';

        // загружаем картинки
        $model->images = $this->getImages($f['id']);
        print_r($model->images);
        
        // загружаем оборудование
        $equipmentMapper = new Villa_Module_Hotel_Mapper_Equipment();
        $model->equipment = $equipmentMapper->get($f['id'], $lang);

        return $model;
    }

    public function getPriceById($id) {
        $sql = "select cost from [%%]hotel_room_types as t where t.id={$id}";
        return Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'cost');
    }

    public function getAll($params) {
        $sql = "select id from [%%]hotel_room_types";

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $types = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $types[] = (int)$f['id'];
        }
        if (count($types)==0) return false;

        $params['types'] = $types;
        return $this->getList($params);
    }

    public function getFirstImage($types) {
        $typesList = is_array($types) ? implode(',', $types) : $types;
        $sql = "select type_id, image from [%%]hotel_room_types_images where type_id in ({$typesList}) group by type_id";

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $list = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $list[(int)$f['type_id']] = $f['image'];
        }

        return $list;
    }

    /**
     * Загрузка картинок заданного типа
     */
    public function getImages($typeId) {
        $sql = "select image from [%%]hotel_room_types_images where type_id = {$typeId} order by draw_order";

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $list = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $list[] = '/media/hotel/roomtypes/'.$f['image'];
        }

        return $list;
    }
    
    public function getEquipment($typeId, $langId = false)
    {
        $equipment = array();
        
        if(!$langId) $langId = Module_Lang_Helper::getCurrent();
        
        $sql = "SELECT * FROM {$this->_tableEquipment} AS `e`
                LEFT JOIN {$this->_tableEquipmentLang} AS `el` 
                ON(`e`.`eq_id` = `el`.`id` AND `el`.`lang_id` = {$langId})
                WHERE `e`.`type_id` = {$typeId} 
                ORDER BY `e`.`draw_order` ASC";
                
        if(!$r = Dante_Lib_SQL_DB::get_instance()->open($sql)) return false;
        
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            if(!$f['name']) continue;

            $model = new Villa_Module_Hotel_Model_Equipment();
            $model->id = $f['id'];
            $model->name = $f['name'];
            $model->bold = $f['bold'];
            $model->font_size = $f['font_size'];

            $equipment[$model->id] = $model;
        }
        
        return $equipment;
    }
    
    public function getNamesList() 
    {
        $lang = Module_Lang_Helper::getCurrent();
        
        $sql = "SELECT `r`.`id`, `rl`.`title` FROM {$this->_tableName} AS `r`
                LEFT JOIN {$this->_tableNameLang} `rl` 
                ON(`r`.`id` = `rl`.`type_id` AND `rl`.`lang_id` = {$lang})";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
                
        $names = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            $names[$f['id']] = $f['title'];
        }
        return $names;
    }
}