<?php
/**
 * Description of excursion
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Mapper_Excursion 
{
    protected $_tableName = '[%%]villa_homepage_excursions';
    protected $_tableImages = '[%%]villa_homepage_gallery';
    
    public function getByDay($day)
    {
        $lang = Module_Lang_Helper::getCurrent();
        $sql = "SELECT `day`, `title`, `full_descr` FROM {$this->_tableName} 
                WHERE `lang_id` = {$lang} AND `day` = {$day}";
        
        $excursion = Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
        $excursion['images'] = array();
        
        $entity = Villa_Helper_Entity::getExcursion();
        $sql = "SELECT `image` FROM {$this->_tableImages} 
                WHERE `entity_type_id` = {$entity} AND `entity_id` = {$day}
                ORDER BY `draw_order` ASC";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
            $excursion['images'][] = Villa_Module_Homepage_Helper::getImageDir().$f['image'];
        
        return $excursion;
    }
}

?>
