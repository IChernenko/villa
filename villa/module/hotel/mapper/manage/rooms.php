<?php

/**
 * Description of rooms
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Mapper_Manage_Rooms extends Module_Handbooksgenerator_Mapper_Base
{
    
    protected $_tableName = '[%%]hotel_rooms';
    protected $_tableTypesName = '[%%]hotel_room_types';
    protected $_tableTypesLangName = '[%%]hotel_room_types_lang';
    
    protected $_tableBooking = '[%%]hotel_booking';
        
    
    public function getById($id)
    {
        $model = new Villa_Module_Hotel_Model_Manage_Rooms();
        if(!$id) return $model;
        
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->select(array('id' => $id));
        $model->id = $table->id;
        $model->type = $table->type;
        $model->priority = $table->priority;
        
        return $model;
    }
    
    /**
     * обновляем номер
     * @param Villa_Module_Hotel_Model_Manage_Rooms $model 
     */
    public function apply(Villa_Module_Hotel_Model_Manage_Rooms $model, $prevId) 
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);   
        
        // Проверка на существование такого номера в таблице
        if($prevId != $model->id)
        {
            $table->select(array('id' => $model->id));
            if($table->getNumRows() > 0) return false;
        }
        
        $table->id = $model->id;
        $table->type = $model->type;
        $table->priority = $model->priority;
        
        if($prevId)
            $table->update(array('id' => $prevId));
        else
            $table->insert();
        
        return true;
    }

    public function getApartmentsNumbers()
    {
        $sql = "select * from ".$this->_tableName;
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $roomsNumbers = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            $roomsNumbers[$f['id']] = $f['id'];
        }
        return $roomsNumbers;
    }
    
    protected function _getRoomSearchSql($typeId, $arrival, $departure, $currentBooking)
    {
        $sql = "SELECT `a`.`id` FROM {$this->_tableName} AS `a`
                WHERE `a`.`type` = {$typeId} AND `a`.`id` NOT IN (
                        SELECT `b`.`room` FROM {$this->_tableBooking} AS `b`
                        WHERE {$arrival} < `b`.`departure` 
                        AND {$departure} > `b`.`arrival` 
                        AND `b`.`room` > 0
                        AND `b`.`id` != {$currentBooking})";
        
        return $sql;
    }


    public function getFreeRooms($typeId, $arrival, $departure, $currentBooking = 0)
    {
        $sql = $this->_getRoomSearchSql($typeId, $arrival, $departure, $currentBooking);
        
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $rooms = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) 
        {
            $rooms[] = $f['id'];
        }
        return $rooms;
    }
    
    public function getRoomForBooking($typeId, $arrival, $departure, $currentBooking = 0)
    {
        $sql = $this->_getRoomSearchSql($typeId, $arrival, $departure, $currentBooking).
                " ORDER BY `a`.`priority` ASC LIMIT 1";
        
        return Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'id');
    }
}

?>