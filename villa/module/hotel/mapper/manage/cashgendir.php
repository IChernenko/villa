<?php
/**
 * Description of cashcontrol
 *
 * @author Mort
 */
class Villa_Module_Hotel_Mapper_Manage_Cashgendir {
    
    protected $_tableName = '[%%]hotel_cash';
    protected $_transactionsTableName = '[%%]hotel_cash_transactions';
    protected $_profileTableName = '[%%]user_profile';
    
    /**
     * выбираем состояния по счетам
     * @return array 
     */
    public function getCash()
    {
        $sql = "select
                    *
                from ".$this->_tableName." AS c INNER JOIN ".$this->_profileTableName." AS p ON c.user_id=p.user_id";

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $cash = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $cash[$f['user_id']] = $f;
        }
        
        return $cash;
    }
    
    /**
     * выбираем транзакции
     * @return array 
     */
    public function getCashHistory()
    {
        $sql = "select
                    ct.*, c1.fio AS user_from_name, c2.fio AS user_to_name
                from ".$this->_transactionsTableName." AS ct 
                    INNER JOIN ".$this->_profileTableName." AS c1
                    ON ct.user_from = c1.user_id
                    INNER JOIN ".$this->_profileTableName." AS c2
                    ON ct.user_to = c2.user_id";

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $cash = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $cash[$f['id']] = $f;
        }
        
        return $cash;
    }
}

?>
