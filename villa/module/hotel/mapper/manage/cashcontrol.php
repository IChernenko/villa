<?php
/**
 * Description of cashcontrol
 *
 * @author Mort
 */
class Villa_Module_Hotel_Mapper_Manage_Cashcontrol {
    
    protected $_tableName = '[%%]hotel_cash';
    protected $_transactionsTableName = '[%%]hotel_cash_transactions';
    protected $_profileTableName = '[%%]user_profile';
    
    protected $_groupsTableName = '[%%]groups';
    protected $_userGroupsTableName = '[%%]user_groups';
    
    
    /**
     * выбираем состояния по счетам
     * @return array 
     */
    public function getCash()
    {
        $sql = "select
                    *
                from ".$this->_tableName." AS c 
                    INNER JOIN ".$this->_profileTableName." AS p 
                        ON c.user_id=p.user_id
                        INNER JOIN ".$this->_userGroupsTableName." AS ug 
                        ON c.user_id=ug.user_id
                        INNER JOIN ".$this->_groupsTableName." AS g 
                        ON ug.group_id=g.group_id
                WHERE g.group_id=1";

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $cash = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $cash[$f['user_id']] = $f;
        }
        
        return $cash;
    }
    
    /**
     * добавляем транзакцию
     * @param int $userIdFrom
     * @param int $userIdTo
     * @param int $summ
     * @param string $description 
     */
    public function addDeposit($userIdFrom, $userIdTo, $summ, $description) {
        $table = new Dante_Lib_Orm_Table($this->_transactionsTableName);
        $table->user_from = $userIdFrom;
        $table->user_to = $userIdTo;
        $table->summ = $summ;
        $table->date = time();
        $table->description = $description;

        $table->id = $table->insert();
        
        $this->updateUserBalance($userIdFrom, -$summ);
        $this->updateUserBalance($userIdTo, $summ);
        
    }
    
    /**
     * обновляем баланс пользователя
     * @param int $userId
     * @param int $summ 
     */
    public function updateUserBalance($userId, $summ) {
        //теперь пополнить бы счет данного юзера
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $cond = array(
            'user_id'=>$userId
            );
        if(!$table->exist($cond)){
            //создаем запись
            $table->user_id = $userId;
            $table->balance = $summ;
            $table->insert();
        }else{
            //апдейтим запись
            $table->select($cond);
            $table->balance = $table->balance+$summ;
            $table->update($cond);
        }
    }
}

?>
