<?php

/**
 * Description of services
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Mapper_Manage_Services extends Module_Handbooksgenerator_Mapper_Base
{
    
    protected $_tableName = '[%%]hotel_services';    
    protected $_tableNameLang = '[%%]hotel_services_lang';
    protected $_imagesTableName = '[%%]hotel_services_images';    
    
    public function getOne($id, $lang = 1)
    {
        $model = new Villa_Module_Hotel_Model_Manage_Service();
        $model->lang_id = $lang;
        if(!$id) return $model;
        
        $sql = "SELECT * FROM {$this->_tableName} AS `o` LEFT JOIN {$this->_tableNameLang} AS `ol` 
                ON(`ol`.`service_id` = `o`.`id` AND `ol`.`lang_id` = {$lang}) WHERE `o`.`id` = {$id}";
                
        $f = Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
        $model->id = $id;
        $model->sys_name = $f['sys_name'];
        $model->image = $f['image'];
        $model->name = $f['name'];
        $model->description = $f['description'];
        $model->full_descr = $f['full_descr'];
        $model->outer_id = $f['outer_id'];

        
        return $model;
    }
    
    /**
     * обновляем опции бронирования
     * @param Villa_Module_Hotel_Model_Manage_Service $params 
     */
    public function apply(Villa_Module_Hotel_Model_Manage_Service $model) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->id = $model->id;
        $table->sys_name = $model->sys_name;
        $table->outer_id = $model->outer_id;
//        $table->image = $model->image;
        
        if($model->id) 
            $table->update(array('id' => $model->id));
        else
            $model->id = $table->insert();
        
        //а есть ли запись сео для данной штуки ? 
        $tableLang = new Dante_Lib_Orm_Table($this->_tableNameLang);
        $tableLang->service_id = $model->id;
        $tableLang->lang_id = $model->lang_id;
        $tableLang->name = $model->name;
        $tableLang->description = $model->description;
        $tableLang->full_descr = $model->full_descr;

        $cond = array(
            'service_id' => $model->id,
            'lang_id' => $model->lang_id
        );
        
        if($tableLang->exist($cond))
            $tableLang->update($cond);
        else
            $tableLang->insert();
    }
        
    public function getNamesList() 
    {
        $list = Villa_Module_Hotel_Dmo_Bookingoptions::dmo()->fetchAll('id');
        foreach($list as &$val)
        {
            $val = $val['sys_name'];
        }
        return $list;
    }

    /**
     * выборка изображений для определенного типа опции
     * @param int $serviceId - ID опции
     * @return array
     */
    public function getImages($serviceId)
    {
        $sql = "SELECT * FROM {$this->_imagesTableName}
             WHERE `service_id` = {$serviceId} ORDER BY `draw_order` ASC";
             
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $images = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $images[] = $f;
        }
        
        return $images;
    }

    public function getCurrentImage($id)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        return $table->select(array('id'=>$id))->image;
    }

    /**
     * добавление изображения
     * @param int $typeId
     * @param string $fileName
     */
    public function addImage($serviceId, $fileName)
    {
        $table = new Dante_Lib_Orm_Table($this->_imagesTableName);
        $table->service_id = $serviceId;
        $table->image = $fileName;
        $table->draw_order = $this->_getLastImgOrder($serviceId)+1;

        $table->insert();
    }

    protected function _getLastImgOrder($serviceId)
    {
        $sql = "SELECT `draw_order` FROM {$this->_imagesTableName}
                WHERE `service_id` = {$serviceId} ORDER BY `draw_order` DESC LIMIT 1";

        return Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'draw_order');
    }

    /**
     * удаление изображения
     * @param int $typeId
     * @param string $image
     */
    public function delImage($imageId)
    {
        $table = new Dante_Lib_Orm_Table($this->_imagesTableName);        
        $table->delete(array(
            'id' => $imageId
        ));
    }

    /**
     * меняем порядок отображения картинки
     * @param int $id
     * @param int $orderFactor
     */
    public function changeOrder($serviceId, $imageId, $drawOrder, $orderFactor)
    {
        $switchRow = $this->_getImageByDrawOrder($serviceId, $drawOrder+$orderFactor);
        
        if(!$switchRow || !$switchRow->draw_order ){
            return;
        }
        
        $table = new Dante_Lib_Orm_Table($this->_imagesTableName);
        $table->draw_order = $drawOrder+$orderFactor;

        $table->apply(array(
            'id'=>$imageId,
        ));

        $table = new Dante_Lib_Orm_Table($this->_imagesTableName);
        $table->draw_order = $drawOrder;

        $table->apply(array(
            'id'=>$switchRow->id
        ));
    }

    /**
     * выбираем изображение с заданным порядком отображения
     * @param int $typeId
     * @param int $drawOrder
     * @return Dante_Lib_Orm_Table
     */
    protected function _getImageByDrawOrder($serviceId, $drawOrder)
    {
        $table = new Dante_Lib_Orm_Table($this->_imagesTableName);

        $table->get()->where(array('service_id'=>$serviceId, 'draw_order'=>$drawOrder))
            ->limit(array(0, 1))->orderby('draw_order desc');
        $table->fetch();

        return $table;
    }
}

?>
