<?php

/**
 * Description of loading
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Mapper_Manage_Loading
{
    protected $_tableName = '[%%]hotel_booking';
    protected $_tableStatuses = '[%%]hotel_booking_statuses';
    
    protected $_tableBookingTransfer = '[%%]hotel_booking_transfer';
    protected $_tableTransfer = '[%%]hotel_transfer';
    protected $_tableBookingService = '[%%]hotel_booking_services';
    protected $_hotel_booking_payment_history = '[%%]hotel_booking_payment_history';

    /**
     * выбираем бронирования в заданый промежуток дат
     * @param int $dateFrom
     * @param int $dateTo
     * @return array 
     */
    public function getBookings($dateFrom, $dateTo, $service = 0)
    {
        $sql = "SELECT * FROM {$this->_tableBookingTransfer} AS `bt`
                LEFT JOIN {$this->_tableTransfer} AS `t` ON(`t`.`id` = `bt`.`transfer_id`)";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        
        $transfers = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            if(!isset($transfers[$f['booking_id']]))
                $transfers[$f['booking_id']] = array();
            
            switch($f['type'])
            {
                case TRANSFER_TYPE_ARRIVAL:
                    $transfers[$f['booking_id']]['arrival'] = $f;
                    break;
                
                case TRANSFER_TYPE_DEPARTURE:
                    $transfers[$f['booking_id']]['departure'] = $f;
                    break;
            }            
        }

        $sql = "SELECT 
                        `b`.`id`, `b`.`status`, `b`.`arrival`, `b`.`departure`, `b`.`adults`, 
                        `b`.`children`, `b`.`cost`, `b`.`room`, `s`.`name` AS `status_name` ".(($service > 0 ? ", `c`.`count` as service_count " : ""))."
                FROM {$this->_tableName} AS `b`
                LEFT JOIN {$this->_tableStatuses} AS `s` ON (`b`.`status` = `s`.`id`)
                ".($service > 0 ? "JOIN {$this->_tableBookingService} AS `c` ON (`c`.`booking_id` = `b`.`id`)" : "")."
                WHERE `b`.`arrival` >= {$dateFrom} AND `b`.`departure` <= {$dateTo}
                ".($service > 0 ? " AND `c`.`service_id` = '$service'" : "")."
                ";

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        
        $bookingArr = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) 
        {
            if(!isset($bookingArr[$f['room']])) $bookingArr[$f['room']] = array();
            
            $f['transfer'] = isset($transfers[$f['id']]) ? $transfers[$f['id']] : false;
            
            $bookingArr[$f['room']][] = $f;
        }

        $bookingArr['test'] = '1';
        
        return $bookingArr;
    }

    public function getLastServices() {
        $bookingArray = array();

        $sql = "SELECT * FROM {$this->_tableBookingService} AS tablee WHERE version = ( SELECT max(version) FROM {$this->_tableBookingService} WHERE booking_id = tablee.booking_id GROUP BY booking_id  )";

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $booking_id = intval($f['booking_id']);
            $service_id = intval($f['service_id']);
//            $service_guest_type = intval($f['guest_type']);
            $count = 1;
            if ( array_key_exists($booking_id, $bookingArray ) && array_key_exists($service_id, $bookingArray[$booking_id] )) {
                $bookingArray[$booking_id][$service_id] += $count;
            } else {
                $bookingArray[$booking_id][$service_id] = $count;
            }
        }

        return $bookingArray;
    }

    public function getServiceChild() {
        $bookingArray = array();

        $sql = "SELECT * FROM {$this->_tableBookingService} AS tablee WHERE version = ( SELECT max(version) FROM {$this->_tableBookingService} WHERE booking_id = tablee.booking_id GROUP BY booking_id  )";

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $booking_id = intval($f['booking_id']);
            $guest_type = intval($f['guest_type']);
            $service_id = intval($f['service_id']);
            $count = 1;
            if ( $guest_type == 2 ) {
                if ( array_key_exists($booking_id, $bookingArray ) && array_key_exists($service_id, $bookingArray[$booking_id] )) {
                    $bookingArray[$booking_id][$service_id] += $count;
                } else {
                    $bookingArray[$booking_id][$service_id] = $count;
                }
            }
        }

        return $bookingArray;
    }
    public function getPayRemain() {
        $paymentArray = array();

        $sql = "SELECT * FROM {$this->_hotel_booking_payment_history} WHERE booking_id IN (SELECT id FROM {$this->_tableName})";

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
//            array_push($paymentArray, $f);
            $booking_id = intval($f['booking_id']);
            $amount = intval($f['amount']);

            if ( array_key_exists($booking_id, $paymentArray )) {
                $paymentArray[$booking_id] += $amount;
            } else {
                $paymentArray[$booking_id] = $amount;
            }
        }

        return $paymentArray;
    }

}





?>
