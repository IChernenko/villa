<?php
/**
 * Description of prices
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Mapper_Manage_Prices 
{
    protected $_tableName = '[%%]hotel_prices';
    protected $_tableNameDates = '[%%]hotel_prices_dates';
        
    public function getAllPrices() 
    {
        $listModel = new Villa_Module_Hotel_Model_Manage_Priceslist();
        
        $room = Villa_Helper_Entity::getRoomType();
        $service = Villa_Helper_Entity::getService();
        $transfer = Villa_Helper_Entity::getTransfer(); 
        $excursion = Villa_Helper_Entity::getExcursion();
        
        $sql = "SELECT * FROM {$this->_tableName}";
        if(!$r = Dante_Lib_SQL_DB::get_instance()->open($sql)) return false;
        
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            switch($f['entity_type_id'])
            {
                case $room:
                    if(!isset($listModel->roomTypesPrices[$f['entity_id']]))
                        $listModel->roomTypesPrices[$f['entity_id']] = array();
                    
                    $listModel->roomTypesPrices[$f['entity_id']][$f['prep_type']] = $this->_populateModel($f);                    
                    break;
                    
                case $service:
                    $listModel->servicesPrices[$f['entity_id']] = $this->_populateModel($f);
                    break;
                
                case $transfer:
                    $listModel->transferPrices[$f['entity_id']] = $this->_populateModel($f);
                    break;
                
                case $excursion:
                    $listModel->excursionPrices[$f['entity_id']] = $this->_populateModel($f);
                    break;
            }
        }
        
        return $listModel;
    }  
    
    protected function _populateModel($f)
    {
        $model = new Villa_Module_Hotel_Model_Manage_Prices();
        $model->id = $f['id'];
        $model->entity_type_id = $f['entity_type_id'];
        $model->entity_id = $f['entity_id'];
        $model->prep_type = $f['prep_type'];
        $model->standardPrice = $f['standardPrice'];
        $model->displayedPrice = $f['displayedPrice'];
        
        return $model;
    }
    
    public function apply($model)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->entity_type_id = $model->entity_type_id;
        $table->entity_id = $model->entity_id;
        $table->prep_type = $model->prep_type;
        $table->standardPrice = $model->standardPrice;
        $table->displayedPrice = $model->displayedPrice;
        
        if($model->id) $table->update(array('id' => $model->id));
        else $table->insert();
    }
    
    public function del($id)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->delete(array('id' => $id));
    }
    
    public function getById($id)
    {
        $model = new Villa_Module_Hotel_Model_Manage_Prices();
        if(!$id) return $model;
        
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->select(array('id' => $id));
        
        $model->id = $id;
        $model->entity_type_id = $table->entity_type_id;
        $model->entity_id = $table->entity_id;
        $model->prep_type = $table->prep_type;
        $model->standardPrice = $table->standardPrice;
        $model->displayedPrice = $table->displayedPrice;
        
        return $model;
    }
    
    public function getDates($id)
    {
        $sql = "SELECT * FROM {$this->_tableNameDates} WHERE `price_id` = {$id}";
        if(!$r = Dante_Lib_SQL_DB::get_instance()->open($sql)) return false;
        
        $list = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            $model = new Villa_Module_Hotel_Model_Manage_Pricesdate();
            $model->id = $f['id'];
            $model->price_id = $f['price_id'];
            $model->dateFrom = $f['dateFrom'];
            $model->dateTo = $f['dateTo'];
            $model->price = $f['price'];
            $model->disabled = $f['disabled'];
            $list[$f['id']] = $model;
        }
        return $list;
    }
    
    public function applyDate($model)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableNameDates);
        $table->price_id = $model->price_id;
        $table->dateFrom = $model->dateFrom;
        $table->dateTo = $model->dateTo;
        $table->price = $model->price;
        $table->disabled = $model->disabled;
        
        if($model->id) $table->update(array('id' => $model->id));
        else $table->insert();
    }
    
    public function delDate($id)
    {        
        $table = new Dante_Lib_Orm_Table($this->_tableNameDates);
        $table->delete(array('id' => $id));
    }
    
    /**
     * 
     * @return Villa_Module_Hotel_Model_Manage_Priceslist 
     */
    public function getCurrentPriceList($now = false)
    {        
        $listModel = new Villa_Module_Hotel_Model_Manage_Priceslist();
        
        $roomType = Villa_Helper_Entity::getRoomType();
        $service = Villa_Helper_Entity::getService();
        $transfer = Villa_Helper_Entity::getTransfer();    
        $excursion = Villa_Helper_Entity::getExcursion();    
        
        if(!$now) $now = time();
        
        $sql = "SELECT `p`.`entity_type_id`, `p`.`entity_id`, `p`.`prep_type`, `p`.`displayedPrice`, `pd`.`disabled`,
                IF(`pd`.`price`, `pd`.`price`,  `p`.`standardPrice`) AS `price`
                FROM {$this->_tableName} AS `p` 
                LEFT JOIN {$this->_tableNameDates} AS `pd` 
                ON(`pd`.`price_id` = `p`.`id` AND `pd`.`dateFrom` <= {$now} AND `pd`.`dateTo` >= {$now})";
        if(!$r = Dante_Lib_SQL_DB::get_instance()->open($sql)) return false;
        
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            switch($f['entity_type_id'])
            {
                case $roomType:
                    $listModel->roomTypesPrices[] = array(
                        'id' => $f['entity_id'],
                        'prep_type' => $f['prep_type'],
                        'displayedPrice' => $f['displayedPrice'],
                        'price' => $f['price'],
                        'disabled' => $f['disabled']
                    );
                    break;
                case $service:
                    $listModel->servicesPrices[$f['entity_id']] = array(
                        'id' => $f['entity_id'],
                        'displayedPrice' => $f['displayedPrice'],
                        'price' => $f['price'],
                        'disabled' => $f['disabled']
                    );
                    break;
                case $transfer:
                    $listModel->transferPrices[$f['entity_id']] = array(
                        'id' => $f['entity_id'],
                        'displayedPrice' => $f['displayedPrice'],
                        'price' => $f['price'],
                        'disabled' => $f['disabled']
                    );
                    break;                
                case $excursion:
                    $listModel->excursionPrices[$f['entity_id']] = array(
                        'id' => $f['entity_id'],
                        'displayedPrice' => $f['displayedPrice'],
                        'price' => $f['price'],
                        'disabled' => $f['disabled']
                    );
                    break;
            }
        }
        return $listModel;
    }
    
    public function getPrice($entityType, $entityId, $prepType = false, $now = false)
    {
        if(!$now) $now = time();
        
        $sql = "SELECT IF(`pd`.`price`, `pd`.`price`,  `p`.`standardPrice`) AS `price`
                FROM {$this->_tableName} AS `p` 
                LEFT JOIN {$this->_tableNameDates} AS `pd` 
                ON(`pd`.`price_id` = `p`.`id` AND `pd`.`dateFrom` < {$now} AND `pd`.`dateTo` > {$now})
                WHERE `p`.`entity_type_id` = {$entityType} AND `p`.`entity_id` = {$entityId}";
                
        if($prepType)
            $sql .= " AND `p`.`prep_type` = {$prepType}";
            
        return Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'price');
    }
    
    public function getAppartmentTypesPrices()
    {
        $entity = Villa_Helper_Entity::getRoomType();
        
        $sql = "SELECT * FROM {$this->_tableName} `p`
                LEFT JOIN {$this->_tableNameDates} AS `pd` 
                ON (`pd`.`price_id` = `p`.`id`) 
                WHERE `p`.`entity_type_id` = {$entity} ORDER BY `p`.`id`";
                
        if(!$r = Dante_Lib_SQL_DB::get_instance()->open($sql)) return false;
        
        $priceList = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            $apTypeId = $f['entity_id'];
            $prepTypeId = $f['prep_type'];
            
            if(!isset($priceList[$apTypeId]))
                $priceList[$apTypeId] = array();
            
            if(!isset($priceList[$apTypeId][$prepTypeId]))
                $priceList[$apTypeId][$prepTypeId] = array(
                    'main' => array(                    
                        'standardPrice' => Module_Currency_Helper::convert($f['standardPrice']),
                        'displayedPrice' => Module_Currency_Helper::convert($f['displayedPrice'])
                    )
                );
            
            if($f['dateFrom'] && $f['dateTo'] && $f['price'])
            {
                if(!isset($priceList[$apTypeId][$prepTypeId]['dates']))
                    $priceList[$apTypeId][$prepTypeId]['dates'] = array();
                
                $priceList[$apTypeId][$prepTypeId]['dates'][] = array(
                    'dateFrom' => $f['dateFrom'],
                    'dateTo' => $f['dateTo'],
                    'price' => Module_Currency_Helper::convert($f['price'])
                );
            }
        }
        
        return $priceList;
    }
}

?>
