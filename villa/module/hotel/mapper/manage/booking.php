<?php

/**
 * Description of booking
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Mapper_Manage_Booking extends Module_Handbooksgenerator_Mapper_Base
{
    protected $_tableName = '[%%]hotel_booking';
    protected $_tableStatuses = '[%%]hotel_booking_statuses';
    
    protected $_tableGuests = '[%%]hotel_booking_guests';
    protected $_tableServices = '[%%]hotel_booking_services';
    protected $_tableExcursions = '[%%]hotel_booking_excursions';
    protected $_tableTransfer = '[%%]hotel_booking_transfer';
    protected $_tableExtraServices = '[%%]hotel_booking_extra_services';
    
    protected $_paymentTable = '[%%]hotel_booking_payment_history';

    protected $_tableRoomTypes = '[%%]hotel_room_types';
    protected $_tableTransferList = '[%%]hotel_transfer';
    
    protected $_versionsTableName = '[%%]hotel_booking_versions';


    /**
     * @deprecated
     * @var type 
     */
    protected $_optionsSamplesLangTableName = '[%%]hotel_services_lang';
    /**
     * @deprecated
     * @var type 
     */
    protected $_transferSamplesTableName = '[%%]hotel_transfer';
    /**
     * @deprecated
     * @var type 
     */
    protected $_transferSamplesLangTableName = '[%%]hotel_transfer_lang';
   
    
    public function getList($params)
    {
        $cols = array(
            "`t1`.*", 
            "`t2`.`first_name`", 
            "`t2`.`last_name`", 
            "`t2`.`email`", 
            "`t2`.`phone`"
        );
        $join = "LEFT JOIN {$this->_tableGuests} AS `t2` 
                ON(`t1`.`id` = `t2`.`booking_id` AND `t1`.`version` = `t2`.`version` AND `t2`.`guest_num` = 1 AND `t2`.`guest_type` = ".GUEST_TYPE_ADULT.")";
        
        return $this->getRowsByParams($params, $cols, $join);
    }
    /**
     * Выборка данных бронирования для страницы редактирования
     * бронирования номера в личном кабинете пользователя
     * @param $id - идентификатор номера
     * @return Villa_Module_Hotel_Model_Manage_Booking
     */
    public function getById($id)
    {
        $model = new Villa_Module_Hotel_Model_Manage_Booking();
        if(!$id) return $model;
        
        $sql = "SELECT `b`.*, `r`.`max_adults`, `r`.`max_children` 
                FROM {$this->_tableName} AS `b` 
                LEFT JOIN {$this->_tableRoomTypes} AS `r` ON(`b`.`room_type` = `r`.`id`)
                WHERE `b`.`id` = ".$id;
        $f = Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
        
        $model->id = $id;
        $model->version = $f['version'];
        $model->modified = $f['modified'];
        $model->modified_by = $f['modified_by'];
        $model->basicData->arrival = Dante_Helper_Date::converToDateType($f['arrival'], 0);
        $model->basicData->departure = Dante_Helper_Date::converToDateType($f['departure'], 0);
        $model->basicData->adults = $f['adults'];
        $model->basicData->children = $f['children'];
        $model->days = $f['days'];
        $model->roomData->room_id = $f['room'];
        $model->roomData->type_id = $f['room_type'];
        $model->roomData->prepayment = $f['prepayment_type'];
        $model->roomData->max_adults = $f['max_adults'];
        $model->roomData->max_children = $f['max_children'];
        $model->roomData->setPrice($f['room_price']);
        $model->status = $f['status'];
        $model->user_currency = $f['currency_id'];
        $model->uid = $f['uid'];
        $model->creation_time = $f['creation_time'];
        $model->comment = $f['comment'];
          
        $model->init();
        
        $model->totalsData->setDiscountsByValues($f['dc_residence'], $f['dc_options']);
        
        $model->totalsData->addOptionsPayment($f['paid_options']);
        $model->totalsData->addResidencePayment($f['paid_residence']);
        
        return $this->_getAdditionalModelData($model);
    }
    
    protected function _getAdditionalModelData(Villa_Module_Hotel_Model_Manage_Booking $model)
    {
        $sql = "SELECT * FROM ".$this->_tableGuests." WHERE `booking_id` = {$model->id} AND `version` = {$model->version}";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            switch($f['guest_type'])
            {
                case GUEST_TYPE_ADULT:                     
                    $adultModel = new Villa_Module_Hotel_Model_Booking_Adult();
                    $adultModel->enabled = 1;
                    $adultModel->first_name = $f['first_name'];
                    $adultModel->last_name = $f['last_name'];
                    $adultModel->scan = $f['scan'];
                    $adultModel->address = $f['address'];
                    $adultModel->phone = $f['phone'];
                    $adultModel->email = $f['email'];                    
                    $model->adultsInfo[$f['guest_num']] = $adultModel;                    
                    break;
                
                case GUEST_TYPE_CHILD:
                    $childModel = new Villa_Module_Hotel_Model_Booking_Child();
                    $childModel->enabled = 1;
                    $childModel->first_name = $f['first_name'];
                    $childModel->last_name = $f['last_name'];
                    $model->childrenInfo[$f['guest_num']] = $childModel;
                    break;
            }
        } 
        
        $sql = "SELECT * FROM ".$this->_tableServices." WHERE `booking_id` = {$model->id} AND `version` = {$model->version}";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            $model->addService($f['service_id'], $f, false);
        }
        
        $sql = "SELECT `t`.*, `tl`.`type` 
                FROM {$this->_tableTransfer} AS `t`
                LEFT JOIN {$this->_tableTransferList} AS `tl` ON(`t`.`transfer_id` = `tl`.`id`)
                WHERE `t`.`booking_id` = {$model->id} AND `version` = {$model->version}";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            $f['date'] = Dante_Helper_Date::converToDateType($f['date'], 0);
            $f['time'] = Dante_Helper_Date::converToDateType($f['time'], 10);
            
            $model->addTransfer($f['type'], $f, false);
        }
        
        $sql = "SELECT * FROM ".$this->_tableExcursions." WHERE `booking_id` = {$model->id} AND `version` = {$model->version}";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            $model->addExcursion($f['date'], $f, false);
        }
        
        $sql = "SELECT * FROM ".$this->_tableExtraServices." WHERE `booking_id` = {$model->id} AND `version` = {$model->version}";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            $f['date'] = Dante_Helper_Date::converToDateType($f['date'], 0);
            $model->addExtraService($f, false);
        }
        
        $model->basicData->dateType = 0;
        $model->totalsData->recalc();
        
        return $model;
    }

    public function getBookingDiscounts($id)
    {
        $sql = "SELECT `dc_residence`, `dc_options` FROM {$this->_tableName} WHERE `id` = {$id}";
        return Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
    }

    public function changeStatus($bookingId, $status)
    {
        $tableStatus = new Dante_Lib_Orm_Table($this->_tableStatuses);
        $tableStatus->select(array('id' => $status));
        
        if($tableStatus->getNumRows() == 0)
            return false;
        
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->status = $status;
        
        $table->update(array('id' => $bookingId));
        
        return $tableStatus->name;
    }
    
    public function getScan($bookingId, $guestNum)
    {
        $guestType = GUEST_TYPE_ADULT;
        
        $sql = "SELECT `scan` FROM {$this->_tableGuests} 
                WHERE `booking_id` = {$bookingId} 
                AND `guest_num` = {$guestNum} AND `guest_type` = {$guestType}";
                
        return Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'scan');
    }

    public function getStatuses()
    {
        $sql = "SELECT * FROM ".$this->_tableStatuses;
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        
        $rowsArray = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $rowsArray[$f['id']] = $f['name'];
        }
        return $rowsArray;
    }
    
    public function getEngagedNumbers() 
    {
        $sql = "SELECT * FROM {$this->_tableName}";

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $bookingNumbers = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $bookingNumbers[$f['id']] = $f['room'];
        }
        
        return $bookingNumbers;
    }   
    
    public function saveBooking(Villa_Module_Hotel_Model_Manage_Booking $model)
    {
        $model->version = $this->createVersion($model->id);
        
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        
        $table->version = $model->version;
        $table->modified = time();
        $table->modified_by = Dante_Helper_App::getUid();
        
        $table->room = $model->roomData->room_id;
        $table->arrival = $model->basicData->arrivalToInt();
        $table->departure = $model->basicData->departureToInt();
        $table->days = $model->days;
        $table->adults = $model->basicData->adults;
        $table->children = $model->basicData->children;
        $table->cost = $model->totalsData->getTotalCost();
        $table->update(array('id' => $model->id));
        
        $table = new Dante_Lib_Orm_Table($this->_tableGuests);
        
        foreach($model->adultsInfo as $num => $item)
        {
            $table = new Dante_Lib_Orm_Table($this->_tableGuests);
            $table->booking_id = $model->id;
            $table->version = $model->version;
            $table->guest_num = $num;
            $table->guest_type = GUEST_TYPE_ADULT;
            $table->first_name = $item->first_name;
            $table->last_name = $item->last_name;
            $table->scan = $item->scan;
            $table->address = $item->address;
            $table->email = $item->email;
            $table->phone = $item->phone;
            
            $table->insert();
        }
        
        foreach($model->childrenInfo as $num => $item)
        {
            $table = new Dante_Lib_Orm_Table($this->_tableGuests);
            $table->booking_id = $model->id;
            $table->version = $model->version;
            $table->guest_num = $num;
            $table->guest_type = GUEST_TYPE_CHILD;
            $table->first_name = $item->first_name;
            $table->last_name = $item->last_name;
            
            $table->insert();
        }
        
        $table = new Dante_Lib_Orm_Table($this->_tableServices);
        
        foreach($model->getAddedServices() as $id => $service)
        {
            foreach($service->adults as $num => $item)
            {
                $table = new Dante_Lib_Orm_Table($this->_tableServices);
                $table->booking_id = $model->id;
                $table->version = $model->version;
                $table->guest_num = $num;
                $table->guest_type = GUEST_TYPE_ADULT;
                $table->service_id = $id;
                $table->count = $item->count;
                $table->cost = $item->cost;                

                $table->insert();
            }
            foreach($service->children as $num => $item)
            {
                $table = new Dante_Lib_Orm_Table($this->_tableServices);
                $table->booking_id = $model->id;
                $table->version = $model->version;
                $table->guest_num = $num;
                $table->guest_type = GUEST_TYPE_CHILD;
                $table->service_id = $id;
                $table->count = $item->count;
                $table->cost = $item->cost;                

                $table->insert();
            }
        }
        
        $table = new Dante_Lib_Orm_Table($this->_tableExcursions);
        
        foreach($model->getAddedExcursions() as $date => $excursion)
        {
            foreach($excursion->adults as $num => $item)
            {
                $table = new Dante_Lib_Orm_Table($this->_tableExcursions);
                $table->booking_id = $model->id;
                $table->version = $model->version;
                $table->date = $date;
                $table->guest_num = $num;
                $table->guest_type = GUEST_TYPE_ADULT;
                $table->cost = $item->cost;                

                $table->insert();
            }
            foreach($excursion->children as $num => $item)
            {
                $table = new Dante_Lib_Orm_Table($this->_tableExcursions);
                $table->booking_id = $model->id;
                $table->version = $model->version;
                $table->date = $date;
                $table->guest_num = $num;
                $table->guest_type = GUEST_TYPE_CHILD;
                $table->cost = $item->cost;              

                $table->insert();
            }
        }
        
        $table = new Dante_Lib_Orm_Table($this->_tableTransfer);
        
        foreach($model->getAddedTransfers() as $transfer)
        {
            $table = new Dante_Lib_Orm_Table($this->_tableTransfer);
            $table->booking_id = $model->id;
            $table->version = $model->version;
            $table->transfer_id = $transfer->id;
            $table->date = Dante_Helper_Date::strtotimef($transfer->date, 0);
            $table->time = Dante_Helper_Date::strtotimef($transfer->time, 10);
            $table->location = $transfer->location;
            $table->race = $transfer->race;
            $table->train = $transfer->train;
            $table->wagon = $transfer->wagon;
            $table->cost = $transfer->getCost();
            
            $table->insert();
        }
                
        $table = new Dante_Lib_Orm_Table($this->_tableExtraServices);
        
        foreach($model->getExtraServices() as $service)
        {
            $table = new Dante_Lib_Orm_Table($this->_tableExtraServices);
            $table->booking_id = $model->id;
            $table->version = $model->version;
            $table->date = Dante_Helper_Date::strtotimef($service->date, 0);
            $table->descr = $service->descr;
            $table->cost = $service->getCost();
            
            $table->insert();
        }
    }
    
    public function addBooking(Villa_Module_Hotel_Model_Manage_Booking $model)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->arrival = $model->basicData->arrivalToInt();
        $table->departure = $model->basicData->departureToInt();
        $table->adults = $model->basicData->adults;
        $table->children = $model->basicData->children;
        $table->days = $model->days;
        
        $table->room = $model->roomData->room_id;
        $table->room_type = $model->roomData->type_id;
        $table->room_price = $model->roomData->getPrice();
        $table->prepayment_type = $model->roomData->prepayment;
        
        $table->cost = $model->totalsData->getTotalCost();
        $table->paid_options = $model->totalsData->getOptionsPaid();
        $table->paid_residence = $model->totalsData->getResidencePaid();
        $table->comment = $model->comment;
                
        $table->status = 1;
        $table->currency_id = $model->totalsData->getCurrencyId();
        $table->uid = Dante_Helper_App::getUid();
        $table->creation_time = time();
        $table->modified = time();
        $table->modified_by = Dante_Helper_App::getUid();
        
        $bookingId = $table->insert();
        
        foreach($model->adultsInfo as $num => $item)
        {
            $table = new Dante_Lib_Orm_Table($this->_tableGuests);
            $table->booking_id = $bookingId;
            $table->guest_num = $num;
            $table->guest_type = GUEST_TYPE_ADULT;
            $table->first_name = $item->first_name;
            $table->last_name = $item->last_name;
            $table->scan = $item->scan;
            $table->email = $item->email;
            $table->phone = $item->phone;
            
            $table->insert();
        }
        
        foreach($model->childrenInfo as $num => $item)
        {
            $table = new Dante_Lib_Orm_Table($this->_tableGuests);
            $table->booking_id = $bookingId;
            $table->guest_num = $num;
            $table->guest_type = GUEST_TYPE_CHILD;
            $table->first_name = $item->first_name;
            $table->last_name = $item->last_name;
                        
            $table->insert();
        }
        
        return $bookingId;
    }
    
    public function createPayment(Villa_Module_Hotel_Model_Manage_Payment $model)
    {
        $table = new Dante_Lib_Orm_Table($this->_paymentTable);
        $table->booking_id = $model->booking_id;
        $table->user_id = $model->user_id;
        $table->payment_id = $model->payment_id;
        $table->type = $model->type;
        $table->subject = $model->subject;
        $table->date = $model->date;
        $table->amount = $model->amount;
        $table->pay_way = $model->pay_way;
        $table->status = 1; //temp
        $table->description = $model->description;
        
        $depostitId = $table->insert();
        
        if(!$depostitId) return false;
        
        $tableBooking = new Dante_Lib_Orm_Table($this->_tableName);
        $tableBooking->select(array('id' => $model->booking_id));
        
        $field = false;
        switch($model->subject) 
        {
            case PAYMENT_SUBJECT_RESIDENCE:
                $field = 'paid_residence';
                break;

            case PAYMENT_SUBJECT_SERVICE:
                $field = 'paid_options';
                break;
            
            case PAYMENT_SUBJECT_ALL:
                $this->_createFullPayment($model->booking_id, $model->amount);
                return $depostitId;
        }        
        if(!$field) return false;
        
        $paid = $tableBooking->$field;
        
        switch($model->type)
        {
            case PAYMENT_TYPE_INCOME:
                $paid = $paid + $model->amount;
                break;
            
            case PAYMENT_TYPE_REFUND:
                $paid = $paid - $model->amount;
                break;
        }
        
        $tableBooking->$field = $paid;
        
        $tableBooking->update(array(
            'id' => $model->booking_id
        ));
        
        return $depostitId;
    }
    
    protected function _createFullPayment($id, $amount)
    {
        $totals = new Villa_Module_Hotel_Model_Booking_Totals();
        
        $sql = "SELECT * FROM {$this->_tableName} WHERE `id` = {$id}";
        $f = Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
        
        $totals->init($f['prepayment_type'], $f['room_price'], $f['days']);
        $totals->setDiscountsByValues($f['dc_residence'], $f['dc_options']);
        $totals->addResidencePayment($f['paid_residence']);
        
        $residence = $totals->getResidencePayable();
        $options = $amount - $residence;
        
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->paid_residence = $residence + $f['paid_residence'];
        $table->paid_options = $options + $f['paid_options'];
        
        $table->update(array(
            'id' => $id
        ));
    }


    /**
     * выбираем историю транзакций по заданному бронированию
     * @param int $id
     * @return array 
     */
    public function getPaymentHistory($id) 
    {
        $sql = "SELECT * FROM ".$this->_paymentTable."
                WHERE `booking_id` = {$id} ORDER BY `date` DESC";
                
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $history = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) 
        {
            $model = new Villa_Module_Hotel_Model_Manage_Payment();
            $model->id = $f['id'];
            $model->booking_id = $f['booking_id'];
            $model->payment_id = $f['payment_id'];
            $model->user_id = $f['user_id'];
            $model->date = $f['date'];
            $model->type = $f['type'];
            $model->subject = $f['subject'];
            $model->amount = $f['amount'];
            $model->pay_way = $f['pay_way'];
            $model->status = $f['status'];
            $model->description = $f['description'];
            $history[$model->id] = $model;
        }
        
        return $history;
    }
    
    public function setStatusesByTime()
    {
        $today = Dante_Helper_Date::mktimeToday();
        $hour = 3600;
        
        $timeStart = $today - $hour;
        $timeEnd = $today + $hour;
        
        $statArrival = BOOKING_STATUS_ARRIVAL;
        $statDeparture = BOOKING_STATUS_DEPARTURE;
        
        $statCancel = BOOKING_STATUS_CANCEL;
        
        $sql = "UPDATE {$this->_tableName} SET `status` = {$statArrival} 
                WHERE `status` != {$statCancel} AND `arrival` >= {$timeStart} AND `arrival` <= {$timeEnd}";
               
        Dante_Lib_SQL_DB::get_instance()->exec($sql);
        
        $sql = "UPDATE {$this->_tableName} SET `status` = {$statDeparture} 
                WHERE `status` != {$statCancel} AND `departure` >= {$timeStart} AND `departure` <= {$timeEnd}";
          
        Dante_Lib_SQL_DB::get_instance()->exec($sql);
    }
    
    public function getVersions($bookingId)
    {
        $sql = "SELECT `booking_id`, `version`, `time`, `uid` AS `user` FROM {$this->_versionsTableName}
                    WHERE `booking_id` = {$bookingId} ORDER BY `version` DESC";
                    
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $versions = array();
        
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
            $versions[] = $f;
        
        return $versions;
    }
    
    public function createVersion($bookingId)
    {
        // Переносим текущие данные в таблицу версий
        $sql = "INSERT INTO {$this->_versionsTableName} (
                    `booking_id`, `version`, `time`, `uid`, `arrival`, `departure`, `days`, `adults`, `children`,
                    `prepayment_type`, `room`, `room_type`, `room_price`, `cost`, `comment`
                ) SELECT 
                    `id`, `version`, `modified`, `modified_by`, `arrival`, `departure`, `days`, `adults`, `children`,
                    `prepayment_type`, `room`, `room_type`, `room_price`, `cost`, `comment`
                FROM {$this->_tableName} WHERE `id` = {$bookingId}";
                
        Dante_Lib_SQL_DB::get_instance()->exec($sql);
                
        // Получаем текущий номер версии
        $tableMain = new Dante_Lib_Orm_Table($this->_tableName);
        $version = $tableMain->select(array('id' => $bookingId))->version;
        $newVersion = $version + 1;
                
        return $newVersion;
    }
    
    public function getVersion($bookingId, $version)
    {
        $tableVersion = new Dante_Lib_Orm_Table($this->_versionsTableName);
        $tableVersion->select(array('booking_id' => $bookingId, 'version' => $version));
        
        $model = new Villa_Module_Hotel_Model_Manage_Booking();
        
        $model->id = $bookingId;
        $model->version = $version;
        $model->modified = Dante_Helper_Date::converToDateType($tableVersion->time, 1);
        $model->modified_by = $tableVersion->uid;
        $model->basicData->arrival = Dante_Helper_Date::converToDateType($tableVersion->arrival, 0);
        $model->basicData->departure = Dante_Helper_Date::converToDateType($tableVersion->departure, 0);
        $model->basicData->adults = $tableVersion->adults;
        $model->basicData->children = $tableVersion->children;
        $model->days = $tableVersion->days;
        $model->roomData->room_id = $tableVersion->room;
        $model->roomData->type_id = $tableVersion->room_type;
        $model->roomData->prepayment = $tableVersion->prepayment_type;
        $model->comment = $tableVersion->comment;
        
        $sql = "SELECT `b`.*, `r`.`max_adults`, `r`.`max_children` 
                FROM {$this->_tableName} AS `b` 
                LEFT JOIN {$this->_tableRoomTypes} AS `r` ON(`b`.`room_type` = `r`.`id`)
                WHERE `b`.`id` = ".$bookingId;
        $f = Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
                
        $model->roomData->max_adults = $f['max_adults'];
        $model->roomData->max_children = $f['max_children'];
        $model->roomData->setPrice($f['room_price']);
        $model->status = $f['status'];
        $model->user_currency = $f['currency_id'];
        $model->uid = $f['uid'];
        $model->creation_time = $f['creation_time'];
          
        $model->init();
        
        $model->totalsData->setDiscountsByValues($f['dc_residence'], $f['dc_options']);
        
        $model->totalsData->addOptionsPayment($f['paid_options']);
        $model->totalsData->addResidencePayment($f['paid_residence']);
        
        return $this->_getAdditionalModelData($model);
    }
    
    public function restoreVersion($bookingId, $version)
    {
        $model = $this->getVersion($bookingId, $version);
        $this->saveBooking($model);
    }
    
    public function deleteVersion($bookingId, $version)
    {
        $tableNames = array(
            $this->_versionsTableName,
            $this->_tableGuests,
            $this->_tableServices,
            $this->_tableExcursions,
            $this->_tableTransfer,
            $this->_tableExtraServices,
        );
        
        foreach($tableNames as $tableName)
        {
            $table = new Dante_Lib_Orm_Table($tableName);
            $table->delete(array(
                'booking_id' => $bookingId,
                'version' => $version
            ));
        }
    }
    
    public function getNewBookingsCount()
    {
        $sql = "SELECT COUNT(*) AS `count` FROM {$this->_tableName} WHERE `status` = ".BOOKING_STATUS_NEW;
        return Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'count');
    }

    // ЛЕС ГУСТОЙ - НАЧАЛО

    /**
     * выбираем возможные опции бронирования
     * @return array 
     * @deprecated
     */
    public function getBookingOptionsSamplesLang() {
        $langId = Module_Lang_Helper::getCurrent();
        $sql = "SELECT ".$this->_optionsSamplesLangTableName.". * 
                FROM ".$this->_optionsSamplesLangTableName."
                WHERE ".$this->_optionsSamplesLangTableName.".lang_id = ".$langId."
                ORDER BY id";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $parents = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $parents[$f['id']] = $f;
        }

        return $parents;
    }

    /**
     * выбираем возможные опции трансфера
     * @return array
     * @deprecated
     */
    public function getTransferOptionsSamplesLang()
    {
        $langId = Module_Lang_Helper::getCurrent();

        $sql =    "SELECT
                  ".$this->_transferSamplesLangTableName.". * ,
                  ".$this->_transferSamplesTableName.".type,
                  ".$this->_transferSamplesTableName.".transport_type
                  FROM
                  ".$this->_transferSamplesLangTableName.",
                  ".$this->_transferSamplesTableName."
                  WHERE
                  ".$this->_transferSamplesLangTableName.".lang_id =".$langId."
                  AND ".$this->_transferSamplesLangTableName.".transfer_id =
                  ".$this->_transferSamplesTableName.".id
                  ORDER BY transfer_id";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $parents = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $parents[$f['transfer_id']] = $f;
        }

        return $parents;
    }
    
    /**
     * Получение списка броней заданного пользователя
     * @param int $uid 
     * @return array
     */
    public function getHistory($uid) 
    {
        $sql = "SELECT `b`.*, `bg`.`first_name`, `bg`.`last_name`, `bg`.`email`, `bg`.`phone` 
            FROM {$this->_tableName} AS `b` 
            LEFT JOIN {$this->_tableGuests} AS `bg` 
            ON(`b`.`id` = `bg`.`booking_id` AND `b`.`version` = `bg`.`version` AND `bg`.`guest_num` = 1 AND `bg`.`guest_type` = ".GUEST_TYPE_ADULT.") 
            WHERE `b`.`uid` = {$uid} ORDER BY `b`.`creation_time` DESC";      
            
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        // Достаем список
        $list = array();
        $idList = array();
        $versionList = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) 
        {
            $model = new Villa_Module_Hotel_Model_Manage_Booking();
            $model->id = $f['id'];
            $model->version = $f['version'];
            $model->basicData->arrival = Dante_Helper_Date::converToDateType($f['arrival'], 0);
            $model->basicData->departure = Dante_Helper_Date::converToDateType($f['departure'], 0);
            $model->basicData->adults = $f['adults'];
            $model->basicData->children = $f['children'];
            $model->days = $f['days'];
            $model->roomData->room_id = $f['room'];
            $model->roomData->type_id = $f['room_type'];
            $model->roomData->prepayment = $f['prepayment_type'];
            $model->roomData->setPrice($f['room_price']);
            $model->firstName = $f['first_name'];
            $model->secondName = $f['last_name'];
            $model->email = $f['email'];
            $model->phone = $f['phone'];
            $model->status = $f['status'];
            $model->uid = $uid;
            $model->creation_time = Dante_Helper_Date::convertToDateTime($f['creation_time']);
            $model->modified = Dante_Helper_Date::convertToDateTime($f['modified']);
            $model->comment = $f['comment'];
            
            $model->init();
            
            $model->totalsData->setDiscountsByValues($f['dc_residence'], $f['dc_options']);
            $model->totalsData->addOptionsPayment($f['paid_options']);
            $model->totalsData->addResidencePayment($f['paid_residence']);
        
            $model->totalsData->recalc();
            
            $list[$model->id] = $model;
            $idList[] = $model->id;
            $versionList[] = $model->version;
        }
        
        if(count($idList) == 0) return $list;
        $idStr = implode(', ', $idList);
        $versionStr = implode(', ', $versionList);
        
        //Добавляем стоимость услуг
        $sql = "SELECT `booking_id`, SUM(`cost` * `count`) AS `cost` FROM {$this->_tableServices} 
                WHERE `booking_id` IN({$idStr}) AND `version` IN({$versionStr}) GROUP BY `booking_id`";
        
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            $list[$f['booking_id']]->totalsData->addOptionCost($f['cost']);
        }
        
        //Добавляем стоимость трансферов
        $sql = "SELECT `booking_id`, SUM(`cost`) AS `cost` FROM {$this->_tableTransfer} 
                WHERE `booking_id` IN({$idStr}) AND `version` IN({$versionStr}) GROUP BY `booking_id`";
        
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            $list[$f['booking_id']]->totalsData->addOptionCost($f['cost']);
        }
        
        //Добавляем стоимость экскурсий
        $sql = "SELECT `booking_id`, SUM(`cost`) AS `cost` FROM {$this->_tableExcursions} 
                WHERE `booking_id` IN({$idStr}) AND `version` IN({$versionStr})  GROUP BY `booking_id`";
        
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            $list[$f['booking_id']]->totalsData->addOptionCost($f['cost']);
        }
        
        //Добавляем стоимость доп. услуг
        $sql = "SELECT `booking_id`, SUM(`cost`) AS `cost` FROM {$this->_tableExtraServices} 
                WHERE `booking_id` IN({$idStr}) AND `version` IN({$versionStr}) GROUP BY `booking_id`";
        
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            $list[$f['booking_id']]->totalsData->addOptionCost($f['cost']);
        }
        
        return $list;
    }
    
    
}

?>
