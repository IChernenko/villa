<?php
/**
 * Description of roomtypes
 *
 * @author Mort
 */
class Villa_Module_Hotel_Mapper_Manage_Roomtypes extends Module_Handbooksgenerator_Mapper_Base
{    
    protected $_tableName = '[%%]hotel_room_types';
    protected $_tableNameLang = '[%%]hotel_room_types_lang';
    protected $_imagesTableName = '[%%]hotel_room_types_images';
    protected $_equipmentTableName = '[%%]hotel_equipment';
    protected $_roomEquipmentTableName = '[%%]hotel_room_types_equipment';
    protected $_roomsTableName = '[%%]hotel_rooms';
    
    /**
     * выбираем тип номеров для редактирования
     * @param int $id
     * @param int $lang
     */
    public function getOne($id, $lang)
    {
        $model = new Villa_Module_Hotel_Model_Manage_Roomtypes();
        if(!$id) return $model;
        
        $tableLang = new Dante_Lib_Orm_Table($this->_tableNameLang);
        $tableLang->select(array(
            'type_id' => $id,
            'lang_id' => $lang
        ));
        
        $tableMain = new Dante_Lib_Orm_Table($this->_tableName);
        $tableMain->select(array(
            'id' => $id,
        ));
        
        $model->id = $id;
        $model->max_adults = $tableMain->max_adults;
        $model->max_children = $tableMain->max_children;
        $model->link = $tableMain->link;
        $model->lang_id = $lang;
        $model->title = $tableLang->title;
        $model->description = $tableLang->description;
        
        return $model;
    }
        
    /**
     * обновляем тип номеров
     * @param Villa_Module_Hotel_Model_Manage_Roomtypes $model
     */
    public function apply(Villa_Module_Hotel_Model_Manage_Roomtypes $model) 
    {
        $insert = false;
        
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->max_adults = $model->max_adults;
        $table->max_children = $model->max_children;
        if($model->lang_id == 1) $table->title = $model->title;
        $table->link = $model->link;
        
        if($model->id) 
            $table->update(array('id' => $model->id));
        else 
        {
            $model->id = $table->insert();
            $insert = true;
        }
        
        $table = new Dante_Lib_Orm_Table($this->_tableNameLang);
        $table->type_id = $model->id;
        $table->lang_id = $model->lang_id;
        $table->title = $model->title;
        $table->description = $model->description;
        
        $table->apply(array(
            'type_id' => $model->id,
            'lang_id' => $model->lang_id
        ));
        
        return $insert;
    }
        
    /**
     * выборка изображений для определенного типа номеров
     * @param int $typeId
     * @return array 
     */
    public function getImages($typeId) 
    {
        $sql = "SELECT * FROM ".$this->_imagesTableName.
                " WHERE type_id=".$typeId." ORDER BY draw_order ASC";

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $images = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $images[] = $f;
        }
        
        return $images;
    }
    
    /**
     * добавление изображения
     * @param int $typeId
     * @param string $fileName 
     */
    public function addImage($typeId, $fileName) 
    {
        $table = new Dante_Lib_Orm_Table($this->_imagesTableName);
        $table->type_id = $typeId;
        $table->image = $fileName;
        $table->draw_order = $this->getLastOrderByType($typeId)+1;

        $table->insert();
    }
    
    public function getLastOrderByType($typeId)
    {
        $sql = "SELECT `draw_order` FROM {$this->_imagesTableName} 
                WHERE `type_id` = {$typeId} ORDER BY `draw_order` DESC LIMIT 1";
                
        return Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'draw_order');
    }
    
    /**
     * удаление изображения
     * @param int $typeId
     * @param string $image 
     */
    public function delImage($typeId, $image)
    {
        $table = new Dante_Lib_Orm_Table($this->_imagesTableName);
        $table->delete(array(
            'type_id' => $typeId,
            'image' => $image
        ));
    }
    
    
    /**
     * меняем порядок отображения картинки
     * @param int $id
     * @param int $orderFactor 
     */
    public function changeOrder($typeId, $drawOrder, $orderFactor)
    {
        $switchRow = $this->getImageRow($typeId, $drawOrder+$orderFactor);
        
        if(!$switchRow || !$switchRow->draw_order ){
            return;
        }
        
        $table = new Dante_Lib_Orm_Table($this->_imagesTableName);
        $table->draw_order = $drawOrder+$orderFactor;

        $table->apply(array(
            'type_id'=>$typeId,
            'draw_order'=>$drawOrder
        ));

        $table = new Dante_Lib_Orm_Table($this->_imagesTableName);
        $table->draw_order = $drawOrder;

        $table->apply(array(
            'type_id'=>$typeId,
            'image'=>$switchRow->image,
            'draw_order'=>$switchRow->draw_order,
        ));
    }
    
    /**
     * выбираем изображение с заданным порядком отображения
     * @param int $typeId
     * @param int $drawOrder
     * @return Dante_Lib_Orm_Table 
     */
    public function getImageRow($typeId, $drawOrder)
    {
        $table = new Dante_Lib_Orm_Table($this->_imagesTableName);
        
        $table->get()->where(array('type_id'=>$typeId, 'draw_order'=>$drawOrder))
            ->limit(array(0, 1))->orderby('draw_order desc');
        $table->fetch();

        return $table;
    }
    
    /**
     * делаем выборку по оборудованию
     * @param int $typeId
     * @return array 
     */
    public function getEquipment($typeId)
    {        
        $sql = "SELECT `ap`.*, `eq`.`sys_name` FROM {$this->_roomEquipmentTableName} AS `ap`
                    LEFT JOIN {$this->_equipmentTableName} AS `eq` ON `ap`.`eq_id` = `eq`.`id` 
                    WHERE `ap`.`type_id` = {$typeId} ORDER BY `draw_order` ASC";

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $roomEquipment = array();
        $aptEqIds = array();
        
        //keys: eq_id, type_id, sort_order, bold, font_size
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $roomEquipment[$f['eq_id']] = $f;
            $aptEqIds[] = $f['eq_id'];
        }
        
        
        $sql = "SELECT * FROM ".$this->_equipmentTableName;
        if(count($aptEqIds)) 
        {
            $idStr = implode(', ', $aptEqIds);
            $sql .= " WHERE `id` NOT IN ({$idStr})";
        }

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $equipment = array();
        
        // keys: id, sys_name
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $equipment[$f['id']] = $f;
        }
        
        return array($equipment, $roomEquipment);
    }
    
    /**
     * сохраняем оборудование
     * @param int $typeId
     * @param array $equipmentArr 
     */
    public function saveEquipment($typeId, $equipmentArr, $boldArr, $font_sizeArr)
    {
        $sql = "DELETE FROM ".$this->_roomEquipmentTableName.
                " WHERE type_id=".$typeId;

        $r = Dante_Lib_SQL_DB::get_instance()->exec($sql);
        $i = 1;
        foreach ($equipmentArr as $key=>$equipment) 
        {
            $table = new Dante_Lib_Orm_Table($this->_roomEquipmentTableName);
            $table->type_id = $typeId;
            $table->eq_id = $equipment;
            $table->draw_order = $i;
            $table->bold = $boldArr[$key];
            $table->font_size = (int)$font_sizeArr[$key];

            $table->insert();
            $i++;
        }
    }
    
    public function getNamesList()
    {
        $sql = "SELECT * FROM ".$this->_tableName;

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $types = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $types[$f['id']] = $f['title'];
        }
        
        return $types;
    }
}

?>