<?php
/**
 * Description of equipment
 *
 * @author Admin
 */
class Villa_Module_Hotel_Mapper_Manage_Equipment extends Module_Handbooksgenerator_Mapper_Base
{    
    protected $_tableName = '[%%]hotel_equipment';    
    protected $_tableNameLang = '[%%]hotel_equipment_lang';
    
    public function getById($id, $lang)
    {
        $model = new Villa_Module_Hotel_Model_Manage_Equipment();
        $model->lang_id = $lang;
        if(!$id) return $model;
        
        $sql = "SELECT * FROM {$this->_tableName} AS `e` LEFT JOIN {$this->_tableNameLang} AS `el`
            ON(`e`.`id` = `el`.`id` AND `el`.`lang_id` = {$lang}) WHERE `e`.`id` = {$id} LIMIT 1";
        
        $f = Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
        $model->id = $id;
        $model->sys_name = $f['sys_name'];
        $model->name = $f['name'];
        
        return $model;
    }
    
    /**
     * @param Villa_Module_Hotel_Model_Manage_Equipment $model
     */
    public function apply(Villa_Module_Hotel_Model_Manage_Equipment $model) 
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->id = $model->id;
        $table->sys_name = $model->sys_name;
        
        if($model->id) $table->update(array('id' => $model->id));
        else $model->id = $table->insert();
        
        $tableLang = new Dante_Lib_Orm_Table($this->_tableNameLang);
        $tableLang->id = $model->id;
        $tableLang->lang_id = $model->lang_id;
        $tableLang->name = $model->name;

        $tableLang->apply(array(
            'id' => $model->id,
            'lang_id' => $model->lang_id
        ));
    }
    
}

?>
