<?php
/**
 * Description of bookingtransfer
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Mapper_Manage_Bookingtransfer extends Module_Handbooksgenerator_Mapper_Base
{
    protected $_tableName = '[%%]hotel_booking_transfer';
    protected $_tableTransfer = '[%%]hotel_transfer';
    protected $_tableBooking = '[%%]hotel_booking';


    public function getList($params)
    {
        $cols = array(
            "`t1`.*",
            "`t3`.*"
        );
        
        $join = array(
            "INNER JOIN {$this->_tableBooking} AS `t2` ON (`t1`.`booking_id` = `t2`.`id` AND `t1`.`version` = `t2`.`version`)",
            "LEFT JOIN {$this->_tableTransfer} AS `t3` ON `t1`.`transfer_id` = `t3`.`id`"
        );
            
        return $this->getRowsByParams($params, $cols, $join);
    }
    
    protected function _getReminderSql($selectValue)
    {
        $now = time() - 3600 * 3;
        $timeRemind = time();
        return "SELECT {$selectValue} FROM {$this->_tableName} WHERE `date` + `time` <= {$timeRemind} AND `date` + `time` > {$now}";
    }


    public function getTransfersToRemind()
    {        
        $selectValue = "COUNT(*) AS `count`";
        
        $sql = $this->_getReminderSql($selectValue);
        $count = Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'count');
        
        if(!$count) return $count;
                
        $selectValue = "`booking_id`, `transfer_id`";
        $sql = $this->_getReminderSql($selectValue);
        
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            $this->changeStatus($f['booking_id'], $f['transfer_id']);         
        }

        return $count;
    }
    
    public function get($bookingId, $transId)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->select(array(
            'booking_id' => $bookingId,
            'transfer_id' => $transId
        ));
        
        $model = new Villa_Module_Hotel_Model_Manage_Bookingtransfer();
        $model->booking_id = $bookingId;
        $model->transfer_id = $transId;
        $model->status = $table->status;
        $model->setDate($table->date);
        $model->setTime($table->time);
        
        return $model;
    }
    
    public function update(Villa_Module_Hotel_Model_Manage_Bookingtransfer $model)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        
        $table->date = $model->getDate();
        $table->time = $model->getTime();
        
        $table->update(array(
            'booking_id' => $model->booking_id,
            'transfer_id' => $model->transfer_id
        ));
        
        $this->changeStatus($model->booking_id, $model->transfer_id, $model->status);
    }
    
    public function changeStatus($bookingId, $transId, $status = TRANSFER_STATUS_ORDER)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->status = $status;
        $table->update(array(
            'booking_id' => $bookingId,
            'transfer_id' => $transId
        ));
    }
}

?>
