<?php
/**
 * Description of prepayment
 *
 * @author Valkyria
 */

class Villa_Module_Hotel_Mapper_Manage_Prepayment extends Module_Handbooksgenerator_Mapper_Base
{   
    protected $_tableName = '[%%]hotel_prepayment_types';
    
    protected $_tableNameLang = '[%%]hotel_prepayment_types_lang';
    
    public function getById($id, $lang = 1)
    {
        $model = new Villa_Module_Hotel_Model_Manage_Prepayment();
        $model->lang_id = $lang;
        if(!$id) return $model;
        
        $sql = "SELECT * FROM {$this->_tableName} AS `p` LEFT JOIN {$this->_tableNameLang} AS `pl`
            ON(`p`.`id` = `pl`.`type_id` AND `pl`.`lang_id` = {$lang}) WHERE `p`.`id` = {$id} LIMIT 1";
        
        $f = Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
        $model->id = $f['id'];
        $model->sys_name = $f['title'];
        $model->name = $f['name'];
        
        return $model;
    }
    /**
     * @param Villa_Module_Hotel_Model_Manage_Prepayment $model 
     */
    public function apply(Villa_Module_Hotel_Model_Manage_Prepayment $model) 
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->id = $model->id;
        $table->title = $model->sys_name;
        
        if($model->id) $table->update(array('id' => $model->id));
        else $model->id = $table->insert();
                
        $tableLang = new Dante_Lib_Orm_Table($this->_tableNameLang);
        $tableLang->type_id = $model->id;
        $tableLang->lang_id = $model->lang_id;
        $tableLang->name = $model->name;
        
        $tableLang->apply(array(
            'type_id' => $model->id,
            'lang_id' => $model->lang_id
        ));
    }
    
    
    public function getNamesList() 
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $list = $table->fetchAll('id');
        foreach($list as &$val)
        {
            $val = $val['title'];
        }
        return $list;
    }
}

?>
