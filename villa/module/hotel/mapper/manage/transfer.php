<?php
/**
 * Description of transfer
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Mapper_Manage_Transfer extends Module_Handbooksgenerator_Mapper_Base
{
    protected $_tableName = '[%%]hotel_transfer';
    protected $_tableLang = '[%%]hotel_transfer_lang';
    protected $_tableImages = '[%%]hotel_booking_transfer_images';
    
    public function getRow($id, $lang)
    {        
        $model = new Villa_Module_Hotel_Model_Manage_Transfer();
        $model->lang_id = (int)$lang;
        
        $sql = "SELECT * FROM {$this->_tableName} AS `t`
                LEFT JOIN {$this->_tableLang} AS `tl` ON(`tl`.`transfer_id` = `t`.`id` AND `tl`.`lang_id` = {$lang})
                WHERE `t`.`id` = {$id}";
        if(!$r = Dante_Lib_SQL_DB::get_instance()->open($sql))
            return $model;
        
        $f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r);
        $model->id = $f['id'];
        $model->sys_name = $f['sys_name'];
        $model->transport_type = $f['transport_type'];
        $model->description = $f['description'];
        $model->type = $f['type'];
        $model->name = $f['name'];
        $model->type_name = $f['type_name'];
        $model->outer_id = $f['outer_id'];
        
        return $model;
    }
    
    public function edit(Villa_Module_Hotel_Model_Manage_Transfer $model)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->sys_name = $model->sys_name;
        $table->transport_type = $model->transport_type;
        $table->type = $model->type;
        $table->outer_id = $model->outer_id;
        
        if($model->id) $table->update(array('id'=>$model->id));
        else $model->id = $table->insert();
        
        $tableLang = new Dante_Lib_Orm_Table($this->_tableLang);
        $tableLang->transfer_id = $model->id;
        $tableLang->lang_id = $model->lang_id;
        $tableLang->name = $model->name;
        $tableLang->type_name = $model->type_name;
        $tableLang->description = $model->description;
        
        $cond = array('transfer_id'=>$model->id, 'lang_id'=>$model->lang_id);
        if($tableLang->exist($cond)) $tableLang->update($cond);
        else $tableLang->insert();
    }
    
    public function getList()
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $rows = $table->fetchAll('id');
        $types = Villa_Module_Hotel_Helper_Manage_Booking::getTransferTypes();
        foreach($rows as &$row)
        {
            $row['cost'] = 0;
            $row['type_name'] = $types[$row['type']];
        }
        return $rows;
    }

    /**
     * выборка изображений для определенного типа трансфера
     * @param int $transferId - ID трансфера
     * @return array
     */
    public function getImages($transferId)
    {
        $sql = "SELECT * FROM ".$this->_tableImages.
            " WHERE transfer_id=".$transferId." ORDER BY draw_order ASC";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $images = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $images[] = $f;
        }

        return $images;
    }

    /**
     * добавление изображения
     * @param int $typeId
     * @param string $fileName
     */
    public function addImage($transferId, $fileName)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableImages);
        $table->transfer_id = $transferId;
        $table->image = $fileName;
        $table->draw_order = $this->getLastOrderByType($transferId)+1;

        $table->insert();
    }

    public function getLastOrderByType($transferId)
    {
        $sql = "SELECT `draw_order` FROM {$this->_tableImages}
                WHERE `transfer_id` = {$transferId} ORDER BY `draw_order` DESC LIMIT 1";

        return Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'draw_order');
    }

    /**
     * удаление изображения
     * @param int $typeId
     * @param string $image
     */
    public function delImage($transferId, $image)
    {
        $table = new Dante_Lib_Orm_Table('tst_hotel_booking_transfer_images');

        $table->delete(array(
            'transfer_id' => $transferId,
            'image' => $image
        ));

    }

    /**
     * Изменение порядка отображения картинки
     * @param int $id
     * @param int $orderFactor
     */

    public function changeOrder($transferId, $drawOrder, $orderFactor)
    {
        $switchRow = $this->getImageRow($transferId, $drawOrder+$orderFactor);
        if(!$switchRow || !$switchRow->draw_order ){
            return;
        }

        $table = new Dante_Lib_Orm_Table($this->_tableImages);
        $table->draw_order = $drawOrder+$orderFactor;

        $table->apply(array(
            'transfer_id'=>$transferId,
            'draw_order'=>$drawOrder
        ));

        $table = new Dante_Lib_Orm_Table($this->_tableImages);
        $table->draw_order = $drawOrder;

        $table->apply(array(
            'transfer_id'=>$transferId,
            'image'=>$switchRow->image,
            'draw_order'=>$switchRow->draw_order,
        ));
    }

    /**
     * выбираем изображение с заданным порядком отображения
     * @param int $typeId
     * @param int $drawOrder
     * @return Dante_Lib_Orm_Table
     */
    public function getImageRow($transferId, $drawOrder)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableImages);

        $table->get()->where(array('transfer_id'=>$transferId, 'draw_order'=>$drawOrder))
            ->limit(array(0, 1))->orderby('draw_order desc');
        $table->fetch();

        return $table;
    }
    
    public function getNamesList()
    {
        $sql = "SELECT * FROM {$this->_tableName}";
        if(!$r = Dante_Lib_SQL_DB::get_instance()->open($sql)) return false;
        
        $types = Villa_Module_Hotel_Helper_Manage_Booking::getTransferTypes();   
        $list = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) 
        {
            $list[$f['id']] = $f['sys_name'].' - '.$types[$f['type']];
        }
        return $list;
    }
}

?>
