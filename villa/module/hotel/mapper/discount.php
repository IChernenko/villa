<?php
/**
 * Description of discount
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Mapper_Discount 
{    
    protected $_tableName = '[%%]user_profile';
    
    public function getDiscount($uid)
    {
        $sql = "SELECT `discount_residence`, `discount_options` FROM {$this->_tableName} 
                WHERE `user_id` = {$uid} LIMIT 1 OFFSET 0";
                
        $discount = Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);        
        
        return $discount;
    }
}

?>
