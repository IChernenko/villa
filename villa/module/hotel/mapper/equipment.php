<?php
/**
 * Маппер оборудования типов номеров
 * User: dorian
 * Date: 29.05.12
 * Time: 14:28
 * To change this template use File | Settings | File Templates.
 */
class Villa_Module_Hotel_Mapper_Equipment
{
    /**
     * Загрузка оборудования для заданного типа номера
     * @param int $appartmentId
     * @param int $langId
     * @return array
     */
    public function get($appartmentId, $langId) {
        $sql = "select
                    ae.eq_id as id,
                    e.name,
                    ae.bold,
                    ae.font_size
                from [%%]hotel_room_types_equipment as ae
                left join [%%]hotel_equipment_lang as e on (e.id = ae.eq_id and lang_id = {$langId})
                where ae.type_id = {$appartmentId} 
                order by draw_order";

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $list = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $list[(int)$f['id']] = array(
                'name' => $f['name'],
                'bold' => $f['bold'],
                'font_size' => $f['font_size'],
            );
        }

        return $list;
    }
}
