<?php
/**
 * Description of bookingoptions:
 * Клиентский маппер опций бронирования (услуги, трансферы)
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Mapper_Bookingoptions 
{    
    protected $_servicesTable = '[%%]hotel_services';
    protected $_servicesLangTable = '[%%]hotel_services_lang';
    protected $_servicesImagesTable = '[%%]hotel_services_images';
    
    protected $_transferTable = '[%%]hotel_transfer';
    protected $_transferLangTable = '[%%]hotel_transfer_lang';
    
    protected $_excursTable = '[%%]villa_homepage_excursions';

    protected $_bookingTable = '[%%]hotel_booking';
    protected $_bookingExcursTable = '[%%]hotel_booking_excursions';


    /**
     * выбираем возможные опции бронирования
     * @return array 
     */
    public function getServicesFromBooking()
    {
        $langId = Module_Lang_Helper::getCurrent();

        $sql = "SELECT * FROM {$this->_servicesTable} AS `o` LEFT JOIN {$this->_servicesLangTable} AS `ol`
                ON(`o`.`id` = `ol`.`service_id` AND `ol`.`lang_id` = {$langId})
                ORDER BY `o`.`id`";

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $services = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $services[$f['id']] = $f;
            $services[$f['id']]['price'] = 0; //default
        }

        return $services;
    }


    public function getServices()
    {
        $langId = Module_Lang_Helper::getCurrent();
        
        $sql = "SELECT * FROM {$this->_servicesTable} AS `o` LEFT JOIN {$this->_servicesLangTable} AS `ol`
                ON(`o`.`id` = `ol`.`service_id` AND `ol`.`lang_id` = {$langId})
                ORDER BY `o`.`id`";
                
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $services = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $services[$f['id']] = $f;
            $services[$f['id']]['price'] = 0; //default
        }

        return $services;
    }
    
    public function getServicesImages()
    {
        $sql = "SELECT * FROM {$this->_servicesImagesTable} ORDER BY `draw_order` ASC";
        
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $images = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) 
        {
            if(!isset($images[$f['service_id']])) 
                $images[$f['service_id']] = array();
            
            $images[$f['service_id']][] = Villa_Module_Hotel_Helper_Media::getServicesDir().$f['image'];
        }

        return $images;
    }
    
    public function getTransfers()
    {
        $langId = Module_Lang_Helper::getCurrent();
        
        $sql = "SELECT * FROM {$this->_transferTable} AS `t` LEFT JOIN {$this->_transferLangTable} AS `tl`
                ON(`t`.`id` = `tl`.`transfer_id` AND `tl`.`lang_id` = {$langId})
                ORDER BY `t`.`id`";
                
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $services = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $services[$f['id']] = $f;
            $services[$f['id']]['price'] = 0; //default
        }

        return $services;
    }
    
    public function getService($id)
    {
        $lang = Module_Lang_Helper::getCurrent();
        
        $sql = "SELECT `s`.`id`, `sl`.* FROM {$this->_servicesTable} AS `s` LEFT JOIN {$this->_servicesLangTable} AS `sl` 
                ON(`sl`.`service_id` = `s`.`id` AND `sl`.`lang_id` = {$lang}) WHERE `s`.`id` = {$id}";
                
        $service = Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
        $service['images'] = array();
        
        $sql = "SELECT `image` FROM {$this->_servicesImagesTable} WHERE `service_id` = {$id}
                ORDER BY `draw_order` ASC";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
            $service['images'][] = Villa_Module_Hotel_Helper_Media::getServicesDir().$f['image'];
        
        return $service;
    }
    
    public function getBookedExcursions($dates, $bookingId)
    {
        $where = implode(', ', $dates);
        
        $sql = "SELECT `date`, COUNT(*) AS `count` FROM {$this->_bookingExcursTable} AS t1
            LEFT JOIN {$this->_bookingTable} AS t2 ON (t1.booking_id = t2.id AND t1.version = t2.version) 
            WHERE `t1`.`date` IN ({$where}) AND t1.booking_id = t2.id != {$bookingId} GROUP BY `t1`.`date`";
        
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        
        $list = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            $list[$f['date']] = $f['count'];
        }        
        return $list;
    }
    
    public function getMaxGuests()
    {
        $sql = "SELECT DISTINCT(`day`), `max_guests` FROM {$this->_excursTable} WHERE `max_guests` IS NOT NULL";
        
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        
        $list = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            $list[$f['day']] = $f['max_guests'];
        }
        return $list;
    }
}

?>
