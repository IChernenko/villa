<?php



/**
 * Маппер статусов бронирования
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Villa_Module_Hotel_Mapper_Bookingstatus {
    
    public function getList() {
        $table = new Dante_Lib_Orm_Table('[%%]hotel_booking_statuses'); 
        return $table->fetchAll('id');
    }
}

?>
