<?php
/**
 * Маппер для резервирования отеля
 * User: dorian
 * Date: 18.05.12
 * Time: 15:12
 * To change this template use File | Settings | File Templates.
 */
class Villa_Module_Hotel_Mapper_Roomsearch
{
    protected $_tableName = '[%%]hotel_rooms';
    protected $_tableTypes = '[%%]hotel_room_types';
    protected $_tableBooking = '[%%]hotel_booking';

    /**
     * Поиск для резервирования согласно заданным параметрам
     * @param Villa_Module_Hotel_Model_Booking_Basic $params
     * @return string
     */
    public function search(Villa_Module_Hotel_Model_Booking_Basic $params) 
    {
        $lang = Module_Lang_Helper::getCurrent();
        
        $departure      = $params->departureToInt();
        $arrive         = $params->arrivalToInt();

        $sql = "SELECT `a`.`id`, `a`.`type`
                FROM {$this->_tableName} AS `a`
                LEFT JOIN {$this->_tableTypes} AS `at` ON(`at`.`id` = `a`.`type`)
                WHERE `at`.`max_adults` >= {$params->adults}
                    AND `at`.`max_children` >= {$params->children}
                    AND `a`.`id` NOT IN (
                        SELECT `b`.`room` FROM {$this->_tableBooking} AS `b`
                        WHERE {$arrive} < `b`.`departure` 
                        AND {$departure} > `b`.`arrival` 
                        AND `b`.`room` > 0)";

        Dante_Lib_Log_Factory::getLogger()->debug($sql);                

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $types = array();
        $appartments = array();

        $totalNumbersCount = 0;
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) 
        {
            $type = (int)$f['type'];
            
            $appartments[$type]['numbers'][] = $f['id'];
            
            if (!isset($appartments[$type]['count'])) $appartments[$type]['count'] = 0;
            
            $types[$type] = (int)$f['type'];

            $totalNumbersCount++;
        }
        
        Dante_Lib_Log_Factory::getLogger()->debug("search: totalNumbersCount $totalNumbersCount");
        if (!$totalNumbersCount || count($types) == 0)
            throw new Exception("Нет свободных номеров для заданных Вами параметров");

        Dante_Lib_Log_Factory::getLogger()->debug_item($types);

        // @todo: загузить инфу по типам
        $appartmentTypesMapper = new Villa_Module_Hotel_Mapper_Roomtypes();
        $list = $appartmentTypesMapper->getList(array(
            'types' => $types,
            'lang' => $lang
        ));
        
        foreach($list as $appartmentType => $item) 
        {
            if (isset($appartments[$appartmentType])) 
            {
                $list[$appartmentType]->numbers = $appartments[$appartmentType]['numbers'];
                $list[$appartmentType]->comboCount = $appartments[$appartmentType]['count'];
                $list[$appartmentType]->type = $appartmentType;
            }            
        }
        
        Dante_Lib_Log_Factory::getLogger()->debug_item($list);

        return $list;
    }
}