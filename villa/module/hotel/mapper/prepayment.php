<?php
/**
 * Маппер типов предоплаты
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Villa_Module_Hotel_Mapper_Prepayment 
{
    protected $_tableName = '[%%]hotel_prepayment_types';
    protected $_tableNameLang = '[%%]hotel_prepayment_types_lang';

    public function getList() {
        $lang = Module_Lang_Helper::getCurrent();
        
        $sql = "select 
                    pt.id,
                    pt.title,    
                    l.name    
                from {$this->_tableName} as pt
                left join {$this->_tableNameLang} as l on (l.type_id = pt.id and l.lang_id={$lang})";
                
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $list = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            
            $model =  new Villa_Module_Hotel_Model_Manage_Prepayment();
            $model->id = (int)$f['id'];
            $model->sys_name = $f['title'];
            $model->lang_id = $lang;
            $model->name = $f['name'];
            
            $list[$model->id] = $model;
        }
        
        return $list;
    }
    
    public function getNamesList()
    {
        $lang = Module_Lang_Helper::getCurrent();
        
        $sql = "SELECT `t`.*, `tl`.`name` FROM {$this->_tableName} AS `t`
                LEFT JOIN {$this->_tableNameLang} AS `tl` 
                ON (`t`.`id` = `tl`.`type_id` AND `tl`.`lang_id` = {$lang})";
                
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        
        $list = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) 
        {                
            $list[$f['id']] = $f['name'];
        }
        
        return $list;       
        
    }
}

?>
