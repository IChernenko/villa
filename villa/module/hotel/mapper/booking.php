<?php

/**
 * Description of booking
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Mapper_Booking 
{
    protected $_tableName = '[%%]hotel_booking';
    protected $_tableGuests = '[%%]hotel_booking_guests';
    protected $_tableServices = '[%%]hotel_booking_services';
    protected $_tableTransfer = '[%%]hotel_booking_transfer';
    protected $_tableExcursions = '[%%]hotel_booking_excursions';
    
    protected $_tableRoomTypes = '[%%]hotel_room_types';
    protected $_tableTransferList = '[%%]hotel_transfer';
    
    public function saveBooking(Villa_Module_Hotel_Model_Booking $model)
    {        
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->version = $model->version;
        $table->arrival = $model->basicData->arrivalToInt();
        $table->departure = $model->basicData->departureToInt();
        $table->adults = $model->basicData->adults;
        $table->children = $model->basicData->children;
        $table->days = $model->days;
        
        $table->room = $model->roomData->room_id;
        $table->room_type = $model->roomData->type_id;
        $table->room_price = $model->roomData->getCost();
        $table->prepayment_type = $model->roomData->prepayment;
        
        $table->cost = $model->totalsData->getTotalCost();
        $table->paid_options = $model->totalsData->getOptionsPaid();
        $table->paid_residence = $model->totalsData->getResidencePaid();
        $table->dc_options = $model->totalsData->getOptionsDcPercent();
        $table->dc_residence = $model->totalsData->getResidenceDcPercent();
        $table->comment = $model->comment;
                
        $table->status = 1;
        $table->currency_id = $model->totalsData->getCurrencyId();
        $table->uid = Dante_Helper_App::getUid();
        if(!$model->id) $table->creation_time = time();
        $table->modified = time();
        $table->modified_by = Dante_Helper_App::getUid();
        
        if($model->id) 
        {
            $versionMapper = new Villa_Module_Hotel_Mapper_Manage_Booking();
            $model->version = $versionMapper->createVersion($model->id);
            
            $table->version = $model->version;            
            $table->update(array('id' => $model->id));
        }
        else 
            $model->id = $table->insert();
        
        $table = new Dante_Lib_Orm_Table($this->_tableGuests);
        
        foreach($model->adultsInfo as $num => $item)
        {
            $table = new Dante_Lib_Orm_Table($this->_tableGuests);
            $table->booking_id = $model->id;
            $table->version = $model->version;
            $table->guest_num = $num;
            $table->guest_type = GUEST_TYPE_ADULT;
            $table->first_name = $item->first_name;
            $table->last_name = $item->last_name;
            $table->scan = $item->scan;
            $table->email = $item->email;
            $table->phone = $item->phone;


            $table->insert();

        }

        // заполним инфу по клиенту
        $uid = Dante_Helper_App::getUid();
        if ($uid > 0) {
            $model->clientInfo['uid'] = $uid;

            $mapper = new Villa_Module_User_Manage_Mapper();
            $profile = $mapper->getUser(Dante_Helper_App::getUid());
            if ($profile) {
                $model->clientInfo['groupId'] = $profile->group_id;
            }

        }


        foreach($model->childrenInfo as $num => $item)
        {
            $table = new Dante_Lib_Orm_Table($this->_tableGuests);
            $table->booking_id = $model->id;
            $table->version = $model->version;
            $table->guest_num = $num;
            $table->guest_type = GUEST_TYPE_CHILD;
            $table->first_name = $item->first_name;
            $table->last_name = $item->last_name;
                        
            $table->insert();
        }
        
        $childrenDiscount = Villa_Module_Settings_Helper::getDiscountForChildren();
        
        $table = new Dante_Lib_Orm_Table($this->_tableServices);
        
        foreach($model->getAddedServices() as $id => $service)
        {
            for($i = 1; $i <= $service->adults; $i++)
            {
                $table = new Dante_Lib_Orm_Table($this->_tableServices);
                $table->booking_id = $model->id;
                $table->version = $model->version;
                $table->guest_num = $i;
                $table->guest_type = GUEST_TYPE_ADULT;
                $table->service_id = $id;
                $table->count = $service->count;
                $table->cost = $service->getPrice();
                $table->insert();
            }
            
            if($service->children == 0) continue;
            
            for($i = 1; $i <= $service->children; $i++)
            {
                $table = new Dante_Lib_Orm_Table($this->_tableServices);
                $table->booking_id = $model->id;
                $table->version = $model->version;
                $table->guest_num = $i;
                $table->guest_type = GUEST_TYPE_CHILD;
                $table->service_id = $id;
                $table->count = $service->count;
                $table->cost = $service->getPrice() * $childrenDiscount;
                $table->insert();
            }
        }        
        
        $table = new Dante_Lib_Orm_Table($this->_tableExcursions);
        
        foreach($model->getAddedExcursions() as $date => $excursion)
        {
            for($i = 1; $i <= $excursion->adults; $i++)
            {
                $table = new Dante_Lib_Orm_Table($this->_tableExcursions);
                $table->booking_id = $model->id;
                $table->version = $model->version;
                $table->guest_num = $i;
                $table->guest_type = GUEST_TYPE_ADULT;
                $table->date = $date;            
                $table->cost = $excursion->getPrice();
                $table->insert();
            }
            
            if($excursion->children == 0) continue;
            
            for($i = 1; $i <= $excursion->children; $i++)
            {
                $table = new Dante_Lib_Orm_Table($this->_tableExcursions);
                $table->booking_id = $model->id;
                $table->version = $model->version;  
                $table->guest_num = $i;
                $table->guest_type = GUEST_TYPE_CHILD;
                $table->date = $date;            
                $table->cost = $excursion->getPrice() * $childrenDiscount;
                $table->insert();
            }
        }
        
        $table = new Dante_Lib_Orm_Table($this->_tableTransfer);
        
        foreach($model->getAddedTransfers() as $transfer)
        {
            $table = new Dante_Lib_Orm_Table($this->_tableTransfer);
            $table->booking_id = $model->id;
            $table->version = $model->version;
            $table->transfer_id = $transfer->id;
            $table->date = Dante_Helper_Date::strtotimef($transfer->date, 3);
            $table->time = Dante_Helper_Date::strtotimef($transfer->time, 10);
            $table->location = $transfer->location;
            $table->race = $transfer->race;
            $table->train = $transfer->train;
            $table->wagon = $transfer->wagon;
            $table->cost = $transfer->getCost();
            
            $table->insert();
        }
        return $model->id;
    }
    
    public function updateBookingUid($bookingId, $uid)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->uid = $uid;
        $table->modified_by = $uid;
        
        $table->update(array('id' => $bookingId));
    }

    public function setRoom($bookingId, $room)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->room = $room;

        $table->update(array('id' => $bookingId));
    }
    
    public function getById($id, $allData = true)
    {
        $model = new Villa_Module_Hotel_Model_Booking();
        
        $sql = "SELECT `b`.*, `r`.`max_adults`, `r`.`max_children` 
                FROM {$this->_tableName} AS `b` 
                LEFT JOIN {$this->_tableRoomTypes} AS `r` ON(`b`.`room_type` = `r`.`id`)
                WHERE `b`.`id` = ".$id;
        $f = Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
        
        $model->id = $id;
        $model->version = $f['version'];
        $model->basicData->arrival = Dante_Helper_Date::converToDateType($f['arrival'], $model->basicData->dateType);
        $model->basicData->departure = Dante_Helper_Date::converToDateType($f['departure'], $model->basicData->dateType);
        $model->basicData->adults = $f['adults'];
        $model->basicData->children = $f['children'];
        $model->days = $f['days'];
        $model->roomData->room_id = $f['room'];
        $model->roomData->type_id = $f['room_type'];
        $model->roomData->prepayment = $f['prepayment_type'];
        $model->roomData->max_adults = $f['max_adults'];
        $model->roomData->max_children = $f['max_children'];
        $model->roomData->setPrice($f['room_price']);
        $model->status = $f['status'];
        $model->uid = $f['uid'];
//        $model->creationTime = $f['creation_time'];
        $model->comment = $f['comment'];
          
        $model->init();
        
        $model->totalsData->addOptionsPayment($f['paid_options']);
        $model->totalsData->addResidencePayment($f['paid_residence']);
        
        if(!$allData) return $model;
                
        return $this->getAdditinalModelData($model);
    }
    
    public function getAdditinalModelData(Villa_Module_Hotel_Model_Booking $model)
    {
        $sql = "SELECT * FROM ".$this->_tableGuests." WHERE `booking_id` = {$model->id} AND `version` = {$model->version}";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            switch($f['guest_type'])
            {
                case GUEST_TYPE_ADULT:                     
                    $adultModel = new Villa_Module_Hotel_Model_Booking_Adult();
                    $adultModel->enabled = 1;
                    $adultModel->first_name = $f['first_name'];
                    $adultModel->last_name = $f['last_name'];
                    $adultModel->phone = $f['phone'];
                    $adultModel->email = $f['email'];                    
                    $model->adultsInfo[$f['guest_num']] = $adultModel;                    
                    break;
                
                case GUEST_TYPE_CHILD:
                    $childModel = new Villa_Module_Hotel_Model_Booking_Child();
                    $childModel->enabled = 1;
                    $childModel->first_name = $f['first_name'];
                    $childModel->last_name = $f['last_name'];
                    $model->childrenInfo[$f['guest_num']] = $childModel;
                    break;
            }
        } 
        
        $sql = "SELECT `service_id`, 
                SUM(IF(`guest_type` = 1,1,0)) AS `adults`, 
                SUM(IF(`guest_type` = 2,1,0)) AS `children`, 
                MAX(`count`) AS `days`, 
                MAX(`cost`) AS `cost` 
                FROM {$this->_tableServices} 
                WHERE `booking_id` = {$model->id} AND `version` = {$model->version} GROUP BY `service_id`";
                
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            $f['enabled'] = 1;
            $model->addService($f['service_id'], $f, false);
        }
        
        $sql = "SELECT `t`.*, `tl`.`id`, `tl`.`type` 
                FROM {$this->_tableTransfer} AS `t`
                LEFT JOIN {$this->_tableTransferList} AS `tl` ON(`t`.`transfer_id` = `tl`.`id`)
                WHERE `t`.`booking_id` = {$model->id} AND `version` = {$model->version}";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            $f['enabled'] = 1;
            $f['city'] = $f['location'];
            $f['date'] = Dante_Helper_Date::converToDateType($f['date'], $model->basicData->dateType);
            $f['time'] = Dante_Helper_Date::converToDateType($f['time'], 10);
            
            $model->addTransfer($f['type'], $f, false);
        }
        
        $sql = "SELECT `date`, 
                SUM(IF(`guest_type` = 1,1,0)) AS `adults`, 
                SUM(IF(`guest_type` = 2,1,0)) AS `children`, 
                MAX(`cost`) AS `cost`,
                outer_id
                FROM {$this->_tableExcursions} 
                WHERE `booking_id` = {$model->id} AND `version` = {$model->version} GROUP BY `date`";
                
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            $f['enabled'] = 1;
            $model->addExcursion($f['date'], $f, false);
        }
                
        $model->totalsData->recalc();

        return $model;    
    }
    
    public function changeDates(Villa_Module_Hotel_Model_Booking_Basic $basic, $days, $bookingId, $roomId)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->arrival = $basic->arrivalToInt();
        $table->departure = $basic->departureToInt();
        $table->days = $days;
        $table->room = $roomId;
        
        $table->update(array('id' => $bookingId));
        
        $sql = "UPDATE {$this->_tableServices} SET `count` = {$days} 
                WHERE `booking_id` = {$bookingId} AND `count` > {$days}";
                
        Dante_Lib_SQL_DB::get_instance()->exec($sql);

        $sql = "DELETE FROM {$this->_tableExcursions} 
                WHERE `booking_id` = {$bookingId} AND `date` < {$basic->arrivalToInt()}";
                
        Dante_Lib_SQL_DB::get_instance()->exec($sql);
        
        $booking = $this->getById($bookingId);
        
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->cost = $booking->totalsData->getTotalCost();
        $table->update(array('id' => $bookingId));
    }
}

?>
