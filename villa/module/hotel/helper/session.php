<?php
/**
 * Description of session
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Helper_Session 
{
    /**
     * Установка данных сессии
     * @param Villa_Module_Hotel_Model_Booking_Basic $model 
     */
    public static function setBookingBase(Villa_Module_Hotel_Model_Booking_Basic $model)
    {
        Dante_Lib_Session::set('bookingBasicData', $model->toArray());
    }
    
    /**
     * Установка данных сессии
     * @param Villa_Module_Hotel_Model_Booking_Room $model 
     */
    public static function setBookingRoom(Villa_Module_Hotel_Model_Booking_Room $model)
    {
        Dante_Lib_Session::set('bookingRoomData', $model->toArray());
    }    
    
    /**
     * Установка данных сессии - данные для дальнейшего сохранения платежа
     * @param int $id 
     */
    public static function setBookingPaymentData($uid, $subject)
    {
        Dante_Lib_Session::set('bookingPaymentData', array('uid' => $uid, 'subject' => $subject));
    }
    
    /**
     * Установка данных сессии - id бронирования после сохранения
     * @param int $id 
     */
    public static function setBookingId($id)
    {
        Dante_Lib_Session::set('bookingId', $id);
    }
    
    /**
     * Установка данных сессии - запоминаем, что мы только что зарегили польователя
     */
    public static function setNewUser($password)
    {
        Dante_Lib_Session::set('newUser', base64_encode($password));
    }
    
    /**
     * Установка данных сессии - запоминаем, что мы редактируем бронирование
     */
    public static function setBookingEditMode()
    {
        Dante_Lib_Session::set('bookingEditMode', true);
    }
    
    public static function setNewBookingAfterPayment()
    {
        Dante_Lib_Session::set('newBookingAfterPayment', true);
    }
    
    /**
     * Установка данных сессии
     * @param int $status 
     */
    public static function setPaymentStatus($status)
    {
        Dante_Lib_Session::set('paymentStatus', $status);
    }

    /**
     * Получение данных сессии
     * @return Villa_Module_Hotel_Model_Booking_Basic 
     */
    public static function getBookingBase()
    {
        $data = Dante_Lib_Session::get('bookingBasicData');
        if(!$data) return null;
        
        $model = new Villa_Module_Hotel_Model_Booking_Basic();
        $model->arrival = $data['arrival'];
        $model->departure = $data['departure'];
        $model->adults = $data['adults'];
        $model->children = $data['children'];
        return $model;
    }
    
    /**
     * Получение данных сессии
     * @return Villa_Module_Hotel_Model_Booking_Room 
     */
    public static function getBookingRoom()
    {
        $data = Dante_Lib_Session::get('bookingRoomData');
        if(!$data) return null;
        
        $model = new Villa_Module_Hotel_Model_Booking_Room();
        $model->type_id = $data['type_id'];
        $model->prepayment = $data['prepayment'];
        $model->max_adults = $data['max_adults'];
        $model->max_children = $data['max_children'];
        $model->setPrice($data['price']);
        return $model;
    }  
    
    /**
     * Получение id сохраненной брони
     * @return int
     */
    public static function getBookingId()
    {
        return Dante_Lib_Session::get('bookingId');
    }
    
    public static function getNewUser()
    {
        return base64_decode(Dante_Lib_Session::get('newUser'));
    }
    
    public static function getBookingEditMode()
    {
        return Dante_Lib_Session::get('bookingEditMode');
    }
    
    public static function getNewBookingAfterPayment()
    {
        return Dante_Lib_Session::get('newBookingAfterPayment');
    }
    
    /**
     * Получение данных сессии
     * @param int $status 
     */
    public static function getPaymentStatus()
    {
        return Dante_Lib_Session::get('paymentStatus');
    }
    
    /**
     * Сброс данных сессии 
     */
    public static function unsetBookingBase()
    {
        Dante_Lib_Session::remove('bookingBasicData');
    }
    
    public static function unsetBookingRoom()
    {
        Dante_Lib_Session::remove('bookingRoomData');
    }
    
    public static function unsetBookingId()
    {
        Dante_Lib_Session::remove('bookingId');
    }
    
    public static function unsetPaymentStatus()
    {
        Dante_Lib_Session::remove('paymentStatus');
    }
    
    public static function unsetNewUser()
    {
        Dante_Lib_Session::remove('newUser');
    }
    
    public static function unsetBookingEditMode()
    {
        Dante_Lib_Session::remove('bookingEditMode');
    }   
    
    public static function unsetNewBookingAfterPayment()
    {
        Dante_Lib_Session::remove('newBookingAfterPayment');
    }
    
    public static function resetAll()
    {        
        self::unsetBookingBase();
        self::unsetBookingRoom();
        self::unsetBookingId();  
        self::unsetNewUser();
        self::unsetBookingEditMode();
    }
}

?>
