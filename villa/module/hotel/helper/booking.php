<?php
/**
 * Description of booking
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Helper_Booking
{
    public static function calcServiceCost($price, $days, $adults, $children)
    {
        $cost = ($days * $price * $adults) + ($days * self::calcPriceForChildren($price) * $children);        
        return $cost;
    }
    
    public static function calcExcursionCost($price, $adults, $children)
    {
        $cost = ($price * $adults) + (self::calcPriceForChildren($price) * $children);        
        return $cost;        
    }
    
    public static function calcPriceForChildren($price)
    {        
        $childrenDiscount = Villa_Module_Settings_Helper::getDiscountForChildren();
        return $price * $childrenDiscount;
    }
}

?>
