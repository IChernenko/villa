<?php
/**
 * Description of media
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Helper_Media 
{
    public static function getServicesDir()
    {
        $upload = Dante_Lib_Config::get('app.uploadFolder');
        $opts = Dante_Lib_Config::get('hotel.servicesFolder');
        return "{$upload}/{$opts}/";
    }
    
    public static function getApartmentTypesDir()
    {
        $upload = Dante_Lib_Config::get('app.uploadFolder');
        $opts = Dante_Lib_Config::get('hotel.aptTypesFolder');
        return "{$upload}/hotel/{$opts}/";
    }

    public static function getTransfersDir()
    {
        $upload = Dante_Lib_Config::get('app.uploadFolder');
        $opts = Dante_Lib_Config::get('hotel.transfersFolder');
        return "{$upload}/{$opts}/";
    }

    public static function getAboutPageDir()
    {
        $upload = Dante_Lib_Config::get('app.uploadFolder');
        $opts = Dante_Lib_Config::get('hotel.aboutPageFolder');
        return "{$upload}/{$opts}/";
    }
}

?>
