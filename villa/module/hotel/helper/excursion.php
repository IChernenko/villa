<?php

/**
 * Description of excursion
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Helper_Excursion {

    protected static function _instanceMapper() {
        return new Villa_Module_Hotel_Mapper_Bookingoptions();
    }

    public static function getDays() {
        $days = array(
            1 => Module_Lang_Helper::translate('monday'),
            2 => Module_Lang_Helper::translate('tuesday'),
            3 => Module_Lang_Helper::translate('wednesday'),
            4 => Module_Lang_Helper::translate('thursday'),
            5 => Module_Lang_Helper::translate('friday'),
            6 => Module_Lang_Helper::translate('saturday'),
            7 => Module_Lang_Helper::translate('sunday')
        );

        return $days;
    }

    public static function getDaysShort() {
        $days = array(
            1 => Module_Lang_Helper::translate('sys_mon'),
            2 => Module_Lang_Helper::translate('sys_tue'),
            3 => Module_Lang_Helper::translate('sys_wed'),
            4 => Module_Lang_Helper::translate('sys_thu'),
            5 => Module_Lang_Helper::translate('sys_fri'),
            6 => Module_Lang_Helper::translate('sys_sat'),
            7 => Module_Lang_Helper::translate('sys_sun')
        );

        return $days;
    }

    /**
     * Создает список экскурсий для заданного периода
     * @param int $dateStart - Первая дата в формате Unixtime
     * @param int $days - Кол-во дней
     * @return array $excursions - Список экскурсий
     */
    public static function getListForPeriod($dateStart, $days, $bookingId) {
        $excursions = array();
        $weekList = self::getDaysShort();

        $firstDay = Dante_Helper_Date::getDayNum($dateStart);

        $oneDay = 24 * 3600; // один день в секундах
        $currentDay = $firstDay;
        $currentDate = $dateStart;

        for ($i = 1; $i <= $days; $i++) {

            $excursion = array(
                'dayId' => $currentDay,
                'dayName' => $weekList[$currentDay],
                'dateStr' => Dante_Helper_Date::converToDateType($currentDate, 0)
            );

            $excursions[$currentDate] = $excursion;

            $currentDay++;
            if ($currentDay > 7)
                $currentDay = 1;

            $prevDate = $currentDate;
            $currentDate += $oneDay;

            // Исправление ошибок с летним/зимним временем
            if (date("I", $prevDate) && !date("I", $currentDate))
                $currentDate += 3600;

            if (!date("I", $prevDate) && date("I", $currentDate))
                $currentDate -= 3600;
        }

        $filter = self::_instanceMapper()->getMaxGuests();
        if (!count($filter))
            return $excursions;

        $booked = self::_instanceMapper()->getBookedExcursions(array_keys($excursions), $bookingId);
        if (!count($booked))
            return $excursions;

        /* foreach ($excursions as $date => $val) {
          if (isset($filter[$val['dayId']]) && isset($booked[$date]) && $booked[$date] >= $filter[$val['dayId']]) {
          unset($excursions[$date]);
          }
          } */

        return $excursions;
    }

}

?>
