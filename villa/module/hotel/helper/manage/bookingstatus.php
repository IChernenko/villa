<?php
/**
 * Description of bookingstatus
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Helper_Manage_Bookingstatus 
{    
    public static function getStatusClass($id)
    {
        $class = false;
        switch($id)
        {
            case BOOKING_STATUS_NEW:
                $class = 'new';
                break;
            
            case BOOKING_STATUS_PROCESSED:
                $class = 'processed';
                break;
            
            case BOOKING_STATUS_ARRIVAL:
                $class = 'arrival';
                break;
            
            case BOOKING_STATUS_RESIDENCE:
                $class = 'residence';
                break;
            
            case BOOKING_STATUS_DEPARTURE:
                $class = 'departure';
                break;
            
            case BOOKING_STATUS_END:
                $class = 'end';
                break;
            
            case BOOKING_STATUS_CANCEL:
                $class = 'cancel';
                break;
        }
        
        return $class;
    }
}

?>
