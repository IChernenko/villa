<?php
/**
 * Description of deposit
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Helper_Manage_Payment 
{
    private static $_payWays = array(
        1 => 'P',
        2 => 'K',
        3 => 'T',
        4 => 'I',
        5 => 'N',
        6 => 'L'
    );

    public static function getPayWays()
    {
        return self::$_payWays;
    }
    
    public static function getPayWay($code)
    {
        return array_search($code, self::$_payWays);
    }
    
    public static function getTypes()
    {
        return array(
            PAYMENT_TYPE_INCOME => 'Оплата',
            PAYMENT_TYPE_REFUND => 'Возврат'
        );
    }
    
    public static function getSubjects()
    {

        return array(
            PAYMENT_SUBJECT_RESIDENCE => 'Проживание',
            PAYMENT_SUBJECT_SERVICE => 'Услуги',
            PAYMENT_SUBJECT_ALL => 'Все типы' // TODO указать имя
        );
    }
}

?>
