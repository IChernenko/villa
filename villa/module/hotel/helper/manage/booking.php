<?php

/**
 * Description of booking
 *
 * @author Mort
 */
class Villa_Module_Hotel_Helper_Manage_Booking {
    
    protected $_customers_types = array(
        'guest_1',
        'guest_2',
        'child_1',
        'child_2'
    );
    
    public function getTypes()
    {
        return $this->_customers_types;
    }
    
    public static function getTransferTypes()
    {
        return array(             
            1 => 'прибытие',
            2 => 'отправление'
        );
    }
    
    public static function getTransferTransportTypes()
    {
        return array(             
            1 => 'Самолет',
            2 => 'Поезд'
        );
    }
    
    public static function getTransferStatuses()
    {
        return array(
            0 => '--',
            TRANSFER_STATUS_ORDER => 'Заказать',
            TRANSFER_STATUS_ORDERED => 'Заказано',
            TRANSFER_STATUS_PROCESSED => 'Обработано',
            TRANSFER_STATUS_END => 'Завершено'
        );
    }
}

?>
