<?php
/**
 * Description of discount
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Helper_Discount 
{
    /**
     * Массив с ключами discount_services и discount_residence
     * @var type 
     */
    private static $_userDiscount;
    
    private static function _setDiscount($uid)
    {        
        $mapper = new Villa_Module_Hotel_Mapper_Discount();
        self::$_userDiscount = $mapper->getDiscount($uid);
    }
    
    public static function getDiscountOptions($uid = false)
    {
        if(!$uid) $uid = Dante_Helper_App::getUid();
        if(!$uid) return 0;        
        
        if(!self::$_userDiscount) self::_setDiscount($uid);
        return self::$_userDiscount['discount_options'];
    }
    
    public static function getDiscountResidence($uid = false)
    {
        if(!$uid) $uid = Dante_Helper_App::getUid();
        if(!$uid) return 0;        
        
        if(!self::$_userDiscount) self::_setDiscount($uid);
        return self::$_userDiscount['discount_residence'];
    }

    /**
     * Переводит величину скидки в процентах в величину для умножения цены.
     * В итоге, цена чего-либо, умноженная на эту величину, и будет ценой со скидкой. 
     * @param type $value 
     */
    public static function getDiscountFactor($dcPercent)
    {
        return 1 - ($dcPercent / 100);
    }
    
    /**
     * Возвращает разницу исходной цены и цены со скидкой
     * при наличии исходной цены и величины скидки
     * @param type $srcValue
     * @param type $dcPercent 
     */
    public static function getDiscountValue($srcValue, $dcPercent)
    {
        $factor = self::getDiscountFactor($dcPercent);
        
        return $srcValue - ($srcValue * $factor);
    }
}

?>
