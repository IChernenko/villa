<?php
/**
 * Description of excursion
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Model_Booking_Excursion extends Villa_Module_Hotel_Model_Booking_Payable
{
    public $enabled = 0;
    
    public $title;
    
    public $day;
    
    public $date;
    
    public $adults;
    
    public $children;

    public $id;

    public $outer_id;
        
    public function calcCost() 
    {
        $this->_cost = Villa_Module_Hotel_Helper_Booking::calcExcursionCost(
                $this->_price, $this->adults, $this->children
            );
    }
}

?>
