<?php
/**
 * Description of transfer
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Model_Booking_Transfer extends Villa_Module_Hotel_Model_Booking_Payable
{
    public $enabled = 0;
    
    public $title;
    
    public $id;
    
    public $type;
    
    public $transport_type;
    
    public $date;
    
    public $time;
    
    public $race;
    
    public $train;
    
    public $wagon;
    
    public $location;
    
    public function setPrice($price) 
    {
        parent::setPrice($price);
        parent::calcCost();
    }
}

?>
