<?php
/**
 * Description of service
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Model_Booking_Service extends Villa_Module_Hotel_Model_Booking_Payable
{
    public $enabled = 0;
    
    public $title;
    
    public $id;
    
    public $count;
    
    public $adults;
    
    public $children;
    
    public function calcCost() 
    {
        $this->_cost = Villa_Module_Hotel_Helper_Booking::calcServiceCost(
                $this->_price, $this->count, 
                $this->adults, $this->children
        );
    }
}

?>
