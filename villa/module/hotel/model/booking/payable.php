<?php
/**
 * Description of payable
 * Класс оплачиваемого элемента
 *
 * @author Valkyria
 */
abstract class Villa_Module_Hotel_Model_Booking_Payable 
{  
    protected $_oldPrice;
    /**
     * Стоимость единицы
     * @var type 
     */
    protected $_price;
    
    /**
     * Полная стоимость
     * @var type 
     */
    protected $_cost;

    /**
     * Получение цены для отображения
     * @return type 
     */
    public function displayPrice()
    {
        return Module_Currency_Helper::convert($this->_price);
    }
    
    /**
     * Установка цены
     * @param type $price 
     */
    public function setPrice($price)
    {
        $this->_price = $price;
    }
    
    /**
     * Получение цены для расчетов (без учета валюты)
     * @return type 
     */
    public function getPrice()
    {
        return $this->_price;
    }
    
    /**
     * Установка "старой" цены
     * @param type $price
     */
    public function setOldPrice($price)
    {
        $this->_oldPrice = $price;
    }    
    
    
    public function displayOldPrice()
    {
        return Module_Currency_Helper::convert($this->_oldPrice);
    }
    
    /**
     * Получение стоимости для отображения
     * @return type 
     */
    public function displayCost()
    {
        return Module_Currency_Helper::convert($this->_cost);
    }
    
    /**
     * Установка стоимости (по умолчанию - просто перенос цены)
     */
    public function calcCost()
    {
        $this->_cost = $this->_price;
    }
    
    /**
     * Получение стоимости для расчетов (без учета валюты)
     * @return type 
     */
    public function getCost()
    {
        return $this->_cost;
    }
}

?>
