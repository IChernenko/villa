<?php
/**
 * Description of room
 * Данные бронируемого номера
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Model_Booking_Room extends Villa_Module_Hotel_Model_Booking_Payable
{
    public $room_id;
    
    public $type_id;
    
    public $prepayment;
    
    public $max_adults;
    
    public $max_children;
        
    public function setPrice($price) 
    {
        parent::setPrice($price);
        parent::calcCost();
    }
    
    public function toArray()
    {
        return array(
            'type_id' => $this->type_id,
            'prepayment' => $this->prepayment,
            'max_adults' => $this->max_adults,
            'max_children' => $this->max_children,
            'price' => $this->_price
        );
    }
}

?>
