<?php
/**
 * Description of adult
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Model_Booking_Adult 
{
    // идентификатор записи в таблице tst_hotel_booking_guests
    public $id;

    public $enabled = 0;
    
    public $first_name;
    
    public $last_name;
    
    public $scan;
    
    public $address;
    
    public $phone;
    
    public $email;
}

?>
