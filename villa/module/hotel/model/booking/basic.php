<?php
/**
 * Description of basic
 * Базовые данные для бронирования
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Model_Booking_Basic
{    
    public $dateType = 3;
    
    public $arrival;
    
    public $departure;
    
    public $adults;
    
    public $children;
    
    public function arrivalToInt()
    {
        $validator = new Dante_Lib_Validator();
        $arriveDateInt = $validator->dateToInt($this->arrival, $this->dateType);
        unset ($validator);
        
        return $arriveDateInt;
    }
    
    public function departureToInt()
    {
        $validator = new Dante_Lib_Validator();
        $departureDateInt  = $validator->dateToInt($this->departure, $this->dateType);
        unset ($validator);
        
        return $departureDateInt;
    }
    
    public function getDays()
    {
        $departureDateInt = $this->departureToInt();
        $arriveDateInt = $this->arrivalToInt();
        
        return round(($departureDateInt - $arriveDateInt)/24/3600);
    }
    
    public function toArray()
    {
        return array(
            'arrival' => $this->arrival,
            'departure' => $this->departure,
            'adults' => $this->adults,
            'children' => $this->children,
        );
    }
}

?>
