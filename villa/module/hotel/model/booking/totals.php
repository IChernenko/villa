<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 28.03.14
 * Time: 20:09
 */

class Villa_Module_Hotel_Model_Booking_Totals
{
    protected $_prepayment;
    protected $_roomPrice;
    protected $_days;
    protected $_currencyId;
    
    protected $_optionsDiscountPercent;
    public function getOptionsDcPercent()
    {
        return $this->_optionsDiscountPercent;
    }
    protected $_residenceDiscountPercent;
    public function getResidenceDcPercent()
    {
        return $this->_residenceDiscountPercent;
    }

    // Cтоимость
    protected $_totalCost = 0;
    public function dispTotalCost()
    {
        return Module_Currency_Helper::convertTo($this->_totalCost, $this->_currencyId, true);
    }
    public function getTotalCost()
    {
        return $this->_totalCost;
    }
    
    protected $_optionsCost = 0;
    public function dispOptionsCost()
    {
        return Module_Currency_Helper::convertTo($this->_optionsCost, $this->_currencyId, true);
    }    
    public function getOptionsCost()
    {
        return $this->_optionsCost;
    }
    
    protected $_residenceCost = 0;
    public function dispResidenceCost()
    {
        return Module_Currency_Helper::convertTo($this->_residenceCost, $this->_currencyId, true);
    }
    public function getResidenceCost()
    {
        return $this->_residenceCost;
    }
    

    // Скидка
    protected $_totalDiscount = 0;
    public function dispTotalDiscount()
    {
        return Module_Currency_Helper::convertTo($this->_totalDiscount, $this->_currencyId, true);
    }
    public function getTotalDiscount()
    {
        return $this->_totalDiscount;
    }
    
    protected $_optionsDiscount = 0;
    public function dispOptionsDiscount()
    {
        return Module_Currency_Helper::convertTo($this->_optionsDiscount, $this->_currencyId, true);
    }
    public function getOptionsDiscount()
    {
        return $this->_optionsDiscount;
    }
    
    protected $_residenceDiscount = 0;
    public function dispResidenceDiscount()
    {
        return Module_Currency_Helper::convertTo($this->_residenceDiscount, $this->_currencyId, true);
    }
    public function getResidenceDiscount()
    {
        return $this->_residenceDiscount;
    }

    // К оплате
    protected $_totalPayable = 0;
    public function dispTotalPayable()
    {
        return Module_Currency_Helper::convertTo($this->_totalPayable, $this->_currencyId, true);
    }
    public function getTotalPayable()
    {
        return $this->_totalPayable;
    }
    
    protected $_optionsPayable = 0;
    public function dispOptionsPayable()
    {
        return Module_Currency_Helper::convertTo($this->_optionsPayable, $this->_currencyId, true);
    }
    public function getOptionsPayable()
    {
        return $this->_optionsPayable;
    }
    
    protected $_residencePayable = 0;
    public function dispResidencePayable()
    {
        return Module_Currency_Helper::convertTo($this->_residencePayable, $this->_currencyId, true);
    }
    public function getResidencePayable()
    {
        return $this->_residencePayable;
    }
    

    // Оплачено
    protected $_totalPaid = 0;
    public function dispTotalPaid()
    {
        return Module_Currency_Helper::convertTo($this->_totalPaid, $this->_currencyId, true);
    }
    public function getTotalPaid()
    {
        return $this->_totalPaid;
    }
    
    protected $_optionsPaid = 0;
    public function dispOptionsPaid()
    {
        return Module_Currency_Helper::convertTo($this->_optionsPaid, $this->_currencyId, true);
    }
    public function getOptionsPaid()
    {
        return $this->_optionsPaid;
    }
    
    protected $_residencePaid = 0;
    public function dispResidencePaid()
    {
        return Module_Currency_Helper::convertTo($this->_residencePaid, $this->_currencyId, true);
    }
    public function getResidencePaid()
    {
        return $this->_residencePaid;
    }
    
    protected $_prepaymentValue = 0;
    public function dispPrepaymentValue()
    {
        return Module_Currency_Helper::convertTo($this->_prepaymentValue, $this->_currencyId, true);
    }
    public function getPrepaymentValue()
    {
        return $this->_prepaymentValue;
    }
    
    public function init($prepayment, $roomPrice, $days)
    {
        $this->setPrepayment($prepayment);
        $this->setRoomPrice($roomPrice);
        $this->setDays($days);
    }
    
    public function setPrepayment($prepaymentId)
    {
        $this->_prepayment = $prepaymentId;
    }
    public function setRoomPrice($price)
    {
        $this->_roomPrice = $price;
    }
    public function setDays($days)
    {
        $this->_days = $days;
    }
    
    public function setCurrency($currency)
    {
        $this->_currencyId = $currency['id'];
    }
    
    public function getCurrencyId()
    {
        return $this->_currencyId;
    }

    public function setDiscountsByUser($uid = false)
    {
        $this->_residenceDiscountPercent = Villa_Module_Hotel_Helper_Discount::getDiscountResidence($uid);
        $this->_optionsDiscountPercent = Villa_Module_Hotel_Helper_Discount::getDiscountOptions($uid);
    }
    
    public function setDiscountsByValues($dcResidence, $dcOptions)
    {
        $this->_residenceDiscountPercent = $dcResidence;
        $this->_optionsDiscountPercent = $dcOptions;
    }
    
    public function addOptionCost($cost, $recalc = true)
    {
        $this->_optionsCost += $cost;
        if($recalc) $this->recalc();
    }
    
    public function addResidencePayment($value, $recalc = true)
    {
        $this->_residencePaid += $value;
        if($recalc) $this->recalc();
    }
    
    public function addOptionsPayment($value, $recalc = true)
    {
        $this->_optionsPaid += $value;
        if($recalc) $this->recalc();
    }
    
    public function resetPayments($recalc = true)
    {
        $this->_optionsPaid = 0;
        $this->_residencePaid = 0;
        if($recalc) $this->recalc();
    }
    
    public function recalc()
    {
        if(!$this->_prepayment)
            throw new Exception('totals recalc: prepayment error');
        
        if(!$this->_roomPrice)
            throw new Exception('totals recalc: room price error');
        
//        if(!$this->_days)
//            throw new Exception('totals recalc: days error');
        
        $this->_residenceCost = $this->_days * $this->_roomPrice;        
        $this->_totalCost = $this->_optionsCost + $this->_residenceCost;
                
        $this->_optionsDiscount = 
                Villa_Module_Hotel_Helper_Discount::getDiscountValue($this->_optionsCost, $this->_optionsDiscountPercent);
        
        $this->_residenceDiscount = 
                Villa_Module_Hotel_Helper_Discount::getDiscountValue($this->_residenceCost, $this->_residenceDiscountPercent);
        
        $this->_totalDiscount = $this->_optionsDiscount + $this->_residenceDiscount;
                
        $optionsDcFactor = Villa_Module_Hotel_Helper_Discount::getDiscountFactor($this->_optionsDiscountPercent);
        $residenceDcFactor = Villa_Module_Hotel_Helper_Discount::getDiscountFactor($this->_residenceDiscountPercent);
        
        $this->_residencePayable = $this->_residenceCost * $residenceDcFactor - $this->_residencePaid;
        $this->_optionsPayable = $this->_optionsCost * $optionsDcFactor - $this->_optionsPaid;
        
        $this->_totalPaid = $this->_residencePaid + $this->_optionsPaid;
        $this->_totalPayable = $this->_residencePayable + $this->_optionsPayable;
                
        switch($this->_prepayment) 
        {
            case PREPAYMENT_TYPE_NONE:
                $this->_prepaymentValue = 0;
                break;
            case PREPAYMENT_TYPE_FIRST:
                $this->_prepaymentValue = $this->_roomPrice * $residenceDcFactor;
                break;
            case PREPAYMENT_TYPE_ALL:
                $this->_prepaymentValue = $this->_residenceCost * $residenceDcFactor;
                break;
        }
    }
    
    public function toArray()
    {
        return array(
            'totalCost' => $this->_totalCost,
            'optionsCost' => $this->_optionsCost,
            'residenceCost' => $this->_residenceCost,
            
            'totalDiscount' => $this->_totalDiscount,
            'optionsDiscount' => $this->_optionsDiscount,
            'residenceDiscount' => $this->_residenceDiscount,
            
            'totalPayable' => $this->_totalPayable,
            'optionsPayable' => $this->_optionsPayable,
            'residencePayable' => $this->_residencePayable,
            
            'totalPaid' => $this->_totalPaid,
            'optionsPaid' => $this->_optionsPaid,
            'residencePaid' => $this->_residencePaid,
        );
    }
}