<?php
/**
 * Description of priceslist
 * Модель полного списка цен
 * 
 * @author Valkyria
 */
class Villa_Module_Hotel_Model_Manage_Priceslist 
{
    public $roomTypesPrices = array();
    
    public $servicesPrices = array();
    
    public $transferPrices = array();
    
    public $excursionPrices = array();
    
    public function getRoomTypePrice($id, $prepType, $real = true)
    {
        $price = 0;        
        $key = $real ? 'price' : 'displayedPrice';
        
        foreach($this->roomTypesPrices as $roomPrice)
        {
            if($roomPrice['id'] == $id && $roomPrice['prep_type'] == $prepType) 
                $price = $roomPrice[$key];
        }
        
        return $price;
    }
    
    public function getServicePrice($id, $real = true)
    {
        $key = $real ? 'price' : 'displayedPrice';
        return isset($this->servicesPrices[$id]) ? $this->servicesPrices[$id][$key] : 0;
    }
    
    public function isServiceDisabled($id)
    {        
        return isset($this->servicesPrices[$id]) ? $this->servicesPrices[$id]['disabled'] : 0;
    }
    
    public function getExcursionPrice($day, $real = true)
    {
        $key = $real ? 'price' : 'displayedPrice';
        return isset($this->excursionPrices[$day]) ? $this->excursionPrices[$day][$key] : 0;
    }
    
    public function isExcursionDisabled($day)
    {
        return isset($this->excursionPrices[$day]) ? $this->excursionPrices[$day]['disabled'] : 0;
    }
    
    public function getTransferPrice($id, $real = true)
    {
        $key = $real ? 'price' : 'displayedPrice';
        return isset($this->transferPrices[$id]) ? $this->transferPrices[$id][$key] : 0;
    }
    
    public function isTransferDisabled($id)
    {
        return isset($this->transferPrices[$id]) ? $this->transferPrices[$id]['disabled'] : 0;
    }
}

?>
