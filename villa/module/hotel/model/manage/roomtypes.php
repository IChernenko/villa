<?php
/**
 * Description of roomtypes
 *
 * @author Mort
 */
class Villa_Module_Hotel_Model_Manage_Roomtypes {
    
    public $sortable_rules=array(
        'name' => 'id',
        'type' => 'asc',
    );
    
    public $id;
    
    public $lang_id;

    public $title;

    public $description;

    public $image;

    public $images;
    
    public $max_adults;
    
    public $max_children;

    public $link;
    
    public $cost;

    public $equipment;
    
    public $standart_cost;
    
    /**
     *
     * @var bool
     */
    public $selected = false;
    
    public $numbers;
    
    public $comboCount;
    
    public $type;
}

?>