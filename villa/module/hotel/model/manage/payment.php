<?php
/**
 * Description of payment
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Model_Manage_Payment 
{
    public $id;
    
    public $booking_id;
    
    public $payment_id;
    
    public $user_id;
    
    public $user_email;
    
    public $type;
    
    public $subject;
    
    public $date;
    
    public $amount;
    
    public $status;
    
    public $pay_way;
    
    public $description;
}

?>
