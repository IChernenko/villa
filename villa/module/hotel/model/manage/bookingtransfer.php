<?php
/**
 * Description of bookingtransfer
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Model_Manage_Bookingtransfer
{
    public $sortable_rules = array(
        'name' => '`date` + `time`',
        'type' => 'desc'
    );
    
    public $filter_rules = array(
        array(
            'name'=>'status',
            'format'=>'`t1`.`%1$s` = "%2$s"'
        ),
    );
    
    public $booking_id;
    
    public $transfer_id;
    
    public $status;
    
    protected $date;
    
    protected $time;
    
    public function dateToStr()
    {
        return Dante_Helper_Date::converToDateType($this->date, 0);
    }
    
    public function timeToStr()
    {
        return Dante_Helper_Date::converToDateType($this->time, 10);
    }
    
    public function setDate($strDate)
    {
        if(is_int($strDate)) $this->date = $strDate;
        else $this->date = Dante_Helper_Date::strtotimef($strDate, 0);
    }
    
    public function setTime($strTime)
    {
        if(is_int($strTime)) $this->time = $strTime;
        else $this->time = Dante_Helper_Date::strtotimef($strTime, 10);
    }
    
    public function getDate()
    {
        return $this->date;
    }
    
    public function getTime()
    {
        return $this->time;
    }
}

?>
