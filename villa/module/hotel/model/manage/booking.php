<?php

/**
 * Description of booking
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Model_Manage_Booking extends Villa_Module_Hotel_Model_Booking
{       
    public $modified;
    
    public $modified_by;
    
    public $user_currency;
    
    public $creation_time;
    
    protected $_extraServices;
    
    function __construct() 
    {
        parent::__construct();
        $this->_extraServices = array();
    }
    
    protected function _setTransfers() 
    {
        $this->_transfers = array();
    }
    
    protected function _setGuests() 
    {
        for($i = 1; $i <= $this->roomData->max_adults; $i++)
        {
            $this->adultsInfo[$i] = new Villa_Module_Hotel_Model_Booking_Adult();
            if($i <= $this->basicData->adults) $this->adultsInfo[$i]->enabled = 1;
        }
        for($i = 1; $i <= $this->roomData->max_children; $i++)
        {
            $this->childrenInfo[$i] = new Villa_Module_Hotel_Model_Booking_Child();
            if($i <= $this->basicData->children) $this->childrenInfo[$i]->enabled = 1;
        }
    }

    /**
     * Вызывает инициализацию модели итоговых сумм.
     * Модель номера roomData должна быть уже установлена 
     */
    protected function _totalsDataInit()
    {        
        $this->totalsData->init($this->roomData->prepayment, $this->roomData->getCost(), $this->days);                        
        $this->totalsData->setCurrency(Module_Currency_Helper::getCurrentCurrency());
    }
    
    public function serviceExists($id) 
    {
        return array_key_exists($id, $this->_services);
    }
    public function excursionExists($date)
    {
        return array_key_exists($date, $this->_excursions);
    }
    /**
     * Добавление услуги
     * @param int $id
     * @param array $service - ключи: guest_type, guest_num, count, cost
     * @param type $recalc 
     */
    public function addService($id, $service, $recalc = true) 
    {
        $this->_arrayKeysValidate($service, array('guest_type', 'guest_num', 'count', 'cost'), 'cant add service: '); 
        
        $defCost = 0;
        if($this->_priceList) $defCost = $this->_priceList->getServicePrice($id);
        
        if(!isset($this->_services[$id]))
            $this->_services[$id] = new Villa_Module_Hotel_Model_Manage_Booking_Service(
                        $id,
                        $this->roomData->max_adults,
                        $this->roomData->max_children,
                        $this->days,
                        $defCost
                    );
        
        if($service['count'] > $this->days) $service['count'] = $this->days;
        
        $this->_services[$id]->addGuestService(
                $service['guest_type'], 
                $service['guest_num'], 
                $service['count'], 
                $service['cost']
            );
        
        $this->totalsData->addOptionCost($service['cost'] * $service['count'], $recalc);
    }
    
    public function addNewService($id, $days)
    {
        $defCost = 0;
        if($this->_priceList) $defCost = $this->_priceList->getServicePrice($id);
        
        $this->_services[$id] = new Villa_Module_Hotel_Model_Manage_Booking_Service(
                    $id,
                    $this->roomData->max_adults,
                    $this->roomData->max_children,
                    $days,
                    $defCost
                );
        
        $childrenDiscount = Villa_Module_Settings_Helper::getDiscountForChildren();
        foreach($this->adultsInfo as $num => $adult)
        {
            if(!$adult->enabled) continue;
            
            $adultType = GUEST_TYPE_ADULT;
            $this->_services[$id]->addGuestService($adultType, $num);
            
            $this->totalsData->addOptionCost($defCost, false);
        }
        
        foreach($this->childrenInfo as $num => $child)
        {
            if(!$child->enabled) continue;
            
            $childType = GUEST_TYPE_CHILD;
            $this->_services[$id]->addGuestService($childType, $num);
            
            $this->totalsData->addOptionCost($defCost * $childrenDiscount, false);
        }
        
        $this->totalsData->recalc();
    }
    
    /**
     * Добавление экскурсии
     * @param array $excursion - ключи: guest_type, guest_num, cost
     * @param type $recalc 
     */
    public function addExcursion($date, $excursion, $recalc = true) 
    {
        $this->_arrayKeysValidate($excursion, array('guest_type', 'guest_num', 'cost'), 'cant add service: '); 
        
        $defCost = 0;
        if($this->_priceList) 
        {
            $day = Dante_Helper_Date::getDayNum($date);            
            $defCost = $this->_priceList->getExcursionPrice($day);
        }
        
        if(!isset($this->_excursions[$date]))
            $this->_excursions[$date] = new Villa_Module_Hotel_Model_Manage_Booking_Excursion(
                        $date,
                        $this->roomData->max_adults,
                        $this->roomData->max_children,
                        $defCost
                    );
        
        $this->_excursions[$date]->addGuestExcursion(
                $excursion['guest_type'], 
                $excursion['guest_num'], 
                $excursion['cost']
            );
        
            
        $this->totalsData->addOptionCost($excursion['cost'], $recalc);
    }
    
    public function addNewExcursion($date)
    {
        $defCost = 0;
        if($this->_priceList) 
        {
            $day = Dante_Helper_Date::getDayNum($date);            
            $defCost = $this->_priceList->getExcursionPrice($day);
        }
        
        $this->_excursions[$date] = new Villa_Module_Hotel_Model_Manage_Booking_Excursion(
                    $date,
                    $this->roomData->max_adults,
                    $this->roomData->max_children,
                    $defCost
                );
        
        foreach($this->adultsInfo as $num => $adult)
        {
            if(!$adult->enabled) continue;
            
            $adultType = GUEST_TYPE_ADULT;
            $this->_excursions[$date]->addGuestExcursion($adultType, $num);
            
            $this->totalsData->addOptionCost($defCost, false);
        }
        
        foreach($this->childrenInfo as $num => $child)
        {
            if(!$child->enabled) continue;
            
            $childType = GUEST_TYPE_CHILD;
            $this->_excursions[$date]->addGuestExcursion($childType, $num);
            
            $this->totalsData->addOptionCost($defCost, false);
        }
        
        $this->totalsData->recalc();
    }
    
    /**
     * Добавление трансфера
     * @param $transfer - ключи: transfer_id, date, time, location
     * @param type $recalc 
     */
    public function addTransfer($type, $transfer, $recalc = true)
    {            
        $keys = array(
            'transfer_id', 'date', 'time', 'location'
        );
        
        $this->_arrayKeysValidate($transfer, $keys, 'cant add transfer: ');  
        
        $defCost = 0;
        if($this->_priceList) $defCost = $this->_priceList->getTransferPrice($transfer['transfer_id']);
        
        $transferModel = new Villa_Module_Hotel_Model_Booking_Transfer();
        $transferModel->type = $type;
        $transferModel->enabled = 1;
        $transferModel->id = $transfer['transfer_id'];
        $transferModel->date = $transfer['date'];
        $transferModel->time = $transfer['time'];
        $transferModel->location = $transfer['location'];
        $transferModel->race = isset($transfer['race']) ? $transfer['race'] : NULL;
        $transferModel->train = isset($transfer['train']) ? $transfer['train'] : NULL;
        $transferModel->wagon = isset($transfer['wagon']) ? $transfer['wagon'] : NULL;
        $transferModel->setPrice($defCost);
        
        $this->_transfers[$transferModel->type] = $transferModel;
        
        $this->totalsData->addOptionCost($transferModel->getCost(), $recalc);
    }
    
    /**
     * Добавление дополнительной услуги
     * @param type $extraService - ключи: date, descr, cost
     * @param type $recalc 
     */
    public function addExtraService($extraService, $recalc = true)
    {
        $this->_arrayKeysValidate($extraService, array('date', 'descr', 'cost'), 'cant add extra-service: ');  
        
        $model = new Villa_Module_Hotel_Model_Manage_Booking_Extraservice();
        $model->date = $extraService['date'];
        $model->descr = $extraService['descr'];
        $model->setPrice($extraService['cost']);
        
        $this->_extraServices[] = $model;
        
        $this->totalsData->addOptionCost($model->getCost(), $recalc);
    }
    
    public function getExtraServices()
    {
        return $this->_extraServices;
    }
    
    public function removeEmptyValues() 
    {
        foreach($this->adultsInfo as $num => $info)
        {
            if(!$info->enabled || (!$info->first_name && !$info->last_name && !$info->email))
                unset ($this->adultsInfo[$num]);
        }
        
        if($this->basicData->children && is_array($this->childrenInfo))
        {
            foreach($this->childrenInfo as $num => $info)
            {
                if(!$info->enabled || (!$info->first_name && !$info->last_name))
                    unset ($this->childrenInfo[$num]);
            }
        }
        else 
        {
            $this->childrenInfo = array();
        }  
        
        foreach($this->_services as $id => $service)
        {
            foreach($service->adults as $num => $item)
            {
                if(!$item->enabled) 
                    unset($this->_services[$id]->adults[$num]);
            }
            foreach($service->children as $num => $item)
            {
                if(!$item->enabled)
                    unset($this->_services[$id]->children[$num]);
            }
        }
        
        foreach($this->_excursions as $date => $excursion)
        {
            foreach($excursion->adults as $num => $item)
            {
                if(!$item->enabled)
                    unset($this->_excursions[$date]->adults[$num]);
            }
            foreach($excursion->children as $num => $item)
            {
                if(!$item->enabled)
                    unset($this->_excursions[$date]->children[$num]);
            }
        }
    }
    
    public function resetDiscountAndPayment()
    {
        $this->totalsData->setDiscountsByValues(0, 0);
        $this->totalsData->resetPayments();
    }
}

?>
