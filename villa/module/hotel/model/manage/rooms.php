<?php

/**
 * Description of rooms
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Model_Manage_Rooms 
{    
    public $sortable_rules = array(
        'name' => 'id',
        'type' => 'asc',
    );
    
    public $rows_in_page = 100;
    
    public $filter_rules = array(
        array(
            'name' => 'id',
            'format' => '`t1`.`%1$s` = "%2$s"'
        ),
        array(
            'name' => 'type',
            'format'=>'`t1`.`%1$s` = "%2$s"'
        ),
        array(
            'name' => 'priority',
            'format' => '`t1`.`%1$s` = "%2$s"'
        ),
    );
    
    public $id;

    public $type;
    
    public $priority;
}

?>