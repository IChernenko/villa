<?php

/**
 * Description of manage
 *
 * @author Mort
 */
class Villa_Module_Hotel_Model_Manage_Service 
{    
    public $id;

    public $sys_name;

    public $cost;
    
    public $image;
    
    public $lang_id;
    
    public $name;
    
    public $description;
    
    public $full_descr;
    
    public $outer_id;
}

?>
