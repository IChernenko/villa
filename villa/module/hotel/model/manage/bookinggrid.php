<?php
/**
 * Description of bookinggrid
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Model_Manage_Bookinggrid 
{
    public $rows_in_page = 200;

    public $sortable_rules = array(
        'name' => 'creation_time',
        'type' => 'desc',
    );
    
    public $filter_rules = array(
        array(
            'name'=>'status',
            'format'=>'`t1`.`%1$s` = "%2$s"'
        ),
        array(
            'name'=>'currency_id',
            'format'=>'`t1`.`%1$s` = "%2$s"'
        ),
        array(
            'name'=>'arrival',
            'format'=>'date'
        ),
        array(
            'name'=>'departure',
            'format'=>'date'
        ),
        array(
            'name'=>'days',
            'format'=>'`t1`.`%1$s` = "%2$s"'
        ),
        array(
            'name'=>'prepayment_type',
            'format'=>'`t1`.`%1$s` = "%2$s"'
        ),
        array(
            'name'=>'room',
            'format'=>'`t1`.`%1$s` = "%2$s"'
        ),
        array(
            'name'=>'room_type',
            'format'=>'`t1`.`%1$s` = "%2$s"'
        ),
        array(
            'name'=>'first_name',
            'format'=>'`t2`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ),
        array(
            'name'=>'last_name',
            'format'=>'`t2`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ),
        array(
            'name'=>'email',
            'format'=>'`t2`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ),
        array(
            'name'=>'phone',
            'format'=>'`t2`.`%1$s` LIKE "%3$s%2$s%3$s"'
        )
    );
}

?>
