<?php
/**
 * Description of excursion
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Model_Manage_Booking_Excursion
{
    public $date;
    
    public $day;
    
    public $adults;
    
    public $children;
    
    protected $_totalCost = 0;
    
    protected $_defaultCost;
    
    function __construct($date, $adults, $children, $defaultCost)
    {
        $this->date = $date;
        $this->day = Dante_Helper_Date::getDayNum($date);
                
        $this->_defaultCost = $defaultCost;
        $childrenDiscount = Villa_Module_Settings_Helper::getDiscountForChildren();
        
        for($i = 1; $i <= $adults; $i++)
        {
            $excursion = new Villa_Module_Hotel_Model_Manage_Booking_Excursionguest();
            $excursion->cost = $this->_defaultCost;
            
            $this->adults[$i] = $excursion;
        }
        
        for($i = 1; $i <= $children; $i++)
        {
            $excursion = new Villa_Module_Hotel_Model_Manage_Booking_Excursionguest();
            $excursion->cost = $this->_defaultCost * $childrenDiscount;
            
            $this->children[$i] = $excursion;
        }
    }
    
    public function addGuestExcursion($guestType, $guestNum, $cost = NULL)
    {
        switch($guestType)
        {
            case GUEST_TYPE_ADULT:
                $this->adults[$guestNum]->enabled = 1;
                
                if($cost != NULL)
                    $this->adults[$guestNum]->cost = $cost;
                
                $this->_totalCost += $this->adults[$guestNum]->cost;
                break;

            case GUEST_TYPE_CHILD:
                $this->children[$guestNum]->enabled = 1;
                
                if($cost != NULL)
                    $this->children[$guestNum]->cost = $cost;
                
                $this->_totalCost += $this->children[$guestNum]->cost;
                break;
        }            
    }
    
    public function displayCost()
    {
        return Module_Currency_Helper::convert($this->_totalCost);
    }
}

?>
