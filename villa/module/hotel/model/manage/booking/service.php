<?php
/**
 * Description of service
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Model_Manage_Booking_Service
{    
    public $id;
    
    public $adults;
    
    public $children;
    
    protected $_totalCost = 0;
    
    protected $_defaultDays;

    protected $_defaultCost;
    
    function __construct($id, $adults, $children, $days, $defaultCost)
    {
        $this->id = $id;
        
        $this->_defaultCost = $defaultCost;
        $this->_defaultDays = $days;
        
        $childrenDiscount = Villa_Module_Settings_Helper::getDiscountForChildren();
        
        for($i = 1; $i <= $adults; $i++)
        {
            $service = new Villa_Module_Hotel_Model_Manage_Booking_Serviceguest();
            $service->count = $this->_defaultDays;
            $service->cost = $this->_defaultCost;
            
            $this->adults[$i] = $service;
        }
        
        for($i = 1; $i <= $children; $i++)
        {
            $service = new Villa_Module_Hotel_Model_Manage_Booking_Serviceguest();
            $service->count = $this->_defaultDays;
            $service->cost = $this->_defaultCost * $childrenDiscount;
            
            $this->children[$i] = $service;
        }
    }


    public function addGuestService($guestType, $guestNum, $count = NULL, $cost = NULL)
    {
        if($count == NULL)
            $count = $this->_defaultDays;
                
        switch($guestType)
        {
            case GUEST_TYPE_ADULT:
                $this->adults[$guestNum]->enabled = 1;
                $this->adults[$guestNum]->count = $count;
                
                if($cost != NULL)
                    $this->adults[$guestNum]->cost = $cost;
                
                $this->_totalCost += $this->adults[$guestNum]->cost * $count;
                break;

            case GUEST_TYPE_CHILD:
                $this->children[$guestNum]->enabled = 1;
                $this->children[$guestNum]->count = $count;
                
                if($cost != NULL)
                    $this->children[$guestNum]->cost = $cost;
                
                $this->_totalCost += $this->children[$guestNum]->cost * $count;
                break;
        }
    }
    
    public function displayCost()
    {
        return Module_Currency_Helper::convert($this->_totalCost);
    }
}

?>
