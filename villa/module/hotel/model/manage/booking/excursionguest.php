<?php
/**
 * Description of excursionguest
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Model_Manage_Booking_Excursionguest 
{
    public $enabled = 0;
    
    public $cost = 0;
    
    public function displayCost()
    {
        return Module_Currency_Helper::convert($this->cost);
    }
}

?>
