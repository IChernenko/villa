<?php
/**
 * Description of serviceguest
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Model_Manage_Booking_Serviceguest 
{
    public $enabled = 0;
    
    public $count = 1;
    
    public $cost = 0;
    
    public function displayCost()
    {
        return Module_Currency_Helper::convert($this->cost);
    }
}

?>
