<?php
/**
 * Description of prices
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Model_Manage_Prices 
{    
    public $id = 0; // default value for new price item
    
    public $title;
    
    public $entity_type_id;
    
    public $entity_id;
    
    public $prep_type;
    
    public $standardPrice;
    
    public $displayedPrice;
}

?>
