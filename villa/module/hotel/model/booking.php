<?php
/**
 * Description of booking
 * Полная модель бронирования
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_Model_Booking 
{
    public $id = 0;
    
    public $version = 1;
    
    /**
     * @var Villa_Module_Hotel_Model_Booking_Basic
     */
    public $basicData;
    
    /**
     * @var Villa_Module_Hotel_Model_Booking_Room
     */
    public $roomData;
    
    /**
     * @var Villa_Module_Hotel_Model_Booking_Totals
     */
    public $totalsData;
    
    public $days;
    
    public $adultsInfo;
    
    public $childrenInfo;
    
    protected $_services;
    
    protected $_transfers;
    
    protected $_excursions;
    
    protected $_childrenDiscount;

    /**
     * Стандартные цены на услуги
     * @var Villa_Module_Hotel_Model_Manage_Priceslist 
     */
    protected $_priceList;
    
    public $comment;
    
    public $uid;
    
    public $status;

    // инфа по клиенту
    public $clientInfo = array();

    /**
     * Конструктор. Установка дефолтных значений 
     */
    function __construct() 
    {
        $this->basicData = new Villa_Module_Hotel_Model_Booking_Basic();
        $this->roomData = new Villa_Module_Hotel_Model_Booking_Room();
        $this->totalsData = new Villa_Module_Hotel_Model_Booking_Totals();
        $this->adultsInfo = array();
        $this->childrenInfo = array();
        $this->_services = array();
        $this->_transfers = array();
        $this->_excursions = array();
    }
    
    /**
     * Инициализация общей модели:
     * - Установка дней
     * - Заполнение массивов adultsInfo и childrenInfo
     * - Заполнение массивa _transfers
     * - Инициализация модели сумм
     * Перед вызовом метода нужно установить basicData и roomData
     */
    public function init()
    {
        $this->_setDays();
        $this->_setGuests();
        $this->_setTransfers();
        $this->_setPriceList();
        $this->_totalsDataInit();
    }

    /**
     * Устанавливает дни
     * Модель базовой информации basicData должна быть установлена  
     */
    protected function _setDays()
    {
        if(!$this->basicData)
            throw new Exception('set days: no basic data');
        
        $this->days = $this->basicData->getDays();
    }
    
    /**
     * Сразу устанавливаем максимальное кол-во гостей
     * Мы ведь не хотим получить что-то вроде 'undefined offset' ?
     */
    protected function _setGuests()
    {
        for($i = 1; $i <= $this->basicData->adults; $i++)
        {
            $this->adultsInfo[$i] = new Villa_Module_Hotel_Model_Booking_Adult();
            if($i <= $this->basicData->adults) $this->adultsInfo[$i]->enabled = 1;
        }
        for($i = 1; $i <= $this->basicData->children; $i++)
        {
            $this->childrenInfo[$i] = new Villa_Module_Hotel_Model_Booking_Child();
            if($i <= $this->basicData->children) $this->childrenInfo[$i]->enabled = 1;
        }
    }
      
    protected function _setPriceList()
    {
        $priceMapper = new Villa_Module_Hotel_Mapper_Manage_Prices();
        $this->_priceList = $priceMapper->getCurrentPriceList($this->basicData->arrivalToInt());
    }
    
    public function getPriceList()
    {
        return $this->_priceList;
    }
    
    protected function _setTransfers()
    {
        $arrival = new Villa_Module_Hotel_Model_Booking_Transfer();
        $arrival->date = $this->basicData->arrival;
        
        $departure = new Villa_Module_Hotel_Model_Booking_Transfer();
        $departure->date = $this->basicData->departure;
        
        $this->_transfers = array(
            TRANSFER_TYPE_ARRIVAL => $arrival,
            TRANSFER_TYPE_DEPARTURE => $departure
        );
    }

    public function getTransfers() {
        return $this->_transfers;
    }

    /**
     * Вызывает инициализацию модели итоговых сумм.
     * Модель номера roomData должна быть установлена 
     */
    protected function _totalsDataInit()
    {        
        $this->totalsData->init($this->roomData->prepayment, $this->roomData->getCost(), $this->days);
        
        // Считаем скидки, если юзер авторизован (опред. автоматически)
        $this->totalsData->setDiscountsByUser();
        
        //Устанавливаем валюту
        $this->totalsData->setCurrency(Module_Currency_Helper::getCurrentCurrency());
    }
    
    public function getAddedServices()
    {
        return $this->_services;
    }
    public function hasServices()
    {
        return (boolean)count($this->_services);
    }
    public function serviceExists($id)
    {
        return array_key_exists($id, $this->_services) && $this->_services[$id]->enabled;
    }
    public function getService($id)
    {
        return isset($this->_services[$id]) ? $this->_services[$id] : null;
    }

    public function getServices()
    {
        return $this->_services;
    }
    
    public function getAddedExcursions()
    {
        return $this->_excursions;
    }
    public function hasExcursions()
    {
        return (boolean)count($this->_excursions);
    }    
    public function excursionExists($date)
    {
        return array_key_exists($date, $this->_excursions) && $this->_excursions[$date]->enabled;
    }
    public function getExcursion($date)
    {
        return isset($this->_excursions[$date]) ? $this->_excursions[$date] : null;
    }

    public function getExcursions()
    {
        return $this->_excursions;
    }
    
    public function getAddedTransfers()
    {
        return $this->_transfers;
    }
    public function hasTransfers()
    {
        foreach($this->_transfers as $transfer)
        {
            if($transfer->enabled) return true;
        }
        return false;
    }
    
    public function transferExists($type, $id = false)
    {
        if(!array_key_exists($type, $this->_transfers)) return false;
        if(!$id) return true;
        
        $obj = $this->_transfers[$type];
        if($obj->id == $id && $obj->enabled == 1) return true;
        else return false;
    }
    
    public function getTransfer($type)
    {
        return $this->_transfers[$type];
    }
    
    /**
     * Добавление услуги
     * @param int $id
     * @param array $service - ключи: enabled, days, adults, children
     * @param type $recalc 
     */
    public function addService($id, $service, $recalc = true)
    {   
        if($this->_priceList->isServiceDisabled($id)) return;
        
        $this->_arrayKeysValidate($service, array('enabled', 'days', 'adults', 'children'), 'cant add service: ');        
        
        $serviceModel = new Villa_Module_Hotel_Model_Booking_Service();
        $serviceModel->id = $id;
        $serviceModel->enabled = $service['enabled'];
        $serviceModel->count = $service['days'];
        
        $serviceModel->adults = $service['adults'] <= $this->basicData->adults ? 
                $service['adults'] : $this->basicData->adults;
        
        $serviceModel->children = $service['children'] <= $this->basicData->children ? 
                $service['children'] : $this->basicData->children;
        
        $serviceModel->setPrice($this->_priceList->getServicePrice($id)); 
        $serviceModel->setOldPrice($this->_priceList->getServicePrice($id, false));
        $serviceModel->calcCost();
        
        $this->_services[$serviceModel->id] = $serviceModel;
        
        if($serviceModel->enabled == 1)
            $this->totalsData->addOptionCost($serviceModel->getCost(), $recalc);
    }
    
    /**
     * Добавление экскурсии
     * @param array $excursion - ключи: enabled, price, adults, children
     * @param type $recalc 
     */
    public function addExcursion($date, $excursion, $recalc = true)
    {
        $day = Dante_Helper_Date::getDayNum($date);
        if($this->_priceList->isExcursionDisabled($day)) return;
        
        $this->_arrayKeysValidate($excursion, array('enabled', 'adults', 'children'), 'cant add excursion: ');  
        
        $excursionModel = new Villa_Module_Hotel_Model_Booking_Excursion();
        $excursionModel->date = $date;
        $excursionModel->enabled = $excursion['enabled'];
        
        $excursionModel->adults = $excursion['adults'] <= $this->basicData->adults ? 
                $excursion['adults'] : $this->basicData->adults;
        
        $excursionModel->children = $excursion['children'] <= $this->basicData->children ? 
                $excursion['children'] : $this->basicData->children;
                
        $excursionModel->setPrice($this->_priceList->getExcursionPrice($day));    
        $excursionModel->setOldPrice($this->_priceList->getExcursionPrice($day, false));            
        $excursionModel->calcCost();
        
        $this->_excursions[$excursionModel->date] = $excursionModel;
        
        if($excursionModel->enabled == 1)
            $this->totalsData->addOptionCost($excursionModel->getCost(), $recalc);
    }
    
    /**
     * Добавление трансфера
     * @param $transfer - ключи: enabled, id, date, time, city, race, train, wagon
     * @param type $recalc 
     */
    public function addTransfer($type, $transfer, $recalc = true)
    {     
        $keys = array(
            'enabled', 'id', 'date', 'time', 
            'city', 'race', 'train', 'wagon'
        );
        
        $this->_arrayKeysValidate($transfer, $keys, 'cant add transfer: ');  
            
        if($this->_priceList->isTransferDisabled($transfer['id'])) return;
        
        $transferModel = new Villa_Module_Hotel_Model_Booking_Transfer();
        $transferModel->type = $type;
        $transferModel->enabled = $transfer['enabled'];
        $transferModel->id = $transfer['id'];
        $transferModel->date = $transfer['date'];
        $transferModel->time = $transfer['time'];
        $transferModel->location = $transfer['city'];
        $transferModel->race = $transfer['race'];
        $transferModel->train = $transfer['train'];
        $transferModel->wagon = $transfer['wagon'];
        $transferModel->setPrice($this->_priceList->getTransferPrice($transfer['id']));
        $transferModel->setOldPrice($this->_priceList->getTransferPrice($transfer['id'], false));
        
        $this->_transfers[$transferModel->type] = $transferModel;
        
        if($transferModel->enabled == 1)
            $this->totalsData->addOptionCost($transferModel->getCost(), $recalc);
    }
    
    public function removeEmptyValues()
    {
        $defText = '';
        foreach($this->adultsInfo as $num => &$info)
        {
            if($num == 1) continue;
            
            $info->first_name = $info->first_name ? $info->first_name : $defText;
            $info->last_name = $info->last_name ? $info->last_name : $defText;
            $info->email = $info->email ? $info->email : $defText;
            $info->phone = $info->phone ? $info->phone : $defText;
        }
        
        if($this->basicData->children && is_array($this->childrenInfo))
        {
            foreach($this->childrenInfo as $num => &$info)
            {
                $info->first_name = $info->first_name ? $info->first_name : $defText;
                $info->last_name = $info->last_name ? $info->last_name : $defText;             
            }
        }
        else 
        {
            $this->childrenInfo = array();
        } 
        
        foreach($this->_services as $id => $service)
        {
            if(!$service->enabled) unset($this->_services[$id]);
        }
        
        foreach($this->_excursions as $date => $excursion)
        {
            if(!$excursion->enabled) unset($this->_excursions[$date]);
        }
        
        foreach($this->_transfers as $type => $transfer)
        {
            if(!$transfer->enabled) unset($this->_transfers[$type]);
        }
    }
    
    /**
     * Валидация массива на наличие нужных ключей
     * @param type $keys 
     */
    protected function _arrayKeysValidate($array, $keys, $message = false)
    {
        foreach($keys as $key)
        {
            if(!array_key_exists($key, $array))
                throw new Exception($message.' missing key ['.$key.'] in input array');
        }
    }
}

?>
