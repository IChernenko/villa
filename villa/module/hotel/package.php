<?php
global $package;
$package = array(
    'version' => '104',
    'name' => 'villa.module.hotel',
    'dependence' => array(
        'lib.jsphp',
        'lib.jdatepicker',
        'lib.jgrid',
        'lib.lightbox',
        'lib.jquery.smoothdivscroll',
        'lib.jquery.fancybox',
        'module.auth',
        'module.lang',
        'component.tinymce',
        'component.jalerts',
        'module.lang',
        'module.profile',
        'module.entity',
        'villa.module.homepage',
        'module.currency',
        'ws'=>array(
            'admin'=>array(
                'lib.jquery.elrte'
            ),            
        )
    ),
    'js' => array(
        '../villa/module/hotel/js/hotel.js',
        '../villa/module/hotel/js/bookingform.js',
        '../villa/module/hotel/js/roomtypes.js',
        '../villa/module/hotel/js/room_search.js',
        'ws' => array(
            'admin' => array(                
                '../villa/module/hotel/js/manage/roomtypes.js',
                '../villa/module/hotel/js/manage/prepayment.js',
                '../villa/module/hotel/js/manage/equipment.js',
                '../villa/module/hotel/js/manage/rooms.js',
                '../villa/module/hotel/js/manage/loading.js',
                '../villa/module/hotel/js/manage/booking.js',
                '../villa/module/hotel/js/manage/services.js',
                '../villa/module/hotel/js/manage/cashcontrol.js',
                '../villa/module/hotel/js/manage/prices.js',
                '../villa/module/hotel/js/manage/transfer.js',
                '../villa/module/hotel/js/manage/bookingtransfer.js',
            )
        )
    ),
    'css' => array(
        'ws' => array(
            'admin' => array(
                '../villa/module/hotel/css/hotel_manage.css'
            )
        )
    )
);