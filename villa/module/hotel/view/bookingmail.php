<?php
/**
 * Description of bookingmail
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_View_Bookingmail
{
    public $bookingId;
    public $email;
    public $pass;
    
    public function draw()
    {
        $tplFileName = '../villa/module/hotel/template/booking_mail.html';
        
        $tpl = new Dante_Lib_Template();
        $tpl->bookingId = $this->bookingId;
        $tpl->email = $this->email;
        $tpl->password = $this->pass;
        $tpl->domain = Dante_Lib_Config::get('url.domain_main');        
        
        $tpl->newUser = (boolean)$this->pass;
        
        return $tpl->draw($tplFileName);
    }
}

?>
