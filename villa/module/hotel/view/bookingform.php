<?php
/**
 * Description of bookingform
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_View_Bookingform
{
    protected $_mode;

    protected $_currencyCode = '';
    protected $_roomTypeName = '';
    protected $_prepaymentName = '';

    public $user;

    public $services;
    public $transfers;
    public $excursions;

    public $gallery;
    public $bottomText;

    public $payMessage = "";

    protected $_paysystem;

    function __construct()
    {
        $this->_mode = BF_VIEW_MODE_NEW;

        $this->_paysystem = Villa_Module_Paysystems_Factory::getPaySystemHelper();
        $this->_paysystem->setNotifyUrl('notify/');
        $this->_paysystem->setReturnUrl('pay-by-form/');
    }

    public function setMode($mode)
    {
        $this->_mode = $mode;

        if($this->_mode == BF_VIEW_MODE_AFTER_PAYMENT)
        {
            $payStatus = Villa_Module_Hotel_Helper_Session::getPaymentStatus();
            $this->payMessage = $this->getPaymentStatusMessage($payStatus);
        }
    }

    public function getPaymentStatusMessage($payStatus)
    {
        switch($payStatus)
        {
            case PAYMENT_STATUS_SUCCESS:
                $message = "Оплата произведена успешно";
                break;

            case PAYMENT_STATUS_WAIT:
                $message = "Ваш платеж принят и находится в обработке";
                break;

            case PAYMENT_STATUS_ERROR:
                $message = "Процесс оплаты завершен с ошибкой";
                break;

            default:
                $message = '';
                break;
        }

        Villa_Module_Hotel_Helper_Session::unsetPaymentStatus();

        return $message;
    }

    public function setCurrencyCode($code)
    {

        $this->_currencyCode = $code;
        // хак специально для платежных систем
        if (strtoupper($code) == 'ГРН') $code = 'UAH';
        $this->_paysystem->currencyCode = $code;
    }

    public function setRoomTypeName($name)
    {
        $this->_roomTypeName = $name;
        $this->_paysystem->roomType = $name;
    }

    public function setPrepaymentName($name)
    {
        $this->_prepaymentName = $name;
        $this->_paysystem->prepayment = $name;
    }

    public function setPaymentReturnUrl($url)
    {
        $this->_paysystem->setReturnUrl($url);
    }

    public function setPaymentBookingId($id)
    {
        $this->_paysystem->bookingId = $id;
    }

    public function setPaymentUid($uid)
    {
        $this->_paysystem->uid = $uid;
    }

    public function draw(Villa_Module_Hotel_Model_Booking $model)
    {
        $tplFileName = '../villa/module/hotel/template/bookingform.html';
        $tpl = new Dante_Lib_Template();

        $this->setPaymentBookingId($model->id);
        $this->setPaymentUid(Dante_Helper_App::getUid());
        $tpl->bookingId = $model->id;
        $tpl->basicData = $model->basicData;
        $tpl->roomData = $model->roomData;
        $tpl->totalsData = $model->totalsData;
        $tpl->adultsInfo = $model->adultsInfo;
        $tpl->childrenInfo = $model->childrenInfo;
        $tpl->days = $model->days;
        $tpl->comment = $model->comment;
        $tpl->status = $model->status;

        $modelServices = $model->getAddedServices();
        ksort($modelServices);
        $modelExcursions = $model->getAddedExcursions();
        ksort($modelExcursions);

        $tpl->modelServices = $modelServices;
        $tpl->modelExcursions = $modelExcursions;

        $tpl->modelTransfArrival = $model->getTransfer(TRANSFER_TYPE_ARRIVAL);
        $tpl->modelTransfDeparture = $model->getTransfer(TRANSFER_TYPE_DEPARTURE);

        $tpl->roomTypeName = $this->_roomTypeName;
        $tpl->prepaymentName = $this->_prepaymentName;

        $tpl->user = $this->user;
        $tpl->currency = $this->_currencyCode;

        foreach($this->services as $id => &$service)
        {
            $serviceModel = $model->getService($id);
            if(empty($serviceModel)) continue;

            $service['guestSelect'] = $this->_drawOptionGuestSelect(
                    $model->basicData->adults, $model->basicData->children,
                    $serviceModel->adults, $serviceModel->children, 'services['.$id.']'
                );
        }

        foreach($this->excursions as $date => &$excursion)
        {
            $excursModel = $model->getExcursion($date);
            if(empty($excursModel)) continue;

            $excursion['guestSelect'] = $this->_drawOptionGuestSelect(
                    $model->basicData->adults, $model->basicData->children,
                    $excursModel->adults, $excursModel->children, 'excursions['.$date.']', true
                );
        }

        $tpl->services = $this->services;
        $tpl->excursions = $this->excursions;

        $transfersArrival = array();
        $transfersDeparture = array();

        foreach($this->transfers as $id => $transfer)
        {
            switch ($transfer['type'])
            {
                case TRANSFER_TYPE_ARRIVAL:
                    $transfersArrival[$id] = $transfer;
                    break;

                case TRANSFER_TYPE_DEPARTURE:
                    $transfersDeparture[$id] = $transfer;
                    break;
            }
        }

        $tpl->transfersArr = $transfersArrival;
        $tpl->transfersDep = $transfersDeparture;

        $excursionsTotal = 0;

        foreach($model->getAddedExcursions() as $modelExcursion)
        {
            if($modelExcursion->enabled)
                $excursionsTotal += $modelExcursion->getCost();
        }
        $_excursionsTable = '[%%]villa_homepage_excursions';
        $tpl->excursionsTotal = Module_Currency_Helper::convert($excursionsTotal);
        $excursInfo = array();
        $langId = Module_Lang_Helper::getCurrent();
        $sql = "SELECT `day`, `html`, `full_descr`, `title`,  `ex_show` FROM {$_excursionsTable} WHERE `lang_id` = {$langId}";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
            $excursInfo['info_list'][$f['day']] = $f;

        $tpl->excursInfo = $excursInfo;


        $tpl->payMessage = $this->payMessage;

        $endBtn = '';

        $buttons = $this->drawButtons($model->totalsData);
        $payAllBtn = $buttons['payAllBtn'];
        $payServicesBtn = $buttons['payServicesBtn'];
        $payResidenceBtn = $buttons['payResidenceBtn'];

        switch($this->_mode)
        {
            case BF_VIEW_MODE_NEW:
                switch($model->roomData->prepayment)
                {
                    case PREPAYMENT_TYPE_NONE:
                        $endBtn = '<button type="button" id="endBookingBtn" onclick="bookingform.saveBooking('.$model->id.')" '.
                                            'class="green endBookingButton">'.
                                        '<span>'.Module_Lang_Helper::translate('endBooking').'</span>'.
                                    '</button>';
                        $payAllBtn = '';
                        $payServicesBtn = '';
                        $payResidenceBtn = '';
                        break;
                    default:
                        $this->_paysystem->prepaymentId = $model->roomData->prepayment;
                        $endBtn = $this->_paysystem->getEndBookingBtn($model->totalsData->dispPrepaymentValue());
                        break;
                }
                break;

            case BF_VIEW_MODE_AFTER_PAYMENT:
                break;
            case BF_VIEW_MODE_EDIT:
                $endBtn = '<button type="button" id="endBookingBtn" onclick="bookingform.saveBooking('.$model->id.')" '.
                                    'class="green endBookingButton">'.
                                '<span>'.Module_Lang_Helper::translate('saveAndExit').'</span>'.
                            '</button>';
                break;
        }


        $tpl->endBookingBtn = $endBtn;
        $tpl->payAllBtn = $payAllBtn;
        $tpl->payServicesBtn = $payServicesBtn;
        $tpl->payResidenceBtn = $payResidenceBtn;
        $tpl->editMode = $this->_mode == BF_VIEW_MODE_EDIT || $this->_mode == BF_VIEW_MODE_AFTER_PAYMENT ? 1 : 0;

        if(Villa_Module_Hotel_Helper_Session::getNewBookingAfterPayment())
        {
            $tpl->showSuccessMsg = true;
            Villa_Module_Hotel_Helper_Session::unsetNewBookingAfterPayment();
        }

        $tpl->gallery = $this->gallery;
        $tpl->bottomText = $this->bottomText;

        $tpl->isTourAgent = Villa_Module_User_Helper::checkTourAgent();

        return $tpl->draw($tplFileName);
    }

    public function drawButtons(Villa_Module_Hotel_Model_Booking_Totals $totals, $btnFunc = PAYOPT_SAVE_BOOKING)
    {
        $buttons = array(
            'payAllBtn' => '',
            'payServicesBtn' => '',
            'payResidenceBtn' => ''
        );

        if($totals->dispTotalPayable() == 0) return $buttons;

        $buttons['payAllBtn'] = $this->_paysystem->getPayBtn(
                PAYMENT_SUBJECT_ALL,
                $totals->dispTotalPayable(),
                $btnFunc
            );

        if($totals->dispOptionsPayable() != 0)
        {
            $buttons['payServicesBtn'] = $this->_paysystem->getPayBtn(
                    PAYMENT_SUBJECT_SERVICE,
                    $totals->dispOptionsPayable(),
                    $btnFunc
                );
        }

        if($totals->dispResidencePayable() != 0)
        {
            $buttons['payResidenceBtn'] = $this->_paysystem->getPayBtn(
                    PAYMENT_SUBJECT_RESIDENCE,
                    $totals->dispResidencePayable(),
                    $btnFunc
                );
        }

        return $buttons;
    }

    /**
     * Генерирует селект для выбора кол-ва человек для опции бронирования
     * @param type $adults - макс. кол-во взрослых
     * @param type $children - макс. кол-во детей
     * @param type $selectedAduts - выбранное кол-во взрослых
     * @param type $selectedChildren - выбранное кол-во детей
     * @param type $inputNameBase - базовое имя элементов input (например services[1])
     * @param boolean $condensed - если true - селект будет уменьшен по ширине
     * @return string
     */
    protected function _drawOptionGuestSelect($adults, $children, $selectedAduts, $selectedChildren, $inputNameBase, $condensed = false)
    {
        $html = '<div class="guest-num-select'.($condensed ? ' condensed' : '').'">
                <button><span class="selected" title="Кол-во человек">';

        $items = array();
        $adultItems = array();
        $childItems = array();

        while($adults > 0)
        {
            $adultItem = array();

            for($i = $adults; $i >= 1; $i--)
            {
                $adultItem[] = '<span class="adult"></span>';
            }

            $adultItems[] = $adultItem;
            $adults--;
        }

        while($children > 0)
        {
            $childItem = array();

            for($j = $children; $j >= 1; $j--)
            {
                $childItem[] = '<span class="child"></span>';
            }

            $childItems[] = $childItem;
            $children--;
        }

        $selectedItem = false;

        foreach($adultItems as $adultItem)
        {
            foreach($childItems as $childItem)
            {
                $item = implode('', $adultItem).implode('', $childItem);
                $items[] = $item;

                if(count($adultItem) == $selectedAduts && count($childItem) == $selectedChildren)
                    $selectedItem = $item;
            }
            $item = implode('', $adultItem);
            $items[] = $item;

            if(count($adultItem) == $selectedAduts && $selectedChildren == 0)
                $selectedItem = $item;
        }

        if(!$selectedItem) $selectedItem = $items[0];

        $html .= $selectedItem.'</span></button>';

        $html .= '<ul>';
        foreach($items as $item)
        {
            $html .= '<li>'.$item.'</li>';
        }
        $html .= '</ul>';

        $html .= '<input type="hidden" class="guest-select-adults" name="'.$inputNameBase.'[adults]" value="'.$selectedAduts.'"/>
                <input type="hidden" class="guest-select-children" name="'.$inputNameBase.'[children]" value="'.$selectedChildren.'"/>';

        $html .= '</div>';

        return $html;
    }
}

?>
