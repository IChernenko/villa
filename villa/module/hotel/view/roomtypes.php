<?php

class Villa_Module_Hotel_View_Roomtypes
{
    public function draw($params)
    {
        $tplFileName = '../villa/module/hotel/template/room_types.html';
        $tpl = new Dante_Lib_Template();
        $tpl->items = $params['items'];
        $tpl->prepayment = $params['prepayment'];
        $tpl->currency = $params['currency'];  
        $tpl->curPrices = $params['curPrices'];
        $tpl->prices = $params['prices'];
        $tpl->infoblock = $params['infoblock'];
        
        return $tpl->draw($tplFileName);
    }
    
    public function drawEquipment($equipList)
    {
        $html = '<p class="strong"></p>';
        
        if(!$equipList || !count($equipList))
            return $html.'<p class="error">Оборудование не задано</p>';
        
        $html .= '<ul>';
        
        foreach($equipList as $equipModel)
        {
            $style = '';
            
            if($equipModel->font_size)
                $style .= "font-size: {$equipModel->font_size}px;";
            if($equipModel->bold)
                $style .= 'font-weight: bold;';
            
            $html .= '<li style="'.$style.'"> <i class="fa fa-check"></i>'.$equipModel->name.'</li>';
        }
        
        $html .='</ul>';
        return $html;
    }
}

?>