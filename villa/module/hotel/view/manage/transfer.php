<?php
/**
 * Description of transfer
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_View_Manage_Transfer 
{
    public $types;
    
    public $transport;
    
    public $langList;

    public $transferId;
    
    public function draw($model)
    {
        $tplFileName = '../villa/module/hotel/template/manage/transfer_form.html';
        
        $tpl = new Dante_Lib_Template();
        $tpl->model = $model;
        $tpl->types = $this->types;
        $tpl->transport = $this->transport;
        $tpl->langList = $this->langList;       

        return $tpl->draw($tplFileName);
    }

    public function drawImages($images)
    {
        $tplFileName = '../villa/module/hotel/template/manage/transfer_img.html';
        $tpl = new Dante_Lib_Template();
        $tpl->transferId = $this->transferId;
        $tpl->images = $images;

        return $tpl->draw($tplFileName);
    }
}

?>
