<?php
/**
 * Description of bookingoptions
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_View_Manage_Services 
{
    public $langList = array();
    public $serviceId;
    
    public function drawForm($model)
    {
        $tplFileName = '../villa/module/hotel/template/manage/service_form.html';       
        
        $tpl = new Dante_Lib_Template();
        $tpl->langList = $this->langList;
        $tpl->model = $model;
        
        return $tpl->draw($tplFileName);
    }

    public function drawImages($images)
    {
        $tplFileName = '../villa/module/hotel/template/manage/service_form_img.html';
        $tpl = new Dante_Lib_Template();
        $tpl->serviceId = $this->serviceId;
        $tpl->images = $images;

        return $tpl->draw($tplFileName);
    }
}

?>
