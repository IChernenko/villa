<?php
/**
 * Description of prepayment
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_View_Manage_Prepayment 
{
    public $langList;
    
    public function drawForm($model)
    {
        $tplName = '../villa/module/hotel/template/manage/prepayment.html';
        $tpl = new Dante_Lib_Template();
        
        $tpl->model = $model;
        $tpl->langList = $this->langList;
        
        return $tpl->draw($tplName);
    }
}

?>
