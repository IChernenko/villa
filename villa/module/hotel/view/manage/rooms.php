<?php

/**
 * Description of rooms
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_View_Manage_Rooms 
{
    public $typesList;
    
    public function drawForm($model)
    {
        $tplFileName = '../villa/module/hotel/template/manage/rooms_form.html';       
        
        $tpl = new Dante_Lib_Template();
        $tpl->typesList = $this->typesList;
        $tpl->model = $model;
        
        return $tpl->draw($tplFileName);
    }
}

?>
