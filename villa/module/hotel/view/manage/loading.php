<?php
/**
 * Description of view
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_View_Manage_Loading 
{
    public $dateFrom;
    public $dateTo;
    public $services;
    public $selectedService;
    
    public $roomList = array();
    public $dates = array();


    
    public function draw($bookings)
    {
        $totalArray = array();
        $table = array();

        foreach($this->roomList as $num)
        {
            $curRoom = isset($bookings[$num]) ? $bookings[$num] : false;
            foreach($this->dates as $date)
            {
                $cellItems = array();

                if(is_array($curRoom))
                {
                    foreach($curRoom as $curBooking)
                    {
                        if($curBooking['arrival'] <= $date && $curBooking['departure'] >= $date)
                        {
                            $cellClasses = array();
                            $transfer = false;

                            if($curBooking['arrival'] == $date)
                            {
                                $cellClasses[] = 'arrival-cell';
                                $transfer = $curBooking['transfer'] &&
                                            isset($curBooking['transfer']['arrival']) &&
                                            $curBooking['status'] == BOOKING_STATUS_ARRIVAL ?
                                            $curBooking['transfer']['arrival'] : false;
                            }

                            if($curBooking['departure'] == $date)
                            {
                                $cellClasses[] = 'departure-cell';
                                $transfer = $curBooking['transfer'] &&
                                            isset($curBooking['transfer']['departure']) &&
                                            $curBooking['status'] == BOOKING_STATUS_DEPARTURE ?
                                            $curBooking['transfer']['departure'] : false;
                            }

                            $cellClasses[] = Villa_Module_Hotel_Helper_Manage_Bookingstatus::getStatusClass($curBooking['status']);

                            if($transfer)
                            {
                                switch($transfer['transport_type'])
                                {
                                    case TRANSPORT_PLANE:
                                        $transfer['info'] = '<b>Рейс: </b>'.$transfer['race'].'<br/>';
                                        break;

                                    case TRANSPORT_TRAIN:
                                        $transfer['info'] = '<b>Поезд: </b>'.$transfer['train'].'<br/>'.
                                                            '<b>Вагон: </b>'.$transfer['wagon'].'<br/>';
                                        break;
                                }
                            }

                            $cellItems[] = array(
                                'class' => implode(' ', $cellClasses),
                                'transfer' => $transfer,
                                'info' => $curBooking
                            );
                        }
                    }
                }

                $table[$num][$date] = $cellItems;
            }
        }


        $new_bookings = array();
        $serviceCount = $this->serviceCount;
        $serviceChild = $this->serviceChild;

        $room_prices_sql = "select * from tst_hotel_booking";

        $r = Dante_Lib_SQL_DB::get_instance()->open($room_prices_sql);
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $id = intval($f['id']);
            $new_bookings[$id]['room'] = $f['room'];
            $new_bookings[$id]['arrival'] = $f['arrival'];
            $new_bookings[$id]['departure'] = $f['departure'];
            $new_bookings[$id]['id'] = $f['id'];
            $new_bookings[$id]['days'] = $f['days'];
//            if ( isset($serviceCount[$id][$this->selectedService]) ) {
//                $new_bookings[$id]['serviceCount'] = $serviceCount[$id][$this->selectedService];
//            }

        }

        $test = array();

        foreach ( $this->dates as $date ) {
            $totalCount = 0;
            $totalCountChild = 0;
            $newDate = Dante_Helper_Date::converToDateType($date, 0, 'd/m');
            foreach ( $new_bookings as $new_booking ) {
                $bokingDays =  intval($new_booking['days']) * 86430;
                if ( $new_booking['arrival'] <= $date && ( $bokingDays + $new_booking['arrival'] ) >= $date ) {
                    array_push($test, Dante_Helper_Date::converToDateType($new_booking['arrival'], 0, 'd/m'));
                    if ( isset($serviceCount[$new_booking['id']][$this->selectedService]) ) {
                        $totalCount += $serviceCount[$new_booking['id']][$this->selectedService];

                    }
                    if ( isset($serviceChild[$new_booking['id']][$this->selectedService]) ) {
                        $totalCountChild += $serviceChild[$new_booking['id']][$this->selectedService];

                    }
                }
            }
//            if ( $totalCount != 0 ) {
                $totalArray[$newDate]['adults'] = $totalCount;
//            }
//            if ( $totalCount != 0 ) {
                $totalArray[$newDate]['child'] = $totalCountChild;
//            }


        }

        foreach($this->dates as &$date)
            $date = Dante_Helper_Date::converToDateType($date, 0, 'd/m');

        $tpl = new Dante_Lib_Template();
        $tplName = '../villa/module/hotel/template/manage/loading.html';

        $tpl->dateFrom = $this->dateFrom;
        $tpl->dateTo = $this->dateTo;
        $tpl->table = $table;
        $tpl->roomList = $this->roomList;
        $tpl->dates = $this->dates;
        $tpl->services = $this->services;
        $tpl->selectedService = $this->selectedService;
        $tpl->serviceCount = $this->serviceCount;
        $tpl->serviceChild = $this->serviceChild;
        $tpl->payRemain = $this->payRemain;
        $tpl->totalService = $totalArray;
        $tpl->test = $test;



        return $tpl->draw($tplName);
    }
}

?>
