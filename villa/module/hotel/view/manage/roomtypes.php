<?php

/**
 * Description of roomtypes
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_View_Manage_Roomtypes 
{
    public $langList;
    public $typeId;
    
    public function drawForm($model)
    {
        $tplFileName = '../villa/module/hotel/template/manage/room_types.html';       
        
        $tpl = new Dante_Lib_Template();
        $tpl->langList = $this->langList;
        $tpl->model = $model;
        
        return $tpl->draw($tplFileName);
    }
    
    public function drawImages($images)
    {        
        $tplFileName = '../villa/module/hotel/template/manage/room_types_img.html';       
        
        $tpl = new Dante_Lib_Template();
        $tpl->typeId = $this->typeId;
        $tpl->images = $images;
        
        return $tpl->draw($tplFileName);
    }
    
    public function drawEquipment($defEquip, $aptEquip)
    {        
        $tplFileName = '../villa/module/hotel/template/manage/room_types_equip.html';       
        
        $tpl = new Dante_Lib_Template();
        $tpl->aptEquip = $aptEquip;
        $tpl->defEquip = $defEquip;
        
        return $tpl->draw($tplFileName);
    }
}

?>
