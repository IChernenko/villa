<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of equipment
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_View_Manage_Equipment
{
    public $langList;
    
    public function drawForm($model)
    {
        $tplName = '../villa/module/hotel/template/manage/equipment.html';
        $tpl = new Dante_Lib_Template();
        
        $tpl->model = $model;
        $tpl->langList = $this->langList;
        
        return $tpl->draw($tplName);
    }
}
?>
