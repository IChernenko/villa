<?php
/**
 * Description of bookingtransfer
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_View_Manage_Bookingtransfer 
{
    public function drawForm($model)
    {
        $tplName = '../villa/module/hotel/template/manage/booking_transfer.html';
        $tpl = new Dante_Lib_Template();
        
        $tpl->model = $model;
        $tpl->statusList = Villa_Module_Hotel_Helper_Manage_Booking::getTransferStatuses();
        
        return $tpl->draw($tplName);
    }
}
