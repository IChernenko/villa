<?php

/**
 * Description of prices
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_View_Manage_Prices 
{
    public $prepTypes;
    public $roomTypes;
    public $servicesList;
    public $transferList;
    public $daysList;
    
    public function drawPrices(Villa_Module_Hotel_Model_Manage_Priceslist $listModel)
    {
        $tplName = '../villa/module/hotel/template/manage/prices.html';
        $tpl = new Dante_Lib_Template();
        
        $entityRoomType = Villa_Helper_Entity::getRoomType();
        $entityService = Villa_Helper_Entity::getService();
        $entityTransfer = Villa_Helper_Entity::getTransfer();
        $entityExcurs = Villa_Helper_Entity::getExcursion();
        
        // Подготовка данных для отображения
        $fullPriceList = array();
        
        //Отдельные блоки для типов номеров (знаю, мозг сломать можно, а что делать?)
        foreach($this->roomTypes as $roomType => $roomName)
        {               
            $block = array(
                'caption' => $roomName,
                'items' => array()
            );
            foreach($this->prepTypes as $typeId => $typeName)
            {
                if(isset($listModel->roomTypesPrices[$roomType][$typeId]))
                {
                    $item = $listModel->roomTypesPrices[$roomType][$typeId];
                    $item->title = $typeName;
                    $block['items'][] = $item;
                }
                else
                {
                    $item = new Villa_Module_Hotel_Model_Manage_Prices();
                    $item->entity_type_id = $entityRoomType;
                    $item->entity_id = $roomType;
                    $item->prep_type = $typeId;
                    $item->title = $typeName;
                    $block['items'][] = $item;
                }
            }
            
            $fullPriceList[] = $block;
        }
        
        // И стандартные блоки для услуг и трансферов (уфф, ну тут попроще)
        $fullPriceList[] = $this->_getBlock('Услуги', $this->servicesList, $listModel->servicesPrices, $entityService);
        $fullPriceList[] = $this->_getBlock('Трансферы', $this->transferList, $listModel->transferPrices, $entityTransfer);
        $fullPriceList[] = $this->_getBlock('Экскурсии', $this->daysList, $listModel->excursionPrices, $entityExcurs);
        
        $tpl->list = $fullPriceList;
        
        return $tpl->draw($tplName);
    }
    
    /**
     * Получение блока для отрисовки таблицы.
     * @param type $title - название блока
     * @param type $srcList - список сущностей для установки цен (напр. список услуг, 
     *                          для типов номеров - список типов предоплат).
     *                          Структура списка: $id => $name
     * @param type $priceList - список, из которого берутся цены для кажой отдельной сущности
     * @param type $entityType - тип сущности по умолчанию (если нет записи в $priceList)
     * @return type - на выходе блок с ключами caption и items. items - список строк для будущей таблицы
     */
    protected function _getBlock($title, $srcList, $priceList, $entityType)
    {
        $block = array(
            'caption' => $title,
            'items' => array()
        );
        
        foreach($srcList as $id => $name)
        {
            if(isset($priceList[$id]))
            {
                $item = $priceList[$id];
                $item->title = $name;
                $block['items'][] = $item;
            }
            else
            {
                $item = new Villa_Module_Hotel_Model_Manage_Prices();
                $item->entity_type_id = $entityType;
                $item->entity_id = $id;
                $item->title = $name;
                $block['items'][] = $item;
            }
        }
        
        return $block;
    }
    
    public function drawDates($model, $dates)
    {
        $tplName = '../villa/module/hotel/template/manage/prices_dates.html';
        $tpl = new Dante_Lib_Template();
        
        foreach($dates as &$date)
        {
            $date->dateFrom = Dante_Helper_Date::converToDateType($date->dateFrom, 0);
            $date->dateTo = Dante_Helper_Date::converToDateType($date->dateTo, 0);
        }
        
        $tpl->model = $model;
        $tpl->dates = $dates;
        
        return $tpl->draw($tplName);
    }
}

?>
