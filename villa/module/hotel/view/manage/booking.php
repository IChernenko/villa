<?php
/**
 * View формы бронирования
 * User: dorian
 * Date: 30.05.12
 * Time: 13:44
 * To change this template use File | Settings | File Templates.
 */
class Villa_Module_Hotel_View_Manage_Booking
{    
    public $currencyCode;
    public $roomTypes;
    public $prepaymentTypes;
    
    public $roomTypeName;
    public $prepaymentName;
    public $userCurrencyCode;
    
    public $services;
    public $excursions;
    public $transfers;
    
    public $roomList;
    public $statusList;
    
    public $userList;
    
    public function drawForm(Villa_Module_Hotel_Model_Manage_Booking $model, $versionMode = false) 
    {
        $tplFileName = '../villa/module/hotel/template/manage/booking/form.html';
        $btnTplName = '../villa/module/hotel/template/manage/booking/form_buttons.html';
        
        $tpl = new Dante_Lib_Template();
        $tpl->ccy = $this->currencyCode;
        
        $tpl->roomList = $this->roomList;
        $tpl->statusList = $this->statusList;

        $tpl->services = $this->services;
        $tpl->excursions = $this->excursions;
        $tpl->transfers = $this->transfers;

        $tpl->roomTypeName = $this->roomTypeName;
        $tpl->prepaymentName = $this->prepaymentName;
        $tpl->userCurrency = $this->userCurrencyCode;
        $tpl->version_time = Dante_Helper_Date::converToDateType($model->modified, 1);
        
        $model->creation_time = Dante_Helper_Date::converToDateType($model->creation_time, 0);
        $model->modified = Dante_Helper_Date::converToDateType($model->modified, 0);
        $tpl->model = $model;
        
        $tpl->versionMode = $versionMode;
        
        $btnTpl = new Dante_Lib_Template();
        $tpl->formButtons = $btnTpl->draw($btnTplName);
                
        return $tpl->draw($tplFileName);
    }
    
    public function drawAddForm($maxAdults, $maxChildren)
    {
        $tplFileName = '../villa/module/hotel/template/manage/booking/add_form.html';
        
        $tpl = new Dante_Lib_Template();
        $tpl->ccy = $this->currencyCode;
        
        $tpl->maxAdults = $maxAdults;
        $tpl->maxChildren = $maxChildren;
        $tpl->prepaymentTypes = $this->prepaymentTypes;
        
        return $tpl->draw($tplFileName);
    }
    
    public function drawRoomTypesSelect($items)
    {
        $html = '<select name="room_type" class="room-data" style="width: 99%">
                <option value="">-- Выберите --</option>';
        
        foreach($items as $id => $name)
        {
            $html .= '<option value="'.$id.'">'.$name.'</option>';
        }
        
        $html .= '</select>';
        
        return $html;
    }
    
    public function drawScanForm($bookingId, $guestNum, $scan)
    {
        $tplFileName = '../villa/module/hotel/template/manage/booking/scan_form.html';
        
        $tpl = new Dante_Lib_Template();
        if($scan)
            $tpl->scanPath = Villa_Module_User_Helper::getPassportScanDir().$scan;
        else
            $tpl->scanPath = Lib_Image_Helper::getNoImage();
        
        $tpl->bookingId = $bookingId;
        $tpl->guestNum = $guestNum;
        
        return $tpl->draw($tplFileName);
    }
    
    public function drawPaymentForm($bookingId, $type, $totals)
    {
        $tplFileName = '../villa/module/hotel/template/manage/booking/payment_form.html';
        
        $tpl = new Dante_Lib_Template();
        $tpl->ccy = $this->currencyCode;
        switch($type)
        {
            case PAYMENT_TYPE_INCOME: 
                $tpl->tableCaption = 'Внесение оплаты';
                $tpl->residenceMax = Module_Currency_Helper::convert($totals['residencePayable']);
                $tpl->serviceMax = Module_Currency_Helper::convert($totals['servicePayable']);
                break;
            
            case PAYMENT_TYPE_REFUND:
                $tpl->tableCaption = 'Возврат оплаты';
                $tpl->residenceMax = Module_Currency_Helper::convert($totals['residencePaid']);
                $tpl->serviceMax = Module_Currency_Helper::convert($totals['servicePaid']);
                break;
        }
        
        $tpl->bookingId = $bookingId;
        $tpl->type = $type;
        $tpl->payWays = Villa_Module_Hotel_Helper_Manage_Payment::getPayWays();
        $tpl->subjects = Villa_Module_Hotel_Helper_Manage_Payment::getSubjects();
                
        return $tpl->draw($tplFileName);
    }
    
    public function drawPaymentHistory($bookingId, $list)
    {
        $tplFileName = '../villa/module/hotel/template/manage/booking/payment_history.html';
        
        $paymentTypes = Villa_Module_Hotel_Helper_Manage_Payment::getTypes();
        $payWays = Villa_Module_Hotel_Helper_Manage_Payment::getPayWays();
        $subjects = Villa_Module_Hotel_Helper_Manage_Payment::getSubjects();
        
        foreach($list as &$model)
        {
            $model->date = Dante_Helper_Date::converToDateType($model->date, 1);
            $model->user_email = $this->userList[$model->user_id];
            $model->pay_way = $payWays[$model->pay_way];
            $model->type = $paymentTypes[$model->type];
            $model->subject = $subjects[$model->subject];
            $model->amount = Module_Currency_Helper::convert($model->amount);
            
            if($model->payment_id && $model->pay_way == 'L')
            {
                $model->payment_id = '<a href="/manage/liqpay2/open-'.$model->payment_id.
                        '" target="_blank">'.$model->payment_id.'</a>';
            }
        }
        
        $tpl = new Dante_Lib_Template();
        $tpl->ccy = $this->currencyCode;
        $tpl->bookingId = $bookingId;
        $tpl->list = $list;
        
        return $tpl->draw($tplFileName);
    }
    
    public function drawVersions($list, $users)
    {
        $fileName = '../villa/module/hotel/template/manage/booking/versions.html';
        
        foreach($list as &$item)
        {
            $item['user'] = $users[$item['user']];
            $item['time'] = Dante_Helper_Date::converToDateType($item['time'], 1);
        }
        
        $tpl = new Dante_Lib_Template();
        $tpl->list = $list;
        
        return $tpl->draw($fileName);
    }
}
