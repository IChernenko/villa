<?php

/**
 * Description of roomsearch
 *
 * @author Valkyria
 */
class Villa_Module_Hotel_View_Roomsearch 
{
    /**
     *
     * @var Villa_Module_Hotel_Model_Booking_Basic 
     */
    public $basicData;
    public $roomData;
    public $maxAdults = 4;
    public $maxChildren = 2;
    
    public $items;
    public $prepaymentTypes;
    public $prices;
    public $currency;
    
    public function drawForm() 
    {  
        $tplFileName = '../villa/module/hotel/template/room_search_form.html';

        $tpl = new Dante_Lib_Template();
        $tpl->maxAdults = $this->maxAdults;
        $tpl->maxChildren = $this->maxChildren;
        $tpl->model = $this->basicData;
        return $tpl->draw($tplFileName);
    }
    
    public function drawResult()
    {        
        $tplFileName = '../villa/module/hotel/template/room_search_result.html';
        $tpl = new Dante_Lib_Template();
        $tpl->arriveDate = $this->basicData->arrival;
        $tpl->departureDate = $this->basicData->departure;
        $tpl->adults = $this->basicData->adults;
        $tpl->children = $this->basicData->children;
        
        $tpl->roomData = $this->roomData;
        
        $tpl->days = $this->days;
        $tpl->items = $this->items;
        $tpl->prepaymentTypes = $this->prepaymentTypes;
        $tpl->prices = $this->prices;
        $tpl->currency = $this->currency;
        
        return $tpl->draw($tplFileName);
    }
}

?>
