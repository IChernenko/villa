<?php
/**
 * View для отображения отдельного номера
 * User: dorian
 * Date: 27.05.12
 * Time: 12:18
 * To change this template use File | Settings | File Templates.
 */
class Villa_Module_Hotel_View_Appartment
{
    public $currencyName;
    
    public function draw(Villa_Module_Hotel_Model_Manage_Roomtypes $model) {
        $tplFileName = '../villa/module/hotel/template/appartment.html';

        /*$customTpl = Dante_Helper_App::getWsPath().'/shop/cart.html';
        if (file_exists($customTpl)) {
            $tplFileName = $customTpl;
        }*/

        $tpl = new Dante_Lib_Template();
        $tpl->model = $model;
        $tpl->currencyName = $this->currencyName;
        return $tpl->draw($tplFileName);
    }
}
