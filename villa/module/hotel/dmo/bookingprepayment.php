<?php

/**
 * Description of Bookingprepayment
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Villa_Module_Hotel_Dmo_Bookingprepayment extends Dante_Lib_Orm_Table{
    
    protected $_tableName = '[%%]hotel_booking_prepayment_types';
    
    /**
     *
     * @return Villa_Module_Hotel_Dmo_Roomtypes 
     */
    public static function dmo() {
        return self::getInstance();
    }
}

?>
