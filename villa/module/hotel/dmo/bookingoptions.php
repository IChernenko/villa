<?php



/**
 * Description of apartmentstypes
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Villa_Module_Hotel_Dmo_Bookingoptions extends Dante_Lib_Orm_Table{
    
    protected $_tableName = '[%%]hotel_services';
    
    /**
     *
     * @return Villa_Module_Hotel_Dmo_Roomtypes 
     */
    public static function dmo() {
        return self::getInstance();
    }
}

?>
