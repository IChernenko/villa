<?php



/**
 * Description of roomtypes
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Villa_Module_Hotel_Dmo_Roomtypes extends Dante_Lib_Orm_Table{
    
    protected $_tableName = '[%%]hotel_room_types';
    
    /**
     *
     * @return Villa_Module_Hotel_Dmo_Roomtypes 
     */
    public static function dmo() {
        return self::getInstance();
    }
}

?>
