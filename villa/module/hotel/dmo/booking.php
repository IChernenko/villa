<?php



/**
 * Description of booking
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Villa_Module_Hotel_Dmo_Booking extends Dante_Lib_Orm_Table{
    
    protected $_tableName = '[%%]hotel_booking';
    
    /**
     *
     * @return Villa_Module_Hotel_Dmo_Booking 
     */
    public static function dmo() {
        return self::getInstance();
    }

    /**
     *
     * @param type $id
     * @return Villa_Module_Hotel_Dmo_Booking 
     */
    public function byId($id) {
        return $this->getByAttribute(array('id'=>$id));
    }
    
    /**
     *
     * @param type $uid
     * @return Villa_Module_Hotel_Dmo_Booking 
     */
    public function byUid($uid) {
        return $this->getByAttribute(array('uid'=>$uid));
    }
}

?>
