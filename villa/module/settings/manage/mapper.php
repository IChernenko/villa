<?php

/**
 * Description of mapper
 *
 * @author Valkyria
 */
class Villa_Module_Settings_Manage_Mapper 
{
    protected $_tableName = '[%%]villa_settings';
    
    public function getAll()
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->select(array('id' => 1));
        
        $model = new Villa_Module_Settings_Manage_Model();
        $model->discount_child = $table->discount_child;
        
        return $model;
    }
    
    public function save($model)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->discount_child = $model->discount_child;
        
        $table->update(array('id' => 1));
    }
    
    public function getSetting($name)
    {
        $sql = "SELECT `{$name}` FROM {$this->_tableName} WHERE `id` = 1";
        return Dante_Lib_SQL_DB::get_instance()->fetchField($sql, $name);
    }
}

?>
