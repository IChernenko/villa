<?php

/**
 * Description of controller
 *
 * @author Valkyria
 */
class Villa_Module_Settings_Manage_Controller extends Dante_Controller_Base
{
    protected $_mapper;
    
    protected function _init() 
    {
        parent::_init();
        $helper = new Module_Division_Helper();
        $helper->checkAccess();
        $this->_mapper = new Villa_Module_Settings_Manage_Mapper();
    }

    protected function _defaultAction() 
    {        
        $model = $this->_mapper->getAll();
        
        $view = new Villa_Module_Settings_Manage_View();
        return $view->draw($model);
    }
    
    protected function _saveAction()
    {
        $model = new Villa_Module_Settings_Manage_Model();
        $model->discount_child = $this->_getRequest()->get_validate('discount_child', 'int', 0);
        
        $this->_mapper->save($model);
        return array(
            'result' => 1
        );
    }
}

?>
