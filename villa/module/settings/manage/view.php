<?php

/**
 * Description of view
 *
 * @author Valkyria
 */
class Villa_Module_Settings_Manage_View 
{
    public function draw($model)
    {
        $tplFileName = '../villa/module/settings/manage/template/table.html';
        $tpl = new Dante_Lib_Template();
        
        $tpl->model = $model;
        
        return $tpl->draw($tplFileName);
    }
}

?>
