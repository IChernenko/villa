<?php

/**
 * Description of helper
 *
 * @author Valkyria
 */
class Villa_Module_Settings_Helper
{
    private static $_discount;

    public static function getDiscountForChildren()
    {
        if(!self::$_discount)
        {
            $mapper = new Villa_Module_Settings_Manage_Mapper();
            self::$_discount = Villa_Module_Hotel_Helper_Discount::getDiscountFactor($mapper->getSetting('discount_child'));
        }
        
        return self::$_discount;
    }
}

?>
