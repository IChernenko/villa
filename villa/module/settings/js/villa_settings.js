villa_settings = {    
    save: function(obj)
    {
        if($(obj).hasClass('disabled')) return false;
        $('#settingsForm a').addClass('disabled');
        
        var data = $('#settingsForm').serializeArray();
        
        $.ajax({
            type: 'POST',
            data: data,
            url: '/ajax.php',
            dataType : "json",
            success: function (data){
                if(data.result==1)
                {
                    jAlert('Изменения сохранены', 'Сообщение'); 
                    $('#settingsForm a').removeClass('disabled');
                }
                else  console.log(data);
            }
        });
    }
}


