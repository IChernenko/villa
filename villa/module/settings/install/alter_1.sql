CREATE TABLE `[%%]villa_settings` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `discount_child` INT(11) NOT NULL DEFAULT 0,
    PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8  AUTO_INCREMENT=1;

INSERT INTO `[%%]villa_settings` SET `id` = 1, `discount_child` = 50;