about =
{
    setMetaSeo: function(seoTitle, seoDescription, seoKeywords)
    {
        jQuery('meta[name=title]').attr('content', seoTitle);
        jQuery('meta[name=description]').attr('content', seoDescription);
        jQuery('meta[name=keywords]').attr('content', seoKeywords);
    },

    viewImages: function(event)
    {
        var imgObjects = $(event.target).parents('ul').find('img');
        var location = $(event.target).parents('#aboutHotel');
        var activeIndex = $(imgObjects).index(event.target);

        var params = [];
        params['images'] = [];

        $(imgObjects).each(function(){
            params['images'].push($(this).attr('src'));
        });

        showViewer(params, location, activeIndex);
    }
}