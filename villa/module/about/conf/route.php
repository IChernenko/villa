<?php
/**
 * Правило маршрутизации для url
 * страницы "Об Отеле" в клиентской части сайта.
 * (Модуль /villa/module/about/)
 */
Dante_Lib_Router::addRule('/^(\/[a-z]{2,3})?\/about\-page\/$/', array(
    'controller' => 'Villa_Module_About_Controller'
));
?>
