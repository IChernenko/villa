<?php
/**
 * Class Villa_Module_About_Mapper
 * Маппер модуля About (для работы с базой данных)
 */

class Villa_Module_About_Mapper
{
    protected $_tableName = '[%%]villa_about_page';
    protected $_galleryTableName = '[%%]gallery';
    protected $_servicesTableName = '[%%]villa_about_page_services';
    protected $_imagesTableName = '[%%]villa_about_page_images';

    /**
     * Функция, получающая запись из таблицы $_tableName
     * @param $langId - ID языка
     * @return array - данные записи
     */
    public function getEntry($langId)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $pageInfo = array();
        $pageInfo['title'] = $table->select(array('lang_id' => $langId))->title;
        $pageInfo['html'] = $table->select(array('lang_id' => $langId))->html;
        $pageInfo['footerHtml'] = $table->select(array('lang_id' => $langId))->footer_html;
        $pageInfo['images'] = $table->select(array('lang_id' => $langId))->images;
        $pageInfo['seoTitle'] = $table->select(array('lang_id' => $langId))->seo_title;
        $pageInfo['seoDescription'] = $table->select(array('lang_id' => $langId))->seo_description;
        $pageInfo['seoKeywords'] = $table->select(array('lang_id' => $langId))->seo_keywords;
        return $pageInfo;
    }


    public function getNumberSeo($lang) {
        $table = new Dante_Lib_Orm_Table('tst_room_types_seo');
        $table->select(array('lang_id' => $lang));

        return $table;
    }
    /**
     * Функция, получающая изображения из таблицы $_galleryTableName
     */

    public function getImages()
    {
        /*$table = new Dante_Lib_Orm_Table($this->_galleryTableName);
        $pageGallery = $table->fetchAll('id');
        return $pageGallery;*/

        /*$table = new Dante_Lib_Orm_Table($this->_imagesTableName);
        $pageGallery = $table->fetchAll('id');
        return $pageGallery;*/

        $sql = "SELECT * FROM ".$this->_imagesTableName." ORDER BY draw_order";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);

        $pageGallery = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            $pageGallery[] = array(
                'id' => $f['id'],
                'image' => $f['image'],
                'draw_order' => $f['draw_order']
            );
        }

        return $pageGallery;
    }

    /**
     * Функция, получающая услуги из таблицы $_servicesTableName
     */

    public function getServices($lang)
    {
        $sql = "SELECT * FROM {$this->_servicesTableName} 
                WHERE `lang_id` = {$lang} ORDER BY draw_order";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);

        $servicesList = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $servicesList[] = array(
                'id' => $f['id'],
                'lang_id' => $f['lang_id'],
                'title' => $f['title'],
                'description' => $f['description'],
                'draw_order' => $f['draw_order']
            );
        }
        return $servicesList;
    }
}