<?php
/**
 * Главный контроллер модуля About (/villa/module/about/)
 */
class Villa_Module_About_Controller extends Dante_Controller_Base
{
    /**
    * @var $_mapper - маппер модуля About
    */
    protected $_mapper;

    /**
    * Инициализация переменных класса контроллера
    */
    protected function _init()
    {
        parent::_init();
        $this->_mapper = new Villa_Module_About_Mapper();
    }

    /**
    * Action выполняющийся по умолчанию
    */
    protected function _defaultAction()
    {
        // Получение данных страницы с учетом текущей языковой версии $lang
        $pageData = $this->_mapper->getEntry(Module_Lang_Helper::getCurrent());

        // Получение изображений для галереи изображений
        $pageGallery = $this->_mapper->getImages();
        $imgDir = Villa_Module_Hotel_Helper_Media::getAboutPageDir();
        foreach($pageGallery as &$item)
        {
            $item['image'] = $imgDir.$item['image'];
            unset($item);
        }

        // Получение услуг
        $servicesList = $this->_mapper->getServices(Module_Lang_Helper::getCurrent());

        /**
            * Создание представления (view) для
            * страницы "Об Отеле" в клиентской части
            */
        $view = new Villa_Module_About_View();
        $view->gallery = $pageGallery;
        $view->services = $servicesList;

        return $view->drawPage($pageData);
    }
}
?>