<?php
/**
 * Модуль, реализующий вывод страницы "Об Отеле"
 * в клиентской части сайта
 * Path: /villa/module/about/
 * Name: villa.module.about
 * Current version: 1
 */
global $package;
$package = array(
    'version' => '1',
    'name' => 'villa.module.about',
    'dependence' => array(),
    'js' => array(
        '../villa/module/about/js/about.js'
    )
);
?>