<?php
/**
 * Class Villa_Module_About_View
 * Класс представления модуля About
 */

class Villa_Module_About_View
{
    public $gallery;
    public $services;

    public function drawPage($pageData)
    {
        $tpl = new Dante_Lib_Template();
        $tplFileName = '../villa/module/about/template/about-page.html';

        $tpl->pageData = $pageData;
        $tpl->gallery = $this->gallery;
        $tpl->services = $this->services;


        return $tpl->draw($tplFileName);

    }
}