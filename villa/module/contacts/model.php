<?php
/**
 * Description of model
 *
 * @author Valkyria
 */
class Villa_Module_Contacts_Model 
{
   public $lang_id;
   
   public $title;
   
   public $contacts;
   
   public $map_html; 
   
   public $seo_title;
   
   public $seo_keywords;
   
   public $seo_description;
}

?>
