<?php

/**
 * Description of controller
 *
 * @author Valkyria
 */
class Villa_Module_Contacts_Controller extends Dante_Controller_Base
{
    protected function _defaultAction() 
    {
        $mapper = new Villa_Module_Contacts_Mapper();
        $model = $mapper->get(Module_Lang_Helper::getCurrent());
        
        $view = new Villa_Module_Contacts_View();
        
        return $view->draw($model);
    }
}

?>
