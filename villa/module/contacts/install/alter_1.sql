CREATE TABLE `[%%]villa_contacts` (
    `lang_id` INT(11) UNSIGNED NOT NULL,
    `title` VARCHAR(100) NOT NULL,
    `country` VARCHAR(50) NOT NULL,
    `city` VARCHAR(50) NOT NULL,
    `address` VARCHAR(100) NOT NULL,
    `map_html` VARCHAR(500) DEFAULT NULL,   
    `seo_title` TEXT,
    `seo_keywords` TEXT,
    `seo_description` TEXT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `[%%]villa_contacts` ADD CONSTRAINT `fk_villa_contacts_lang_id`
    FOREIGN KEY (`lang_id`) REFERENCES `[%%]languages`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;

INSERT INTO `[%%]villa_contacts` SET `lang_id` = 1, `title` = 'Контакты',
    `country` = 'Украина', `city` = 'город Киев', `address` = 'просп. Отрадный, дом 7',
    `map_html` = '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1270.614152541083!2d30.436243!3d50.436848!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xde05b4f6eed8fedd!2sHotel+Villa+LaScala!5e0!3m2!1sru!2sua!4v1413881278715" width="940" height="600" frameborder="0" style="border:0"></iframe>';