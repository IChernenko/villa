ALTER TABLE `[%%]villa_contacts` DROP COLUMN `country`;
ALTER TABLE `[%%]villa_contacts` DROP COLUMN `city`;
ALTER TABLE `[%%]villa_contacts` DROP COLUMN `address`;

ALTER TABLE `[%%]villa_contacts` ADD COLUMN `contacts` VARCHAR(500) NOT NULL AFTER `title`;

UPDATE `[%%]villa_contacts` SET `contacts` = 'Украина, город Киев, просп. Отрадный, дом 7. ' WHERE `lang_id` = 1;
