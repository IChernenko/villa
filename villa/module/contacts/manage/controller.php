<?php
/**
 * Description of controller
 *
 * @author Valkyria
 */
class Villa_Module_Contacts_Manage_Controller extends Dante_Controller_Base
{    
    protected function _init() 
    {
        parent::_init();
        $helper = new Module_Division_Helper();
        $helper->checkAccess();
    }


    protected function _instanceMapper()
    {
        return new Villa_Module_Contacts_Manage_Mapper();
    }
    protected function _instanceView()
    {
        return new Villa_Module_Contacts_Manage_View();
    }
    
    protected function _defaultAction() 
    {
        $lang = $this->_getRequest()->get_validate('lang_id', 'int', 1);
        
        $mapper = $this->_instanceMapper();
        $model = $mapper->get($lang);
        
        $view = $this->_instanceView();
               
        $langMapper = new Module_Lang_Mapper();
        
        $view->langList = $langMapper->getList();
        
        return $view->draw($model);
    }
    
    protected function _drawAjaxAction()
    {
        return array(
            'result' => 1,
            'html' => $this->_defaultAction()
        );
    }
    
    protected function _editAction()
    {
        $model = new Villa_Module_Contacts_Model();
        $request = $this->_getRequest();
        
        $model->lang_id = $request->get_validate('lang_id', 'int', 1);
        $model->title = $request->get_validate('title', 'text', '');
        $model->country = $request->get_validate('country', 'text', '');
        $model->city = $request->get_validate('city', 'text', '');
        $model->address = $request->get_validate('address', 'text', '');
//        $model->map_html = $request->get_validate('map_html', 'text', '');
        $model->seo_title = $request->get_validate('seo_title', 'text', '');
        $model->seo_keywords = $request->get_validate('seo_keywords', 'text', '');
        $model->seo_description = $request->get_validate('seo_description', 'text', '');
        $model->contacts = $request->get_validate('contacts', 'text', '');
        
        $this->_instanceMapper()->apply($model);
        
        return array(
            'result' => 1
        );
    }
}

?>
