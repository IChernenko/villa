<?php
/**
 * Description of view
 *
 * @author Valkyria
 */
class Villa_Module_Contacts_Manage_View 
{
    public $langList;
    
    public function draw($model)
    {
        $fileName = '../villa/module/contacts/manage/template/form.html';
        
        $tpl = new Dante_Lib_Template();
        $tpl->model = $model;
        $tpl->langList = $this->langList;
        
        return $tpl->draw($fileName);
    }
}

?>
