<?php
/**
 * Description of mapper
 *
 * @author Valkyria
 */
class Villa_Module_Contacts_Manage_Mapper extends Villa_Module_Contacts_Mapper
{
    public function apply(Villa_Module_Contacts_Model $model)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        
        $table->lang_id = $model->lang_id;
        $table->title = $model->title;
//        $table->map_html = $model->map_html;
        $table->seo_title = $model->seo_title;
        $table->seo_keywords = $model->seo_keywords;
        $table->seo_description = $model->seo_description;
        $table->contacts = $model->contacts;

        if($table->exist(array('lang_id' => $model->lang_id))) 
            $table->update(array('lang_id' => $model->lang_id));
        else
            $table->insert();
    }
}

?>
