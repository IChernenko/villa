<?php
/**
 * Description of view
 *
 * @author Valkyria
 */
class Villa_Module_Contacts_View 
{
    public function draw($model)
    {
        $fileName = '../villa/module/contacts/template/contacts.html';
        
        $tpl = new Dante_Lib_Template();
        $tpl->model = $model;
        
        return $tpl->draw($fileName);
    }
}

?>
