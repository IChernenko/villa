var VillaContacts = {
    controller: 'villa.module.contacts.manage.controller',
    changeLang: function(obj)
    {
        if(this.checkDisabled(obj)) return;

        this.disableBtn();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'ajax.php',
            data: {
                controller: VillaContacts.controller,
                action: 'drawAjax',
                lang_id: $(obj).val()
            },
            success: function(data){
                if(data.result == 1)
                    VillaContacts.replace(data.html);
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    },
    save: function(obj)
    {
        if(this.checkDisabled(obj)) return;

        this.disableBtn();

        var data = $('#contactsEditForm').serializeArray();
        data.push(
            {name: 'controller', value: VillaContacts.controller},
            {name: 'action', value: 'edit'}
        );

        data.forEach(function(item,i){
            if(item.name == "VillaContacts" || item.name == "contacts") {
                data[i].value = CKEDITOR.instances["contactsEditor"].getData();
            }

        });

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'ajax.php',
            data: data,
            success: function(data)
            {
                VillaContacts.enableBtn();

                if(data.result == 1)
                    jAlert('Изменения сохранены', 'Сообщение');
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    },
    replace: function(html)
    {
        $('#contactsEditForm').replaceWith(html);
    },
    disableBtn: function()
    {
        $('a.btn, select').addClass('disabled');
    },
    enableBtn: function()
    {
        $('a.btn, select').removeClass('disabled');
    },
    checkDisabled: function(obj)
    {
        if($(obj).hasClass('disabled')) return true;
        else return false;
    }
};


