<?php

Dante_Lib_Router::addRule('/^(\/[a-z]{2,3})?\/contacts\/$/', array(
    'controller' => 'Villa_Module_Contacts_Controller'
));

Dante_Lib_Router::addRule("/manage\/contacts\//", array(
    'controller' => 'Villa_Module_Contacts_Manage_Controller'
));

?>
