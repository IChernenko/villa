<?php
/**
 * Description of mapper
 *
 * @author Valkyria
 */
class Villa_Module_Contacts_Mapper 
{
    protected $_tableName = '[%%]villa_contacts';
    
    /**
     *
     * @param type $lang
     * @return \Villa_Module_Contacts_Model 
     */
    public function get($lang)
    {
        $model = new Villa_Module_Contacts_Model();
        
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->select(array('lang_id' => $lang));
        
        $model->lang_id = $lang;
        $model->title = $table->title;
        $model->contacts = $table->contacts;
        $model->map_html = $table->map_html;
        $model->seo_title = $table->seo_title;
        $model->seo_keywords = $table->seo_keywords;
        $model->seo_description = $table->seo_description;
        
        return $model;
    }
}

?>
