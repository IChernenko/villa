<?php
/**
 * Description of helper
 *
 * @author Valkyria
 */
class Villa_Module_Countries_Helper 
{
    public static function getPaymentSystemSelect()
    {
        return array(
            PAYMENT_SYSTEM_PAYPAL => 'PayPal',
            PAYMENT_SYSTEM_LIQPAY => 'LiqPay'
        );
    }
    
    protected static function _instanceMapper()
    {
        return new Villa_Module_Countries_Manage_Mapper();
    }
    
    protected static function _getCountryCode()
    {
//        try {
            $user_ip = getenv('REMOTE_ADDR');
//            if ( $geo = file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_ip") ) {
//                $geo = unserialize($geo);
//                $code = $geo["geoplugin_countryCode"];
//            } else {
//                return 'UA';
//            }
//
//        } catch(ErrorException $e) {
//            return 'UA';
//        }
//        return $code ? $code : 'UA';

//        $test = "http://www.geoplugin.net/php.gp?ip=$user_ip";

        $data = "http://www.geoplugin.net/php.gp?ip=$user_ip";
        $headers = get_headers($data);
        $responseCode = substr($headers[0], 9, 3);

        if( $responseCode != "200" ){
            return 'UA';
        } else{
            $geo = file_get_contents($data);
            $geo = unserialize($geo);
            $code = $geo["geoplugin_countryCode"];
            return $code ? $code : 'UA';
        }
    }

    public static function getPhone()
    {
        $code = self::_getCountryCode();
        return self::_instanceMapper()->getPhoneByCode($code);
    }
    
    public static function getCurrencyId()
    {
        $code = self::_getCountryCode();
        return self::_instanceMapper()->getCurrencyByCode($code);
    }

    public static function getPaymentSystem()
    {
        $code = self::_getCountryCode();
        return self::_instanceMapper()->getPaySystemByCode($code);
    }
}

?>
