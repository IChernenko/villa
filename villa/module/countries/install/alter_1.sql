ALTER TABLE `[%%]currencycountries` ADD `phone` VARCHAR(100) DEFAULT NULL;
ALTER TABLE `[%%]currencycountries` ADD `pay_system` TINYINT(1) NOT NULL DEFAULT 1;

UPDATE `[%%]division` SET `link` = '/manage/countries/' WHERE `link` = '/manage/currencycountries/';