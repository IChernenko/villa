<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of mapper
 *
 * @author Valkyria
 */
class Villa_Module_Countries_Manage_Mapper extends Module_Currencycountries_Manage_Mapper
{
    protected function _instanceModel() 
    {
        return new Villa_Module_Countries_Manage_Model();
    }
    
    protected function _arrayToModel($f) 
    {
        $model = parent::_arrayToModel($f);
        $model->phone = $f['phone'];
        $model->pay_system = $f['pay_system'];
        
        return $model;
    }
    
    public function apply($model) 
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->title = $model->title;
        $table->country_code = $model->country_code;
        $table->lang_id = $model->lang_id;
        $table->currency = $model->currency;
        $table->phone = $model->phone;
        $table->pay_system = $model->pay_system;
        
        if($model->id) $table->update(array('id' => $model->id));
        else $model->id = $table->insert();
        
        $tableLang = new Dante_Lib_Orm_Table($this->_tableNameLang);
        $tableLang->country_id = $model->id;
        $tableLang->disp_name = $model->disp_name;
        $tableLang->set_lang = $model->set_lang;
        
        $tableLang->apply(array(
            'country_id' => $model->id,
            'set_lang' => $model->set_lang
        ));
        
        return $model->id;
    }
    
    public function getPhoneByCode($code)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->select(array('country_code' => $code));
        
        if($table->getNumRows() == 0) return '';
        
        return $table->phone;
    }
    
    public function getPaySystemByCode($code)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->select(array('country_code' => $code));
        
        if($table->getNumRows() == 0) return '';
        
        return $table->pay_system;
    }
    
    public function getCurrencyByCode($code)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->select(array('country_code' => $code));
        
        if($table->getNumRows() == 0) return '';
        
        return $table->currency;
    }
}

?>
