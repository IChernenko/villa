<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of controller
 *
 * @author Valkyria
 */
class Villa_Module_Countries_Manage_Controller extends Module_Currencycountries_Manage_Controller
{
    protected $_alias = 'villa.module.countries.manage.controller';
    
    protected function _init() 
    {
        parent::_init();
        $this->_mapper = new Villa_Module_Countries_Manage_Mapper();
    }    
    
    protected function _instanceModel() 
    {
        return new Villa_Module_Countries_Manage_Model();
    }
    
    protected function _instanceView() 
    {
        return new Villa_Module_Countries_Manage_View();
    }

    protected function _getTitles() 
    {
        $titles = parent::_getTitles();
        
        $funcTitle = array_pop($titles);
        
        $titles[] = array(
            'content' => 'Телефон',
            'sortable' => 'phone'
        );
        
        $titles[] = array(
            'content' => 'Платеж. система'
        );
        
        $titles[] = $funcTitle;
        
        return $titles;
    }
    
    protected function _genFilters($lngList, $ccyList) 
    {
        $filters = parent::_genFilters($lngList, $ccyList);
        
        $btnFilter = array_pop($filters);
        
        $filters['phone'] = array(
            'type' => 'text',
            'value' => $this->_getRequest()->get('phone')
        );
        
        $filters['pay_system'] = array(
            'type' => 'combobox',
            'items' => Villa_Module_Countries_Helper::getPaymentSystemSelect(),
            'value' => $this->_getRequest()->get_validate('pay_system', 'int')
        );
        
        $filters['button_filter'] = $btnFilter;
        
        return $filters;
    }
    
    protected function _genRows($rows, $lngList, $ccyList) 
    {
        $paymentSystems = Villa_Module_Countries_Helper::getPaymentSystemSelect();
        
        $table = array();
        foreach($rows as $row)
        {
            $cur = array();
            $cur['id'] = $row['id'];
            $cur['title'] = $row['title'];
            $cur['country_code'] = $row['country_code'];
            $cur['lang'] = $lngList[$row['lang_id']];
            $cur['currency'] = $ccyList[$row['currency']];     
            $cur['phone'] = $row['phone'];
            $cur['pay_system'] = $paymentSystems[$row['pay_system']];
            $cur['buttons'] = array(
                'id' => $row['id'],
                'type' => array('edit', 'del'),                
            );  
            $table[$cur['id']] = $cur;
        }
        
        return $table;
    }
    
    protected function _populateModel() 
    {
        $model = parent::_populateModel();
        $model->phone = $this->_getRequest()->get('phone');
        $model->pay_system = $this->_getRequest()->get_validate('pay_system', 'int', PAYMENT_SYSTEM_PAYPAL);
        
        return $model;
    }
}

?>
