<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of view
 *
 * @author Valkyria
 */
class Villa_Module_Countries_Manage_View extends Module_Currencycountries_Manage_View
{
    public function drawForm($model)
    {
        $filePath = '../villa/module/countries/manage/template/form.html';
        
        $tpl = new Dante_Lib_Template();
        
        $tpl->langList = $this->langList;
        $tpl->cсyList = $this->currencyList;
        $tpl->paySystems = Villa_Module_Countries_Helper::getPaymentSystemSelect();
        $tpl->model = $model;
        return $tpl->draw($filePath);
    }
}

?>
