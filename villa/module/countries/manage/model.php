<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of model
 *
 * @author Valkyria
 */
class Villa_Module_Countries_Manage_Model extends Module_Currencycountries_Manage_Model
{
    public $filter_rules = array(
        array(
            'name'=>'title',
            'format'=>'`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ),
        array(
            'name'=>'country_code',
            'format'=>'`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ),
        array(
            'name'=>'lang_id',
            'format'=>'`t1`.`%1$s` = "%2$s"'
        ),
        array(
            'name'=>'currency',
            'format'=>'`t1`.`%1$s` = "%2$s"'
        ),
        array(
            'name'=>'phone',
            'format'=>'`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ),
        array(
            'name'=>'pay_system',
            'format'=>'`t1`.`%1$s` = "%2$s"'
        ),
    );
    
    public $phone;
    
    public $pay_system;
}

?>
