CREATE TABLE `[%%]villa_infoblock`(
    `lang_id` INT(11) UNSIGNED NOT NULL,
    `text` TEXT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `[%%]villa_infoblock` ADD CONSTRAINT `fk_infoblock_lang_id`
    FOREIGN KEY (`lang_id`) REFERENCES `[%%]languages`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;

INSERT INTO `[%%]entity` SET `sys_name` = 'infoblock', `name` = 'Инфоблок';