<?php

global $package;
$package = array(
    'version' => '2',
    'name' => 'villa.module.infoblock',
    'dependence' => array(
        'module.entity',
        'module.gallery',
        'lib.jqueryUpload',
    ),
    'js' => array(
        '../villa/module/infoblock/js/villa_infoblock.js',
    )
);

?>