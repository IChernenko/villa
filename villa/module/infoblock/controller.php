<?php

/**
 * Description of controller
 *
 * @author Valkyria
 */
class Villa_Module_Infoblock_Controller extends Dante_Controller_Base
{
    protected function _defaultAction() 
    {
        $mapper = new Villa_Module_Infoblock_Mapper();
        $galMapper = new Module_Gallery_Mapper();
        $view = new Villa_Module_Infoblock_View();
        
        $view->text = $mapper->get(Module_Lang_Helper::getCurrent());
        $view->gallery = $galMapper->getByEntity(
            Villa_Module_Infoblock_Helper::getEntityTypeId(), Villa_Module_Infoblock_Helper::ENTITY_ID
        );
        
        return $view->draw();
    }
}

?>
