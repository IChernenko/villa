<?php

/**
 * Description of view
 *
 * @author Valkyria
 */
class Villa_Module_Infoblock_View 
{
    public $text;
    public $gallery;
    
    public function draw()
    {        
        $tpl = new Dante_Lib_Template();
        $filePath = '../villa/module/infoblock/template/infoblock.html';
        
        $tpl->text = $this->text;
        $tpl->gallery = $this->gallery;
        return $tpl->draw($filePath);
    }
}

?>
