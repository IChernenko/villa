<?php

/**
 * Description of view
 *
 * @author Valkyria
 */
class Villa_Module_Infoblock_Manage_View 
{
    public $langList = array();
    
    public $lang;
    
    public function draw($text)
    {
        $tpl = new Dante_Lib_Template();
        $filePath = '../villa/module/infoblock/manage/template/table.html';
        
        $tpl->text = $text;
        $tpl->lang = $this->lang;
        $tpl->langList = $this->langList;
        return $tpl->draw($filePath);
    }
    
    public function drawGallery($gallery)
    {
        $tpl = new Dante_Lib_Template();
        $filePath = '../villa/module/infoblock/manage/template/gallery.html';
        
        $tpl->gallery = $gallery;
        return $tpl->draw($filePath);
    }
    
    public function drawRow($id, $filePath)
    {
        $html = '<tr id="'.$id.'">
            <td>
                <image src="'.$filePath.'" style="height:200px; max-width:400px;">
            </td>
            <td>                
                <textarea name="img_comment" style="width: 250px; height: 70px;"></textarea>
                <button class="btn btn-info btn-mini" style="width: 70px; margin-top: 10px;" onclick="villa_infoblock.addComment(this, '.$id.');">
                    Сохранить
                </button>
                <p class="comment-success" style="margin: 10px 0;"></p>
            </td>
            <td>
                <button class="btn btn-danger btn-mini" style="width: 70px;" onclick="villa_infoblock.delImage(this, '.$id.');">
                    Удалить
                </button>
            </td>
        </tr>';
        
        return $html;
    }
}

?>
