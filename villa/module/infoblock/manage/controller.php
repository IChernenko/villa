<?php

/**
 * Description of controller
 *
 * @author Valkyria
 */
class Villa_Module_Infoblock_Manage_Controller extends Dante_Controller_Base
{
    protected $_mapper;
    
    protected function _init() 
    {
        parent::_init();
        
        $helper = new Module_Division_Helper();
        $helper->checkAccess();
        
        $this->_mapper = new Villa_Module_Infoblock_Mapper();
    }

    protected function _defaultAction() 
    {
        $langMapper = new Module_Lang_Mapper();
        
        $lang = $this->_getRequest()->get_validate('lang_id', 'int', 1);
        $text = $this->_mapper->get($lang);
        
        $view = new Villa_Module_Infoblock_Manage_View();
        $view->lang = $lang;
        $view->langList = $langMapper->getList();
        return $view->draw($text);
    }
    
    protected function _drawAjaxAction()
    {
        return array(
            'result' => 1,
            'html' => $this->_defaultAction()
        );
    }
    
    protected function _editAction()
    {
        $langId = $this->_getRequest()->get('lang_id');
        $text = $this->_getRequest()->get('text');
        
        $this->_mapper->apply($langId, $text);
        
        return array('result' => 1);
    }
    
    protected function _openGalleryAction()
    {
        $galMapper = new Module_Gallery_Mapper();
        $gallery = $galMapper->getByEntity(
                Villa_Module_Infoblock_Helper::getEntityTypeId(), 
                Villa_Module_Infoblock_Helper::ENTITY_ID
        );
        
        $view = new Villa_Module_Infoblock_Manage_View();
        return array(
            'result' => 1,
            'html' => $view->drawGallery($gallery)
        );
    }

    protected function _imageUploadAction()
    {
        if(!isset($_FILES['file']))
            return array(
                'result' => 0,
                'message' => 'Нет файла для загрузки'
            );
                    
        $uploadDir = Villa_Module_Infoblock_Helper::getFolder();
        if (!is_dir($uploadDir)) mkdir($uploadDir, 0755, true);             

        $newName = substr(md5(time()), 0, 25);  

        $uploader = new Lib_JqueryUpload_Uploader();
        $uploader->globalKey = 'file';
        
        $image = $uploader->upload($uploadDir, $newName);   
        $id = $this->_addImage($image);
        
        if(!$id) 
            return array(
                'result' => 0,
                'message' => 'Не удалось сохранить файл'
            );
        
        $view = new Villa_Module_Infoblock_Manage_View();
        $html = $view->drawRow($id, $uploadDir.$image);
        
        return array(
            'result' => 1,
            'html' => $html
        );
    } 

    protected function _addImage($image)
    {
        $galMapper = new Module_Gallery_Mapper();
        
        $model = new Module_Gallery_Model();
        $model->entity_id = Villa_Module_Infoblock_Helper::ENTITY_ID;
        $model->entity_type_id = Villa_Module_Infoblock_Helper::getEntityTypeId();
        $model->file_name = $image;
        $model->adding_date = time();
        
        if(!$model->entity_type_id) return false;
        
        return $galMapper->addFile($model);
    }
    
    protected function _addCommentAction()
    {
        $mapper = new Module_Gallery_Mapper();
        
        $model = new Module_Gallery_Model();
        $model->id = $this->_getRequest()->get('id');
        $model->comment = $this->_getRequest()->get_validate('comment', 'text');
        
        $mapper->addComment($model);
        
        return array(
            'result' => 1
        );
    }
    
    protected function _delImageAction()
    {
        $id = $this->_getRequest()->get('id');
        
        $mapper = new Module_Gallery_Mapper();
        $filename = Villa_Module_Infoblock_Helper::getFolder().$mapper->getFileName($id);
        $mapper->fileDelete($id);
                
        if(is_file($filename)) unlink($filename);
        
        return array(
            'result' => 1
        );
    }
}

?>
