<?php
/**
 * Description of helper
 *
 * @author Valkyria
 */
class Villa_Module_Infoblock_Helper 
{
    const ENTITY_ID = 1;
    const ENTITY_NAME = 'infoblock';
    
    public static function getFolder()
    {
        return Dante_Lib_Config::get('app.uploadFolder').'/'.Dante_Lib_Config::get('infoblock.dir').'/';
    }
    
    public static function getEntityTypeId()
    {        
        $mapper = new Module_Entity_Mapper();
        return $mapper->getBySysName(self::ENTITY_NAME)->id;
    }
}

?>
