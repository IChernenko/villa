<?php

/**
 * Description of mapper
 *
 * @author Valkyria
 */
class Villa_Module_Infoblock_Mapper 
{
    protected $_tableName = '[%%]villa_infoblock';
    
    public function get($langId)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        return $table->select(array('lang_id' => $langId))->text;
    }
    
    public function apply($langId, $text)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->lang_id = $langId;
        $table->text = $text;
        
        $table->apply(array('lang_id' => $langId));
    }
}

?>
