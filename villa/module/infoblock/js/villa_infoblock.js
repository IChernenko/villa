villa_infoblock = {
    controller: 'villa.module.infoblock.manage.controller',
    changeLang: function(obj)
    {
        if(this.checkDisabled(obj)) return;
        
        villa_infoblock.disableBtn();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'ajax.php',
            data: {                
                controller: villa_infoblock.controller,
                action: 'drawAjax',
                lang_id: $(obj).val()
            },
            success: function(data){
                if(data.result == 1)
                    $('#editInfoBlock').replaceWith(data.html);
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    },    
    save: function(obj)
    {
        if(this.checkDisabled(obj)) return;
        
        $("textarea[name=text]").elrte('updateSource');
        villa_infoblock.disableBtn();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'ajax.php',
            data: $('#editPromoBlock').serializeArray(),
            success: function(data){
                villa_infoblock.enableBtn();
                
                if(data.result == 1)
                    jAlert('Изменения сохранены', 'Сообщение');
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    },    
    openGallery: function(obj)
    {
        if(this.checkDisabled(obj)) return;
        
        villa_infoblock.disableBtn();
        $('#modalWindow').arcticmodal({
            type: 'ajax',
            url: 'ajax.php',
            ajax: {
                type: 'get',
                dataType: "json",
                data: {
                    controller: villa_infoblock.controller,
                    action: 'openGallery'
                },
                success: function(data, el, response)
                {
                    data.body.html(el);
                    $(el).find('.modal-body').html(response.html);
                }
            },
            afterClose: function(data, el)
            {
                villa_infoblock.enableBtn();
            }
        });
    },    
    delImage: function(obj, id)
    {
        if(this.checkDisabled(obj)) return;
        
        this.disableBtn();
        dante.ajaxContentLoadStart($(obj).parent('td'));
        $.ajax({
            type: 'POST',
            data: {
                controller : villa_infoblock.controller,
                action :'delImage',
                id: id
            },
            url: '/ajax.php',
            dataType : "json",
            success: function (data){ 
                if(data.result==1)
                {                   
                    $('tr#'+id).remove();
                    villa_infoblock.enableBtn();
                }
                else
                {
                    jAlert(data.message, 'Ошибка');
                }
            }
        });
    },    
    addComment: function(obj, id)
    {
        if(this.checkDisabled(obj)) return;
        this.disableBtn();
        
        var comment = $(obj).prev('textarea').val();
        if(!comment) 
        {
            jAlert('Вы не ввели комментарий!', 'Ошибка');
            return;
        }
        
        dante.ajaxContentLoadStart($(obj).next('p'));
        $.ajax({
            type: 'POST',
            data: {
                controller : villa_infoblock.controller,
                action :'addComment',
                id: id,
                comment: comment
            },
            url: '/ajax.php',
            dataType : "json",
            success: function (data){
                villa_infoblock.enableBtn();
                if(data.result==1)
                {
                    $(obj).next('p').text('Ваш комментарий сохранен!');
                }
                else
                {
                    jAlert(data.message, 'Ошибка');
                }
            }
        });
    },    
    editEnd: function()
    {
        $('#modalWindow').arcticmodal('close');
    },
    disableBtn: function()
    {
        $('a.btn, select').addClass('disabled');
    },    
    enableBtn: function()
    {
        $('a.btn, select').removeClass('disabled');
    },
    checkDisabled: function(obj)
    {
        if($(obj).hasClass('disabled')) return true;
        else return false;
    }
}


