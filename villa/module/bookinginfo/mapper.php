<?php

/**
 * Description of mapper
 *
 * @author Valkyria
 */
class Villa_Module_Bookinginfo_Mapper 
{
    protected $_tableName = '[%%]villa_booking_info';
    protected $_tableList = '[%%]villa_booking_info_list';
    
    public function get($lang)
    {
        $sql = "SELECT * FROM {$this->_tableName} WHERE `lang_id` = {$lang} ORDER BY `draw_order` ASC";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        
        $blocks = array();
        $listBlocks = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            $block = new Villa_Module_Bookinginfo_Model_Block();
            $block->id = $f['id'];
            $block->lang_id = $lang;
            $block->type = $f['type'];
            $block->caption = $f['caption'];
            $block->text = $f['text'];
            $block->draw_order = $f['draw_order'];
            
            $blocks[$block->id] = $block;
            if($block->type == BOOKING_INFO_TYPE_LIST)
                $listBlocks[] = $block->id;
        }
        
        if(!count($listBlocks)) return $blocks;
        
        $idStr = implode(', ', $listBlocks);
        $sql = "SELECT * FROM {$this->_tableList} WHERE `block_id` IN($idStr)";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            $rowModel = new Villa_Module_Bookinginfo_Model_Row();
            $rowModel->row_id = $f['row_id'];
            $rowModel->name = $f['name'];
            $rowModel->text = $f['text'];
            
            $blocks[$f['block_id']]->rows[$rowModel->row_id] = $rowModel;
        }
        
        return $blocks;
    }
}

?>
