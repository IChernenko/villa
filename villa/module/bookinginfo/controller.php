<?php
/**
 * Description of controller
 *
 * @author Valkyria
 */
class Villa_Module_Bookinginfo_Controller extends Dante_Controller_Base
{
    protected function _defaultAction() 
    {
        $mapper = new Villa_Module_Bookinginfo_Mapper();
        $blocks = $mapper->get(Module_Lang_Helper::getCurrent());
        
        $view = new Villa_Module_Bookinginfo_View();
        return $view->draw($blocks);
    }
}

?>
