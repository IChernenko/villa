<?php

global $package;
$package = array(
    'version' => '2',
    'name' => 'villa.module.bookinginfo',
    'dependence' => array(),
    'js' => array(
        '../villa/module/bookinginfo/js/booking_info.js',
    )
);

?>