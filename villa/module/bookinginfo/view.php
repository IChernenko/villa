<?php
/**
 * Description of view
 *
 * @author Valkyria
 */
class Villa_Module_Bookinginfo_View 
{
    public function draw($blocks)
    {
        $tpl = new Dante_Lib_Template();
        $filePath = '../villa/module/bookinginfo/template/booking_info.html';
        
        $tpl->blocks = $blocks;        
        return $tpl->draw($filePath);
    }
}

?>
