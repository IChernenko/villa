<?php
/**
 * Description of mapper
 *
 * @author Valkyria
 */
class Villa_Module_Bookinginfo_Manage_Mapper extends Villa_Module_Bookinginfo_Mapper
{
    public function addBlock(Villa_Module_Bookinginfo_Model_Block $model)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->lang_id = $model->lang_id;
        $table->type = $model->type;
        $table->caption = $model->caption;
        $table->draw_order = $this->_getNewDrawOrder($model->lang_id);
        
        return $table->insert();
    }
    
    protected function _getNewDrawOrder($lang)
    {
        $sql = "SELECT `draw_order` FROM {$this->_tableName} WHERE `lang_id` = {$lang}
                ORDER BY `draw_order` DESC LIMIT 1";
        
        return (int)Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'draw_order') + 1;
    }
    
    protected function _getNewRowId($block)
    {
        $sql = "SELECT `row_id` FROM {$this->_tableList} WHERE `block_id` = {$block}
                ORDER BY `row_id` DESC LIMIT 1";
        
        return (int)Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'row_id') + 1;
    }
    
    public function apply($blocks)
    {
        foreach($blocks as $block)
        {
            $table = new Dante_Lib_Orm_Table($this->_tableName);
            $table->caption = $block->caption;
            $table->text = $block->text;
            $table->draw_order = $block->draw_order;
            
            $table->update(array('id' => $block->id));
            
            if($block->type == BOOKING_INFO_TYPE_LIST)
            {                
                $tableList = new Dante_Lib_Orm_Table($this->_tableList);  
                $tableList->delete(array('block_id' => $block->id));
                
                if(is_array($block->rows))
                {                
                    foreach($block->rows as $row)
                    {
                        $tableList = new Dante_Lib_Orm_Table($this->_tableList);                    
                        $tableList->block_id = $block->id;
                        $tableList->row_id = $row->row_id;
                        $tableList->name = $row->name;
                        $tableList->text = $row->text;

                        $tableList->insert();
                    }
                }
            }
        }
    }
    
    public function del($id)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);        
        $table->delete(array('id' => $id));
    }
}

?>
