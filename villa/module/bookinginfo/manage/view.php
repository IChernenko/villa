<?php

/**
 * Description of view
 *
 * @author Valkyria
 */
class Villa_Module_Bookinginfo_Manage_View 
{
    public $langList = array();
    
    public $lang;
    
    public function draw($blocks)
    {
        $tpl = new Dante_Lib_Template();
        $filePath = '../villa/module/bookinginfo/manage/template/form.html';
        
        $tpl->lang = $this->lang;
        $tpl->langList = $this->langList;
        
        $blockHtml = array();
        foreach($blocks as $id => $block)
        {
            $blockHtml[$id] = $this->drawBlock($block);
        }
        $tpl->blocks = $blockHtml;
        
        return $tpl->draw($filePath);
    }
    
    public function drawBlock($block)
    {
        $tpl = new Dante_Lib_Template();
        $filePath = '../villa/module/bookinginfo/manage/template/block.html';
        
        $tpl->block = $block;        
        return $tpl->draw($filePath);
    }
}

?>
