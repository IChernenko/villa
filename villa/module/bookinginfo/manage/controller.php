<?php

/**
 * Description of controller
 *
 * @author Valkyria
 */
class Villa_Module_Bookinginfo_Manage_Controller extends Dante_Controller_Base
{   
    protected function _init() 
    {
        parent::_init();
        
        $helper = new Module_Division_Helper();
        $helper->checkAccess();
    }


    protected function _instanceMapper()
    {
        return new Villa_Module_Bookinginfo_Manage_Mapper();
    }
    protected function _instanceView()
    {
        return new Villa_Module_Bookinginfo_Manage_View();
    }

    protected function _defaultAction() 
    {
        $lang = $this->_getRequest()->get_validate('lang_id', 'int', 1);
        
        $blocks = $this->_instanceMapper()->get($lang);
        
        $view = $this->_instanceView();
        $langMapper = new Module_Lang_Mapper();
        
        $view->lang = $lang;
        $view->langList = $langMapper->getList();
        
        return $view->draw($blocks);
    }
    
    protected function _drawAjaxAction()
    {
        return array(
            'result' => 1,
            'html' => $this->_defaultAction()
        );
    }
    
    protected function _addBlockAction()
    {
        $blockModel = new Villa_Module_Bookinginfo_Model_Block();
        $blockModel->type = $this->_getRequest()->get_validate('type', 'int', BOOKING_INFO_TYPE_TEXT);
        $blockModel->lang_id = $this->_getRequest()->get_validate('lang_id', 'int', 1);
        $blockModel->caption = $this->_getRequest()->get_validate('caption', 'text');
        
        $blockModel->id = $this->_instanceMapper()->addBlock($blockModel);
        
        if(!$blockModel->id)
            return array(
                'result' => 0,
                'message' => 'Ошибка при добавлении блока'
            );
        
        return array(
            'result' => 1,
            'block' => $this->_instanceView()->drawBlock($blockModel)
        );
    }

    protected function _saveAction()
    {
        $langId = $this->_getRequest()->get('lang_id');
        $blocks = $this->_getRequest()->get('blocks');
          
        $models = array();
        $drawOrder = 1;
        foreach($blocks as $id => $block)
        {
            $blockModel = new Villa_Module_Bookinginfo_Model_Block();
            $blockModel->id = $id;
            $blockModel->lang_id = $langId;
            $blockModel->type = $block['type'];
            $blockModel->caption = $block['caption'];
            $blockModel->draw_order = $drawOrder;
            
            switch($blockModel->type)
            {
                case BOOKING_INFO_TYPE_TEXT:
                    $blockModel->text = isset($block['text']) ? $block['text'] : '';
                    break;
                
                case BOOKING_INFO_TYPE_LIST:
                    if(!isset($block['rows']) || !is_array($block['rows'])) break;
                    
                    $rowId = 1;
                    foreach($block['rows'] as $row)
                    {
                        if(!($row['name']) && !$row['text']) continue;
                        
                        $rowModel = new Villa_Module_Bookinginfo_Model_Row();
                        $rowModel->row_id = $rowId;
                        $rowModel->name = $row['name'];
                        $rowModel->text = $row['text'];
                        
                        $blockModel->rows[] = $rowModel;
                        $rowId++;
                    }
                    
                    break;
            }
                        
            $models[] = $blockModel;
            $drawOrder++;
        }
        
        $this->_instanceMapper()->apply($models);
        
        return $this->_drawAjaxAction();
    }
    
    protected function _delBlockAction()
    {
        $id = $this->_getRequest()->get('id');
        $this->_instanceMapper()->del($id);
        
        return array(
            'result' => 1
        );
    }
}

?>
