<?php
/**
 * Description of block
 *
 * @author Valkyria
 */
class Villa_Module_Bookinginfo_Model_Block 
{
    public $id;
    
    public $lang_id;
    
    public $type;
    
    public $caption;
    
    public $text;
    
    public $rows = array();
    
    public $draw_order;
}

?>
