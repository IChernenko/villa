booking_info = {
    controller: 'villa.module.bookinginfo.manage.controller',
    changeLang: function(obj)
    {
        if(this.checkDisabled(obj)) return;
        
        booking_info.disableBtn();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'ajax.php',
            data: {                
                controller: booking_info.controller,
                action: 'drawAjax',
                lang_id: $(obj).val()
            },
            success: function(data){
                if(data.result == 1)
                    booking_info.replace(data.html);
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    },   
    addBlock: function(type, obj)
    {
        if(this.checkDisabled(obj)) return;
        
        jPrompt('Введите заголовок блока:', '', 'Добавление блока', function(caption){
            if(!caption) return;
            
            booking_info.disableBtn();
            $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: 'ajax.php',
                data: {
                    controller: booking_info.controller,
                    action: 'addBlock',
                    type: type,
                    lang_id: $('#langId').val(),
                    caption: caption
                },
                success: function(data)
                {
                    if(data.result == 1)
                    {
                        jAlert('Блок добавлен', 'Сообщение', function(){
                            $(data.block).appendTo($('#blockList')).wrap('<li></li>');
                            booking_info.enableBtn();
                        });
                    }
                    else
                        jAlert(data.message, 'Ошибка');
                }
            });
        });
        
    },
    addBlockRow: function(blockId, obj)
    {
        if(this.checkDisabled(obj)) return;
        this.disableBtn();
        
        var blockTable = $(obj).closest('.block-table');
        var newRowId = parseInt($(blockTable).find('tr.block-row').not('.new-row').last().attr('index')) + 1;
        var nameBase = "blocks["+blockId+"][rows]["+newRowId+"]";
        
        var newRow = $(blockTable).find('tr.new-row').clone();
        newRow.attr('index', newRowId);
        newRow.find('input').attr('name', nameBase+"[name]");
        newRow.find('textarea').attr('name', nameBase+"[text]");
        newRow.removeClass('new-row').insertBefore($(blockTable).find('tr.new-row')).show(300);
        
        this.enableBtn();
    }, 
    delBlockRow: function(obj)
    {
        if(this.checkDisabled(obj)) return;
        this.disableBtn();
        
        $(obj).closest('tr.block-row').remove();
        this.enableBtn();
    },
    delBlock: function(blockId, obj)
    {
        jConfirm('Вы уверены?', 'Удалить блок', function(r){
            if(!r) return;
            
            booking_info.disableBtn();
            $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: 'ajax.php',
                data: {
                    controller: booking_info.controller,
                    action: 'delBlock',
                    id: blockId
                },
                success: function(data)
                {
                    if(data.result == 1)
                    {
                        jAlert('Блок удален', 'Сообщение', function(){
                            $(obj).closest('.block-table').remove();
                            booking_info.enableBtn();
                        });
                    }
                    else
                        jAlert(data.message, 'Ошибка');
                }
            });
        });
    },
    save: function(obj)
    {
        if(this.checkDisabled(obj)) return;
        
        if(!$('.block-table').length)
        {
            jAlert('Нет блоков для сохранения', 'Ошибка');
            return;
        }
        
        booking_info.disableBtn();
        
        var data = $('#bookingInfoEdit').serializeArray();
        data.push(
            {name: 'controller', value: booking_info.controller},
            {name: 'action', value: 'save'}
        );
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'ajax.php',
            data: data,
            success: function(data){
                booking_info.enableBtn();
                
                if(data.result == 1)
                {
                    jAlert('Изменения сохранены', 'Сообщение', function(){
                        booking_info.replace(data.html);
                    });                    
                }
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    },   
    replace: function(html)
    {
        $('#bookingInfoEdit').replaceWith(html);
    },
    disableBtn: function()
    {
        $('a.btn, select').addClass('disabled');
    },    
    enableBtn: function()
    {
        $('a.btn, select').removeClass('disabled');
    },
    checkDisabled: function(obj)
    {
        if($(obj).hasClass('disabled')) return true;
        else return false;
    }
}


