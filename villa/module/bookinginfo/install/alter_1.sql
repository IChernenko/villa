CREATE TABLE `[%%]villa_booking_info`(
    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `lang_id` INT(11) UNSIGNED NOT NULL,
    `type` TINYINT(1) NOT NULL DEFAULT 1,
    `caption` VARCHAR(200) DEFAULT NULL,
    `text` TEXT,
    `draw_order` INT(11) NOT NULL DEFAULT 1,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

ALTER TABLE `[%%]villa_booking_info` ADD CONSTRAINT `fk_booking_info_lang_id`
    FOREIGN KEY (`lang_id`) REFERENCES `[%%]languages`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE `[%%]villa_booking_info_list`(
    `block_id` INT(11) UNSIGNED NOT NULL,
    `row_id` INT(11) UNSIGNED NOT NULL,
    `name` VARCHAR(200) DEFAULT NULL,
    `text` TEXT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `[%%]villa_booking_info_list` ADD CONSTRAINT `fk_booking_info_list_block_id`
    FOREIGN KEY (`block_id`) REFERENCES `[%%]villa_booking_info`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;