<?php
/**
 * Главный контроллер модуля Manage-About (/villa/module/manage/about/)
 */
class Villa_Module_Manage_About_Controller extends Dante_Controller_Base
{
    /**
        * @var $_mapper - маппер модуля Manage_About
        */
    protected $_mapper;

    /**
        * Инициализация переменных класса контроллера
        */
    protected function _init()
    {
        parent::_init();
        $helper = new Module_Division_Helper();
        $helper->checkAccess();
        $this->_mapper = new Villa_Module_Manage_About_Mapper();
    }

    /**
        * Action выполняющийся по умолчанию
        */
    protected function _defaultAction()
    {
        // Создание маппера для языковой локали
        $langMapper = new Module_Lang_Mapper();

        // Определение ID текущей языковой локали
        $lang = $this->_getRequest()->get_validate('lang_id', 'int', 1);

        // Получение данных страницы с учетом текущей языковой версии $lang
        $pageData = $this->_mapper->getEntry($lang);

        // Получение данных об услугах с учетом текущей языковой версии $lang
        $servicesData = $this->_mapper->getServicesData($lang);

        /**
            * Создание представления (view) для раздела редактирования
            * страницы "Об Отеле" в административной части
        */
        $view = new Villa_Module_Manage_About_View();
        $view->lang = $lang;
        $view->servicesData = $servicesData;
        $view->langList = $langMapper->getList();

        return $view->drawEditor($pageData);
    }

    /**
        * Получение данных страницы из БД
        */
    protected function _drawAjaxAction()
    {
        return array(
            'result' => 1,
            'html' => $this->_defaultAction()
        );
    }

    /**
        * Сохранение данных страницы в БД
        */
    protected  function _savePageDataAjaxAction()
    {
        $request = new Dante_Lib_Request();

        $langId = $request->get('langId');
        $title = $request->get('title');
        $html = $request->get('html');
        $footerHtml = $request->get('footerHtml');
        $images = $request->get('images');
        $seoTitle = $request->get('seoTitle');
        $seoDescription = $request->get('seoDescription');
        $seoKeywords = $request->get('seoKeywords');

        $mapper = new Villa_Module_Manage_About_Mapper();
        if($mapper->changeEntry($langId, $title, $html, $footerHtml, $images, $seoTitle, $seoDescription, $seoKeywords))
        {
            $result = 1;
        }
        else
        {
            $result = 0;
        }
        return array(
            'result' => $result
        );
    }

    /**
        * Добавление сервиса в БД
        */

    protected function _addServiceAjaxAction()
    {
        $request = new Dante_Lib_Request();
        $langId = $request->get('langId');
        $title = $request->get('title');
        $description = $request->get('description');

        if($this->_mapper->addService($langId, $title, $description))
        {
            $result = 1;
        }
        else
        {
            $result = 0;
        }

        return array(
            'result' => $result
        );

    }

    /**
        * Обновление таблицы услуг для страницы Об отеле
        * @return array
        */
    protected function _showTableAjaxAction()
    {
        // Определение ID текущей языковой локали
        $lang = $this->_getRequest()->get_validate('lang_id', 'int', 1);

        // Получение данных об услугах с учетом текущей языковой версии $lang
        $servicesData = $this->_mapper->getServicesData($lang);

        $html = '';

        foreach($servicesData as $item)
        {

            $drawOrder = $item["draw_order"];
            $html .= '<tr>
                        <input
                                    type="hidden"
                                    name="serviceId"
                                    value="'.$item['id'].'" />
                    <td>
                            '.$drawOrder.'
                            <a class="btnUp btn btn-mini btn-info"
                            data-toggle="tooltip"
                            data-original-title="Переместить на одну строку вверх"
                            onclick="manage_about.changeDrawOrder
                                ('. $drawOrder .', '.$item['id'].', -1); return false;">

                            <i class="icon-arrow-up "></i>
                            <i class="icon-arrow-up icon-white"></i>
                            </a>

                            <a class="btnDown btn btn-mini btn-info" data-toggle="tooltip"
                                    data-original-title="Переместить на одну строку вниз"
                                    onclick="manage_about.changeDrawOrder
                                        ('.$drawOrder.', '.$item['id'].', 1); return false;">
                                    <i class="icon-arrow-down "></i>
                                    <i class="icon-arrow-down icon-white"></i>
                                </a>
                    </td>
                    <td>'.$item['title'].'</td>
                    <td>'.$item['description'].'</td>

                    <td>
                        <a class="btn btn-mini btn-success" href="#"
                            onclick="manage_about.editService(this); return false;">
                            <i class="icon-pencil icon-white"></i> Редактировать
                        </a>
                        <a class="btn btn-mini btn-danger" href="#"
                            onclick="manage_about.deleteService(this); return false;">
                            <i class="icon-remove icon-white"></i> Удалить
                        </a>
                    </td>
                </tr>
                <script type="text/javascript">jQuery(".btnUp, .btnDown").tooltip();</script>';
        }

        return array(
            'result' => 1,
            'html' => $html
        );
    }

    /**
        * Удаление услуги из таблицы на странице Об отеле
        * @return array
        */
    protected function _deleteServiceAjaxAction()
    {
        $request = new Dante_Lib_Request();
        $serviceId = $request->get('serviceId');

        if($this->_mapper->deleteService($serviceId))
        {
            $result = 1;
        }
        else
        {
            $result = 0;
        }

        return array(
            'result' => $result
        );

    }

    /**
        * Редактирование услуги из таблицы на странице Об отеле
        * @return array
        */
    protected function _editServiceAjaxAction()
    {
        $request = new Dante_Lib_Request();
        $serviceId = $request->get('serviceId');
        $result = $this->_mapper->getService($serviceId);
        
        $result['result'] = 1;
        return $result;
    }

    protected function _updateServiceAjaxAction()
    {
        $request = new Dante_Lib_Request();
        $serviceId = $request->get('serviceId');
        $title = $request->get('title');
        $description = $request->get('description');

        if($this->_mapper->updateService($serviceId, $title, $description))
        {
            $result = 1;
        }
        else
        {
            $result = 0;
        }

        return array(
            'result' => $result
        );
    }

    /**
        * Изменение порядка отображения услуги
        * @return array
        */

    protected function _changeOrderAction()
    {
        $request = new Dante_Lib_Request();
        $serviceId = $request->get('serviceId');
        $drawOrder = $request->get('drawOrder');
        $orderFactor = $request->get('orderFactor');

        $this->_mapper->changeOrder($serviceId, $drawOrder, $orderFactor);

//        $this->_showTableAjaxAction();

        return array(
            'result' => 1
        );
    }

    /**
        * Отобразить изображения для галереи
        * @return array
        */
    protected function _drawImagesAction()
    {
        $images = $this->_mapper->getImages();
        $view = new Villa_Module_Manage_About_View();
        return array(
            'result' => 1,
            'html' => $view->drawImages($images)
        );
    }

    /**
        * Функция для загрузки изображения для галереи
        * @return array
        *
        */
    protected function _imageUploadAction()
    {
        if(!isset($_FILES['file']))
            return array(
                'result' => 0,
                'message' => 'Нет файла для загрузки'
            );
        $uploadDir = Villa_Module_Hotel_Helper_Media::getAboutPageDir();
        if (!is_dir($uploadDir)) mkdir($uploadDir, 0755, true);

        $newName = time();
        $uploader = new Lib_JqueryUpload_Uploader();
        $uploader->globalKey = 'file';
        $image = $uploader->upload($uploadDir, $newName);
        $this->_mapper->addImage($image);

        $html = $this->_drawImagesAction();
        return $html;
    }

    /**
        * Функция для удаления изображения для галереи
        * @return array
        */
    protected function _delImageAction()
    {
        $imageId = $this->_getRequest()->get('imageId');
        $this->_mapper->delImage($imageId);

        return array(
            'result' => 1
        );
    }

    /**
        * Функция для изменения порядка отображения изображений для галереи
        * @return array
        */
    protected function _changeImgOrderAction()
    {
        $request = new Dante_Lib_Request();
        $imageId = $request->get('imageId');
        $drawOrder = $request->get('drawOrder');
        $orderFactor = $request->get('orderFactor');

        $this->_mapper->changeImgOrder($imageId, $drawOrder, $orderFactor);
        $html = $this->_drawImagesAction();

        return array(
            'result' => 1,
            'html' => $html
        );
    }



}


?>