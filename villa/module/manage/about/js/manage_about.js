manage_about =
{
    // Главный контроллер модуля Villa_Module_Manage_About_Controller
    controller: 'villa.module.manage.about.controller',

    /**
     * Обработчик изменения селектора выбора языка
     * @param object - объект селектора
     */
    changeLang: function(object)
    {
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data:
            {
                controller: manage_about.controller,
                action: 'drawAjax',
                lang_id: $(object).val()
            },
            success: function(data)
            {
                if(data.result == 1)
                {
                    $('.container-main').html(data.html);
                }
                else
                {
                    jAlert(data.message, 'Ошибка');
                }
            }
        });
    },

    /**
     * Инициализация редактора CKEditor
     * @param language - код языковой локали (ru, en, de ...)
     */
    initCKEditor: function(language)
    {
        CKEDITOR.replace('pageHtml',
            {
                language: language
            });
        CKEDITOR.replace('footerHtml',
            {
                language: language
            });
    },

    /**
     * Фунция - обработчик нажатия кнопки "Сохранить" в разделе редактирования
     * страницы "Об Отеле" в административной части
     */
    saveEditorData: function(object)
    {
        // Если кнопка "Сохранить" неактивна
        // return false;
        // else

        var langId = jQuery('#pageLangId').val();
        var title = jQuery('#pageTitle').val();
        var html = CKEDITOR.instances.pageHtml.getData();
        var footerHtml = CKEDITOR.instances.footerHtml.getData();
        var images = '';
        var seoTitle = jQuery('#seoTitle').val();
        var seoDescription = jQuery('#seoDescription').val();
        var seoKeywords = jQuery('#seoKeywords').val();

        jQuery.ajax({
            type: 'POST',
            url: 'ajax.php',
            data:
            {
                controller: manage_about.controller,
                action: 'savePageDataAjax',
                langId: langId,
                title: title,
                html: html,
                footerHtml: footerHtml,
                images: images,
                seoTitle: seoTitle,
                seoDescription: seoDescription,
                seoKeywords: seoKeywords
            },
            success: function(data)
            {
                if(data.result == 1)
                {
                    jAlert("Версия страницы успешно сохранена", "Сообщение");
                }
                else
                {
                    jAlert(data.message, "Ошибка");
                }
            }
        });
    },

    /**
     * Вывод данных таблицы услуг
     */
    showTable: function()
    {
        var langId = jQuery('#pageLangId').val();
        jQuery.ajax({
            type: 'POST',
            url: 'ajax.php',
            data:
            {
                controller: manage_about.controller,
                action: 'showTableAjax',
                langId: langId
            },

            success: function(data)
            {
                if(data.result == 1)
                {
                    jQuery('#serviceTable tbody').html(data.html);
                }
                else
                {
                    jAlert(data.message);
                    console.log(data);
                }
            }
        });
    },


    /**
     * Фунция - обработчик нажатия кнопки "Сохранить" в разделе создания
     * услуги для страницы
     */

    addService: function(object)
    {
        var langId = jQuery('#currLangId').val();
        var title = jQuery('#addInputTitle').val();
        var description = jQuery('#addInputDescription').val();

        if(!langId || langId == 0) return false;
        if(!title || title == '')
        {
            jAlert('Поле "Заголовок" должно быть заполнено.')
            return false;
        }
        if(!description || description == '')
        {
            jAlert('Поле "Описание" должно быть заполнено.')
            return false;
        }

        jQuery.ajax({
            type: 'POST',
            url: 'ajax.php',
            data:
            {
                controller: manage_about.controller,
                action: 'addServiceAjax',
                langId: langId,
                title: title,
                description: description
            },

            success: function(data)
            {
                if(data.result == 1)
                {
                    manage_about.showTable();
                    //jAlert("Версия страницы успешно сохранена", "Сообщение");
                }
                else
                {
                    jAlert(data.message, "Ошибка");
                }
            }
        });
    },

    /**
     * Фунция - обработчик нажатия кнопки "Удалить" в таблице
     * услуг для страницы
     */
    deleteService: function(object)
    {
        var serviceId = jQuery(object).closest('tr').find('input[name=serviceId]').val();

        jQuery.ajax({
            type: 'POST',
            url: 'ajax.php',
            data:
            {
                controller: manage_about.controller,
                action: 'deleteServiceAjax',
                serviceId: serviceId
            },

            success: function(data)
            {
                if(data.result == 1)
                {
                    manage_about.showTable();

                }
                else
                {
                    jAlert(data.message, "Ошибка");
                }
            }
        });
    },

    /**
     * Фунция - обработчик нажатия кнопки "Редактировать" в таблице
     * услуг для страницы
     */
    editService: function(object)
    {
        var serviceId = jQuery(object).closest('tr').find('input[name=serviceId]').val();

        jQuery.ajax({
            type: 'POST',
            url: 'ajax.php',
            data:
            {
                controller: manage_about.controller,
                action: 'editServiceAjax',
                serviceId: serviceId
            },

            success: function(data)
            {
                if(data.result == 1)
                {
                    jQuery('#editServiceModal #editInputTitle').val(data.title);
                    jQuery('#editServiceModal #editInputDescription').text(data.description);
                    jQuery('#editServiceModal #editServiceId').val(data.id);
                    jQuery('#editServiceModal').modal();
                }
                else
                {
                    jAlert(data.message, "Ошибка");
                }
            }
        });
    },

    /**
     * Вывод окна для управления изображениями галереи
     */
    editImages: function()
    {
        jQuery('#editImagesModal').modal({show: true});
        jQuery.ajax({
            type: 'POST',
            url: 'ajax.php',
            data:
            {
                controller: manage_about.controller,
                action: 'drawImages'
            },

            success: function(data)
            {
                if(data.result == 1)
                {
                    jQuery('#editImagesModal .modal-body').html(data.html);
                }
                else
                {
                    jAlert(data.message, "Ошибка");
                }
            }
        });
    },

    /**
     * Удаление изображения
     * @param object
     * @param imageId
     */
    delImage: function(object, imageId)
    {
        $.ajax({
            url: '/ajax.php',
            type: 'post',
            dataType : "json",
            data:{
                controller: manage_about.controller,
                action: 'delImage',
                imageId: imageId
            },
            success: function (data){
                if(data.result == 1)
                    $(object).closest('tr').remove();
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    },

    /**
     * Изменение очередности отображенияя изображений
     * @param imageId
     * @param draw_order
     * @param order_factor
     */
    changeImgOrder: function(imageId, draw_order, order_factor)
    {
        $.ajax({
            url: '/ajax.php',
            dataType: "json",
            type: 'post',
            data:{
                controller: manage_about.controller,
                action: 'changeImgOrder',
                imageId: imageId,
                drawOrder: draw_order,
                orderFactor: order_factor
            },
            success: function (data){
                if(data.result == 1)
                {
                    jQuery('#editImagesModal .modal-body').html(data.html.html);
                }


                else
                {
                    jAlert(data.message, 'Ошибка');
                }

            }
        });
    },


    /**
     * Фунция - обработчик нажатия кнопки "Сохранить" в окне редактирования услуги
     * услуг для страницы
     */
    updateService: function(object)
    {
        var langId = jQuery('#currLangId').val();
        var serviceId = jQuery(object).closest('#editServiceModal').find('#editServiceId').val();
        var title = jQuery('#editInputTitle').val();
        var description = jQuery('#editInputDescription').val();



        if(!langId || langId == 0) return false;
        if(!title || title == '')
        {
            jAlert('Поле "Заголовок" должно быть заполнено.')
            return false;
        }
        if(!description || description == '')
        {
            jAlert('Поле "Описание" должно быть заполнено.')
            return false;
        }

        jQuery.ajax({
            type: 'POST',
            url: 'ajax.php',
            data:
            {
                controller: manage_about.controller,
                action: 'updateServiceAjax',
                langId: langId,
                title: title,
                description: description,
                serviceId: serviceId
            },

            success: function(data)
            {
                if(data.result == 1)
                {
                    jQuery('#editServiceModal').modal('hide');
                    manage_about.showTable();
                    //jAlert("Версия страницы успешно сохранена", "Сообщение");
                }
                else
                {
                    jAlert(data.message, "Ошибка");
                }
            }
        });
    },



    /**
     * Фунция - обработчик нажатия кнопок "Переместить на одну строку" в таблице
     * услуг для страницы
     */
    changeDrawOrder: function(drawOrder, serviceId, orderFactor)
    {
        // alert('test');
        jQuery.ajax({
            url:        '/ajax.php',
            dataType:   'json',
            type:       'post',
            data:
            {
                controller:     manage_about.controller,
                action:         'changeOrder',
                serviceId:   serviceId,
                drawOrder:  drawOrder,
                orderFactor: orderFactor
            },

            success: function(data)
            {
                if(data.result == 1)
                {
                    manage_about.showTable();
                }
                else
                {
                    jAlert(data.message, 'Ошибка');
                    console.log(data);
                }
            }
        });
    }
}
