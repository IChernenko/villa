<?php
/**
 * Class Villa_Module_Manage_About_Mapper
 * Маппер модуля About (для работы с базой данных)
 */
    class Villa_Module_Manage_About_Mapper
    {
        protected $_tableName = '[%%]villa_about_page';
        protected $_servicesTableName = '[%%]villa_about_page_services';
        protected $_imagesTableName = '[%%]villa_about_page_images';

        /**
         * Функция, получающая запись из таблицы $_tableName
         * @param $langId - ID языка
         * @return array - данные записи
         */
        public function getEntry($langId)
        {
            $table = new Dante_Lib_Orm_Table($this->_tableName);
            $pageInfo = array();
            $pageInfo['title'] = $table->select(array('lang_id' => $langId))->title;
            $pageInfo['html'] = $table->select(array('lang_id' => $langId))->html;
            $pageInfo['footer_html'] = $table->select(array('lang_id' => $langId))->footer_html;
            $pageInfo['images'] = $table->select(array('lang_id' => $langId))->images;
            $pageInfo['seo_title'] = $table->select(array('lang_id' => $langId))->seo_title;
            $pageInfo['seo_description'] = $table->select(array('lang_id' => $langId))->seo_description;
            $pageInfo['seo_keywords'] = $table->select(array('lang_id' => $langId))->seo_keywords;
            return $pageInfo;
        }

        /**
         * Функция, добавляющая запись в таблицу $_tableName
         * @param $langId - ID языка
         * @param $title - заголовок
         * @param $html - html разметка
         * @param $images - изображения для слайдера
         * @return mixed - результат выполнения запроса к DB
         */
        public function addEntry($langId, $title='', $html='', $images='')
        {
            $table = new Dante_Lib_Orm_Table($this->_tableName);
            $table->lang_id = $langId;
            $table->title = $title;
            $table->html = $html;
            $table->images = $images;
            return $table->apply(array('lang_id' => $langId));
        }

        /**
         * Функция, изменяющая запись в таблице $_tableName
         * @param $langId - ID языка
         * @param $title - заголовок
         * @param $html - html разметка
         * @param $images - изображения для слайдера
         * @return mixed - результат выполнения запроса к DB
         */
        public function changeEntry($langId, $title, $html, $footerHtml, $images, $seoTitle, $seoDescription, $seoKeywords)
        {
            $table = new Dante_Lib_Orm_Table($this->_tableName);
            $exist = $table->exist(array('lang_id' => $langId));


            $table->lang_id = $langId;
            $table->title = $title;
            $table->html = $html;
            $table->footer_html = $footerHtml;
            $table->images = $images;
            $table->seo_title = $seoTitle;
            $table->seo_description = $seoDescription;
            $table->seo_keywords = $seoKeywords;

            if (!$exist) {
                $table->insert();
                return 1;
            }
            else {
                return $table->update(array('lang_id' => $langId));
            }
        }

        /**
         * Функция, получающая записи из таблицы $_servicesTableName
         * @param $langId - ID языка
         * @return array - данные записи
         */

        public function getServicesData($langId)
        {
            $sql = "SELECT id, title, description, draw_order FROM {$this->_servicesTableName}
                WHERE `lang_id` = {$langId} ORDER BY draw_order";
            $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
            $servicesData = array();

            while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
                $servicesData[(int)$f['id']] = array(
                    'id'=>$f['id'],
                    'title'=>$f['title'],
                    'description' => $f['description'],
                    'draw_order' => $f['draw_order']
                );
            }
            return $servicesData;
        }

        /**
         * Функция, добавляющая запись в таблицу $_servicesTableName
         * @param $langId - ID языка
         * @param $title - заголовок
         * @param $description - описание
         * @return mixed - результат выполнения запроса к DB
         */
        public function addService($langId, $title='', $description='')
        {
            $table = new Dante_Lib_Orm_Table($this->_servicesTableName);
            $table->lang_id = $langId;
            $table->title = $title;
            $table->description = $description;
            $table->draw_order = $this->getLastOrder() + 1;

            return $table->insert();
        }

        /**
         * Функция, удаляющая запись из таблицы $_servicesTableName
         * @param $serviceId - ID услуги
         * @return mixed - результат выполнения запроса к DB
         */
        public function deleteService($serviceId)
        {
            $table = new Dante_Lib_Orm_Table($this->_servicesTableName);
            $table->id = $serviceId;


            return $table->delete(array('id' => $serviceId));
        }
        
        public function getService($id)
        {
            $sql = "SELECT id, title, description, draw_order FROM {$this->_servicesTableName} WHERE `id` = {$id}";
            return Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
        }

        /**
         * Функция, возвращающая поле 'draw_order' для последней записи из таблицы $_servicesTableName
         * @return mixed - результат выполнения запроса к DB
         */

        public function getLastOrder()
        {
            $sql = "SELECT `draw_order` FROM {$this->_servicesTableName} ORDER BY `draw_order` DESC LIMIT 1";
            return Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'draw_order');
        }

        public function updateService($serviceId, $title, $description)
        {
            $table = new Dante_Lib_Orm_Table($this->_servicesTableName);
            $table->title = $title;
            $table->description = $description;

            return $table->update(array('id' => $serviceId));
        }

        /**
         * меняем порядок отображения услуги
         * @param int $id
         * @param int $orderFactor
         */
        public function changeOrder($serviceId, $drawOrder, $orderFactor)
        {
//            $switchRow = $this->getServiceRow($serviceId, $drawOrder+$orderFactor);
//            if(!$switchRow || !$switchRow->draw_order ){
//                return;
//            }
//
//            $table = new Dante_Lib_Orm_Table($this->_servicesTableName);
//            $table->draw_order = $drawOrder+$orderFactor;
//            $table->apply(array(
//                'id' => $serviceId,
//                'draw_order' => $drawOrder
//            ));
//
//            $table = new Dante_Lib_Orm_Table($this->_servicesTableName);
//            $table->draw_order = $drawOrder;
//            $table->apply(array(
//
//                'id' => $switchRow->id,
//                'lang_id' =>$switchRow->lang_id,
//                'title' => $switchRow->title,
//                'description' =>$switchRow->description,
//                'draw_order' =>$switchRow->draw_order
//            ));

            $table = new Dante_Lib_Orm_Table($this->_servicesTableName);
            $drawOrder = $drawOrder + $orderFactor;
            $table->id = $serviceId;
            $table->draw_order = $drawOrder;

            return $table->apply();
        }

        /**
         * выбираем услугу с заданным порядком отображения
         * @param int $serviceId
         * @param int $drawOrder
         * @return Dante_Lib_Orm_Table
         */
        public function getServiceRow($serviceId, $drawOrder)
        {
            $table = new Dante_Lib_Orm_Table($this->_servicesTableName);
            $table->get()->where(array('draw_order'=>$drawOrder))
                ->limit(array(0, 1))->orderby('draw_order desc');
            $table->fetch();

            return $table;
        }

        /**
         * Выбрать изображения для галереи
         * @return Dante_Lib_Orm_Table
         */
        public function getImages()
        {
            $sql = "SELECT * FROM ".$this->_imagesTableName." ORDER BY draw_order ASC";
            $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
            $images = array();
            while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
                $images[] = $f;
            }
            return $images;
        }

        public function addImage($image)
        {
            $table = new Dante_Lib_Orm_Table($this->_imagesTableName);
            $table->image = $image;
            $table->draw_order = $this->getLastImgOrder()+1;
            $table->insert();
        }

        public function delImage($imageId)
        {
            $table = new Dante_Lib_Orm_Table($this->_imagesTableName);
            $table->delete(array('id' => $imageId ));
        }

        public function changeImgOrder($imageId, $drawOrder, $orderFactor)
        {
            $switchRow = $this->getImageRow($imageId, $drawOrder+$orderFactor);
            if(!$switchRow || !$switchRow->draw_order ){
                return;
            }

            $table = new Dante_Lib_Orm_Table($this->_imagesTableName);
            $table->draw_order = $drawOrder+$orderFactor;
            $table->apply(array(
                'id' => $imageId,
                'draw_order' => $drawOrder
            ));

            $table = new Dante_Lib_Orm_Table($this->_imagesTableName);
            $table->draw_order = $drawOrder;
            $table->apply(array(
                'id' => $switchRow->id,
                'image' =>$switchRow->image,
                'draw_order' =>$switchRow->draw_order
            ));

        }

        public function getLastImgOrder()
        {
            $sql = "SELECT `draw_order` FROM ".$this->_imagesTableName." ORDER BY `draw_order` DESC LIMIT 1";
            return Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'draw_order');
        }

        public function getImageRow($imageId, $drawOrder)
        {
            $table = new Dante_Lib_Orm_Table($this->_imagesTableName);
            $table->get()->where(array('draw_order'=>$drawOrder))
                ->limit(array(0, 1))->orderby('draw_order desc');
            $table->fetch();

            return $table;
        }



    }