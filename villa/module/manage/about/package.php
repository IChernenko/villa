<?php
/**
 * Модуль, реализующий редактирование страницы "Об Отеле"
 * в административной части сайта
 * Path: /villa/module/manage/about/
 * Name: villa.module.manage.about
 * Current version: 3
 */
global $package;
$package = array(
    'version' => '7',
    'name' => 'villa.module.manage.about',
    'dependence' => array(),
    'js' => array(
        '../villa/module/manage/about/js/manage_about.js',
        '../villa/module/manage/about/component/ckeditor/ckeditor.js'
    )
);
?>