<?php
/**
 * Создание записей в таблице tst_villa_about_page
 * для всех языковых версий страницы "Об Отеле"
 */

    $aboutMapper = new Villa_Module_Manage_About_Mapper();
    $langMapper = new Module_Lang_Mapper();
    $langArr = $langMapper->getList();
    foreach($langArr as $langId=>$id)
    {
        $aboutMapper->addEntry($langId);
    }
?>