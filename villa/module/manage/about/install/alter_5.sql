/*
 * Создание таблицы tst_villa_about_page_services для хранения данных об услугах на странице
*/
CREATE TABLE `[%%]villa_about_page_services`(
    `id` INT(11) UNSIGNED AUTO_INCREMENT NOT NULL,
    `lang_id` INT(11) UNSIGNED NOT NULL,
    `title` VARCHAR(255),
    `description` TEXT,
    `draw_order` TINYINT(2),
    PRIMARY KEY(`id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `[%%]villa_about_page_services` ADD CONSTRAINT `fk_villa_about_page_services_lang_id`
    FOREIGN KEY (`lang_id`) REFERENCES `[%%]languages`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;