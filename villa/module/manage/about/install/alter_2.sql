/*
 * Создание таблицы tst_villa_about_page для хранения данных о странице
 * (Заголовок, html-код, изображения для галереи) на всех существующих
 * на сайте языках. (По одной версии для каждого языка)
*/
CREATE TABLE `[%%]villa_about_page`(
    `lang_id` INT(11) UNSIGNED NOT NULL,
    `title` VARCHAR(255),
    `html` TEXT,
    `images` TEXT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `[%%]villa_about_page` ADD CONSTRAINT `fk_villa_about_page_lang_id`
    FOREIGN KEY (`lang_id`) REFERENCES `[%%]languages`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;