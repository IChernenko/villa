<?php
/**
 * Правило маршрутизации для url раздела редактирования
 * страницы "Об Отеле" в административной части сайта.
 * (Модуль /villa/module/manage/about/)
 */
Dante_Lib_Router::addRule('/^\/manage\/about\-page\/$/', array(
    'controller' => 'Villa_Module_Manage_About_Controller'
));
?>
