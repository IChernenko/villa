<?php
/**
 * Class Villa_Module_Manage_About_View
 * Класс представления модуля Manage_About
 */
    class Villa_Module_Manage_About_View
    {
        public $lang;
        public $langList = array();
        public $servicesData;
        public function drawEditor($pageData)
        {
            $tpl = new Dante_Lib_Template();
            $filePath = '../villa/module/manage/about/template/editor.html';
            $tpl->pageData = $pageData;
            $tpl->lang = $this->lang;
            $tpl->langList = $this->langList;
            $tpl->services = $this->servicesData;
            return $tpl->draw($filePath);
        }

        public function drawImages($images)
        {
            $tplFileName = '../villa/module/manage/about/template/editor_img.html';
            $tpl = new Dante_Lib_Template();
            $tpl->images = $images;

            return $tpl->draw($tplFileName);
        }
    }
?>