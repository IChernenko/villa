<?php

function writeTest($somecontent){
    $filename = 'logs/test.txt';
    // Let's make sure the file exists and is writable first.
    if (is_writable($filename)) {

        // In our example we're opening $filename in append mode.
        // The file pointer is at the bottom of the file hence 
        // that's where $somecontent will go when we fwrite() it.
        if (!$handle = fopen($filename, 'a')) {
             echo "Cannot open file ($filename)";
             exit;
        }

        // Write $somecontent to our opened file.
        if (fwrite($handle, $somecontent) === FALSE) {
            echo "Cannot write to file ($filename)";
            exit;
        }

        echo "Success, wrote ($somecontent) to file ($filename)";

        fclose($handle);

    } else {
        echo "The file $filename is not writable";
    }
}

function writeArray($array, $key = -1){
    writeTest("\n");
    if($key != -1){
        $key = $key.' => ';
        writeTest($key);
    }
    
    if(is_array($array)){
        foreach ($array as $key => $value) {
            writeArray($value, $key);
        }
    }
    
    if(is_string($array)){
        writeTest($array.'; ');
        writeTest("\n");
    }
}

$somecontent = "Add this to the file\n";
$testArray = array(
    1 => '1111111',
    2=> '222222',
    3=> array('123123','234234234')
);

writeTest("POST \n");
writeArray($_POST);

writeTest("GET \n");
writeArray($_GET);

?>