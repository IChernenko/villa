<?php
/**
 * Description of controller
 *
 * @author Valkyria
 */
class Villa_App_Registration_Controller extends Module_Registration_Controller
{
    protected function _instanceService() 
    {
        return new Villa_App_Registration_Service();
    }
}

?>
