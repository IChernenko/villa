<?php
/**
 * Description of service
 *
 * @author Valkyria
 */
class Villa_App_Registration_Service extends Module_Registration_Service
{
    public function sendRegEmail(Module_User_Model $user)
    {
        $builder = Villa_Module_Mail_Builder_Factory::create(MAIL_TYPE_REGISTRATION);
        $builder->setModel();
        $data = array(
            'email' => $user->email,
            'password' => $user->password
        );
        $builder->setData($data);
        
        $mailModel = $builder->buildMessage();
        
        $service = new Module_Mail_Service_Sender();        
        $service->send($user->email, $mailModel->subject, $mailModel->message);
    }
}

?>
