<?php
/**
 * Description of observer
 *
 * @author Valkyria
 */
class Villa_App_Registration_Observer implements Dante_Lib_Observer_Iobserver
{
    public function notify($objSource, $strEventType) 
    {
        $auth = new Module_Auth_Service();
        $auth->login(clone $objSource);
    }
            
}
