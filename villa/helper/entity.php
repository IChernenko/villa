<?php
/**
 * Description of entity
 *
 * @author Valkyria
 */
class Villa_Helper_Entity 
{   
    private static $_roomTypeId;
    private static $_servId;
    private static $_transId;
    private static $_excursId;
    private static $_homepageId;

    private static function _instanceMapper()
    {
        return new Module_Entity_Mapper();
    }
    
    public static function getRoomType()
    {
        if(!isset(self::$_roomTypeId))
            self::$_roomTypeId = self::_instanceMapper()->getBySysName('room_type')->id;
        
        return self::$_roomTypeId;
    }
    
    public static function getService()
    {
        if(!isset(self::$_servId))
            self::$_servId = self::_instanceMapper()->getBySysName('service')->id;
        
        return self::$_servId;
    }
    
    public static function getTransfer()
    {
        if(!isset(self::$_transId))
            self::$_transId = self::_instanceMapper()->getBySysName('transfer')->id;
        
        return self::$_transId;
    }  
    
    public static function getExcursion()
    {
        if(!isset(self::$_excursId))
            self::$_excursId = self::_instanceMapper()->getBySysName('excursion')->id;
        
        return self::$_excursId;
    }
    
    public static function getHomepage()
    {
        if(!isset(self::$_homepageId))
            self::$_homepageId = self::_instanceMapper()->getBySysName('homepage')->id;
        
        return self::$_homepageId;
    }
}

?>
