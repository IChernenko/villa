<?php

// скрипт для автоматической установки статусов брони

/*
 * Example to run:
 *
 * env=dev host=villa.dante.loc php bookingstatus.php
 *
 */


ini_set('default_charset', 'UTF-8');



define('ROOT_PATH', '../');


$_SERVER['REQUEST_URI'] = '';
$_SERVER['HTTP_HOST'] = getenv('host');
$_SERVER['REMOTE_ADDR'] = 'localhost';
$_SERVER['SERVER_NAME'] = 'villa.dante.loc';

include_once(ROOT_PATH.'dante/bootstrap.php');



try {
    $service = new Villa_Module_Hotel_Service_Manage_Booking();
    $service->autoSetStatusAction();

    echo("Done!");
} catch (Exception $e) {

    echo 'Exception : '.$e->getMessage();
}
