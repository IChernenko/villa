<?php
/**
 * Created by PhpStorm.
 * User: dorian
 * Date: 7/9/16
 * Time: 4:19 PM
 */


define('ROOT_PATH', '../');

$_SERVER['HTTP_HOST'] = 'villa.dante.loc';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

include_once(ROOT_PATH.'dante/bootstrap.php');

$userMapper = new Villa_Module_User_Manage_Mapper();
$uid = 20;
$user = $userMapper->getUser($uid);

//print_r($user);

$service = new Villa_Module_Hotel_Service_Booking();
$service->setMode(BOOKING_MAIL_MODE_TOURAGENT);
$service->notifyUser($user, 216);