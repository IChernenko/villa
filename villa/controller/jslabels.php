<?php
/**
 * Description of jslabels
 *
 * @author Sonya
 */
class Villa_Controller_Jslabels extends Dante_Controller_Base
{
    protected function _defaultAction() 
    {
        $labels = array(
            'wrongGuestData',
            'serverError',
            'message',
            'error',
            'yourBookingAdded',
            'fillAllTransferFields',
            'noArriveDate',
            'noDepartureDate',
            'chooseRoomAndPrepayment',
            'fileDeleteSuccess',
            'changesApplied',
            'confirmCancel',
            'bookingCancelEnterPass',
            'continueLbl',
            'enterSystem',
            'registration',
            'yourEmail',
            'yourPassword',
            'doEnter',
            'forgotPassword',
            'yourPassword7',
            'repeatPassword',
            'doRegister',
            'registeredEmail',
            'sendNewPass',
            'saveEmailAndPass',
            'enterEmail',
            'enterPassword',
            'wrongEmail',
            'passNotEqual',
            'personalData',
            'changeEmail',
            'changePassword',
            'name',
            'lastName',
            'phone',
            'postalIndex',
            'country',
            'town',
            'address',
            'workCompany',
            'save',
            'curEmail',
            'newEmail',
            'curPass',
            'newPass',
            'enterEmail',
            'wrongEmail',
            'enterCurPassword',
            'enterNewPassword',
            'repeatNewPassword',
            'youCantChangeArrival',
            'pressToChangeArrival',
            'enterAnotherArrival',
            'saving',
            'dontForgetToResetTransfers'
        );

        $html = 'villa.labels = {';
        $i = 0;
        foreach($labels as $label)
        {
            $html .= $label.': "'.addslashes(Module_Lang_Helper::translate($label)).'"';
            $i++;
            if($i<count($labels)) $html .= ',';
        }
        $html .= '};';

        return '<script>'.$html.'</script>';
    }
}
