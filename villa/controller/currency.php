<?php
/**
 * Description of currency
 *
 * @author Valkyria
 */
class Villa_Controller_Currency extends Module_Currency_Controller
{
    protected function _defaultAction()
    {
        $mapper = new Module_Currency_Mapper();
        $session = new Dante_Lib_Session();
        $list = $mapper->getList();
        if($session->get('currency'))
        {
            $current = $session->get('currency');
        }
        else
        {
            $currentId = Villa_Module_Countries_Helper::getCurrencyId();
            if($currentId)
                $current = $mapper->getById($currentId);
            else
                $current = $mapper->getDefaultCurrency();

            $session->set('currency', $current);
        }

        $view = new Module_Currency_View();
        return $view->draw($list, $current);
    }
}

?>
