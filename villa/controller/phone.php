<?php
/**
 * Description of phone
 *
 * @author Valkyria
 */
class Villa_Controller_Phone extends Dante_Controller_Base
{
    protected function _defaultAction() 
    {
        return Villa_Module_Countries_Helper::getPhone();
    }
}

?>
