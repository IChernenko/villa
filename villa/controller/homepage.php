<?php
class Villa_Controller_Homepage extends Dante_Controller_Base
{
    protected function _defaultAction()
    {
        
    }

    protected function _drawAboutAction()
    {
        $publicMapper = new Module_Publications_Mapper();
        $gallMapper = new Module_Gallery_Mapper();
        $entMapper = new Module_Entity_Mapper();
        $optMapper = new Villa_Module_Hotel_Mapper_Manage_Booking();
        $entType = $entMapper->getBySysName('publication')->id;
        
        $lang = Module_Lang_Helper::getCurrent();
        // Module_Currencycountries_Manage_Mapper
        
        $publication = $publicMapper->getByLink('about.html', $lang);
        if(!$publication) return '';
        
        $gallery = $gallMapper->getByEntity($entType, $publication->id);        
        
        $view = new Villa_Module_Homepage_View();
        if($this->_getRequest()->get('gallery')) 
        {
            $files = array();
            foreach($gallery as $img)
            {
                $src = Dante_Lib_Config::get('publication.imgDir').$img['file_name'];
                $files[] = $src;
            }
            return $files;
        }
        
        $view->publication = $publication;

        $view->gallery = $gallery;
        $view->options = $optMapper->getBookingOptionsSamplesLang(false, $lang);
        $view->transfer = $optMapper->getTransferOptionsSamplesLang(false, $lang);        
        
        return $view->drawAbout();
    }
}

?>