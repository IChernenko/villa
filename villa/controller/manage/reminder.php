<?php
/**
 * Description of reminder
 *
 * @author Valkyria
 */
class Villa_Controller_Manage_Reminder extends Dante_Controller_Base
{
    protected function _defaultAction() 
    {
        if(Dante_Helper_App::getWsName() != 'admin')
            return array(
                'result' => 0
            );        
                
        $commentMapper = new Villa_Module_Comment_Manage_Mapper();
        $commentCount = $commentMapper->getNewCommentsCount();
        $questCount = $commentMapper->getNewQuestionsCount();
        
        $bookingMapper = new Villa_Module_Hotel_Mapper_Manage_Booking();
        $bookingCount = $bookingMapper->getNewBookingsCount();
        
        $transferMapper = new Villa_Module_Hotel_Mapper_Manage_Bookingtransfer();
        $transferCount = $transferMapper->getTransfersToRemind();
        
        if(!$questCount && !$bookingCount && !$commentCount && !$transferCount)
            return array(
                'result' => 0
            );
        
        return array(
            'result' => 1,
            'questions' => $questCount,
            'bookings' => $bookingCount,
            'comments' => $commentCount,
            'transfers' => $transferCount
        );
    }
}

?>
