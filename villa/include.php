<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian, elrafir
 * Date: 04.06.12
 * Time: 19:29
 * To change this template use File | Settings | File Templates.
 */
Dante_Lib_Config::set('registration.activateAfterReg', true);

Dante_Lib_Config::set('paypal.business', 'villalascala@gmail.com');
Dante_Lib_Config::set('paypal.currency', 'USD');

Dante_Lib_Config::set('defaultController', 'villa.module.homepage.controller');
Dante_Lib_Config::set('publication.allowGallery', true);
Dante_Lib_Config::set('publication.allowImage', false);

Dante_Lib_Config::set('slider.allowSettingsEdit', false);
Dante_Lib_Config::set('slider.allowDescr', true);
Dante_Lib_Config::set('slider.allowUrl', false);

Dante_Lib_Config::set('passwordchange.fromName', 'Villa LaScala');
Dante_Lib_Config::set('passwordchange.fromEmail', 'noreply@villa.ua');

Dante_Lib_Config::set('hotel.peopleinnumber', 2);

Dante_Lib_Config::set('lang.allowImage', true);
//Dante_Helper_Money::round($num);

Dante_Lib_Config::set('liqpay2.order_link', 'javascript:booking.editFromOutside({id})');

Dante_Lib_Observer_Helper::addObserver(new Villa_App_Registration_Observer(), 'registration.done');
Dante_Lib_Observer_Helper::addObserver(new Villa_App_Sync_Observer_Booking(), 'new_booking');
Dante_Lib_Observer_Helper::addObserver(new Villa_App_Sync_Observer_Booking(), 'edit_booking');
Dante_Lib_Observer_Helper::addObserver(new Villa_App_Sync_Observer_Option(), 'new_booking');
Dante_Lib_Observer_Helper::addObserver(new Villa_App_Sync_Observer_Payment(), 'internet_payment');
Dante_Lib_Observer_Helper::addObserver(new Villa_App_Sync_Observer_Currency(), 'edit_exchange');

// seo
Dante_Lib_Observer_Helper::addObserver(new \Villa_Observer_Seo(), 'seo.draw.title');
Dante_Lib_Observer_Helper::addObserver(new \Villa_Observer_Seo(), 'seo.draw.keywords');
Dante_Lib_Observer_Helper::addObserver(new \Villa_Observer_Seo(), 'seo.draw.description');
