/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50508
Source Host           : localhost:3306
Source Database       : villa

Target Server Type    : MYSQL
Target Server Version : 50508
File Encoding         : 65001

Date: 2012-06-05 17:33:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `db_version`
-- ----------------------------
DROP TABLE IF EXISTS `db_version`;
CREATE TABLE `db_version` (
  `package` varchar(32) DEFAULT NULL,
  `version` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of db_version
-- ----------------------------
INSERT INTO `db_version` VALUES ('dante', '1');
INSERT INTO `db_version` VALUES ('lib.jdatepicker', '1');
INSERT INTO `db_version` VALUES ('lib.jgrid', '1');
INSERT INTO `db_version` VALUES ('module.auth', '2');
INSERT INTO `db_version` VALUES ('module.lang', '2');
INSERT INTO `db_version` VALUES ('module.hotel', '34');
INSERT INTO `db_version` VALUES ('module.entity', '1');
INSERT INTO `db_version` VALUES ('module.rating', '1');
INSERT INTO `db_version` VALUES ('module.comment', '1');
INSERT INTO `db_version` VALUES ('villa', '1');
INSERT INTO `db_version` VALUES ('component.swfupload', '1');
INSERT INTO `db_version` VALUES ('component.tinymce', '1');
INSERT INTO `db_version` VALUES ('module.menu', '2');
INSERT INTO `db_version` VALUES ('module.division', '12');
INSERT INTO `db_version` VALUES ('component.jalerts', '1');
INSERT INTO `db_version` VALUES ('module.callback', '1');
INSERT INTO `db_version` VALUES ('module.publications', '1');
INSERT INTO `db_version` VALUES ('lib.lightbox', '1');
INSERT INTO `db_version` VALUES ('lib.jquery.ui', '1');
INSERT INTO `db_version` VALUES ('lib.jquery.mousewheel', '1');
INSERT INTO `db_version` VALUES ('lib.jquery.smoothdivscroll', '1');
INSERT INTO `db_version` VALUES ('lib.paypall', '2');
INSERT INTO `db_version` VALUES ('module.labels', '1');

-- ----------------------------
-- Table structure for `tst_comment`
-- ----------------------------
DROP TABLE IF EXISTS `tst_comment`;
CREATE TABLE `tst_comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `entity_type_id` int(11) unsigned NOT NULL,
  `entity_id` int(11) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id`),
  KEY `fk_comment_entity_type_id` (`entity_type_id`),
  CONSTRAINT `fk_comment_entity_type_id` FOREIGN KEY (`entity_type_id`) REFERENCES `tst_entity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Комментарии';

-- ----------------------------
-- Records of tst_comment
-- ----------------------------

-- ----------------------------
-- Table structure for `tst_division`
-- ----------------------------
DROP TABLE IF EXISTS `tst_division`;
CREATE TABLE `tst_division` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `parent_id` int(11) unsigned NOT NULL,
  `link` varchar(128) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `draw_order` int(11) DEFAULT NULL,
  `d_type` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_division_parent_id` (`parent_id`),
  CONSTRAINT `fk_division_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `tst_division` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COMMENT='Категории';

-- ----------------------------
-- Records of tst_division
-- ----------------------------
INSERT INTO `tst_division` VALUES ('1', 'root', '1', null, null, '1', '');
INSERT INTO `tst_division` VALUES ('2', 'adminka', '1', null, null, '1', '');
INSERT INTO `tst_division` VALUES ('3', 'client', '1', null, null, '2', '');
INSERT INTO `tst_division` VALUES ('20', 'Номера', '2', '/manage/apartments/', null, '3', '');
INSERT INTO `tst_division` VALUES ('21', 'Типы номеров', '2', '/manage/apartmentstypes/', null, '4', '');
INSERT INTO `tst_division` VALUES ('22', 'Разделы', '2', '/manage/division/', null, '1', '');
INSERT INTO `tst_division` VALUES ('23', 'Комментарии', '2', '/manage/comment/', null, '2', '');
INSERT INTO `tst_division` VALUES ('24', 'Бронирование', '2', '/manage/booking/', null, '5', '');
INSERT INTO `tst_division` VALUES ('25', 'Опции бронирования', '2', '/manage/bookingoptions/', null, '6', '');
INSERT INTO `tst_division` VALUES ('26', 'Загрузка отеля', '2', '/manage/loading/', null, '7', '');
INSERT INTO `tst_division` VALUES ('27', 'Кабинет', '2', '/manage/cashcontrol/', null, '8', '');
INSERT INTO `tst_division` VALUES ('28', 'Кабинет Ген Директора', '2', '/manage/cashgendir/', null, '9', '');
INSERT INTO `tst_division` VALUES ('29', 'Звонки', '2', '/manage/callback/', null, '10', '');
INSERT INTO `tst_division` VALUES ('30', 'Публикации', '2', '/manage/publications/', null, '11', '');
INSERT INTO `tst_division` VALUES ('31', 'Оборудование', '2', '/manage/equipment/', null, '11', '');
INSERT INTO `tst_division` VALUES ('32', 'логи paypall', '2', '/manage/paylogs/', null, '12', '');
INSERT INTO `tst_division` VALUES ('33', 'Метки', '2', '/manage/labels/', null, '13', '');
INSERT INTO `tst_division` VALUES ('34', 'Типы предоплат', '2', '/manage/bookingprepayment/', null, '14', '');

-- ----------------------------
-- Table structure for `tst_division_groups`
-- ----------------------------
DROP TABLE IF EXISTS `tst_division_groups`;
CREATE TABLE `tst_division_groups` (
  `division_id` int(11) unsigned DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  KEY `fk_division_groups_division_id` (`division_id`),
  KEY `fk_division_groups_group_id` (`group_id`),
  CONSTRAINT `fk_division_groups_division_id` FOREIGN KEY (`division_id`) REFERENCES `tst_division` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_division_groups_group_id` FOREIGN KEY (`group_id`) REFERENCES `tst_groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='соответствие категорий и групп пользователей';

-- ----------------------------
-- Records of tst_division_groups
-- ----------------------------
INSERT INTO `tst_division_groups` VALUES ('22', '3');
INSERT INTO `tst_division_groups` VALUES ('23', '1');
INSERT INTO `tst_division_groups` VALUES ('23', '2');
INSERT INTO `tst_division_groups` VALUES ('23', '3');
INSERT INTO `tst_division_groups` VALUES ('20', '1');
INSERT INTO `tst_division_groups` VALUES ('20', '2');
INSERT INTO `tst_division_groups` VALUES ('20', '3');
INSERT INTO `tst_division_groups` VALUES ('21', '1');
INSERT INTO `tst_division_groups` VALUES ('21', '2');
INSERT INTO `tst_division_groups` VALUES ('21', '3');
INSERT INTO `tst_division_groups` VALUES ('24', '1');
INSERT INTO `tst_division_groups` VALUES ('24', '2');
INSERT INTO `tst_division_groups` VALUES ('24', '3');
INSERT INTO `tst_division_groups` VALUES ('25', '1');
INSERT INTO `tst_division_groups` VALUES ('25', '2');
INSERT INTO `tst_division_groups` VALUES ('25', '3');
INSERT INTO `tst_division_groups` VALUES ('26', '1');
INSERT INTO `tst_division_groups` VALUES ('26', '2');
INSERT INTO `tst_division_groups` VALUES ('26', '3');
INSERT INTO `tst_division_groups` VALUES ('28', '3');
INSERT INTO `tst_division_groups` VALUES ('27', '2');
INSERT INTO `tst_division_groups` VALUES ('27', '3');
INSERT INTO `tst_division_groups` VALUES ('29', '3');
INSERT INTO `tst_division_groups` VALUES ('29', '1');
INSERT INTO `tst_division_groups` VALUES ('29', '2');
INSERT INTO `tst_division_groups` VALUES ('30', '3');
INSERT INTO `tst_division_groups` VALUES ('30', '1');
INSERT INTO `tst_division_groups` VALUES ('30', '2');
INSERT INTO `tst_division_groups` VALUES ('31', '3');
INSERT INTO `tst_division_groups` VALUES ('31', '1');
INSERT INTO `tst_division_groups` VALUES ('31', '2');
INSERT INTO `tst_division_groups` VALUES ('32', '3');
INSERT INTO `tst_division_groups` VALUES ('33', '3');
INSERT INTO `tst_division_groups` VALUES ('34', '3');

-- ----------------------------
-- Table structure for `tst_division_publications`
-- ----------------------------
DROP TABLE IF EXISTS `tst_division_publications`;
CREATE TABLE `tst_division_publications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `division_id` int(11) unsigned NOT NULL,
  `publications_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_division_publications_division_id` (`division_id`),
  KEY `fk_division_publications_publications_id` (`publications_id`),
  CONSTRAINT `fk_division_publications_division_id` FOREIGN KEY (`division_id`) REFERENCES `tst_division` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_division_publications_publications_id` FOREIGN KEY (`publications_id`) REFERENCES `tst_publications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='соответствие категорий и публикаций';

-- ----------------------------
-- Records of tst_division_publications
-- ----------------------------

-- ----------------------------
-- Table structure for `tst_division_seo`
-- ----------------------------
DROP TABLE IF EXISTS `tst_division_seo`;
CREATE TABLE `tst_division_seo` (
  `menu_id` int(11) unsigned DEFAULT NULL,
  `lang_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` text,
  KEY `fk_division_seo_lang_id` (`lang_id`),
  CONSTRAINT `fk_division_seo_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='SEO данные категорий';

-- ----------------------------
-- Records of tst_division_seo
-- ----------------------------
INSERT INTO `tst_division_seo` VALUES ('20', '1', 'asd', 'asd', 'asd', '');
INSERT INTO `tst_division_seo` VALUES ('21', '1', 'asdfsdf', 'asdfadsfg', 'sdfgdsfg', 'dsfgdsfg');
INSERT INTO `tst_division_seo` VALUES ('22', '1', '', '', '', '');
INSERT INTO `tst_division_seo` VALUES ('23', '1', '', '', '', '');
INSERT INTO `tst_division_seo` VALUES ('24', '1', '', '', '', '');
INSERT INTO `tst_division_seo` VALUES ('25', '1', '', '', '', '');
INSERT INTO `tst_division_seo` VALUES ('26', '1', '', '', '', '');
INSERT INTO `tst_division_seo` VALUES ('27', '1', '', '', '', '');
INSERT INTO `tst_division_seo` VALUES ('28', '1', '', '', '', '');
INSERT INTO `tst_division_seo` VALUES ('29', '1', '', '', '', '');
INSERT INTO `tst_division_seo` VALUES ('30', '1', '', '', '', '');
INSERT INTO `tst_division_seo` VALUES ('31', '1', '', '', '', '');
INSERT INTO `tst_division_seo` VALUES ('32', '1', '', '', '', '');
INSERT INTO `tst_division_seo` VALUES ('33', '1', '', '', '', '');
INSERT INTO `tst_division_seo` VALUES ('34', '1', '', '', '', '');

-- ----------------------------
-- Table structure for `tst_entity`
-- ----------------------------
DROP TABLE IF EXISTS `tst_entity`;
CREATE TABLE `tst_entity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `sys_name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Сущности';

-- ----------------------------
-- Records of tst_entity
-- ----------------------------

-- ----------------------------
-- Table structure for `tst_groups`
-- ----------------------------
DROP TABLE IF EXISTS `tst_groups`;
CREATE TABLE `tst_groups` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(32) DEFAULT NULL,
  `group_caption` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Группы пользователей';

-- ----------------------------
-- Records of tst_groups
-- ----------------------------
INSERT INTO `tst_groups` VALUES ('1', 'moder', 'Модератор');
INSERT INTO `tst_groups` VALUES ('2', 'admin', 'Администратор');
INSERT INTO `tst_groups` VALUES ('3', 'gendir', 'Ген Директор');
INSERT INTO `tst_groups` VALUES ('4', 'user', 'Пользователь');
INSERT INTO `tst_groups` VALUES ('5', 'guest', 'Гость');

-- ----------------------------
-- Table structure for `tst_hotel_apartments`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_apartments`;
CREATE TABLE `tst_hotel_apartments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `people` tinyint(2) NOT NULL,
  `childs` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hotel_apartments_type` (`type`),
  CONSTRAINT `fk_hotel_apartments_type` FOREIGN KEY (`type`) REFERENCES `tst_hotel_apartmentstypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='номера';

-- ----------------------------
-- Records of tst_hotel_apartments
-- ----------------------------
INSERT INTO `tst_hotel_apartments` VALUES ('1', '3', '2', '2');
INSERT INTO `tst_hotel_apartments` VALUES ('2', '4', '2', '2');
INSERT INTO `tst_hotel_apartments` VALUES ('3', '3', '0', '0');
INSERT INTO `tst_hotel_apartments` VALUES ('4', '4', '0', '0');
INSERT INTO `tst_hotel_apartments` VALUES ('5', '5', '0', '0');
INSERT INTO `tst_hotel_apartments` VALUES ('6', '3', '0', '0');
INSERT INTO `tst_hotel_apartments` VALUES ('7', '4', '0', '0');

-- ----------------------------
-- Table structure for `tst_hotel_apartmentstypes`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_apartmentstypes`;
CREATE TABLE `tst_hotel_apartmentstypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `link` varchar(50) NOT NULL,
  `cost` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='типы номеров';

-- ----------------------------
-- Records of tst_hotel_apartmentstypes
-- ----------------------------
INSERT INTO `tst_hotel_apartmentstypes` VALUES ('3', 'Стандарт', 'standart', '500');
INSERT INTO `tst_hotel_apartmentstypes` VALUES ('4', 'Люкс', 'lux', '300');
INSERT INTO `tst_hotel_apartmentstypes` VALUES ('5', 'ПолуЛюкс', 'half-lux', '200');

-- ----------------------------
-- Table structure for `tst_hotel_apartmentstypes_equipment`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_apartmentstypes_equipment`;
CREATE TABLE `tst_hotel_apartmentstypes_equipment` (
  `apt_id` int(11) NOT NULL,
  `eq_id` int(11) NOT NULL,
  KEY `fk_hotel_apartmentstypes_equipment_apt_id` (`apt_id`),
  KEY `fk_hotel_apartmentstypes_equipment_eq_id` (`eq_id`),
  CONSTRAINT `fk_hotel_apartmentstypes_equipment_apt_id` FOREIGN KEY (`apt_id`) REFERENCES `tst_hotel_apartmentstypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_hotel_apartmentstypes_equipment_eq_id` FOREIGN KEY (`eq_id`) REFERENCES `tst_hotel_equipment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tst_hotel_apartmentstypes_equipment
-- ----------------------------
INSERT INTO `tst_hotel_apartmentstypes_equipment` VALUES ('3', '1');
INSERT INTO `tst_hotel_apartmentstypes_equipment` VALUES ('3', '3');

-- ----------------------------
-- Table structure for `tst_hotel_apartmentstypes_images`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_apartmentstypes_images`;
CREATE TABLE `tst_hotel_apartmentstypes_images` (
  `type_id` int(11) NOT NULL,
  `image` varchar(50) NOT NULL,
  `draw_order` tinyint(2) NOT NULL,
  KEY `fk_hotel_apartmentstypes_images_type_id` (`type_id`),
  CONSTRAINT `fk_hotel_apartmentstypes_images_type_id` FOREIGN KEY (`type_id`) REFERENCES `tst_hotel_apartmentstypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Изображения типов номеров';

-- ----------------------------
-- Records of tst_hotel_apartmentstypes_images
-- ----------------------------
INSERT INTO `tst_hotel_apartmentstypes_images` VALUES ('3', 'ava.jpg', '1');
INSERT INTO `tst_hotel_apartmentstypes_images` VALUES ('3', 'cosplay.jpg', '2');
INSERT INTO `tst_hotel_apartmentstypes_images` VALUES ('3', 'drogon_girl.jpg', '3');
INSERT INTO `tst_hotel_apartmentstypes_images` VALUES ('3', 'Auto_Lamborghini__006044_.jpg', '4');
INSERT INTO `tst_hotel_apartmentstypes_images` VALUES ('3', 'Subaru_Legacy.jpg', '5');
INSERT INTO `tst_hotel_apartmentstypes_images` VALUES ('3', '1.jpg', '6');

-- ----------------------------
-- Table structure for `tst_hotel_apartmentstypes_lang`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_apartmentstypes_lang`;
CREATE TABLE `tst_hotel_apartmentstypes_lang` (
  `id` int(11) NOT NULL,
  `lang_id` int(11) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  KEY `fk_hotel_apartmentstypes_lang_lang_id` (`lang_id`),
  KEY `fk_hotel_apartmentstypes_lang_id` (`id`),
  CONSTRAINT `fk_hotel_apartmentstypes_lang_id` FOREIGN KEY (`id`) REFERENCES `tst_hotel_apartmentstypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_hotel_apartmentstypes_lang_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='локализация типов номеров';

-- ----------------------------
-- Records of tst_hotel_apartmentstypes_lang
-- ----------------------------
INSERT INTO `tst_hotel_apartmentstypes_lang` VALUES ('3', '1', 'Стандарт', '<p>Стандартный номер</p>');
INSERT INTO `tst_hotel_apartmentstypes_lang` VALUES ('3', '2', '45435', '<p>435345</p>');
INSERT INTO `tst_hotel_apartmentstypes_lang` VALUES ('4', '1', 'Люкс', '<p>люксовый номер</p>');
INSERT INTO `tst_hotel_apartmentstypes_lang` VALUES ('5', '1', 'ПолуЛюкс', '<p>полулюксовый номер</p>');

-- ----------------------------
-- Table structure for `tst_hotel_booking`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_booking`;
CREATE TABLE `tst_hotel_booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `arrival` int(11) NOT NULL,
  `departure` int(11) NOT NULL,
  `people` int(3) NOT NULL,
  `comment` text NOT NULL,
  `prepayment` int(11) NOT NULL,
  `cost` float(8,2) NOT NULL,
  `apartment` int(11) NOT NULL,
  `fio` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hotel_booking_type` (`type`),
  KEY `fk_hotel_booking_apartment` (`apartment`),
  KEY `fk_hotel_booking_prepayment` (`prepayment`),
  CONSTRAINT `fk_hotel_booking_apartment` FOREIGN KEY (`apartment`) REFERENCES `tst_hotel_apartments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_hotel_booking_prepayment` FOREIGN KEY (`prepayment`) REFERENCES `tst_hotel_booking_prepayment_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_hotel_booking_type` FOREIGN KEY (`type`) REFERENCES `tst_hotel_apartmentstypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='бронирование';

-- ----------------------------
-- Records of tst_hotel_booking
-- ----------------------------
INSERT INTO `tst_hotel_booking` VALUES ('10', '3', '1338757200', '1338930000', '0', '', '3', '1060.00', '1', '', '', '');
INSERT INTO `tst_hotel_booking` VALUES ('11', '3', '1338757200', '1338843600', '0', '', '3', '510.00', '1', '', '', '');
INSERT INTO `tst_hotel_booking` VALUES ('12', '3', '1338757200', '1338843600', '0', '', '3', '530.00', '1', '', '', '');
INSERT INTO `tst_hotel_booking` VALUES ('13', '3', '1338757200', '1339448400', '0', '', '3', '4080.00', '1', '', '', '');
INSERT INTO `tst_hotel_booking` VALUES ('14', '3', '1338757200', '1338843600', '0', '', '3', '510.00', '1', '', '', '');

-- ----------------------------
-- Table structure for `tst_hotel_booking_deposit_history`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_booking_deposit_history`;
CREATE TABLE `tst_hotel_booking_deposit_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `summ` int(11) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hotel_booking_deposit_history_booking_id` (`booking_id`),
  KEY `fk_hotel_booking_deposit_history_user_id` (`user_id`),
  CONSTRAINT `fk_hotel_booking_deposit_history_booking_id` FOREIGN KEY (`booking_id`) REFERENCES `tst_hotel_booking` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_hotel_booking_deposit_history_user_id` FOREIGN KEY (`user_id`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='фин история бронирования';

-- ----------------------------
-- Records of tst_hotel_booking_deposit_history
-- ----------------------------

-- ----------------------------
-- Table structure for `tst_hotel_booking_options`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_booking_options`;
CREATE TABLE `tst_hotel_booking_options` (
  `hotel_booking_id` int(11) NOT NULL,
  `sample_id` int(11) NOT NULL,
  KEY `fk_hotel_booking_options_hotel_booking_id` (`hotel_booking_id`),
  CONSTRAINT `fk_hotel_booking_options_hotel_booking_id` FOREIGN KEY (`hotel_booking_id`) REFERENCES `tst_hotel_booking` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='опции бронирования';

-- ----------------------------
-- Records of tst_hotel_booking_options
-- ----------------------------
INSERT INTO `tst_hotel_booking_options` VALUES ('10', '1');
INSERT INTO `tst_hotel_booking_options` VALUES ('10', '2');
INSERT INTO `tst_hotel_booking_options` VALUES ('11', '1');
INSERT INTO `tst_hotel_booking_options` VALUES ('12', '1');
INSERT INTO `tst_hotel_booking_options` VALUES ('12', '2');
INSERT INTO `tst_hotel_booking_options` VALUES ('13', '1');
INSERT INTO `tst_hotel_booking_options` VALUES ('14', '1');

-- ----------------------------
-- Table structure for `tst_hotel_booking_options_samples`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_booking_options_samples`;
CREATE TABLE `tst_hotel_booking_options_samples` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `cost` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='примеры опций бронирования';

-- ----------------------------
-- Records of tst_hotel_booking_options_samples
-- ----------------------------
INSERT INTO `tst_hotel_booking_options_samples` VALUES ('1', 'Завтрак', '10');
INSERT INTO `tst_hotel_booking_options_samples` VALUES ('2', 'Ужин', '20');
INSERT INTO `tst_hotel_booking_options_samples` VALUES ('3', 'Туристическое обслуживание', '30');

-- ----------------------------
-- Table structure for `tst_hotel_booking_options_samples_lang`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_booking_options_samples_lang`;
CREATE TABLE `tst_hotel_booking_options_samples_lang` (
  `id` int(11) NOT NULL,
  `lang_id` int(11) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  KEY `fk_hotel_booking_options_samples_lang_id` (`id`),
  KEY `fk_hotel_booking_options_samples_lang_lang_id` (`lang_id`),
  CONSTRAINT `fk_hotel_booking_options_samples_lang_id` FOREIGN KEY (`id`) REFERENCES `tst_hotel_equipment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_hotel_booking_options_samples_lang_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tst_hotel_booking_options_samples_lang
-- ----------------------------

-- ----------------------------
-- Table structure for `tst_hotel_booking_prepayment_types`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_booking_prepayment_types`;
CREATE TABLE `tst_hotel_booking_prepayment_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='типы предоплаты бронирования';

-- ----------------------------
-- Records of tst_hotel_booking_prepayment_types
-- ----------------------------
INSERT INTO `tst_hotel_booking_prepayment_types` VALUES ('1', 'Без предоплаты');
INSERT INTO `tst_hotel_booking_prepayment_types` VALUES ('2', 'Предоплата за первую ночь');
INSERT INTO `tst_hotel_booking_prepayment_types` VALUES ('3', 'Предоплата за весь период проживания');

-- ----------------------------
-- Table structure for `tst_hotel_booking_prepayment_types_lang`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_booking_prepayment_types_lang`;
CREATE TABLE `tst_hotel_booking_prepayment_types_lang` (
  `id` int(11) NOT NULL,
  `lang_id` int(11) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  KEY `fk_hotel_booking_prepayment_types_lang_id` (`id`),
  KEY `fk_hotel_booking_prepayment_types_lang_lang_id` (`lang_id`),
  CONSTRAINT `fk_hotel_booking_prepayment_types_lang_id` FOREIGN KEY (`id`) REFERENCES `tst_hotel_booking_prepayment_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_hotel_booking_prepayment_types_lang_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tst_hotel_booking_prepayment_types_lang
-- ----------------------------

-- ----------------------------
-- Table structure for `tst_hotel_callback`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_callback`;
CREATE TABLE `tst_hotel_callback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(20) NOT NULL,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='обратный звонок';

-- ----------------------------
-- Records of tst_hotel_callback
-- ----------------------------
INSERT INTO `tst_hotel_callback` VALUES ('1', '546546', '');
INSERT INTO `tst_hotel_callback` VALUES ('2', '45435', '');

-- ----------------------------
-- Table structure for `tst_hotel_cash`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_cash`;
CREATE TABLE `tst_hotel_cash` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `balance` float(10,2) NOT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_hotel_cash_user_id` FOREIGN KEY (`user_id`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='баланс работников отеля';

-- ----------------------------
-- Records of tst_hotel_cash
-- ----------------------------

-- ----------------------------
-- Table structure for `tst_hotel_cash_transactions`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_cash_transactions`;
CREATE TABLE `tst_hotel_cash_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_from` int(11) NOT NULL,
  `user_to` int(11) NOT NULL,
  `summ` float(10,2) NOT NULL,
  `date` int(11) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hotel_cash_transactions_user_from` (`user_from`),
  KEY `fk_hotel_cash_transactions_user_to` (`user_to`),
  CONSTRAINT `fk_hotel_cash_transactions_user_from` FOREIGN KEY (`user_from`) REFERENCES `tst_hotel_cash` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_hotel_cash_transactions_user_to` FOREIGN KEY (`user_to`) REFERENCES `tst_hotel_cash` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='фин операции';

-- ----------------------------
-- Records of tst_hotel_cash_transactions
-- ----------------------------

-- ----------------------------
-- Table structure for `tst_hotel_equipment`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_equipment`;
CREATE TABLE `tst_hotel_equipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tst_hotel_equipment
-- ----------------------------
INSERT INTO `tst_hotel_equipment` VALUES ('1', 'cond');
INSERT INTO `tst_hotel_equipment` VALUES ('2', 'bed');
INSERT INTO `tst_hotel_equipment` VALUES ('3', 'girl');

-- ----------------------------
-- Table structure for `tst_hotel_equipment_lang`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_equipment_lang`;
CREATE TABLE `tst_hotel_equipment_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang_id` int(11) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hotel_equipment_lang_lang_id` (`lang_id`),
  CONSTRAINT `fk_hotel_equipment_lang_id` FOREIGN KEY (`id`) REFERENCES `tst_hotel_equipment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_hotel_equipment_lang_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tst_hotel_equipment_lang
-- ----------------------------
INSERT INTO `tst_hotel_equipment_lang` VALUES ('1', '1', 'кондиционер');
INSERT INTO `tst_hotel_equipment_lang` VALUES ('2', '1', 'кровать');
INSERT INTO `tst_hotel_equipment_lang` VALUES ('3', '1', 'девка');

-- ----------------------------
-- Table structure for `tst_labels`
-- ----------------------------
DROP TABLE IF EXISTS `tst_labels`;
CREATE TABLE `tst_labels` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sys_name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='labels';

-- ----------------------------
-- Records of tst_labels
-- ----------------------------
INSERT INTO `tst_labels` VALUES ('1', 'login');

-- ----------------------------
-- Table structure for `tst_labels_lang`
-- ----------------------------
DROP TABLE IF EXISTS `tst_labels_lang`;
CREATE TABLE `tst_labels_lang` (
  `id` int(11) unsigned DEFAULT NULL,
  `lang_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  KEY `fk_labels_lang_lang_id` (`lang_id`),
  KEY `fk_labels_lang_id` (`id`),
  CONSTRAINT `fk_labels_lang_id` FOREIGN KEY (`id`) REFERENCES `tst_labels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_labels_lang_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='языковые данные labels';

-- ----------------------------
-- Records of tst_labels_lang
-- ----------------------------
INSERT INTO `tst_labels_lang` VALUES ('1', '1', 'Логин');

-- ----------------------------
-- Table structure for `tst_languages`
-- ----------------------------
DROP TABLE IF EXISTS `tst_languages`;
CREATE TABLE `tst_languages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `sys_name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Языки';

-- ----------------------------
-- Records of tst_languages
-- ----------------------------
INSERT INTO `tst_languages` VALUES ('1', 'Русский', 'rus');
INSERT INTO `tst_languages` VALUES ('2', 'English', 'eng');
INSERT INTO `tst_languages` VALUES ('3', 'Deutsch', 'de');

-- ----------------------------
-- Table structure for `tst_paypall_log`
-- ----------------------------
DROP TABLE IF EXISTS `tst_paypall_log`;
CREATE TABLE `tst_paypall_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_type` varchar(50) DEFAULT NULL,
  `payment_date` varchar(50) DEFAULT NULL,
  `payment_status` varchar(50) DEFAULT NULL,
  `address_status` varchar(50) DEFAULT NULL,
  `payer_status` varchar(50) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `payer_email` varchar(50) DEFAULT NULL,
  `payer_id` varchar(50) DEFAULT NULL,
  `address_name` varchar(50) DEFAULT NULL,
  `address_country` varchar(50) DEFAULT NULL,
  `address_country_code` varchar(50) DEFAULT NULL,
  `address_zip` varchar(50) DEFAULT NULL,
  `address_state` varchar(50) DEFAULT NULL,
  `address_city` varchar(50) DEFAULT NULL,
  `address_street` varchar(100) DEFAULT NULL,
  `business` varchar(50) DEFAULT NULL,
  `receiver_email` varchar(50) DEFAULT NULL,
  `receiver_id` varchar(50) DEFAULT NULL,
  `residence_country` varchar(50) DEFAULT NULL,
  `item_name` varchar(50) DEFAULT NULL,
  `item_number` varchar(50) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `shipping` float(10,2) NOT NULL,
  `tax` float(10,2) NOT NULL,
  `mc_currency` varchar(50) DEFAULT NULL,
  `mc_fee` float(10,2) NOT NULL,
  `mc_gross` float(10,2) NOT NULL,
  `txn_type` varchar(50) DEFAULT NULL,
  `txn_id` int(11) DEFAULT NULL,
  `notify_version` varchar(50) DEFAULT NULL,
  `custom` varchar(50) DEFAULT NULL,
  `invoice` varchar(50) DEFAULT NULL,
  `charset` varchar(50) DEFAULT NULL,
  `verify_sign` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='paypall логи';

-- ----------------------------
-- Records of tst_paypall_log
-- ----------------------------

-- ----------------------------
-- Table structure for `tst_publications`
-- ----------------------------
DROP TABLE IF EXISTS `tst_publications`;
CREATE TABLE `tst_publications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='публикации';

-- ----------------------------
-- Records of tst_publications
-- ----------------------------
INSERT INTO `tst_publications` VALUES ('1', 'contacts');

-- ----------------------------
-- Table structure for `tst_publications_lang`
-- ----------------------------
DROP TABLE IF EXISTS `tst_publications_lang`;
CREATE TABLE `tst_publications_lang` (
  `page_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `page_title` varchar(20) NOT NULL,
  `page_content` text NOT NULL,
  `page_preview` text NOT NULL,
  `title` text NOT NULL,
  `keywords` text NOT NULL,
  `description` text NOT NULL,
  KEY `fk_publications_lang_page_id` (`page_id`),
  CONSTRAINT `fk_publications_lang_page_id` FOREIGN KEY (`page_id`) REFERENCES `tst_publications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='публикации';

-- ----------------------------
-- Records of tst_publications_lang
-- ----------------------------
INSERT INTO `tst_publications_lang` VALUES ('1', '1', 'Контакты', 'наши контакты', '', '', '', '');
INSERT INTO `tst_publications_lang` VALUES ('1', '2', 'Contacts', 'our contacts', '', '', '', '');

-- ----------------------------
-- Table structure for `tst_rating`
-- ----------------------------
DROP TABLE IF EXISTS `tst_rating`;
CREATE TABLE `tst_rating` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rating` int(11) DEFAULT '0',
  `vote_count` int(11) DEFAULT '0',
  `entity_type_id` int(11) DEFAULT NULL,
  `entity_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Система рейтингов';

-- ----------------------------
-- Records of tst_rating
-- ----------------------------

-- ----------------------------
-- Table structure for `tst_rating_log`
-- ----------------------------
DROP TABLE IF EXISTS `tst_rating_log`;
CREATE TABLE `tst_rating_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `obj_id` int(11) unsigned DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `sid` varchar(32) DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_rating_log_obj_id` (`obj_id`),
  CONSTRAINT `fk_rating_log_obj_id` FOREIGN KEY (`obj_id`) REFERENCES `tst_rating` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Лог рейтингов';

-- ----------------------------
-- Records of tst_rating_log
-- ----------------------------

-- ----------------------------
-- Table structure for `tst_sessions`
-- ----------------------------
DROP TABLE IF EXISTS `tst_sessions`;
CREATE TABLE `tst_sessions` (
  `uid` int(11) NOT NULL,
  `session` varchar(32) NOT NULL,
  `last_update` int(11) NOT NULL,
  `ip` varchar(20) NOT NULL,
  PRIMARY KEY (`uid`),
  CONSTRAINT `fk_sessions_uid` FOREIGN KEY (`uid`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tst_sessions
-- ----------------------------

-- ----------------------------
-- Table structure for `tst_users`
-- ----------------------------
DROP TABLE IF EXISTS `tst_users`;
CREATE TABLE `tst_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `rating` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tst_users
-- ----------------------------
INSERT INTO `tst_users` VALUES ('13', 'paypall', 'paypall', '1');
INSERT INTO `tst_users` VALUES ('18', 'admin', '202cb962ac59075b964b07152d234b70', '100');

-- ----------------------------
-- Table structure for `tst_user_groups`
-- ----------------------------
DROP TABLE IF EXISTS `tst_user_groups`;
CREATE TABLE `tst_user_groups` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  KEY `fk_user_groups_user_id` (`user_id`),
  KEY `fk_user_groups_group_id` (`group_id`),
  CONSTRAINT `fk_user_groups_group_id` FOREIGN KEY (`group_id`) REFERENCES `tst_groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_groups_user_id` FOREIGN KEY (`user_id`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tst_user_groups
-- ----------------------------
INSERT INTO `tst_user_groups` VALUES ('18', '3');
INSERT INTO `tst_user_groups` VALUES ('13', '2');

-- ----------------------------
-- Table structure for `tst_user_profile`
-- ----------------------------
DROP TABLE IF EXISTS `tst_user_profile`;
CREATE TABLE `tst_user_profile` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `fio` varchar(100) NOT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_user_profile_user_id` FOREIGN KEY (`user_id`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tst_user_profile
-- ----------------------------
INSERT INTO `tst_user_profile` VALUES ('13', 'paypall');
