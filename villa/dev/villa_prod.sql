-- phpMyAdmin SQL Dump
-- version 3.3.7
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Окт 10 2012 г., 15:16
-- Версия сервера: 5.0.77
-- Версия PHP: 5.2.14

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `villa_dbase`
--

-- --------------------------------------------------------

--
-- Структура таблицы `db_version`
--

CREATE TABLE IF NOT EXISTS `db_version` (
  `package` varchar(32) default NULL,
  `version` int(11) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `db_version`
--

INSERT INTO `db_version` (`package`, `version`) VALUES
('dante', 1),
('lib.jquery.ui', 1),
('lib.jdatepicker', 1),
('lib.jgrid', 1),
('module.auth', 3),
('module.lang', 2),
('component.jalerts', 1),
('module.division', 2),
('lib.lightbox', 1),
('lib.jquery.mousewheel', 1),
('lib.jquery.smoothdivscroll', 1),
('lib.jquery.fancybox', 1),
('component.swfupload', 1),
('component.tinymce', 1),
('module.user', 2),
('module.profile', 1),
('module.hotel', 51),
('module.entity', 1),
('module.rating', 1),
('module.comment', 2),
('module.currency', 4),
('module.currencycountries', 3),
('module.menu', 2),
('module.callback', 3),
('module.publications', 6),
('lib.paypall', 3),
('module.labels', 2),
('module.search', 1),
('villa', 6),
('lib.timepicker', 1),
('lib.jquery.fancytransitions', 1),
('module.slideshow', 1),
('lib.jquery.form', 1),
('lib.jquery.validate', 1),
('lib.jquery.arcticmodal', 1),
('module.config', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tst_comment`
--

CREATE TABLE IF NOT EXISTS `tst_comment` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `entity_type_id` int(11) unsigned NOT NULL,
  `entity_id` int(11) default NULL,
  `date` int(11) default NULL,
  `name` varchar(128) default NULL,
  `email` varchar(128) default NULL,
  `message` text,
  PRIMARY KEY  (`id`),
  KEY `fk_comment_entity_type_id` (`entity_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Комментарии' AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tst_comment`
--

INSERT INTO `tst_comment` (`id`, `entity_type_id`, `entity_id`, `date`, `name`, `email`, `message`) VALUES
(1, 1, 1, 1346006878, '3454', 'ddd@dd.com', '45435');

-- --------------------------------------------------------

--
-- Структура таблицы `tst_currency`
--

CREATE TABLE IF NOT EXISTS `tst_currency` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(50) NOT NULL,
  `reduction` varchar(10) NOT NULL,
  `factor` float(10,2) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tst_currency`
--

INSERT INTO `tst_currency` (`id`, `title`, `reduction`, `factor`) VALUES
(1, 'Евро', 'EUR', 1.20);

-- --------------------------------------------------------

--
-- Структура таблицы `tst_currencycountries`
--

CREATE TABLE IF NOT EXISTS `tst_currencycountries` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(50) NOT NULL,
  `lang_id` int(11) unsigned NOT NULL,
  `currency` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_currencycountries_currency` (`currency`),
  KEY `fk_currencycountries_lang_id` (`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Дамп данных таблицы `tst_currencycountries`
--


-- --------------------------------------------------------

--
-- Структура таблицы `tst_currency_lang`
--

CREATE TABLE IF NOT EXISTS `tst_currency_lang` (
  `id` int(11) NOT NULL,
  `lang_id` int(11) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  KEY `fk_currency_lang_id` (`id`),
  KEY `fk_currency_lang_lang_id` (`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='локализация типов валют';

--
-- Дамп данных таблицы `tst_currency_lang`
--

INSERT INTO `tst_currency_lang` (`id`, `lang_id`, `title`) VALUES
(1, 1, 'Евро локал');

-- --------------------------------------------------------

--
-- Структура таблицы `tst_division`
--

CREATE TABLE IF NOT EXISTS `tst_division` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(128) default NULL,
  `parent_id` int(11) unsigned default '0',
  `link` varchar(128) default NULL,
  `image` varchar(255) default NULL,
  `draw_order` int(11) default NULL,
  `d_type` varchar(20) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Категории' AUTO_INCREMENT=25 ;

--
-- Дамп данных таблицы `tst_division`
--

INSERT INTO `tst_division` (`id`, `name`, `parent_id`, `link`, `image`, `draw_order`, `d_type`) VALUES
(1, 'root', 1, '', NULL, 1, 'url'),
(2, 'adminka', 1, '', NULL, 2, 'url'),
(3, 'client', 1, '', NULL, 3, 'url'),
(6, 'Номера', 2, '/manage/apartments/', NULL, 3, 'url'),
(7, 'Типы номеров', 2, '/manage/apartmentstypes/', NULL, 4, 'url'),
(8, 'Бронирование', 2, '/manage/booking/', NULL, 5, 'url'),
(9, 'Опции бронирования', 2, '/manage/bookingoptions/', NULL, 6, 'url'),
(10, 'Загрузка отеля', 2, '/manage/loading/', NULL, 7, 'url'),
(11, 'Оборудование', 2, '/manage/equipment/', NULL, 8, 'url'),
(12, 'Типы предоплат', 2, '/manage/bookingprepayment/', NULL, 9, 'url'),
(13, 'Кабинет', 2, '/manage/cashcontrol/', NULL, 10, 'url'),
(14, 'Кабинет Ген Директора', 2, '/manage/cashgendir/', NULL, 11, 'url'),
(15, 'Комментарии', 2, '/manage/comment/', NULL, 12, 'url'),
(16, 'Валюты', 2, '/manage/currency/', NULL, 13, 'url'),
(17, 'Страны', 2, '/manage/currencycountries/', NULL, 14, 'url'),
(18, 'Звонки', 2, '/manage/callback/', NULL, 15, 'url'),
(19, 'Публикации', 2, '/manage/publications/', NULL, 16, 'url'),
(20, 'Логи paypall', 2, '/manage/paylogs/', NULL, 17, 'url'),
(21, 'Метки', 2, '/manage/labels/', NULL, 18, 'url'),
(22, 'Пользователи', 2, '/manage/user/', NULL, 19, 'url'),
(23, 'Разделы', 2, '/manage/division/', NULL, 20, 'url'),
(24, 'Языки', 2, '/manage/languages/', NULL, 21, 'url');

-- --------------------------------------------------------

--
-- Структура таблицы `tst_division_groups`
--

CREATE TABLE IF NOT EXISTS `tst_division_groups` (
  `division_id` int(11) unsigned default NULL,
  `group_id` int(11) default NULL,
  KEY `fk_division_groups_division_id` (`division_id`),
  KEY `fk_division_groups_group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='соответствие категорий и групп пользователей';

--
-- Дамп данных таблицы `tst_division_groups`
--

INSERT INTO `tst_division_groups` (`division_id`, `group_id`) VALUES
(6, 3),
(6, 2),
(6, 1),
(7, 3),
(7, 2),
(7, 1),
(8, 3),
(8, 2),
(8, 1),
(9, 3),
(9, 2),
(9, 1),
(10, 3),
(10, 2),
(10, 1),
(11, 3),
(12, 3),
(13, 3),
(14, 3),
(15, 3),
(15, 2),
(15, 1),
(16, 3),
(17, 3),
(18, 3),
(18, 2),
(18, 1),
(19, 3),
(20, 3),
(21, 3),
(21, 2),
(21, 1),
(22, 3),
(23, 3),
(24, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `tst_division_seo`
--

CREATE TABLE IF NOT EXISTS `tst_division_seo` (
  `menu_id` int(11) unsigned default NULL,
  `lang_id` int(11) unsigned default NULL,
  `name` varchar(255) default NULL,
  `title` varchar(255) default NULL,
  `keywords` varchar(255) default NULL,
  `description` text,
  KEY `fk_division_seo_lang_id` (`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='SEO данные категорий';

--
-- Дамп данных таблицы `tst_division_seo`
--

INSERT INTO `tst_division_seo` (`menu_id`, `lang_id`, `name`, `title`, `keywords`, `description`) VALUES
(23, NULL, NULL, NULL, NULL, NULL),
(24, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tst_entity`
--

CREATE TABLE IF NOT EXISTS `tst_entity` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(128) default NULL,
  `sys_name` varchar(128) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Сущности' AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tst_entity`
--

INSERT INTO `tst_entity` (`id`, `name`, `sys_name`) VALUES
(1, 'Общий отзыв', 'feedback');

-- --------------------------------------------------------

--
-- Структура таблицы `tst_groups`
--

CREATE TABLE IF NOT EXISTS `tst_groups` (
  `group_id` int(11) NOT NULL auto_increment,
  `group_name` varchar(32) default NULL,
  `group_caption` varchar(32) default NULL,
  PRIMARY KEY  (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Группы пользователей' AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `tst_groups`
--

INSERT INTO `tst_groups` (`group_id`, `group_name`, `group_caption`) VALUES
(1, 'moder', 'Модератор'),
(2, 'admin', 'Администратор'),
(3, 'gendir', 'Ген Директор'),
(4, 'user', 'Пользователь'),
(5, 'guest', 'Гость');

-- --------------------------------------------------------

--
-- Структура таблицы `tst_hotel_apartments`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_apartments` (
  `id` int(11) NOT NULL auto_increment,
  `type` int(11) NOT NULL,
  `people` tinyint(2) NOT NULL,
  `childs` tinyint(2) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_hotel_apartments_type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='номера' AUTO_INCREMENT=434 ;

--
-- Дамп данных таблицы `tst_hotel_apartments`
--

INSERT INTO `tst_hotel_apartments` (`id`, `type`, `people`, `childs`) VALUES
(101, 3, 2, 1),
(102, 3, 2, 1),
(103, 3, 2, 1),
(104, 5, 2, 1),
(105, 5, 2, 1),
(106, 3, 2, 1),
(107, 3, 2, 1),
(108, 3, 2, 1),
(109, 3, 2, 1),
(110, 3, 2, 1),
(111, 3, 2, 1),
(112, 3, 2, 1),
(113, 3, 2, 1),
(114, 3, 2, 1),
(115, 3, 2, 1),
(116, 3, 2, 1),
(201, 3, 2, 1),
(202, 3, 2, 1),
(203, 3, 2, 1),
(204, 5, 2, 1),
(205, 5, 2, 1),
(206, 3, 2, 1),
(207, 3, 2, 1),
(208, 3, 2, 1),
(209, 3, 2, 1),
(210, 3, 2, 1),
(211, 3, 2, 1),
(212, 3, 2, 1),
(213, 3, 2, 1),
(214, 3, 2, 1),
(215, 3, 2, 1),
(216, 3, 2, 1),
(301, 3, 2, 1),
(302, 3, 2, 1),
(303, 3, 2, 1),
(304, 4, 2, 1),
(305, 4, 2, 1),
(306, 3, 2, 1),
(307, 3, 2, 1),
(308, 3, 2, 1),
(309, 3, 2, 1),
(310, 3, 2, 1),
(311, 3, 2, 1),
(312, 3, 2, 1),
(313, 3, 2, 1),
(314, 3, 2, 1),
(315, 3, 2, 1),
(316, 3, 2, 1),
(401, 3, 2, 1),
(402, 3, 2, 1),
(403, 3, 2, 1),
(404, 3, 2, 1),
(405, 3, 2, 1),
(406, 3, 2, 1),
(407, 3, 2, 1),
(408, 3, 2, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tst_hotel_apartmentstypes`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_apartmentstypes` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(50) NOT NULL,
  `link` varchar(50) NOT NULL,
  `cost` int(11) NOT NULL,
  `standart_cost` float(10,2) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='типы номеров' AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `tst_hotel_apartmentstypes`
--

INSERT INTO `tst_hotel_apartmentstypes` (`id`, `title`, `link`, `cost`, `standart_cost`) VALUES
(3, 'Двухместный номер', 'hotel_double_room', 1, 0.70),
(4, 'Двухместный номер категории \\"Люкс\\"', 'hotel_luxe_room', 1200, 1400.00),
(5, 'Двухместный номер категории \\"ПОЛУЛЮКС\\"', 'hotel_junior_suite_room', 900, 1000.00);

-- --------------------------------------------------------

--
-- Структура таблицы `tst_hotel_apartmentstypes_equipment`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_apartmentstypes_equipment` (
  `apt_id` int(11) NOT NULL,
  `eq_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `bold` tinyint(1) NOT NULL,
  `fontsize` tinyint(2) NOT NULL,
  KEY `fk_hotel_apartmentstypes_equipment_apt_id` (`apt_id`),
  KEY `fk_hotel_apartmentstypes_equipment_eq_id` (`eq_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tst_hotel_apartmentstypes_equipment`
--

INSERT INTO `tst_hotel_apartmentstypes_equipment` (`apt_id`, `eq_id`, `sort_order`, `bold`, `fontsize`) VALUES
(3, 1, 1, 1, 0),
(3, 2, 2, 0, 16),
(3, 4, 3, 0, 0),
(3, 3, 4, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `tst_hotel_apartmentstypes_images`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_apartmentstypes_images` (
  `type_id` int(11) NOT NULL,
  `image` varchar(50) NOT NULL,
  `draw_order` tinyint(2) NOT NULL,
  KEY `fk_hotel_apartmentstypes_images_type_id` (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Изображения типов номеров';

--
-- Дамп данных таблицы `tst_hotel_apartmentstypes_images`
--

INSERT INTO `tst_hotel_apartmentstypes_images` (`type_id`, `image`, `draw_order`) VALUES
(4, 'DSC_0010.jpg', 2),
(4, 'DSC_0015.jpg', 3),
(3, 'DSC_0121.jpg', 1),
(5, 'DSC_0032.jpg', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tst_hotel_apartmentstypes_lang`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_apartmentstypes_lang` (
  `id` int(11) NOT NULL,
  `lang_id` int(11) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  KEY `fk_hotel_apartmentstypes_lang_lang_id` (`lang_id`),
  KEY `fk_hotel_apartmentstypes_lang_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='локализация типов номеров';

--
-- Дамп данных таблицы `tst_hotel_apartmentstypes_lang`
--

INSERT INTO `tst_hotel_apartmentstypes_lang` (`id`, `lang_id`, `title`, `description`) VALUES
(3, 1, 'Двухместный номер', '<p><span><strong><em>Двухместный номер категории Стандарт Дабл</em></strong> (общей площадью от 17 кв. м до 24 кв.м) - представляет собой уютный однокомнатный номер с красивым, комфортным и функциональным интерьером. Дизайн номера выполнен в классическом стиле, в теплых тонах и создает ощущение роскоши и спокойствия. Номер укомплектован современным мебельным набором из двуспальной кровати размера 190x220 (king-size), двух прикроватных тумбочек, письменного стола со стульями и зеркального шкафа-купе. Одна практически зеркальная стена прекрасно сочетается с интерьером номера и придает ему ощущения легкости и безграничности. Также в номере: LCD телевизор с 42ʺ диагональю, бесплатный высокоскоростной Wi-Fi доступ в интернет, телефон, электронный сейф, с возможностью хранения в нем ноутбука, холодильник/мини-бар а так же всё необходимое для комфортного отдыха. Ванная комната номера (общей площадью от 3 кв. м до 6 кв. м) укомплектована душем. В ней находится фен, комплект из 4-х полотенец, тапочек и всей необходимой парфумерии. Во всем номере, включая ванную комнату действует централизированая климатическая система, с регулированием температурного режима. Для большего комфорта номер оборудован электронным замком с магнитной карточкой для входной двери и электронным индикатором уборки номера. Уборка номера производится ежедневно, смена белья - раз в 3 дня.</span></p>'),
(3, 2, '45435', '<p>435345</p>'),
(4, 1, 'Двухместный номер категории \\"Люкс\\"', '<p>Двухместный номер категории Стандарт Дабл (общей площадью от 17 кв. м до 24 кв.м) - представляет собой уютный однокомнатный номер с красивым, комфортным и функциональным интерьером. Дизайн номера выполнен в классическом стиле, в теплых тонах и создает ощущение роскоши и спокойствия. Номер укомплектован современным мебельным набором из двуспальной кровати размера 190x220 (king-size), двух прикроватных тумбочек, письменного стола со стульями и зеркального шкафа-купе. Одна практически зеркальная стена прекрасно сочетается с интерьером номера и придает ему ощущения легкости и безграничности. Также в номере: LCD телевизор с 42ʺ диагональю, бесплатный высокоскоростной Wi-Fi доступ в интернет, телефон, электронный сейф, с возможностью хранения в нем ноутбука, холодильник/мини-бар а так же всё необходимое для комфортного отдыха. Ванная комната номера (общей площадью от 3 кв. м до 6 кв. м) укомплектована душем. В ней находится фен, комплект из 4-х полотенец, тапочек и всей необходимой парфумерии. Во всем номере, включая ванную комнату действует централизированая климатическая система, с регулированием температурного режима. Для большего комфорта номер оборудован электронным замком с магнитной карточкой для входной двери и электронным индикатором уборки номера. Уборка номера производится ежедневно, смена белья - раз в 3 дня.</p>'),
(5, 1, 'Двухместный номер категории \\"ПОЛУЛЮКС\\"', '<p>Номера категории полулюкс создают ощущение просторных апартаментов. Преимущества двух уютных разделенных зон: спальни и зоны отдыха, непременно оценит тот, кто путешествует не один.</p>');

-- --------------------------------------------------------

--
-- Структура таблицы `tst_hotel_booking`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_booking` (
  `id` int(11) NOT NULL auto_increment,
  `type` int(11) NOT NULL,
  `arrival` int(11) NOT NULL,
  `departure` int(11) NOT NULL,
  `people` int(3) NOT NULL,
  `comment` text NOT NULL,
  `prepayment` int(11) NOT NULL,
  `cost` float(8,2) NOT NULL,
  `costTotal` varchar(100) NOT NULL,
  `apartment` int(11) default NULL,
  `firstName` varchar(100) NOT NULL,
  `secondName` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `creation_time` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_hotel_booking_type` (`type`),
  KEY `fk_hotel_booking_apartment` (`apartment`),
  KEY `fk_hotel_booking_prepayment` (`prepayment`),
  KEY `fk_hotel_booking_uid` (`uid`),
  KEY `fk_hotel_booking_status` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='бронирование' AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tst_hotel_booking`
--

INSERT INTO `tst_hotel_booking` (`id`, `type`, `arrival`, `departure`, `people`, `comment`, `prepayment`, `cost`, `costTotal`, `apartment`, `firstName`, `secondName`, `email`, `phone`, `status`, `uid`, `creation_time`) VALUES
(1, 3, 1349038800, 1349211600, 2, '111', 3, 7.00, '6', 101, 'Dorian', 'Gray', 'doriangray@ukr.net', '+380671234567', 1, 19, 1349083120);

-- --------------------------------------------------------

--
-- Структура таблицы `tst_hotel_booking_deposit_history`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_booking_deposit_history` (
  `id` int(11) NOT NULL auto_increment,
  `booking_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `summ` int(11) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_hotel_booking_deposit_history_booking_id` (`booking_id`),
  KEY `fk_hotel_booking_deposit_history_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='фин история бронирования' AUTO_INCREMENT=1 ;

--
-- Дамп данных таблицы `tst_hotel_booking_deposit_history`
--


-- --------------------------------------------------------

--
-- Структура таблицы `tst_hotel_booking_options`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_booking_options` (
  `hotel_booking_id` int(11) NOT NULL,
  `sample_id` int(11) NOT NULL,
  KEY `fk_hotel_booking_options_hotel_booking_id` (`hotel_booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='опции бронирования';

--
-- Дамп данных таблицы `tst_hotel_booking_options`
--

INSERT INTO `tst_hotel_booking_options` (`hotel_booking_id`, `sample_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tst_hotel_booking_options_samples`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_booking_options_samples` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(100) NOT NULL,
  `cost` float(10,2) NOT NULL,
  `reusable` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='примеры опций бронирования' AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `tst_hotel_booking_options_samples`
--

INSERT INTO `tst_hotel_booking_options_samples` (`id`, `title`, `cost`, `reusable`) VALUES
(1, 'Завтрак с человека', 2.00, 0),
(2, 'Ужин', 3.00, 0),
(3, 'Туристическое обслуживание', 4.00, 0),
(4, 'te\\"dd d\\''d d`d \\'' ddd\\"dd', 2.00, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `tst_hotel_booking_options_samples_lang`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_booking_options_samples_lang` (
  `id` int(11) NOT NULL,
  `lang_id` int(11) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  KEY `fk_hotel_booking_options_samples_lang_id` (`id`),
  KEY `fk_hotel_booking_options_samples_lang_lang_id` (`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tst_hotel_booking_options_samples_lang`
--

INSERT INTO `tst_hotel_booking_options_samples_lang` (`id`, `lang_id`, `name`) VALUES
(1, 1, 'Завтрак с человека'),
(1, 2, 'lweidhfierwj'),
(2, 2, 'Ужин '),
(3, 2, 'kuglkjlo'),
(2, 1, 'Ужин'),
(3, 1, 'Туристическое обслуживание'),
(4, 1, '2');

-- --------------------------------------------------------

--
-- Структура таблицы `tst_hotel_booking_prepayment_types`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_booking_prepayment_types` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(50) NOT NULL,
  `cost_modifier` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='типы предоплаты бронирования' AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `tst_hotel_booking_prepayment_types`
--

INSERT INTO `tst_hotel_booking_prepayment_types` (`id`, `title`, `cost_modifier`) VALUES
(1, 'Без предоплаты', 2),
(2, 'Предоплата за первую ночь', 1),
(3, 'Предоплата за весь период проживания', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `tst_hotel_booking_prepayment_types_lang`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_booking_prepayment_types_lang` (
  `id` int(11) NOT NULL,
  `lang_id` int(11) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  KEY `fk_hotel_booking_prepayment_types_lang_id` (`id`),
  KEY `fk_hotel_booking_prepayment_types_lang_lang_id` (`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tst_hotel_booking_prepayment_types_lang`
--

INSERT INTO `tst_hotel_booking_prepayment_types_lang` (`id`, `lang_id`, `name`) VALUES
(1, 1, 'Бронирование без предоплаты'),
(2, 1, 'Предоплата за первую ночь'),
(3, 1, 'Предоплата за весь период проживания'),
(1, 2, 'powetrporeifp');

-- --------------------------------------------------------

--
-- Структура таблицы `tst_hotel_booking_statuses`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_booking_statuses` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `tst_hotel_booking_statuses`
--

INSERT INTO `tst_hotel_booking_statuses` (`id`, `name`) VALUES
(1, 'Не обработано'),
(2, 'В обработке'),
(3, 'Обработано'),
(4, 'Отменено пользователем');

-- --------------------------------------------------------

--
-- Структура таблицы `tst_hotel_callback`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_callback` (
  `id` int(11) NOT NULL auto_increment,
  `phone` varchar(20) NOT NULL,
  `status` varchar(50) NOT NULL,
  `view_flag` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='обратный звонок' AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `tst_hotel_callback`
--

INSERT INTO `tst_hotel_callback` (`id`, `phone`, `status`, `view_flag`) VALUES
(1, '+380675641740', '', 1),
(2, '+380675641740', '', 1),
(3, '+380675641740', '', 1),
(4, '+380675641740', '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tst_hotel_cash`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_cash` (
  `user_id` int(11) NOT NULL auto_increment,
  `balance` float(10,2) NOT NULL,
  PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='баланс работников отеля' AUTO_INCREMENT=14 ;

--
-- Дамп данных таблицы `tst_hotel_cash`
--

INSERT INTO `tst_hotel_cash` (`user_id`, `balance`) VALUES
(13, 14.34);

-- --------------------------------------------------------

--
-- Структура таблицы `tst_hotel_cash_transactions`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_cash_transactions` (
  `id` int(11) NOT NULL auto_increment,
  `user_from` int(11) NOT NULL,
  `user_to` int(11) NOT NULL,
  `summ` float(10,2) NOT NULL,
  `date` int(11) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_hotel_cash_transactions_user_from` (`user_from`),
  KEY `fk_hotel_cash_transactions_user_to` (`user_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='фин операции' AUTO_INCREMENT=1 ;

--
-- Дамп данных таблицы `tst_hotel_cash_transactions`
--


-- --------------------------------------------------------

--
-- Структура таблицы `tst_hotel_equipment`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_equipment` (
  `id` int(11) NOT NULL auto_increment,
  `sys_name` varchar(50) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Дамп данных таблицы `tst_hotel_equipment`
--

INSERT INTO `tst_hotel_equipment` (`id`, `sys_name`, `sort_order`) VALUES
(1, 'LCD телевизор с 42\\" экраном, кабельное телевидени', 0),
(2, 'Холодильник / минибар', 0),
(3, 'Digital Safe, size: H19,5×43×37(cm)', 0),
(4, 'Двухспальная кровать king-size', 0),
(5, 'Прикроватные тумбочки, шкаф - купе', 0),
(6, 'Ванная комната  укомплектованная    душем', 0),
(7, 'Комнатные тапочки и ванные принадлежности', 0),
(8, 'Фен', 0),
(9, 'Телефон, услуга звонок-будильник', 0),
(10, 'Возможность затемнения комнаты в светлое время сут', 0),
(11, 'Подушки наполненные гусиным пухом', 0),
(12, 'Одеяла наполненные верблюжьим подпушком', 0),
(13, 'Подушки наполненные гусиным пухом', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `tst_hotel_equipment_lang`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_equipment_lang` (
  `id` int(11) NOT NULL,
  `lang_id` int(11) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  KEY `fk_hotel_equipment_lang_id` (`id`),
  KEY `fk_hotel_equipment_lang_lang_id` (`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tst_hotel_equipment_lang`
--

INSERT INTO `tst_hotel_equipment_lang` (`id`, `lang_id`, `name`) VALUES
(1, 1, 'LCD телевизор с 42\\" экраном, кабельное телевидени'),
(2, 1, 'Холодильник / минибар'),
(3, 1, 'Электронный сейф, размером 19,5×43×37(cm)'),
(3, 2, 'Digital Safe, size: H19,5×43×37(cm)'),
(4, 1, 'Двухспальная кровать king-size'),
(5, 1, 'Прикроватные тумбочки, шкаф - купе'),
(6, 1, 'Ванная комната  укомплектованная    душем'),
(7, 1, 'Комнатные тапочки и ванные принадлежности'),
(8, 1, 'Фен'),
(9, 1, 'Телефон, услуга звонок-будильник'),
(10, 1, 'Возможность затемнения комнаты в светлое время сут'),
(11, 1, 'Подушки наполненные гусиным пухом'),
(12, 1, 'Одеяла наполненные верблюжьим подпушком'),
(13, 1, 'Подушки наполненные гусиным пухом');

-- --------------------------------------------------------

--
-- Структура таблицы `tst_labels`
--

CREATE TABLE IF NOT EXISTS `tst_labels` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `sys_name` varchar(128) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='labels' AUTO_INCREMENT=34 ;

--
-- Дамп данных таблицы `tst_labels`
--

INSERT INTO `tst_labels` (`id`, `sys_name`) VALUES
(1, 'login'),
(2, 'aboutHotel'),
(3, 'numbers'),
(4, 'contacts'),
(5, 'backCall'),
(6, 'callMe'),
(7, 'example'),
(8, 'newWindowMap'),
(9, 'bookingNumber'),
(10, 'arriveDate'),
(11, 'departureDate'),
(12, 'type'),
(13, 'adults'),
(14, 'children'),
(15, 'name'),
(16, 'lastName'),
(17, 'email'),
(18, 'phone'),
(19, 'comment'),
(20, 'cost'),
(21, 'forADay'),
(22, 'forWholeTerm'),
(23, 'doBooking'),
(24, 'description'),
(25, 'equipment'),
(26, 'checkPresence'),
(27, 'booking'),
(28, 'arrive'),
(29, 'departure'),
(30, 'adults'),
(31, 'children'),
(32, 'keepFeedback'),
(33, 'details');

-- --------------------------------------------------------

--
-- Структура таблицы `tst_labels_lang`
--

CREATE TABLE IF NOT EXISTS `tst_labels_lang` (
  `id` int(11) unsigned default NULL,
  `lang_id` int(11) unsigned default NULL,
  `name` varchar(255) default NULL,
  KEY `fk_labels_lang_lang_id` (`lang_id`),
  KEY `fk_labels_lang_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='языковые данные labels';

--
-- Дамп данных таблицы `tst_labels_lang`
--

INSERT INTO `tst_labels_lang` (`id`, `lang_id`, `name`) VALUES
(1, 1, 'Логин'),
(2, 1, 'Об отеле'),
(3, 1, 'Номера'),
(4, 1, 'Контакты'),
(5, 1, 'обратный звонок'),
(6, 1, 'перезвоните мне'),
(7, 1, 'Например'),
(8, 1, 'Карта в отдельном окне'),
(9, 1, 'Бронировать номер'),
(10, 1, 'Дата заезда'),
(11, 1, 'Дата выезда'),
(12, 1, 'Тип'),
(13, 1, 'Взрослые'),
(14, 1, 'Дети'),
(15, 1, 'Имя'),
(16, 1, 'Фамилия'),
(17, 1, 'Email'),
(18, 1, 'Телефон'),
(19, 1, 'Комментарий'),
(20, 1, 'Стоимость'),
(21, 1, 'Стоимость'),
(22, 1, 'за весь период'),
(23, 1, 'Забронировать'),
(24, 1, 'Описание'),
(25, 1, 'Номер укомплектован'),
(26, 1, 'Бронировать'),
(27, 1, 'БРОНИРОВАНИЕ'),
(28, 1, 'Заезд'),
(29, 1, 'Выезд'),
(30, 1, 'Взрослые'),
(31, 1, 'Дети'),
(32, 1, 'оставить отзыв'),
(33, 1, 'подробнее');

-- --------------------------------------------------------

--
-- Структура таблицы `tst_languages`
--

CREATE TABLE IF NOT EXISTS `tst_languages` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(128) default NULL,
  `sys_name` varchar(128) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Языки' AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `tst_languages`
--

INSERT INTO `tst_languages` (`id`, `name`, `sys_name`) VALUES
(1, 'Русский', 'ua'),
(2, 'English', 'eng'),
(3, 'Deutsch', 'de');

-- --------------------------------------------------------

--
-- Структура таблицы `tst_paypall_log`
--

CREATE TABLE IF NOT EXISTS `tst_paypall_log` (
  `id` int(11) NOT NULL auto_increment,
  `payment_type` varchar(50) default NULL,
  `payment_date` varchar(50) default NULL,
  `payment_status` varchar(50) default NULL,
  `address_status` varchar(50) default NULL,
  `payer_status` varchar(50) default NULL,
  `first_name` varchar(50) default NULL,
  `last_name` varchar(50) default NULL,
  `payer_email` varchar(50) default NULL,
  `payer_id` varchar(50) default NULL,
  `address_name` varchar(50) default NULL,
  `address_country` varchar(50) default NULL,
  `address_country_code` varchar(50) default NULL,
  `address_zip` varchar(50) default NULL,
  `address_state` varchar(50) default NULL,
  `address_city` varchar(50) default NULL,
  `address_street` varchar(100) default NULL,
  `business` varchar(50) default NULL,
  `receiver_email` varchar(50) default NULL,
  `receiver_id` varchar(50) default NULL,
  `residence_country` varchar(50) default NULL,
  `item_name` varchar(50) default NULL,
  `item_number` varchar(50) default NULL,
  `quantity` int(11) default NULL,
  `shipping` float(10,2) NOT NULL,
  `tax` float(10,2) NOT NULL,
  `mc_currency` varchar(50) default NULL,
  `mc_fee` float(10,2) NOT NULL,
  `mc_gross` float(10,2) NOT NULL,
  `txn_type` varchar(50) default NULL,
  `txn_id` int(11) default NULL,
  `notify_version` varchar(50) default NULL,
  `custom` varchar(50) default NULL,
  `invoice` varchar(50) default NULL,
  `charset` varchar(50) default NULL,
  `verify_sign` varchar(100) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='paypall логи' AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `tst_paypall_log`
--

INSERT INTO `tst_paypall_log` (`id`, `payment_type`, `payment_date`, `payment_status`, `address_status`, `payer_status`, `first_name`, `last_name`, `payer_email`, `payer_id`, `address_name`, `address_country`, `address_country_code`, `address_zip`, `address_state`, `address_city`, `address_street`, `business`, `receiver_email`, `receiver_id`, `residence_country`, `item_name`, `item_number`, `quantity`, `shipping`, `tax`, `mc_currency`, `mc_fee`, `mc_gross`, `txn_type`, `txn_id`, `notify_version`, `custom`, `invoice`, `charset`, `verify_sign`) VALUES
(1, 'echeck', '16:45:48 May 28, 2012 PDT', 'Completed', 'confirmed', 'verified', 'John', 'Smith', 'buyer@paypalsandbox.com', 'TESTBUYERID01', 'John Smith', 'United States', 'US', '95131', 'CA', 'San Jose', '123, any street', 'seller@paypalsandbox.com', 'seller@paypalsandbox.com', 'TESTSELLERID1', 'US', 'something', 'AK-1234', 1, 3.04, 2.02, 'USD', 0.44, 12.34, 'web_accept', 485282345, '2.1', 'xyz123', 'abc1234', 'windows-1252', 'AVGUa04bUtMm3j7yed7qpzQsUMbEAjf-R-uwAtkl4Rw-kxjQ07pNYaXE'),
(2, 'instant', '02:37:51 Sep 23, 2012 PDT', 'Completed', NULL, 'verified', 'DMYTRO', 'SUKACHEV', '0675641740ua@gmail.com', 'D4PA53HAH897Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'villalascala@gmail.com', 'villalascala@gmail.com', 'TRVR5KCTKRD5J', 'UA', NULL, NULL, NULL, 0.00, 0.00, 'USD', 0.34, 1.00, 'cart', 0, '3.7', NULL, NULL, 'gb2312', 'ArFaDaxZIj2Ipo7jmUIDFyZo1becAltd169j1xAucZLUAnMKPNykS5i.');

-- --------------------------------------------------------

--
-- Структура таблицы `tst_publications`
--

CREATE TABLE IF NOT EXISTS `tst_publications` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(20) NOT NULL,
  `url` varchar(100) NOT NULL,
  `date` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `uid` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_publications_uid` (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='публикации' AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `tst_publications`
--

INSERT INTO `tst_publications` (`id`, `name`, `url`, `date`, `time`, `image`, `uid`) VALUES
(1, 'Контакты', 'contacts', -10800, 0, '', NULL),
(2, 'Об отеле', 'about', -10800, 0, '', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tst_publications_lang`
--

CREATE TABLE IF NOT EXISTS `tst_publications_lang` (
  `page_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `page_title` varchar(20) NOT NULL,
  `page_content` text NOT NULL,
  `page_preview` text NOT NULL,
  `title` text NOT NULL,
  `keywords` text NOT NULL,
  `description` text NOT NULL,
  KEY `fk_publications_lang_page_id` (`page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='публикации';

--
-- Дамп данных таблицы `tst_publications_lang`
--

INSERT INTO `tst_publications_lang` (`page_id`, `lang_id`, `page_title`, `page_content`, `page_preview`, `title`, `keywords`, `description`) VALUES
(1, 1, 'Контакты', '<p>Украина, город Киев, просп. Отрадный, дом 7.</p>', '', '', '', ''),
(1, 2, 'Contacts', 'our contacts', '', '', '', ''),
(2, 1, 'Об отеле', '<p>Добрый день, Вас приветствует отель Villa LaScala .</p>\n<p>Гости нашего отеля могут рассчитывать на качественное предоставление услуг, заботливое обслуживание и профессионализм англоязычного персонала. <br />Отель гарантирует безопасность и конфиденциальность своим гостям. Здесь присутствуют атмосфера гармонии и покоя. Номерной фонд отеля - 54 номерa, разных категорий: Люкс - 2 шт, Полулюкс - 4 шт , Стандарт Дабл - 38 шт. Отель оформлен в теплых тонах.</p>\n<p>Каждый номер оборудован современной мебелью и большой кроватью размера King-size. В номере имеется, LCD-телевизор с 42 дюймовым экраном, бесплатный высокоскоростной Wi-Fi, холодильник/мини-бар, электронный сейф с возможностью использования его под ноутбук, телефонная связь, электрочайник с посудой и все не маловажные мелочи, для комфортного длительного или краткосрочного проживания. <br />Все номера оборудованы ванной комнатой (туалет, душ, горячая и холодная вода, фен) с подогревом полов. Гостям предоставляются персональные наборы из трех полотенец, халата и тапочек.</p>\n<p>Все номера оборудованы электронными замками с магнитной карточкой для входной двери, что обеспечивает безопасность и удобства в использовании</p>\n<p>Благодаря централизированой климатической системе, с регулированием температурного режима, в помещениях зимой всегда тепло, а летом прохладно, что полностью избавляет от необходимости использования кондиционеров.</p>\n<p>В отеле работает круглосуточная стойка администратора, что позволяет гостю, заселится в любое удобное для него время суток.<br />Когда угодно(день ,ночь), не выходя из своего номера Вы можете воспользоваться Room servise (предоставление закусок в номера), и заказать понравившееся Вам блюда из меню и насладиться их вкусом .</p>\n<p>На территории гостиницы проходит постоянно действующая выставка- ярмарка работ знаменитых украинских фотографов. Постояльцы гостиницы имеют исключительную возможность приобретения любой понравившейся им работы.</p>\n<p>На территории гостиницы находится ресторан в украинском стиле.<br />Посетив который, вы сможете окунуться в глубину национальных обычаев и быта, насладиться национальной украинской кухней и полюбоваться громадной картиной известных украинских художников выполненной маслом (общая площадь картины составляет 45 кв. метров).</p>\n<p>Так же на территории гостиницы расположен фирменный французский магазин постельного белья LaScala. Где вы сможете приобрести постельное белье, покрывала, подушки, одеяла, простыни и полотенца из натуральных материалов высокого качества: хлопка, шёлка, шерсти, пуха, по доступной цене. Такое приобретении станет шикарным сувениром для Вас и всей Ваших близких. Ознакомится с ассортиментом магазина, вы можете заранее http://www.lascala.ua/.</p>\n<p>При необходимости, персонал отеля в любое время суток встретит Вас в аэропорту Борисполь с табличкой на выходе из зоны прилета, поможет с багажом и поселением в гостиницу.</p>\n<p>Мы всегда очень Вам рады! С Уважением дружный коллектив гостиницы Villa LaScala!</p>', '', '', '', 'Добрый день, Вас приветствует отель Villa LaScala .\n\nГости нашего отеля могут рассчитывать на качественное предоставление услуг, заботливое обслуживание и профессионализм англоязычного персонала. \nОтель гарантирует безопасность и конфиденциальность своим гостям. Здесь присутствуют атмосфера гармонии и покоя. Номерной фонд отеля - 54 номерa, разных категорий: Люкс - 2 шт, Полулюкс - 4 шт , Стандарт Дабл - 38 шт. Отель оформлен в теплых тонах.\n\nКаждый номер оборудован современной мебелью и большой кроватью размера King-size. В номере имеется, LCD-телевизор с 42 дюймовым экраном, бесплатный высокоскоростной Wi-Fi, холодильник/мини-бар, электронный сейф с возможностью использования его под ноутбук, телефонная связь, электрочайник с посудой и все не маловажные мелочи, для комфортного длительного или краткосрочного проживания. \nВсе номера оборудованы ванной комнатой (туалет, душ, горячая и холодная вода, фен) с подогревом полов. Гостям предоставляются персональные наборы из трех полотенец, халата и тапочек.\n\nВсе номера оборудованы электронными замками с магнитной карточкой для входной двери, что обеспечивает безопасность и удобства в использовании\n\nБлагодаря централизированой климатической системе, с регулированием температурного режима, в помещениях зимой всегда тепло, а летом прохладно, что полностью избавляет от необходимости использования кондиционеров.\n\nВ отеле работает круглосуточная стойка администратора, что позволяет гостю, заселится в любое удобное для него время суток.\nКогда угодно(день ,ночь), не выходя из своего номера Вы можете воспользоваться Room servise (предоставление закусок в номера), и заказать понравившееся Вам блюда из меню и насладиться их вкусом .\n\nНа территории гостиницы проходит постоянно действующая выставка- ярмарка работ знаменитых украинских фотографов. Постояльцы гостиницы имеют исключительную возможность приобретения любой понравившейся им работы.\n\nНа территории гостиницы находится ресторан в украинском стиле.\nПосетив который, вы сможете окунуться в глубину национальных обычаев и быта, насладиться национальной украинской кухней и полюбоваться громадной картиной известных украинских художников выполненной маслом (общая площадь картины составляет 45 кв. метров).\n\nТак же на территории гостиницы расположен фирменный французский магазин постельного белья LaScala. Где вы сможете приобрести постельное белье, покрывала, подушки, одеяла, простыни и полотенца из натуральных материалов высокого качества: хлопка, шёлка, шерсти, пуха, по доступной цене. Такое приобретении станет шикарным сувениром для Вас и всей Ваших близких. Ознакомится с ассортиментом магазина, вы можете заранее http://www.lascala.ua/.\n\nПри необходимости, персонал отеля в любое время суток встретит Вас в аэропорту Борисполь с табличкой на выходе из зоны прилета, поможет с багажом и поселением в гостиницу.\n\nМы всегда очень Вам рады! С Уважением дружный коллектив гостиницы Villa LaScala!');

-- --------------------------------------------------------

--
-- Структура таблицы `tst_rating`
--

CREATE TABLE IF NOT EXISTS `tst_rating` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `rating` int(11) default '0',
  `vote_count` int(11) default '0',
  `entity_type_id` int(11) default NULL,
  `entity_id` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Система рейтингов' AUTO_INCREMENT=1 ;

--
-- Дамп данных таблицы `tst_rating`
--


-- --------------------------------------------------------

--
-- Структура таблицы `tst_rating_log`
--

CREATE TABLE IF NOT EXISTS `tst_rating_log` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `obj_id` int(11) unsigned default NULL,
  `uid` int(11) default NULL,
  `sid` varchar(32) default NULL,
  `ip` varchar(20) default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_rating_log_obj_id` (`obj_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Лог рейтингов' AUTO_INCREMENT=1 ;

--
-- Дамп данных таблицы `tst_rating_log`
--


-- --------------------------------------------------------

--
-- Структура таблицы `tst_sessions`
--

CREATE TABLE IF NOT EXISTS `tst_sessions` (
  `uid` int(11) NOT NULL,
  `session` varchar(32) NOT NULL,
  `last_update` int(11) NOT NULL,
  `ip` varchar(20) NOT NULL,
  PRIMARY KEY  (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tst_sessions`
--

INSERT INTO `tst_sessions` (`uid`, `session`, `last_update`, `ip`) VALUES
(18, 'f2736930ff7d001acdf7d68a8e17a981', 1349545087, '94.244.62.146');

-- --------------------------------------------------------

--
-- Структура таблицы `tst_users`
--

CREATE TABLE IF NOT EXISTS `tst_users` (
  `id` int(11) NOT NULL auto_increment,
  `email` varchar(128) default NULL,
  `password` varchar(128) default NULL,
  `rating` int(3) default NULL,
  `is_active` tinyint(1) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Дамп данных таблицы `tst_users`
--

INSERT INTO `tst_users` (`id`, `email`, `password`, `rating`, `is_active`) VALUES
(13, 'paypall', 'paypall', 1, 0),
(18, 'admin', '202cb962ac59075b964b07152d234b70', 100, 1),
(19, 'doriangray@ukr.net', '202cb962ac59075b964b07152d234b70', 1, 0),
(20, '0675641740ua@gmail.com', '202cb962ac59075b964b07152d234b70', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `tst_user_groups`
--

CREATE TABLE IF NOT EXISTS `tst_user_groups` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  KEY `fk_user_groups_user_id` (`user_id`),
  KEY `fk_user_groups_group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tst_user_groups`
--

INSERT INTO `tst_user_groups` (`user_id`, `group_id`) VALUES
(13, 2),
(18, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `tst_user_profile`
--

CREATE TABLE IF NOT EXISTS `tst_user_profile` (
  `user_id` int(11) NOT NULL auto_increment,
  `fio` varchar(100) NOT NULL,
  PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Дамп данных таблицы `tst_user_profile`
--

INSERT INTO `tst_user_profile` (`user_id`, `fio`) VALUES
(13, 'paypall');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `tst_comment`
--
ALTER TABLE `tst_comment`
  ADD CONSTRAINT `fk_comment_entity_type_id` FOREIGN KEY (`entity_type_id`) REFERENCES `tst_entity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tst_currencycountries`
--
ALTER TABLE `tst_currencycountries`
  ADD CONSTRAINT `fk_currencycountries_currency` FOREIGN KEY (`currency`) REFERENCES `tst_currency` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_currencycountries_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tst_currency_lang`
--
ALTER TABLE `tst_currency_lang`
  ADD CONSTRAINT `fk_currency_lang_id` FOREIGN KEY (`id`) REFERENCES `tst_currency` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_currency_lang_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tst_division_groups`
--
ALTER TABLE `tst_division_groups`
  ADD CONSTRAINT `fk_division_groups_division_id` FOREIGN KEY (`division_id`) REFERENCES `tst_division` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_division_groups_group_id` FOREIGN KEY (`group_id`) REFERENCES `tst_groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tst_division_seo`
--
ALTER TABLE `tst_division_seo`
  ADD CONSTRAINT `fk_division_seo_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tst_hotel_apartments`
--
ALTER TABLE `tst_hotel_apartments`
  ADD CONSTRAINT `fk_hotel_apartments_type` FOREIGN KEY (`type`) REFERENCES `tst_hotel_apartmentstypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tst_hotel_apartmentstypes_equipment`
--
ALTER TABLE `tst_hotel_apartmentstypes_equipment`
  ADD CONSTRAINT `fk_hotel_apartmentstypes_equipment_apt_id` FOREIGN KEY (`apt_id`) REFERENCES `tst_hotel_apartmentstypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hotel_apartmentstypes_equipment_eq_id` FOREIGN KEY (`eq_id`) REFERENCES `tst_hotel_equipment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tst_hotel_apartmentstypes_images`
--
ALTER TABLE `tst_hotel_apartmentstypes_images`
  ADD CONSTRAINT `fk_hotel_apartmentstypes_images_type_id` FOREIGN KEY (`type_id`) REFERENCES `tst_hotel_apartmentstypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tst_hotel_apartmentstypes_lang`
--
ALTER TABLE `tst_hotel_apartmentstypes_lang`
  ADD CONSTRAINT `fk_hotel_apartmentstypes_lang_id` FOREIGN KEY (`id`) REFERENCES `tst_hotel_apartmentstypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hotel_apartmentstypes_lang_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tst_hotel_booking`
--
ALTER TABLE `tst_hotel_booking`
  ADD CONSTRAINT `fk_hotel_booking_apartment` FOREIGN KEY (`apartment`) REFERENCES `tst_hotel_apartments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hotel_booking_prepayment` FOREIGN KEY (`prepayment`) REFERENCES `tst_hotel_booking_prepayment_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hotel_booking_status` FOREIGN KEY (`status`) REFERENCES `tst_hotel_booking_statuses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hotel_booking_type` FOREIGN KEY (`type`) REFERENCES `tst_hotel_apartmentstypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hotel_booking_uid` FOREIGN KEY (`uid`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tst_hotel_booking_deposit_history`
--
ALTER TABLE `tst_hotel_booking_deposit_history`
  ADD CONSTRAINT `fk_hotel_booking_deposit_history_booking_id` FOREIGN KEY (`booking_id`) REFERENCES `tst_hotel_booking` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hotel_booking_deposit_history_user_id` FOREIGN KEY (`user_id`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tst_hotel_booking_options`
--
ALTER TABLE `tst_hotel_booking_options`
  ADD CONSTRAINT `fk_hotel_booking_options_hotel_booking_id` FOREIGN KEY (`hotel_booking_id`) REFERENCES `tst_hotel_booking` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tst_hotel_booking_options_samples_lang`
--
ALTER TABLE `tst_hotel_booking_options_samples_lang`
  ADD CONSTRAINT `fk_hotel_booking_options_samples_lang_id` FOREIGN KEY (`id`) REFERENCES `tst_hotel_booking_options_samples` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hotel_booking_options_samples_lang_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tst_hotel_booking_prepayment_types_lang`
--
ALTER TABLE `tst_hotel_booking_prepayment_types_lang`
  ADD CONSTRAINT `fk_hotel_booking_prepayment_types_lang_id` FOREIGN KEY (`id`) REFERENCES `tst_hotel_booking_prepayment_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hotel_booking_prepayment_types_lang_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tst_hotel_cash`
--
ALTER TABLE `tst_hotel_cash`
  ADD CONSTRAINT `fk_hotel_cash_user_id` FOREIGN KEY (`user_id`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tst_hotel_cash_transactions`
--
ALTER TABLE `tst_hotel_cash_transactions`
  ADD CONSTRAINT `fk_hotel_cash_transactions_user_from` FOREIGN KEY (`user_from`) REFERENCES `tst_hotel_cash` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hotel_cash_transactions_user_to` FOREIGN KEY (`user_to`) REFERENCES `tst_hotel_cash` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tst_hotel_equipment_lang`
--
ALTER TABLE `tst_hotel_equipment_lang`
  ADD CONSTRAINT `fk_hotel_equipment_lang_id` FOREIGN KEY (`id`) REFERENCES `tst_hotel_equipment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hotel_equipment_lang_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tst_labels_lang`
--
ALTER TABLE `tst_labels_lang`
  ADD CONSTRAINT `fk_labels_lang_id` FOREIGN KEY (`id`) REFERENCES `tst_labels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_labels_lang_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tst_publications`
--
ALTER TABLE `tst_publications`
  ADD CONSTRAINT `fk_publications_uid` FOREIGN KEY (`uid`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tst_publications_lang`
--
ALTER TABLE `tst_publications_lang`
  ADD CONSTRAINT `fk_publications_lang_page_id` FOREIGN KEY (`page_id`) REFERENCES `tst_publications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tst_rating_log`
--
ALTER TABLE `tst_rating_log`
  ADD CONSTRAINT `fk_rating_log_obj_id` FOREIGN KEY (`obj_id`) REFERENCES `tst_rating` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tst_sessions`
--
ALTER TABLE `tst_sessions`
  ADD CONSTRAINT `fk_sessions_uid` FOREIGN KEY (`uid`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tst_user_groups`
--
ALTER TABLE `tst_user_groups`
  ADD CONSTRAINT `fk_user_groups_group_id` FOREIGN KEY (`group_id`) REFERENCES `tst_groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_groups_user_id` FOREIGN KEY (`user_id`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `tst_user_profile`
--
ALTER TABLE `tst_user_profile`
  ADD CONSTRAINT `fk_user_profile_user_id` FOREIGN KEY (`user_id`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
