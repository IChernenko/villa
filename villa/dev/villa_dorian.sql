/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50508
Source Host           : localhost:3306
Source Database       : villa_prod

Target Server Type    : MYSQL
Target Server Version : 50508
File Encoding         : 65001

Date: 2013-01-30 13:58:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `db_version`
-- ----------------------------
DROP TABLE IF EXISTS `db_version`;
CREATE TABLE `db_version` (
  `package` varchar(32) DEFAULT NULL,
  `version` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of db_version
-- ----------------------------
INSERT INTO `db_version` VALUES ('dante', '1');
INSERT INTO `db_version` VALUES ('lib.jquery.ui', '1');
INSERT INTO `db_version` VALUES ('lib.jdatepicker', '1');
INSERT INTO `db_version` VALUES ('lib.jgrid', '1');
INSERT INTO `db_version` VALUES ('module.auth', '3');
INSERT INTO `db_version` VALUES ('module.lang', '2');
INSERT INTO `db_version` VALUES ('component.jalerts', '1');
INSERT INTO `db_version` VALUES ('module.division', '5');
INSERT INTO `db_version` VALUES ('lib.lightbox', '1');
INSERT INTO `db_version` VALUES ('lib.jquery.mousewheel', '1');
INSERT INTO `db_version` VALUES ('lib.jquery.smoothdivscroll', '1');
INSERT INTO `db_version` VALUES ('lib.jquery.fancybox', '1');
INSERT INTO `db_version` VALUES ('component.swfupload', '1');
INSERT INTO `db_version` VALUES ('component.tinymce', '1');
INSERT INTO `db_version` VALUES ('module.user', '4');
INSERT INTO `db_version` VALUES ('module.profile', '1');
INSERT INTO `db_version` VALUES ('module.hotel', '57');
INSERT INTO `db_version` VALUES ('module.entity', '1');
INSERT INTO `db_version` VALUES ('module.rating', '1');
INSERT INTO `db_version` VALUES ('module.comment', '3');
INSERT INTO `db_version` VALUES ('module.currency', '4');
INSERT INTO `db_version` VALUES ('module.currencycountries', '3');
INSERT INTO `db_version` VALUES ('module.menu', '2');
INSERT INTO `db_version` VALUES ('module.callback', '3');
INSERT INTO `db_version` VALUES ('module.publications', '11');
INSERT INTO `db_version` VALUES ('lib.paypall', '3');
INSERT INTO `db_version` VALUES ('module.labels', '2');
INSERT INTO `db_version` VALUES ('module.search', '1');
INSERT INTO `db_version` VALUES ('villa', '6');
INSERT INTO `db_version` VALUES ('lib.timepicker', '1');
INSERT INTO `db_version` VALUES ('lib.jquery.fancytransitions', '1');
INSERT INTO `db_version` VALUES ('module.slideshow', '1');
INSERT INTO `db_version` VALUES ('lib.jquery.form', '1');
INSERT INTO `db_version` VALUES ('lib.jquery.validate', '1');
INSERT INTO `db_version` VALUES ('lib.jquery.arcticmodal', '1');
INSERT INTO `db_version` VALUES ('module.config', '1');
INSERT INTO `db_version` VALUES ('module.group', '1');
INSERT INTO `db_version` VALUES ('lib.geoip', '1');
INSERT INTO `db_version` VALUES ('module.commentextended', '3');
INSERT INTO `db_version` VALUES ('lib.jsphp', '1');
INSERT INTO `db_version` VALUES ('module.questions', '3');
INSERT INTO `db_version` VALUES ('lib.jquery.elrte', '1');
INSERT INTO `db_version` VALUES ('module.handbooksgenerator', '1');
INSERT INTO `db_version` VALUES ('module.currencytowns', '3');
INSERT INTO `db_version` VALUES ('module.transportcompanies', '3');

-- ----------------------------
-- Table structure for `tst_comment`
-- ----------------------------
DROP TABLE IF EXISTS `tst_comment`;
CREATE TABLE `tst_comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `entity_type_id` int(11) unsigned NOT NULL,
  `entity_id` int(11) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `message` text,
  `approved` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comment_entity_type_id` (`entity_type_id`),
  CONSTRAINT `fk_comment_entity_type_id` FOREIGN KEY (`entity_type_id`) REFERENCES `tst_entity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Комментарии';

-- ----------------------------
-- Records of tst_comment
-- ----------------------------
INSERT INTO `tst_comment` VALUES ('1', '1', '1', '1346006878', '3454', 'ddd@dd.com', '45435', null);

-- ----------------------------
-- Table structure for `tst_commentextended`
-- ----------------------------
DROP TABLE IF EXISTS `tst_commentextended`;
CREATE TABLE `tst_commentextended` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `message` text,
  `approved` tinyint(1) NOT NULL,
  `parent_id` int(11) unsigned DEFAULT '0',
  `parents_tree` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Комментарии';

-- ----------------------------
-- Records of tst_commentextended
-- ----------------------------

-- ----------------------------
-- Table structure for `tst_currency`
-- ----------------------------
DROP TABLE IF EXISTS `tst_currency`;
CREATE TABLE `tst_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `reduction` varchar(10) NOT NULL,
  `factor` float(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tst_currency
-- ----------------------------
INSERT INTO `tst_currency` VALUES ('1', 'Евро', 'EUR', '1.20');

-- ----------------------------
-- Table structure for `tst_currencycountries`
-- ----------------------------
DROP TABLE IF EXISTS `tst_currencycountries`;
CREATE TABLE `tst_currencycountries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `lang_id` int(11) unsigned NOT NULL,
  `currency` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_currencycountries_currency` (`currency`),
  KEY `fk_currencycountries_lang_id` (`lang_id`),
  CONSTRAINT `fk_currencycountries_currency` FOREIGN KEY (`currency`) REFERENCES `tst_currency` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_currencycountries_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tst_currencycountries
-- ----------------------------

-- ----------------------------
-- Table structure for `tst_currencytowns`
-- ----------------------------
DROP TABLE IF EXISTS `tst_currencytowns`;
CREATE TABLE `tst_currencytowns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_currencytowns_country_id` (`country_id`),
  CONSTRAINT `fk_currencytowns_country_id` FOREIGN KEY (`country_id`) REFERENCES `tst_currencycountries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tst_currencytowns
-- ----------------------------

-- ----------------------------
-- Table structure for `tst_currency_lang`
-- ----------------------------
DROP TABLE IF EXISTS `tst_currency_lang`;
CREATE TABLE `tst_currency_lang` (
  `id` int(11) NOT NULL,
  `lang_id` int(11) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  KEY `fk_currency_lang_id` (`id`),
  KEY `fk_currency_lang_lang_id` (`lang_id`),
  CONSTRAINT `fk_currency_lang_id` FOREIGN KEY (`id`) REFERENCES `tst_currency` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_currency_lang_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='локализация типов валют';

-- ----------------------------
-- Records of tst_currency_lang
-- ----------------------------
INSERT INTO `tst_currency_lang` VALUES ('1', '1', 'Евро локал');

-- ----------------------------
-- Table structure for `tst_division`
-- ----------------------------
DROP TABLE IF EXISTS `tst_division`;
CREATE TABLE `tst_division` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `link` varchar(128) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `draw_order` int(11) DEFAULT NULL,
  `d_type` varchar(20) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `is_parrent` tinyint(4) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `link` (`link`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='Категории';

-- ----------------------------
-- Records of tst_division
-- ----------------------------
INSERT INTO `tst_division` VALUES ('1', 'root', '1', '', null, '1', 'url', '0', null, null);
INSERT INTO `tst_division` VALUES ('2', 'adminka', '1', '', null, '2', 'url', '0', null, null);
INSERT INTO `tst_division` VALUES ('3', 'client', '1', '', null, '3', 'url', '0', null, null);
INSERT INTO `tst_division` VALUES ('6', 'Номера', '2', '/manage/apartments/', null, '3', 'url', '0', null, null);
INSERT INTO `tst_division` VALUES ('7', 'Типы номеров', '2', '/manage/apartmentstypes/', null, '4', 'url', '0', null, null);
INSERT INTO `tst_division` VALUES ('8', 'Бронирование', '2', '/manage/booking/', null, '5', 'url', '0', null, null);
INSERT INTO `tst_division` VALUES ('9', 'Опции бронирования', '2', '/manage/bookingoptions/', null, '6', 'url', '0', null, null);
INSERT INTO `tst_division` VALUES ('10', 'Загрузка отеля', '2', '/manage/loading/', null, '7', 'url', '0', null, null);
INSERT INTO `tst_division` VALUES ('11', 'Оборудование', '2', '/manage/equipment/', null, '8', 'url', '0', null, null);
INSERT INTO `tst_division` VALUES ('12', 'Типы предоплат', '2', '/manage/bookingprepayment/', null, '9', 'url', '0', null, null);
INSERT INTO `tst_division` VALUES ('13', 'Кабинет', '2', '/manage/cashcontrol/', null, '10', 'url', '0', null, null);
INSERT INTO `tst_division` VALUES ('14', 'Кабинет Ген Директора', '2', '/manage/cashgendir/', null, '11', 'url', '0', null, null);
INSERT INTO `tst_division` VALUES ('15', 'Комментарии', '2', '/manage/comment/', null, '12', 'url', '0', null, null);
INSERT INTO `tst_division` VALUES ('16', 'Валюты', '2', '/manage/currency/', null, '13', 'url', '0', null, null);
INSERT INTO `tst_division` VALUES ('17', 'Страны', '2', '/manage/currencycountries/', null, '14', 'url', '0', null, null);
INSERT INTO `tst_division` VALUES ('18', 'Звонки', '2', '/manage/callback/', null, '15', 'url', '0', null, null);
INSERT INTO `tst_division` VALUES ('19', 'Публикации', '2', '/manage/publications/', null, '16', 'url', '0', null, null);
INSERT INTO `tst_division` VALUES ('20', 'Логи paypall', '2', '/manage/paylogs/', null, '17', 'url', '0', null, null);
INSERT INTO `tst_division` VALUES ('21', 'Метки', '2', '/manage/labels/', null, '18', 'url', '0', null, null);
INSERT INTO `tst_division` VALUES ('22', 'Пользователи', '2', '/manage/user/', null, '19', 'url', '0', null, null);
INSERT INTO `tst_division` VALUES ('23', 'Разделы', '2', '/manage/division/', null, '20', 'url', '0', null, null);
INSERT INTO `tst_division` VALUES ('24', 'Языки', '2', '/manage/languages/', null, '21', 'url', '0', null, null);
INSERT INTO `tst_division` VALUES ('25', 'homepage', '3', 'homepage', null, '1', 'url', '0', null, null);
INSERT INTO `tst_division` VALUES ('26', 'Управление отзывами', '2', '/manage/commentextended/', null, '22', 'url', '0', null, null);
INSERT INTO `tst_division` VALUES ('27', 'Управление вопросами', '2', '/manage/questions/', null, '23', 'url', '0', null, null);
INSERT INTO `tst_division` VALUES ('28', 'Скидки', '2', '/manage/discount/', null, '24', 'url', '0', null, null);
INSERT INTO `tst_division` VALUES ('29', 'Города', '2', '/manage/currencytowns/', null, '25', 'url', '0', null, null);
INSERT INTO `tst_division` VALUES ('30', 'Транспортные компании', '2', '/manage/transportcompanies/', null, '26', 'url', '0', null, null);

-- ----------------------------
-- Table structure for `tst_division_groups`
-- ----------------------------
DROP TABLE IF EXISTS `tst_division_groups`;
CREATE TABLE `tst_division_groups` (
  `division_id` int(11) unsigned DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  KEY `fk_division_groups_division_id` (`division_id`),
  KEY `fk_division_groups_group_id` (`group_id`),
  CONSTRAINT `fk_division_groups_division_id` FOREIGN KEY (`division_id`) REFERENCES `tst_division` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_division_groups_group_id` FOREIGN KEY (`group_id`) REFERENCES `tst_groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='соответствие категорий и групп пользователей';

-- ----------------------------
-- Records of tst_division_groups
-- ----------------------------
INSERT INTO `tst_division_groups` VALUES ('6', '3');
INSERT INTO `tst_division_groups` VALUES ('6', '2');
INSERT INTO `tst_division_groups` VALUES ('6', '1');
INSERT INTO `tst_division_groups` VALUES ('7', '3');
INSERT INTO `tst_division_groups` VALUES ('7', '2');
INSERT INTO `tst_division_groups` VALUES ('7', '1');
INSERT INTO `tst_division_groups` VALUES ('8', '3');
INSERT INTO `tst_division_groups` VALUES ('8', '2');
INSERT INTO `tst_division_groups` VALUES ('8', '1');
INSERT INTO `tst_division_groups` VALUES ('9', '3');
INSERT INTO `tst_division_groups` VALUES ('9', '2');
INSERT INTO `tst_division_groups` VALUES ('9', '1');
INSERT INTO `tst_division_groups` VALUES ('10', '3');
INSERT INTO `tst_division_groups` VALUES ('10', '2');
INSERT INTO `tst_division_groups` VALUES ('10', '1');
INSERT INTO `tst_division_groups` VALUES ('11', '3');
INSERT INTO `tst_division_groups` VALUES ('12', '3');
INSERT INTO `tst_division_groups` VALUES ('13', '3');
INSERT INTO `tst_division_groups` VALUES ('14', '3');
INSERT INTO `tst_division_groups` VALUES ('15', '3');
INSERT INTO `tst_division_groups` VALUES ('15', '2');
INSERT INTO `tst_division_groups` VALUES ('15', '1');
INSERT INTO `tst_division_groups` VALUES ('16', '3');
INSERT INTO `tst_division_groups` VALUES ('17', '3');
INSERT INTO `tst_division_groups` VALUES ('18', '3');
INSERT INTO `tst_division_groups` VALUES ('18', '2');
INSERT INTO `tst_division_groups` VALUES ('18', '1');
INSERT INTO `tst_division_groups` VALUES ('19', '3');
INSERT INTO `tst_division_groups` VALUES ('20', '3');
INSERT INTO `tst_division_groups` VALUES ('21', '3');
INSERT INTO `tst_division_groups` VALUES ('21', '2');
INSERT INTO `tst_division_groups` VALUES ('21', '1');
INSERT INTO `tst_division_groups` VALUES ('22', '3');
INSERT INTO `tst_division_groups` VALUES ('23', '3');
INSERT INTO `tst_division_groups` VALUES ('24', '3');
INSERT INTO `tst_division_groups` VALUES ('26', '3');
INSERT INTO `tst_division_groups` VALUES ('26', '2');
INSERT INTO `tst_division_groups` VALUES ('26', '1');
INSERT INTO `tst_division_groups` VALUES ('27', '3');
INSERT INTO `tst_division_groups` VALUES ('27', '2');
INSERT INTO `tst_division_groups` VALUES ('27', '1');
INSERT INTO `tst_division_groups` VALUES ('28', '3');
INSERT INTO `tst_division_groups` VALUES ('28', '2');
INSERT INTO `tst_division_groups` VALUES ('28', '1');
INSERT INTO `tst_division_groups` VALUES ('29', '3');
INSERT INTO `tst_division_groups` VALUES ('30', '3');

-- ----------------------------
-- Table structure for `tst_division_seo`
-- ----------------------------
DROP TABLE IF EXISTS `tst_division_seo`;
CREATE TABLE `tst_division_seo` (
  `menu_id` int(11) unsigned NOT NULL,
  `lang_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` text,
  KEY `fk_division_seo_lang_id` (`lang_id`),
  KEY `fk_division_seo_menu_id` (`menu_id`),
  CONSTRAINT `fk_division_seo_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_division_seo_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `tst_division` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='SEO данные категорий';

-- ----------------------------
-- Records of tst_division_seo
-- ----------------------------
INSERT INTO `tst_division_seo` VALUES ('23', null, null, null, null, null);
INSERT INTO `tst_division_seo` VALUES ('24', null, null, null, null, null);
INSERT INTO `tst_division_seo` VALUES ('25', '1', 'Главная страница', '', '', '');
INSERT INTO `tst_division_seo` VALUES ('26', null, null, null, null, null);
INSERT INTO `tst_division_seo` VALUES ('27', null, null, null, null, null);
INSERT INTO `tst_division_seo` VALUES ('28', null, null, null, null, null);
INSERT INTO `tst_division_seo` VALUES ('29', null, null, null, null, null);
INSERT INTO `tst_division_seo` VALUES ('30', null, null, null, null, null);

-- ----------------------------
-- Table structure for `tst_entity`
-- ----------------------------
DROP TABLE IF EXISTS `tst_entity`;
CREATE TABLE `tst_entity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `sys_name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Сущности';

-- ----------------------------
-- Records of tst_entity
-- ----------------------------
INSERT INTO `tst_entity` VALUES ('1', 'Общий отзыв', 'feedback');
INSERT INTO `tst_entity` VALUES ('2', 'Публикация', 'publication');

-- ----------------------------
-- Table structure for `tst_groups`
-- ----------------------------
DROP TABLE IF EXISTS `tst_groups`;
CREATE TABLE `tst_groups` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(32) DEFAULT NULL,
  `group_caption` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Группы пользователей';

-- ----------------------------
-- Records of tst_groups
-- ----------------------------
INSERT INTO `tst_groups` VALUES ('1', 'moder', 'Модератор');
INSERT INTO `tst_groups` VALUES ('2', 'admin', 'Администратор');
INSERT INTO `tst_groups` VALUES ('3', 'gendir', 'Ген Директор');
INSERT INTO `tst_groups` VALUES ('4', 'user', 'Пользователь');
INSERT INTO `tst_groups` VALUES ('5', 'guest', 'Гость');

-- ----------------------------
-- Table structure for `tst_hotel_apartments`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_apartments`;
CREATE TABLE `tst_hotel_apartments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `people` tinyint(2) NOT NULL,
  `childs` tinyint(2) NOT NULL,
  `guests` tinyint(2) NOT NULL,
  `sys_name` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hotel_apartments_type` (`type`),
  CONSTRAINT `fk_hotel_apartments_type` FOREIGN KEY (`type`) REFERENCES `tst_hotel_apartmentstypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=409 DEFAULT CHARSET=utf8 COMMENT='номера';

-- ----------------------------
-- Records of tst_hotel_apartments
-- ----------------------------
INSERT INTO `tst_hotel_apartments` VALUES ('101', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('102', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('103', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('104', '5', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('105', '5', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('106', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('107', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('108', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('109', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('110', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('111', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('112', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('113', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('114', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('115', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('116', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('201', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('202', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('203', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('204', '5', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('205', '5', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('206', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('207', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('208', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('209', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('210', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('211', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('212', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('213', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('214', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('215', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('216', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('301', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('302', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('303', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('304', '4', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('305', '4', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('306', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('307', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('308', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('309', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('310', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('311', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('312', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('313', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('314', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('315', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('316', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('401', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('402', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('403', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('404', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('405', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('406', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('407', '3', '2', '1', '0', '');
INSERT INTO `tst_hotel_apartments` VALUES ('408', '3', '2', '1', '0', '');

-- ----------------------------
-- Table structure for `tst_hotel_apartmentstypes`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_apartmentstypes`;
CREATE TABLE `tst_hotel_apartmentstypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `link` varchar(50) NOT NULL,
  `cost` int(11) NOT NULL,
  `standart_cost` float(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='типы номеров';

-- ----------------------------
-- Records of tst_hotel_apartmentstypes
-- ----------------------------
INSERT INTO `tst_hotel_apartmentstypes` VALUES ('3', 'Двухместный номер', 'hotel_double_room', '1', '0.70');
INSERT INTO `tst_hotel_apartmentstypes` VALUES ('4', 'Двухместный номер категории \"Люкс\"', 'hotel_luxe_room', '1200', '1400.00');
INSERT INTO `tst_hotel_apartmentstypes` VALUES ('5', 'Двухместный номер категории \"ПОЛУЛЮКС\"', 'hotel_junior_suite_room', '900', '1000.00');

-- ----------------------------
-- Table structure for `tst_hotel_apartmentstypes_equipment`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_apartmentstypes_equipment`;
CREATE TABLE `tst_hotel_apartmentstypes_equipment` (
  `apt_id` int(11) NOT NULL,
  `eq_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `bold` tinyint(1) NOT NULL,
  `fontsize` tinyint(2) NOT NULL,
  KEY `fk_hotel_apartmentstypes_equipment_apt_id` (`apt_id`),
  KEY `fk_hotel_apartmentstypes_equipment_eq_id` (`eq_id`),
  CONSTRAINT `fk_hotel_apartmentstypes_equipment_apt_id` FOREIGN KEY (`apt_id`) REFERENCES `tst_hotel_apartmentstypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_hotel_apartmentstypes_equipment_eq_id` FOREIGN KEY (`eq_id`) REFERENCES `tst_hotel_equipment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tst_hotel_apartmentstypes_equipment
-- ----------------------------
INSERT INTO `tst_hotel_apartmentstypes_equipment` VALUES ('3', '1', '1', '1', '0');
INSERT INTO `tst_hotel_apartmentstypes_equipment` VALUES ('3', '2', '2', '0', '16');
INSERT INTO `tst_hotel_apartmentstypes_equipment` VALUES ('3', '4', '3', '0', '0');
INSERT INTO `tst_hotel_apartmentstypes_equipment` VALUES ('3', '3', '4', '0', '0');

-- ----------------------------
-- Table structure for `tst_hotel_apartmentstypes_images`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_apartmentstypes_images`;
CREATE TABLE `tst_hotel_apartmentstypes_images` (
  `type_id` int(11) NOT NULL,
  `image` varchar(50) NOT NULL,
  `draw_order` tinyint(2) NOT NULL,
  KEY `fk_hotel_apartmentstypes_images_type_id` (`type_id`),
  CONSTRAINT `fk_hotel_apartmentstypes_images_type_id` FOREIGN KEY (`type_id`) REFERENCES `tst_hotel_apartmentstypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Изображения типов номеров';

-- ----------------------------
-- Records of tst_hotel_apartmentstypes_images
-- ----------------------------
INSERT INTO `tst_hotel_apartmentstypes_images` VALUES ('4', 'DSC_0010.jpg', '2');
INSERT INTO `tst_hotel_apartmentstypes_images` VALUES ('4', 'DSC_0015.jpg', '3');
INSERT INTO `tst_hotel_apartmentstypes_images` VALUES ('3', 'DSC_0121.jpg', '1');
INSERT INTO `tst_hotel_apartmentstypes_images` VALUES ('5', 'DSC_0032.jpg', '1');
INSERT INTO `tst_hotel_apartmentstypes_images` VALUES ('5', 'ava.jpg', '2');

-- ----------------------------
-- Table structure for `tst_hotel_apartmentstypes_lang`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_apartmentstypes_lang`;
CREATE TABLE `tst_hotel_apartmentstypes_lang` (
  `id` int(11) NOT NULL,
  `lang_id` int(11) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  KEY `fk_hotel_apartmentstypes_lang_lang_id` (`lang_id`),
  KEY `fk_hotel_apartmentstypes_lang_id` (`id`),
  CONSTRAINT `fk_hotel_apartmentstypes_lang_id` FOREIGN KEY (`id`) REFERENCES `tst_hotel_apartmentstypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_hotel_apartmentstypes_lang_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='локализация типов номеров';

-- ----------------------------
-- Records of tst_hotel_apartmentstypes_lang
-- ----------------------------
INSERT INTO `tst_hotel_apartmentstypes_lang` VALUES ('3', '1', 'Двухместный номер', '<p><span><strong><em>Двухместный номер категории Стандарт Дабл</em></strong> (общей площадью от 17 кв. м до 24 кв.м) - представляет собой уютный однокомнатный номер с красивым, комфортным и функциональным интерьером. Дизайн номера выполнен в классическом стиле, в теплых тонах и создает ощущение роскоши и спокойствия. Номер укомплектован современным мебельным набором из двуспальной кровати размера 190x220 (king-size), двух прикроватных тумбочек, письменного стола со стульями и зеркального шкафа-купе. Одна практически зеркальная стена прекрасно сочетается с интерьером номера и придает ему ощущения легкости и безграничности. Также в номере: LCD телевизор с 42ʺ диагональю, бесплатный высокоскоростной Wi-Fi доступ в интернет, телефон, электронный сейф, с возможностью хранения в нем ноутбука, холодильник/мини-бар а так же всё необходимое для комфортного отдыха. Ванная комната номера (общей площадью от 3 кв. м до 6 кв. м) укомплектована душем. В ней находится фен, комплект из 4-х полотенец, тапочек и всей необходимой парфумерии. Во всем номере, включая ванную комнату действует централизированая климатическая система, с регулированием температурного режима. Для большего комфорта номер оборудован электронным замком с магнитной карточкой для входной двери и электронным индикатором уборки номера. Уборка номера производится ежедневно, смена белья - раз в 3 дня.</span></p>');
INSERT INTO `tst_hotel_apartmentstypes_lang` VALUES ('3', '2', '45435', '<p>435345</p>');
INSERT INTO `tst_hotel_apartmentstypes_lang` VALUES ('4', '1', 'Двухместный номер категории \"Люкс\"', '<p>Двухместный номер категории Стандарт Дабл (общей площадью от 17 кв. м до 24 кв.м) - представляет собой уютный однокомнатный номер с красивым, комфортным и функциональным интерьером. Дизайн номера выполнен в классическом стиле, в теплых тонах и создает ощущение роскоши и спокойствия. Номер укомплектован современным мебельным набором из двуспальной кровати размера 190x220 (king-size), двух прикроватных тумбочек, письменного стола со стульями и зеркального шкафа-купе. Одна практически зеркальная стена прекрасно сочетается с интерьером номера и придает ему ощущения легкости и безграничности. Также в номере: LCD телевизор с 42ʺ диагональю, бесплатный высокоскоростной Wi-Fi доступ в интернет, телефон, электронный сейф, с возможностью хранения в нем ноутбука, холодильник/мини-бар а так же всё необходимое для комфортного отдыха. Ванная комната номера (общей площадью от 3 кв. м до 6 кв. м) укомплектована душем. В ней находится фен, комплект из 4-х полотенец, тапочек и всей необходимой парфумерии. Во всем номере, включая ванную комнату действует централизированая климатическая система, с регулированием температурного режима. Для большего комфорта номер оборудован электронным замком с магнитной карточкой для входной двери и электронным индикатором уборки номера. Уборка номера производится ежедневно, смена белья - раз в 3 дня.</p>');
INSERT INTO `tst_hotel_apartmentstypes_lang` VALUES ('5', '1', 'Двухместный номер категории \"ПОЛУЛЮКС\"', '<p>Номера категории полулюкс создают ощущение просторных апартаментов. Преимущества двух уютных разделенных зон: спальни и зоны отдыха, непременно оценит тот, кто путешествует не один.</p>');

-- ----------------------------
-- Table structure for `tst_hotel_booking`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_booking`;
CREATE TABLE `tst_hotel_booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `arrival` int(11) NOT NULL,
  `departure` int(11) NOT NULL,
  `people` int(3) NOT NULL,
  `comment` text NOT NULL,
  `prepayment` int(11) NOT NULL,
  `cost` float(8,2) NOT NULL,
  `costTotal` varchar(100) NOT NULL,
  `apartment` int(11) DEFAULT NULL,
  `firstName` varchar(100) NOT NULL,
  `secondName` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `creation_time` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `nights` int(2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hotel_booking_type` (`type`),
  KEY `fk_hotel_booking_apartment` (`apartment`),
  KEY `fk_hotel_booking_prepayment` (`prepayment`),
  KEY `fk_hotel_booking_uid` (`uid`),
  KEY `fk_hotel_booking_status` (`status`),
  KEY `fk_hotel_booking_parent_id` (`parent_id`),
  CONSTRAINT `fk_hotel_booking_apartment` FOREIGN KEY (`apartment`) REFERENCES `tst_hotel_apartments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_hotel_booking_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `tst_hotel_booking` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_hotel_booking_prepayment` FOREIGN KEY (`prepayment`) REFERENCES `tst_hotel_booking_prepayment_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_hotel_booking_status` FOREIGN KEY (`status`) REFERENCES `tst_hotel_booking_statuses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_hotel_booking_type` FOREIGN KEY (`type`) REFERENCES `tst_hotel_apartmentstypes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_hotel_booking_uid` FOREIGN KEY (`uid`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='бронирование';

-- ----------------------------
-- Records of tst_hotel_booking
-- ----------------------------
INSERT INTO `tst_hotel_booking` VALUES ('1', '3', '1349038800', '1349211600', '2', '111', '3', '7.00', '6', '101', 'Dorian', 'Gray', 'doriangray@ukr.net', '+380671234567', '1', '19', '1349083120', null, '0');
INSERT INTO `tst_hotel_booking` VALUES ('2', '3', '1350162000', '1350334800', '8', '', '3', '3.00', '2', '101', 'Dorian', 'Gray', 'dd@dd.com', '+380441234567', '1', '21', '1350250432', null, '0');
INSERT INTO `tst_hotel_booking` VALUES ('3', '3', '1350162000', '1350334800', '8', '', '3', '3.00', '2', '109', 'Dorian', 'Gray', 'dd@dd.com', '+380441234567', '1', '21', '1350250620', null, '0');
INSERT INTO `tst_hotel_booking` VALUES ('10', '3', '1350162000', '1350334800', '8', '', '3', '2.00', '0', null, 'Dorian', 'Gray', 'dd@dd.com', '+380441234567', '1', '21', '1350825951', null, '0');
INSERT INTO `tst_hotel_booking` VALUES ('11', '3', '1350162000', '1350334800', '8', '', '3', '3.00', '2', null, 'Dorian', 'Gray', 'dd@dd.com', '+380441234567', '1', '21', '1350826582', null, '0');
INSERT INTO `tst_hotel_booking` VALUES ('12', '3', '1350162000', '1350334800', '8', '', '3', '3.00', '2', null, 'Dorian', 'Gray', 'dd@dd.com', '+380441234567', '1', '21', '1350826842', null, '0');
INSERT INTO `tst_hotel_booking` VALUES ('13', '3', '1350162000', '1350334800', '8', '', '3', '3.00', '2', null, 'Dorian', 'Gray', 'dd@dd.com', '+380441234567', '1', '21', '1350826941', null, '0');
INSERT INTO `tst_hotel_booking` VALUES ('14', '3', '1350162000', '1350334800', '8', '', '3', '3.00', '2', null, 'Dorian', 'Gray', 'dd@dd.com', '+380441234567', '1', '21', '1350826941', null, '0');
INSERT INTO `tst_hotel_booking` VALUES ('15', '4', '1350162000', '1350334800', '8', '', '3', '1202.00', '2400', null, 'Dorian', 'Gray', 'dd@dd.com', '+380441234567', '1', '21', '1350826941', null, '0');
INSERT INTO `tst_hotel_booking` VALUES ('16', '5', '1350162000', '1350334800', '8', '', '3', '902.00', '1800', null, 'Dorian', 'Gray', 'dd@dd.com', '+380441234567', '1', '21', '1350826941', null, '0');
INSERT INTO `tst_hotel_booking` VALUES ('17', '3', '1350162000', '1350334800', '8', '', '3', '3.00', '2', null, 'Dorian', 'Gray', 'dd@dd.com', '+380441234567', '1', '21', '1350827697', null, '0');
INSERT INTO `tst_hotel_booking` VALUES ('18', '3', '1350162000', '1350334800', '8', '', '3', '3.00', '2', null, 'Dorian', 'Gray', 'dd@dd.com', '+380441234567', '1', '21', '1350827697', '17', '0');
INSERT INTO `tst_hotel_booking` VALUES ('19', '4', '1350162000', '1350334800', '8', '', '3', '1202.00', '2400', null, 'Dorian', 'Gray', 'dd@dd.com', '+380441234567', '1', '21', '1350827697', '17', '0');
INSERT INTO `tst_hotel_booking` VALUES ('20', '5', '1350162000', '1350334800', '8', '', '3', '902.00', '1800', null, 'Dorian', 'Gray', 'dd@dd.com', '+380441234567', '1', '21', '1350827697', '17', '0');
INSERT INTO `tst_hotel_booking` VALUES ('21', '3', '1350162000', '1350334800', '8', '', '3', '3.00', '2', null, 'Dorian', 'Gray', 'dd@dd.com', '+380441234567', '1', '21', '1351250926', null, '0');
INSERT INTO `tst_hotel_booking` VALUES ('22', '3', '1350162000', '1350334800', '8', '', '3', '3.00', '2', null, 'Dorian', 'Gray', 'dd@dd.com', '+380441234567', '1', '21', '1351250926', '21', '0');
INSERT INTO `tst_hotel_booking` VALUES ('23', '3', '1350162000', '1350334800', '8', '', '3', '43.00', '42', null, 'Dorian', 'Gray', 'dd@dd.com', '+380441234567', '1', '21', '1351256050', null, '0');
INSERT INTO `tst_hotel_booking` VALUES ('24', '3', '1350162000', '1350334800', '8', '', '3', '4.00', '4', '101', 'Dorian', 'Gray', 'dd@dd.com', '+380441234567', '1', '21', '1351256051', '23', '0');

-- ----------------------------
-- Table structure for `tst_hotel_booking_deposit_history`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_booking_deposit_history`;
CREATE TABLE `tst_hotel_booking_deposit_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `summ` int(11) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hotel_booking_deposit_history_booking_id` (`booking_id`),
  KEY `fk_hotel_booking_deposit_history_user_id` (`user_id`),
  CONSTRAINT `fk_hotel_booking_deposit_history_booking_id` FOREIGN KEY (`booking_id`) REFERENCES `tst_hotel_booking` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_hotel_booking_deposit_history_user_id` FOREIGN KEY (`user_id`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='фин история бронирования';

-- ----------------------------
-- Records of tst_hotel_booking_deposit_history
-- ----------------------------

-- ----------------------------
-- Table structure for `tst_hotel_booking_options`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_booking_options`;
CREATE TABLE `tst_hotel_booking_options` (
  `hotel_booking_id` int(11) NOT NULL,
  `sample_id` int(11) NOT NULL,
  KEY `fk_hotel_booking_options_hotel_booking_id` (`hotel_booking_id`),
  CONSTRAINT `fk_hotel_booking_options_hotel_booking_id` FOREIGN KEY (`hotel_booking_id`) REFERENCES `tst_hotel_booking` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='опции бронирования';

-- ----------------------------
-- Records of tst_hotel_booking_options
-- ----------------------------
INSERT INTO `tst_hotel_booking_options` VALUES ('1', '1');
INSERT INTO `tst_hotel_booking_options` VALUES ('23', '1');
INSERT INTO `tst_hotel_booking_options` VALUES ('23', '2');
INSERT INTO `tst_hotel_booking_options` VALUES ('24', '1');

-- ----------------------------
-- Table structure for `tst_hotel_booking_options_samples`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_booking_options_samples`;
CREATE TABLE `tst_hotel_booking_options_samples` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `cost` float(10,2) NOT NULL,
  `reusable` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='примеры опций бронирования';

-- ----------------------------
-- Records of tst_hotel_booking_options_samples
-- ----------------------------
INSERT INTO `tst_hotel_booking_options_samples` VALUES ('1', 'Завтрак с человека', '2.00', '0');
INSERT INTO `tst_hotel_booking_options_samples` VALUES ('2', 'Ужин', '3.00', '0');
INSERT INTO `tst_hotel_booking_options_samples` VALUES ('3', 'Туристическое обслуживание', '4.00', '0');
INSERT INTO `tst_hotel_booking_options_samples` VALUES ('4', 'Девочки', '2.23', '0');

-- ----------------------------
-- Table structure for `tst_hotel_booking_options_samples_lang`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_booking_options_samples_lang`;
CREATE TABLE `tst_hotel_booking_options_samples_lang` (
  `id` int(11) NOT NULL,
  `lang_id` int(11) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  KEY `fk_hotel_booking_options_samples_lang_id` (`id`),
  KEY `fk_hotel_booking_options_samples_lang_lang_id` (`lang_id`),
  CONSTRAINT `fk_hotel_booking_options_samples_lang_id` FOREIGN KEY (`id`) REFERENCES `tst_hotel_booking_options_samples` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_hotel_booking_options_samples_lang_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tst_hotel_booking_options_samples_lang
-- ----------------------------
INSERT INTO `tst_hotel_booking_options_samples_lang` VALUES ('1', '1', 'Завтрак с человека');
INSERT INTO `tst_hotel_booking_options_samples_lang` VALUES ('1', '2', 'lweidhfierwj');
INSERT INTO `tst_hotel_booking_options_samples_lang` VALUES ('2', '2', 'Ужин ');
INSERT INTO `tst_hotel_booking_options_samples_lang` VALUES ('3', '2', 'kuglkjlo');
INSERT INTO `tst_hotel_booking_options_samples_lang` VALUES ('2', '1', 'Ужин');
INSERT INTO `tst_hotel_booking_options_samples_lang` VALUES ('3', '1', 'Туристическое обслуживание');
INSERT INTO `tst_hotel_booking_options_samples_lang` VALUES ('4', '1', '2');

-- ----------------------------
-- Table structure for `tst_hotel_booking_prepayment_types`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_booking_prepayment_types`;
CREATE TABLE `tst_hotel_booking_prepayment_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `cost_modifier` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='типы предоплаты бронирования';

-- ----------------------------
-- Records of tst_hotel_booking_prepayment_types
-- ----------------------------
INSERT INTO `tst_hotel_booking_prepayment_types` VALUES ('1', 'Без предоплаты', '2');
INSERT INTO `tst_hotel_booking_prepayment_types` VALUES ('2', 'Предоплата за первую ночь', '1');
INSERT INTO `tst_hotel_booking_prepayment_types` VALUES ('3', 'Предоплата за весь период проживания', '0');

-- ----------------------------
-- Table structure for `tst_hotel_booking_prepayment_types_lang`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_booking_prepayment_types_lang`;
CREATE TABLE `tst_hotel_booking_prepayment_types_lang` (
  `id` int(11) NOT NULL,
  `lang_id` int(11) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  KEY `fk_hotel_booking_prepayment_types_lang_id` (`id`),
  KEY `fk_hotel_booking_prepayment_types_lang_lang_id` (`lang_id`),
  CONSTRAINT `fk_hotel_booking_prepayment_types_lang_id` FOREIGN KEY (`id`) REFERENCES `tst_hotel_booking_prepayment_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_hotel_booking_prepayment_types_lang_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tst_hotel_booking_prepayment_types_lang
-- ----------------------------
INSERT INTO `tst_hotel_booking_prepayment_types_lang` VALUES ('1', '1', 'Бронирование без предоплаты');
INSERT INTO `tst_hotel_booking_prepayment_types_lang` VALUES ('2', '1', 'Предоплата за первую ночь');
INSERT INTO `tst_hotel_booking_prepayment_types_lang` VALUES ('3', '1', 'Предоплата за весь период проживания');
INSERT INTO `tst_hotel_booking_prepayment_types_lang` VALUES ('1', '2', 'powetrporeifp');

-- ----------------------------
-- Table structure for `tst_hotel_booking_statuses`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_booking_statuses`;
CREATE TABLE `tst_hotel_booking_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tst_hotel_booking_statuses
-- ----------------------------
INSERT INTO `tst_hotel_booking_statuses` VALUES ('1', 'Не обработано');
INSERT INTO `tst_hotel_booking_statuses` VALUES ('2', 'В обработке');
INSERT INTO `tst_hotel_booking_statuses` VALUES ('3', 'Обработано');
INSERT INTO `tst_hotel_booking_statuses` VALUES ('4', 'Отменено пользователем');

-- ----------------------------
-- Table structure for `tst_hotel_callback`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_callback`;
CREATE TABLE `tst_hotel_callback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(20) NOT NULL,
  `status` varchar(50) NOT NULL,
  `view_flag` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='обратный звонок';

-- ----------------------------
-- Records of tst_hotel_callback
-- ----------------------------
INSERT INTO `tst_hotel_callback` VALUES ('1', '+380675641740', '', '1');
INSERT INTO `tst_hotel_callback` VALUES ('2', '+380675641740', '', '1');
INSERT INTO `tst_hotel_callback` VALUES ('3', '+380675641740', '', '1');
INSERT INTO `tst_hotel_callback` VALUES ('4', '+380675641740', '', '1');

-- ----------------------------
-- Table structure for `tst_hotel_cash`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_cash`;
CREATE TABLE `tst_hotel_cash` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `balance` float(10,2) NOT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_hotel_cash_user_id` FOREIGN KEY (`user_id`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='баланс работников отеля';

-- ----------------------------
-- Records of tst_hotel_cash
-- ----------------------------
INSERT INTO `tst_hotel_cash` VALUES ('13', '14.34');

-- ----------------------------
-- Table structure for `tst_hotel_cash_transactions`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_cash_transactions`;
CREATE TABLE `tst_hotel_cash_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_from` int(11) NOT NULL,
  `user_to` int(11) NOT NULL,
  `summ` float(10,2) NOT NULL,
  `date` int(11) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hotel_cash_transactions_user_from` (`user_from`),
  KEY `fk_hotel_cash_transactions_user_to` (`user_to`),
  CONSTRAINT `fk_hotel_cash_transactions_user_from` FOREIGN KEY (`user_from`) REFERENCES `tst_hotel_cash` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_hotel_cash_transactions_user_to` FOREIGN KEY (`user_to`) REFERENCES `tst_hotel_cash` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='фин операции';

-- ----------------------------
-- Records of tst_hotel_cash_transactions
-- ----------------------------

-- ----------------------------
-- Table structure for `tst_hotel_discount`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_discount`;
CREATE TABLE `tst_hotel_discount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `discount` int(2) NOT NULL,
  `discount_percent` int(2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hotel_discount_option_id` (`option_id`),
  KEY `fk_hotel_discount_user_id` (`user_id`),
  CONSTRAINT `fk_hotel_discount_option_id` FOREIGN KEY (`option_id`) REFERENCES `tst_hotel_booking_options_samples` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_hotel_discount_user_id` FOREIGN KEY (`user_id`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tst_hotel_discount
-- ----------------------------
INSERT INTO `tst_hotel_discount` VALUES ('1', '1', '19', '0', '99');

-- ----------------------------
-- Table structure for `tst_hotel_equipment`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_equipment`;
CREATE TABLE `tst_hotel_equipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_name` varchar(50) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tst_hotel_equipment
-- ----------------------------
INSERT INTO `tst_hotel_equipment` VALUES ('1', 'LCD телевизор с 42\\\" экраном, кабельное телевидени', '0');
INSERT INTO `tst_hotel_equipment` VALUES ('2', 'Холодильник / минибар', '0');
INSERT INTO `tst_hotel_equipment` VALUES ('3', 'Digital Safe, size: H19,5×43×37(cm)', '0');
INSERT INTO `tst_hotel_equipment` VALUES ('4', 'Двухспальная кровать king-size', '0');
INSERT INTO `tst_hotel_equipment` VALUES ('5', 'Прикроватные тумбочки, шкаф - купе', '0');
INSERT INTO `tst_hotel_equipment` VALUES ('6', 'Ванная комната  укомплектованная    душем', '0');
INSERT INTO `tst_hotel_equipment` VALUES ('7', 'Комнатные тапочки и ванные принадлежности', '0');
INSERT INTO `tst_hotel_equipment` VALUES ('8', 'Фен', '0');
INSERT INTO `tst_hotel_equipment` VALUES ('9', 'Телефон, услуга звонок-будильник', '0');
INSERT INTO `tst_hotel_equipment` VALUES ('10', 'Возможность затемнения комнаты в светлое время сут', '0');
INSERT INTO `tst_hotel_equipment` VALUES ('11', 'Подушки наполненные гусиным пухом', '0');
INSERT INTO `tst_hotel_equipment` VALUES ('12', 'Одеяла наполненные верблюжьим подпушком', '0');
INSERT INTO `tst_hotel_equipment` VALUES ('13', 'Подушки наполненные гусиным пухом', '0');

-- ----------------------------
-- Table structure for `tst_hotel_equipment_lang`
-- ----------------------------
DROP TABLE IF EXISTS `tst_hotel_equipment_lang`;
CREATE TABLE `tst_hotel_equipment_lang` (
  `id` int(11) NOT NULL,
  `lang_id` int(11) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  KEY `fk_hotel_equipment_lang_id` (`id`),
  KEY `fk_hotel_equipment_lang_lang_id` (`lang_id`),
  CONSTRAINT `fk_hotel_equipment_lang_id` FOREIGN KEY (`id`) REFERENCES `tst_hotel_equipment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_hotel_equipment_lang_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tst_hotel_equipment_lang
-- ----------------------------
INSERT INTO `tst_hotel_equipment_lang` VALUES ('1', '1', 'LCD телевизор с 42\\\" экраном, кабельное телевидени');
INSERT INTO `tst_hotel_equipment_lang` VALUES ('2', '1', 'Холодильник / минибар');
INSERT INTO `tst_hotel_equipment_lang` VALUES ('3', '1', 'Электронный сейф, размером 19,5×43×37(cm)');
INSERT INTO `tst_hotel_equipment_lang` VALUES ('3', '2', 'Digital Safe, size: H19,5×43×37(cm)');
INSERT INTO `tst_hotel_equipment_lang` VALUES ('4', '1', 'Двухспальная кровать king-size');
INSERT INTO `tst_hotel_equipment_lang` VALUES ('5', '1', 'Прикроватные тумбочки, шкаф - купе');
INSERT INTO `tst_hotel_equipment_lang` VALUES ('6', '1', 'Ванная комната  укомплектованная    душем');
INSERT INTO `tst_hotel_equipment_lang` VALUES ('7', '1', 'Комнатные тапочки и ванные принадлежности');
INSERT INTO `tst_hotel_equipment_lang` VALUES ('8', '1', 'Фен');
INSERT INTO `tst_hotel_equipment_lang` VALUES ('9', '1', 'Телефон, услуга звонок-будильник');
INSERT INTO `tst_hotel_equipment_lang` VALUES ('10', '1', 'Возможность затемнения комнаты в светлое время сут');
INSERT INTO `tst_hotel_equipment_lang` VALUES ('11', '1', 'Подушки наполненные гусиным пухом');
INSERT INTO `tst_hotel_equipment_lang` VALUES ('12', '1', 'Одеяла наполненные верблюжьим подпушком');
INSERT INTO `tst_hotel_equipment_lang` VALUES ('13', '1', 'Подушки наполненные гусиным пухом');

-- ----------------------------
-- Table structure for `tst_labels`
-- ----------------------------
DROP TABLE IF EXISTS `tst_labels`;
CREATE TABLE `tst_labels` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sys_name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COMMENT='labels';

-- ----------------------------
-- Records of tst_labels
-- ----------------------------
INSERT INTO `tst_labels` VALUES ('1', 'login');
INSERT INTO `tst_labels` VALUES ('2', 'aboutHotel');
INSERT INTO `tst_labels` VALUES ('3', 'numbers');
INSERT INTO `tst_labels` VALUES ('4', 'contacts');
INSERT INTO `tst_labels` VALUES ('5', 'backCall');
INSERT INTO `tst_labels` VALUES ('6', 'callMe');
INSERT INTO `tst_labels` VALUES ('7', 'example');
INSERT INTO `tst_labels` VALUES ('8', 'newWindowMap');
INSERT INTO `tst_labels` VALUES ('9', 'bookingNumber');
INSERT INTO `tst_labels` VALUES ('10', 'arriveDate');
INSERT INTO `tst_labels` VALUES ('11', 'departureDate');
INSERT INTO `tst_labels` VALUES ('12', 'type');
INSERT INTO `tst_labels` VALUES ('13', 'adults');
INSERT INTO `tst_labels` VALUES ('14', 'children');
INSERT INTO `tst_labels` VALUES ('15', 'name');
INSERT INTO `tst_labels` VALUES ('16', 'lastName');
INSERT INTO `tst_labels` VALUES ('17', 'email');
INSERT INTO `tst_labels` VALUES ('18', 'phone');
INSERT INTO `tst_labels` VALUES ('19', 'comment');
INSERT INTO `tst_labels` VALUES ('20', 'cost');
INSERT INTO `tst_labels` VALUES ('21', 'forADay');
INSERT INTO `tst_labels` VALUES ('22', 'forWholeTerm');
INSERT INTO `tst_labels` VALUES ('23', 'doBooking');
INSERT INTO `tst_labels` VALUES ('24', 'description');
INSERT INTO `tst_labels` VALUES ('25', 'equipment');
INSERT INTO `tst_labels` VALUES ('26', 'checkPresence');
INSERT INTO `tst_labels` VALUES ('27', 'booking');
INSERT INTO `tst_labels` VALUES ('28', 'arrive');
INSERT INTO `tst_labels` VALUES ('29', 'departure');
INSERT INTO `tst_labels` VALUES ('30', 'adults');
INSERT INTO `tst_labels` VALUES ('31', 'children');
INSERT INTO `tst_labels` VALUES ('32', 'keepFeedback');
INSERT INTO `tst_labels` VALUES ('33', 'details');
INSERT INTO `tst_labels` VALUES ('34', 'search');

-- ----------------------------
-- Table structure for `tst_labels_lang`
-- ----------------------------
DROP TABLE IF EXISTS `tst_labels_lang`;
CREATE TABLE `tst_labels_lang` (
  `id` int(11) unsigned DEFAULT NULL,
  `lang_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  KEY `fk_labels_lang_lang_id` (`lang_id`),
  KEY `fk_labels_lang_id` (`id`),
  CONSTRAINT `fk_labels_lang_id` FOREIGN KEY (`id`) REFERENCES `tst_labels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_labels_lang_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='языковые данные labels';

-- ----------------------------
-- Records of tst_labels_lang
-- ----------------------------
INSERT INTO `tst_labels_lang` VALUES ('1', '1', 'Логин');
INSERT INTO `tst_labels_lang` VALUES ('2', '1', 'Об отеле');
INSERT INTO `tst_labels_lang` VALUES ('3', '1', 'Номера');
INSERT INTO `tst_labels_lang` VALUES ('4', '1', 'Контакты');
INSERT INTO `tst_labels_lang` VALUES ('5', '1', 'обратный звонок');
INSERT INTO `tst_labels_lang` VALUES ('6', '1', 'перезвоните мне');
INSERT INTO `tst_labels_lang` VALUES ('7', '1', 'Например');
INSERT INTO `tst_labels_lang` VALUES ('8', '1', 'Карта в отдельном окне');
INSERT INTO `tst_labels_lang` VALUES ('9', '1', 'Бронировать номер');
INSERT INTO `tst_labels_lang` VALUES ('10', '1', 'Дата заезда');
INSERT INTO `tst_labels_lang` VALUES ('11', '1', 'Дата выезда');
INSERT INTO `tst_labels_lang` VALUES ('12', '1', 'Тип');
INSERT INTO `tst_labels_lang` VALUES ('13', '1', 'Взрослые');
INSERT INTO `tst_labels_lang` VALUES ('14', '1', 'Дети');
INSERT INTO `tst_labels_lang` VALUES ('15', '1', 'Имя');
INSERT INTO `tst_labels_lang` VALUES ('16', '1', 'Фамилия');
INSERT INTO `tst_labels_lang` VALUES ('17', '1', 'Email');
INSERT INTO `tst_labels_lang` VALUES ('18', '1', 'Телефон');
INSERT INTO `tst_labels_lang` VALUES ('19', '1', 'Комментарий');
INSERT INTO `tst_labels_lang` VALUES ('20', '1', 'Стоимость');
INSERT INTO `tst_labels_lang` VALUES ('21', '1', 'Стоимость');
INSERT INTO `tst_labels_lang` VALUES ('22', '1', 'за весь период');
INSERT INTO `tst_labels_lang` VALUES ('23', '1', 'Забронировать');
INSERT INTO `tst_labels_lang` VALUES ('24', '1', 'Описание');
INSERT INTO `tst_labels_lang` VALUES ('25', '1', 'Номер укомплектован');
INSERT INTO `tst_labels_lang` VALUES ('26', '1', 'Бронировать');
INSERT INTO `tst_labels_lang` VALUES ('27', '1', 'БРОНИРОВАНИЕ');
INSERT INTO `tst_labels_lang` VALUES ('28', '1', 'Заезд');
INSERT INTO `tst_labels_lang` VALUES ('29', '1', 'Выезд');
INSERT INTO `tst_labels_lang` VALUES ('30', '1', 'Взрослые');
INSERT INTO `tst_labels_lang` VALUES ('31', '1', 'Дети');
INSERT INTO `tst_labels_lang` VALUES ('32', '1', 'оставить отзыв');
INSERT INTO `tst_labels_lang` VALUES ('33', '1', 'подробнее');
INSERT INTO `tst_labels_lang` VALUES ('34', '1', 'Поиск');

-- ----------------------------
-- Table structure for `tst_languages`
-- ----------------------------
DROP TABLE IF EXISTS `tst_languages`;
CREATE TABLE `tst_languages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `sys_name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Языки';

-- ----------------------------
-- Records of tst_languages
-- ----------------------------
INSERT INTO `tst_languages` VALUES ('1', 'Русский', 'ua');
INSERT INTO `tst_languages` VALUES ('2', 'English', 'eng');
INSERT INTO `tst_languages` VALUES ('3', 'Deutsch', 'de');

-- ----------------------------
-- Table structure for `tst_paypall_log`
-- ----------------------------
DROP TABLE IF EXISTS `tst_paypall_log`;
CREATE TABLE `tst_paypall_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_type` varchar(50) DEFAULT NULL,
  `payment_date` varchar(50) DEFAULT NULL,
  `payment_status` varchar(50) DEFAULT NULL,
  `address_status` varchar(50) DEFAULT NULL,
  `payer_status` varchar(50) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `payer_email` varchar(50) DEFAULT NULL,
  `payer_id` varchar(50) DEFAULT NULL,
  `address_name` varchar(50) DEFAULT NULL,
  `address_country` varchar(50) DEFAULT NULL,
  `address_country_code` varchar(50) DEFAULT NULL,
  `address_zip` varchar(50) DEFAULT NULL,
  `address_state` varchar(50) DEFAULT NULL,
  `address_city` varchar(50) DEFAULT NULL,
  `address_street` varchar(100) DEFAULT NULL,
  `business` varchar(50) DEFAULT NULL,
  `receiver_email` varchar(50) DEFAULT NULL,
  `receiver_id` varchar(50) DEFAULT NULL,
  `residence_country` varchar(50) DEFAULT NULL,
  `item_name` varchar(50) DEFAULT NULL,
  `item_number` varchar(50) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `shipping` float(10,2) NOT NULL,
  `tax` float(10,2) NOT NULL,
  `mc_currency` varchar(50) DEFAULT NULL,
  `mc_fee` float(10,2) NOT NULL,
  `mc_gross` float(10,2) NOT NULL,
  `txn_type` varchar(50) DEFAULT NULL,
  `txn_id` int(11) DEFAULT NULL,
  `notify_version` varchar(50) DEFAULT NULL,
  `custom` varchar(50) DEFAULT NULL,
  `invoice` varchar(50) DEFAULT NULL,
  `charset` varchar(50) DEFAULT NULL,
  `verify_sign` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='paypall логи';

-- ----------------------------
-- Records of tst_paypall_log
-- ----------------------------
INSERT INTO `tst_paypall_log` VALUES ('1', 'echeck', '16:45:48 May 28, 2012 PDT', 'Completed', 'confirmed', 'verified', 'John', 'Smith', 'buyer@paypalsandbox.com', 'TESTBUYERID01', 'John Smith', 'United States', 'US', '95131', 'CA', 'San Jose', '123, any street', 'seller@paypalsandbox.com', 'seller@paypalsandbox.com', 'TESTSELLERID1', 'US', 'something', 'AK-1234', '1', '3.04', '2.02', 'USD', '0.44', '12.34', 'web_accept', '485282345', '2.1', 'xyz123', 'abc1234', 'windows-1252', 'AVGUa04bUtMm3j7yed7qpzQsUMbEAjf-R-uwAtkl4Rw-kxjQ07pNYaXE');
INSERT INTO `tst_paypall_log` VALUES ('2', 'instant', '02:37:51 Sep 23, 2012 PDT', 'Completed', null, 'verified', 'DMYTRO', 'SUKACHEV', '0675641740ua@gmail.com', 'D4PA53HAH897Y', null, null, null, null, null, null, null, 'villalascala@gmail.com', 'villalascala@gmail.com', 'TRVR5KCTKRD5J', 'UA', null, null, null, '0.00', '0.00', 'USD', '0.34', '1.00', 'cart', '0', '3.7', null, null, 'gb2312', 'ArFaDaxZIj2Ipo7jmUIDFyZo1becAltd169j1xAucZLUAnMKPNykS5i.');

-- ----------------------------
-- Table structure for `tst_publications`
-- ----------------------------
DROP TABLE IF EXISTS `tst_publications`;
CREATE TABLE `tst_publications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(124) NOT NULL,
  `url` varchar(100) NOT NULL,
  `date` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `division` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_publications_uid` (`uid`),
  KEY `fk_publications_division` (`division`),
  CONSTRAINT `fk_publications_division` FOREIGN KEY (`division`) REFERENCES `tst_division` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_publications_uid` FOREIGN KEY (`uid`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='публикации';

-- ----------------------------
-- Records of tst_publications
-- ----------------------------
INSERT INTO `tst_publications` VALUES ('1', 'Контакты', 'contacts', '-10800', '0', '', null, null);
INSERT INTO `tst_publications` VALUES ('2', 'Об отеле', 'about', '-10800', '0', '', null, null);
INSERT INTO `tst_publications` VALUES ('3', 'первая тестовая новость', 'pervaya-testovaya-novost.html', '1349816400', '0', 'Hero.png', null, '25');

-- ----------------------------
-- Table structure for `tst_publications_division`
-- ----------------------------
DROP TABLE IF EXISTS `tst_publications_division`;
CREATE TABLE `tst_publications_division` (
  `publications_id` int(11) NOT NULL,
  `division_id` int(11) unsigned NOT NULL,
  KEY `publications_id` (`publications_id`),
  KEY `division_id` (`division_id`),
  CONSTRAINT `publications_division_ibfk_1` FOREIGN KEY (`publications_id`) REFERENCES `tst_publications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `publications_division_ibfk_2` FOREIGN KEY (`division_id`) REFERENCES `tst_division` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tst_publications_division
-- ----------------------------

-- ----------------------------
-- Table structure for `tst_publications_lang`
-- ----------------------------
DROP TABLE IF EXISTS `tst_publications_lang`;
CREATE TABLE `tst_publications_lang` (
  `page_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `page_title` varchar(124) NOT NULL,
  `page_content` text NOT NULL,
  `page_preview` text NOT NULL,
  `title` text NOT NULL,
  `keywords` text NOT NULL,
  `description` text NOT NULL,
  KEY `fk_publications_lang_page_id` (`page_id`),
  CONSTRAINT `fk_publications_lang_page_id` FOREIGN KEY (`page_id`) REFERENCES `tst_publications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='публикации';

-- ----------------------------
-- Records of tst_publications_lang
-- ----------------------------
INSERT INTO `tst_publications_lang` VALUES ('1', '1', 'Контакты', '<p>Украина, город Киев, просп. Отрадный, дом 7.</p>', '', '', '', '');
INSERT INTO `tst_publications_lang` VALUES ('1', '2', 'Contacts', 'our contacts', '', '', '', '');
INSERT INTO `tst_publications_lang` VALUES ('2', '1', 'Об отеле', '<p>Добрый день, Вас приветствует отель Villa LaScala .</p>\n<p>Гости нашего отеля могут рассчитывать на качественное предоставление услуг, заботливое обслуживание и профессионализм англоязычного персонала. <br />Отель гарантирует безопасность и конфиденциальность своим гостям. Здесь присутствуют атмосфера гармонии и покоя. Номерной фонд отеля - 54 номерa, разных категорий: Люкс - 2 шт, Полулюкс - 4 шт , Стандарт Дабл - 38 шт. Отель оформлен в теплых тонах.</p>\n<p>Каждый номер оборудован современной мебелью и большой кроватью размера King-size. В номере имеется, LCD-телевизор с 42 дюймовым экраном, бесплатный высокоскоростной Wi-Fi, холодильник/мини-бар, электронный сейф с возможностью использования его под ноутбук, телефонная связь, электрочайник с посудой и все не маловажные мелочи, для комфортного длительного или краткосрочного проживания. <br />Все номера оборудованы ванной комнатой (туалет, душ, горячая и холодная вода, фен) с подогревом полов. Гостям предоставляются персональные наборы из трех полотенец, халата и тапочек.</p>\n<p>Все номера оборудованы электронными замками с магнитной карточкой для входной двери, что обеспечивает безопасность и удобства в использовании</p>\n<p>Благодаря централизированой климатической системе, с регулированием температурного режима, в помещениях зимой всегда тепло, а летом прохладно, что полностью избавляет от необходимости использования кондиционеров.</p>\n<p>В отеле работает круглосуточная стойка администратора, что позволяет гостю, заселится в любое удобное для него время суток.<br />Когда угодно(день ,ночь), не выходя из своего номера Вы можете воспользоваться Room servise (предоставление закусок в номера), и заказать понравившееся Вам блюда из меню и насладиться их вкусом .</p>\n<p>На территории гостиницы проходит постоянно действующая выставка- ярмарка работ знаменитых украинских фотографов. Постояльцы гостиницы имеют исключительную возможность приобретения любой понравившейся им работы.</p>\n<p>На территории гостиницы находится ресторан в украинском стиле.<br />Посетив который, вы сможете окунуться в глубину национальных обычаев и быта, насладиться национальной украинской кухней и полюбоваться громадной картиной известных украинских художников выполненной маслом (общая площадь картины составляет 45 кв. метров).</p>\n<p>Так же на территории гостиницы расположен фирменный французский магазин постельного белья LaScala. Где вы сможете приобрести постельное белье, покрывала, подушки, одеяла, простыни и полотенца из натуральных материалов высокого качества: хлопка, шёлка, шерсти, пуха, по доступной цене. Такое приобретении станет шикарным сувениром для Вас и всей Ваших близких. Ознакомится с ассортиментом магазина, вы можете заранее http://www.lascala.ua/.</p>\n<p>При необходимости, персонал отеля в любое время суток встретит Вас в аэропорту Борисполь с табличкой на выходе из зоны прилета, поможет с багажом и поселением в гостиницу.</p>\n<p>Мы всегда очень Вам рады! С Уважением дружный коллектив гостиницы Villa LaScala!</p>', '', '', '', 'Добрый день, Вас приветствует отель Villa LaScala .\n\nГости нашего отеля могут рассчитывать на качественное предоставление услуг, заботливое обслуживание и профессионализм англоязычного персонала. \nОтель гарантирует безопасность и конфиденциальность своим гостям. Здесь присутствуют атмосфера гармонии и покоя. Номерной фонд отеля - 54 номерa, разных категорий: Люкс - 2 шт, Полулюкс - 4 шт , Стандарт Дабл - 38 шт. Отель оформлен в теплых тонах.\n\nКаждый номер оборудован современной мебелью и большой кроватью размера King-size. В номере имеется, LCD-телевизор с 42 дюймовым экраном, бесплатный высокоскоростной Wi-Fi, холодильник/мини-бар, электронный сейф с возможностью использования его под ноутбук, телефонная связь, электрочайник с посудой и все не маловажные мелочи, для комфортного длительного или краткосрочного проживания. \nВсе номера оборудованы ванной комнатой (туалет, душ, горячая и холодная вода, фен) с подогревом полов. Гостям предоставляются персональные наборы из трех полотенец, халата и тапочек.\n\nВсе номера оборудованы электронными замками с магнитной карточкой для входной двери, что обеспечивает безопасность и удобства в использовании\n\nБлагодаря централизированой климатической системе, с регулированием температурного режима, в помещениях зимой всегда тепло, а летом прохладно, что полностью избавляет от необходимости использования кондиционеров.\n\nВ отеле работает круглосуточная стойка администратора, что позволяет гостю, заселится в любое удобное для него время суток.\nКогда угодно(день ,ночь), не выходя из своего номера Вы можете воспользоваться Room servise (предоставление закусок в номера), и заказать понравившееся Вам блюда из меню и насладиться их вкусом .\n\nНа территории гостиницы проходит постоянно действующая выставка- ярмарка работ знаменитых украинских фотографов. Постояльцы гостиницы имеют исключительную возможность приобретения любой понравившейся им работы.\n\nНа территории гостиницы находится ресторан в украинском стиле.\nПосетив который, вы сможете окунуться в глубину национальных обычаев и быта, насладиться национальной украинской кухней и полюбоваться громадной картиной известных украинских художников выполненной маслом (общая площадь картины составляет 45 кв. метров).\n\nТак же на территории гостиницы расположен фирменный французский магазин постельного белья LaScala. Где вы сможете приобрести постельное белье, покрывала, подушки, одеяла, простыни и полотенца из натуральных материалов высокого качества: хлопка, шёлка, шерсти, пуха, по доступной цене. Такое приобретении станет шикарным сувениром для Вас и всей Ваших близких. Ознакомится с ассортиментом магазина, вы можете заранее http://www.lascala.ua/.\n\nПри необходимости, персонал отеля в любое время суток встретит Вас в аэропорту Борисполь с табличкой на выходе из зоны прилета, поможет с багажом и поселением в гостиницу.\n\nМы всегда очень Вам рады! С Уважением дружный коллектив гостиницы Villa LaScala!');
INSERT INTO `tst_publications_lang` VALUES ('3', '1', 'первая тестовая новость', '<p>первая тестовая новость</p>', '', '', '', '');

-- ----------------------------
-- Table structure for `tst_questions`
-- ----------------------------
DROP TABLE IF EXISTS `tst_questions`;
CREATE TABLE `tst_questions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `message` text,
  `approved` tinyint(1) NOT NULL,
  `parent_id` int(11) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='вопросы';

-- ----------------------------
-- Records of tst_questions
-- ----------------------------
INSERT INTO `tst_questions` VALUES ('1', '1356876452', 'Администрация', '', '', '0', '0');
INSERT INTO `tst_questions` VALUES ('2', '1356876462', 'Администрация', 'serg@thebest.com', 'sdfsdfg', '1', '0');
INSERT INTO `tst_questions` VALUES ('3', '1356876503', 'Администрация', 'ddd@dd.com', 'ffff22', '1', '2');
INSERT INTO `tst_questions` VALUES ('4', '1356876527', 'Разработка', 'serg', 'Ну как?', '0', '0');

-- ----------------------------
-- Table structure for `tst_rating`
-- ----------------------------
DROP TABLE IF EXISTS `tst_rating`;
CREATE TABLE `tst_rating` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rating` int(11) DEFAULT '0',
  `vote_count` int(11) DEFAULT '0',
  `entity_type_id` int(11) DEFAULT NULL,
  `entity_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Система рейтингов';

-- ----------------------------
-- Records of tst_rating
-- ----------------------------

-- ----------------------------
-- Table structure for `tst_rating_log`
-- ----------------------------
DROP TABLE IF EXISTS `tst_rating_log`;
CREATE TABLE `tst_rating_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `obj_id` int(11) unsigned DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `sid` varchar(32) DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_rating_log_obj_id` (`obj_id`),
  CONSTRAINT `fk_rating_log_obj_id` FOREIGN KEY (`obj_id`) REFERENCES `tst_rating` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Лог рейтингов';

-- ----------------------------
-- Records of tst_rating_log
-- ----------------------------

-- ----------------------------
-- Table structure for `tst_sessions`
-- ----------------------------
DROP TABLE IF EXISTS `tst_sessions`;
CREATE TABLE `tst_sessions` (
  `uid` int(11) NOT NULL,
  `session` varchar(32) NOT NULL,
  `last_update` int(11) NOT NULL,
  `ip` varchar(20) NOT NULL,
  PRIMARY KEY (`uid`),
  CONSTRAINT `fk_sessions_uid` FOREIGN KEY (`uid`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tst_sessions
-- ----------------------------
INSERT INTO `tst_sessions` VALUES ('19', '357502919c5386aa38e2187617211f8d', '1350335883', '127.0.0.1');

-- ----------------------------
-- Table structure for `tst_transportcompanies`
-- ----------------------------
DROP TABLE IF EXISTS `tst_transportcompanies`;
CREATE TABLE `tst_transportcompanies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `image` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tst_transportcompanies
-- ----------------------------

-- ----------------------------
-- Table structure for `tst_transportcompanies_towns`
-- ----------------------------
DROP TABLE IF EXISTS `tst_transportcompanies_towns`;
CREATE TABLE `tst_transportcompanies_towns` (
  `town_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  KEY `fk_transportcompanies_towns_town_id` (`town_id`),
  KEY `fk_transportcompanies_towns_company_id` (`company_id`),
  CONSTRAINT `fk_transportcompanies_towns_company_id` FOREIGN KEY (`company_id`) REFERENCES `tst_transportcompanies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_transportcompanies_towns_town_id` FOREIGN KEY (`town_id`) REFERENCES `tst_currencytowns` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tst_transportcompanies_towns
-- ----------------------------

-- ----------------------------
-- Table structure for `tst_users`
-- ----------------------------
DROP TABLE IF EXISTS `tst_users`;
CREATE TABLE `tst_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `rating` int(3) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `login` varchar(32) DEFAULT NULL,
  `restory_code` varchar(100) DEFAULT NULL COMMENT 'Поле кода доступа к генерации нового пароля',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tst_users
-- ----------------------------
INSERT INTO `tst_users` VALUES ('13', 'paypall', 'paypall', '1', '0', null, null);
INSERT INTO `tst_users` VALUES ('18', 'admin', '202cb962ac59075b964b07152d234b70', '100', '1', null, null);
INSERT INTO `tst_users` VALUES ('19', 'doriangray@ukr.net', '202cb962ac59075b964b07152d234b70', '1', '1', null, null);
INSERT INTO `tst_users` VALUES ('20', '0675641740ua@gmail.com', '202cb962ac59075b964b07152d234b70', '1', '0', null, null);
INSERT INTO `tst_users` VALUES ('21', 'dd@dd.com', '202cb962ac59075b964b07152d234b70', '1', '1', null, null);

-- ----------------------------
-- Table structure for `tst_user_groups`
-- ----------------------------
DROP TABLE IF EXISTS `tst_user_groups`;
CREATE TABLE `tst_user_groups` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  KEY `fk_user_groups_user_id` (`user_id`),
  KEY `fk_user_groups_group_id` (`group_id`),
  CONSTRAINT `fk_user_groups_group_id` FOREIGN KEY (`group_id`) REFERENCES `tst_groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_groups_user_id` FOREIGN KEY (`user_id`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tst_user_groups
-- ----------------------------
INSERT INTO `tst_user_groups` VALUES ('13', '2');
INSERT INTO `tst_user_groups` VALUES ('18', '3');

-- ----------------------------
-- Table structure for `tst_user_profile`
-- ----------------------------
DROP TABLE IF EXISTS `tst_user_profile`;
CREATE TABLE `tst_user_profile` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `fio` varchar(100) NOT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_user_profile_user_id` FOREIGN KEY (`user_id`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tst_user_profile
-- ----------------------------
INSERT INTO `tst_user_profile` VALUES ('13', 'paypall');
