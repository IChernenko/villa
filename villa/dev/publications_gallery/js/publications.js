villaPublications={
    switchImage:function()
    {
        var mainImgSrc = $('#imageBlockMainImage').attr('src');
        var applyNext = 0;
        var imgSrcFirst = '';
        var changedFlag = 0;
        
        $('#similarProdWrap ul li').each(function() {
            
            var imgSrc = $(this).children('a').children('img').attr('src');
            
            if(imgSrcFirst == ''){imgSrcFirst = imgSrc;}
            if(applyNext == 1){
                applyNext = 0;
                changedFlag = 1;
                villaPublications.changeMainImage(imgSrc);
                return;
            }
            if(imgSrc == mainImgSrc){
                applyNext = 1;
            }
        });
        
        //���� �������� �� �������, ����������� �� ������
        if(changedFlag == 0){
            villaPublications.changeMainImage(imgSrcFirst);
        }
    },
    changeMainImage: function(imgSrc)
    {
        $("#imageBlockMainImage").fadeOut('fast',function(){
            $('#imageBlockMainImage').attr('src', imgSrc);
        });
        $("#imageBlockMainImage").fadeIn('slow');
    },
    OpenImagePopup: function (imgPath, title, alt) {
        var win = window.open('','preview',
        'width=50,height=50,left=0,top=0,screenX=0,screenY=0,resizable=1,scrollbar=0,status=0');

        var winDoc = win.document;
        if (title == undefined) title = 'My Image, Click to Close';
        if (alt == undefined) alt = 'My Image, Click to Close';
        var content = '<html><head><title>' + title + '</title>' +
        '<style>body{overflow: hidden;margin:0;}img{border:0;}</style>' +
        '</head><body><a href="javascript:self.close()">' +
        '<img alt="' + alt + '" id="image" src="' + imgPath + '" /></a></body></html>'
        win.document.write(content);

        winDoc.body.onload = function() {
        var obj = winDoc.getElementById('image');
        var w = obj.width, h = obj.height;
        var iHeight= document.body.clientHeight, iWidth = self.innerWidth;

        var left = (self.opera ? iWidth : screen.availWidth)/2 - w/2;
        var top = (self.opera ? iHeight : screen.availHeight)/2 - h/2;
        win.resizeTo(w+10, h+26);
        win.moveTo(left, top);
        }

        win.onload = winDoc.body.onload; // special for Mozilla

        // !!! Important statement: popup onload won't execute without it!
        win.document.close();
        win.focus();
    }
}

//checkNewComment();
setInterval(function() { villaPublications.switchImage(); }, 5000);