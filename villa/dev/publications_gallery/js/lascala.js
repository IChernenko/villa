//Subscription input
function clearInput()
{
    if($('#subscription input').val() == 'введите свой e-mail')
    {                             
        $('#subscription input').val('');
        $('#subscription input').css('text-align', 'left');
    }
}
function changeInput()
{
    if($('#subscription input').val() == '') 
    {
        $('#subscription input').val('введите свой e-mail');
        $('#subscription input').css('text-align', 'center');
    }
}
//
function addToCart(element)
{
    $('.tempCartImage').remove();
    
    var liTag = $(element).closest('li');
    var img = liTag.children('a').children('img');
    var offsets = img.offset();
    //console.log(img.attr('src'));
    
    var newImage = img.clone();
    newImage.attr('class', 'tempCartImage');
    newImage.css('position', 'absolute');
    newImage.css('left', offsets.left+'px');
    newImage.css('top', offsets.top+'px');
    newImage.css('zIndex', '1005');
    newImage.appendTo('body');
    
    var cartOffsets = $('#bucketPreviewWrap').offset();
    
    newImage.animate({
        opacity: 0.25,
        left: cartOffsets.left,
        top: cartOffsets.top,
        height: 47+'px',
        width: 60+'px'
        //height: 'toggle'
      }, 1000, function() {
        // Animation complete.
        $('.tempCartImage').remove();
      });
}

//Dropdown
$(document).ready(function(){menuDropdown();});
function menuDropdown()
{
    $('li.has-level-2').hover(
    function()
    {
        var list = $(this).find('ul');
        $(list).stop(true, true).slideDown(200);
    }, 
    function()
    {
        var list = $(this).find('ul');
        $(list).stop(true, true).animate({opacity: 'hide', height: 'hide'}, 500);
    });
}
//

//Catalog items header dropdown
$(document).ready(function(){showItemHeader();});
function showItemHeader()
{
    $('ul#catalog').find('li').hover(
    function()
    {
        var header = $(this).find('div.head');
        $(header).stop(true, true).slideDown(150);
    },
    function()
    {
        var header = $(this).find('div.head');
        $(header).stop(true, true).slideUp(150);
    });
}