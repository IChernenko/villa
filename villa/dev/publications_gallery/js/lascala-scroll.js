$(document).ready(function(){setWidth();});

function setWidth()
{
    var ulScroll = $('ul.scrollable');
    if(ulScroll.length > 0)
        {
            for(var i=0; i < ulScroll.length; i++)
            {
                var listItems = $(ulScroll[i]).children('li');
                var listWidth = 0;
                for(var j=0; j < listItems.length; j++)
                    {
                        listWidth += $(listItems[j]).outerWidth(true);                        
                    }
                $(ulScroll[i]).width(listWidth);
            }
        }    
}

function scrolling(div, direction)
{
    var divId = '#'+div;
    var wrapId = divId+'Wrap';
    var listId = divId+'List';
    
    var itemWidth = $($(listId).children('li')[0]).outerWidth(true);
    var maxScroll = $(listId).width()-$(wrapId).width();
    
    var value = $(wrapId).scrollLeft();
    var check = value/itemWidth-Math.floor(value/itemWidth);
    if(direction == 'right' && value < maxScroll)
        {   
            if(check == 0)
                {
                    scrollingRight(divId, wrapId, value, itemWidth, maxScroll);
                }
            else
                {
                    value += itemWidth-(value%itemWidth);
                    scrollingRight(divId, wrapId, value, itemWidth, maxScroll);
                }   
        }        
    if(direction == 'left' && value > 0)
        {            
            if(check == 0)
                {
                    scrollingLeft(divId, wrapId, value, itemWidth, maxScroll);
                }
            else
                {
                    value -= value%itemWidth;
                    console.log('value='+value);
                    scrollingLeft(divId, wrapId, value, itemWidth, maxScroll);
                }
        }        
}

function scrollingRight(divId, wrapId, value, itemWidth, maxScroll)
{
    var newValue = false;
    newValue = value + itemWidth;
    $(wrapId).animate({scrollLeft: newValue}, 500);
    if(newValue == itemWidth)
        {
            var buttonLeft = $(divId).find('button.arrow-l')[0];
            $(buttonLeft).fadeIn(300);
        }
    if(newValue == maxScroll)
        {
            var buttonRight = $(divId).find('button.arrow-r')[0];
            $(buttonRight).fadeOut(300);
        }
}

function scrollingLeft(divId, wrapId, value, itemWidth, maxScroll)
{
    var newValue = false;
    newValue = value - itemWidth;
    $(wrapId).animate({scrollLeft: newValue}, 500);
    if(newValue == (maxScroll-itemWidth))
        {
            var buttonRight = $(divId).find('button.arrow-r')[0];
            $(buttonRight).fadeIn(300);
        }
    if(newValue == 0)
        {
            var buttonLeft = $(divId).find('button.arrow-l')[0];
            $(buttonLeft).fadeOut(300);
        }
}

