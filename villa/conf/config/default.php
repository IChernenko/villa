<?php

Dante_Lib_Config::set('url.domain_main',      'http://'.$_SERVER['HTTP_HOST']);

Dante_Lib_Config::set('defaultController',          'module.home');
Dante_Lib_Config::set('app.ws',          'villa');

Dante_Lib_Config::set('app.title',          'Default app');

Dante_Lib_Config::set('app.enableLogger',          true);


Dante_Lib_Config::set('DB.db_login',          'root');
Dante_Lib_Config::set('DB.db_psw',            '');
Dante_Lib_Config::set('DB.db_host',           'localhost');
Dante_Lib_Config::set('DB.db_dbname',         'staging_villa');
Dante_Lib_Config::set('DB.db_port',           '');
Dante_Lib_Config::set('DB.db_driver', 'mysqli');
Dante_Lib_Config::set('DB.tablePrefix',       'tst_');

Dante_Lib_Config::set('app.uploadFolder',          'media');
Dante_Lib_Config::set('app.uploadFolderFullPath',  '/villa/media/');


Dante_Lib_Config::set('smtp.email',           'noreply@cupsforphone.com');
Dante_Lib_Config::set('smtp.from',            'CupsForPhone');
Dante_Lib_Config::set('smtp.host',            'mail.astralbit.com');
Dante_Lib_Config::set('smtp.port',            25);
Dante_Lib_Config::set('smtp.login',           '');
Dante_Lib_Config::set('smtp.pass',            '');


?>
