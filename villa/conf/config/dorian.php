<?php

Dante_Lib_Config::set('defaultController', 'module.publications.list');

Dante_Lib_Config::set('app.enableLogger',          true);
Dante_Lib_Config::set('DB.db_login',          'dorian_villa');
Dante_Lib_Config::set('DB.db_psw',            '123');
Dante_Lib_Config::set('DB.db_host',           'localhost');
Dante_Lib_Config::set('DB.db_dbname',         'dorian_villa');
Dante_Lib_Config::set('DB.db_port',           '');
Dante_Lib_Config::set('DB.tablePrefix',       'tst_');

Dante_Lib_Config::set('app.uploadFolder',          'media');
Dante_Lib_Config::set('app.uploadFolderFullPath',  '/villa/media/');

Dante_Lib_Config::set('smtp.email',           'noreply@cupsforphone.com');
Dante_Lib_Config::set('smtp.from',            'CupsForPhone');
Dante_Lib_Config::set('smtp.host',            'mail.astralbit.com');
Dante_Lib_Config::set('smtp.port',            25);
Dante_Lib_Config::set('smtp.login',           '');
Dante_Lib_Config::set('smtp.pass',            '');
?>
