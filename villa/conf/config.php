<?php

include_once('conf/rules.php');

if (file_exists('conf/config/shared.php')) {
    include_once('conf/config/shared.php');
}

$host = $_SERVER['HTTP_HOST'];
$configFile = Dante_Lib_Config::get('host:'.$host);
$configFilePath = false;

if ($configFile) {
    $configFilePath = 'conf/config/'.$configFile;
}
else {
    $configFilePath = 'conf/config/default.php';
}

//echo($configFilePath);
include_once($configFilePath);

?>
