<?php

$url = Dante_Controller_Front::getRequest()->get_requested_url();

Dante_Lib_Router::addRule('/^(\/[a-z]{2,3})?\/about\/$/', array(
    'controller' => 'Villa_Controller_Homepage',
    'action' => 'drawAbout'
));




Dante_Lib_Router::addRule("/manage\/comment\//", array(
    'controller' => 'Villa_Module_Comment_Manage_Controller_Comment'
));
Dante_Lib_Router::addRule("/manage\/questions\//", array(
    'controller' => 'Villa_Module_Comment_Manage_Controller_Questions'
));

Dante_Lib_Router::addRule('/^(\/[a-z]{2,3})?\/comments\/$/', array(
    'controller' => 'Villa_Module_Comment_Controller',
    'params' => array('type' => 1)
));
Dante_Lib_Router::addRule("/^(\/[a-z]{2,3})?\/questions\/$/", array(
    'controller' => 'Villa_Module_Comment_Controller',
    'params' => array('type' => 2)
));

Dante_Lib_Router::addRule("/sync\-test\//", array(
    'controller' => 'Villa_App_Sync_Test'
));

Dante_Lib_Router::addRule("/manage\/labels\//", array(
    'controller' => 'Villa_Module_Labels_Manage_Controller'
));

?>
