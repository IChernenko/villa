<?php
global $package;
$package = array(
    'version' => '1',
    'name' => 'component.swfupload',
    'dependence' => array(),
    'js' => array(
        '../component/swfupload/js/swfupload.js',
        '../component/swfupload/js/plugins/swfupload.queue.js',
        '../component/swfupload/js/main.js'
    ),
    'css' => array(
    )
);