<?php
/**
 * загрузчик файлов
 *
 * @author Mort, Elrafir, Dorian
 */

class Component_Swfupload_Upload{

    public $generateRandomFileName = false;
    
   /**
    * 
    * @param string $uploadDir
    * @param string $newFileName
    * @param boolean $returnAllNames - true то вернёт дефолтное имя файла, false(default) вернёт новое имя файла
    * @return string|array|boolean
    * 
    * @author Mort, Elrafir, Dorian
    */
    public function upload($uploadDir, $newFileName = false, $returnAllName=FALSE) {
        
        if(!$uploadDir)$uploadDir = 'uploads/'; //папка для хранения файлов

        Dante_Lib_Log_Factory::getLogger()->debug("check path for dir : ".$uploadDir);
        Component_Vcl_Checkpath::check($uploadDir);

        //$allowedExt = array('jpg', 'jpeg', 'png', 'gif');
        $maxFileSize = Dante_Helper_System::convertBytes( ini_get('upload_max_filesize') ); //1024 * 1024 * 1024; //1 MB
        //echo($maxFileSize);
        //если получен файл
        if (!isset($_FILES))  {
            Dante_Lib_Log_Factory::getLogger()->debug("exception : Нет файлов для загрузки");
            throw new Exception('Нет файлов для загрузки', 7002);
        }
            
        //проверяем размер и тип файла
        //$ext = end(explode('.', strtolower($_FILES['Filedata']['name'])));
        $path_info = pathinfo(strtolower($_FILES['Filedata']['name']));
        if (isset($path_info['extension']))  $ext = $path_info['extension'];
        
        
        /*++if (!in_array($ext, $allowedExt)) {
            return;
        }*/
        
        if ($maxFileSize < $_FILES['Filedata']['size']) {
            Dante_Lib_Log_Factory::getLogger()->debug('exception : Размер файла должен быть меньше '.$maxFileSize);
            throw new Exception('Размер файла должен быть меньше '.$maxFileSize, 7000);
        }

        $tmpFileName = $_FILES['Filedata']['tmp_name'];
        if (!is_uploaded_file($tmpFileName)) {
            Dante_Lib_Log_Factory::getLogger()->debug('exception : Файл не загрузился на сервер : '.$tmpFileName);
            throw new Exception('Файл не загрузился на сервер ', 7003);    
        }

        /*if ($this->generateRandomFileName) {
            $fileName = time();    
        }*/
        $fileName = $_FILES['Filedata']['name'];
        $oldFileName = $fileName;
        if ($newFileName) {
            $fileName = $newFileName.'.'.$ext;
        }
        
        //если файл с таким именем уже существует...
        if (file_exists($fileName)) {
            //...добавляем текущее время к имени файла
            $nameParts = explode('.', $_FILES['Filedata']['name']);
            $nameParts[count($nameParts)-2] .= time();
            $fileName = $uploadDir.implode('.', $nameParts);
        }


        Dante_Lib_Log_Factory::getLogger()->debug("move_uploaded_file from  $tmpFileName to ".$uploadDir.$fileName);
        if (!move_uploaded_file($tmpFileName, $uploadDir.$fileName)) {
            throw new Exception('Не могу сохранить файл на сервере  ', 7001);
        }
        
        //echo '<img src="'.$fileName.'" alt="'.$fileName.'" />';
        if($returnAllName){
            return array($oldFileName, $fileName);                      
        }else{
            return $fileName;
        }
        
        
    }
}
