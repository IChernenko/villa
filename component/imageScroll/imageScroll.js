/**
 * Created with JetBrains PhpStorm.
 * User: dorian
 * Date: 9/26/13
 * Time: 12:10 AM
 * To change this template use File | Settings | File Templates.
 */
values =
{
    slider: 0,
    sliderGet: function()
    {
        return this.slider;
    },
    sliderChange: function(value)
    {
        this.slider = value;
    },

    imgSelect: 0,
    imgSelectGet: function()
    {
        return this.imgSelect;
    },
    imgSelectChange: function(value)
    {
        this.imgSelect = value;
    },

    simModels: 0,
    simModelsGet: function()
    {
        return this.simModels;
    },
    simModelsChange: function(value)
    {
        this.simModels = value;
    }
}

function setWidth()
{
    var ulScroll = $('ul.scrollable');
    if(ulScroll.length > 0)
    {
        for(var i=0; i < ulScroll.length; i++)
        {
            var listItems = $(ulScroll[i]).children('li');
            var listWidth = 0;
            for(var j=0; j < listItems.length; j++)
            {
                listWidth += $(listItems[j]).outerWidth(true);
            }
            $(ulScroll[i]).width(listWidth);
        }
    }
}

function scrollingLeft(list)
{
    var listId = '#'+list;
    var itemWidth = $($(listId).children('li')[0]).outerWidth(true);
    var listWidth = $(listId).width();
    var value = false;
    var newValue = false;
    if(listId == '#sliderList')
    {
        value = values.sliderGet();
        if(value > 0)
        {
            newValue = value - itemWidth*3;
            $('#sliderWrap').animate({scrollLeft: newValue}, 900);
            values.sliderChange(newValue);
            buttonClasses('#sliderMain', newValue, listWidth);
        }
    }
    if(listId == '#imgSelectList')
    {
        value = values.imgSelectGet();
        if(value > 0)
        {
            newValue = value - itemWidth;
            $('#imgSelectWrap').animate({scrollLeft: newValue}, 600);
            values.imgSelectChange(newValue);
            buttonClasses('#imgSelect', newValue, listWidth);
        }
    }
    if(listId == '#similarModelsList')
    {
        value = values.simModelsGet();
        if(value > 0)
        {
            newValue = value - itemWidth;
            $('#similarModelsWrap').animate({scrollLeft: newValue}, 600);
            values.simModelsChange(newValue);
            buttonClasses('#similarModels', newValue, listWidth);
        }
    }
}

function scrollingRight(list)
{
    var listId = '#'+list;
    var itemWidth = $($(listId).children('li')[0]).outerWidth(true);
    var listWidth = $(listId).width();
    var value = false;
    var endWidth = false;
    var newValue = false;
    if(listId == '#sliderList')
    {
        value = values.sliderGet();
        endWidth = listWidth-3*itemWidth;
        if(value < endWidth)
        {
            newValue = value + itemWidth*3;
            $('#sliderWrap').animate({scrollLeft: newValue}, 900);
            values.sliderChange(newValue);
            buttonClasses('#sliderMain', newValue, endWidth);
        }
    }
    if(listId == '#imgSelectList')
    {
        value = values.imgSelectGet();
        endWidth = listWidth-3*itemWidth;
        if(value < endWidth)
        {
            newValue = value + itemWidth;
            $('#imgSelectWrap').animate({scrollLeft: newValue}, 600);
            values.imgSelectChange(newValue);
            buttonClasses('#imgSelect', newValue, endWidth);
        }
    }
    if(listId == '#similarModelsList')
    {
        value = values.simModelsGet();
        endWidth = listWidth-7*itemWidth;
        if(value < endWidth)
        {
            newValue = value + itemWidth;
            $('#similarModelsWrap').animate({scrollLeft: newValue}, 600);
            values.simModelsChange(newValue);
            buttonClasses('#similarModels', newValue, endWidth);
        }
    }
}

function buttonClasses(divId, scrollValue, endWidth)
{
    var buttonLeft = $(divId).children('button.arrow-left')[0];
    var buttonRight = $(divId).children('button.arrow-right')[0];
    if(scrollValue == 0)
    {
        $(buttonLeft).addClass('disabled');
        $(buttonRight).removeClass('disabled');
    }
    if(scrollValue == endWidth)
    {
        $(buttonRight).addClass('disabled');
        $(buttonLeft).removeClass('disabled');
    }
    if(scrollValue > 0 && scrollValue < endWidth)
    {
        $($(divId).children('button')).removeClass('disabled');
    }
}