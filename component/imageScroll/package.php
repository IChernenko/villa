<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 9/26/13
 * Time: 12:13 AM
 * To change this template use File | Settings | File Templates.
 */
global $package;
$package = array(
    'version' => '1',
    'name' => 'component.imageScroll',
    'dependence' => array(),
    'js' => array(
        '../component/imageScroll/imageScroll.js'
    ),
    'css' => array(
    )
);