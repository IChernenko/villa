<?php
global $package;
$package = array(
    'version' => '1',
    'name' => 'component.jalerts',
    'dependence' => array(),
    'js' => array(
        '../component/jalerts/jquery.alerts.js'
    ),
    'css' => array(
        '../component/jalerts/jquery.alerts.css'
    )
);