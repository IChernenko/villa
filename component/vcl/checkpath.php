<?php

/**
 * построитель деревьев
 *
 * @author Mort
 */
class Component_Vcl_Checkpath{
    
    /**
     *
     * @param string path
     * @return boolean 
     */
    public static function check($path) {
        
        if(!is_dir($path)){
            $pathArray = explode('/', $path);
            
            $pathTemp = '';
            foreach ($pathArray as $value) {
                $pathTemp .= $value.'/';
                if(!is_dir($pathTemp)){
                    //Надо создать дирректорию
                    Dante_Lib_Log_Factory::getLogger()->debug("Component_Vcl_Checkpath : try to create dir : ".$pathTemp);
                    if (!mkdir($pathTemp,0755)) {
                        Dante_Lib_Log_Factory::getLogger()->debug("Component_Vcl_Checkpath : failed to create dir : ".$pathTemp);
                    }
                }
            }
            
        }
        
        
        return true;
    }
    
}

?>
