<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 22.05.12
 * Time: 15:25
 * Elrafir - Добавил возможность  указать в параметрах класс для <options> и  value при желании
 * To change this template use File | Settings | File Templates.
 */
class Component_Vcl_Combobox
{
    
    /**
     * Элементы комбобокса - перечень options
     * @var array
     */
    protected $_items = array();
    
    protected $_newItems = array();
    /**
     * Элементы комбобокса - атрибуты селекта
     * @var array
     */
    protected $_attributes = array();
    
    /**
     * Элемент со значением selected='selected'
     * @var string|int
     */
    public $selected_value='';
    
    protected $_tree = false;
    /**
     * Добавление аттрибута
     * @param string $name
     * @param string $value
     * @author Dorian
     */
    public function addAttribute($name, $value) {
        $this->_attributes[$name] = $value;
    }
    
    public function tree($childIdName='id', $parentIdName='parent_id', $parentId=1, $itemValueKey='id' , $itemTextKey='name', $prefix='&sdot;&#9002; ', $separator='&sdot;&sdot;&sdot;'){
        //добавляем параметры для древа.
        $this->_tree = array(
            'parentId'      => $parentId,
            'childIName'    => $childIdName,
            'parentIdName'  => $parentIdName,
            'itemValueKey'  => $itemValueKey,
            'itemTextKey'   => $itemTextKey,
            'prefix'        => $prefix,
            'separator'     => $separator
        );
    }
    public function getItems(){
        return $this->_items;
    }

        /**
     * Добавление массива атрибутов в формате array('key'=>'val')
     * @param array $array
     */
    public function addAttributeArray($array){
        $this->_attributes=$array;
    }
    protected function _attributesToString() {
        $html = '';
        if (count($this->_attributes)==0) {
            return $html;
        }

        foreach($this->_attributes as $name => $value) {
            $html .= "{$name}=\"{$value}\"";
        }

        return $html;
    }
    
    public function addItem($itemValue, $itemCaption) {
        $this->_items[$itemValue] = $itemCaption;
    }
    
    /**
     * Доюавление полей для комбобокса в формате array('value'=>'caption')
     * @param array $options массив для тегов options 
     */
    public function addItemsArray($options){
        foreach($options as $key=>$val){
            $this->addItem($key, $val);
        }
    }

    /**
     * Выводит комбик
     * @return string
     */
    public function draw() {
        
        $html = '<select '.$this->_attributesToString().'>';
        foreach($this->_items as $value => $cap) {
            if(!is_array($cap)){
                $caption=array(
                    'text'=>$cap
                );
            }else{
                $caption=$cap;
            }
            $class=false;
            if(isset($caption['class'])){
                if(!is_array($caption['class'])){
                    $class[]=$caption['class'];
                }else{
                    $class=$caption['class'];
                }
                $class='class="'.join(' ', $class).'"';
                            }
            if(isset($caption['value'])){
                $value=$caption['value'];
            }
            if(isset($caption['disabled'])){
                $disabled='disabled="disabled"';
            }else{
                $disabled='';
            }
            $html .= "<option value=\"{$value}\" {$class} ".($this->selected_value==$value?'selected="selected"':'')." {$disabled}>{$caption['text']}</option>";
        }
        $html .= '</select>';
        return $html;
    }
    
    public function _addItemsArrayTree($vars = false, $parent_id=1, $separator = ''){
        if(!$vars) return false;        
        foreach($vars as $cap){
            $cap[$this->_tree['itemTextKey']] = $separator.$this->_tree['prefix'].$cap[$this->_tree['itemTextKey']];
            if($cap[$this->_tree['parentIdName']]== $parent_id){
                //Добавляем элемент и отправляем за дочерними итемами
                $this->_items[$cap[$this->_tree['itemValueKey']]] = $cap[$this->_tree['itemTextKey']];
                if($cap[$this->_tree['parentIdName']]==$cap[$this->_tree['childIName']]) continue;
                $this->_addItemsArrayTree($vars, $cap[$this->_tree['childIName']], $separator.$this->_tree['separator']);
            }            
        }
        return $this->_items;
    }
}