<?php

/**
 * построитель деревьев
 *
 * @author Mort
 */
class Component_Vcl_Tree{
    
    /**
     *
     * @param array $drawTreeParams
     * @return string 
     */
    public function drawTree($drawTreeParams, $firstParentKeyValue=0) {
        
        //добавляем системного родителя, чтобы сохранить оригинальные переменные
        foreach ($drawTreeParams['array'] as $i => $value) {
            $drawTreeParams['array'][$i]['parentKeySys'] = $drawTreeParams['array'][$i][$drawTreeParams['parentKey']];
        }
        $drawTreeParams['parentKey'] = 'parentKeySys';
        
        if(!isset($drawTreeParams['rec'])) $drawTreeParams['rec']=0;
        if(!isset($drawTreeParams['separatorString'])) $drawTreeParams['separatorString']='';
        if(!isset($drawTreeParams['globalArray']) || count($drawTreeParams['globalArray'])==0) $drawTreeParams['globalArray'] = $drawTreeParams['array'];
        
        //бежим по массиву
        $result=array();
        foreach ($drawTreeParams['array'] as $i => $value) {
            if($drawTreeParams['array'][$i][$drawTreeParams['parentKey']]==$firstParentKeyValue){
                $value['separator'] = '';
                for ($index = 0; $index < $drawTreeParams['rec']; $index++) {
                    $value['separator'] .= $drawTreeParams['separatorString'];
                }
                
                preg_match_all('~{(.*)}~U', $drawTreeParams['stringParams'], $m);
                $curString = $drawTreeParams['stringParams'];
                
                foreach ($m[1] as $regKey) {
                    $curString = str_replace('{'.$regKey.'}', $value[$regKey], $curString);
                }
                
                $result[] = $curString;
                
                $childParams = $drawTreeParams;
                $childArr=array();

                foreach ($drawTreeParams['globalArray'] as $j => $value) {
                    if($drawTreeParams['globalArray'][$j][$drawTreeParams['parentKey']]!=$firstParentKeyValue && $drawTreeParams['globalArray'][$j][$drawTreeParams['parentKey']]==$drawTreeParams['array'][$i]['id']){
                        $key = count($childArr) +1;
                        $childArr[$key] = $value;
                        
                        $childArr[$key]['id'] = $drawTreeParams['globalArray'][$j]['id'];
                        $childArr[$key][$drawTreeParams['parentKey']]=0;
                        $childArr[$key]['name'] = $drawTreeParams['globalArray'][$j]['name'];
                    }
                }
                
                $childParams['array'] = $childArr;
                $childParams['rec'] = $drawTreeParams['rec']+1;
                $resultChild = $this->drawTree($childParams);
                $result = array_merge($result, $resultChild);
            }
        }
        return $result;
    }
    
}

?>
