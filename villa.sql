-- phpMyAdmin SQL Dump
-- version 3.5.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 13, 2016 at 01:17 PM
-- Server version: 5.5.38-0ubuntu0.12.04.1
-- PHP Version: 5.3.10-1ubuntu3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `staging_villa`
--

-- --------------------------------------------------------

--
-- Table structure for table `db_version`
--

CREATE TABLE IF NOT EXISTS `db_version` (
  `package` varchar(32) DEFAULT NULL,
  `version` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `db_version`
--

INSERT INTO `db_version` (`package`, `version`) VALUES
('dante', 1),
('lib.jquery.ui', 1),
('lib.jdatepicker', 1),
('lib.jgrid', 1),
('module.auth', 5),
('module.lang', 4),
('component.jalerts', 1),
('module.division', 8),
('lib.lightbox', 1),
('lib.jquery.mousewheel', 1),
('lib.jquery.smoothdivscroll', 1),
('lib.jquery.fancybox', 1),
('component.swfupload', 1),
('component.tinymce', 1),
('module.user', 7),
('module.profile', 1),
('villa.module.hotel', 104),
('module.entity', 1),
('module.rating', 1),
('module.comment', 4),
('module.currency', 9),
('module.currencycountries', 5),
('module.menu', 2),
('module.callback', 3),
('module.publications', 16),
('module.labels', 4),
('module.search', 1),
('villa', 24),
('lib.timepicker', 1),
('lib.jquery.fancytransitions', 1),
('module.slideshow', 1),
('lib.jquery.form', 1),
('lib.jquery.validate', 1),
('lib.jquery.arcticmodal', 1),
('module.config', 1),
('lib.geoip', 1),
('module.group', 2),
('module.commentextended', 3),
('module.currencytowns', 4),
('lib.jsphp', 1),
('module.transportcompanies', 4),
('lib.jquery.elrte', 1),
('module.handbooksgenerator', 2),
('villa.module.user', 7),
('lib.jquery.ikselect', 1),
('lib.mustache', 1),
('module.slider', 3),
('module.gallery', 2),
('lib.recaptcha', 1),
('module.uploadify', 1),
('lib.paginator', 1),
('lib.jquery.v1_8_3', 1),
('module.airlines', 7),
('module.capcha', 1),
('villa.module.settings', 2),
('lib.jqueryUpload', 1),
('villa.module.infoblock', 2),
('villa.module.about', 1),
('villa.module.manage.about', 7),
('villa.module.homepage', 10),
('villa.module.countries', 2),
('module.paypal', 2),
('module.liqpay2', 5),
('villa.module.comment', 4),
('module.captcha', 1),
('villa.module.bookinginfo', 2),
('villa.module.mail', 5),
('villa.module.contacts', 3),
('villa.module.division', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tst_airlines`
--

CREATE TABLE IF NOT EXISTS `tst_airlines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `tst_airlines`
--

INSERT INTO `tst_airlines` (`id`, `name`, `logo`, `website`) VALUES
(17, 'Международные Авиалинии Украины', 'c76352f06ab256649e46c90da1c0ec7d.jpg', 'http://www.flyuia.com'),
(18, 'WIZZ AIR', '8e734485ee26f3aaa0751ceae7e6257a.jpg', 'https://wizzair.com'),
(20, 'Lufthansa2', '0d9a07dfa984b034635ed42d78b2eca4.png', 'http://www.lufthansa.com/');

-- --------------------------------------------------------

--
-- Table structure for table `tst_airlines_flights`
--

CREATE TABLE IF NOT EXISTS `tst_airlines_flights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `airline_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `town_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_airlines_flights_id` (`airline_id`),
  KEY `fk_airlines_flights_country_id` (`country_id`),
  KEY `fk_airlines_flights_town_id` (`town_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `tst_airlines_flights`
--

INSERT INTO `tst_airlines_flights` (`id`, `airline_id`, `country_id`, `town_id`) VALUES
(13, 18, 381, 33),
(14, 18, 501, 34),
(16, 20, 350, 35),
(17, 18, 350, 35),
(18, 20, 381, 33);

-- --------------------------------------------------------

--
-- Table structure for table `tst_comment`
--

CREATE TABLE IF NOT EXISTS `tst_comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `entity_type_id` int(11) unsigned NOT NULL,
  `entity_id` int(11) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `lang_id` int(11) unsigned NOT NULL,
  `message` text,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `answer` text,
  PRIMARY KEY (`id`),
  KEY `fk_comment_entity_type_id` (`entity_type_id`),
  KEY `fk_comment_lang_id` (`lang_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Комментарии' AUTO_INCREMENT=47 ;

--
-- Dumping data for table `tst_comment`
--

INSERT INTO `tst_comment` (`id`, `entity_type_id`, `entity_id`, `date`, `name`, `email`, `lang_id`, `message`, `is_read`, `answer`) VALUES
(43, 1, 2, 1425592800, 'nika muller', 'nika.valkyria@gmail.com', 2, 'question in english', 1, 'answer in english'),
(44, 1, 1, 1425592800, 'nika muller', 'nika.valkyria@gmail.com', 2, 'comment in english', 1, 'comment answer in english');

-- --------------------------------------------------------

--
-- Table structure for table `tst_currency`
--

CREATE TABLE IF NOT EXISTS `tst_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `reduction` varchar(10) NOT NULL,
  `factor` float(10,5) NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `is_basic` tinyint(1) NOT NULL DEFAULT '0',
  `exchange_base` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tst_currency`
--

INSERT INTO `tst_currency` (`id`, `title`, `reduction`, `factor`, `is_default`, `is_basic`, `exchange_base`) VALUES
(1, 'Евро', 'EUR', 1.00000, 1, 1, 0),
(2, 'Доллар США', 'USD', 1.10000, 0, 0, 0),
(3, 'Украинская гривна', 'ГРН', 22.00000, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tst_currencycountries`
--

CREATE TABLE IF NOT EXISTS `tst_currencycountries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(5) DEFAULT NULL,
  `title` varchar(50) NOT NULL,
  `lang_id` int(11) unsigned NOT NULL,
  `currency` int(11) NOT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `pay_system` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_currencycountries_currency` (`currency`),
  KEY `fk_currencycountries_lang_id` (`lang_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=528 ;

--
-- Dumping data for table `tst_currencycountries`
--

INSERT INTO `tst_currencycountries` (`id`, `country_code`, `title`, `lang_id`, `currency`, `phone`, `pay_system`) VALUES
(274, 'AP', 'Asia/Pacific Region', 2, 2, '+223', 2),
(276, 'AD', 'Andorra', 2, 2, NULL, 2),
(277, 'AE', 'United Arab Emirates', 2, 2, NULL, 2),
(278, 'AF', 'Afghanistan', 2, 2, NULL, 2),
(279, 'AG', 'Antigua and Barbuda', 2, 2, NULL, 2),
(280, 'AI', 'Anguilla', 2, 2, NULL, 2),
(281, 'AL', 'Albania', 2, 2, NULL, 2),
(282, 'AM', 'Armenia', 2, 2, NULL, 2),
(283, 'CW', 'Curacao', 2, 2, NULL, 2),
(284, 'AO', 'Angola', 2, 2, NULL, 2),
(285, 'AQ', 'Antarctica', 2, 2, NULL, 2),
(286, 'AR', 'Argentina', 2, 2, NULL, 2),
(287, 'AS', 'American Samoa', 2, 2, NULL, 2),
(288, 'AT', 'Austria', 2, 2, NULL, 2),
(289, 'AU', 'Australia', 2, 2, NULL, 2),
(290, 'AW', 'Aruba', 2, 2, NULL, 2),
(291, 'AZ', 'Azerbaijan', 2, 2, NULL, 2),
(292, 'BA', 'Bosnia and Herzegovina', 2, 2, NULL, 2),
(293, 'BB', 'Barbados', 2, 2, NULL, 2),
(294, 'BD', 'Bangladesh', 2, 2, NULL, 2),
(295, 'BE', 'Belgium', 2, 2, NULL, 2),
(296, 'BF', 'Burkina Faso', 2, 2, NULL, 2),
(297, 'BG', 'Bulgaria', 2, 2, NULL, 2),
(298, 'BH', 'Bahrain', 2, 2, NULL, 2),
(299, 'BI', 'Burundi', 2, 2, NULL, 2),
(300, 'BJ', 'Benin', 2, 2, NULL, 2),
(301, 'BM', 'Bermuda', 2, 2, NULL, 2),
(302, 'BN', 'Brunei Darussalam', 2, 2, NULL, 2),
(303, 'BO', 'Bolivia', 2, 2, NULL, 2),
(304, 'BR', 'Brazil', 2, 2, NULL, 2),
(305, 'BS', 'Bahamas', 2, 2, NULL, 2),
(306, 'BT', 'Bhutan', 2, 2, NULL, 2),
(307, 'BV', 'Bouvet Island', 2, 2, NULL, 2),
(308, 'BW', 'Botswana', 2, 2, NULL, 2),
(310, 'BZ', 'Belize', 2, 2, NULL, 2),
(311, 'CA', 'Canada', 2, 2, NULL, 2),
(312, 'CC', 'Cocos (Keeling) Islands', 2, 2, NULL, 2),
(313, 'CD', 'Congo, The Democratic Republic of the', 2, 2, NULL, 2),
(314, 'CF', 'Central African Republic', 2, 2, NULL, 2),
(315, 'CG', 'Congo', 2, 2, NULL, 2),
(316, 'CH', 'Switzerland', 2, 2, NULL, 2),
(317, 'CI', 'Cote D''Ivoire', 2, 2, NULL, 2),
(318, 'CK', 'Cook Islands', 2, 2, NULL, 2),
(319, 'CL', 'Chile', 2, 2, NULL, 2),
(320, 'CM', 'Cameroon', 2, 2, NULL, 2),
(321, 'CN', 'China', 2, 2, NULL, 2),
(322, 'CO', 'Colombia', 2, 2, NULL, 2),
(323, 'CR', 'Costa Rica', 2, 2, NULL, 2),
(324, 'CU', 'Cuba', 2, 2, NULL, 2),
(325, 'CV', 'Cape Verde', 2, 2, NULL, 2),
(326, 'CX', 'Christmas Island', 2, 2, NULL, 2),
(327, 'CY', 'Cyprus', 2, 2, NULL, 2),
(328, 'CZ', 'Czech Republic', 2, 2, NULL, 2),
(329, 'DE', 'Germany', 2, 2, NULL, 2),
(330, 'DJ', 'Djibouti', 2, 2, NULL, 2),
(331, 'DK', 'Denmark', 2, 2, NULL, 2),
(332, 'DM', 'Dominica', 2, 2, NULL, 2),
(333, 'DO', 'Dominican Republic', 2, 2, NULL, 2),
(334, 'DZ', 'Algeria', 2, 2, NULL, 2),
(335, 'EC', 'Ecuador', 2, 2, NULL, 2),
(336, 'EE', 'Estonia', 2, 2, NULL, 2),
(337, 'EG', 'Egypt', 2, 2, NULL, 2),
(338, 'EH', 'Western Sahara', 2, 2, NULL, 2),
(339, 'ER', 'Eritrea', 2, 2, NULL, 2),
(340, 'ES', 'Spain', 2, 2, NULL, 2),
(341, 'ET', 'Ethiopia', 2, 2, NULL, 2),
(342, 'FI', 'Finland', 2, 2, NULL, 2),
(343, 'FJ', 'Fiji', 2, 2, NULL, 2),
(344, 'FK', 'Falkland Islands (Malvinas)', 2, 2, NULL, 2),
(345, 'FM', 'Micronesia, Federated States of', 2, 2, NULL, 2),
(346, 'FO', 'Faroe Islands', 2, 2, NULL, 2),
(347, 'FR', 'France', 2, 2, NULL, 2),
(348, 'SX', 'Sint Maarten (Dutch part)', 2, 2, NULL, 2),
(349, 'GA', 'Gabon', 2, 2, NULL, 2),
(350, 'GB', 'United Kingdom', 2, 2, NULL, 2),
(351, 'GD', 'Grenada', 2, 2, NULL, 2),
(352, 'GE', 'Georgia', 2, 2, NULL, 2),
(353, 'GF', 'French Guiana', 2, 2, NULL, 2),
(354, 'GH', 'Ghana', 2, 2, NULL, 2),
(355, 'GI', 'Gibraltar', 2, 2, NULL, 2),
(356, 'GL', 'Greenland', 2, 2, NULL, 2),
(357, 'GM', 'Gambia', 2, 2, NULL, 2),
(358, 'GN', 'Guinea', 2, 2, NULL, 2),
(359, 'GP', 'Guadeloupe', 2, 2, NULL, 2),
(360, 'GQ', 'Equatorial Guinea', 2, 2, NULL, 2),
(361, 'GR', 'Greece', 2, 2, NULL, 2),
(362, 'GS', 'South Georgia and the South Sandwich Islands', 2, 2, NULL, 2),
(363, 'GT', 'Guatemala', 2, 2, NULL, 2),
(364, 'GU', 'Guam', 2, 2, NULL, 2),
(365, 'GW', 'Guinea-Bissau', 2, 2, NULL, 2),
(366, 'GY', 'Guyana', 2, 2, NULL, 2),
(367, 'HK', 'Hong Kong', 2, 2, NULL, 2),
(368, 'HM', 'Heard Island and McDonald Islands', 2, 2, NULL, 2),
(369, 'HN', 'Honduras', 2, 2, NULL, 2),
(370, 'HR', 'Croatia', 2, 2, NULL, 2),
(371, 'HT', 'Haiti', 2, 2, NULL, 2),
(372, 'HU', 'Hungary', 2, 2, NULL, 2),
(373, 'ID', 'Indonesia', 2, 2, NULL, 2),
(374, 'IE', 'Ireland', 2, 2, NULL, 2),
(375, 'IL', 'Israel', 2, 2, NULL, 2),
(376, 'IN', 'India', 2, 2, NULL, 2),
(377, 'IO', 'British Indian Ocean Territory', 2, 2, NULL, 2),
(378, 'IQ', 'Iraq', 2, 2, NULL, 2),
(379, 'IR', 'Iran, Islamic Republic of', 2, 2, NULL, 2),
(380, 'IS', 'Iceland', 2, 2, NULL, 2),
(381, 'IT', 'Italy', 2, 2, NULL, 2),
(382, 'JM', 'Jamaica', 2, 2, NULL, 2),
(383, 'JO', 'Jordan', 2, 2, NULL, 2),
(384, 'JP', 'Japan', 2, 2, NULL, 2),
(385, 'KE', 'Kenya', 2, 2, NULL, 2),
(386, 'KG', 'Kyrgyzstan', 2, 2, NULL, 2),
(387, 'KH', 'Cambodia', 2, 2, NULL, 2),
(388, 'KI', 'Kiribati', 2, 2, NULL, 2),
(389, 'KM', 'Comoros', 2, 2, NULL, 2),
(390, 'KN', 'Saint Kitts and Nevis', 2, 2, NULL, 2),
(391, 'KP', 'Korea, Democratic People''s Republic of', 2, 2, NULL, 2),
(392, 'KR', 'Korea, Republic of', 2, 2, NULL, 2),
(393, 'KW', 'Kuwait', 2, 2, NULL, 2),
(394, 'KY', 'Cayman Islands', 2, 2, NULL, 2),
(395, 'KZ', 'Kazakhstan', 2, 2, NULL, 2),
(396, 'LA', 'Lao People''s Democratic Republic', 2, 2, NULL, 2),
(397, 'LB', 'Lebanon', 2, 2, NULL, 2),
(398, 'LC', 'Saint Lucia', 2, 2, NULL, 2),
(399, 'LI', 'Liechtenstein', 2, 2, NULL, 2),
(400, 'LK', 'Sri Lanka', 2, 2, NULL, 2),
(401, 'LR', 'Liberia', 2, 2, NULL, 2),
(402, 'LS', 'Lesotho', 2, 2, NULL, 2),
(403, 'LT', 'Lithuania', 2, 2, NULL, 2),
(404, 'LU', 'Luxembourg', 2, 2, NULL, 2),
(405, 'LV', 'Latvia', 2, 2, NULL, 2),
(406, 'LY', 'Libya', 2, 2, NULL, 2),
(407, 'MA', 'Morocco', 2, 2, NULL, 2),
(408, 'MC', 'Monaco', 2, 2, NULL, 2),
(409, 'MD', 'Moldova, Republic of', 2, 2, NULL, 2),
(410, 'MG', 'Madagascar', 2, 2, NULL, 2),
(411, 'MH', 'Marshall Islands', 2, 2, NULL, 2),
(412, 'MK', 'Macedonia', 2, 2, NULL, 2),
(413, 'ML', 'Mali', 2, 2, NULL, 2),
(414, 'MM', 'Myanmar', 2, 2, NULL, 2),
(415, 'MN', 'Mongolia', 2, 2, NULL, 2),
(416, 'MO', 'Macau', 2, 2, NULL, 2),
(417, 'MP', 'Northern Mariana Islands', 2, 2, NULL, 2),
(418, 'MQ', 'Martinique', 2, 2, NULL, 2),
(419, 'MR', 'Mauritania', 2, 2, NULL, 2),
(420, 'MS', 'Montserrat', 2, 2, NULL, 2),
(421, 'MT', 'Malta', 2, 2, NULL, 2),
(422, 'MU', 'Mauritius', 2, 2, NULL, 2),
(423, 'MV', 'Maldives', 2, 2, NULL, 2),
(424, 'MW', 'Malawi', 2, 2, NULL, 2),
(425, 'MX', 'Mexico', 2, 2, NULL, 2),
(426, 'MY', 'Malaysia', 2, 2, NULL, 2),
(427, 'MZ', 'Mozambique', 2, 2, NULL, 2),
(428, 'NA', 'Namibia', 2, 2, NULL, 2),
(429, 'NC', 'New Caledonia', 2, 2, NULL, 2),
(430, 'NE', 'Niger', 2, 2, NULL, 2),
(431, 'NF', 'Norfolk Island', 2, 2, NULL, 2),
(432, 'NG', 'Nigeria', 2, 2, NULL, 2),
(433, 'NI', 'Nicaragua', 2, 2, NULL, 2),
(434, 'NL', 'Netherlands', 2, 2, NULL, 2),
(435, 'NO', 'Norway', 2, 2, NULL, 2),
(436, 'NP', 'Nepal', 2, 2, NULL, 2),
(437, 'NR', 'Nauru', 2, 2, NULL, 2),
(438, 'NU', 'Niue', 2, 2, NULL, 2),
(439, 'NZ', 'New Zealand', 2, 2, NULL, 2),
(440, 'OM', 'Oman', 2, 2, NULL, 2),
(441, 'PA', 'Panama', 2, 2, NULL, 2),
(442, 'PE', 'Peru', 2, 2, NULL, 2),
(443, 'PF', 'French Polynesia', 2, 2, NULL, 2),
(444, 'PG', 'Papua New Guinea', 2, 2, NULL, 2),
(445, 'PH', 'Philippines', 2, 2, NULL, 2),
(446, 'PK', 'Pakistan', 2, 2, NULL, 2),
(447, 'PL', 'Poland', 2, 2, NULL, 2),
(448, 'PM', 'Saint Pierre and Miquelon', 2, 2, NULL, 2),
(449, 'PN', 'Pitcairn Islands', 2, 2, NULL, 2),
(450, 'PR', 'Puerto Rico', 2, 2, NULL, 2),
(451, 'PS', 'Palestinian Territory', 2, 2, NULL, 2),
(452, 'PT', 'Portugal', 2, 2, NULL, 2),
(453, 'PW', 'Palau', 2, 2, NULL, 2),
(454, 'PY', 'Paraguay', 2, 2, NULL, 2),
(455, 'QA', 'Qatar', 2, 2, NULL, 2),
(456, 'RE', 'Reunion', 2, 2, NULL, 2),
(457, 'RO', 'Romania', 2, 2, NULL, 2),
(459, 'RW', 'Rwanda', 2, 2, NULL, 2),
(460, 'SA', 'Saudi Arabia', 2, 2, NULL, 2),
(461, 'SB', 'Solomon Islands', 2, 2, NULL, 2),
(462, 'SC', 'Seychelles', 2, 2, NULL, 2),
(463, 'SD', 'Sudan', 2, 2, NULL, 2),
(464, 'SE', 'Sweden', 2, 2, NULL, 2),
(465, 'SG', 'Singapore', 2, 2, NULL, 2),
(466, 'SH', 'Saint Helena', 2, 2, NULL, 2),
(467, 'SI', 'Slovenia', 2, 2, NULL, 2),
(468, 'SJ', 'Svalbard and Jan Mayen', 2, 2, NULL, 2),
(469, 'SK', 'Slovakia', 2, 2, NULL, 2),
(470, 'SL', 'Sierra Leone', 2, 2, NULL, 2),
(471, 'SM', 'San Marino', 2, 2, NULL, 2),
(472, 'SN', 'Senegal', 2, 2, NULL, 2),
(473, 'SO', 'Somalia', 2, 2, NULL, 2),
(474, 'SR', 'Suriname', 2, 2, NULL, 2),
(475, 'ST', 'Sao Tome and Principe', 2, 2, NULL, 2),
(476, 'SV', 'El Salvador', 2, 2, NULL, 2),
(477, 'SY', 'Syrian Arab Republic', 2, 2, NULL, 2),
(478, 'SZ', 'Swaziland', 2, 2, NULL, 2),
(479, 'TC', 'Turks and Caicos Islands', 2, 2, NULL, 2),
(480, 'TD', 'Chad', 2, 2, NULL, 2),
(481, 'TF', 'French Southern Territories', 2, 2, NULL, 2),
(482, 'TG', 'Togo', 2, 2, NULL, 2),
(483, 'TH', 'Thailand', 2, 2, NULL, 2),
(484, 'TJ', 'Tajikistan', 2, 2, NULL, 2),
(485, 'TK', 'Tokelau', 2, 2, NULL, 2),
(486, 'TM', 'Turkmenistan', 2, 2, NULL, 2),
(487, 'TN', 'Tunisia', 2, 2, NULL, 2),
(488, 'TO', 'Tonga', 2, 2, NULL, 2),
(489, 'TL', 'Timor-Leste', 2, 2, NULL, 2),
(490, 'TR', 'Turkey', 2, 2, NULL, 2),
(491, 'TT', 'Trinidad and Tobago', 2, 2, NULL, 2),
(492, 'TV', 'Tuvalu', 2, 2, NULL, 2),
(493, 'TW', 'Taiwan', 2, 2, NULL, 2),
(494, 'TZ', 'Tanzania, United Republic of', 2, 2, NULL, 2),
(496, 'UG', 'Uganda', 2, 2, NULL, 2),
(497, 'UM', 'United States Minor Outlying Islands', 2, 2, NULL, 2),
(498, 'US', 'United States', 2, 2, NULL, 2),
(499, 'UY', 'Uruguay', 2, 2, NULL, 2),
(500, 'UZ', 'Uzbekistan', 2, 2, NULL, 2),
(501, 'VA', 'Holy See (Vatican City State)', 2, 1, NULL, 2),
(502, 'VC', 'Saint Vincent and the Grenadines', 2, 2, NULL, 2),
(503, 'VE', 'Venezuela', 2, 2, NULL, 2),
(504, 'VG', 'Virgin Islands, British', 2, 2, NULL, 2),
(505, 'VI', 'Virgin Islands, U.S.', 2, 2, NULL, 2),
(506, 'VN', 'Vietnam', 2, 2, NULL, 2),
(507, 'VU', 'Vanuatu', 2, 2, NULL, 2),
(508, 'WF', 'Wallis and Futuna', 2, 2, NULL, 2),
(509, 'WS', 'Samoa', 2, 2, NULL, 2),
(510, 'YE', 'Yemen', 2, 2, NULL, 2),
(511, 'YT', 'Mayotte', 2, 2, NULL, 2),
(512, 'RS', 'Serbia', 2, 2, NULL, 2),
(513, 'ZA', 'South Africa', 2, 2, NULL, 2),
(514, 'ZM', 'Zambia', 2, 2, NULL, 2),
(515, 'ME', 'Montenegro', 2, 2, NULL, 2),
(516, 'ZW', 'Zimbabwe', 2, 2, NULL, 2),
(517, 'A1', 'Anonymous Proxy', 2, 2, NULL, 2),
(518, 'A2', 'Satellite Provider', 2, 2, NULL, 2),
(519, 'O1', 'Other', 2, 2, NULL, 2),
(520, 'AX', 'Aland Islands', 2, 2, NULL, 2),
(521, 'GG', 'Guernsey', 2, 2, NULL, 2),
(522, 'IM', 'Isle of Man', 2, 2, NULL, 2),
(523, 'JE', 'Jersey', 2, 2, NULL, 2),
(524, 'BL', 'Saint Barthelemy', 2, 2, NULL, 2),
(525, 'MF', 'Saint Martin', 2, 2, NULL, 2),
(526, 'BQ', 'Bonaire, Saint Eustatius and Saba', 2, 2, NULL, 2),
(527, 'UA', 'Украина', 1, 3, '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tst_currencycountries_lang`
--

CREATE TABLE IF NOT EXISTS `tst_currencycountries_lang` (
  `country_id` int(11) NOT NULL,
  `set_lang` int(11) unsigned NOT NULL,
  `disp_name` varchar(100) NOT NULL,
  KEY `fk_currencycountries_lang_country_id` (`country_id`),
  KEY `fk_currencycountries_lang_lang_id` (`set_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tst_currencycountries_lang`
--

INSERT INTO `tst_currencycountries_lang` (`country_id`, `set_lang`, `disp_name`) VALUES
(274, 2, 'Asia/Pacific Region'),
(274, 3, 'Asia/Pacific Region'),
(276, 2, 'Andorra'),
(276, 3, 'Andorra'),
(277, 2, 'United Arab Emirates'),
(277, 3, 'United Arab Emirates'),
(278, 2, 'Afghanistan'),
(278, 3, 'Afghanistan'),
(279, 2, 'Antigua and Barbuda'),
(279, 3, 'Antigua and Barbuda'),
(280, 2, 'Anguilla'),
(280, 3, 'Anguilla'),
(281, 2, 'Albania'),
(281, 3, 'Albania'),
(282, 2, 'Armenia'),
(282, 3, 'Armenia'),
(283, 2, 'Curacao'),
(283, 3, 'Curacao'),
(284, 2, 'Angola'),
(284, 3, 'Angola'),
(285, 2, 'Antarctica'),
(285, 3, 'Antarctica'),
(286, 2, 'Argentina'),
(286, 3, 'Argentina'),
(287, 2, 'American Samoa'),
(287, 3, 'American Samoa'),
(288, 2, 'Austria'),
(288, 3, 'Austria'),
(289, 2, 'Australia'),
(289, 3, 'Australia'),
(290, 2, 'Aruba'),
(290, 3, 'Aruba'),
(291, 2, 'Azerbaijan'),
(291, 3, 'Azerbaijan'),
(292, 2, 'Bosnia and Herzegovina'),
(292, 3, 'Bosnia and Herzegovina'),
(293, 2, 'Barbados'),
(293, 3, 'Barbados'),
(294, 2, 'Bangladesh'),
(294, 3, 'Bangladesh'),
(295, 2, 'Belgium'),
(295, 3, 'Belgium'),
(296, 2, 'Burkina Faso'),
(296, 3, 'Burkina Faso'),
(297, 2, 'Bulgaria'),
(297, 3, 'Bulgaria'),
(298, 2, 'Bahrain'),
(298, 3, 'Bahrain'),
(299, 2, 'Burundi'),
(299, 3, 'Burundi'),
(300, 2, 'Benin'),
(300, 3, 'Benin'),
(301, 2, 'Bermuda'),
(301, 3, 'Bermuda'),
(302, 2, 'Brunei Darussalam'),
(302, 3, 'Brunei Darussalam'),
(303, 2, 'Bolivia'),
(303, 3, 'Bolivia'),
(304, 2, 'Brazil'),
(304, 3, 'Brazil'),
(305, 2, 'Bahamas'),
(305, 3, 'Bahamas'),
(306, 2, 'Bhutan'),
(306, 3, 'Bhutan'),
(307, 2, 'Bouvet Island'),
(307, 3, 'Bouvet Island'),
(308, 2, 'Botswana'),
(308, 3, 'Botswana'),
(310, 2, 'Belize'),
(310, 3, 'Belize'),
(311, 2, 'Canada'),
(311, 3, 'Canada'),
(312, 2, 'Cocos (Keeling) Islands'),
(312, 3, 'Cocos (Keeling) Islands'),
(313, 2, 'Congo, The Democratic Republic of the'),
(313, 3, 'Congo, The Democratic Republic of the'),
(314, 2, 'Central African Republic'),
(314, 3, 'Central African Republic'),
(315, 2, 'Congo'),
(315, 3, 'Congo'),
(316, 2, 'Switzerland'),
(316, 3, 'Switzerland'),
(317, 2, 'Cote D''Ivoire'),
(317, 3, 'Cote D''Ivoire'),
(318, 2, 'Cook Islands'),
(318, 3, 'Cook Islands'),
(319, 2, 'Chile'),
(319, 3, 'Chile'),
(320, 2, 'Cameroon'),
(320, 3, 'Cameroon'),
(321, 2, 'China'),
(321, 3, 'China'),
(322, 2, 'Colombia'),
(322, 3, 'Colombia'),
(323, 2, 'Costa Rica'),
(323, 3, 'Costa Rica'),
(324, 2, 'Cuba'),
(324, 3, 'Cuba'),
(325, 2, 'Cape Verde'),
(325, 3, 'Cape Verde'),
(326, 2, 'Christmas Island'),
(326, 3, 'Christmas Island'),
(327, 2, 'Cyprus'),
(327, 3, 'Cyprus'),
(328, 2, 'Czech Republic'),
(328, 3, 'Czech Republic'),
(329, 2, 'Germany'),
(329, 3, 'Germany'),
(330, 2, 'Djibouti'),
(330, 3, 'Djibouti'),
(331, 2, 'Denmark'),
(331, 3, 'Denmark'),
(332, 2, 'Dominica'),
(332, 3, 'Dominica'),
(333, 2, 'Dominican Republic'),
(333, 3, 'Dominican Republic'),
(334, 2, 'Algeria'),
(334, 3, 'Algeria'),
(335, 2, 'Ecuador'),
(335, 3, 'Ecuador'),
(336, 2, 'Estonia'),
(336, 3, 'Estonia'),
(337, 2, 'Egypt'),
(337, 3, 'Egypt'),
(338, 2, 'Western Sahara'),
(338, 3, 'Western Sahara'),
(339, 2, 'Eritrea'),
(339, 3, 'Eritrea'),
(340, 2, 'Spain'),
(340, 3, 'Spain'),
(341, 2, 'Ethiopia'),
(341, 3, 'Ethiopia'),
(342, 2, 'Finland'),
(342, 3, 'Finland'),
(343, 2, 'Fiji'),
(343, 3, 'Fiji'),
(344, 2, 'Falkland Islands (Malvinas)'),
(344, 3, 'Falkland Islands (Malvinas)'),
(345, 2, 'Micronesia, Federated States of'),
(345, 3, 'Micronesia, Federated States of'),
(346, 2, 'Faroe Islands'),
(346, 3, 'Faroe Islands'),
(347, 2, 'France'),
(347, 3, 'France'),
(348, 2, 'Sint Maarten (Dutch part)'),
(348, 3, 'Sint Maarten (Dutch part)'),
(349, 2, 'Gabon'),
(349, 3, 'Gabon'),
(350, 2, 'United Kingdom'),
(350, 3, 'United Kingdom'),
(351, 2, 'Grenada'),
(351, 3, 'Grenada'),
(352, 2, 'Georgia'),
(352, 3, 'Georgia'),
(353, 2, 'French Guiana'),
(353, 3, 'French Guiana'),
(354, 2, 'Ghana'),
(354, 3, 'Ghana'),
(355, 2, 'Gibraltar'),
(355, 3, 'Gibraltar'),
(356, 2, 'Greenland'),
(356, 3, 'Greenland'),
(357, 2, 'Gambia'),
(357, 3, 'Gambia'),
(358, 2, 'Guinea'),
(358, 3, 'Guinea'),
(359, 2, 'Guadeloupe'),
(359, 3, 'Guadeloupe'),
(360, 2, 'Equatorial Guinea'),
(360, 3, 'Equatorial Guinea'),
(361, 2, 'Greece'),
(361, 3, 'Greece'),
(362, 2, 'South Georgia and the South Sandwich Islands'),
(362, 3, 'South Georgia and the South Sandwich Islands'),
(363, 2, 'Guatemala'),
(363, 3, 'Guatemala'),
(364, 2, 'Guam'),
(364, 3, 'Guam'),
(365, 2, 'Guinea-Bissau'),
(365, 3, 'Guinea-Bissau'),
(366, 2, 'Guyana'),
(366, 3, 'Guyana'),
(367, 2, 'Hong Kong'),
(367, 3, 'Hong Kong'),
(368, 2, 'Heard Island and McDonald Islands'),
(368, 3, 'Heard Island and McDonald Islands'),
(369, 2, 'Honduras'),
(369, 3, 'Honduras'),
(370, 2, 'Croatia'),
(370, 3, 'Croatia'),
(371, 2, 'Haiti'),
(371, 3, 'Haiti'),
(372, 2, 'Hungary'),
(372, 3, 'Hungary'),
(373, 2, 'Indonesia'),
(373, 3, 'Indonesia'),
(374, 2, 'Ireland'),
(374, 3, 'Ireland'),
(375, 2, 'Israel'),
(375, 3, 'Israel'),
(376, 2, 'India'),
(376, 3, 'India'),
(377, 2, 'British Indian Ocean Territory'),
(377, 3, 'British Indian Ocean Territory'),
(378, 2, 'Iraq'),
(378, 3, 'Iraq'),
(379, 2, 'Iran, Islamic Republic of'),
(379, 3, 'Iran, Islamic Republic of'),
(380, 2, 'Iceland'),
(380, 3, 'Iceland'),
(381, 2, 'Italy'),
(381, 3, 'Italy'),
(382, 2, 'Jamaica'),
(382, 3, 'Jamaica'),
(383, 2, 'Jordan'),
(383, 3, 'Jordan'),
(384, 2, 'Japan'),
(384, 3, 'Japan'),
(385, 2, 'Kenya'),
(385, 3, 'Kenya'),
(386, 2, 'Kyrgyzstan'),
(386, 3, 'Kyrgyzstan'),
(387, 2, 'Cambodia'),
(387, 3, 'Cambodia'),
(388, 2, 'Kiribati'),
(388, 3, 'Kiribati'),
(389, 2, 'Comoros'),
(389, 3, 'Comoros'),
(390, 2, 'Saint Kitts and Nevis'),
(390, 3, 'Saint Kitts and Nevis'),
(391, 2, 'Korea, Democratic People''s Republic of'),
(391, 3, 'Korea, Democratic People''s Republic of'),
(392, 2, 'Korea, Republic of'),
(392, 3, 'Korea, Republic of'),
(393, 2, 'Kuwait'),
(393, 3, 'Kuwait'),
(394, 2, 'Cayman Islands'),
(394, 3, 'Cayman Islands'),
(395, 2, 'Kazakhstan'),
(395, 3, 'Kazakhstan'),
(396, 2, 'Lao People''s Democratic Republic'),
(396, 3, 'Lao People''s Democratic Republic'),
(397, 2, 'Lebanon'),
(397, 3, 'Lebanon'),
(398, 2, 'Saint Lucia'),
(398, 3, 'Saint Lucia'),
(399, 2, 'Liechtenstein'),
(399, 3, 'Liechtenstein'),
(400, 2, 'Sri Lanka'),
(400, 3, 'Sri Lanka'),
(401, 2, 'Liberia'),
(401, 3, 'Liberia'),
(402, 2, 'Lesotho'),
(402, 3, 'Lesotho'),
(403, 2, 'Lithuania'),
(403, 3, 'Lithuania'),
(404, 2, 'Luxembourg'),
(404, 3, 'Luxembourg'),
(405, 2, 'Latvia'),
(405, 3, 'Latvia'),
(406, 2, 'Libya'),
(406, 3, 'Libya'),
(407, 2, 'Morocco'),
(407, 3, 'Morocco'),
(408, 2, 'Monaco'),
(408, 3, 'Monaco'),
(409, 2, 'Moldova, Republic of'),
(409, 3, 'Moldova, Republic of'),
(410, 2, 'Madagascar'),
(410, 3, 'Madagascar'),
(411, 2, 'Marshall Islands'),
(411, 3, 'Marshall Islands'),
(412, 2, 'Macedonia'),
(412, 3, 'Macedonia'),
(413, 2, 'Mali'),
(413, 3, 'Mali'),
(414, 2, 'Myanmar'),
(414, 3, 'Myanmar'),
(415, 2, 'Mongolia'),
(415, 3, 'Mongolia'),
(416, 2, 'Macau'),
(416, 3, 'Macau'),
(417, 2, 'Northern Mariana Islands'),
(417, 3, 'Northern Mariana Islands'),
(418, 2, 'Martinique'),
(418, 3, 'Martinique'),
(419, 2, 'Mauritania'),
(419, 3, 'Mauritania'),
(420, 2, 'Montserrat'),
(420, 3, 'Montserrat'),
(421, 2, 'Malta'),
(421, 3, 'Malta'),
(422, 2, 'Mauritius'),
(422, 3, 'Mauritius'),
(423, 2, 'Maldives'),
(423, 3, 'Maldives'),
(424, 2, 'Malawi'),
(424, 3, 'Malawi'),
(425, 2, 'Mexico'),
(425, 3, 'Mexico'),
(426, 2, 'Malaysia'),
(426, 3, 'Malaysia'),
(427, 2, 'Mozambique'),
(427, 3, 'Mozambique'),
(428, 2, 'Namibia'),
(428, 3, 'Namibia'),
(429, 2, 'New Caledonia'),
(429, 3, 'New Caledonia'),
(430, 2, 'Niger'),
(430, 3, 'Niger'),
(431, 2, 'Norfolk Island'),
(431, 3, 'Norfolk Island'),
(432, 2, 'Nigeria'),
(432, 3, 'Nigeria'),
(433, 2, 'Nicaragua'),
(433, 3, 'Nicaragua'),
(434, 2, 'Netherlands'),
(434, 3, 'Netherlands'),
(435, 2, 'Norway'),
(435, 3, 'Norway'),
(436, 2, 'Nepal'),
(436, 3, 'Nepal'),
(437, 2, 'Nauru'),
(437, 3, 'Nauru'),
(438, 2, 'Niue'),
(438, 3, 'Niue'),
(439, 2, 'New Zealand'),
(439, 3, 'New Zealand'),
(440, 2, 'Oman'),
(440, 3, 'Oman'),
(441, 2, 'Panama'),
(441, 3, 'Panama'),
(442, 2, 'Peru'),
(442, 3, 'Peru'),
(443, 2, 'French Polynesia'),
(443, 3, 'French Polynesia'),
(444, 2, 'Papua New Guinea'),
(444, 3, 'Papua New Guinea'),
(445, 2, 'Philippines'),
(445, 3, 'Philippines'),
(446, 2, 'Pakistan'),
(446, 3, 'Pakistan'),
(447, 2, 'Poland'),
(447, 3, 'Poland'),
(448, 2, 'Saint Pierre and Miquelon'),
(448, 3, 'Saint Pierre and Miquelon'),
(449, 2, 'Pitcairn Islands'),
(449, 3, 'Pitcairn Islands'),
(450, 2, 'Puerto Rico'),
(450, 3, 'Puerto Rico'),
(451, 2, 'Palestinian Territory'),
(451, 3, 'Palestinian Territory'),
(452, 2, 'Portugal'),
(452, 3, 'Portugal'),
(453, 2, 'Palau'),
(453, 3, 'Palau'),
(454, 2, 'Paraguay'),
(454, 3, 'Paraguay'),
(455, 2, 'Qatar'),
(455, 3, 'Qatar'),
(456, 2, 'Reunion'),
(456, 3, 'Reunion'),
(457, 2, 'Romania'),
(457, 3, 'Romania'),
(459, 2, 'Rwanda'),
(459, 3, 'Rwanda'),
(460, 2, 'Saudi Arabia'),
(460, 3, 'Saudi Arabia'),
(461, 2, 'Solomon Islands'),
(461, 3, 'Solomon Islands'),
(462, 2, 'Seychelles'),
(462, 3, 'Seychelles'),
(463, 2, 'Sudan'),
(463, 3, 'Sudan'),
(464, 2, 'Sweden'),
(464, 3, 'Sweden'),
(465, 2, 'Singapore'),
(465, 3, 'Singapore'),
(466, 2, 'Saint Helena'),
(466, 3, 'Saint Helena'),
(467, 2, 'Slovenia'),
(467, 3, 'Slovenia'),
(468, 2, 'Svalbard and Jan Mayen'),
(468, 3, 'Svalbard and Jan Mayen'),
(469, 2, 'Slovakia'),
(469, 3, 'Slovakia'),
(470, 2, 'Sierra Leone'),
(470, 3, 'Sierra Leone'),
(471, 2, 'San Marino'),
(471, 3, 'San Marino'),
(472, 2, 'Senegal'),
(472, 3, 'Senegal'),
(473, 2, 'Somalia'),
(473, 3, 'Somalia'),
(474, 2, 'Suriname'),
(474, 3, 'Suriname'),
(475, 2, 'Sao Tome and Principe'),
(475, 3, 'Sao Tome and Principe'),
(476, 2, 'El Salvador'),
(476, 3, 'El Salvador'),
(477, 2, 'Syrian Arab Republic'),
(477, 3, 'Syrian Arab Republic'),
(478, 2, 'Swaziland'),
(478, 3, 'Swaziland'),
(479, 2, 'Turks and Caicos Islands'),
(479, 3, 'Turks and Caicos Islands'),
(480, 2, 'Chad'),
(480, 3, 'Chad'),
(481, 2, 'French Southern Territories'),
(481, 3, 'French Southern Territories'),
(482, 2, 'Togo'),
(482, 3, 'Togo'),
(483, 2, 'Thailand'),
(483, 3, 'Thailand'),
(484, 2, 'Tajikistan'),
(484, 3, 'Tajikistan'),
(485, 2, 'Tokelau'),
(485, 3, 'Tokelau'),
(486, 2, 'Turkmenistan'),
(486, 3, 'Turkmenistan'),
(487, 2, 'Tunisia'),
(487, 3, 'Tunisia'),
(488, 2, 'Tonga'),
(488, 3, 'Tonga'),
(489, 2, 'Timor-Leste'),
(489, 3, 'Timor-Leste'),
(490, 2, 'Turkey'),
(490, 3, 'Turkey'),
(491, 2, 'Trinidad and Tobago'),
(491, 3, 'Trinidad and Tobago'),
(492, 2, 'Tuvalu'),
(492, 3, 'Tuvalu'),
(493, 2, 'Taiwan'),
(493, 3, 'Taiwan'),
(494, 2, 'Tanzania, United Republic of'),
(494, 3, 'Tanzania, United Republic of'),
(496, 2, 'Uganda'),
(496, 3, 'Uganda'),
(497, 2, 'United States Minor Outlying Islands'),
(497, 3, 'United States Minor Outlying Islands'),
(498, 2, 'United States'),
(498, 3, 'United States'),
(499, 2, 'Uruguay'),
(499, 3, 'Uruguay'),
(500, 2, 'Uzbekistan'),
(500, 3, 'Uzbekistan'),
(501, 2, 'Holy See (Vatican City State)'),
(501, 3, 'Holy See (Vatican City State)'),
(502, 2, 'Saint Vincent and the Grenadines'),
(502, 3, 'Saint Vincent and the Grenadines'),
(503, 2, 'Venezuela'),
(503, 3, 'Venezuela'),
(504, 2, 'Virgin Islands, British'),
(504, 3, 'Virgin Islands, British'),
(505, 2, 'Virgin Islands, U.S.'),
(505, 3, 'Virgin Islands, U.S.'),
(506, 2, 'Vietnam'),
(506, 3, 'Vietnam'),
(507, 2, 'Vanuatu'),
(507, 3, 'Vanuatu'),
(508, 2, 'Wallis and Futuna'),
(508, 3, 'Wallis and Futuna'),
(509, 2, 'Samoa'),
(509, 3, 'Samoa'),
(510, 2, 'Yemen'),
(510, 3, 'Yemen'),
(511, 2, 'Mayotte'),
(511, 3, 'Mayotte'),
(512, 2, 'Serbia'),
(512, 3, 'Serbia'),
(513, 2, 'South Africa'),
(513, 3, 'South Africa'),
(514, 2, 'Zambia'),
(514, 3, 'Zambia'),
(515, 2, 'Montenegro'),
(515, 3, 'Montenegro'),
(516, 2, 'Zimbabwe'),
(516, 3, 'Zimbabwe'),
(517, 2, 'Anonymous Proxy'),
(517, 3, 'Anonymous Proxy'),
(518, 2, 'Satellite Provider'),
(518, 3, 'Satellite Provider'),
(519, 2, 'Other'),
(519, 3, 'Other'),
(520, 2, 'Aland Islands'),
(520, 3, 'Aland Islands'),
(521, 2, 'Guernsey'),
(521, 3, 'Guernsey'),
(522, 2, 'Isle of Man'),
(522, 3, 'Isle of Man'),
(523, 2, 'Jersey'),
(523, 3, 'Jersey'),
(524, 2, 'Saint Barthelemy'),
(524, 3, 'Saint Barthelemy'),
(525, 2, 'Saint Martin'),
(525, 3, 'Saint Martin'),
(526, 2, 'Bonaire, Saint Eustatius and Saba'),
(526, 3, 'Bonaire, Saint Eustatius and Saba'),
(527, 1, 'Украина');

-- --------------------------------------------------------

--
-- Table structure for table `tst_currencytowns`
--

CREATE TABLE IF NOT EXISTS `tst_currencytowns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_currencytowns_country_id` (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `tst_currencytowns`
--

INSERT INTO `tst_currencytowns` (`id`, `title`, `country_id`) VALUES
(24, 'Рим', 381),
(25, 'Кутаиси', 352),
(26, 'Ларнака', 327),
(30, 'Милан', 381),
(31, 'Венеция', 381),
(33, 'Неаполь', 381),
(34, 'Ватикан', 501),
(35, 'London', 350);

-- --------------------------------------------------------

--
-- Table structure for table `tst_currencytowns_lang`
--

CREATE TABLE IF NOT EXISTS `tst_currencytowns_lang` (
  `town_id` int(11) NOT NULL,
  `lang_id` int(11) unsigned NOT NULL,
  `disp_name` varchar(100) NOT NULL,
  KEY `fk_currencytowns_lang_town_id` (`town_id`),
  KEY `fk_currencytowns_lang_lang_id` (`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tst_currencytowns_lang`
--

INSERT INTO `tst_currencytowns_lang` (`town_id`, `lang_id`, `disp_name`) VALUES
(24, 2, 'Rome'),
(24, 3, 'Rome'),
(25, 2, 'Kutaisi'),
(25, 3, ''),
(26, 2, 'Larnaca'),
(26, 3, ''),
(30, 2, 'Milan'),
(30, 3, ''),
(31, 2, ''),
(31, 3, ''),
(33, 2, 'Naples'),
(33, 3, ''),
(34, 2, ''),
(34, 3, ''),
(35, 2, ''),
(35, 3, 'London'),
(35, 5, ''),
(35, 6, '');

-- --------------------------------------------------------

--
-- Table structure for table `tst_currency_lang`
--

CREATE TABLE IF NOT EXISTS `tst_currency_lang` (
  `id` int(11) NOT NULL,
  `lang_id` int(11) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  KEY `fk_currency_lang_id` (`id`),
  KEY `fk_currency_lang_lang_id` (`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='локализация типов валют';

-- --------------------------------------------------------

--
-- Table structure for table `tst_division`
--

CREATE TABLE IF NOT EXISTS `tst_division` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `link` varchar(128) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `draw_order` int(11) DEFAULT NULL,
  `d_type` varchar(20) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `d_class` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `link` (`link`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Категории' AUTO_INCREMENT=52 ;

--
-- Dumping data for table `tst_division`
--

INSERT INTO `tst_division` (`id`, `name`, `parent_id`, `link`, `image`, `draw_order`, `d_type`, `is_active`, `d_class`) VALUES
(1, 'root', 1, '', NULL, 1, 'url', 1, NULL),
(2, 'adminka', 1, '', NULL, 2, 'url', 1, NULL),
(3, 'client', 1, '', NULL, 3, 'url', 1, NULL),
(6, 'Номера', 2, '/manage/rooms/', NULL, 15, 'url', 1, 3),
(7, 'Типы номеров', 2, '/manage/roomtypes/', NULL, 16, 'url', 1, 3),
(8, 'Бронирование', 2, '/manage/booking/', NULL, 2, 'url', 1, 1),
(9, 'Услуги', 2, '/manage/services/', NULL, 24, 'url', 1, 3),
(10, 'Загрузка отеля', 2, '/manage/loading/', NULL, 1, 'url', 1, 1),
(11, 'Оборудование', 2, '/manage/equipment/', NULL, 18, 'url', 1, 3),
(12, 'Типы предоплат', 2, '/manage/bookingprepayment/', NULL, 11, 'url', 1, 2),
(13, 'Кабинет', 2, '/manage/cashcontrol/', NULL, 10, 'url', 0, NULL),
(14, 'Кабинет Ген Директора', 2, '/manage/cashgendir/', NULL, 11, 'url', 0, NULL),
(15, 'Отзывы', 2, '/manage/comment/', NULL, 4, 'url', 1, 1),
(16, 'Валюты', 2, '/manage/currency/', NULL, 9, 'url', 1, 2),
(17, 'Страны', 2, '/manage/countries/', NULL, 26, 'url', 1, 3),
(19, 'Публикации', 2, '/manage/publications/', NULL, 16, 'url', 0, NULL),
(21, 'Метки', 2, '/manage/labels/', NULL, 29, 'url', 1, 3),
(23, 'Разделы', 2, '/manage/division/', NULL, 20, 'url', 0, NULL),
(24, 'Языки', 2, '/manage/languages/', NULL, 30, 'url', 1, 3),
(28, 'Города', 2, '/manage/currencytowns/', NULL, 27, 'url', 1, 3),
(31, 'Слайдер', 2, '/manage/slider/', NULL, 31, NULL, 1, 3),
(32, 'Цены', 2, '/manage/hotelprices/', NULL, 10, NULL, 1, 2),
(33, 'Трансфер', 2, '/manage/transfer/', NULL, 25, NULL, 1, 3),
(34, 'Авиакомпании', 2, '/manage/airlines/', NULL, 28, NULL, 1, 3),
(36, 'Общие настройки', 2, '/manage/settings/', NULL, 32, NULL, 1, 3),
(37, 'Инфо-блок (Страница "Номера")', 2, '/manage/infoblock/', NULL, 17, NULL, 1, 3),
(38, 'Страница "Об Отеле"', 2, '/manage/about-page/', NULL, 20, NULL, 1, 3),
(39, 'Стартовая страница', 2, '/manage/home-page/', NULL, 19, NULL, 1, 3),
(40, 'История платежей PayPal', 2, '/manage/paypal-logs/', NULL, 13, NULL, 1, 2),
(41, 'История платежей LiqPay', 2, '/manage/liqpay2/', NULL, 14, NULL, 1, 2),
(42, 'Настройки LiqPay', 2, '/manage/liqpay2-settings/', NULL, 12, NULL, 1, 2),
(43, 'Текст на форме бронирования', 2, '/manage/bookinginfo/', NULL, 21, NULL, 1, 3),
(44, 'Шаблоны писем', 2, '/manage/mail-templates/', NULL, 22, NULL, 1, 3),
(45, 'Вопросы', 2, '/manage/questions/', NULL, 5, NULL, 1, 1),
(46, 'Контакты', 2, '/manage/contacts/', NULL, 23, NULL, 1, 3),
(47, 'Трансферы', 2, '/manage/booking-transfer/', NULL, 3, NULL, 1, 1),
(48, 'Клиенты', 2, '/manage/villauser/clients', NULL, 6, NULL, 1, 1),
(49, 'Турагенты', 2, '/manage/villauser/touragents', NULL, 7, NULL, 1, 1),
(50, 'Администраторы', 2, '/manage/villauser/admins', NULL, 8, NULL, 1, 1),
(51, 'Экскурсии', 2, '/manage/excursions/', NULL, 33, NULL, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tst_division_access`
--

CREATE TABLE IF NOT EXISTS `tst_division_access` (
  `division_id` int(11) unsigned NOT NULL COMMENT 'id раздела',
  `group_id` int(11) NOT NULL COMMENT 'id группы доступа',
  `create` tinyint(1) DEFAULT '0' COMMENT 'право добавления',
  `read` tinyint(1) DEFAULT '0' COMMENT 'право чтения',
  `update` tinyint(1) DEFAULT '0' COMMENT 'право редактирования',
  `delete` tinyint(1) DEFAULT '0' COMMENT 'право удаления',
  KEY `division_id` (`division_id`,`group_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tst_division_access`
--

INSERT INTO `tst_division_access` (`division_id`, `group_id`, `create`, `read`, `update`, `delete`) VALUES
(24, 3, 1, 1, 1, 1),
(42, 2, 1, 1, 1, 1),
(42, 3, 1, 1, 1, 1),
(41, 2, 0, 1, 0, 0),
(41, 3, 0, 1, 0, 0),
(45, 2, 1, 1, 1, 1),
(45, 3, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tst_division_groups`
--

CREATE TABLE IF NOT EXISTS `tst_division_groups` (
  `division_id` int(11) unsigned DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  KEY `fk_division_groups_division_id` (`division_id`),
  KEY `fk_division_groups_group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='соответствие категорий и групп пользователей';

--
-- Dumping data for table `tst_division_groups`
--

INSERT INTO `tst_division_groups` (`division_id`, `group_id`) VALUES
(6, 3),
(6, 2),
(6, 1),
(7, 3),
(7, 2),
(7, 1),
(8, 3),
(8, 2),
(8, 1),
(9, 3),
(9, 2),
(9, 1),
(10, 3),
(10, 2),
(10, 1),
(11, 3),
(12, 3),
(13, 3),
(14, 3),
(15, 3),
(15, 2),
(15, 1),
(16, 3),
(17, 3),
(19, 3),
(21, 3),
(21, 2),
(21, 1),
(23, 3),
(24, 3),
(28, 3),
(31, 2),
(23, 2),
(19, 2),
(23, 2),
(32, 3),
(33, 3),
(34, 3),
(36, 3),
(37, 3),
(38, 3),
(39, 3),
(31, 3),
(40, 3),
(42, 2),
(42, 3),
(41, 2),
(41, 3),
(43, 3),
(44, 3),
(45, 2),
(45, 3),
(46, 3),
(47, 3),
(48, 3),
(49, 3),
(50, 3),
(6, 7),
(7, 7),
(8, 7),
(9, 7),
(10, 7),
(11, 7),
(12, 7),
(13, 7),
(14, 7),
(15, 7),
(16, 7),
(17, 7),
(19, 7),
(21, 7),
(23, 7),
(24, 7),
(28, 7),
(31, 7),
(32, 7),
(33, 7),
(34, 7),
(36, 7),
(37, 7),
(38, 7),
(39, 7),
(40, 7),
(41, 7),
(42, 7),
(43, 7),
(44, 7),
(45, 7),
(46, 7),
(47, 7),
(48, 7),
(49, 7),
(50, 7),
(6, 8),
(7, 8),
(8, 8),
(9, 8),
(10, 8),
(11, 8),
(12, 8),
(13, 8),
(14, 8),
(15, 8),
(16, 8),
(17, 8),
(19, 8),
(21, 8),
(23, 8),
(24, 8),
(28, 8),
(31, 8),
(32, 8),
(33, 8),
(34, 8),
(36, 8),
(37, 8),
(38, 8),
(39, 8),
(40, 8),
(41, 8),
(42, 8),
(43, 8),
(44, 8),
(45, 8),
(46, 8),
(47, 8),
(48, 8),
(49, 8),
(50, 8),
(9, 10),
(11, 10),
(28, 10),
(31, 10),
(33, 10),
(51, 3),
(7, 11),
(8, 11),
(6, 12),
(7, 12),
(50, 14);

-- --------------------------------------------------------

--
-- Table structure for table `tst_division_seo`
--

CREATE TABLE IF NOT EXISTS `tst_division_seo` (
  `menu_id` int(11) unsigned NOT NULL,
  `lang_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` text,
  KEY `fk_division_seo_lang_id` (`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='SEO данные категорий';

--
-- Dumping data for table `tst_division_seo`
--

INSERT INTO `tst_division_seo` (`menu_id`, `lang_id`, `name`, `title`, `keywords`, `description`) VALUES
(23, NULL, NULL, NULL, NULL, NULL),
(24, NULL, NULL, NULL, NULL, NULL),
(25, NULL, NULL, NULL, NULL, NULL),
(26, NULL, NULL, NULL, NULL, NULL),
(27, NULL, NULL, NULL, NULL, NULL),
(28, NULL, NULL, NULL, NULL, NULL),
(29, NULL, NULL, NULL, NULL, NULL),
(30, NULL, NULL, NULL, NULL, NULL),
(31, NULL, NULL, NULL, NULL, NULL),
(32, NULL, NULL, NULL, NULL, NULL),
(33, NULL, NULL, NULL, NULL, NULL),
(34, NULL, NULL, NULL, NULL, NULL),
(35, NULL, NULL, NULL, NULL, NULL),
(36, NULL, NULL, NULL, NULL, NULL),
(37, NULL, NULL, NULL, NULL, NULL),
(38, NULL, NULL, NULL, NULL, NULL),
(39, NULL, NULL, NULL, NULL, NULL),
(40, NULL, NULL, NULL, NULL, NULL),
(43, NULL, NULL, NULL, NULL, NULL),
(44, NULL, NULL, NULL, NULL, NULL),
(46, NULL, NULL, NULL, NULL, NULL),
(47, NULL, NULL, NULL, NULL, NULL),
(48, NULL, NULL, NULL, NULL, NULL),
(49, NULL, NULL, NULL, NULL, NULL),
(50, NULL, NULL, NULL, NULL, NULL),
(51, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tst_entity`
--

CREATE TABLE IF NOT EXISTS `tst_entity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `sys_name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Сущности' AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tst_entity`
--

INSERT INTO `tst_entity` (`id`, `name`, `sys_name`) VALUES
(1, 'Общий отзыв', 'feedback'),
(2, 'Публикация', 'publication'),
(3, 'Инфоблок', 'infoblock'),
(4, 'Тип номера', 'room_type'),
(5, 'Услуга', 'service'),
(6, 'Трансфер', 'transfer'),
(7, 'Экскурсия', 'excursion'),
(8, 'Стартовая страница', 'homepage');

-- --------------------------------------------------------

--
-- Table structure for table `tst_gallery`
--

CREATE TABLE IF NOT EXISTS `tst_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` int(11) unsigned DEFAULT NULL,
  `entity_id` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `file_name` varchar(32) DEFAULT NULL,
  `comment` text,
  `adding_date` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gallery_entity` (`entity_type_id`),
  KEY `fk_gallery_uid` (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='галерея изображений' AUTO_INCREMENT=17 ;

--
-- Dumping data for table `tst_gallery`
--

INSERT INTO `tst_gallery` (`id`, `entity_type_id`, `entity_id`, `uid`, `file_name`, `comment`, `adding_date`) VALUES
(1, 2, 2, NULL, 'helvetia-venge.jpg', 'Комментарий', 1362418884),
(3, 2, 2, NULL, 'gzdhzxj.jpg', 'Ещё комментарий', 1362418917),
(4, 2, 2, NULL, '_pinta.jpg', 'И ещё комментарий', 1362418941),
(5, 2, 2, NULL, '1250640304ill.jpg', 'И еще что-то там', 1362419067),
(6, 2, 2, NULL, 'Shine_enl.jpg', '111111111111111', 1362419087),
(7, 2, 2, NULL, '27998202487.jpg', '2222222222', 1362419126),
(8, 2, 2, NULL, 'margo-spalnya.jpg', '33333333333', 1362419172),
(10, 3, 1, NULL, '1783b6609485c9e33fd0c9dbc.jpg', NULL, 1416176543),
(11, 3, 1, NULL, '2f7e0e4acaca2e74359e50f71.jpg', 'АВ', 1416766231),
(15, 3, 1, NULL, '99d63dd487e136f46b24d4f77.jpg', 'цупацк', 1430489367),
(16, 3, 1, NULL, 'a59d407cd5e6fec8187a3cd74.jpg', 'ntcn', 1436361390);

-- --------------------------------------------------------

--
-- Table structure for table `tst_groups`
--

CREATE TABLE IF NOT EXISTS `tst_groups` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(32) DEFAULT NULL,
  `group_caption` varchar(32) DEFAULT NULL,
  `is_system` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Группы пользователей' AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tst_groups`
--

INSERT INTO `tst_groups` (`group_id`, `group_name`, `group_caption`, `is_system`) VALUES
(1, 'moder', 'Модератор', 0),
(2, 'admin', 'Администратор', 1),
(3, 'gendir', 'Ген Директор', 0),
(4, 'user', 'Пользователь', 1),
(5, 'guest', 'Гость', 0),
(6, 'tour_agent', 'Турагент', 0),
(7, 'zamdir', 'zamdir', 0),
(8, 'director', 'director', 0),
(10, 'adm_test', 'adm_test', 0),
(11, 'adm_test2', 'adm_test2', 0),
(12, 'adm_test3', 'adm_test3', 0),
(13, 'adm_abcd', 'adm_abcd', 0),
(14, 'adm_burger', 'adm_burger', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tst_hotel_booking`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creation_time` int(11) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  `modified` int(11) NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `arrival` int(11) NOT NULL,
  `departure` int(11) NOT NULL,
  `days` int(11) NOT NULL DEFAULT '1',
  `adults` int(11) NOT NULL DEFAULT '1',
  `children` int(11) NOT NULL DEFAULT '0',
  `prepayment_type` int(11) NOT NULL,
  `room` int(11) DEFAULT NULL,
  `room_type` int(11) NOT NULL,
  `room_price` float(10,2) NOT NULL DEFAULT '0.00',
  `cost` float(10,2) NOT NULL DEFAULT '0.00',
  `paid_options` float(10,2) NOT NULL DEFAULT '0.00',
  `paid_residence` float(10,2) NOT NULL DEFAULT '0.00',
  `uid` int(11) DEFAULT NULL,
  `dc_options` int(11) NOT NULL DEFAULT '0',
  `dc_residence` int(11) NOT NULL DEFAULT '0',
  `comment` text,
  PRIMARY KEY (`id`),
  KEY `fk_hotel_booking_status` (`status`),
  KEY `fk_hotel_booking_uid` (`uid`),
  KEY `fk_booking_currency_id` (`currency_id`),
  KEY `fk_hotel_booking_modified_by_user` (`modified_by`),
  KEY `fk_hotel_booking_room_type_id` (`room_type`),
  KEY `fk_hotel_booking_room_id` (`room`),
  KEY `fk_hotel_booking_prepayment_type` (`prepayment_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=444 ;

--
-- Dumping data for table `tst_hotel_booking`
--

INSERT INTO `tst_hotel_booking` (`id`, `creation_time`, `version`, `modified`, `modified_by`, `status`, `currency_id`, `arrival`, `departure`, `days`, `adults`, `children`, `prepayment_type`, `room`, `room_type`, `room_price`, `cost`, `paid_options`, `paid_residence`, `uid`, `dc_options`, `dc_residence`, `comment`) VALUES
(179, 1414677474, 1, 1423235975, 18, 2, 1, 1414792800, 1415052000, 3, 1, 1, 1, 304, 4, 123.00, 414.46, 390.00, 0.00, NULL, 0, 0, ''),
(181, 1414693590, 1, 1423235975, 18, 2, 1, 1414620000, 1415311200, 8, 2, 1, 1, 109, 3, 125.00, 1000.00, 0.00, 0.00, NULL, 0, 0, ''),
(183, 1414770804, 1, 1423235975, 18, 2, 1, 1414706400, 1416088800, 16, 1, 0, 1, 107, 3, 125.00, 2502.00, 0.00, 0.00, NULL, 0, 0, ''),
(185, 1415293379, 1, 1423235975, 18, 2, 1, 1415224800, 1415484000, 3, 2, 0, 1, 304, 4, 123.00, 369.00, 0.00, 0.00, NULL, 0, 0, ''),
(186, 1415293524, 1, 1423235975, 18, 4, 1, 1415224800, 1415484000, 3, 2, 1, 1, 204, 5, 235.00, 1521.00, 0.00, 0.00, NULL, 0, 0, ''),
(187, 1415437938, 1, 1423235975, 18, 2, 1, 1415397600, 1415829600, 5, 2, 1, 1, 408, 3, 125.00, 1493.00, 0.00, 0.00, NULL, 0, 0, ''),
(188, 1415950980, 1, 1423235975, 18, 2, 1, 1415916000, 1416002400, 1, 2, 0, 1, 408, 3, 125.00, 149.00, 0.00, 0.00, 19, 0, 0, ''),
(189, 1416079440, 1, 1423235975, 18, 2, 1, 1416002400, 1416088800, 1, 2, 0, 1, 408, 3, 8.12, 2.09, 0.00, 0.00, NULL, 0, 0, ''),
(190, 1416080092, 1, 1423235975, 18, 2, 1, 1416002400, 1416088800, 1, 2, 0, 1, 407, 3, 8.12, 0.53, 0.00, 0.00, NULL, 0, 0, ''),
(191, 1416082801, 1, 1423235975, 18, 2, 1, 1416002400, 1416088800, 1, 1, 0, 1, 406, 3, 8.12, 1.50, 0.00, 0.00, NULL, 0, 0, ''),
(192, 1416238706, 1, 1423235975, 18, 2, 1, 1416175200, 1416520800, 4, 1, 0, 1, 408, 3, 8.12, 15.75, 0.00, 0.00, NULL, 0, 0, ''),
(193, 1416238735, 1, 1423235975, 18, 2, 1, 1416175200, 1416520800, 4, 1, 0, 1, 407, 3, 8.12, 28.03, 0.00, 0.00, NULL, 0, 0, ''),
(199, 1416764750, 1, 1423235975, 18, 2, 1, 1417557600, 1418248800, 8, 2, 0, 2, 408, 3, 115.00, 3666.00, 0.00, 0.00, NULL, 0, 0, ''),
(200, 1416764876, 1, 1423235975, 18, 2, 1, 1417557600, 1418248800, 8, 2, 0, 2, 407, 3, 115.00, 920.00, 0.00, 0.00, NULL, 0, 0, ''),
(201, 1416769775, 1, 1423235975, 18, 2, 1, 1420063200, 1420668000, 7, 2, 1, 1, 408, 3, 125.00, 4530.00, 0.00, 0.00, NULL, 0, 0, ''),
(202, 1416770194, 1, 1423235975, 18, 2, 1, 1416693600, 1417039200, 4, 1, 0, 1, 408, 3, 125.00, 1663.00, 0.00, 0.00, NULL, 0, 0, ''),
(203, 1416770521, 1, 1423235975, 18, 2, 1, 1416693600, 1416780000, 1, 1, 0, 1, 407, 3, 125.00, 481.00, 0.00, 0.00, NULL, 0, 0, ''),
(204, 1416863800, 1, 1423235975, 18, 4, 1, 1416780000, 1416866400, 1, 2, 0, 1, 407, 3, 125.00, 1246.00, 0.00, 0.00, NULL, 0, 0, ''),
(205, 1416997627, 1, 1423235975, 18, 2, 1, 1416952800, 1417039200, 1, 1, 1, 1, 304, 4, 123.00, 123.00, 0.00, 0.00, NULL, 0, 0, ''),
(208, 1417265274, 1, 1423235975, 18, 2, 1, 1417212000, 1417298400, 1, 2, 0, 1, 407, 3, 125.00, 135.00, 0.00, 0.00, NULL, 0, 0, ''),
(210, 1417266593, 1, 1423235975, 18, 2, 1, 1417212000, 1418680800, 17, 1, 0, 1, 406, 3, 125.00, 2864.00, 0.00, 0.00, 19, 0, 0, 'test'),
(212, 1417281825, 1, 1423235975, 18, 2, 1, 1417212000, 1417298400, 1, 2, 0, 1, 405, 3, 125.00, 1392.00, 0.00, 0.00, NULL, 0, 0, ''),
(213, 1417281962, 1, 1423235975, 18, 2, 1, 1418767200, 1419544800, 9, 1, 1, 1, 204, 5, 235.00, 4663.50, 0.00, 0.00, NULL, 0, 0, ''),
(214, 1417437956, 1, 1423235975, 18, 2, 1, 1417384800, 1417644000, 3, 2, 1, 1, 405, 3, 125.00, 1777.50, 0.00, 0.00, NULL, 0, 0, ''),
(216, 1417527871, 1, 1423235975, 18, 2, 1, 1417644000, 1418162400, 6, 1, 0, 1, 405, 3, 100.00, 708.00, 0.00, 0.00, NULL, 0, 0, ''),
(220, 1417768401, 1, 1423235975, 18, 2, 1, 1417730400, 1417816800, 1, 1, 0, 1, 404, 3, 100.00, 100.00, 0.00, 0.00, NULL, 0, 0, ''),
(221, 1417768861, 1, 1423235975, 18, 2, 1, 1417730400, 1417816800, 1, 1, 0, 1, 403, 3, 100.00, 100.00, 0.00, 0.00, NULL, 0, 0, ''),
(224, 1417779089, 1, 1423235975, 18, 2, 1, 1417730400, 1417816800, 1, 1, 0, 1, 402, 3, 100.00, 140.00, 40.00, 100.00, 49, 0, 0, ''),
(227, 1417789340, 1, 1423235975, 18, 6, 1, 1417730400, 1417816800, 1, 2, 1, 1, 316, 3, 100.00, 1032.50, 0.00, 0.00, 20, 0, 0, ''),
(228, 1419534868, 1, 1423235975, 18, 2, 1, 1419458400, 1419544800, 1, 1, 1, 1, 407, 3, 125.00, 125.00, 25.00, 125.00, 49, 0, 0, ''),
(229, 1419601577, 1, 1423235975, 18, 2, 1, 1420063200, 1420668000, 7, 2, 1, 1, 304, 4, 123.00, 0.00, 0.00, 0.00, 18, 0, 0, NULL),
(230, 1419709443, 1, 1423235975, 18, 2, 3, 1419631200, 1419717600, 1, 1, 0, 1, 308, 3, 125.00, 291.00, 0.00, 0.00, 54, 0, 0, ''),
(231, 1419867295, 1, 1423235975, 18, 2, 3, 1419804000, 1419890400, 1, 1, 0, 1, 407, 3, 125.00, 566.00, 0.55, 1.10, 55, 0, 0, ''),
(236, 1419939717, 1, 1423235975, 18, 2, 3, 1420063200, 1420581600, 6, 2, 0, 2, 204, 5, 263.00, 1578.00, 0.00, 0.00, 18, 0, 0, NULL),
(237, 1420374228, 1, 1423235975, 18, 2, 3, 1420322400, 1420408800, 1, 1, 0, 1, 407, 3, 901.00, 2712.00, 0.00, 0.00, 58, 0, 0, ''),
(238, 1421148673, 1, 1423235975, 18, 2, 3, 1422914400, 1423000800, 1, 1, 1, 2, 407, 3, 115.00, 115.00, 0.00, 0.00, 18, 0, 0, NULL),
(239, 1421153108, 1, 1423235975, 18, 7, 3, 1421100000, 1421186400, 1, 2, 0, 1, 407, 3, 901.00, 3986.00, 0.00, 0.00, 31, 0, 0, ''),
(240, 1422031180, 1, 1423235975, 18, 6, 2, 1421964000, 1422396000, 5, 1, 0, 2, 407, 3, 40.00, 235.00, 35.00, 200.00, 31, 0, 0, ''),
(241, 1422301123, 1, 1423235975, 18, 2, 3, 1422223200, 1422309600, 1, 1, 0, 1, 406, 3, 125.00, 292.00, 0.53, 0.00, 49, 99, 50, ''),
(242, 1422286195, 1, 1423235975, 18, 2, 3, 1422223200, 1422309600, 1, 1, 0, 2, 405, 3, 40.00, 40.00, 0.00, 0.00, 59, 0, 0, ''),
(243, 1422298937, 1, 1423235975, 18, 2, 3, 1422223200, 1422309600, 1, 1, 0, 1, 404, 3, 125.00, 125.00, 0.00, 0.00, 59, 0, 0, ''),
(244, 1422310461, 2, 1423236663, 18, 2, 3, 1422309600, 1422396000, 1, 1, 0, 2, 314, 3, 40.00, 136.00, -31.34, 40.00, 60, 0, 0, ''),
(245, 1423306921, 3, 1423307193, 18, 2, 3, 1422309600, 1422396000, 1, 1, 0, 1, 405, 3, 125.00, 132.00, 0.07, 62.50, 49, 99, 50, ''),
(246, 1422357133, 1, 1423235975, 18, 7, 2, 1422309600, 1422396000, 1, 1, 0, 1, 404, 3, 125.00, 213.00, 0.88, 62.50, 49, 99, 50, ''),
(247, 1422724417, 2, 1425138374, 18, 4, 1, 1422655200, 1422741600, 1, 1, 0, 2, 407, 3, 1.00, 1.00, 0.00, 0.00, 20, 20, 10, ''),
(248, 1423242039, 1, 1423242039, 61, 6, 3, 1423173600, 1423260000, 1, 1, 0, 1, 407, 3, 91.00, 106.30, 0.00, 0.00, 61, 0, 0, ''),
(249, 1423423963, 2, 1423424021, 18, 4, 3, 1423432800, 1423692000, 4, 2, 0, 1, 403, 3, 91.00, 274.70, 0.00, 0.00, 31, 0, 0, ''),
(250, 1423585735, 1, 1423585735, 49, 6, 3, 1423519200, 1423605600, 1, 1, 0, 1, 407, 3, 91.00, 91.00, 0.00, 0.00, 49, 99, 50, ''),
(251, 1423593203, 1, 1423593203, 19, 1, 3, 1423519200, 1423605600, 1, 2, 0, 1, 406, 3, 91.00, 91.00, 0.00, 0.00, 19, 0, 0, ''),
(252, 1423593252, 1, 1423593252, 62, 1, 3, 1423519200, 1423605600, 1, 2, 0, 3, 405, 3, 110.00, 110.00, 0.00, 0.00, 62, 0, 0, ''),
(253, 1423596179, 1, 1423596179, 20, 1, 3, 1423519200, 1423605600, 1, 1, 0, 1, 404, 3, 91.00, 91.00, 0.00, 0.00, 20, 20, 10, ''),
(254, 1423762291, 4, 1423762291, 49, 7, 3, 1423692000, 1423778400, 1, 1, 0, 1, 407, 3, 91.00, 121.10, 0.00, 0.00, 49, 99, 50, ''),
(255, 1423870441, 1, 1423870441, 20, 1, 3, 1423864800, 1423951200, 1, 2, 0, 1, 216, 3, 91.00, 91.00, 0.00, 0.00, 20, 20, 10, ''),
(256, 1423870491, 1, 1423870491, 20, 1, 3, 1423864800, 1423951200, 1, 2, 0, 1, 201, 3, 91.00, 91.00, 0.00, 0.00, 20, 20, 10, ''),
(257, 1423870540, 2, 1423870618, 18, 1, 3, 1423864800, 1423951200, 1, 2, 0, 1, 206, 3, 91.00, 91.00, 0.00, 0.00, 20, 20, 10, ''),
(258, 1423870637, 1, 1423870637, 20, 1, 3, 1423864800, 1423951200, 1, 2, 0, 1, 203, 3, 91.00, 91.00, 0.00, 0.00, 20, 20, 10, ''),
(259, 1423870712, 1, 1423870712, 20, 1, 3, 1423864800, 1423951200, 1, 2, 0, 1, 104, 5, 93.00, 93.00, 0.00, 0.00, 20, 20, 10, ''),
(260, 1423870733, 1, 1423870733, 20, 7, 3, 1423864800, 1423951200, 1, 2, 0, 1, 105, 5, 93.00, 93.00, 0.00, 0.00, 20, 20, 10, ''),
(261, 1424596381, 1, 1424596381, 49, 1, 3, 1424556000, 1424642400, 1, 1, 0, 1, 216, 3, 91.00, 91.00, 0.00, 0.00, 49, 99, 50, ''),
(262, 1424735319, 1, 1424735319, 59, 1, 3, 1424642400, 1424728800, 1, 1, 0, 1, 216, 3, 91.00, 91.00, 0.00, 0.00, 59, 0, 0, ''),
(265, 1424879854, 1, 1424879854, 49, 4, 3, 1424815200, 1424901600, 1, 1, 0, 1, 201, 3, 91.00, 91.00, 0.00, 0.00, 49, 99, 50, ''),
(272, 1425137909, 1, 1425137909, 20, 1, 3, 1425074400, 1425160800, 1, 2, 0, 1, 303, 4, 92.00, 92.00, 0.00, 0.00, 20, 20, 10, ''),
(273, 1425143108, 2, 1425143108, 20, 1, 3, 1425074400, 1425160800, 1, 2, 0, 2, 314, 3, 0.10, 0.50, 0.00, 0.00, 20, 20, 10, ''),
(274, 1425141967, 1, 1425141967, 66, 1, 3, 1425074400, 1425160800, 1, 1, 0, 1, 301, 3, 91.00, 91.00, 0.00, 0.00, 66, 0, 0, ''),
(277, 1425731645, 1, 1425731645, 61, 1, 3, 1425679200, 1425765600, 1, 2, 0, 1, 216, 3, 125.00, 195.00, 0.00, 0.00, 61, 0, 0, ''),
(278, 1425900238, 1, 1425900238, 49, 1, 3, 1425852000, 1425938400, 1, 2, 0, 1, 216, 3, 125.00, 125.00, 0.00, 0.00, 49, 99, 50, ''),
(279, 1426249055, 1, 1426249055, 31, 1, 3, 1426197600, 1426284000, 1, 1, 0, 2, 216, 3, 0.10, 0.10, 0.00, 0.10, 31, 0, 0, ''),
(280, 1426258899, 1, 1426258899, 20, 1, 1, 1426197600, 1426284000, 1, 2, 0, 2, 201, 3, 0.10, 0.10, 0.00, 0.09, 20, 20, 10, ''),
(281, 1426528417, 2, 1426532098, 18, 1, 3, 1426456800, 1426543200, 1, 1, 0, 2, 216, 3, 0.10, 0.10, 0.00, 0.10, 67, 0, 0, ''),
(284, 1427119473, 2, 1427119473, 49, 1, 3, 1427061600, 1427148000, 1, 2, 0, 2, 316, 3, 115.00, 115.00, 0.00, 0.00, 49, 99, 50, ''),
(285, 1427121053, 1, 1427121053, 49, 1, 3, 1427061600, 1427148000, 1, 2, 0, 1, 216, 3, 125.00, 125.00, 0.00, 0.00, 49, 99, 99, ''),
(286, 1427121275, 3, 1427121275, 49, 1, 3, 1427061600, 1427148000, 1, 2, 0, 2, 201, 3, 115.00, 115.00, 0.00, 1.15, 49, 99, 99, ''),
(288, 1427365428, 2, 1427365428, 49, 1, 3, 1427320800, 1427407200, 1, 2, 0, 2, 316, 3, 115.00, 115.00, 0.00, 1.15, 49, 99, 99, ''),
(289, 1428131398, 1, 1428131398, 71, 1, 3, 1427320800, 1427407200, 1, 2, 0, 2, 216, 3, 115.00, 115.00, 0.00, 0.00, 71, 0, 0, ''),
(290, 1428134813, 1, 1428134813, 71, 1, 3, 1427320800, 1427407200, 1, 2, 0, 1, 201, 3, 125.00, 568.00, 0.00, 0.00, 71, 0, 0, ''),
(292, 1428138376, 1, 1428138376, 71, 1, 3, 1428094800, 1428181200, 1, 2, 0, 1, 316, 3, 125.00, 125.00, 0.00, 0.00, 71, 0, 0, ''),
(293, 1428138450, 1, 1428138450, 71, 1, 3, 1428094800, 1428181200, 1, 2, 0, 1, 216, 3, 125.00, 167.00, 0.00, 0.00, 71, 0, 0, ''),
(294, 1428138902, 1, 1428138902, 71, 1, 3, 1428094800, 1428181200, 1, 2, 0, 1, 201, 3, 125.00, 125.00, 0.00, 0.00, 71, 0, 0, ''),
(296, 1428669754, 1, 1428669754, 73, 1, 3, 1428613200, 1428699600, 1, 2, 1, 3, 316, 3, 0.10, 0.10, 0.00, 0.10, 73, 0, 0, ''),
(298, 1428672195, 2, 1428672195, 73, 1, 1, 1428613200, 1428699600, 1, 2, 0, 3, 201, 3, 0.10, 2.10, 0.00, 0.10, 73, 0, 0, ''),
(299, 1428674937, 5, 1428674992, 18, 2, 3, 1428613200, 1428699600, 1, 2, 0, 3, 203, 3, 0.10, 2.10, 0.00, 0.09, 20, 20, 10, ''),
(302, 1428676478, 1, 1428676478, 20, 1, 1, 1428613200, 1428699600, 1, 2, 0, 1, 211, 3, 125.00, 157.00, 0.00, 0.00, 20, 20, 10, ''),
(303, 1428677896, 1, 1428677896, 74, 1, 3, 1428613200, 1428699600, 1, 1, 0, 1, 314, 3, 125.00, 125.00, 0.00, 0.00, 74, 0, 0, ''),
(306, 1428681315, 1, 1428681315, 20, 2, 3, 1428613200, 1428699600, 1, 2, 0, 3, 206, 3, 0.10, 423.10, 0.00, 0.09, 20, 20, 10, 'Коментарий'),
(307, 1428767844, 1, 1428767844, 49, 1, 3, 1428094800, 1428181200, 1, 2, 0, 1, 203, 3, 125.00, 125.00, 0.00, 0.00, 49, 99, 99, ''),
(309, 1429107977, 1, 1429107977, 20, 1, 3, 1429045200, 1429131600, 1, 2, 0, 1, 316, 3, 125.00, 468.00, 0.00, 0.00, 20, 0, 0, 'Пожелания'),
(311, 1429184460, 1, 1429184460, 78, 1, 3, 1429131600, 1429218000, 1, 1, 0, 2, 316, 3, 115.00, 115.00, 0.00, 0.00, 78, 0, 0, ''),
(318, 1430490250, 2, 1430490250, 59, 1, 2, 1430427600, 1430514000, 1, 1, 0, 2, 316, 3, 115.00, 115.00, 0.00, 115.00, 59, 0, 0, ''),
(319, 1430490425, 1, 1430490425, NULL, 1, 2, 1430427600, 1430514000, 1, 1, 0, 3, 216, 3, 0.10, 0.10, 0.00, 0.00, NULL, 0, 0, ''),
(320, 1430490447, 2, 1430490447, 59, 1, 2, 1430427600, 1430514000, 1, 1, 0, 3, 201, 3, 0.10, 0.10, 0.00, 0.00, 59, 0, 0, ''),
(321, 1430493934, 1, 1430493934, 59, 7, 3, 1430427600, 1430514000, 1, 1, 0, 1, 203, 3, 125.00, 125.00, 0.00, 0.00, 59, 0, 0, ''),
(324, 1430494687, 1, 1430494687, 49, 1, 3, 1430427600, 1430514000, 1, 1, 0, 1, 206, 3, 125.00, 324.00, 0.00, 0.00, 49, 99, 99, ''),
(325, 1430495443, 6, 1430496705, 18, 1, 3, 1430427600, 1430514000, 1, 2, 0, 1, 208, 3, 125.00, 180.00, 0.00, 0.00, 49, 99, 99, ''),
(328, 1431024421, 1, 1431024421, 49, 1, 3, 1430946000, 1431032400, 1, 2, 0, 3, 316, 3, 0.10, 0.10, 0.00, 0.00, 49, 99, 99, ''),
(329, 1431024866, 1, 1431024866, 49, 1, 3, 1430946000, 1431032400, 1, 2, 0, 3, 216, 3, 0.10, 0.10, 0.00, 0.00, 49, 99, 99, ''),
(331, 1431810545, 3, 1431810545, 20, 1, 3, 1431723600, 1431810000, 1, 2, 0, 1, 316, 3, 125.00, 173.00, 0.00, 0.00, 20, 20, 10, ''),
(332, 1431807172, 1, 1431807172, 20, 6, 3, 1431723600, 1431810000, 1, 2, 0, 1, 216, 3, 125.00, 125.00, 0.00, 0.00, 20, 20, 10, ''),
(333, 1431811069, 1, 1431811069, 83, 1, 3, 1431810000, 1431896400, 1, 2, 0, 3, 316, 3, 0.10, 2.10, 0.00, 0.10, 83, 0, 0, ''),
(334, 1431814555, 3, 1431814555, 20, 1, 3, 1431810000, 1431896400, 1, 2, 0, 3, 216, 3, 0.10, 24.10, 0.00, 0.09, 20, 20, 10, ''),
(335, 1437732842, 4, 1437732842, 31, 1, 3, 1437080400, 1437512400, 5, 2, 0, 1, 316, 3, 125.00, 1215.00, 0.00, 0.00, 31, 0, 0, ''),
(337, 1433939954, 1, 1433939954, 85, 1, 3, 1433883600, 1433970000, 1, 2, 0, 1, 316, 3, 125.00, 269.00, 0.00, 0.00, 85, 0, 0, ''),
(338, 1433940196, 1, 1433940196, 85, 1, 3, 1433883600, 1433970000, 1, 2, 0, 3, 216, 3, 0.10, 0.10, 0.00, 0.10, 85, 0, 0, ''),
(339, 1434177020, 1, 1434177020, 49, 1, 3, 1434142800, 1434229200, 1, 2, 0, 3, 316, 3, 0.10, 2.10, 0.02, 0.00, 49, 99, 99, ''),
(340, 1434182381, 1, 1434182381, 49, 1, 3, 1434142800, 1434229200, 1, 2, 0, 1, 216, 3, 125.00, 135.00, 0.00, 0.00, 49, 99, 99, ''),
(341, 1434182434, 1, 1434182434, 49, 1, 3, 1434142800, 1434229200, 1, 2, 0, 3, 201, 3, 0.10, 10.10, 0.00, 0.00, 49, 99, 99, ''),
(342, 1434183064, 2, 1434183064, 86, 1, 3, 1434142800, 1434229200, 1, 2, 0, 3, 203, 3, 0.10, 0.10, 0.00, 0.10, 86, 0, 0, ''),
(349, 1434568279, 1, 1434568279, 49, 1, 3, 1434488400, 1434574800, 1, 2, 0, 3, 316, 3, 0.10, 0.10, 0.00, 0.00, 49, 99, 99, ''),
(350, 1434568964, 2, 1434568964, 49, 1, 3, 1434488400, 1434574800, 1, 2, 0, 3, 216, 3, 0.10, 0.10, 0.00, 0.00, 49, 99, 99, ''),
(351, 1436295571, 2, 1436295626, 18, 1, 3, 1437598800, 1437771600, 2, 2, 0, 1, 316, 3, 125.00, 250.00, 0.00, 0.00, 88, 0, 0, ''),
(352, 1436362244, 1, 1436362244, 20, 1, 3, 1436302800, 1436389200, 1, 2, 0, 1, 316, 3, 125.00, 135.00, 0.00, 0.00, 20, 20, 10, ''),
(353, 1436362251, 1, 1436362251, 20, 1, 3, 1436302800, 1436389200, 1, 2, 0, 1, 216, 3, 125.00, 135.00, 0.00, 0.00, 20, 20, 10, ''),
(354, 1436362295, 1, 1436362295, 20, 1, 3, 1436302800, 1436389200, 1, 2, 0, 1, 201, 3, 125.00, 135.00, 0.00, 0.00, 20, 20, 10, ''),
(355, 1436362296, 1, 1436362296, 89, 1, 3, 1436302800, 1436389200, 1, 2, 0, 1, 203, 3, 125.00, 135.00, 0.00, 0.00, 89, 0, 0, ''),
(356, 1436362313, 1, 1436362313, 20, 1, 3, 1436302800, 1436389200, 1, 2, 0, 1, 206, 3, 125.00, 125.00, 0.00, 0.00, 20, 20, 10, ''),
(357, 1436362926, 1, 1436362926, 83, 1, 3, 1436302800, 1436389200, 1, 2, 0, 1, 208, 3, 125.00, 523.00, 0.00, 0.00, 83, 0, 0, ''),
(358, 1436364455, 1, 1436364455, 83, 1, 3, 1436734800, 1436907600, 2, 2, 0, 1, 303, 4, 123.00, 246.00, 0.00, 0.00, 83, 0, 0, ''),
(359, 1436364988, 1, 1436364988, 90, 1, 3, 1436302800, 1436389200, 1, 2, 0, 1, 211, 3, 125.00, 125.00, 0.00, 0.00, 90, 0, 0, ''),
(360, 1436365097, 1, 1436365097, 90, 1, 3, 1436302800, 1436389200, 1, 2, 0, 1, 314, 3, 125.00, 125.00, 0.00, 0.00, 90, 0, 0, ''),
(361, 1436365289, 1, 1436365289, 91, 1, 3, 1436302800, 1436389200, 1, 2, 0, 1, 301, 3, 125.00, 249.00, 0.00, 0.00, 91, 0, 0, ''),
(362, 1436365909, 1, 1436365909, 91, 1, 3, 1436302800, 1436389200, 1, 2, 0, 3, 306, 3, 0.10, 0.10, 0.00, 0.00, 91, 0, 0, ''),
(363, 1436366050, 1, 1436366050, 91, 1, 2, 1436302800, 1436389200, 1, 2, 0, 3, 309, 3, 0.10, 0.10, 0.00, 0.00, 91, 0, 0, ''),
(364, 1436366371, 1, 1436366371, 20, 1, 2, 1436302800, 1436389200, 1, 2, 0, 1, 116, 3, 125.00, 125.00, 0.00, 0.00, 20, 20, 10, ''),
(365, 1436366904, 1, 1436366904, 92, 1, 3, 1436302800, 1436389200, 1, 2, 0, 1, 101, 3, 125.00, 125.00, 0.00, 0.00, 92, 0, 0, ''),
(366, 1436369444, 3, 1436369444, 20, 1, 2, 1436302800, 1436389200, 1, 2, 0, 3, 106, 3, 0.10, 154.10, 0.00, 0.09, 20, 20, 10, ''),
(367, 1436369656, 1, 1436369656, 20, 1, 2, 1437771600, 1438290000, 6, 2, 0, 1, 201, 3, 125.00, 752.00, 0.00, 0.00, 20, 20, 10, ''),
(368, 1436371177, 1, 1436371177, 20, 1, 2, 1439326800, 1439931600, 7, 2, 0, 1, 316, 3, 125.00, 900.00, 0.00, 0.00, 20, 20, 10, ''),
(369, 1437054552, 2, 1447182557, 18, 1, 3, 1447106400, 1447452000, 4, 2, 0, 1, NULL, 3, 125.00, 1116.00, 0.00, 0.00, 94, 0, 0, ''),
(370, 1437130055, 3, 1447182604, 18, 1, 3, 1447192800, 1447279200, 1, 2, 0, 1, 103, 3, 125.00, 125.00, 0.00, 0.00, 95, 0, 0, ''),
(371, 1437130146, 2, 1439394593, 18, 6, 3, 1437080400, 1437166800, 1, 2, 0, 1, 201, 3, 125.00, 125.00, 0.00, 0.00, 94, 0, 0, ''),
(372, 1437231700, 8, 1439393235, 20, 1, 3, 1439413200, 1439672400, 3, 2, 0, 1, 216, 3, 125.00, 1225.00, 0.00, 0.00, 20, 20, 10, ''),
(373, 1437233909, 3, 1437233909, 96, 1, 3, 1437166800, 1437512400, 4, 2, 1, 3, 203, 3, 0.10, 0.40, 0.00, 0.40, 96, 0, 0, ''),
(374, 1437296247, 2, 1437296247, 96, 1, 3, 1440968400, 1441227600, 3, 2, 1, 3, 316, 3, 0.10, 1312.80, 0.00, 0.30, 96, 0, 0, ''),
(375, 1437499937, 1, 1437499937, 97, 7, 3, 1437426000, 1437512400, 1, 2, 0, 3, 206, 3, 0.10, 48.10, 0.00, 0.00, 97, 0, 0, ''),
(376, 1437499970, 2, 1437732733, 18, 1, 3, 1437426000, 1437512400, 1, 2, 0, 2, 208, 3, 115.00, 185.00, 0.00, 0.00, 97, 0, 0, ''),
(377, 1440624634, 1, 1440624634, 20, 1, 3, 1440622800, 1441227600, 7, 2, 1, 3, 216, 3, 0.10, 1459.70, 0.00, 0.63, 20, 20, 10, ''),
(378, 1441709872, 1, 1441709872, 100, 1, 3, 1441659600, 1442005200, 4, 1, 0, 1, 316, 3, 125.00, 500.00, 0.00, 0.00, 100, 0, 0, ''),
(379, 1441709901, 4, 1447182631, 18, 1, 3, 1447279200, 1447452000, 2, 2, 0, 1, 103, 3, 125.00, 250.00, 0.00, 0.00, 31, 0, 0, ''),
(380, 1441710339, 1, 1441710339, 61, 1, 3, 1441659600, 1441746000, 1, 2, 0, 1, 201, 3, 125.00, 125.00, 0.00, 0.00, 61, 0, 0, ''),
(381, 1441710917, 1, 1441710917, 61, 1, 3, 1441659600, 1441746000, 1, 2, 0, 1, 203, 3, 125.00, 153.00, 0.00, 0.00, 61, 0, 0, ''),
(382, 1441903547, 1, 1441903547, 20, 1, 3, 1441832400, 1442091600, 3, 2, 1, 1, 216, 3, 125.00, 1785.00, 0.00, 0.00, 20, 20, 10, ''),
(383, 1443627511, 1, 1443627511, 20, 1, 1, 1443906000, 1444078800, 2, 2, 1, 1, 316, 3, 125.00, 280.00, 0.00, 0.00, 20, 20, 10, ''),
(384, 1443627577, 1, 1443627577, 20, 1, 1, 1443906000, 1444078800, 2, 1, 0, 1, 216, 3, 125.00, 262.00, 0.00, 0.00, 20, 20, 10, ''),
(387, 1445368828, 1, 1445368828, 112, 1, 3, 1445288400, 1445374800, 1, 2, 0, 1, 316, 3, 125.00, 177.00, 0.00, 0.00, 112, 0, 0, ''),
(388, 1447179857, 1, 1447179857, 113, 1, 1, 1447624800, 1448143200, 6, 2, 1, 1, 316, 3, 125.00, 2117.50, 0.00, 0.00, 113, 0, 0, ''),
(389, 1447179947, 3, 1447182541, 18, 1, 3, 1447192800, 1447279200, 1, 2, 0, 1, 104, 5, 135.00, 135.00, 0.00, 0.00, 114, 0, 0, ''),
(390, 1447180274, 1, 1447180274, 113, 1, 1, 1447624800, 1448143200, 6, 2, 1, 1, 216, 3, 125.00, 1493.50, 0.00, 0.00, 113, 0, 0, ''),
(391, 1449694969, 1, 1449694969, 62, 1, 2, 1449612000, 1449698400, 1, 2, 0, 1, 316, 3, 125.00, 125.00, 0.00, 0.00, 62, 0, 0, ''),
(392, 1449695057, 1, 1449695057, 115, 1, 3, 1449612000, 1449698400, 1, 2, 0, 1, 303, 4, 123.00, 379.00, 0.00, 0.00, 115, 0, 0, ''),
(393, 1449695106, 1, 1449695106, 115, 1, 3, 1449612000, 1450476000, 10, 2, 0, 2, 216, 3, 115.00, 4402.00, 0.00, 0.00, 115, 0, 0, ''),
(394, 1449832728, 1, 1449832728, 49, 1, 3, 1449784800, 1450130400, 4, 2, 1, 3, 316, 3, 0.10, 0.40, 0.00, 0.00, 49, 99, 99, ''),
(395, 1450289130, 1, 1450289130, 49, 1, 3, 1450216800, 1450303200, 1, 2, 0, 1, 316, 3, 125.00, 125.00, 0.00, 0.00, 49, 99, 99, ''),
(396, 1450289180, 1, 1450289180, 49, 1, 3, 1450216800, 1450303200, 1, 2, 0, 1, 201, 3, 125.00, 125.00, 0.00, 0.00, 49, 0, 0, ''),
(397, 1450289390, 1, 1450289390, 116, 1, 3, 1450216800, 1450303200, 1, 2, 0, 1, 203, 3, 125.00, 125.00, 0.00, 0.00, 116, 0, 0, ''),
(398, 1452625758, 1, 1452625758, 20, 1, 3, 1452549600, 1452981600, 5, 2, 0, 1, 104, 5, 135.00, 2509.00, 0.00, 0.00, 20, 20, 10, ''),
(399, 1453283416, 1, 1453283416, 49, 1, 3, 1453240800, 1453327200, 1, 2, 0, 1, 316, 3, 125.00, 546.00, 0.00, 0.00, 49, 99, 99, ''),
(400, 1453283673, 1, 1453283673, 49, 1, 3, 1453413600, 1453500000, 1, 2, 0, 1, 316, 3, 125.00, 125.00, 0.00, 0.00, 49, 99, 99, ''),
(401, 1454435309, 1, 1454435309, 117, 1, 3, 1454364000, 1454450400, 1, 2, 0, 1, 316, 3, 125.00, 223.00, 0.00, 0.00, 117, 0, 0, ''),
(402, 1454435637, 1, 1454435637, 117, 1, 3, 1454364000, 1454450400, 1, 2, 0, 1, 216, 3, 125.00, 125.00, 0.00, 0.00, 117, 0, 0, ''),
(403, 1455120545, 1, 1455120545, 119, 1, 3, 1455055200, 1455141600, 1, 2, 0, 1, 316, 3, 125.00, 466.00, 0.00, 0.00, 119, 0, 0, ''),
(404, 1455120645, 1, 1455120645, 119, 1, 3, 1455055200, 1455141600, 1, 1, 0, 1, 216, 3, 125.00, 348.00, 0.00, 0.00, 119, 0, 0, ''),
(405, 1455203706, 1, 1455203706, 120, 1, 2, 1455141600, 1455228000, 1, 1, 0, 1, 316, 3, 125.00, 137.00, 0.00, 0.00, 120, 0, 0, ''),
(406, 1455203730, 1, 1455203730, 120, 1, 2, 1455141600, 1455228000, 1, 1, 0, 1, 216, 3, 125.00, 137.00, 0.00, 0.00, 120, 0, 0, ''),
(407, 1455203747, 1, 1455203747, 120, 1, 2, 1455141600, 1455228000, 1, 1, 0, 1, 201, 3, 125.00, 171.00, 0.00, 0.00, 120, 0, 0, ''),
(408, 1455203954, 1, 1455203954, 120, 1, 2, 1455141600, 1455228000, 1, 2, 0, 1, 203, 3, 125.00, 149.00, 0.00, 0.00, 120, 0, 0, ''),
(409, 1455212411, 1, 1455212411, 121, 1, 2, 1455141600, 1455228000, 1, 2, 0, 1, 206, 3, 125.00, 125.00, 0.00, 0.00, 121, 0, 0, ''),
(410, 1455213252, 1, 1455213252, 122, 1, 2, 1455141600, 1455228000, 1, 1, 0, 1, 208, 3, 125.00, 137.00, 0.00, 0.00, 122, 0, 0, ''),
(411, 1455271199, 1, 1455271199, 123, 1, 3, 1455228000, 1455314400, 1, 1, 0, 1, 316, 3, 125.00, 125.00, 0.00, 0.00, 123, 0, 0, ''),
(412, 1455271224, 1, 1455271224, 123, 1, 3, 1455228000, 1455314400, 1, 1, 0, 1, 216, 3, 125.00, 137.00, 0.00, 0.00, 123, 0, 0, ''),
(413, 1455298936, 1, 1455298936, 120, 1, 3, 1455228000, 1455314400, 1, 1, 0, 1, 201, 3, 125.00, 165.00, 0.00, 0.00, 120, 0, 0, ''),
(414, 1455477623, 1, 1455477623, 19, 1, 3, 1455400800, 1455487200, 1, 2, 0, 1, 316, 3, 125.00, 195.00, 0.00, 0.00, 19, 0, 0, ''),
(415, 1455651142, 1, 1455651142, 124, 1, 3, 1456351200, 1456524000, 2, 2, 0, 1, 316, 3, 125.00, 1039.00, 0.00, 0.00, 124, 0, 0, ''),
(416, 1458673702, 1, 1458673702, 113, 1, 3, 1458597600, 1459285200, 8, 2, 0, 1, 316, 3, 125.00, 3896.00, 0.00, 0.00, 113, 0, 0, ''),
(417, 1461406498, 5, 1461504691, 18, 7, 3, 1461445200, 1461531600, 1, 2, 1, 1, 104, 5, 135.00, 298.00, 0.50, 121.50, 99, 20, 10, ''),
(418, 1461407448, 1, 1461407448, 99, 1, 3, 1461358800, 1461618000, 3, 2, 0, 1, 105, 5, 135.00, 1353.00, 0.00, 0.00, 99, 20, 10, ''),
(419, 1461407619, 3, 1461503637, 18, 4, 3, 1461358800, 1461531600, 2, 2, 0, 1, 205, 5, 135.00, 578.00, 0.00, 0.00, 125, 0, 0, ''),
(420, 1461407836, 1, 1461407836, 125, 6, 3, 1461358800, 1461531600, 2, 2, 0, 1, 204, 5, 135.00, 270.00, 0.00, 0.00, 125, 0, 0, ''),
(421, 1461495091, 3, 1461501079, 18, 1, 3, 1461531600, 1461618000, 1, 2, 0, 1, 312, 3, 125.00, 374.00, 0.00, 0.00, 20, 20, 10, ''),
(422, 1461501537, 2, 1461501890, 18, 1, 3, 1461963600, 1462050000, 1, 2, 0, 1, 316, 3, 125.00, 125.00, 0.00, 0.00, 20, 20, 10, ''),
(423, 1461501560, 1, 1461501560, 20, 1, 3, 1461963600, 1462050000, 1, 2, 0, 1, 216, 3, 125.00, 125.00, 0.00, 0.00, 20, 20, 10, ''),
(424, 1461501579, 2, 1461505513, 18, 2, 3, 1461963600, 1462050000, 1, 2, 0, 1, 202, 3, 125.00, 125.00, 887.50, 112.50, 20, 20, 10, ''),
(425, 1462625793, 1, 1462625793, 99, 1, 3, 1462568400, 1462654800, 1, 2, 0, 1, 316, 3, 125.00, 125.00, 0.00, 0.00, 99, 20, 10, ''),
(426, 1462630114, 1, 1462630114, 19, 1, 3, 1462568400, 1462654800, 1, 2, 0, 1, 216, 3, 125.00, 125.00, 0.00, 0.00, 19, 0, 0, ''),
(427, 1462957268, 1, 1462957268, 20, 1, 3, 1462914000, 1463000400, 1, 2, 0, 1, 316, 3, 125.00, 195.00, 0.00, 0.00, 20, 20, 10, ''),
(428, 1463334485, 1, 1463334485, 19, 1, 3, 1463259600, 1463346000, 1, 2, 0, 1, 316, 3, 125.00, 125.00, 0.00, 0.00, 19, 0, 0, ''),
(429, 1463420560, 1, 1463420560, 19, 1, 3, 1463346000, 1463432400, 1, 2, 0, 1, 316, 3, 125.00, 517.00, 0.00, 0.00, 19, 0, 0, ''),
(430, 1463421288, 2, 1463421686, 18, 7, 3, 1463346000, 1463432400, 1, 2, 0, 3, NULL, 3, 0.10, 0.10, 0.00, 0.00, 113, 0, 0, ''),
(431, 1463421969, 1, 1463421969, 20, 1, 3, 1463605200, 1463864400, 3, 2, 0, 1, 316, 3, 125.00, 1959.00, 0.00, 0.00, 20, 20, 10, ''),
(432, 1465536037, 1, 1465536037, 126, 1, 3, 1465506000, 1465592400, 1, 2, 0, 1, 104, 5, 135.00, 499.00, 0.00, 0.00, 126, 0, 0, ''),
(433, 1465538587, 7, 1468224950, 18, 1, 3, 1465506000, 1465592400, 1, 2, 0, 1, 105, 5, 135.00, 289.00, 0.00, 0.00, 127, 0, 0, ''),
(434, 1465539122, 1, 1465539122, 128, 6, 3, 1465506000, 1465592400, 1, 2, 0, 1, NULL, 3, 125.00, 441.00, 0.00, 0.00, 128, 0, 0, ''),
(435, 1465539257, 1, 1465539257, 128, 1, 3, 1465506000, 1465592400, 1, 2, 0, 1, 216, 3, 125.00, 125.00, 0.00, 0.00, 128, 0, 0, ''),
(436, 1465745380, 1, 1465745380, 128, 1, 3, 1465678800, 1465765200, 1, 2, 0, 1, 316, 3, 125.00, 311.00, 0.00, 0.00, 128, 0, 0, ''),
(437, 1468226846, 2, 1468226916, 18, 1, 3, 1468184400, 1468270800, 1, 2, 0, 1, 104, 5, 135.00, 135.00, 0.00, 0.00, 129, 0, 0, ''),
(438, 1468603579, 1, 1468603579, 20, 1, 3, 1468530000, 1468789200, 3, 2, 0, 1, 316, 3, 125.00, 546.00, 0.00, 0.00, 20, 0, 0, ''),
(439, 1468603734, 1, 1468603734, 19, 1, 3, 1468530000, 1468616400, 1, 2, 0, 1, 216, 3, 125.00, 199.00, 0.00, 0.00, 19, 0, 0, ''),
(440, 1468603858, 1, 1468603858, 130, 1, 3, 1468530000, 1468616400, 1, 2, 0, 1, 201, 3, 125.00, 225.00, 0.00, 0.00, 130, 0, 0, ''),
(441, 1468603962, 1, 1468603962, 20, 1, 3, 1468530000, 1468789200, 3, 2, 1, 1, 203, 3, 125.00, 567.50, 0.00, 0.00, 20, 0, 0, ''),
(442, 1468604348, 1, 1468604348, 19, 1, 3, 1468530000, 1468616400, 1, 2, 0, 1, 206, 3, 125.00, 125.00, 0.00, 0.00, 19, 0, 0, ''),
(443, 1470327909, 1, 1470327909, 20, 1, 3, 1470258000, 1470517200, 3, 2, 0, 1, 316, 3, 125.00, 2000.00, 0.00, 0.00, 20, 20, 10, '');

-- --------------------------------------------------------

--
-- Table structure for table `tst_hotel_booking_excursions`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_booking_excursions` (
  `booking_id` int(11) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  `date` int(11) NOT NULL,
  `guest_num` int(11) NOT NULL,
  `guest_type` int(11) NOT NULL,
  `cost` float(10,2) NOT NULL,
  `outer_id` int(11) DEFAULT NULL,
  KEY `fk_hotel_booking_excursions_booking_id` (`booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tst_hotel_booking_excursions`
--

INSERT INTO `tst_hotel_booking_excursions` (`booking_id`, `version`, `date`, `guest_num`, `guest_type`, `cost`, `outer_id`) VALUES
(183, 1, 1415138400, 1, 1, 251.00, NULL),
(187, 1, 1415484000, 1, 1, 251.00, NULL),
(187, 1, 1415656800, 1, 1, 261.00, NULL),
(186, 1, 1415311200, 1, 1, 254.00, NULL),
(186, 1, 1415311200, 2, 1, 254.00, NULL),
(186, 1, 1415397600, 1, 1, 154.00, NULL),
(186, 1, 1415397600, 2, 1, 154.00, NULL),
(185, 1, 1415224800, 1, 1, 262.00, NULL),
(185, 1, 1415224800, 2, 1, 262.00, NULL),
(185, 1, 1415311200, 2, 1, 254.00, NULL),
(185, 1, 1415397600, 2, 1, 154.00, NULL),
(179, 1, 1414879200, 1, 1, 142.00, NULL),
(179, 1, 1414879200, 1, 2, 71.00, NULL),
(192, 1, 1416348000, 1, 1, 10.00, NULL),
(193, 1, 1416175200, 1, 1, 8.00, NULL),
(199, 1, 1417557600, 1, 1, 142.00, NULL),
(199, 1, 1417557600, 2, 1, 142.00, NULL),
(199, 1, 1417730400, 1, 1, 254.00, NULL),
(199, 1, 1417730400, 2, 1, 254.00, NULL),
(199, 1, 1418076000, 1, 1, 261.00, NULL),
(199, 1, 1418076000, 2, 1, 261.00, NULL),
(201, 1, 1420236000, 1, 1, 154.00, NULL),
(201, 1, 1420236000, 2, 1, 154.00, NULL),
(201, 1, 1420236000, 1, 2, 77.00, NULL),
(201, 1, 1420322400, 1, 1, 251.00, NULL),
(201, 1, 1420322400, 2, 1, 251.00, NULL),
(201, 1, 1420322400, 1, 2, 125.50, NULL),
(201, 1, 1420408800, 1, 1, 121.00, NULL),
(201, 1, 1420408800, 2, 1, 121.00, NULL),
(201, 1, 1420408800, 1, 2, 60.50, NULL),
(201, 1, 1420495200, 1, 1, 261.00, NULL),
(201, 1, 1420495200, 2, 1, 261.00, NULL),
(201, 1, 1420495200, 1, 2, 130.50, NULL),
(202, 1, 1416693600, 1, 1, 251.00, NULL),
(202, 1, 1416780000, 1, 1, 121.00, NULL),
(202, 1, 1416866400, 1, 1, 261.00, NULL),
(202, 1, 1416952800, 1, 1, 142.00, NULL),
(203, 1, 1416693600, 1, 1, 251.00, NULL),
(204, 1, 1416866400, 1, 1, 261.00, NULL),
(204, 1, 1416866400, 2, 1, 261.00, NULL),
(204, 1, 1416780000, 1, 1, 121.00, NULL),
(204, 1, 1416780000, 2, 1, 121.00, NULL),
(210, 1, 1417212000, 1, 1, 154.00, NULL),
(210, 1, 1417384800, 1, 1, 121.00, NULL),
(210, 1, 1417557600, 1, 1, 142.00, NULL),
(210, 1, 1417730400, 1, 1, 254.00, NULL),
(212, 1, 1417212000, 1, 1, 154.00, NULL),
(212, 1, 1417212000, 2, 1, 154.00, NULL),
(212, 1, 1417298400, 1, 1, 251.00, NULL),
(212, 1, 1417298400, 2, 1, 251.00, NULL),
(213, 1, 1418767200, 1, 1, 142.00, NULL),
(213, 1, 1418767200, 1, 2, 71.00, NULL),
(213, 1, 1418853600, 1, 1, 262.00, NULL),
(213, 1, 1418853600, 1, 2, 131.00, NULL),
(213, 1, 1418940000, 1, 1, 254.00, NULL),
(213, 1, 1418940000, 1, 2, 127.00, NULL),
(213, 1, 1419026400, 1, 1, 154.00, NULL),
(213, 1, 1419026400, 1, 2, 77.00, NULL),
(213, 1, 1419112800, 1, 1, 251.00, NULL),
(213, 1, 1419112800, 1, 2, 125.50, NULL),
(213, 1, 1419199200, 1, 1, 121.00, NULL),
(213, 1, 1419199200, 1, 2, 60.50, NULL),
(213, 1, 1419285600, 1, 1, 261.00, NULL),
(213, 1, 1419285600, 1, 2, 130.50, NULL),
(213, 1, 1419544800, 1, 1, 254.00, NULL),
(213, 1, 1419544800, 1, 2, 127.00, NULL),
(214, 1, 1417471200, 1, 1, 261.00, NULL),
(214, 1, 1417471200, 2, 1, 261.00, NULL),
(214, 1, 1417471200, 1, 2, 130.50, NULL),
(227, 1, 1417816800, 1, 1, 154.00, NULL),
(227, 1, 1417816800, 2, 1, 154.00, NULL),
(227, 1, 1417816800, 1, 2, 77.00, NULL),
(231, 1, 1419804000, 1, 1, 121.00, NULL),
(231, 1, 1419890400, 1, 1, 261.00, NULL),
(237, 1, 1420322400, 1, 1, 807.00, NULL),
(230, 1, 1419631200, 1, 1, 154.00, NULL),
(241, 1, 1422223200, 1, 1, 80.00, NULL),
(241, 1, 1422309600, 1, 1, 82.00, NULL),
(246, 1, 1422396000, 1, 1, 83.00, NULL),
(244, 2, 1422309600, 1, 1, 82.00, NULL),
(248, 1, 1423173600, 1, 1, 15.00, NULL),
(254, 2, 1423692000, 1, 1, 14.00, NULL),
(254, 2, 1423778400, 1, 1, 15.00, NULL),
(254, 4, 1423692000, 1, 1, 14.00, NULL),
(254, 4, 1423778400, 1, 1, 15.00, NULL),
(290, 1, 1427320800, 1, 1, 26.00, NULL),
(290, 1, 1427320800, 2, 1, 26.00, NULL),
(290, 1, 1427407200, 1, 1, 25.00, NULL),
(290, 1, 1427407200, 2, 1, 25.00, NULL),
(293, 1, 1428181200, 1, 1, 21.00, NULL),
(293, 1, 1428181200, 2, 1, 21.00, NULL),
(302, 1, 1428699600, 1, 1, 15.00, NULL),
(302, 1, 1428699600, 2, 1, 15.00, NULL),
(306, 1, 1428613200, 1, 1, 25.00, NULL),
(306, 1, 1428613200, 2, 1, 25.00, NULL),
(306, 1, 1428699600, 1, 1, 15.00, NULL),
(306, 1, 1428699600, 2, 1, 15.00, NULL),
(324, 1, 1430427600, 1, 1, 25.00, NULL),
(324, 1, 1430514000, 1, 1, 15.00, NULL),
(325, 4, 1430514000, 1, 1, 15.00, NULL),
(325, 4, 1430514000, 2, 1, 15.00, NULL),
(325, 5, 1430514000, 1, 1, 15.00, NULL),
(325, 5, 1430514000, 2, 1, 15.00, NULL),
(325, 6, 1430514000, 1, 1, 15.00, NULL),
(325, 6, 1430514000, 2, 1, 15.00, NULL),
(334, 1, 1431810000, 1, 1, 21.00, NULL),
(334, 1, 1431810000, 2, 1, 21.00, NULL),
(334, 2, 1431810000, 1, 1, 21.00, NULL),
(334, 2, 1431810000, 2, 1, 21.00, NULL),
(337, 1, 1433883600, 1, 1, 14.00, NULL),
(337, 1, 1433883600, 2, 1, 14.00, NULL),
(357, 1, 1436302800, 1, 1, 14.00, NULL),
(357, 1, 1436302800, 2, 1, 14.00, NULL),
(357, 1, 1436389200, 1, 1, 26.00, NULL),
(357, 1, 1436389200, 2, 1, 26.00, NULL),
(361, 1, 1436389200, 1, 1, 26.00, NULL),
(361, 1, 1436389200, 2, 1, 26.00, NULL),
(374, 1, 1440968400, 1, 1, 12.00, NULL),
(374, 1, 1441054800, 1, 1, 26.00, NULL),
(374, 1, 1441054800, 1, 2, 13.00, NULL),
(374, 1, 1441141200, 1, 1, 14.00, NULL),
(374, 1, 1441141200, 2, 1, 14.00, NULL),
(374, 2, 1440968400, 1, 1, 12.00, NULL),
(374, 2, 1441054800, 1, 1, 26.00, NULL),
(374, 2, 1441054800, 1, 2, 13.00, NULL),
(374, 2, 1441141200, 1, 1, 14.00, NULL),
(374, 2, 1441141200, 2, 1, 14.00, NULL),
(377, 1, 1440622800, 1, 1, 26.00, NULL),
(377, 1, 1440622800, 2, 1, 26.00, NULL),
(377, 1, 1440622800, 1, 2, 13.00, NULL),
(377, 1, 1440709200, 1, 1, 25.00, NULL),
(377, 1, 1440709200, 2, 1, 25.00, NULL),
(377, 1, 1440795600, 1, 1, 15.00, NULL),
(377, 1, 1440795600, 1, 2, 7.50, NULL),
(377, 1, 1440882000, 1, 1, 21.00, NULL),
(377, 1, 1440968400, 1, 1, 12.00, NULL),
(377, 1, 1440968400, 2, 1, 12.00, NULL),
(377, 1, 1440968400, 1, 2, 6.00, NULL),
(377, 1, 1441054800, 1, 1, 26.00, NULL),
(377, 1, 1441054800, 2, 1, 26.00, NULL),
(377, 1, 1441054800, 1, 2, 13.00, NULL),
(377, 1, 1441141200, 1, 1, 14.00, NULL),
(377, 1, 1441141200, 2, 1, 14.00, NULL),
(377, 1, 1441141200, 1, 2, 7.00, NULL),
(381, 1, 1441746000, 1, 1, 14.00, NULL),
(381, 1, 1441746000, 2, 1, 14.00, NULL),
(382, 1, 1441832400, 1, 1, 26.00, NULL),
(382, 1, 1441832400, 2, 1, 26.00, NULL),
(382, 1, 1441832400, 1, 2, 13.00, NULL),
(382, 1, 1441918800, 1, 1, 25.00, NULL),
(382, 1, 1441918800, 2, 1, 25.00, NULL),
(382, 1, 1441918800, 1, 2, 12.50, NULL),
(382, 1, 1442005200, 1, 1, 15.00, NULL),
(382, 1, 1442005200, 2, 1, 15.00, NULL),
(382, 1, 1442005200, 1, 2, 7.50, NULL),
(382, 1, 1442091600, 1, 1, 21.00, NULL),
(382, 1, 1442091600, 2, 1, 21.00, NULL),
(382, 1, 1442091600, 1, 2, 10.50, NULL),
(383, 1, 1443992400, 1, 1, 12.00, NULL),
(383, 1, 1443992400, 2, 1, 12.00, NULL),
(383, 1, 1443992400, 1, 2, 6.00, NULL),
(384, 1, 1443992400, 1, 1, 12.00, NULL),
(387, 1, 1445288400, 1, 1, 26.00, NULL),
(387, 1, 1445288400, 2, 1, 26.00, NULL),
(388, 1, 1447624800, 1, 1, 12.00, NULL),
(388, 1, 1447624800, 2, 1, 12.00, NULL),
(388, 1, 1447624800, 1, 2, 6.00, NULL),
(388, 1, 1447711200, 1, 1, 26.00, NULL),
(388, 1, 1447711200, 2, 1, 26.00, NULL),
(388, 1, 1447797600, 1, 1, 14.00, NULL),
(388, 1, 1447797600, 1, 2, 7.00, NULL),
(388, 1, 1447884000, 1, 1, 26.00, NULL),
(388, 1, 1447884000, 2, 1, 26.00, NULL),
(388, 1, 1447884000, 1, 2, 13.00, NULL),
(388, 1, 1447970400, 1, 1, 25.00, NULL),
(388, 1, 1447970400, 2, 1, 25.00, NULL),
(390, 1, 1448056800, 1, 1, 15.00, NULL),
(390, 1, 1448056800, 2, 1, 15.00, NULL),
(390, 1, 1448143200, 1, 1, 21.00, NULL),
(390, 1, 1448143200, 2, 1, 21.00, NULL),
(390, 1, 1448143200, 1, 2, 10.50, NULL),
(393, 1, 1449784800, 1, 1, 25.00, NULL),
(393, 1, 1449784800, 2, 1, 25.00, NULL),
(393, 1, 1449957600, 1, 1, 21.00, NULL),
(393, 1, 1449957600, 2, 1, 21.00, NULL),
(398, 1, 1452549600, 1, 1, 26.00, NULL),
(398, 1, 1452549600, 2, 1, 26.00, NULL),
(398, 1, 1452636000, 1, 1, 14.00, NULL),
(398, 1, 1452636000, 2, 1, 14.00, NULL),
(398, 1, 1452722400, 1, 1, 26.00, NULL),
(398, 1, 1452722400, 2, 1, 26.00, NULL),
(398, 1, 1452808800, 1, 1, 25.00, NULL),
(398, 1, 1452808800, 2, 1, 25.00, NULL),
(398, 1, 1452895200, 1, 1, 15.00, NULL),
(398, 1, 1452895200, 2, 1, 15.00, NULL),
(398, 1, 1452981600, 1, 1, 21.00, NULL),
(398, 1, 1452981600, 2, 1, 21.00, NULL),
(399, 1, 1453240800, 1, 1, 14.00, NULL),
(399, 1, 1453240800, 2, 1, 14.00, NULL),
(399, 1, 1453327200, 1, 1, 26.00, NULL),
(399, 1, 1453327200, 2, 1, 26.00, NULL),
(401, 1, 1454450400, 1, 1, 14.00, NULL),
(401, 1, 1454450400, 2, 1, 14.00, NULL),
(404, 1, 1455055200, 1, 1, 14.00, NULL),
(404, 1, 1455141600, 1, 1, 26.00, NULL),
(413, 1, 1455228000, 1, 1, 25.00, NULL),
(413, 1, 1455314400, 1, 1, 15.00, NULL),
(415, 1, 1456351200, 1, 1, 26.00, NULL),
(415, 1, 1456351200, 2, 1, 26.00, NULL),
(415, 1, 1456437600, 1, 1, 25.00, NULL),
(415, 1, 1456437600, 2, 1, 25.00, NULL),
(415, 1, 1456524000, 1, 1, 15.00, NULL),
(415, 1, 1456524000, 2, 1, 15.00, NULL),
(416, 1, 1458597600, 1, 1, 26.00, NULL),
(416, 1, 1458597600, 2, 1, 26.00, NULL),
(416, 1, 1458684000, 1, 1, 14.00, NULL),
(416, 1, 1458684000, 2, 1, 14.00, NULL),
(416, 1, 1458770400, 1, 1, 26.00, NULL),
(416, 1, 1458770400, 2, 1, 26.00, NULL),
(416, 1, 1458856800, 1, 1, 25.00, NULL),
(416, 1, 1458856800, 2, 1, 25.00, NULL),
(416, 1, 1458943200, 1, 1, 15.00, NULL),
(416, 1, 1459029600, 1, 1, 21.00, NULL),
(416, 1, 1459029600, 2, 1, 21.00, NULL),
(416, 1, 1459112400, 1, 1, 12.00, NULL),
(416, 1, 1459112400, 2, 1, 12.00, NULL),
(416, 1, 1459198800, 1, 1, 26.00, NULL),
(416, 1, 1459198800, 2, 1, 26.00, NULL),
(416, 1, 1459285200, 1, 1, 14.00, NULL),
(416, 1, 1459285200, 2, 1, 14.00, NULL),
(429, 1, 1463346000, 1, 1, 12.00, NULL),
(429, 1, 1463346000, 2, 1, 12.00, NULL),
(429, 1, 1463432400, 1, 1, 26.00, NULL),
(429, 1, 1463432400, 2, 1, 26.00, NULL),
(431, 1, 1463605200, 1, 1, 26.00, NULL),
(431, 1, 1463605200, 2, 1, 26.00, NULL),
(431, 1, 1463691600, 1, 1, 25.00, NULL),
(431, 1, 1463691600, 2, 1, 25.00, NULL),
(431, 1, 1463778000, 1, 1, 15.00, NULL),
(431, 1, 1463778000, 2, 1, 15.00, NULL),
(431, 1, 1463864400, 1, 1, 21.00, NULL),
(431, 1, 1463864400, 2, 1, 21.00, NULL),
(432, 1, 1465592400, 1, 1, 15.00, NULL),
(432, 1, 1465592400, 2, 1, 15.00, NULL),
(436, 1, 1465765200, 1, 1, 12.00, NULL),
(436, 1, 1465765200, 2, 1, 12.00, NULL),
(438, 1, 1468530000, 1, 1, 25.00, NULL),
(438, 1, 1468530000, 2, 1, 25.00, NULL),
(438, 1, 1468616400, 1, 1, 15.00, NULL),
(438, 1, 1468616400, 2, 1, 15.00, NULL),
(438, 1, 1468702800, 1, 1, 21.00, NULL),
(438, 1, 1468702800, 2, 1, 21.00, NULL),
(438, 1, 1468789200, 1, 1, 12.00, NULL),
(438, 1, 1468789200, 2, 1, 12.00, NULL),
(439, 1, 1468530000, 1, 1, 25.00, NULL),
(439, 1, 1468530000, 2, 1, 25.00, NULL),
(440, 1, 1468530000, 1, 1, 25.00, NULL),
(440, 1, 1468530000, 2, 1, 25.00, NULL),
(441, 1, 1468530000, 1, 1, 25.00, NULL),
(441, 1, 1468530000, 2, 1, 25.00, NULL),
(441, 1, 1468530000, 1, 2, 12.50, NULL),
(441, 1, 1468616400, 1, 1, 15.00, NULL),
(441, 1, 1468616400, 2, 1, 15.00, NULL),
(441, 1, 1468616400, 1, 2, 7.50, NULL),
(441, 1, 1468702800, 1, 1, 21.00, NULL),
(441, 1, 1468702800, 2, 1, 21.00, NULL),
(441, 1, 1468702800, 1, 2, 10.50, NULL),
(441, 1, 1468789200, 1, 1, 12.00, NULL),
(441, 1, 1468789200, 2, 1, 12.00, NULL),
(441, 1, 1468789200, 1, 2, 6.00, NULL),
(443, 1, 1470258000, 1, 1, 26.00, NULL),
(443, 1, 1470258000, 2, 1, 26.00, NULL),
(443, 1, 1470344400, 1, 1, 25.00, NULL),
(443, 1, 1470344400, 2, 1, 25.00, NULL),
(443, 1, 1470430800, 1, 1, 15.00, NULL),
(443, 1, 1470430800, 2, 1, 15.00, NULL),
(443, 1, 1470517200, 1, 1, 21.00, NULL),
(443, 1, 1470517200, 2, 1, 21.00, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tst_hotel_booking_extra_services`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_booking_extra_services` (
  `booking_id` int(11) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  `date` int(11) NOT NULL,
  `cost` float(10,2) NOT NULL DEFAULT '0.00',
  `descr` varchar(300) NOT NULL,
  KEY `fk_booking_extra_services_booking_id` (`booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tst_hotel_booking_extra_services`
--

INSERT INTO `tst_hotel_booking_extra_services` (`booking_id`, `version`, `date`, `cost`, `descr`) VALUES
(179, 1, 1415916000, 34.00, 'sehdjdr swt aw'),
(179, 1, 1416175200, 26.00, 'shsehesesh'),
(204, 1, 1416866400, 200.00, 'Доп услуга');

-- --------------------------------------------------------

--
-- Table structure for table `tst_hotel_booking_guests`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_booking_guests` (
  `booking_id` int(11) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  `guest_num` int(11) NOT NULL DEFAULT '1',
  `guest_type` int(11) NOT NULL DEFAULT '1',
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `scan` varchar(50) DEFAULT NULL,
  `address` varchar(150) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  KEY `fk_hotel_booking_guests_booking_id` (`booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tst_hotel_booking_guests`
--

INSERT INTO `tst_hotel_booking_guests` (`booking_id`, `version`, `guest_num`, `guest_type`, `first_name`, `last_name`, `scan`, `address`, `email`, `phone`) VALUES
(183, 1, 1, 1, 'Dorian', 'Gray', '', '', 'doriangray@ukr.net', '1111'),
(187, 1, 1, 1, 'sehseh', 'sehseh', '', '', 'sehse@shs.seh', '2523'),
(187, 1, 2, 1, 'dgu', 'jufu', '', '', 'yucs@jfs.wuyf', '78888'),
(187, 1, 1, 2, 'sehesh', 'sehs', '', '', NULL, NULL),
(186, 1, 1, 1, 'seh', 'esh', '', '', 'esh@srh.seg', '3252'),
(186, 1, 2, 1, 'seg', 'seh', '', '', 'seh@srh.seg', '325'),
(186, 1, 1, 2, 'shseh', 'sehseh', '', '', NULL, NULL),
(181, 1, 1, 1, 'Lvbnhbq', 'vcwgd', '', '', 'weygdeby@gmail.com', '54654654646'),
(181, 1, 2, 1, 'dufiudf', 'idufi', '', 'iduf', 'iufis7f@kusg.quf', '7258258'),
(181, 1, 1, 2, 'sehshe', 'sehse', '', '', NULL, NULL),
(185, 1, 1, 1, 'aw', 'awg', '', '', 'awg@sh.seg', '325'),
(185, 1, 2, 1, 'seg', 'seh', '', '', 'seh@sh.rsh', '352'),
(188, 1, 1, 1, 'Dorian', 'Gray', '', '', NULL, '+380683573536'),
(189, 1, 1, 1, 'Гость №1', NULL, '', '', NULL, NULL),
(189, 1, 2, 1, 'Гость №1', NULL, '', '', NULL, NULL),
(190, 1, 1, 1, 'Гость №2', NULL, '', '', NULL, NULL),
(190, 1, 2, 1, 'Гость №2', NULL, '', '', NULL, NULL),
(179, 1, 1, 1, 'rsh', 'hsr', '', '', 'esh@sh.se', '32525'),
(179, 1, 1, 2, 'esg', 'segh', '', '', NULL, NULL),
(191, 1, 1, 1, 'Ребенок №2 ', NULL, '', '', NULL, NULL),
(201, 1, 1, 2, 'griogjrtg', 'dgrtgrtg', '', '', NULL, NULL),
(205, 1, 1, 2, 'Гость №21', 'Гость №21', '', '', NULL, NULL),
(208, 1, 1, 1, 'drhrh', 'ehw', NULL, NULL, 'whw@djt.drh', '3252'),
(208, 1, 2, 1, 'sehes', 'sehes', NULL, NULL, 'sehse@drh.she', '252223'),
(210, 1, 1, 1, 'dsfds', 'sdfsd', 'scan_1416202325.jpg', '', 'doriangray@ukr.net', 'sds'),
(212, 1, 1, 1, 'Гость', 'Гость', NULL, NULL, 'Гость', 'Гость'),
(212, 1, 2, 1, 'Гость', 'Гость', NULL, NULL, 'Гость', 'Гость'),
(213, 1, 1, 1, 'Гость', 'Гость', NULL, NULL, 'Гость', 'Гость'),
(213, 1, 1, 2, 'Гость', 'Гость', NULL, NULL, NULL, NULL),
(214, 1, 1, 1, 'seg', 'esh', NULL, NULL, 'ftz@fj.dr', '3463'),
(214, 1, 2, 1, 'hh', 'rshrs', NULL, NULL, 'srh@dh.dd', '463'),
(214, 1, 1, 2, 'rhs', 'sh', NULL, NULL, NULL, NULL),
(216, 1, 1, 1, 'seges', 'sehesh', NULL, NULL, 'sehes@kfyf', '84332'),
(220, 1, 1, 1, 'ника', 'м', NULL, NULL, 'nika.valkyria@gmail.com', '113322'),
(221, 1, 1, 1, 'ника', 'м', NULL, NULL, 'nika.valkyria@gmail.com', '124347'),
(227, 1, 1, 1, 'Дмитрий', 'Сукачев', 'scan_1417786586.jpg', '', '0675641740ua@gmail.com', '0675641740'),
(227, 1, 2, 1, 'Иванов', 'Иван', '', '', 'ivan@gmail.com', '2342424233'),
(227, 1, 1, 2, 'Иванов', 'Иван', NULL, NULL, NULL, NULL),
(224, 1, 1, 1, 'vanquisher', 'ua', 'scan_1416686348.png', '', 'vanquisher.ua@gmail.com', 'weq'),
(228, 1, 1, 1, '1', '1', 'scan_1416686348.png', NULL, 'vanquisher.ua@gmail.com', 'number'),
(228, 1, 2, 1, '1', '1', 'scan_1416686348.png', NULL, 'vanquisher.ua@gmail.com', NULL),
(228, 1, 1, 2, '1', '1', NULL, NULL, NULL, NULL),
(229, 1, 1, 1, 'sfuf', 'dufiu', NULL, NULL, 'udfi@owy.qi', '298272-72-22'),
(229, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(229, 1, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL),
(231, 1, 1, 1, 'seg', 'esh', '', '', 'seh@sa.sq', '3252511'),
(236, 1, 1, 1, 'seg', 'esh@shs.se', NULL, NULL, 'awv@vus.aku', '8375026502'),
(236, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(237, 1, 1, 1, 'iodhiog', 'oudfo', NULL, NULL, 'viuf@iug.wif', '3970370'),
(230, 1, 1, 1, 'dxh', 'xeh', '', '', 'xeh@sh.se', '141241'),
(238, 1, 1, 1, 'я', 'я', '', '', 'я', 'я'),
(238, 1, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL),
(239, 1, 1, 1, 'Test', 'edsse', '', NULL, 'test@test.net', '235235'),
(239, 1, 2, 1, 'GGGGGG', 'Segse', NULL, NULL, 'se_gs@sr.se', '23525252'),
(240, 1, 1, 1, 'Test', 'edsse', '', NULL, 'test@test.net', '235235'),
(242, 1, 1, 1, '1', '1', '', NULL, '1', '1'),
(243, 1, 1, 1, '1', '1', '', NULL, '1', '1'),
(241, 1, 1, 1, '1', '1', 'scan_1416686348.png', NULL, 'vanquisher.ua@gmail.com', 'number'),
(244, 1, 1, 1, 'wgkui', 'ugfia', NULL, NULL, 'suif@isg.ak', '9325025602'),
(245, 1, 1, 1, '1', '1', 'scan_1416686348.png', NULL, 'vanquisher.ua@gmail.com', 'number'),
(246, 1, 1, 1, '1', '1', 'scan_1416686348.png', '', 'vanquisher.ua@gmail.com', 'number'),
(247, 1, 1, 1, 'Дмитрий', 'Сукачев', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '0675641740'),
(244, 2, 1, 1, 'wgkui', 'ugfia', '', '', 'suif@isg.ak', '9325025602'),
(248, 1, 1, 1, 'dufi', 'kuf', NULL, NULL, 'hxyu@lig.alj', '2808260'),
(245, 2, 1, 1, '1', '1', 'scan_1416686348.png', NULL, 'vanquisher.ua@gmail.com', 'number'),
(245, 3, 1, 1, '1', '1', 'scan_1416686348.png', NULL, 'vanquisher.ua@gmail.com', 'number'),
(249, 1, 1, 1, 'Test', 'edsse', '', NULL, 'test@test.net', '235235'),
(249, 1, 2, 1, 'drg', 'sieg', NULL, NULL, 'sieg@lish.slg', '8465025602'),
(249, 2, 1, 1, 'Test', 'edsse', '', '', 'test@test.net', '235235'),
(249, 2, 2, 1, 'drg', 'sieg', '', '', 'sieg@lish.slg', '8465025602'),
(250, 1, 1, 1, '1', '1', 'scan_1416686348.png', NULL, 'vanquisher.ua@gmail.com', 'number'),
(251, 1, 1, 1, 'Sergey', 'Suzdaltsev', 'scan_1416202325.jpg', NULL, 'doriangray@ukr.net', '123'),
(251, 1, 2, 1, 'Sergey', 'Suzdaltsev', NULL, NULL, 'd', '213213'),
(252, 1, 1, 1, 'Sergey', 'Suzdaltsev', NULL, NULL, 'dd@dd.com', '1111'),
(252, 1, 2, 1, 'Sergey', 'hghjg', NULL, NULL, 'hjdsgfhjds', '2374783264'),
(253, 1, 1, 1, 'Дмитрий', 'Сукачев', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '0675641740'),
(254, 1, 1, 1, 'FirstName', 'FamilyName', 'scan_1416686348.png', NULL, 'vanquisher.ua@gmail.com', 'number'),
(254, 2, 1, 1, 'FirstName', 'FamilyName', 'scan_1416686348.png', NULL, 'vanquisher.ua@gmail.com', 'number'),
(254, 3, 1, 1, 'FirstName', 'FamilyName', 'scan_1416686348.png', NULL, 'vanquisher.ua@gmail.com', 'number'),
(254, 4, 1, 1, 'FirstName', 'FamilyName', 'scan_1416686348.png', NULL, 'vanquisher.ua@gmail.com', 'number'),
(255, 1, 1, 1, 'Дмитрий', 'Сукачев', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '0675641740'),
(255, 1, 2, 1, '123', '123', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '131231'),
(256, 1, 1, 1, 'Дмитрий', 'Сукачев', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '0675641740'),
(256, 1, 2, 1, '123', '123', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '123'),
(257, 1, 1, 1, 'Дмитрий', 'Сукачев', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '0675641740'),
(257, 1, 2, 1, '234', '234', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '234'),
(257, 2, 1, 1, 'Дмитрий', 'Сукачев', 'scan_1417786586.jpg', '', '0675641740ua@gmail.com', '0675641740'),
(257, 2, 2, 1, '234', '234', 'scan_1417786586.jpg', '', '0675641740ua@gmail.com', '234'),
(258, 1, 1, 1, 'Дмитрий', 'Сукачев', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '0675641740'),
(258, 1, 2, 1, '234', '234', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '234'),
(259, 1, 1, 1, 'Дмитрий', 'Сукачев', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '0675641740'),
(259, 1, 2, 1, '24', '234', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '234'),
(260, 1, 1, 1, 'Дмитрий', 'Сукачев', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '0675641740'),
(260, 1, 2, 1, '24', '234', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '234'),
(261, 1, 1, 1, 'FirstName', 'FamilyName', 'scan_1416686348.png', NULL, 'vanquisher.ua@gmail.com', 'PhoneNumber'),
(262, 1, 1, 1, '1', '1', '', NULL, '1', '1'),
(265, 1, 1, 1, 'FirstName', 'FamilyName', 'scan_1416686348.png', NULL, 'vanquisher.ua@gmail.com', 'PhoneNumber'),
(272, 1, 1, 1, 'Иванов', 'Иван', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '0675641740'),
(272, 1, 2, 1, 'Иванов', 'Андрей', NULL, NULL, 'цушап@udw.oi', '13123123214'),
(247, 2, 1, 1, 'Дмитрий', 'Сукачев', 'scan_1417786586.jpg', '', '0675641740ua@gmail.com', '0675641740'),
(273, 1, 1, 1, 'Иванов', 'Иван', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '0675641740'),
(273, 1, 2, 1, 'Иванов', 'ыапкер', NULL, NULL, 'гнцуавгн@dhge.rit', '235345667'),
(274, 1, 1, 1, 'це34е', 'цуацуа', NULL, NULL, 'цуцукцу@uydgf', '4345345345'),
(273, 2, 1, 1, 'Иванов', 'Иван', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '0675641740'),
(273, 2, 2, 1, 'Замена', 'ыапкер', NULL, NULL, 'гнцуавгн@dhge.rit', '235345667'),
(277, 1, 1, 1, 'dd', 'dd', NULL, NULL, 'dnal@ida.ksg', '286926'),
(277, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(278, 1, 1, 1, 'у', 'у', '', NULL, 'aka.uchiha.itachi@gmail.com', '11'),
(278, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(279, 1, 1, 1, 'Test', 'edsse', '', NULL, 'test@test.net', '235235'),
(279, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(280, 1, 1, 1, 'Иванов', 'Иван', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '0675641740'),
(280, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(281, 1, 1, 1, 'f', 'f', NULL, NULL, 'ss@dd.dd', '12345'),
(281, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(281, 2, 1, 1, 'f', 'f', '', '', 'ss@dd.dd', '12345'),
(284, 1, 1, 1, 'e', 'e', NULL, NULL, 'e', 'e'),
(284, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(284, 2, 1, 1, 'e', 'e', NULL, NULL, 'e', 'e'),
(284, 2, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(285, 1, 1, 1, '1', '1', '', NULL, '1', '1'),
(285, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(286, 1, 1, 1, '1', '1', NULL, NULL, '11', '1'),
(286, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(286, 2, 1, 1, '1', '1', NULL, NULL, '11', '1'),
(286, 2, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(286, 3, 1, 1, '1', '1', NULL, NULL, '11', '1'),
(286, 3, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(288, 1, 1, 1, '1', '1', NULL, NULL, '11', '1'),
(288, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(288, 2, 1, 1, '1', '1', NULL, NULL, '11', '1'),
(288, 2, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(289, 1, 1, 1, '1', '1', NULL, NULL, '11', '1'),
(289, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(290, 1, 1, 1, '1', '1', 'scan_1416686348.png', NULL, 'vanquisher.ua@gmail.com', '1'),
(290, 1, 2, 1, '2', '2', 'scan_1416686348.png', NULL, 'vanquisher.ua@gmail.com', '2'),
(292, 1, 1, 1, '1', '1', '', NULL, '11', '1'),
(292, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(293, 1, 1, 1, '1', '1', NULL, NULL, 'korolevych.lyubomyr@gmail.com', '1'),
(293, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(294, 1, 1, 1, '1', '1', '', NULL, '11', '1'),
(294, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(296, 1, 1, 1, 'Иванов', 'Иван', '', NULL, 'ivan@com', '847323743'),
(296, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(296, 1, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL),
(298, 1, 1, 1, 'Иванов', 'Иван', '', NULL, 'ivan@com.ua', '847323743'),
(298, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(298, 2, 1, 1, 'Иванов', 'Иван', '', NULL, 'ivan@com.ua', '847323743'),
(298, 2, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(299, 1, 1, 1, 'Иванов', 'Иван', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '0675641740'),
(299, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(299, 2, 1, 1, 'Иванов', 'Иван', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '0675641740'),
(299, 2, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(299, 3, 1, 1, 'Иванов', 'Иван', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '0675641740'),
(299, 3, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(299, 4, 1, 1, 'Иванов', 'Иван', 'scan_1417786586.jpg', '', '0675641740ua@gmail.com', '0675641740'),
(299, 4, 2, 1, NULL, NULL, '', '', NULL, NULL),
(299, 5, 1, 1, 'Иванов', 'Иван', 'scan_1417786586.jpg', '', '0675641740ua@gmail.com', '0675641740'),
(299, 5, 2, 1, NULL, NULL, '', '', NULL, NULL),
(302, 1, 1, 1, 'Клиент ', '111', NULL, NULL, 'huntercamer@gmail.com', '23589237589'),
(302, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(303, 1, 1, 1, 'sej', 'diho', NULL, NULL, 'somebody@mail.com', '2352'),
(306, 1, 1, 1, 'Тест', 'Тест', NULL, NULL, 'eruifuierhf@d.rf', '234234234234'),
(306, 1, 2, 1, 'Ntcn', NULL, NULL, NULL, NULL, NULL),
(307, 1, 1, 1, '1', '1', NULL, NULL, 'korolevych.lyubomyr@gmail.com', '1'),
(307, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(309, 1, 1, 1, 'Dmytro', 'Sukachev 016261', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '+380675641740'),
(309, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(311, 1, 1, 1, 'awt', 'awg', NULL, NULL, 'awg@aaa.aa', '3522'),
(318, 1, 1, 1, '1', '1', '', NULL, '1', '1'),
(318, 2, 1, 1, '1', '1', '', NULL, '1', '1'),
(319, 1, 1, 1, '1', '1', '', NULL, '1', '1'),
(320, 1, 1, 1, '1', '1', '', NULL, '1', '1'),
(320, 2, 1, 1, '1', '1', '', NULL, '1', '1'),
(321, 1, 1, 1, '1', '1', '', NULL, '1', '1'),
(324, 1, 1, 1, '1', '1', NULL, NULL, 'thalick@ukr.net', '1'),
(325, 1, 1, 1, '1', '1', 'scan_1428139015.jpg', NULL, '11', '1'),
(325, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(325, 2, 1, 1, '1', '1', 'scan_1428139015.jpg', NULL, '11', '1'),
(325, 2, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(325, 3, 1, 1, '1', '1', 'scan_1428139015.jpg', NULL, '11', '1'),
(325, 3, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(325, 4, 1, 1, '1', '1', 'scan_1428139015.jpg', NULL, '11', '1'),
(325, 4, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(325, 5, 1, 1, '1', '1', 'scan_1428139015.jpg', NULL, '11', '1'),
(325, 5, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(325, 6, 1, 1, '1', '1', 'scan_1428139015.jpg', '', '11', '1'),
(325, 6, 2, 1, NULL, NULL, '', '', NULL, NULL),
(328, 1, 1, 1, 'FirstName', 'FamilyName', 'scan_1416686348.png', NULL, 'vanquisher.ua@gmail.com', 'PhoneNumber'),
(328, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(329, 1, 1, 1, 'FirstName', 'FamilyName', 'scan_1416686348.png', NULL, 'vanquisher.ua@gmail.com', 'PhoneNumber'),
(329, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(331, 1, 1, 1, 'Dmytro', 'Sukachev 016261', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '+380675641740'),
(331, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(332, 1, 1, 1, 'Dmytro', 'Sukachev 016261', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '+380675641740'),
(332, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(331, 2, 1, 1, 'Dmytro', 'Sukachev 016261', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '+380675641740'),
(331, 2, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(331, 3, 1, 1, 'Dmytro', 'Sukachev 016261', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '+380675641740'),
(331, 3, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(333, 1, 1, 1, 'Dmytro', 'Sukachev 016261', NULL, NULL, 'huntercamer@gmail.com', '+380675641740'),
(333, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(334, 1, 1, 1, 'Dmytro', 'Sukachev 016261', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '+380675641740'),
(334, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(334, 2, 1, 1, 'Dmytro', 'Sukachev 016261', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '+380675641740'),
(334, 2, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(334, 3, 1, 1, 'Dmytro', 'Sukachev 016261', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '+380675641740'),
(334, 3, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(335, 1, 1, 1, 'Test', 'edsse', '', NULL, 'test@test.net', '235235'),
(335, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(335, 2, 1, 1, 'Test', 'edsse', '', NULL, 'test@test.net', '235235'),
(335, 2, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(335, 3, 1, 1, 'Test', 'edsse', '', NULL, 'test@test.net', '235235'),
(335, 3, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(337, 1, 1, 1, 'bbbb', 'bbbbb', NULL, NULL, 'bbb@bbb.bbb', '1345676'),
(337, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(338, 1, 1, 1, 'bbbb', 'bbbbb', '', NULL, 'bbb@bbb.bbb', '1345676'),
(338, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(339, 1, 1, 1, 'FirstName', 'FamilyName', 'scan_1416686348.png', NULL, 'vanquisher.ua@gmail.com', 'PhoneNumber'),
(339, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(340, 1, 1, 1, 'FirstName', 'FamilyName', 'scan_1416686348.png', NULL, 'vanquisher.ua@gmail.com', 'PhoneNumber'),
(340, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(341, 1, 1, 1, 'FirstName', 'FamilyName', 'scan_1416686348.png', NULL, 'vanquisher.ua@gmail.com', 'PhoneNumber'),
(341, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(342, 1, 1, 1, 'й', 'й', NULL, NULL, 'aka.uchiha.itachi@gmail.com', 'й'),
(342, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(342, 2, 1, 1, 'й', 'й', '', NULL, 'aka.uchiha.itachi@gmail.com', 'й'),
(342, 2, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(349, 1, 1, 1, 'FirstName', 'FamilyName', 'scan_1416686348.png', NULL, 'vanquisher.ua@gmail.com', 'PhoneNumber'),
(349, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(350, 1, 1, 1, 'FirstName', 'FamilyName', 'scan_1416686348.png', NULL, 'vanquisher.ua@gmail.com', 'PhoneNumber'),
(350, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(350, 2, 1, 1, 'FirstName', 'FamilyName', 'scan_1416686348.png', NULL, 'vanquisher.ua@gmail.com', 'PhoneNumber'),
(350, 2, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(351, 1, 1, 1, 'sadasd', 'asdasd', NULL, NULL, 'ddd@dd.dd', '123123'),
(351, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(351, 2, 1, 1, 'sadasd', 'asdasd', '', '', 'ddd@dd.dd', '123123'),
(351, 2, 2, 1, NULL, NULL, '', '', NULL, NULL),
(352, 1, 1, 1, 'uywedyef', 'укаукаукаук', NULL, NULL, 'sdycbgefcn@com', '23894792384'),
(352, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(353, 1, 1, 1, 'uywedyef', 'укаукаукаук', NULL, NULL, 'sdycbgefcn@com', '23894792384'),
(353, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(354, 1, 1, 1, 'uywedyef', 'укаукаукаук', NULL, NULL, 'sdycbgefcn@com', '23894792384'),
(354, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(355, 1, 1, 1, 'dddd', 'dddd', NULL, NULL, 'dasd@dd.dd', '12333'),
(355, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(356, 1, 1, 1, 'uywedyef', 'укаукаукаук', NULL, NULL, 'sdycbgefcn@com', '23894792384'),
(356, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(357, 1, 1, 1, 'Dmytro', 'Sukachev 016261', '', NULL, 'huntercamer@gmail.com', '+380675641740'),
(357, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(358, 1, 1, 1, 'Dmytro', 'Sukachev 016261', '', NULL, 'huntercamer@gmail.com', '+380675641740'),
(358, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(359, 1, 1, 1, '454t54t', '34rfergdgdf', NULL, NULL, 'reg@uigu', '23423423'),
(359, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(360, 1, 1, 1, 'Александр', 'Биличак', NULL, NULL, '5482828@i.ua', '23423423'),
(360, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(361, 1, 1, 1, 'Александр', 'Биличак', NULL, NULL, '5482828@i.ua', '0678888888'),
(361, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(362, 1, 1, 1, 'Александр', 'Биличак', '', NULL, '5482828@i.ua', '0678888888'),
(362, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(363, 1, 1, 1, 'Александр', 'Биличак', '', NULL, '5482828@i.ua', '0678888888'),
(363, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(364, 1, 1, 1, 'wruiyruwr', 'iuyiyyuiy', NULL, NULL, 'uiweuify@efr', '23423423444'),
(364, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(365, 1, 1, 1, 'фыввв', 'saddd', NULL, NULL, 'asdad@dd.dd', '123123123'),
(365, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(366, 1, 1, 1, 'qwqqwqwr', 'qweqweqwe', NULL, NULL, 'qweqweqwe@eyy', '234234523'),
(366, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(366, 2, 1, 1, 'qwqqwqwr', 'qweqweqwe', NULL, NULL, 'qweqweqwe@eyy', '234234523'),
(366, 2, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(366, 3, 1, 1, 'qwqqwqwr', 'qweqweqwe', NULL, NULL, 'qweqweqwe@eyy', '234234523'),
(366, 3, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(367, 1, 1, 1, 'wrqwerwer', 'qwrqwrwe', NULL, NULL, 'werwe@eyfg', ' 23424234'),
(367, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(368, 1, 1, 1, '234234234', '234dfefsf', NULL, NULL, '23rd34e@hferf', ' 23423434'),
(368, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(369, 1, 1, 1, 'n', 'n', NULL, NULL, 'firewolf@i.ua', '8236532650'),
(369, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(370, 1, 1, 1, 'bn', 'ndj', NULL, NULL, 'firewolf@mail.i.ua', '8365926'),
(370, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(371, 1, 1, 1, 'n', 'n', '', NULL, 'firewolf@i.ua', '8236532650'),
(371, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(372, 1, 1, 1, 'Иванов', 'Иван', NULL, NULL, 'ivanov@com.ua', '234234235'),
(372, 1, 2, 1, 'Ivanov2', 'Ivan2', NULL, NULL, 'ivanov2@com.ua', '1234567890'),
(372, 2, 1, 1, 'Иванов', 'Иван', NULL, NULL, 'ivanov@com.ua', '234234235'),
(372, 2, 2, 1, 'Ivanov2', 'Ivan2', NULL, NULL, 'ivanov2@com.ua', '1234567890'),
(372, 3, 1, 1, 'Иванов', 'Иван', NULL, NULL, 'ivanov@com.ua', '234234235'),
(372, 3, 2, 1, 'Ivanov2', 'Ivan2', NULL, NULL, 'ivanov2@com.ua', '1234567890'),
(372, 4, 1, 1, 'Иванов', 'Иван', NULL, NULL, 'ivanov@com.ua', '234234235'),
(372, 4, 2, 1, 'Ivanov2', 'Ivan2', NULL, NULL, 'ivanov2@com.ua', '1234567890'),
(372, 5, 1, 1, 'Иванов', 'Иван', NULL, NULL, 'ivanov@com.ua', '234234235'),
(372, 5, 2, 1, 'Ivanov2', 'Ivan2', NULL, NULL, 'ivanov2@com.ua', '1234567890'),
(372, 6, 1, 1, 'Иванов', 'Иван', NULL, NULL, 'ivanov@com.ua', '234234235'),
(372, 6, 2, 1, 'Ivanov2', 'Ivan2', NULL, NULL, 'ivanov2@com.ua', '1234567890'),
(373, 1, 1, 1, 'Иван', 'Иванов', '', NULL, 'shantukovich@gmail.com', '6544644846'),
(373, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(373, 1, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL),
(373, 2, 1, 1, 'Иван', 'Иванов', '', NULL, 'shantukovich@gmail.com', '6544644846'),
(373, 2, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(373, 2, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL),
(373, 3, 1, 1, 'Иван', 'Иванов', '', NULL, 'shantukovich@gmail.com', '6544644846'),
(373, 3, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(373, 3, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL),
(374, 1, 1, 1, 'Иван', 'Иванов', '', NULL, 'shantukovich@gmail.com', '6544644846'),
(374, 1, 2, 1, 'Ivana', 'Ivanova', NULL, NULL, 'qwerty@com.ua', '123456789'),
(374, 1, 1, 2, 'Masha', 'Ivanova', NULL, NULL, NULL, NULL),
(374, 2, 1, 1, 'Иван', 'Иванов', '', NULL, 'shantukovich@gmail.com', '6544644846'),
(374, 2, 2, 1, 'Ivana', 'Ivanova', NULL, NULL, 'qwerty@com.ua', '123456789'),
(374, 2, 1, 2, 'Masha', 'Ivanova', NULL, NULL, NULL, NULL),
(375, 1, 1, 1, 'sa', 'sd', NULL, NULL, 'ff@hh.jj', '43636'),
(375, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(376, 1, 1, 1, 'sa', 'sd', '', NULL, 'ff@hh.jj', '43636'),
(376, 1, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(376, 2, 1, 1, 'sa', 'sd', '', '', 'ff@hh.jj', '43636'),
(335, 4, 1, 1, 'Test', 'edsse', '', NULL, 'test@test.net', '235235'),
(335, 4, 2, 1, '', '', NULL, NULL, '', ''),
(372, 7, 1, 1, 'Иванов', 'Иван', NULL, NULL, 'ivanov@com.ua', '234234235'),
(372, 7, 2, 1, 'Ivanov2', 'Ivan2', NULL, NULL, 'ivanov2@com.ua', '1234567890'),
(372, 8, 1, 1, 'Иванов', 'Иван', NULL, NULL, 'ivanov@com.ua', '234234235'),
(372, 8, 2, 1, 'Ivanov2', 'Ivan2', NULL, NULL, 'ivanov2@com.ua', '1234567890'),
(371, 2, 1, 1, 'n', 'n', '', '', 'firewolf@i.ua', '8236532650'),
(377, 1, 1, 1, 'Иван', 'Иванов', NULL, NULL, 'ivanov@gmail.com', '1234567890'),
(377, 1, 2, 1, 'Иванна', 'Иванова', NULL, NULL, 'ivanova@gmail.com', '0987654321'),
(377, 1, 1, 2, 'Bruse', 'Willis', NULL, NULL, NULL, NULL),
(378, 1, 1, 1, 'esg', 'hse', NULL, NULL, 'seh@srh.seh', '25223'),
(379, 1, 1, 1, 'Test', 'edsse', '', NULL, 'test@test.net', '235235'),
(379, 1, 2, 1, '', '', NULL, NULL, '', ''),
(380, 1, 1, 1, 'seh', 'seh', NULL, NULL, 'seh@eg.se', '352525'),
(380, 1, 2, 1, '', '', NULL, NULL, '', ''),
(381, 1, 1, 1, 'aeg', 'aeg', NULL, NULL, 'aeg@seg.seg', '3252662'),
(381, 1, 2, 1, '', '', NULL, NULL, '', ''),
(382, 1, 1, 1, 'Иванов', 'йцу', NULL, NULL, 'йцуйцуц', '91284398234'),
(382, 1, 2, 1, '', '', NULL, NULL, '', ''),
(382, 1, 1, 2, '', '', NULL, NULL, NULL, NULL),
(383, 1, 1, 1, 'wefwfwe', 'wefwef', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '239846234'),
(383, 1, 2, 1, '', '', NULL, NULL, '', ''),
(383, 1, 1, 2, '', '', NULL, NULL, NULL, NULL),
(384, 1, 1, 1, '234wefwef', 'fefef', NULL, NULL, 'wefwefwef', '234234234'),
(387, 1, 1, 1, 'aaaa', 'aaa', NULL, NULL, 'aaa@aaa.saa', '3252542'),
(387, 1, 2, 1, '', '', NULL, NULL, '', ''),
(388, 1, 1, 1, 'Иванов', 'Иван', NULL, NULL, 'weytrwe@hhh.com', '380675555555'),
(388, 1, 2, 1, 'Иванова', 'Иванна', NULL, NULL, 'qwtydqw@hhh.com', '06723423433'),
(388, 1, 1, 2, 'Ребенок', 'Ивановичь', NULL, NULL, NULL, NULL),
(389, 1, 1, 1, 'asdasd', 'asdasd', NULL, NULL, 'asdasd', '213'),
(389, 1, 2, 1, '', '', NULL, NULL, '', ''),
(390, 1, 1, 1, 'Иванов', 'Иван', '', NULL, 'weytrwe@hhh.com', '380675555555'),
(390, 1, 2, 1, 'Ивановa', 'Иванна', NULL, NULL, 'qwusqwus@hhh.com', '380678237311'),
(390, 1, 1, 2, 'Гарри', 'Потер', NULL, NULL, NULL, NULL),
(379, 2, 1, 1, 'Test', 'edsse', '', '', 'test@test.net', '235235'),
(389, 2, 1, 1, 'asdasd', 'asdasd', '', '', 'asdasd', '213'),
(379, 3, 1, 1, 'Test', 'edsse', '', '', 'test@test.net', '235235'),
(389, 3, 1, 1, 'asdasd', 'asdasd', '', '', 'asdasd', '213'),
(369, 2, 1, 1, 'n', 'n', '', '', 'firewolf@i.ua', '8236532650'),
(370, 2, 1, 1, 'bn', 'ndj', '', '', 'firewolf@mail.i.ua', '8365926'),
(370, 3, 1, 1, 'bn', 'ndj', '', '', 'firewolf@mail.i.ua', '8365926'),
(379, 4, 1, 1, 'Test', 'edsse', '', '', 'test@test.net', '235235'),
(391, 1, 1, 1, 'Dorian', 'Gray', '', NULL, 'dd@dd.com', '381234567'),
(391, 1, 2, 1, '', '', NULL, NULL, '', ''),
(392, 1, 1, 1, 'sdfgdfg', 'sdfg', NULL, NULL, 'gg@dd.dd', '2123123'),
(392, 1, 2, 1, '', '', NULL, NULL, '', ''),
(393, 1, 1, 1, 'sdfgdfg', 'sdfg', '', NULL, 'gg@dd.dd', '2123123'),
(393, 1, 2, 1, '', '', NULL, NULL, '', ''),
(394, 1, 1, 1, 'FirstName', 'FamilyName', 'scan_1416686348.png', NULL, 'vanquisher.ua@gmail.com', '634528658'),
(394, 1, 2, 1, '', '', NULL, NULL, '', ''),
(394, 1, 1, 2, '', '', NULL, NULL, NULL, NULL),
(395, 1, 1, 1, 'FirstName', 'FamilyName', 'scan_1416686348.png', NULL, 'vanquisher.ua@gmail.com', '3808384282365'),
(395, 1, 2, 1, '', '', NULL, NULL, '', ''),
(396, 1, 1, 1, 'name', 'family', 'scan_1416686348.png', NULL, 'vanquisher.ua@gmail.com', '380845294752'),
(396, 1, 2, 1, '', '', NULL, NULL, '', ''),
(397, 1, 1, 1, 'name', 'family', NULL, NULL, '123131.com', '3808352365936'),
(397, 1, 2, 1, '', '', NULL, NULL, '', ''),
(398, 1, 1, 1, 'Иванов', 'Иван', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '0673333333'),
(398, 1, 2, 1, 'Иванов', 'Иван', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '234234234334'),
(399, 1, 1, 1, 'FirstName', 'FamilyName', 'scan_1416686348.png', NULL, 'vanquisher.ua@gmail.com', 'PhoneNumber'),
(399, 1, 2, 1, '', '', NULL, NULL, '', ''),
(400, 1, 1, 1, 'FirstName', 'FamilyName', 'scan_1416686348.png', NULL, 'vanquisher.ua@gmail.com', 'PhoneNumber'),
(400, 1, 2, 1, '', '', NULL, NULL, '', ''),
(401, 1, 1, 1, 'Dorian', 'Gray', NULL, NULL, 'ddd@dd.com', '380683573536'),
(401, 1, 2, 1, '', '', NULL, NULL, '', ''),
(402, 1, 1, 1, 'Dorian', 'Gray', '', NULL, 'ddd@dd.com', '380683573536'),
(402, 1, 2, 1, '', '', NULL, NULL, '', ''),
(403, 1, 1, 1, '1', '1', NULL, NULL, '1@1.com', '380909909297'),
(403, 1, 2, 1, '', '', NULL, NULL, '', ''),
(404, 1, 1, 1, '1', '1', '', NULL, '1@1.com', '380909909297'),
(405, 1, 1, 1, '11', '11', NULL, NULL, '1@mail.ru', '111'),
(406, 1, 1, 1, '11', '11', '', NULL, '1@mail.ru', '111'),
(407, 1, 1, 1, '11', '11', '', NULL, '1@mail.ru', '111'),
(408, 1, 1, 1, '11', '11', '', NULL, '1@mail.ru', '111'),
(408, 1, 2, 1, '111', '111', '', NULL, '1@mail.ru', '11'),
(409, 1, 1, 1, '§§§', '§§§§', NULL, NULL, '§§§§', '§§§§'),
(409, 1, 2, 1, '§§§', '1', NULL, NULL, '§§', '§'),
(410, 1, 1, 1, '222222', '222222', NULL, NULL, '2@mail.ru', '22222'),
(411, 1, 1, 1, '1', '1', NULL, NULL, '1111@mail.ru', '1'),
(412, 1, 1, 1, '11', '111', '', NULL, '1111@mail.ru', '111'),
(413, 1, 1, 1, '1', '1', '', NULL, '1@mail.ru', '111'),
(414, 1, 1, 1, 'Sergey', 'Suzdaltsev', 'scan_1416202325.jpg', NULL, 'doriangray@ukr.net', '0683573536'),
(414, 1, 2, 1, '', '', NULL, NULL, '', ''),
(415, 1, 1, 1, '1', '1', NULL, NULL, '1"1юсщь', '038947528752'),
(415, 1, 2, 1, '', '', NULL, NULL, '', ''),
(416, 1, 1, 1, 'Иванов', 'Иван', '', NULL, 'weytrwe@hhh.com', '353453453455'),
(416, 1, 2, 1, 'Иванов', 'Иван', NULL, NULL, '', ''),
(417, 1, 1, 1, 'Иван', 'Иванов', '', NULL, 'hunter-dima@ukr.net', '380671111111'),
(417, 1, 2, 1, '', '', NULL, NULL, '', ''),
(417, 2, 1, 1, 'Иван', 'Иванов', '', NULL, 'hunter-dima@ukr.net', '380671111111'),
(417, 2, 2, 1, '', '', NULL, NULL, '', ''),
(417, 3, 1, 1, 'Иван', 'Иванов', '', NULL, 'hunter-dima@ukr.net', '380671111111'),
(417, 3, 2, 1, '', '', NULL, NULL, '', ''),
(418, 1, 1, 1, 'Иван', 'Иванов', '', NULL, 'hunter-dima@ukr.net', '380671111111'),
(418, 1, 2, 1, '', '', NULL, NULL, '', ''),
(419, 1, 1, 1, 'Алекс', 'Рыжков', NULL, NULL, 'hj@him', '2048923848234'),
(419, 1, 2, 1, '', '', NULL, NULL, '', ''),
(419, 2, 1, 1, 'Алекс', 'Рыжков', '', NULL, 'hj@him', '2048923848234'),
(419, 2, 2, 1, '', '', NULL, NULL, '', ''),
(420, 1, 1, 1, 'Алекс', 'Рыжков', '', NULL, 'hj@him', '2048923848234'),
(420, 1, 2, 1, '', '', NULL, NULL, '', ''),
(421, 1, 1, 1, 'Dmytro', 'Sukachev 016261', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '+380675641740'),
(421, 1, 2, 1, '', '', NULL, NULL, '', ''),
(421, 2, 1, 1, 'Dmytro', 'Sukachev 016261', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '+380675641740'),
(421, 2, 2, 1, '', '', NULL, NULL, '', ''),
(421, 3, 1, 1, 'Dmytro', 'Sukachev 016261', 'scan_1417786586.jpg', '', '0675641740ua@gmail.com', '+380675641740'),
(422, 1, 1, 1, 'Dmytro', 'Sukachev 016261', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '+380675641740'),
(422, 1, 2, 1, '', '', NULL, NULL, '', ''),
(423, 1, 1, 1, 'Dmytro', 'Sukachev 016261', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '+380675641740'),
(423, 1, 2, 1, '', '', NULL, NULL, '', ''),
(424, 1, 1, 1, 'Dmytro', 'Sukachev 016261', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '+380675641740'),
(424, 1, 2, 1, '', '', NULL, NULL, '', ''),
(422, 2, 1, 1, 'Dmytro', 'Sukachev 016261', 'scan_1417786586.jpg', '', '0675641740ua@gmail.com', '+380675641740'),
(417, 4, 1, 1, 'Иван', 'Иванов', '', '', 'hunter-dima@ukr.net', '380671111111'),
(419, 3, 1, 1, 'Алекс', 'Рыжков', '', '', 'hj@him', '2048923848234'),
(417, 5, 1, 1, 'Иван', 'Иванов', '', '', 'hunter-dima@ukr.net', '380671111111'),
(424, 2, 1, 1, 'Dmytro', 'Sukachev 016261', 'scan_1417786586.jpg', '', '0675641740ua@gmail.com', '+380675641740'),
(425, 1, 1, 1, 'Dim', 'Hunter', '', NULL, 'hunter-dima@ukr.net', '0671234567'),
(425, 1, 2, 1, '', '', NULL, NULL, '', ''),
(426, 1, 1, 1, 'Sergey', 'Suzdaltsev', 'scan_1416202325.jpg', NULL, 'doriangray@ukr.net', '380683573536'),
(426, 1, 2, 1, '', '', NULL, NULL, '', ''),
(427, 1, 1, 1, 'Dmytro', 'Sukachev 016261', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '+380675641740'),
(427, 1, 2, 1, '', '', NULL, NULL, '', ''),
(428, 1, 1, 1, 'Sergey', 'Suzdaltsev', 'scan_1416202325.jpg', NULL, 'doriangray@ukr.net', '380683573536'),
(428, 1, 2, 1, '', '', NULL, NULL, '', ''),
(429, 1, 1, 1, 'Sergey', 'Suzdaltsev', 'scan_1416202325.jpg', NULL, 'doriangray@ukr.net', '380683573536'),
(429, 1, 2, 1, '', '', NULL, NULL, '', ''),
(430, 1, 1, 1, 'Иванов', 'Иван', '', NULL, 'weytrwe@hhh.com', '234923890482'),
(430, 1, 2, 1, '', '', NULL, NULL, '', ''),
(430, 2, 1, 1, 'Иванов', 'Иван', '', '', 'weytrwe@hhh.com', '234923890482'),
(431, 1, 1, 1, 'Иванов', 'Иван', '', NULL, 'weytrwe@hhh.com', '2342342342'),
(431, 1, 2, 1, '', '', NULL, NULL, '', ''),
(432, 1, 1, 1, 'asd', 'asd', NULL, NULL, 'asd', '123123123'),
(432, 1, 2, 1, 'asdf', 'asd', '', NULL, 'asdasd', '123123123'),
(433, 1, 1, 1, 'Сергей', 'mort', '', NULL, 'megbrimef@gmail.com', '0632893585'),
(433, 1, 2, 1, '', '', NULL, NULL, '', ''),
(434, 1, 1, 1, 'Сергей', 'mort', NULL, NULL, 'asd@dd.dd', '13123123'),
(434, 1, 2, 1, '', '', NULL, NULL, '', ''),
(435, 1, 1, 1, 'Сергей', 'mort', '', NULL, 'asd@dd.dd', '13123123'),
(435, 1, 2, 1, '', '', NULL, NULL, '', ''),
(436, 1, 1, 1, 'фвф', 'фыв', '', NULL, 'asd@dd.dd', '123123'),
(436, 1, 2, 1, 'asd', 'adsg', NULL, NULL, 'qaqa@mail.ru', '123123'),
(433, 2, 1, 1, 'Сергей', 'mort', '', '', 'megbrimef@gmail.com', '0632893585'),
(433, 3, 1, 1, 'Сергей', 'mort', '', '', 'megbrimef@gmail.com', '0632893585'),
(433, 4, 1, 1, 'Сергей', 'mort', '', '', 'megbrimef@gmail.com', '0632893585'),
(433, 5, 1, 1, 'Сергей', 'mort', '', '', 'megbrimef@gmail.com', '0632893585'),
(433, 6, 1, 1, 'Сергей', 'mort', '', '', 'megbrimef@gmail.com', '0632893585'),
(433, 7, 1, 1, 'Сергей', 'mort', '', '', 'megbrimef@gmail.com', '0632893585'),
(437, 1, 1, 1, 'asddfas', 'dafsdafs', NULL, NULL, 'adsfadfs', '1321231232'),
(437, 1, 2, 1, 'dafsdafs', 'dfasdafs', NULL, NULL, 'dasadfs', '12312312'),
(437, 2, 1, 1, 'asddfas', 'dafsdafs', '', '', 'adsfadfs', '1321231232'),
(437, 2, 2, 1, 'dafsdafs', 'dfasdafs', '', '', 'dasadfs', '12312312'),
(438, 1, 1, 1, 'Иванов', 'Иван', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '2342342342'),
(438, 1, 2, 1, '', '', NULL, NULL, '', ''),
(439, 1, 1, 1, 'Sergey', 'Suzdaltsev', 'scan_1416202325.jpg', NULL, 'doriangray@ukr.net', '380683573536'),
(439, 1, 2, 1, '', '', NULL, NULL, '', ''),
(440, 1, 1, 1, 'Sergey', 'Suzdaltsev', NULL, NULL, 'ddddd@ddddd.com', '380683573536'),
(440, 1, 2, 1, '', '', NULL, NULL, '', ''),
(441, 1, 1, 1, 'Иванов', 'Иван', 'scan_1417786586.jpg', NULL, '0675641740ua@gmail.com', '2342342342'),
(441, 1, 2, 1, '', '', NULL, NULL, '', ''),
(441, 1, 1, 2, '', '', NULL, NULL, NULL, NULL),
(442, 1, 1, 1, 'Sergey', 'Suzdaltsev', 'scan_1416202325.jpg', NULL, 'doriangray@ukr.net', '380683573536'),
(442, 1, 2, 1, '', '', NULL, NULL, '', ''),
(443, 1, 1, 1, 'Иванов', 'Иван', '', NULL, 'weytrwe@hhh.com', '2342342342'),
(443, 1, 2, 1, 'Иванна', 'Иванова', NULL, NULL, 'uihuiiij@gtg.com', '2342342341');

-- --------------------------------------------------------

--
-- Table structure for table `tst_hotel_booking_payment_history`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_booking_payment_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` int(11) NOT NULL,
  `payment_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `subject` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL,
  `amount` float(10,2) NOT NULL,
  `pay_way` int(11) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hotel_booking_deposit_history_booking_id` (`booking_id`),
  KEY `fk_hotel_booking_deposit_history_user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='фин история бронирования' AUTO_INCREMENT=83 ;

--
-- Dumping data for table `tst_hotel_booking_payment_history`
--

INSERT INTO `tst_hotel_booking_payment_history` (`id`, `booking_id`, `payment_id`, `user_id`, `date`, `type`, `subject`, `status`, `amount`, `pay_way`, `description`) VALUES
(24, 183, NULL, 18, 1414771082, 1, 1, 1, 100.00, 4, 'за первую ночь'),
(25, 179, NULL, 18, 1416082232, 1, 2, 1, 390.00, 1, ''),
(26, 231, NULL, 18, 1419867335, 1, 1, 1, 1.10, 1, ''),
(27, 231, NULL, 18, 1419867342, 1, 2, 1, 0.55, 1, ''),
(28, 240, NULL, 31, 1422037768, 1, 1, 1, 40.00, 1, 'something'),
(29, 240, NULL, 31, 1422037779, 1, 1, 1, 40.00, 1, 'something'),
(30, 240, NULL, 31, 1422037805, 1, 1, 1, 40.00, 1, 'something'),
(31, 240, NULL, 31, 1422037806, 1, 1, 1, 40.00, 1, 'something'),
(32, 240, NULL, 31, 1422037850, 1, 3, 1, 75.00, 1, 'something'),
(33, 241, NULL, 18, 1422303086, 1, 2, 1, 0.53, 1, ''),
(34, 246, NULL, 49, 1422357961, 1, 3, 1, 63.38, 1, 'something'),
(35, 245, NULL, 45, 1422358830, 1, 2, 1, 0.07, 1, 'something'),
(36, 245, NULL, 49, 1422358901, 1, 3, 1, 62.50, 1, 'something'),
(37, 228, NULL, 49, 1422359097, 1, 3, 1, 150.00, 1, 'something'),
(38, 224, NULL, 49, 1422359196, 1, 2, 1, 40.00, 1, 'something'),
(39, 224, NULL, 49, 1422359240, 1, 1, 1, 100.00, 1, 'something'),
(40, 244, NULL, 60, 1424354456, 1, 3, 1, 8.66, 1, 'something'),
(43, 279, 5, 31, 1426249109, 1, 1, 1, 0.10, 6, 'Первая ночь (Предоплата за первую ночь) [S:1;U:31]'),
(44, 280, 6, 20, 1426259128, 1, 1, 1, 0.09, 6, 'Первая ночь (Предоплата за первую ночь) [S:1;U:20]'),
(46, 281, 8, 67, 1426528541, 1, 1, 1, 0.10, 6, 'Первая ночь (Предоплата за первую ночь)  [S:1;U:67]'),
(48, 286, 10, 49, 1427365149, 1, 3, 1, 1.15, 6, 'Общая стоимость (Предоплата за первую ночь) [S:3;U:49]'),
(49, 288, 11, 49, 1427365662, 1, 3, 1, 1.15, 6, 'Общая стоимость (Предоплата за первую ночь) [S:3;U:49]'),
(50, 296, 12, 73, 1428670146, 1, 1, 1, 0.10, 6, 'allResidence_en (All period prepayment) [S:1;U:73]'),
(51, 298, 13, 73, 1428671673, 1, 1, 1, 0.10, 6, 'Весь период (Предоплата за весь период проживания) [S:1;U:73]'),
(52, 299, 14, 20, 1428673139, 1, 1, 1, 0.09, 6, 'Весь период (Предоплата за весь период проживания) [S:1;U:20]'),
(53, 306, 15, 20, 1428681386, 1, 1, 1, 0.09, 6, 'Весь период (Предоплата за весь период проживания) [S:1;U:20]'),
(55, 318, 17, 59, 1430490376, 1, 3, 1, 115.00, 6, 'totalCost_en (First night prepayment)  [S:3;U:59]'),
(56, 328, 18, 49, 1431024816, 1, 1, 1, 0.00, 6, 'Весь период (Предоплата за весь период проживания) [S:1;U:49]'),
(58, 333, 20, 83, 1431811168, 1, 1, 1, 0.10, 6, 'Весь период (Предоплата за весь период проживания)  [S:1;U:83]'),
(59, 334, 21, 20, 1431813609, 1, 1, 1, 0.09, 6, 'Весь период (Предоплата за весь период проживания) [S:1;U:20]'),
(62, 338, 24, 85, 1433940258, 1, 1, 1, 0.10, 6, 'Весь период (Предоплата за весь период проживания) [S:1;U:85]'),
(63, 339, 25, 49, 1434177112, 1, 2, 1, 0.02, 6, 'Услуги (Предоплата за весь период проживания) [S:2;U:49]'),
(64, 339, 26, 49, 1434179178, 1, 3, 1, 0.00, 6, 'Общая стоимость (Предоплата за весь период проживания) [S:3;U:49]'),
(65, 342, 27, 86, 1434182893, 1, 1, 1, 0.10, 6, 'Весь период (Предоплата за весь период проживания)  [S:1;U:86]'),
(72, 350, 34, 49, 1434568867, 1, 1, 1, 0.00, 6, 'Весь период (Предоплата за весь период проживания) [S:1;U:49]'),
(73, 366, 35, 20, 1436369373, 1, 1, 1, 0.09, 6, 'Весь период (Предоплата за весь период проживания) [S:1;U:20]'),
(74, 373, 36, 96, 1437233631, 1, 1, 1, 0.40, 6, 'Весь период (Предоплата за весь период проживания) [S:1;U:96]'),
(75, 374, 37, 96, 1437295759, 1, 1, 1, 0.30, 6, 'Весь период (Предоплата за весь период проживания) [S:1;U:96]'),
(76, 375, NULL, 18, 1437849188, 2, 1, 1, 0.00, 1, ''),
(77, 375, NULL, 18, 1437849194, 2, 1, 1, 0.00, 1, ''),
(78, 375, NULL, 18, 1437849214, 2, 1, 1, 0.00, 1, ''),
(79, 377, 38, 20, 1440624759, 1, 1, 1, 0.63, 6, 'Весь период (Предоплата за весь период проживания) [S:1;U:20]'),
(80, 417, NULL, 18, 1461502181, 1, 3, 1, 1000.00, 1, ''),
(81, 417, NULL, 18, 1461502859, 2, 2, 1, 878.00, 1, ''),
(82, 424, NULL, 18, 1462956363, 1, 3, 1, 1000.00, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `tst_hotel_booking_services`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_booking_services` (
  `booking_id` int(11) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  `service_id` int(11) NOT NULL,
  `guest_num` int(11) NOT NULL,
  `guest_type` int(11) NOT NULL,
  `count` int(11) NOT NULL DEFAULT '1',
  `cost` float(10,2) NOT NULL DEFAULT '0.00',
  KEY `fk_hotel_booking_services_service_id` (`service_id`),
  KEY `fk_hotel_booking_services_booking_id` (`booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tst_hotel_booking_services`
--

INSERT INTO `tst_hotel_booking_services` (`booking_id`, `version`, `service_id`, `guest_num`, `guest_type`, `count`, `cost`) VALUES
(187, 1, 5, 1, 1, 5, 12.00),
(187, 1, 5, 1, 2, 5, 0.00),
(185, 1, 5, 1, 1, 3, 12.00),
(185, 1, 5, 2, 1, 3, 12.00),
(185, 1, 6, 1, 1, 3, 23.00),
(188, 1, 5, 1, 1, 1, 12.00),
(189, 1, 5, 1, 1, 1, 0.78),
(189, 1, 5, 2, 1, 1, 0.78),
(179, 1, 6, 1, 1, 3, 23.00),
(179, 1, 6, 1, 2, 3, 11.50),
(179, 1, 8, 1, 1, 3, 46.00),
(179, 1, 8, 1, 2, 3, 23.00),
(192, 1, 5, 1, 1, 4, 0.78),
(193, 1, 6, 1, 1, 4, 1.49),
(193, 1, 8, 1, 1, 4, 2.99),
(199, 1, 5, 1, 1, 3, 12.00),
(199, 1, 5, 2, 1, 3, 12.00),
(199, 1, 6, 1, 1, 8, 23.00),
(199, 1, 6, 2, 1, 8, 23.00),
(199, 1, 8, 1, 1, 8, 46.00),
(199, 1, 8, 2, 1, 8, 46.00),
(201, 1, 5, 1, 1, 7, 12.00),
(201, 1, 5, 2, 1, 7, 12.00),
(201, 1, 5, 1, 2, 7, 6.00),
(201, 1, 6, 1, 1, 7, 23.00),
(201, 1, 6, 2, 1, 7, 23.00),
(201, 1, 6, 1, 2, 7, 11.50),
(201, 1, 8, 1, 1, 7, 46.00),
(201, 1, 8, 2, 1, 7, 46.00),
(201, 1, 8, 1, 2, 7, 23.00),
(202, 1, 5, 1, 1, 4, 12.00),
(202, 1, 6, 1, 1, 4, 23.00),
(202, 1, 8, 1, 1, 4, 46.00),
(203, 1, 5, 1, 1, 1, 12.00),
(203, 1, 6, 1, 1, 1, 23.00),
(203, 1, 8, 1, 1, 1, 46.00),
(204, 1, 8, 1, 1, 1, 46.00),
(204, 1, 8, 2, 1, 2, 46.00),
(204, 1, 6, 1, 1, 1, 23.00),
(204, 1, 6, 2, 1, 2, 23.00),
(208, 1, 5, 1, 1, 1, 5.00),
(208, 1, 5, 2, 1, 1, 5.00),
(210, 1, 5, 1, 1, 17, 5.00),
(210, 1, 6, 1, 1, 17, 23.00),
(212, 1, 5, 1, 1, 1, 5.00),
(212, 1, 5, 2, 1, 1, 5.00),
(212, 1, 6, 1, 1, 1, 23.00),
(212, 1, 6, 2, 1, 1, 23.00),
(212, 1, 8, 1, 1, 1, 46.00),
(212, 1, 8, 2, 1, 1, 46.00),
(212, 1, 13, 1, 1, 1, 77.00),
(212, 1, 13, 2, 1, 1, 77.00),
(214, 1, 6, 1, 1, 3, 23.00),
(214, 1, 6, 2, 1, 3, 23.00),
(214, 1, 6, 1, 2, 3, 11.50),
(214, 1, 13, 1, 1, 3, 77.00),
(214, 1, 13, 2, 1, 3, 77.00),
(214, 1, 13, 1, 2, 3, 38.50),
(216, 1, 5, 1, 1, 6, 8.00),
(216, 1, 6, 1, 1, 6, 10.00),
(227, 1, 8, 1, 1, 1, 46.00),
(227, 1, 8, 2, 1, 1, 46.00),
(227, 1, 8, 1, 2, 1, 23.00),
(231, 1, 5, 1, 1, 1, 12.00),
(231, 1, 6, 1, 1, 1, 23.00),
(237, 1, 5, 1, 1, 1, 501.00),
(230, 1, 5, 1, 1, 1, 12.00),
(239, 1, 5, 1, 1, 1, 1002.00),
(239, 1, 5, 2, 1, 1, 1002.00),
(239, 1, 6, 1, 1, 1, 1004.00),
(239, 1, 6, 2, 1, 1, 1004.00),
(239, 1, 8, 1, 1, 1, 1008.00),
(239, 1, 8, 2, 1, 1, 1008.00),
(240, 1, 6, 1, 1, 5, 10.00),
(244, 1, 5, 1, 1, 1, 12.00),
(245, 1, 13, 1, 1, 1, 7.00),
(244, 2, 5, 1, 1, 1, 12.00),
(244, 2, 6, 1, 1, 1, 2.00),
(248, 1, 5, 1, 1, 1, 0.10),
(248, 1, 6, 1, 1, 1, 0.20),
(245, 2, 8, 1, 1, 1, 4.00),
(245, 2, 13, 1, 1, 1, 7.00),
(245, 3, 13, 1, 1, 1, 7.00),
(245, 3, 13, 2, 1, 1, 7.00),
(245, 3, 13, 1, 2, 1, 3.50),
(249, 1, 5, 1, 1, 4, 0.10),
(249, 1, 5, 2, 1, 4, 0.10),
(249, 1, 6, 1, 1, 4, 0.20),
(249, 1, 6, 2, 1, 4, 0.20),
(249, 2, 5, 1, 1, 2, 0.10),
(249, 2, 5, 2, 1, 3, 0.10),
(249, 2, 6, 1, 1, 3, 0.20),
(249, 2, 6, 2, 1, 3, 0.20),
(254, 3, 8, 1, 1, 1, 0.40),
(254, 3, 13, 1, 1, 1, 0.70),
(254, 4, 8, 1, 1, 1, 0.40),
(254, 4, 13, 1, 1, 1, 0.70),
(273, 1, 6, 1, 1, 1, 0.20),
(273, 1, 6, 2, 1, 1, 0.20),
(273, 2, 6, 1, 1, 1, 0.20),
(273, 2, 6, 2, 1, 1, 0.20),
(277, 1, 5, 1, 1, 1, 12.00),
(277, 1, 5, 2, 1, 1, 12.00),
(277, 1, 6, 1, 1, 1, 23.00),
(277, 1, 6, 2, 1, 1, 23.00),
(290, 1, 5, 1, 1, 1, 12.00),
(290, 1, 5, 2, 1, 1, 12.00),
(290, 1, 6, 1, 1, 1, 23.00),
(290, 1, 6, 2, 1, 1, 23.00),
(290, 1, 8, 1, 1, 1, 46.00),
(290, 1, 8, 2, 1, 1, 46.00),
(290, 1, 13, 1, 1, 1, 77.00),
(290, 1, 13, 2, 1, 1, 77.00),
(290, 1, 16, 1, 1, 1, 0.00),
(290, 1, 16, 2, 1, 1, 0.00),
(298, 2, 16, 1, 1, 1, 1.00),
(298, 2, 16, 2, 1, 1, 1.00),
(299, 2, 16, 1, 1, 1, 1.00),
(299, 2, 16, 2, 1, 1, 1.00),
(299, 3, 16, 1, 1, 1, 1.00),
(299, 3, 16, 2, 1, 1, 1.00),
(299, 4, 16, 1, 1, 1, 1.00),
(299, 4, 16, 2, 1, 1, 1.00),
(299, 5, 16, 1, 1, 1, 1.00),
(299, 5, 16, 2, 1, 1, 1.00),
(302, 1, 16, 1, 1, 1, 1.00),
(302, 1, 16, 2, 1, 1, 1.00),
(306, 1, 5, 1, 1, 1, 12.00),
(306, 1, 5, 2, 1, 1, 12.00),
(306, 1, 6, 1, 1, 1, 23.00),
(306, 1, 6, 2, 1, 1, 23.00),
(306, 1, 8, 1, 1, 1, 46.00),
(306, 1, 8, 2, 1, 1, 46.00),
(306, 1, 13, 1, 1, 1, 77.00),
(306, 1, 13, 2, 1, 1, 77.00),
(306, 1, 16, 1, 1, 1, 1.00),
(306, 1, 16, 2, 1, 1, 1.00),
(309, 1, 5, 1, 1, 1, 12.00),
(309, 1, 5, 2, 1, 1, 12.00),
(309, 1, 6, 1, 1, 1, 23.00),
(309, 1, 6, 2, 1, 1, 23.00),
(309, 1, 8, 1, 1, 1, 46.00),
(309, 1, 8, 2, 1, 1, 46.00),
(309, 1, 13, 1, 1, 1, 77.00),
(309, 1, 13, 2, 1, 1, 77.00),
(309, 1, 16, 1, 1, 1, 1.00),
(309, 1, 16, 2, 1, 1, 1.00),
(324, 1, 5, 1, 1, 1, 12.00),
(324, 1, 6, 1, 1, 1, 23.00),
(324, 1, 8, 1, 1, 1, 46.00),
(324, 1, 13, 1, 1, 1, 77.00),
(324, 1, 16, 1, 1, 1, 1.00),
(331, 2, 6, 1, 1, 1, 23.00),
(331, 2, 6, 2, 1, 1, 23.00),
(331, 2, 16, 1, 1, 1, 1.00),
(331, 2, 16, 2, 1, 1, 1.00),
(331, 3, 6, 1, 1, 1, 23.00),
(331, 3, 6, 2, 1, 1, 23.00),
(331, 3, 16, 1, 1, 1, 1.00),
(331, 3, 16, 2, 1, 1, 1.00),
(333, 1, 16, 1, 1, 1, 1.00),
(333, 1, 16, 2, 1, 1, 1.00),
(334, 1, 5, 1, 1, 1, 12.00),
(334, 1, 5, 2, 1, 1, 12.00),
(334, 1, 6, 1, 1, 1, 23.00),
(334, 1, 6, 2, 1, 1, 23.00),
(334, 1, 8, 1, 1, 1, 46.00),
(334, 1, 8, 2, 1, 1, 46.00),
(334, 1, 13, 1, 1, 1, 77.00),
(334, 1, 13, 2, 1, 1, 77.00),
(334, 1, 16, 1, 1, 1, 1.00),
(334, 1, 16, 2, 1, 1, 1.00),
(334, 2, 5, 1, 1, 1, 12.00),
(334, 2, 5, 2, 1, 1, 12.00),
(334, 2, 6, 1, 1, 1, 23.00),
(334, 2, 6, 2, 1, 1, 23.00),
(334, 2, 8, 1, 1, 1, 46.00),
(334, 2, 8, 2, 1, 1, 46.00),
(334, 2, 13, 1, 1, 1, 77.00),
(334, 2, 13, 2, 1, 1, 77.00),
(334, 2, 16, 1, 1, 1, 1.00),
(334, 2, 16, 2, 1, 1, 1.00),
(335, 1, 5, 1, 1, 5, 12.00),
(335, 1, 5, 2, 1, 5, 12.00),
(335, 2, 5, 1, 1, 5, 12.00),
(335, 2, 5, 2, 1, 5, 12.00),
(335, 3, 5, 1, 1, 5, 12.00),
(335, 3, 5, 2, 1, 5, 12.00),
(337, 1, 5, 1, 1, 1, 12.00),
(337, 1, 5, 2, 1, 1, 12.00),
(337, 1, 8, 1, 1, 1, 46.00),
(337, 1, 8, 2, 1, 1, 46.00),
(339, 1, 16, 1, 1, 1, 1.00),
(339, 1, 16, 2, 1, 1, 1.00),
(357, 1, 5, 1, 1, 1, 12.00),
(357, 1, 5, 2, 1, 1, 12.00),
(357, 1, 6, 1, 1, 1, 23.00),
(357, 1, 6, 2, 1, 1, 23.00),
(357, 1, 8, 1, 1, 1, 46.00),
(357, 1, 8, 2, 1, 1, 46.00),
(357, 1, 13, 1, 1, 1, 77.00),
(357, 1, 13, 2, 1, 1, 77.00),
(357, 1, 16, 1, 1, 1, 1.00),
(357, 1, 16, 2, 1, 1, 1.00),
(361, 1, 5, 1, 1, 1, 12.00),
(361, 1, 5, 2, 1, 1, 12.00),
(361, 1, 6, 1, 1, 1, 23.00),
(361, 1, 6, 2, 1, 1, 23.00),
(361, 1, 16, 1, 1, 1, 1.00),
(361, 1, 16, 2, 1, 1, 1.00),
(366, 1, 13, 1, 1, 1, 77.00),
(366, 1, 13, 2, 1, 1, 77.00),
(366, 2, 13, 1, 1, 1, 77.00),
(366, 2, 13, 2, 1, 1, 77.00),
(366, 3, 13, 1, 1, 1, 77.00),
(366, 3, 13, 2, 1, 1, 77.00),
(367, 1, 16, 1, 1, 1, 1.00),
(367, 1, 16, 2, 1, 1, 1.00),
(369, 1, 13, 1, 1, 4, 77.00),
(369, 1, 13, 2, 1, 4, 77.00),
(372, 1, 5, 1, 1, 1, 12.00),
(372, 1, 5, 2, 1, 1, 12.00),
(372, 1, 6, 1, 1, 1, 23.00),
(372, 1, 6, 2, 1, 1, 23.00),
(372, 1, 8, 1, 1, 1, 46.00),
(372, 1, 8, 2, 1, 1, 46.00),
(372, 1, 13, 1, 1, 1, 77.00),
(372, 1, 13, 2, 1, 1, 77.00),
(372, 1, 16, 1, 1, 1, 1.00),
(372, 1, 16, 2, 1, 1, 1.00),
(372, 2, 5, 1, 1, 1, 12.00),
(372, 2, 5, 2, 1, 1, 12.00),
(372, 2, 6, 1, 1, 1, 23.00),
(372, 2, 6, 2, 1, 1, 23.00),
(372, 2, 8, 1, 1, 1, 46.00),
(372, 2, 8, 2, 1, 1, 46.00),
(372, 2, 13, 1, 1, 1, 77.00),
(372, 2, 13, 2, 1, 1, 77.00),
(372, 2, 16, 1, 1, 1, 1.00),
(372, 2, 16, 2, 1, 1, 1.00),
(372, 3, 5, 1, 1, 1, 12.00),
(372, 3, 5, 2, 1, 1, 12.00),
(372, 3, 6, 1, 1, 1, 23.00),
(372, 3, 6, 2, 1, 1, 23.00),
(372, 3, 8, 1, 1, 1, 46.00),
(372, 3, 8, 2, 1, 1, 46.00),
(372, 3, 13, 1, 1, 1, 77.00),
(372, 3, 13, 2, 1, 1, 77.00),
(372, 3, 16, 1, 1, 1, 1.00),
(372, 3, 16, 2, 1, 1, 1.00),
(372, 4, 5, 1, 1, 1, 12.00),
(372, 4, 5, 2, 1, 1, 12.00),
(372, 4, 6, 1, 1, 1, 23.00),
(372, 4, 6, 2, 1, 1, 23.00),
(372, 4, 8, 1, 1, 1, 46.00),
(372, 4, 8, 2, 1, 1, 46.00),
(372, 4, 13, 1, 1, 1, 77.00),
(372, 4, 13, 2, 1, 1, 77.00),
(372, 4, 16, 1, 1, 1, 1.00),
(372, 5, 5, 1, 1, 1, 12.00),
(372, 5, 5, 2, 1, 1, 12.00),
(372, 5, 6, 1, 1, 1, 23.00),
(372, 5, 6, 2, 1, 1, 23.00),
(372, 5, 8, 1, 1, 1, 46.00),
(372, 5, 8, 2, 1, 1, 46.00),
(372, 5, 13, 1, 1, 1, 77.00),
(372, 5, 13, 2, 1, 1, 77.00),
(372, 5, 16, 1, 1, 1, 1.00),
(372, 6, 5, 1, 1, 1, 12.00),
(372, 6, 5, 2, 1, 1, 12.00),
(372, 6, 6, 1, 1, 1, 23.00),
(372, 6, 6, 2, 1, 1, 23.00),
(372, 6, 8, 1, 1, 1, 46.00),
(372, 6, 8, 2, 1, 1, 46.00),
(372, 6, 13, 1, 1, 1, 77.00),
(372, 6, 16, 1, 1, 1, 1.00),
(374, 1, 5, 1, 1, 3, 12.00),
(374, 1, 5, 2, 1, 3, 12.00),
(374, 1, 5, 1, 2, 3, 6.00),
(374, 1, 6, 1, 1, 3, 23.00),
(374, 1, 6, 2, 1, 3, 23.00),
(374, 1, 6, 1, 2, 3, 11.50),
(374, 1, 8, 1, 1, 3, 46.00),
(374, 1, 8, 2, 1, 3, 46.00),
(374, 1, 8, 1, 2, 3, 23.00),
(374, 1, 13, 1, 1, 3, 77.00),
(374, 1, 13, 2, 1, 3, 77.00),
(374, 1, 13, 1, 2, 3, 38.50),
(374, 1, 16, 1, 1, 3, 1.00),
(374, 1, 16, 2, 1, 3, 1.00),
(374, 1, 16, 1, 2, 3, 0.50),
(374, 2, 5, 1, 1, 3, 12.00),
(374, 2, 5, 2, 1, 3, 12.00),
(374, 2, 5, 1, 2, 3, 6.00),
(374, 2, 6, 1, 1, 3, 23.00),
(374, 2, 6, 2, 1, 3, 23.00),
(374, 2, 6, 1, 2, 3, 11.50),
(374, 2, 8, 1, 1, 3, 46.00),
(374, 2, 8, 2, 1, 3, 46.00),
(374, 2, 8, 1, 2, 3, 23.00),
(374, 2, 13, 1, 1, 3, 77.00),
(374, 2, 13, 2, 1, 3, 77.00),
(374, 2, 13, 1, 2, 3, 38.50),
(374, 2, 16, 1, 1, 3, 1.00),
(374, 2, 16, 2, 1, 3, 1.00),
(374, 2, 16, 1, 2, 3, 0.50),
(375, 1, 6, 1, 1, 1, 23.00),
(375, 1, 6, 2, 1, 1, 23.00),
(375, 1, 16, 1, 1, 1, 1.00),
(375, 1, 16, 2, 1, 1, 1.00),
(376, 1, 5, 1, 1, 1, 12.00),
(376, 1, 5, 2, 1, 1, 12.00),
(376, 1, 6, 1, 1, 1, 23.00),
(376, 1, 6, 2, 1, 1, 23.00),
(376, 2, 5, 1, 1, 1, 12.00),
(376, 2, 5, 2, 1, 1, 12.00),
(376, 2, 6, 1, 1, 1, 23.00),
(376, 2, 6, 2, 1, 1, 23.00),
(335, 4, 5, 1, 1, 5, 12.00),
(335, 4, 5, 2, 1, 5, 12.00),
(335, 4, 8, 1, 1, 5, 46.00),
(335, 4, 8, 2, 1, 5, 46.00),
(372, 7, 5, 1, 1, 1, 12.00),
(372, 7, 5, 2, 1, 1, 12.00),
(372, 7, 6, 1, 1, 1, 23.00),
(372, 7, 6, 2, 1, 1, 23.00),
(372, 7, 16, 1, 1, 1, 1.00),
(372, 8, 5, 1, 1, 1, 12.00),
(372, 8, 5, 2, 1, 1, 12.00),
(372, 8, 6, 1, 1, 1, 23.00),
(372, 8, 6, 2, 1, 1, 23.00),
(372, 8, 8, 1, 1, 3, 46.00),
(372, 8, 8, 2, 1, 3, 46.00),
(372, 8, 13, 1, 1, 3, 77.00),
(372, 8, 13, 2, 1, 3, 77.00),
(372, 8, 16, 1, 1, 1, 1.00),
(377, 1, 5, 1, 1, 7, 12.00),
(377, 1, 5, 2, 1, 7, 12.00),
(377, 1, 5, 1, 2, 7, 6.00),
(377, 1, 6, 1, 1, 6, 23.00),
(377, 1, 6, 2, 1, 6, 23.00),
(377, 1, 8, 1, 1, 5, 46.00),
(377, 1, 8, 1, 2, 5, 23.00),
(377, 1, 13, 1, 1, 4, 77.00),
(377, 1, 16, 1, 1, 3, 1.00),
(377, 1, 16, 2, 1, 3, 1.00),
(377, 1, 16, 1, 2, 3, 0.50),
(382, 1, 5, 1, 1, 3, 12.00),
(382, 1, 5, 2, 1, 3, 12.00),
(382, 1, 5, 1, 2, 3, 6.00),
(382, 1, 6, 1, 1, 3, 23.00),
(382, 1, 6, 2, 1, 3, 23.00),
(382, 1, 6, 1, 2, 3, 11.50),
(382, 1, 8, 1, 1, 3, 46.00),
(382, 1, 8, 2, 1, 3, 46.00),
(382, 1, 8, 1, 2, 3, 23.00),
(382, 1, 13, 1, 1, 3, 77.00),
(382, 1, 13, 2, 1, 3, 77.00),
(382, 1, 13, 1, 2, 3, 38.50),
(382, 1, 16, 1, 1, 3, 1.00),
(382, 1, 16, 2, 1, 3, 1.00),
(382, 1, 16, 1, 2, 3, 0.50),
(388, 1, 5, 1, 1, 6, 12.00),
(388, 1, 5, 2, 1, 6, 12.00),
(388, 1, 5, 1, 2, 6, 6.00),
(388, 1, 6, 1, 1, 6, 23.00),
(388, 1, 6, 2, 1, 6, 23.00),
(388, 1, 8, 1, 1, 2, 46.00),
(388, 1, 13, 1, 1, 5, 77.00),
(388, 1, 13, 1, 2, 5, 38.50),
(388, 1, 16, 1, 1, 2, 0.00),
(388, 1, 16, 2, 1, 2, 0.00),
(388, 1, 16, 1, 2, 2, 0.00),
(390, 1, 5, 1, 1, 1, 12.00),
(390, 1, 5, 2, 1, 1, 12.00),
(390, 1, 5, 1, 2, 1, 6.00),
(390, 1, 6, 1, 1, 2, 23.00),
(390, 1, 6, 2, 1, 2, 23.00),
(390, 1, 8, 1, 1, 3, 46.00),
(390, 1, 8, 1, 2, 3, 23.00),
(390, 1, 13, 1, 1, 4, 77.00),
(390, 1, 16, 1, 1, 6, 0.00),
(390, 1, 16, 2, 1, 6, 0.00),
(390, 1, 16, 1, 2, 6, 0.00),
(369, 2, 13, 1, 1, 4, 77.00),
(369, 2, 13, 2, 1, 4, 77.00),
(392, 1, 8, 1, 1, 1, 46.00),
(392, 1, 8, 2, 1, 1, 46.00),
(392, 1, 13, 1, 1, 1, 77.00),
(392, 1, 13, 2, 1, 1, 77.00),
(392, 1, 16, 1, 1, 1, 0.00),
(392, 1, 16, 2, 1, 1, 0.00),
(393, 1, 5, 1, 1, 10, 12.00),
(393, 1, 5, 2, 1, 10, 12.00),
(393, 1, 6, 1, 1, 10, 23.00),
(393, 1, 6, 2, 1, 10, 23.00),
(393, 1, 8, 1, 1, 10, 46.00),
(393, 1, 8, 2, 1, 10, 46.00),
(393, 1, 13, 1, 1, 10, 77.00),
(393, 1, 13, 2, 1, 10, 77.00),
(398, 1, 5, 1, 1, 5, 12.00),
(398, 1, 5, 2, 1, 5, 12.00),
(398, 1, 6, 1, 1, 5, 23.00),
(398, 1, 6, 2, 1, 5, 23.00),
(398, 1, 8, 1, 1, 5, 46.00),
(398, 1, 8, 2, 1, 5, 46.00),
(398, 1, 13, 1, 1, 5, 77.00),
(398, 1, 13, 2, 1, 5, 77.00),
(398, 1, 16, 1, 1, 5, 0.00),
(398, 1, 16, 2, 1, 5, 0.00),
(399, 1, 5, 1, 1, 1, 12.00),
(399, 1, 5, 2, 1, 1, 12.00),
(399, 1, 6, 1, 1, 1, 23.00),
(399, 1, 6, 2, 1, 1, 23.00),
(399, 1, 8, 1, 1, 1, 46.00),
(399, 1, 8, 2, 1, 1, 46.00),
(399, 1, 13, 1, 1, 1, 77.00),
(399, 1, 13, 2, 1, 1, 77.00),
(399, 1, 16, 1, 1, 1, 0.00),
(399, 1, 16, 2, 1, 1, 0.00),
(401, 1, 5, 1, 1, 1, 12.00),
(401, 1, 5, 2, 1, 1, 12.00),
(401, 1, 6, 1, 1, 1, 23.00),
(401, 1, 6, 2, 1, 1, 23.00),
(403, 1, 5, 1, 1, 1, 12.00),
(403, 1, 5, 2, 1, 1, 12.00),
(403, 1, 6, 1, 1, 1, 23.00),
(403, 1, 6, 2, 1, 1, 23.00),
(403, 1, 8, 1, 1, 1, 46.00),
(403, 1, 8, 2, 1, 1, 46.00),
(403, 1, 13, 1, 1, 1, 77.00),
(403, 1, 13, 2, 1, 1, 77.00),
(403, 1, 16, 1, 1, 1, 0.00),
(403, 1, 16, 2, 1, 1, 0.00),
(404, 1, 5, 1, 1, 1, 12.00),
(404, 1, 6, 1, 1, 1, 23.00),
(404, 1, 8, 1, 1, 1, 46.00),
(404, 1, 13, 1, 1, 1, 77.00),
(404, 1, 16, 1, 1, 1, 0.00),
(405, 1, 5, 1, 1, 1, 12.00),
(406, 1, 5, 1, 1, 1, 12.00),
(407, 1, 8, 1, 1, 1, 46.00),
(408, 1, 5, 1, 1, 1, 12.00),
(408, 1, 5, 2, 1, 1, 12.00),
(409, 1, 16, 1, 1, 1, 0.00),
(409, 1, 16, 2, 1, 1, 0.00),
(410, 1, 5, 1, 1, 1, 12.00),
(412, 1, 5, 1, 1, 1, 12.00),
(414, 1, 5, 1, 1, 1, 12.00),
(414, 1, 5, 2, 1, 1, 12.00),
(414, 1, 6, 1, 1, 1, 23.00),
(414, 1, 6, 2, 1, 1, 23.00),
(415, 1, 5, 1, 1, 2, 12.00),
(415, 1, 5, 2, 1, 2, 12.00),
(415, 1, 6, 1, 1, 2, 23.00),
(415, 1, 6, 2, 1, 2, 23.00),
(415, 1, 8, 1, 1, 2, 46.00),
(415, 1, 8, 2, 1, 2, 46.00),
(415, 1, 13, 1, 1, 2, 77.00),
(415, 1, 13, 2, 1, 2, 77.00),
(415, 1, 16, 1, 1, 2, 0.00),
(415, 1, 16, 2, 1, 2, 0.00),
(416, 1, 5, 1, 1, 8, 12.00),
(416, 1, 5, 2, 1, 8, 12.00),
(416, 1, 6, 1, 1, 8, 23.00),
(416, 1, 6, 2, 1, 8, 23.00),
(416, 1, 8, 1, 1, 8, 46.00),
(416, 1, 8, 2, 1, 8, 46.00),
(416, 1, 13, 1, 1, 8, 77.00),
(416, 1, 13, 2, 1, 8, 77.00),
(416, 1, 16, 1, 1, 8, 0.00),
(416, 1, 16, 2, 1, 8, 0.00),
(417, 3, 5, 1, 1, 1, 12.00),
(417, 3, 5, 2, 1, 1, 12.00),
(417, 3, 8, 1, 1, 1, 46.00),
(417, 3, 8, 2, 1, 1, 46.00),
(417, 3, 16, 1, 1, 1, 0.00),
(417, 3, 16, 2, 1, 1, 0.00),
(418, 1, 5, 1, 1, 3, 12.00),
(418, 1, 5, 2, 1, 3, 12.00),
(418, 1, 6, 1, 1, 3, 23.00),
(418, 1, 6, 2, 1, 3, 23.00),
(418, 1, 8, 1, 1, 3, 46.00),
(418, 1, 8, 2, 1, 3, 46.00),
(418, 1, 13, 1, 1, 3, 77.00),
(418, 1, 13, 2, 1, 3, 77.00),
(418, 1, 16, 1, 1, 3, 0.00),
(418, 1, 16, 2, 1, 3, 0.00),
(419, 2, 13, 1, 1, 2, 77.00),
(419, 2, 13, 2, 1, 2, 77.00),
(421, 1, 5, 1, 1, 1, 12.00),
(421, 1, 5, 2, 1, 1, 12.00),
(421, 1, 6, 1, 1, 1, 23.00),
(421, 1, 6, 2, 1, 1, 23.00),
(421, 1, 8, 1, 1, 1, 46.00),
(421, 1, 8, 2, 1, 1, 46.00),
(421, 1, 13, 1, 1, 1, 77.00),
(421, 1, 13, 2, 1, 1, 77.00),
(421, 1, 16, 1, 1, 1, 0.00),
(421, 1, 16, 2, 1, 1, 0.00),
(421, 2, 5, 1, 1, 1, 12.00),
(421, 2, 5, 2, 1, 1, 12.00),
(421, 2, 6, 1, 1, 1, 23.00),
(421, 2, 6, 2, 1, 1, 23.00),
(421, 2, 13, 1, 1, 1, 77.00),
(421, 2, 13, 2, 1, 1, 77.00),
(421, 2, 16, 1, 1, 1, 0.00),
(421, 2, 16, 2, 1, 1, 0.00),
(421, 3, 5, 1, 1, 1, 12.00),
(421, 3, 5, 2, 1, 1, 12.00),
(421, 3, 6, 1, 1, 1, 23.00),
(421, 3, 6, 2, 1, 1, 23.00),
(421, 3, 13, 1, 1, 1, 77.00),
(421, 3, 13, 2, 1, 1, 77.00),
(421, 3, 16, 1, 1, 1, 0.00),
(421, 3, 16, 2, 1, 1, 0.00),
(417, 4, 5, 1, 1, 1, 12.00),
(417, 4, 5, 2, 1, 1, 12.00),
(417, 4, 8, 1, 1, 1, 46.00),
(417, 4, 8, 2, 1, 1, 46.00),
(417, 4, 16, 1, 1, 1, 0.00),
(417, 4, 16, 2, 1, 1, 0.00),
(419, 3, 13, 1, 1, 2, 77.00),
(419, 3, 13, 2, 1, 2, 77.00),
(417, 5, 5, 1, 1, 1, 12.00),
(417, 5, 5, 2, 1, 1, 12.00),
(417, 5, 5, 1, 2, 1, 6.00),
(417, 5, 8, 1, 1, 1, 46.00),
(417, 5, 8, 2, 1, 1, 46.00),
(417, 5, 16, 1, 1, 1, 0.00),
(417, 5, 16, 2, 1, 1, 0.00),
(427, 1, 5, 1, 1, 1, 12.00),
(427, 1, 5, 2, 1, 1, 12.00),
(427, 1, 6, 1, 1, 1, 23.00),
(427, 1, 6, 2, 1, 1, 23.00),
(429, 1, 5, 1, 1, 1, 12.00),
(429, 1, 5, 2, 1, 1, 12.00),
(429, 1, 6, 1, 1, 1, 23.00),
(429, 1, 6, 2, 1, 1, 23.00),
(429, 1, 8, 1, 1, 1, 46.00),
(429, 1, 8, 2, 1, 1, 46.00),
(429, 1, 13, 1, 1, 1, 77.00),
(429, 1, 13, 2, 1, 1, 77.00),
(429, 1, 16, 1, 1, 1, 0.00),
(429, 1, 16, 2, 1, 1, 0.00),
(431, 1, 5, 1, 1, 3, 12.00),
(431, 1, 5, 2, 1, 3, 12.00),
(431, 1, 6, 1, 1, 3, 23.00),
(431, 1, 6, 2, 1, 3, 23.00),
(431, 1, 8, 1, 1, 3, 46.00),
(431, 1, 8, 2, 1, 3, 46.00),
(431, 1, 13, 1, 1, 3, 77.00),
(431, 1, 13, 2, 1, 3, 77.00),
(431, 1, 16, 1, 1, 3, 77.00),
(431, 1, 16, 2, 1, 3, 77.00),
(431, 1, 17, 1, 1, 3, 0.00),
(431, 1, 17, 2, 1, 3, 0.00),
(432, 1, 13, 1, 1, 1, 77.00),
(432, 1, 13, 2, 1, 1, 77.00),
(432, 1, 16, 1, 1, 1, 77.00),
(432, 1, 16, 2, 1, 1, 77.00),
(432, 1, 17, 1, 1, 1, 0.00),
(432, 1, 17, 2, 1, 1, 0.00),
(433, 1, 16, 1, 1, 1, 77.00),
(433, 1, 16, 2, 1, 1, 77.00),
(433, 1, 17, 1, 1, 1, 0.00),
(433, 1, 17, 2, 1, 1, 0.00),
(434, 1, 5, 1, 1, 1, 12.00),
(434, 1, 5, 2, 1, 1, 12.00),
(434, 1, 6, 1, 1, 1, 23.00),
(434, 1, 6, 2, 1, 1, 23.00),
(434, 1, 8, 1, 1, 1, 46.00),
(434, 1, 8, 2, 1, 1, 46.00),
(434, 1, 16, 1, 1, 1, 77.00),
(434, 1, 16, 2, 1, 1, 77.00),
(434, 1, 17, 1, 1, 1, 0.00),
(434, 1, 17, 2, 1, 1, 0.00),
(436, 1, 5, 1, 1, 1, 12.00),
(436, 1, 5, 2, 1, 1, 12.00),
(436, 1, 6, 1, 1, 1, 23.00),
(436, 1, 6, 2, 1, 1, 23.00),
(436, 1, 8, 1, 1, 1, 46.00),
(436, 1, 8, 2, 1, 1, 46.00),
(436, 1, 17, 1, 1, 1, 0.00),
(436, 1, 17, 2, 1, 1, 0.00),
(433, 2, 16, 1, 1, 1, 77.00),
(433, 2, 16, 2, 1, 1, 77.00),
(433, 2, 17, 1, 1, 1, 0.00),
(433, 2, 17, 2, 1, 1, 0.00),
(433, 3, 16, 1, 1, 1, 77.00),
(433, 3, 16, 2, 1, 1, 77.00),
(433, 3, 17, 1, 1, 1, 0.00),
(433, 3, 17, 2, 1, 1, 0.00),
(433, 4, 16, 1, 1, 1, 77.00),
(433, 4, 16, 2, 1, 1, 77.00),
(433, 4, 17, 1, 1, 1, 0.00),
(433, 4, 17, 2, 1, 1, 0.00),
(433, 5, 16, 1, 1, 1, 77.00),
(433, 5, 16, 2, 1, 1, 77.00),
(433, 5, 17, 1, 1, 1, 0.00),
(433, 5, 17, 2, 1, 1, 0.00),
(433, 6, 16, 1, 1, 1, 77.00),
(433, 6, 16, 2, 1, 1, 77.00),
(433, 6, 17, 1, 1, 1, 0.00),
(433, 6, 17, 2, 1, 1, 0.00),
(433, 7, 16, 1, 1, 1, 77.00),
(433, 7, 16, 2, 1, 1, 77.00),
(433, 7, 17, 1, 1, 1, 0.00),
(433, 7, 17, 2, 1, 1, 0.00),
(439, 1, 5, 1, 1, 1, 12.00),
(439, 1, 5, 2, 1, 1, 12.00),
(440, 1, 5, 1, 1, 1, 12.00),
(440, 1, 5, 2, 1, 1, 12.00),
(443, 1, 5, 1, 1, 3, 12.00),
(443, 1, 5, 2, 1, 3, 12.00),
(443, 1, 6, 1, 1, 3, 23.00),
(443, 1, 6, 2, 1, 3, 23.00),
(443, 1, 8, 1, 1, 3, 46.00),
(443, 1, 8, 2, 1, 3, 46.00),
(443, 1, 13, 1, 1, 3, 77.00),
(443, 1, 13, 2, 1, 3, 77.00),
(443, 1, 16, 1, 1, 3, 77.00),
(443, 1, 16, 2, 1, 3, 77.00),
(443, 1, 17, 1, 1, 3, 0.00),
(443, 1, 17, 2, 1, 3, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `tst_hotel_booking_statuses`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_booking_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tst_hotel_booking_statuses`
--

INSERT INTO `tst_hotel_booking_statuses` (`id`, `name`) VALUES
(1, 'Новое'),
(2, 'Обработано'),
(3, 'Ожидание прибытия'),
(4, 'Проживание'),
(5, 'Ожидание выезда'),
(6, 'Завершено'),
(7, 'Отмена');

-- --------------------------------------------------------

--
-- Table structure for table `tst_hotel_booking_transfer`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_booking_transfer` (
  `booking_id` int(11) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  `transfer_id` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `location` varchar(100) NOT NULL,
  `race` varchar(10) DEFAULT NULL,
  `train` varchar(10) DEFAULT NULL,
  `wagon` varchar(10) DEFAULT NULL,
  `cost` float(10,2) NOT NULL DEFAULT '0.00',
  `status` int(2) NOT NULL DEFAULT '0',
  KEY `fk_hotel_booking_transfer_transfer_id` (`transfer_id`),
  KEY `fk_hotel_booking_transfer_booking_id` (`booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tst_hotel_booking_transfer`
--

INSERT INTO `tst_hotel_booking_transfer` (`booking_id`, `version`, `transfer_id`, `date`, `time`, `location`, `race`, `train`, `wagon`, `cost`, `status`) VALUES
(187, 1, 1, 811, 23, '26', '34', '', '', 10.00, 0),
(185, 1, 1, 1415829600, 63300, 'sheshse', '346', NULL, NULL, 10.00, 0),
(179, 1, 1, -10800, 69000, 'aegaeg', '35', NULL, NULL, 14.00, 0),
(191, 1, 2, 1416088800, 0, '', '', '', '', 0.97, 0),
(201, 1, 1, 1420063200, 36600, 'кпккп', '123', '', '', 10.00, 0),
(201, 1, 2, 1420668000, 18600, 'кпккп', '234', '', '', 15.00, 0),
(203, 1, 1, 1416693600, 19200, 'Милан', '123', '', '', 10.00, 0),
(204, 1, 2, 1416952800, 74400, 'Милан', '543', NULL, NULL, 15.00, 0),
(210, 1, 3, 1417212000, 43800, 'Kiev', NULL, '2', '1', 26.00, 0),
(212, 1, 1, 1417212000, 15600, '1', '1', '', '', 10.00, 0),
(212, 1, 2, 1417298400, 73800, '1', '1', '', '', 15.00, 0),
(220, 1, 1, 1417730400, 0, '', '', '', '', 10.00, 0),
(220, 1, 2, 1417816800, 0, '', '', '', '', 15.00, 0),
(221, 1, 1, 1417730400, 0, '', '', '', '', 10.00, 0),
(221, 1, 2, 1417816800, 0, '', '', '', '', 15.00, 0),
(227, 1, 1, 1417730400, 50820, '', '', NULL, NULL, 10.00, 0),
(227, 1, 2, 1417816800, 50820, '', '', NULL, NULL, 15.00, 0),
(224, 1, 3, 1417730400, -3300, 'rbtd', NULL, '1', '2', 26.00, 0),
(224, 1, 4, 1417816800, 46200, 'rbtd', NULL, '1', '2', 14.00, 0),
(228, 1, 1, 1419458400, 0, '', '', '', '', 10.00, 0),
(228, 1, 2, 1419544800, 0, '', '', '', '', 15.00, 0),
(231, 1, 1, 1419804000, 62400, 'sehse', '3245', NULL, NULL, 10.00, 0),
(239, 1, 1, 1421100000, 66900, 'drshses', '2452', '', '', 71.00, 0),
(290, 1, 1, 1427320800, 50700, '11', '1', '', '', 10.00, 0),
(290, 1, 2, 1427407200, 50700, '111', '1', '', '', 15.00, 0),
(306, 1, 1, 1428613200, -600, 'укауука', '23', '', '', 10.00, 0),
(306, 1, 2, 1428699600, 18900, 'укауука', '34', '', '', 15.00, 0),
(309, 1, 1, 1429045200, 45300, 'wefwef', '12', '', '', 10.00, 0),
(309, 1, 2, 1429131600, 41700, 'wefwefsdc', '23', '', '', 15.00, 0),
(325, 2, 1, 1430427600, 39000, '1', '1', '', '', 10.00, 0),
(325, 3, 1, 1430427600, 39000, '1', '1', '', '', 10.00, 0),
(325, 3, 2, 1430514000, 28200, '1', '1', '', '', 15.00, 0),
(325, 4, 1, 1430427600, 39000, '1', '1', '', '', 10.00, 0),
(325, 4, 2, 1430514000, 28200, '1', '1', '', '', 15.00, 0),
(325, 5, 1, 1430427600, 46200, '1', '1', '', '', 10.00, 0),
(325, 5, 2, 1431205200, 64200, '1', '1', '', '', 15.00, 0),
(325, 6, 2, 1431205200, 64200, '1', '1', NULL, NULL, 15.00, 0),
(325, 6, 1, 1430427600, 69300, '1', '1', NULL, NULL, 10.00, 0),
(334, 1, 1, 1431810000, 14700, 'вавыа', '12', '', '', 10.00, 0),
(334, 1, 4, 1431896400, 36900, 'вавыа', '45', '12', '4', 14.00, 0),
(334, 2, 1, 1431810000, 14700, 'вавыа', '12', '', '', 10.00, 0),
(334, 2, 4, 1431896400, 36900, 'вавыа', '45', '12', '4', 14.00, 0),
(334, 3, 1, 1431810000, 14700, 'вавыа', '12', '', '', 10.00, 0),
(334, 3, 4, 1431896400, 36900, 'вавыа', '45', '12', '4', 14.00, 0),
(335, 2, 1, 1433106000, 20100, 'ыаыпыып', '13', '', '', 10.00, 0),
(335, 3, 1, 1433106000, 20100, 'ыаыпыып', '13', '', '', 10.00, 0),
(340, 1, 1, 1434142800, 14400, '1', '1', '', '', 10.00, 0),
(341, 1, 1, 1434142800, 58500, '11', '1', '', '', 10.00, 0),
(352, 1, 1, 1436302800, 57600, 'dddd', '1', '', '', 10.00, 0),
(353, 1, 1, 1436302800, 57600, 'dddd', '1', '', '', 10.00, 0),
(354, 1, 1, 1436302800, 57600, 'dddd', '1', '', '', 10.00, 0),
(355, 1, 1, 1437253200, 32940, 'asdd', '123', '', '', 10.00, 1),
(368, 1, 1, 1439326800, 55680, '34234', '12', '', '', 10.00, 3),
(368, 1, 2, 1439326800, 55860, '34234', '21', '', '', 15.00, 4),
(372, 1, 1, 1437166800, 56460, 'tgghrth', '12', '', '', 10.00, 3),
(372, 1, 2, 1437253200, 71100, 'tgghrth', '234', '', '', 15.00, 0),
(372, 2, 1, 1437166800, 56460, 'tgghrth', '12', '', '', 10.00, 3),
(372, 2, 2, 1437253200, 71100, 'tgghrth', '234', '', '', 15.00, 0),
(372, 3, 1, 1437166800, 56460, 'tgghrth', '12', '', '', 10.00, 3),
(372, 3, 2, 1437253200, 71100, 'tgghrth', '234', '', '', 15.00, 0),
(372, 4, 1, 1437166800, 56460, 'tgghrth', '12', '', '', 10.00, 3),
(372, 4, 2, 1437253200, 71100, 'tgghrth', '234', '', '', 15.00, 0),
(372, 5, 1, 1437166800, 56460, 'tgghrth', '12', '', '', 10.00, 3),
(372, 5, 2, 1437253200, 71100, 'tgghrth', '234', '', '', 15.00, 0),
(372, 6, 1, 1437166800, 56460, 'tgghrth', '12', '', '', 10.00, 3),
(372, 6, 2, 1437253200, 71100, 'tgghrth', '234', '', '', 15.00, 0),
(374, 1, 3, 1440968400, 35400, 'Rome', '', '12', '45', 26.00, 0),
(374, 1, 2, 1441227600, 73200, 'Rome', '34', '', '', 15.00, 0),
(374, 2, 3, 1440968400, 35400, 'Rome', '', '12', '45', 26.00, 0),
(374, 2, 2, 1441227600, 73200, 'Rome', '34', '', '', 15.00, 0),
(335, 4, 1, 1433106000, 20100, 'ыаыпыып', '13', '', '', 10.00, 0),
(372, 8, 3, 1439413200, 40800, 'Rome', '', '12', '12', 26.00, 1),
(372, 8, 2, 1439672400, 45600, 'Rome', '23', '', '', 15.00, 0),
(377, 1, 1, 1440622800, 19200, 'Rome', '12', '', '', 10.00, 0),
(377, 1, 4, 1441227600, 41700, 'Рим', '', '33', '12', 14.00, 0),
(388, 1, 1, 1447624800, 19200, 'Riga', '11', '', '', 10.00, 0),
(388, 1, 4, 1448143200, 33300, 'Riga', '', ' 44', '33', 14.00, 0),
(390, 1, 1, 1447624800, 40800, 'Riga', '12', '', '', 10.00, 0),
(390, 1, 4, 1448143200, 18300, 'Riga', '', '22', '11', 14.00, 0),
(392, 1, 1, 1449612000, -2400, '12', '12', '', '', 10.00, 0),
(399, 1, 1, 1453240800, -7800, '1', '1', '', '', 10.00, 0),
(399, 1, 2, 1453327200, 42600, '11', '1', '', '', 15.00, 0),
(403, 1, 1, 1461358800, 30960, '1', '1', '', '', 10.00, 4),
(403, 1, 2, 1455141600, 72600, '1', '1', '', '', 15.00, 0),
(404, 1, 1, 1461358800, 30960, '1', '1', '', '', 10.00, 4),
(404, 1, 2, 1455141600, 69000, '11', '1', '', '', 15.00, 0),
(415, 1, 1, 1456351200, 23400, '1', '1', '', '', 10.00, 0),
(415, 1, 2, 1456524000, -5400, '11', '1', '', '', 15.00, 0),
(416, 1, 1, 1458597600, 40800, 'Rome', '12', '', '', 10.00, 0),
(416, 1, 2, 1459285200, 18300, 'Kiev', '34', '', '', 15.00, 0),
(417, 1, 3, 1461358800, 38220, 'Минск', '', '12', '22', 26.00, 2),
(417, 1, 2, 1461445200, 41700, 'Минск', '22', '', '', 15.00, 1),
(417, 2, 3, 1461358800, 38220, 'Минск', '', '12', '22', 26.00, 2),
(417, 2, 2, 1461445200, 41700, 'Минск', '22', '', '', 15.00, 1),
(417, 3, 3, 1461358800, 38220, 'Минск', '', '12', '22', 26.00, 2),
(417, 3, 2, 1461445200, 41700, 'Минск', '22', '', '', 15.00, 1),
(421, 1, 1, 1461445200, 15600, 'gregger', '12', '', '', 10.00, 0),
(421, 1, 2, 1461531600, 21000, 'gregger', '12', '', '', 15.00, 0),
(421, 2, 1, 1461445200, 15600, 'gregger', '12', '', '', 10.00, 0),
(421, 2, 2, 1461531600, 21000, 'gregger', '12', '', '', 15.00, 0),
(421, 3, 1, 1461445200, 15600, 'gregger', '12', NULL, NULL, 10.00, 0),
(421, 3, 2, 1461531600, 21000, 'gregger', '12', NULL, NULL, 15.00, 0),
(417, 4, 3, 1461358800, 38220, 'Минск', NULL, '12', '22', 26.00, 0),
(417, 4, 2, 1461445200, 41700, 'Минск', '22', NULL, NULL, 15.00, 0),
(417, 5, 3, 1461358800, 38220, 'Минск', NULL, '12', '22', 26.00, 0),
(417, 5, 2, 1461445200, 41700, 'Минск', '22', NULL, NULL, 15.00, 0),
(432, 1, 3, 1465506000, 19200, 'asd', '', '12', '313', 26.00, 0),
(438, 1, 1, 1468530000, 33120, 'kiev', '12', '', '', 10.00, 0),
(438, 1, 2, 1468789200, 33120, 'kiev', '12', '', '', 15.00, 0),
(440, 1, 3, 1468530000, 63600, '3', '', '1', '2', 26.00, 1),
(441, 1, 1, 1468530000, 73320, 'qedwed', 'qwe', '', '', 10.00, 1),
(443, 1, 3, 1470258000, 58800, 'Киев', '', '23', '23', 26.00, 0),
(443, 1, 2, 1470517200, 62400, 'Киев', '23', '', '', 15.00, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tst_hotel_booking_versions`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_booking_versions` (
  `booking_id` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `uid` int(11) DEFAULT NULL,
  `arrival` int(11) NOT NULL,
  `departure` int(11) NOT NULL,
  `days` int(11) NOT NULL DEFAULT '1',
  `adults` int(11) NOT NULL DEFAULT '1',
  `children` int(11) NOT NULL DEFAULT '0',
  `prepayment_type` int(11) NOT NULL,
  `room` int(11) DEFAULT NULL,
  `room_type` int(11) NOT NULL,
  `room_price` float(10,2) NOT NULL DEFAULT '0.00',
  `cost` float(10,2) NOT NULL DEFAULT '0.00',
  `comment` text,
  KEY `fk_hotel_booking_uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tst_hotel_booking_versions`
--

INSERT INTO `tst_hotel_booking_versions` (`booking_id`, `version`, `time`, `uid`, `arrival`, `departure`, `days`, `adults`, `children`, `prepayment_type`, `room`, `room_type`, `room_price`, `cost`, `comment`) VALUES
(244, 1, 1423235975, 18, 1422309600, 1422396000, 1, 1, 0, 2, 406, 3, 40.00, 52.00, ''),
(245, 1, 1423235975, 18, 1422309600, 1422396000, 1, 1, 0, 1, 405, 3, 125.00, 132.00, ''),
(245, 2, 1423306921, 49, 1422309600, 1422396000, 1, 1, 0, 1, 405, 3, 125.00, 136.00, ''),
(249, 1, 1423423963, 31, 1423346400, 1423692000, 4, 2, 0, 1, 407, 3, 91.00, 366.40, ''),
(254, 1, 1423761352, 49, 1423692000, 1423778400, 1, 1, 0, 1, 407, 3, 91.00, 91.00, ''),
(254, 2, 1423761738, 49, 1423692000, 1423778400, 1, 1, 0, 1, 407, 3, 91.00, 120.00, ''),
(254, 3, 1423761891, 49, 1423692000, 1423778400, 1, 1, 0, 1, 407, 3, 91.00, 92.10, ''),
(257, 1, 1423870540, 20, 1423864800, 1423951200, 1, 2, 0, 1, 203, 3, 91.00, 91.00, ''),
(247, 1, 1423235975, 18, 1422655200, 1422741600, 1, 1, 0, 2, 407, 3, 1.00, 1.00, ''),
(273, 1, 1425140209, 20, 1425074400, 1425160800, 1, 2, 0, 2, 314, 3, 0.10, 0.50, ''),
(281, 1, 1426528417, 67, 1426456800, 1426543200, 1, 1, 0, 2, 216, 3, 0.10, 0.10, ''),
(284, 1, 1427119184, 49, 1427061600, 1427148000, 1, 2, 0, 2, 316, 3, 115.00, 115.00, ''),
(286, 1, 1427121095, 49, 1427061600, 1427148000, 1, 2, 0, 2, 201, 3, 115.00, 115.00, ''),
(286, 2, 1427121243, 49, 1427061600, 1427148000, 1, 2, 0, 2, 201, 3, 115.00, 115.00, ''),
(288, 1, 1427365406, 49, 1427320800, 1427407200, 1, 2, 0, 2, 316, 3, 115.00, 115.00, ''),
(298, 1, 1428671565, 73, 1428613200, 1428699600, 1, 2, 0, 3, 201, 3, 0.10, 0.10, ''),
(299, 1, 1428673064, 20, 1428613200, 1428699600, 1, 2, 0, 3, 203, 3, 0.10, 0.10, ''),
(299, 2, 1428674932, 20, 1428613200, 1428699600, 1, 2, 0, 3, 203, 3, 0.10, 2.10, ''),
(299, 3, 1428674937, 20, 1428613200, 1428699600, 1, 2, 0, 3, 203, 3, 0.10, 2.10, ''),
(299, 4, 1428674983, 18, 1428613200, 1428699600, 1, 2, 0, 3, 203, 3, 0.10, 2.10, ''),
(316, 1, 1429633131, 31, 1429650000, 1429909200, 3, 1, 0, 1, 401, 3, 125.00, 495.00, ''),
(317, 1, 1430490006, 49, 1430427600, 1430514000, 1, 1, 0, 2, 401, 3, 115.00, 115.00, ''),
(318, 1, 1430490231, 59, 1430427600, 1430514000, 1, 1, 0, 2, 316, 3, 115.00, 115.00, ''),
(320, 1, 1430490428, NULL, 1430427600, 1430514000, 1, 1, 0, 3, 201, 3, 0.10, 0.10, ''),
(325, 1, 1430495272, 49, 1430427600, 1430514000, 1, 2, 0, 1, 208, 3, 125.00, 125.00, ''),
(325, 2, 1430495306, 49, 1430427600, 1430514000, 1, 2, 0, 1, 208, 3, 125.00, 135.00, ''),
(325, 3, 1430495321, 49, 1430427600, 1430514000, 1, 2, 0, 1, 208, 3, 125.00, 150.00, ''),
(325, 4, 1430495331, 49, 1430427600, 1430514000, 1, 2, 0, 1, 208, 3, 125.00, 180.00, ''),
(326, 1, 1430495824, 49, 1430686800, 1431291600, 7, 2, 0, 1, 401, 3, 125.00, 875.00, ''),
(326, 2, 1430496189, 18, 1430686800, 1431291600, 7, 2, 0, 1, 401, 3, 125.00, 1406.58, ''),
(326, 3, 1430496232, 49, 1430686800, 1431291600, 7, 2, 0, 1, 401, 3, 125.00, 1153.00, ''),
(326, 4, 1430496361, 49, 1430686800, 1431291600, 7, 2, 0, 1, 401, 3, 125.00, 1153.00, ''),
(325, 5, 1430495443, 49, 1430427600, 1430514000, 1, 2, 0, 1, 208, 3, 125.00, 180.00, ''),
(326, 5, 1430496538, 49, 1430686800, 1431291600, 7, 2, 0, 1, 401, 3, 125.00, 1178.00, ''),
(330, 1, 1431804616, 20, 1431723600, 1431810000, 1, 2, 1, 3, 401, 3, 0.10, 0.10, ''),
(330, 2, 1431806610, 20, 1431723600, 1431810000, 1, 2, 1, 3, 401, 3, 0.10, 10.10, ''),
(330, 3, 1431806649, 20, 1431723600, 1431810000, 1, 2, 1, 3, 401, 3, 0.10, 10.10, ''),
(331, 1, 1431807141, 20, 1431723600, 1431810000, 1, 2, 0, 1, 316, 3, 125.00, 125.00, ''),
(331, 2, 1431810526, 20, 1431723600, 1431810000, 1, 2, 0, 1, 316, 3, 125.00, 173.00, ''),
(334, 1, 1431813505, 20, 1431810000, 1431896400, 1, 2, 0, 3, 216, 3, 0.10, 384.10, ''),
(334, 2, 1431813630, 20, 1431810000, 1431896400, 1, 2, 0, 3, 216, 3, 0.10, 384.10, ''),
(335, 1, 1433166575, 31, 1433106000, 1433538000, 5, 2, 0, 1, 316, 3, 125.00, 773.00, ''),
(335, 2, 1433712353, 31, 1433106000, 1433538000, 5, 2, 0, 1, 316, 3, 125.00, 783.00, ''),
(342, 1, 1434182828, 86, 1434142800, 1434229200, 1, 2, 0, 3, 203, 3, 0.10, 0.10, ''),
(350, 1, 1434568412, 49, 1434488400, 1434574800, 1, 2, 0, 3, 216, 3, 0.10, 0.10, ''),
(351, 1, 1436295571, 88, 1437685200, 1437771600, 1, 2, 0, 1, 316, 3, 125.00, 125.00, ''),
(366, 1, 1436369266, 20, 1436302800, 1436389200, 1, 2, 0, 3, 106, 3, 0.10, 154.10, ''),
(366, 2, 1436369399, 20, 1436302800, 1436389200, 1, 2, 0, 3, 106, 3, 0.10, 154.10, ''),
(372, 1, 1437231087, 20, 1438203600, 1438462800, 3, 2, 0, 1, 316, 3, 125.00, 718.00, ''),
(372, 2, 1437231526, 20, 1438203600, 1438462800, 3, 2, 0, 1, 316, 3, 125.00, 718.00, ''),
(372, 3, 1437231538, 20, 1438203600, 1438462800, 3, 2, 0, 1, 316, 3, 125.00, 718.00, ''),
(372, 4, 1437231548, 20, 1438203600, 1438462800, 3, 2, 0, 1, 316, 3, 125.00, 717.00, ''),
(372, 5, 1437231642, 20, 1438203600, 1438462800, 3, 2, 0, 1, 316, 3, 125.00, 717.00, ''),
(373, 1, 1437233455, 96, 1437166800, 1437512400, 4, 2, 1, 3, 203, 3, 0.10, 0.40, ''),
(373, 2, 1437233900, 96, 1437166800, 1437512400, 4, 2, 1, 3, 203, 3, 0.10, 0.40, ''),
(374, 1, 1437295589, 96, 1440968400, 1441227600, 3, 2, 1, 3, 316, 3, 0.10, 1312.80, ''),
(376, 1, 1437499970, 97, 1437426000, 1437512400, 1, 2, 0, 2, 208, 3, 115.00, 185.00, ''),
(335, 3, 1433854206, 31, 1437080400, 1437512400, 5, 2, 0, 1, 316, 3, 125.00, 755.00, ''),
(372, 6, 1437231700, 20, 1439413200, 1439672400, 3, 2, 0, 1, 216, 3, 125.00, 640.00, ''),
(372, 7, 1439393047, 20, 1439413200, 1439672400, 3, 2, 0, 1, 216, 3, 125.00, 446.00, ''),
(371, 1, 1437130146, 94, 1437080400, 1437166800, 1, 2, 0, 1, 201, 3, 125.00, 125.00, ''),
(379, 1, 1441709901, 31, 1441659600, 1441746000, 1, 2, 0, 1, 216, 3, 125.00, 125.00, ''),
(389, 1, 1447179947, 114, 1447106400, 1447192800, 1, 2, 0, 1, 104, 5, 135.00, 135.00, ''),
(379, 2, 1447182504, 18, 1447279200, 1447365600, 1, 2, 0, 1, 103, 3, 125.00, 125.00, ''),
(389, 2, 1447182528, 18, 1447192800, 1447279200, 1, 2, 0, 1, 104, 5, 135.00, 135.00, ''),
(369, 1, 1437054552, 94, 1437166800, 1437512400, 4, 2, 0, 1, 201, 3, 125.00, 1116.00, ''),
(370, 1, 1437130055, 95, 1437080400, 1437166800, 1, 2, 0, 1, 216, 3, 125.00, 125.00, ''),
(370, 2, 1447182594, 18, 1447106400, 1447192800, 1, 2, 0, 1, 103, 3, 125.00, 125.00, ''),
(379, 3, 1447182538, 18, 1447279200, 1447365600, 1, 2, 0, 1, 103, 3, 125.00, 125.00, ''),
(417, 1, 1461406498, 99, 1461445200, 1461531600, 1, 2, 0, 1, 104, 5, 135.00, 176.00, ''),
(417, 2, 1461406936, 99, 1461445200, 1461531600, 1, 2, 0, 1, 104, 5, 135.00, 176.00, ''),
(419, 1, 1461407619, 125, 1461358800, 1461531600, 2, 2, 0, 1, 205, 5, 135.00, 270.00, ''),
(421, 1, 1461495091, 20, 1461531600, 1461618000, 1, 2, 0, 1, 316, 3, 125.00, 466.00, ''),
(421, 2, 1461495658, 20, 1461531600, 1461618000, 1, 2, 0, 1, 316, 3, 125.00, 374.00, ''),
(422, 1, 1461501537, 20, 1461963600, 1462050000, 1, 2, 0, 1, 316, 3, 125.00, 125.00, ''),
(417, 3, 1461407271, 99, 1461445200, 1461531600, 1, 2, 0, 1, 104, 5, 135.00, 292.00, ''),
(419, 2, 1461407767, 125, 1461358800, 1461531600, 2, 2, 0, 1, 205, 5, 135.00, 578.00, ''),
(417, 4, 1461502035, 18, 1461445200, 1461531600, 1, 2, 0, 1, 104, 5, 135.00, 292.00, ''),
(424, 1, 1461501579, 20, 1461963600, 1462050000, 1, 2, 0, 1, 201, 3, 125.00, 125.00, ''),
(430, 1, 1463421288, 113, 1463346000, 1463432400, 1, 2, 0, 3, NULL, 3, 0.10, 0.10, ''),
(433, 1, 1465538587, 127, 1465506000, 1465592400, 1, 2, 0, 1, 105, 5, 135.00, 289.00, ''),
(433, 2, 1468222212, 18, 1465506000, 1465592400, 1, 2, 0, 1, 105, 5, 135.00, 289.00, ''),
(433, 3, 1468222229, 18, 1465506000, 1465592400, 1, 2, 0, 1, 105, 5, 135.00, 289.00, ''),
(433, 4, 1468224603, 18, 1465506000, 1465592400, 1, 2, 0, 1, 105, 5, 135.00, 289.00, ''),
(433, 5, 1468224804, 18, 1465506000, 1465592400, 1, 2, 0, 1, 105, 5, 135.00, 289.00, ''),
(433, 6, 1468224916, 18, 1465506000, 1465592400, 1, 2, 0, 1, 105, 5, 135.00, 289.00, ''),
(437, 1, 1468226846, 129, 1468184400, 1468270800, 1, 2, 0, 1, 104, 5, 135.00, 135.00, '');

-- --------------------------------------------------------

--
-- Table structure for table `tst_hotel_cash`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_cash` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `balance` float(10,2) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='баланс работников отеля' AUTO_INCREMENT=19 ;

--
-- Dumping data for table `tst_hotel_cash`
--

INSERT INTO `tst_hotel_cash` (`user_id`, `balance`) VALUES
(13, 14.34),
(18, 1445.00);

-- --------------------------------------------------------

--
-- Table structure for table `tst_hotel_cash_transactions`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_cash_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_from` int(11) NOT NULL,
  `user_to` int(11) NOT NULL,
  `summ` float(10,2) NOT NULL,
  `date` int(11) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hotel_cash_transactions_user_from` (`user_from`),
  KEY `fk_hotel_cash_transactions_user_to` (`user_to`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='фин операции' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tst_hotel_cash_transactions`
--

INSERT INTO `tst_hotel_cash_transactions` (`id`, `user_from`, `user_to`, `summ`, `date`, `description`) VALUES
(1, 18, 18, 1.00, 1381819728, '');

-- --------------------------------------------------------

--
-- Table structure for table `tst_hotel_equipment`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_equipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_name` varchar(50) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tst_hotel_equipment`
--

INSERT INTO `tst_hotel_equipment` (`id`, `sys_name`, `sort_order`) VALUES
(1, 'LCD телевизор с 42\\', 0),
(2, 'Холодильник / минибар', 0),
(3, 'Digital Safe, size: H19,5×43×37(cm)', 0),
(4, 'Двухспальная кровать king-size', 0),
(5, 'Прикроватные тумбочки, шкаф - купе', 0),
(6, 'Ванная комната  укомплектованная    душем', 0),
(7, 'Комнатные тапочки и ванные принадлежности', 0),
(8, 'Фен', 0),
(9, 'Телефон, услуга звонок-будильник', 0),
(10, 'Возможность затемнения комнаты в светлое время сут', 0),
(11, 'Подушки наполненные гусиным пухом', 0),
(12, 'Одеяла наполненные верблюжьим подпушком', 0),
(13, 'Подушки наполненные гусиным пухом', 0),
(14, 'нечто', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tst_hotel_equipment_lang`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_equipment_lang` (
  `id` int(11) NOT NULL,
  `lang_id` int(11) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  KEY `fk_hotel_equipment_lang_id` (`id`),
  KEY `fk_hotel_equipment_lang_lang_id` (`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tst_hotel_equipment_lang`
--

INSERT INTO `tst_hotel_equipment_lang` (`id`, `lang_id`, `name`) VALUES
(3, 2, 'Digital Safe, size: H19,5×43×37(cm)'),
(1, 2, 'LCD');

-- --------------------------------------------------------

--
-- Table structure for table `tst_hotel_prepayment_types`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_prepayment_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tst_hotel_prepayment_types`
--

INSERT INTO `tst_hotel_prepayment_types` (`id`, `title`) VALUES
(1, 'Без предоплаты'),
(2, 'Предоплата за первую ночь'),
(3, 'Предоплата за весь период проживания');

-- --------------------------------------------------------

--
-- Table structure for table `tst_hotel_prepayment_types_lang`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_prepayment_types_lang` (
  `type_id` int(11) NOT NULL,
  `lang_id` int(11) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  KEY `fk_hotel_prepayment_types_lang_type_id` (`type_id`),
  KEY `fk_hotel_prepayment_types_lang_lang_id` (`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tst_hotel_prepayment_types_lang`
--

INSERT INTO `tst_hotel_prepayment_types_lang` (`type_id`, `lang_id`, `name`) VALUES
(1, 2, 'Without prepayment'),
(2, 2, 'First night prepayment'),
(3, 2, 'All period prepayment');

-- --------------------------------------------------------

--
-- Table structure for table `tst_hotel_prices`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_prices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` int(11) unsigned NOT NULL,
  `entity_id` int(11) unsigned NOT NULL,
  `prep_type` int(11) DEFAULT NULL,
  `standardPrice` float(10,2) NOT NULL,
  `displayedPrice` float(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hotel_prices_entity_type` (`entity_type_id`),
  KEY `fk_hotel_prices_prep_type` (`prep_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `tst_hotel_prices`
--

INSERT INTO `tst_hotel_prices` (`id`, `entity_type_id`, `entity_id`, `prep_type`, `standardPrice`, `displayedPrice`) VALUES
(31, 4, 3, 1, 125.00, 150.00),
(32, 4, 3, 2, 115.00, 120.00),
(33, 4, 3, 3, 0.10, 1.00),
(34, 5, 5, NULL, 12.00, 13.00),
(35, 6, 1, NULL, 10.00, 20.00),
(36, 7, 1, NULL, 12.00, 13.00),
(37, 7, 2, NULL, 26.00, 24.00),
(38, 7, 3, NULL, 14.00, 16.00),
(39, 4, 4, 1, 123.00, 157.00),
(40, 4, 4, 2, 100.00, 176.00),
(41, 4, 4, 3, 141.00, 146.00),
(42, 4, 5, 1, 135.00, 153.00),
(43, 4, 5, 2, 163.00, 197.00),
(44, 4, 5, 3, 152.00, 174.00),
(45, 5, 6, NULL, 23.00, 67.00),
(46, 5, 7, NULL, 14.00, 36.00),
(47, 5, 8, NULL, 46.00, 79.00),
(49, 7, 4, NULL, 26.00, 50.00),
(50, 7, 5, NULL, 25.00, 75.00),
(51, 7, 6, NULL, 15.00, 47.00),
(52, 7, 7, NULL, 21.00, 36.00),
(53, 6, 2, NULL, 15.00, 23.00),
(54, 6, 3, NULL, 26.00, 57.00),
(55, 6, 4, NULL, 14.00, 46.00),
(56, 6, 7, NULL, 25.00, 46.00),
(57, 6, 8, NULL, 14.00, 74.00),
(59, 5, 10, NULL, 49.00, 66.00),
(60, 5, 13, NULL, 77.00, 100.00),
(62, 5, 16, NULL, 77.00, 100.00);

-- --------------------------------------------------------

--
-- Table structure for table `tst_hotel_prices_dates`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_prices_dates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price_id` int(11) NOT NULL,
  `dateFrom` int(11) NOT NULL,
  `dateTo` int(11) NOT NULL,
  `price` float(10,2) NOT NULL,
  `disabled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_hotel_prices_dates_price_id` (`price_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=72 ;

--
-- Dumping data for table `tst_hotel_prices_dates`
--

INSERT INTO `tst_hotel_prices_dates` (`id`, `price_id`, `dateFrom`, `dateTo`, `price`, `disabled`) VALUES
(7, 32, 1420063200, 1425074400, 1.00, 0),
(9, 59, 1420063200, 1422655200, 506.00, 0),
(10, 34, 1417039200, 1417298400, 5.00, 0),
(11, 39, 1420063200, 1422655200, 90.00, 0),
(12, 42, 1420063200, 1422655200, 90.00, 0),
(14, 45, 1420063200, 1422655200, 2.00, 0),
(15, 46, 1420063200, 1422655200, 5.00, 0),
(16, 47, 1420063200, 1422655200, 4.00, 0),
(18, 60, 1420063200, 1422655200, 7.00, 0),
(19, 35, 1420063200, 1422655200, 17.00, 0),
(20, 53, 1420063200, 1422655200, 13.00, 0),
(21, 54, 1420063200, 1422655200, 24.00, 0),
(22, 55, 1420063200, 1422655200, 27.00, 0),
(23, 56, 1420063200, 1422655200, 21.00, 0),
(24, 57, 1420063200, 1422655200, 7.10, 0),
(25, 36, 1420063200, 1422655200, 80.00, 0),
(26, 37, 1420063200, 1422655200, 82.00, 0),
(27, 38, 1420063200, 1422655200, 83.00, 0),
(28, 49, 1420063200, 1422655200, 84.00, 0),
(29, 50, 1420063200, 1422655200, 85.00, 0),
(30, 51, 1420063200, 1422655200, 86.00, 0),
(31, 52, 1420063200, 1422655200, 87.00, 0),
(32, 31, 1422741600, 1425074400, 91.00, 0),
(33, 39, 1422741600, 1425074400, 92.00, 0),
(34, 42, 1422741600, 1425074400, 93.00, 0),
(35, 35, 1422741600, 1425074400, 1.00, 0),
(36, 53, 1422741600, 1425074400, 2.00, 0),
(37, 54, 1422741600, 1425074400, 3.00, 0),
(38, 55, 1422741600, 1425074400, 4.00, 0),
(39, 56, 1422741600, 1425074400, 5.00, 0),
(40, 57, 1422741600, 1425074400, 6.00, 0),
(41, 36, 1422741600, 1425074400, 10.00, 0),
(42, 37, 1422741600, 1425074400, 12.00, 0),
(43, 38, 1422741600, 1425074400, 13.00, 0),
(44, 49, 1422741600, 1425074400, 14.00, 0),
(45, 50, 1422741600, 1425074400, 15.00, 0),
(46, 51, 1422741600, 1425074400, 16.00, 0),
(47, 52, 1422741600, 1425074400, 17.00, 0),
(48, 34, 1422741600, 1425074400, 0.10, 0),
(49, 45, 1422741600, 1425074400, 0.20, 0),
(50, 46, 1422741600, 1425074400, 0.30, 0),
(51, 47, 1422741600, 1425074400, 0.40, 0),
(53, 59, 1422741600, 1425074400, 0.60, 0),
(54, 60, 1422741600, 1425074400, 0.70, 0),
(55, 31, 1417644000, 1417903200, 100.00, 0),
(56, 34, 1417644000, 1417903200, 8.00, 1),
(57, 45, 1417644000, 1417903200, 10.00, 0),
(58, 31, 1421532000, 1421964000, 95.00, 0),
(59, 32, 1425074400, 1426543200, 0.10, 0),
(61, 31, 1427061600, 1427749200, 3.00, 0),
(62, 32, 1427061600, 1427749200, 0.00, 0),
(63, 33, 1427061600, 1427749200, 0.00, 0),
(64, 32, 1434056400, 1434315600, 0.00, 0),
(65, 33, 1434056400, 1434315600, 0.00, 0),
(66, 35, 1441314000, 1443819600, 0.00, 1),
(67, 34, 1443128400, 1443819600, 0.00, 1),
(68, 42, 1443474000, 1443733200, 0.00, 1),
(69, 36, 1442955600, 1443733200, 0.00, 1),
(70, 41, 1443560400, 1443992400, 0.00, 1),
(71, 49, 1443474000, 1443819600, 10.00, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tst_hotel_rooms`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_hotel_rooms_type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=409 ;

--
-- Dumping data for table `tst_hotel_rooms`
--

INSERT INTO `tst_hotel_rooms` (`id`, `type`, `priority`) VALUES
(101, 3, 12),
(102, 3, 28),
(103, 3, 37),
(104, 5, 1),
(105, 5, 2),
(106, 3, 13),
(107, 3, 30),
(108, 3, 29),
(109, 3, 14),
(110, 3, 33),
(111, 3, 15),
(112, 3, 31),
(113, 3, 36),
(114, 3, 40),
(115, 3, 32),
(116, 3, 11),
(201, 3, 2),
(202, 3, 23),
(203, 3, 3),
(204, 5, 4),
(205, 5, 3),
(206, 3, 4),
(207, 3, 24),
(208, 3, 5),
(209, 3, 16),
(210, 3, 25),
(211, 3, 6),
(212, 3, 21),
(213, 3, 34),
(214, 3, 39),
(215, 3, 22),
(216, 3, 1),
(301, 3, 8),
(302, 3, 17),
(303, 4, 1),
(304, 4, 2),
(305, 3, 26),
(306, 3, 9),
(307, 3, 18),
(308, 3, 27),
(309, 3, 10),
(310, 3, 19),
(311, 3, 35),
(312, 3, 38),
(313, 3, 20),
(314, 3, 7),
(316, 3, 0),
(402, 3, 47),
(403, 3, 44),
(404, 3, 46),
(405, 3, 43),
(406, 3, 42),
(407, 3, 45),
(408, 3, 41);

-- --------------------------------------------------------

--
-- Table structure for table `tst_hotel_room_types`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_room_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `max_adults` int(11) NOT NULL DEFAULT '1',
  `max_children` int(11) NOT NULL DEFAULT '1',
  `link` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tst_hotel_room_types`
--

INSERT INTO `tst_hotel_room_types` (`id`, `title`, `max_adults`, `max_children`, `link`) VALUES
(3, 'Двухместный номер', 2, 1, 'hotel_double_room'),
(4, 'Двухместный люкс', 3, 1, 'hotel_luxe_room'),
(5, 'Двухместный Полулюкс', 2, 1, 'hotel_junior_suite_room');

-- --------------------------------------------------------

--
-- Table structure for table `tst_hotel_room_types_equipment`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_room_types_equipment` (
  `type_id` int(11) NOT NULL,
  `eq_id` int(11) NOT NULL,
  `draw_order` int(11) NOT NULL,
  `bold` tinyint(1) NOT NULL,
  `font_size` tinyint(2) NOT NULL,
  KEY `fk_hotel_room_types_equipment_type_id` (`type_id`),
  KEY `fk_hotel_room_types_equipment_eq_id` (`eq_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tst_hotel_room_types_equipment`
--

INSERT INTO `tst_hotel_room_types_equipment` (`type_id`, `eq_id`, `draw_order`, `bold`, `font_size`) VALUES
(3, 1, 1, 1, 0),
(3, 2, 2, 0, 16),
(3, 4, 3, 0, 0),
(3, 3, 4, 0, 0),
(3, 5, 5, 0, 0),
(3, 6, 6, 0, 0),
(4, 1, 1, 0, 0),
(4, 2, 2, 0, 0),
(4, 3, 3, 0, 0),
(4, 4, 4, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tst_hotel_room_types_images`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_room_types_images` (
  `type_id` int(11) NOT NULL,
  `image` varchar(50) NOT NULL,
  `draw_order` int(11) NOT NULL,
  KEY `fk_hotel_room_types_images_type_id` (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tst_hotel_room_types_images`
--

INSERT INTO `tst_hotel_room_types_images` (`type_id`, `image`, `draw_order`) VALUES
(4, 'DSC_0010.jpg', 2),
(4, 'DSC_0015.jpg', 4),
(3, 'DSC_0121.jpg', 3),
(5, 'DSC_0032.jpg', 1),
(3, '20120801_162131-.jpg', 1),
(3, '3-1433422133.jpg', 2),
(3, '3-1433710266.jpg', 4),
(3, '3-1434185548.png', 5),
(4, '4-1436371868.png', 3),
(3, '3-1461408105.jpg', 6);

-- --------------------------------------------------------

--
-- Table structure for table `tst_hotel_room_types_lang`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_room_types_lang` (
  `type_id` int(11) NOT NULL,
  `lang_id` int(11) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  KEY `fk_hotel_room_types_lang_id` (`lang_id`),
  KEY `fk_hotel_room_types_type_id` (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tst_hotel_room_types_lang`
--

INSERT INTO `tst_hotel_room_types_lang` (`type_id`, `lang_id`, `title`, `description`) VALUES
(3, 2, 'Double Room', '<p>435345</p>  '),
(5, 2, 'wefwfw', 'Двухместный номер категории Стандарт Дабл (общей площадью от 17 кв. м до 24 кв.м) - представляет собой уютный однокомнатный номер с красивым, комфортным и функциональным интерьером. Дизайн номера выполнен в классическом стиле, в теплых тонах и создает ощущение роскоши и спокойствия. Номер укомплектован современным мебельным набором из двуспальной кровати размера 190x220 (king-size), двух прикроватных тумбочек, письменного стола со стульями и зеркального шкафа-купе. Одна практически зеркальная стена прекрасно сочетается с интерьером номера и придает ему ощущения легкости и безграничности. Также в номере: LCD телевизор с 42ʺ диагональю, бесплатный высокоскоростной Wi-Fi доступ в интернет, телефон, электронный сейф, с возможностью хранения в нем ноутбука, холодильник/мини-бар а так же всё необходимое для комфортного отдыха. Ванная комната номера (общей площадью от 3 кв. м до 6 кв. м) укомплектована душем. В ней находится фен, комплект из 4-х полотенец, тапочек и всей необходимой парфумерии. Во всем номере, включая ванную комнату действует централизированая климатическая система, с регулированием температурного режима. Для большего комфорта номер оборудован электронным замком с магнитной карточкой для входной двери и электронным индикатором уборки номера. Уборка номера производится ежедневно, смена белья - раз в 3 дня.  ');

-- --------------------------------------------------------

--
-- Table structure for table `tst_hotel_services`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_name` varchar(100) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `outer_id` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `tst_hotel_services`
--

INSERT INTO `tst_hotel_services` (`id`, `sys_name`, `image`, `outer_id`) VALUES
(5, 'Завтрак', NULL, '11'),
(6, 'Ужин', 'e91cee361c3a6364038b04c02ed07cc2.jpg', '12'),
(8, 'Обед', NULL, '13'),
(13, 'Фотограф', NULL, '14'),
(16, 'ікс', NULL, '15'),
(17, 'обед', NULL, '568');

-- --------------------------------------------------------

--
-- Table structure for table `tst_hotel_services_images`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_services_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `image` varchar(50) NOT NULL,
  `draw_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_hotel_services_images_service_id` (`service_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `tst_hotel_services_images`
--

INSERT INTO `tst_hotel_services_images` (`id`, `service_id`, `image`, `draw_order`) VALUES
(1, 5, '5-1416062182.jpg', 3),
(4, 13, '13-1416864483.jpg', 3),
(5, 13, '13-1416864487.jpg', 4),
(6, 13, '13-1416864491.jpeg', 2),
(9, 13, '13-1416864687.jpg', 1),
(10, 13, '13-1416985707.png', 5),
(11, 13, '13-1416985737.jpg', 6),
(12, 13, '13-1416985743.jpeg', 7),
(13, 5, '5-1428139144.jpg', 5),
(14, 16, '16-1428141652.jpg', 1),
(15, 5, '5-1431815192.jpg', 1),
(16, 5, '5-1431815198.jpg', 2),
(17, 5, '5-1431815201.jpg', 4),
(18, 5, '5-1431815204.jpg', 6),
(19, 5, '5-1431815208.jpg', 7),
(20, 6, '6-1431815320.gif', 1),
(21, 17, '17-1463421091.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tst_hotel_services_lang`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_services_lang` (
  `service_id` int(11) NOT NULL,
  `lang_id` int(11) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text,
  `full_descr` text,
  KEY `fk_hotel_services_lang_id` (`service_id`),
  KEY `fk_hotel_services_lang_lang_id` (`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tst_hotel_services_lang`
--

INSERT INTO `tst_hotel_services_lang` (`service_id`, `lang_id`, `name`, `description`, `full_descr`) VALUES
(5, 2, 'eferferferfer', '<p><span style="font-size:8px;"><span style="font-family: comic sans ms,cursive;"><span style="color: rgb(178, 34, 34);"><strong><em><s>wefefrferferfn g<span style="font-family:arial,helvetica,sans-serif;"><span style="font-size: 12px;"> ghnh</span></span></s></em></strong></span></span></span></p>\r\n', '<h2 style="font-style:italic;"><samp><span style="color:#FFA500;"><span style="font-size: 26px;">wwwwwwwwwwww</span></span></samp></h2>\r\n'),
(17, 1, 'обед', '<p>укпукпукпукп</p>\r\n', '<p>укапукпукпук</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `tst_hotel_transfer`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_transfer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_name` varchar(100) NOT NULL,
  `transport_type` int(11) NOT NULL DEFAULT '1',
  `type` int(11) NOT NULL DEFAULT '1',
  `outer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tst_hotel_transfer`
--

INSERT INTO `tst_hotel_transfer` (`id`, `sys_name`, `transport_type`, `type`, `outer_id`) VALUES
(1, 'Аэропорт Борисполь', 1, 1, 361),
(2, 'Аэропорт Борисполь', 1, 2, NULL),
(3, 'ЖД вокзал Киев', 2, 1, NULL),
(4, 'ЖД вокзал Киев', 2, 2, NULL),
(7, 'Аэропорт Жуляны', 1, 1, NULL),
(8, 'Аэропорт Жуляны', 1, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tst_hotel_transfer_lang`
--

CREATE TABLE IF NOT EXISTS `tst_hotel_transfer_lang` (
  `transfer_id` int(11) NOT NULL,
  `lang_id` int(11) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `type_name` varchar(100) DEFAULT NULL,
  `description` text,
  KEY `fk_hotel_transfer_lang_id` (`transfer_id`),
  KEY `fk_hotel_transfer_lang_lang_id` (`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tst_hotel_transfer_lang`
--

INSERT INTO `tst_hotel_transfer_lang` (`transfer_id`, `lang_id`, `name`, `type_name`, `description`) VALUES
(1, 2, 'Airport', 'Arrival', NULL),
(1, 3, '', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tst_labels`
--

CREATE TABLE IF NOT EXISTS `tst_labels` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sys_name` varchar(128) DEFAULT NULL,
  `is_system` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `labels_sys_name` (`sys_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='labels' AUTO_INCREMENT=252 ;

--
-- Dumping data for table `tst_labels`
--

INSERT INTO `tst_labels` (`id`, `sys_name`, `is_system`) VALUES
(1, 'login', 0),
(2, 'aboutHotel', 0),
(3, 'numbers', 0),
(4, 'contacts', 0),
(5, 'backCall', 0),
(6, 'callMe', 0),
(7, 'example', 0),
(8, 'newWindowMap', 0),
(9, 'bookingNumber', 0),
(10, 'arriveDate', 0),
(11, 'departureDate', 0),
(12, 'type', 0),
(13, 'adults', 0),
(14, 'children', 0),
(15, 'name', 0),
(16, 'lastName', 0),
(17, 'email', 0),
(18, 'phone', 0),
(19, 'comment', 0),
(20, 'cost', 0),
(21, 'forADay', 0),
(22, 'forWholeTerm', 0),
(23, 'doBooking', 0),
(24, 'description', 0),
(25, 'equipment', 0),
(26, 'checkPresence', 0),
(27, 'booking', 0),
(28, 'arrive', 0),
(29, 'departure', 0),
(32, 'keepFeedback', 0),
(33, 'details', 0),
(34, 'search', 0),
(35, 'addQuestion', 0),
(36, 'enterEmail', 0),
(37, 'enterPassword', 0),
(38, 'enterQuestion', 0),
(39, 'search_numbers', 0),
(40, 'fillYourData', 0),
(41, 'password7chars', 0),
(42, 'repeatPassword', 0),
(43, 'enterName', 0),
(44, 'enterSurname', 0),
(45, 'enterPhone', 0),
(46, 'passwordsNotEqual', 0),
(47, 'wrongEmail', 0),
(48, 'emailNotRegistered', 0),
(49, 'wrongPasswordFormat', 0),
(50, 'wrongPassword', 0),
(51, 'sys_add', 1),
(52, 'sys_del', 1),
(53, 'sys_edit', 1),
(54, 'sys_activ', 1),
(55, 'sys_deactiv', 1),
(56, 'sys_save', 1),
(57, 'sys_sortAsc', 1),
(58, 'sys_sortDesc', 1),
(59, 'sys_all', 1),
(60, 'sys_filter', 1),
(61, 'sys_refresh', 1),
(62, 'sys_jan', 1),
(63, 'sys_feb', 1),
(64, 'sys_mar', 1),
(65, 'sys_apr', 1),
(66, 'sys_may', 1),
(67, 'sys_jun', 1),
(68, 'sys_jul', 1),
(69, 'sys_aug', 1),
(70, 'sys_sep', 1),
(71, 'sys_oct', 1),
(72, 'sys_nov', 1),
(73, 'sys_dec', 1),
(74, 'sys_mon', 1),
(75, 'sys_tue', 1),
(76, 'sys_wed', 1),
(77, 'sys_thu', 1),
(78, 'sys_fri', 1),
(79, 'sys_sat', 1),
(80, 'sys_sun', 1),
(81, 'sys_nouser', 1),
(82, 'sys_badpassword', 1),
(83, 'sys_noactive', 1),
(84, 'pageNotFound', 1),
(85, 'airlineSearch', 0),
(86, 'country', 0),
(87, 'town', 0),
(88, 'enter', 0),
(89, 'exit', 0),
(90, 'myProfile', 0),
(91, 'priceCalendar', 0),
(92, 'roomEquipment', 0),
(93, 'roomTypesNote', 0),
(94, 'roomTypesNoteLink', 0),
(95, 'generalInfo', 0),
(96, 'numberOfNights', 0),
(97, 'chooseRoomAndPrepayment', 0),
(98, 'choosePrepayment', 0),
(99, 'price', 0),
(100, 'priceComment', 0),
(101, 'bookingCost', 0),
(102, 'createTourByYourself', 0),
(103, 'roomDayPrice', 0),
(104, 'adultsInRoom', 0),
(105, 'childrenInRoom', 0),
(106, 'guestsInRoom', 0),
(107, 'checkGuestData', 0),
(108, 'checkChildrenData', 0),
(109, 'guestN', 0),
(110, 'childN', 0),
(111, 'showGuestN', 0),
(112, 'hideGuestN', 0),
(113, 'showChildN', 0),
(114, 'hideChildN', 0),
(115, 'phoneNum', 0),
(116, 'countryCode', 0),
(117, 'operCode', 0),
(118, 'chooseAdditionalServices', 0),
(119, 'personDay', 0),
(120, 'days', 0),
(121, 'add', 0),
(122, 'excursions', 0),
(123, 'monday', 0),
(124, 'tuesday', 0),
(125, 'wednesday', 0),
(126, 'thursday', 0),
(127, 'friday', 0),
(128, 'saturday', 0),
(129, 'sunday', 0),
(130, 'transferArrival', 0),
(131, 'transferDeparture', 0),
(132, 'optionsCost', 0),
(133, 'residenceOptionsCost', 0),
(134, 'roomPrepayment', 0),
(135, 'totalCost', 0),
(136, 'services', 0),
(137, 'residence', 0),
(138, 'discount', 0),
(139, 'payable', 0),
(140, 'paid', 0),
(141, 'restToPay', 0),
(142, 'payTotalCost', 0),
(143, 'payServices', 0),
(144, 'payResidence', 0),
(145, 'payAllResidence', 0),
(146, 'allResidence', 0),
(147, 'payFirstNight', 0),
(148, 'firstNight', 0),
(149, 'printAll', 0),
(150, 'printOffer', 0),
(151, 'cancel', 0),
(152, 'date', 0),
(153, 'time', 0),
(154, 'race', 0),
(155, 'wagon', 0),
(156, 'train', 0),
(157, 'back', 0),
(158, 'specifyAdditionalWishes', 0),
(159, 'delete', 0),
(160, 'endBooking', 0),
(161, 'save', 0),
(162, 'question', 0),
(163, 'sendYourQuestion', 0),
(164, 'answer', 0),
(165, 'leaveComment', 0),
(167, 'yourName', 0),
(168, 'yourEmail', 0),
(169, 'yourComment', 0),
(170, 'textFromPicture', 0),
(171, 'sendYourComment', 0),
(172, 'wrongGuestData', 0),
(173, 'addBookingError', 0),
(174, 'serverError', 0),
(175, 'message', 0),
(176, 'error', 0),
(177, 'yourBookingAdded', 0),
(178, 'fillAllTransferFields', 0),
(179, 'noArriveDate', 0),
(180, 'noDepartureDate', 0),
(182, 'fileDeleteSuccess', 0),
(183, 'changesApplied', 0),
(184, 'confirmCancel', 0),
(185, 'bookingCancelEnterPass', 0),
(186, 'servicesInVilla', 0),
(187, 'continueLbl', 0),
(188, 'enterSystem', 0),
(189, 'registration', 0),
(190, 'yourPassword', 0),
(191, 'doEnter', 0),
(192, 'forgotPassword', 0),
(193, 'yourPassword7', 0),
(195, 'doRegister', 0),
(196, 'registeredEmail', 0),
(197, 'sendNewPass', 0),
(198, 'saveEmailAndPass', 0),
(199, 'passNotEqual', 0),
(200, 'yourQuestionSentToAdmin', 0),
(201, 'yourCommentSentToAdmin', 0),
(202, 'answerWillBeSentToEmail', 0),
(203, 'sys_login_success', 1),
(204, 'myBookings', 0),
(205, 'personalData', 0),
(206, 'discounts', 0),
(207, 'passportScan', 0),
(208, 'changeEmail', 0),
(209, 'changePassword', 0),
(210, 'tourAgentData', 0),
(211, 'noFileForUpload', 0),
(212, 'userAccessOnly', 0),
(213, 'wrongUrl', 0),
(214, 'accessDenied', 0),
(215, 'bookingCalcelled', 0),
(216, 'bookingHistory', 0),
(217, 'creationDate', 0),
(218, 'changeDate', 0),
(219, 'roomType', 0),
(220, 'status', 0),
(221, 'edit', 0),
(222, 'changeArrivalDeparture', 0),
(223, 'close', 0),
(224, 'change', 0),
(225, 'uploadFile', 0),
(226, 'delCurrent', 0),
(227, 'payRequisites', 0),
(228, 'logo', 0),
(229, 'postalIndex', 0),
(230, 'address', 0),
(231, 'workCompany', 0),
(232, 'curEmail', 0),
(233, 'newEmail', 0),
(234, 'curPass', 0),
(235, 'newPass', 0),
(238, 'enterCurPassword', 0),
(239, 'enterNewPassword', 0),
(240, 'repeatNewPassword', 0),
(241, 'youCantChangeArrival', 0),
(242, 'pressToChangeArrival', 0),
(243, 'enterAnotherArrival', 0),
(244, 'noFreeRooms', 0),
(245, 'roomSearchSuccess', 0),
(246, 'hours', 0),
(247, 'minutes', 0),
(248, 'saving', 0),
(249, 'recalc', 0),
(250, 'dontForgetToResetTransfers', 0),
(251, 'saveAndExit', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tst_labels_lang`
--

CREATE TABLE IF NOT EXISTS `tst_labels_lang` (
  `id` int(11) unsigned DEFAULT NULL,
  `lang_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  KEY `fk_labels_lang_lang_id` (`lang_id`),
  KEY `fk_labels_lang_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='языковые данные labels';

--
-- Dumping data for table `tst_labels_lang`
--

INSERT INTO `tst_labels_lang` (`id`, `lang_id`, `name`) VALUES
(172, 2, 'wrongGuestData_en'),
(88, 2, 'enter_en'),
(35, 2, 'addQuestion_en'),
(2, 2, 'aboutHotel_en'),
(121, 2, 'add_en'),
(173, 2, 'addBookingError_en'),
(35, 2, 'addQuestion_en'),
(13, 2, 'adults_en'),
(104, 2, 'adultsInRoom_en'),
(85, 2, 'airlineSearch_en'),
(146, 2, 'allResidence_en'),
(164, 2, 'answer_en'),
(28, 2, 'arrive_en'),
(10, 2, 'arriveDate_en'),
(157, 2, 'back_en'),
(5, 2, 'backCall_en'),
(27, 2, 'booking_en'),
(185, 2, 'bookingCancelEnterPass_en'),
(101, 2, 'bookingCost_en'),
(9, 2, 'bookingNumber_en'),
(6, 2, 'callMe_en'),
(151, 2, 'cancel_en'),
(183, 2, 'changesApplied_en'),
(108, 2, 'checkChildrenData_en'),
(107, 2, 'checkGuestData_en'),
(26, 2, 'checkPresence_en'),
(110, 2, 'childN_en'),
(14, 2, 'children_en'),
(105, 2, 'childrenInRoom_en'),
(118, 2, 'chooseAdditionalServices_en'),
(98, 2, 'choosePrepayment_en'),
(97, 2, 'chooseRoomAndPrepayment_en'),
(19, 2, 'comment_en'),
(184, 2, 'confirmCancel_en'),
(4, 2, 'en contacts_en'),
(187, 2, 'continueLbl_en'),
(20, 2, 'cost_en'),
(86, 2, 'country_en'),
(116, 2, 'countryCode_en'),
(102, 2, 'createTourByYourself_en'),
(152, 2, 'date_en'),
(120, 2, 'days_en'),
(159, 2, 'delete_en'),
(29, 2, 'departure_en'),
(11, 2, 'departureDate_en'),
(24, 2, 'description_en'),
(33, 2, 'details_en'),
(138, 2, 'discount_en'),
(23, 2, 'doBooking_en'),
(191, 2, 'doEnter_en'),
(195, 2, 'doRegister_en'),
(17, 2, 'email_en'),
(48, 2, 'emailNotRegistered_en'),
(160, 2, 'endBooking_en'),
(88, 2, 'enter_en'),
(36, 2, 'enterEmail_en'),
(43, 2, 'enterName_en'),
(37, 2, 'enterPassword_en'),
(45, 2, 'enterPhone_en'),
(38, 2, 'enterQuestion_en'),
(44, 2, 'enterSurname_en'),
(188, 2, 'enterSystem_en'),
(25, 2, 'equipment_en'),
(176, 2, 'error_en'),
(7, 2, 'example_en'),
(122, 2, 'excursions_en'),
(89, 2, 'exit_en'),
(182, 2, 'fileDeleteSuccess_en'),
(178, 2, 'fillAllTransferFields_en'),
(40, 2, 'fillYourData_en'),
(148, 2, 'firstNight_en'),
(21, 2, 'forADay_en'),
(192, 2, 'forgotPassword_en'),
(22, 2, 'forWholeTerm_en'),
(127, 2, 'friday_en'),
(95, 2, 'generalInfo_en'),
(109, 2, 'guestN_en'),
(106, 2, 'guestsInRoom_en'),
(114, 2, 'hideChildN_en'),
(112, 2, 'hideGuestN_en'),
(32, 2, 'keepFeedback_en'),
(16, 2, 'lastName_en'),
(165, 2, 'leaveComment_en'),
(1, 2, 'login_en'),
(175, 2, 'message_en'),
(123, 2, 'monday_en'),
(90, 2, 'myProfile_en'),
(15, 2, 'name_en'),
(8, 2, 'newWindowMap_en'),
(179, 2, 'noArriveDate_en'),
(180, 2, 'noDepartureDate_en'),
(96, 2, 'numberOfNights_en'),
(3, 2, 'numbers_en'),
(117, 2, 'operCode_en'),
(132, 2, 'optionsCost_en'),
(84, 2, 'pageNotFound_en'),
(140, 2, 'paid_en'),
(199, 2, 'passNotEqual_en'),
(41, 2, 'password7chars_en'),
(46, 2, 'passwordsNotEqual_en'),
(139, 2, 'payable_en'),
(145, 2, 'payAllResidence_en'),
(147, 2, 'payFirstNight_en'),
(144, 2, 'payResidence_en'),
(143, 2, 'payServices_en'),
(142, 2, 'payTotalCost_en'),
(119, 2, 'personDay_en'),
(18, 2, 'phone_en'),
(115, 2, 'phoneNum_en'),
(99, 2, 'price_en'),
(91, 2, 'priceCalendar_en'),
(100, 2, 'priceComment_en'),
(149, 2, 'printAll_en'),
(150, 2, 'printOffer_en'),
(162, 2, 'question_en'),
(154, 2, 'race_en'),
(196, 2, 'registeredEmail_en'),
(189, 2, 'registration_en'),
(42, 2, 'repeatPassword_en'),
(137, 2, 'residence_en'),
(133, 2, 'residenceOptionsCost_en'),
(141, 2, 'restToPay_en'),
(103, 2, 'roomDayPrice_en'),
(92, 2, 'EN roomEquipment_en'),
(134, 2, 'roomPrepayment_en'),
(93, 2, 'roomTypesNote_en'),
(94, 2, 'roomTypesNoteLink_en'),
(128, 2, 'saturday_en'),
(161, 2, 'save_en'),
(198, 2, 'saveEmailAndPass_en'),
(34, 2, 'search_en'),
(39, 2, 'search_numbers_en'),
(197, 2, 'sendNewPass_en'),
(171, 2, 'sendYourComment_en'),
(163, 2, 'sendYourQuestion_en'),
(174, 2, 'serverError_en'),
(136, 2, 'services_en'),
(186, 2, 'servicesInVilla_en'),
(113, 2, 'showChildN_en'),
(111, 2, 'showGuestN_en'),
(158, 2, 'specifyAdditionalWishes_en'),
(129, 2, 'sunday_en'),
(54, 2, 'sys_activ_en'),
(51, 2, 'sys_add_en'),
(59, 2, 'sys_all_en'),
(65, 2, 'sys_apr_en'),
(69, 2, 'sys_aug_en'),
(82, 2, 'sys_badpassword_en'),
(55, 2, 'sys_deactiv_en'),
(73, 2, 'sys_dec_en'),
(52, 2, 'sys_del_en'),
(53, 2, 'sys_edit_en'),
(63, 2, 'sys_feb_en'),
(60, 2, 'sys_filter_en'),
(78, 2, 'sys_fri_en'),
(62, 2, 'sys_jan_en'),
(68, 2, 'sys_jul_en'),
(67, 2, 'sys_jun_en'),
(64, 2, 'sys_mar_en'),
(66, 2, 'sys_may_en'),
(74, 2, 'sys_mon_en'),
(83, 2, 'sys_noactive_en'),
(81, 2, 'sys_nouser_en'),
(72, 2, 'sys_nov_en'),
(71, 2, 'sys_oct_en'),
(61, 2, 'sys_refresh_en'),
(79, 2, 'sys_sat_en'),
(56, 2, 'sys_save_en'),
(70, 2, 'sys_sep_en'),
(57, 2, 'sys_sortAsc_en'),
(58, 2, 'sys_sortDesc_en'),
(80, 2, 'sys_sun_en'),
(77, 2, 'sys_thu_en'),
(75, 2, 'sys_tue_en'),
(76, 2, 'wed'),
(170, 2, 'textFromPicture_en'),
(126, 2, 'thursday_en'),
(153, 2, 'time_en'),
(135, 2, 'totalCost_en'),
(87, 2, 'town_en'),
(156, 2, 'train_en'),
(130, 2, 'transferArrival_en'),
(131, 2, 'transferDeparture_en'),
(124, 2, 'tuesday_en'),
(12, 2, 'type_en'),
(155, 2, 'wagon_en'),
(125, 2, 'wednesday_en'),
(47, 2, 'wrongEmail_en'),
(172, 2, 'wrongGuestData_en'),
(50, 2, 'wrongPassword_en'),
(49, 2, 'wrongPasswordFormat_en'),
(177, 2, 'yourBookingAdded_en'),
(169, 2, 'yourComment_en'),
(168, 2, 'yourEmail_en'),
(167, 2, 'yourName_en'),
(190, 2, 'yourPassword_en'),
(193, 2, 'yourPassword7_en'),
(2, 3, 'aboutHotel_de'),
(121, 3, 'add_de'),
(173, 3, 'addBookingError_de'),
(35, 3, 'addQuestion_de'),
(13, 3, 'adults_de'),
(104, 3, 'adultsInRoom_de'),
(85, 3, 'airlineSearch_de'),
(146, 3, 'allResidence_de'),
(164, 3, 'answer_de'),
(28, 3, 'arrive_de'),
(10, 3, 'arriveDate_de'),
(157, 3, 'back_de'),
(5, 3, 'backCall_de'),
(27, 3, 'booking_de'),
(185, 3, 'bookingCancelEnterPass_de'),
(101, 3, 'bookingCost_de'),
(9, 3, 'bookingNumber_de'),
(6, 3, 'callMe_de'),
(151, 3, 'cancel_de'),
(183, 3, 'changesApplied_de'),
(108, 3, 'checkChildrenData_de'),
(107, 3, 'checkGuestData_de'),
(26, 3, 'checkPresence_de'),
(110, 3, 'childN_de'),
(14, 3, 'children_de'),
(105, 3, 'childrenInRoom_de'),
(118, 3, 'chooseAdditionalServices_de'),
(98, 3, 'choosePrepayment_de'),
(97, 3, 'chooseRoomAndPrepayment_de'),
(19, 3, 'comment_de'),
(184, 3, 'confirmCancel_de'),
(4, 3, 'contacts_de'),
(187, 3, 'continueLbl_de'),
(20, 3, 'cost_de'),
(86, 3, 'country_de'),
(116, 3, 'countryCode_de'),
(102, 3, 'createTourByYourself_de'),
(152, 3, 'date_de'),
(120, 3, 'days_de'),
(159, 3, 'delete_de'),
(29, 3, 'departure_de'),
(11, 3, 'departureDate_de'),
(24, 3, 'description_de'),
(33, 3, 'details_de'),
(138, 3, 'discount_de'),
(23, 3, 'doBooking_de'),
(191, 3, 'doEnter_de'),
(195, 3, 'doRegister_de'),
(17, 3, 'email_de'),
(48, 3, 'emailNotRegistered_de'),
(160, 3, 'endBooking_de'),
(88, 3, 'enter_de'),
(36, 3, 'enterEmail_de'),
(43, 3, 'enterName_de'),
(37, 3, 'enterPassword_de'),
(45, 3, 'enterPhone_de'),
(38, 3, 'enterQuestion_de'),
(44, 3, 'enterSurname_de'),
(188, 3, 'enterSystem_de'),
(25, 3, 'equipment_de'),
(176, 3, 'error_de'),
(7, 3, 'example_de'),
(122, 3, 'excursions_de'),
(89, 3, 'exit_de'),
(182, 3, 'fileDeleteSuccess_de'),
(178, 3, 'fillAllTransferFields_de'),
(40, 3, 'fillYourData_de'),
(148, 3, 'firstNight_de'),
(21, 3, 'forADay_de'),
(192, 3, 'forgotPassword_de'),
(22, 3, 'forWholeTerm_de'),
(127, 3, 'friday_de'),
(95, 3, 'generalInfo_de'),
(109, 3, 'guestN_de'),
(106, 3, 'guestsInRoom_de'),
(114, 3, 'hideChildN_de'),
(112, 3, 'hideGuestN_de'),
(32, 3, 'keepFeedback_de'),
(16, 3, 'lastName_de'),
(165, 3, 'leaveComment_de'),
(1, 3, 'login_de'),
(175, 3, 'message_de'),
(123, 3, 'monday_de'),
(90, 3, 'myProfile_de'),
(15, 3, 'name_de'),
(8, 3, 'newWindowMap_de'),
(179, 3, 'noArriveDate_de'),
(180, 3, 'noDepartureDate_de'),
(96, 3, 'numberOfNights_de'),
(3, 3, 'numbers_de'),
(117, 3, 'operCode_de'),
(132, 3, 'optionsCost_de'),
(84, 3, 'pageNotFound_de'),
(140, 3, 'paid_de'),
(199, 3, 'passNotEqual_de'),
(41, 3, 'password7chars_de'),
(46, 3, 'passwordsNotEqual_de'),
(139, 3, 'payable_de'),
(145, 3, 'payAllResidence_de'),
(147, 3, 'payFirstNight_de'),
(144, 3, 'payResidence_de'),
(143, 3, 'payServices_de'),
(142, 3, 'payTotalCost_de'),
(119, 3, 'personDay_de'),
(18, 3, 'phone_de'),
(115, 3, 'phoneNum_de'),
(99, 3, 'price_de'),
(91, 3, 'priceCalendar_de'),
(100, 3, 'priceComment_de'),
(149, 3, 'printAll_de'),
(150, 3, 'printOffer_de'),
(162, 3, 'question_de'),
(154, 3, 'race_de'),
(196, 3, 'registeredEmail_de'),
(189, 3, 'registration_de'),
(42, 3, 'repeatPassword_de'),
(137, 3, 'residence_de'),
(133, 3, 'residenceOptionsCost_de'),
(141, 3, 'restToPay_de'),
(103, 3, 'roomDayPrice_de'),
(92, 3, 'roomEquipment_de'),
(134, 3, 'roomPrepayment_de'),
(93, 3, 'roomTypesNote_de'),
(94, 3, 'roomTypesNoteLink_de'),
(128, 3, 'saturday_de'),
(161, 3, 'save_de'),
(198, 3, 'saveEmailAndPass_de'),
(34, 3, 'search_de'),
(39, 3, 'search_numbers_de'),
(197, 3, 'sendNewPass_de'),
(171, 3, 'sendYourComment_de'),
(163, 3, 'sendYourQuestion_de'),
(174, 3, 'serverError_de'),
(136, 3, 'services_de'),
(186, 3, 'servicesInVilla_de'),
(113, 3, 'showChildN_de'),
(111, 3, 'showGuestN_de'),
(158, 3, 'specifyAdditionalWishes_de'),
(129, 3, 'sunday_de'),
(54, 3, 'sys_activ_de'),
(51, 3, 'sys_add_de'),
(59, 3, 'sys_all_de'),
(65, 3, 'sys_apr_de'),
(69, 3, 'sys_aug_de'),
(82, 3, 'sys_badpassword_de'),
(55, 3, 'sys_deactiv_de'),
(73, 3, 'sys_dec_de'),
(52, 3, 'sys_del_de'),
(53, 3, 'sys_edit_de'),
(63, 3, 'sys_feb_de'),
(60, 3, 'sys_filter_de'),
(78, 3, 'sys_fri_de'),
(62, 3, 'sys_jan_de'),
(68, 3, 'sys_jul_de'),
(67, 3, 'sys_jun_de'),
(64, 3, 'sys_mar_de'),
(66, 3, 'sys_may_de'),
(74, 3, 'sys_mon_de'),
(83, 3, 'sys_noactive_de'),
(81, 3, 'sys_nouser_de'),
(72, 3, 'sys_nov_de'),
(71, 3, 'sys_oct_de'),
(61, 3, 'sys_refresh_de'),
(79, 3, 'sys_sat_de'),
(56, 3, 'sys_save_de'),
(70, 3, 'sys_sep_de'),
(57, 3, 'sys_sortAsc_de'),
(58, 3, 'sys_sortDesc_de'),
(80, 3, 'sys_sun_de'),
(77, 3, 'sys_thu_de'),
(75, 3, 'sys_tue_de'),
(76, 3, 'sys_wed_de'),
(170, 3, 'textFromPicture_de'),
(126, 3, 'thursday_de'),
(153, 3, 'time_de'),
(135, 3, 'totalCost_de'),
(87, 3, 'town_de'),
(156, 3, 'train_de'),
(130, 3, 'transferArrival_de'),
(131, 3, 'transferDeparture_de'),
(124, 3, 'tuesday_de'),
(12, 3, 'type_de'),
(155, 3, 'wagon_de'),
(125, 3, 'wednesday_de'),
(47, 3, 'wrongEmail_de'),
(172, 3, 'wrongGuestData_de'),
(50, 3, 'wrongPassword_de'),
(49, 3, 'wrongPasswordFormat_de'),
(177, 3, 'yourBookingAdded_de'),
(169, 3, 'yourComment_de'),
(168, 3, 'yourEmail_de'),
(167, 3, 'yourName_de'),
(190, 3, 'yourPassword_de'),
(193, 3, 'yourPassword7_de'),
(203, 2, 'sys_login_success_en'),
(203, 3, 'sys_login_success_de'),
(204, 2, 'myBookings_en'),
(204, 3, 'myBookings_de'),
(205, 2, 'personalData_en'),
(205, 3, 'personalData_de'),
(206, 2, 'discounts_en'),
(206, 3, 'discounts_de'),
(207, 2, 'passportScan_en'),
(207, 3, 'passportScan_de'),
(208, 2, 'changeEmail_en'),
(208, 3, 'changeEmail_de'),
(209, 2, 'changePassword_en'),
(209, 3, 'changePassword_de'),
(210, 2, 'tourAgentData_en'),
(210, 3, 'tourAgentData_de'),
(211, 2, 'noFileForUpload_en'),
(211, 3, 'noFileForUpload_de'),
(212, 2, 'userAccessOnly_en'),
(212, 3, 'userAccessOnly_de'),
(213, 2, 'wrongUrl_en'),
(213, 3, 'wrongUrl_de'),
(214, 2, 'accessDenied_en'),
(214, 3, 'accessDenied_de'),
(215, 2, 'bookingCalcelled_en'),
(215, 3, 'bookingCalcelled_de'),
(216, 2, 'bookingHistory_en'),
(216, 3, 'bookingHistory_de'),
(217, 2, 'creationDate_en'),
(217, 3, 'creationDate_de'),
(218, 2, 'changeDate_en'),
(218, 3, 'changeDate_de'),
(219, 2, 'roomType_en'),
(219, 3, 'roomType_de'),
(220, 2, 'status_en'),
(220, 3, 'status_de'),
(221, 2, 'edit_en'),
(221, 3, 'edit_de'),
(222, 2, 'changeArriveDate_en'),
(222, 3, 'changeArriveDate_de'),
(223, 2, 'close_en'),
(223, 3, 'close_de'),
(224, 2, 'change_en'),
(224, 3, 'change_de'),
(225, 2, 'uploadFile_en'),
(225, 3, 'uploadFile_de'),
(226, 2, 'delCurrent_en'),
(226, 3, 'delCurrent_de'),
(227, 2, 'payRequisites_en'),
(227, 3, 'payRequisites_de'),
(228, 2, 'logo_en'),
(228, 3, 'logo_de'),
(229, 2, 'postalIndex_en'),
(229, 3, 'postalIndex_de'),
(230, 2, 'address_en'),
(230, 3, 'address_de'),
(231, 2, 'workCompany_en'),
(231, 3, 'workCompany_de'),
(232, 2, 'curEmail_en'),
(232, 3, 'curEmail_de'),
(233, 2, 'newEmail_en'),
(233, 3, 'newEmail_de'),
(234, 2, 'curPass_en'),
(234, 3, 'curPass_de'),
(235, 2, 'newPass_en'),
(235, 3, 'newPass_de'),
(238, 2, 'enterCurPassword_en'),
(238, 3, 'enterCurPassword_de'),
(239, 2, 'enterNewPassword_en'),
(239, 3, 'enterNewPassword_de'),
(240, 2, 'repeatNewPassword_en'),
(240, 3, 'repeatNewPassword_de'),
(241, 2, 'youCantChangeArrival_en'),
(241, 3, 'youCantChangeArrival_de'),
(242, 2, 'pressToChangeArrival_en'),
(242, 3, 'pressToChangeArrival_de'),
(243, 2, 'enterAnotherArrival_en'),
(243, 3, 'enterAnotherArrival_de'),
(244, 2, 'noFreeRooms_en'),
(244, 3, 'noFreeRooms_de'),
(245, 2, 'roomSearchSuccess_en'),
(245, 3, 'roomSearchSuccess_de'),
(246, 2, 'hours_en'),
(246, 3, 'hours_de'),
(247, 2, 'minutes_en'),
(247, 3, 'minutes_de'),
(248, 2, 'saving_en'),
(248, 3, 'saving_de'),
(249, 2, 'recalc_en'),
(249, 3, 'recalc_de'),
(1, 1, 'Логин'),
(2, 1, 'Об отеле'),
(3, 1, 'Номера'),
(4, 1, 'Контакты'),
(5, 1, 'обратный звонок'),
(6, 1, 'перезвоните мне'),
(7, 1, 'Например'),
(8, 1, 'Карта в отдельном окне'),
(9, 1, 'Бронировать номер'),
(10, 1, 'Дата заезда'),
(11, 1, 'Дата выезда'),
(12, 1, 'Тип'),
(13, 1, 'Взрослые'),
(14, 1, 'Дети 3-7 лет'),
(15, 1, 'Имя'),
(16, 1, 'Фамилия'),
(17, 1, 'Email'),
(18, 1, 'Телефон'),
(19, 1, 'Комментарий'),
(20, 1, 'Стоимость'),
(21, 1, 'Стоимость'),
(22, 1, 'за весь период'),
(23, 1, 'Забронировать'),
(24, 1, 'Описание'),
(25, 1, 'Номер укомплектован'),
(26, 1, 'Бронировать'),
(27, 1, 'Бронирование'),
(28, 1, 'Заезд'),
(29, 1, 'Выезд'),
(32, 1, 'оставить отзыв'),
(33, 1, 'подробнее'),
(34, 1, 'Поиск'),
(35, 1, 'Задать вопрос'),
(36, 1, 'Введите Ваш E-mail'),
(37, 1, 'Введите Ваш пароль'),
(38, 1, 'Введите Ваш вопрос'),
(39, 1, 'поиск номеров'),
(40, 1, 'Заполните ваши данные'),
(41, 1, 'Пароль (7 знаков)'),
(42, 1, 'Повторите пароль'),
(43, 1, 'Введите имя'),
(44, 1, 'Введите фамилию'),
(45, 1, 'Введите телефон'),
(46, 1, 'Пароли не совпадают'),
(47, 1, 'Неверный E-mail!'),
(48, 1, 'E-mail не зарегистрирован!'),
(49, 1, 'Неверный формат пароля!'),
(50, 1, 'Неверный пароль!'),
(53, 1, 'Ред.'),
(54, 1, 'Активировать'),
(55, 1, 'Деактивировать'),
(56, 1, 'Сохранить'),
(57, 1, 'Сортировать по возрастанию'),
(58, 1, 'Сортировать по убыванию'),
(59, 1, 'Все'),
(60, 1, 'Фильтр'),
(61, 1, 'Обновить'),
(62, 1, 'Янв'),
(63, 1, 'Фев'),
(64, 1, 'Мар'),
(65, 1, 'Апр'),
(66, 1, 'Май'),
(67, 1, 'Июн'),
(68, 1, 'Июл'),
(69, 1, 'Авг'),
(70, 1, 'Сен'),
(71, 1, 'Окт'),
(72, 1, 'Ноя'),
(73, 1, 'Дек'),
(74, 1, 'Пн'),
(75, 1, 'Вт'),
(76, 1, 'Ср'),
(77, 1, 'Чт'),
(78, 1, 'Пт'),
(79, 1, 'Сб'),
(80, 1, 'Вс'),
(81, 1, 'Такого пользователя нет'),
(82, 1, 'Неверно набран пароль'),
(83, 1, 'Пользователь не активен. Обратитесь к администратору сайта.'),
(84, 1, 'Запрашиваемая страница не найдена'),
(51, 1, 'Добавить'),
(85, 1, 'Поиск Авиакомпании'),
(86, 1, 'Страна'),
(87, 1, 'Город'),
(88, 1, 'Вход'),
(89, 1, 'Выход'),
(90, 1, 'Мой профиль'),
(91, 1, 'Календарь цен'),
(92, 1, 'Оборудование номера'),
(93, 1, 'цены приведены на текущее время, для просмотра цен в интересующие вас даты воспользуйтесь календарем цен или заполните форму бронирования '),
(94, 1, 'форму бронирования'),
(95, 1, 'Общая информация'),
(96, 1, 'Количество ночей'),
(97, 1, 'Выберите тип номера и вид предоплаты'),
(98, 1, 'Выберите вид предоплаты'),
(99, 1, 'Цена'),
(100, 1, 'Цены приведены за номер в сутки на текущее время'),
(101, 1, 'Стоимость бронирования'),
(102, 1, 'Создайте тур своими руками'),
(103, 1, 'Стоимость номера (в сутки)'),
(104, 1, 'Взрослых в номере'),
(105, 1, 'Детей в номере'),
(106, 1, 'Общее количество проживающих в номере'),
(107, 1, 'Пожалуйста, проверьте данные проживающих в номере'),
(108, 1, 'Пожалуйста, проверьте данные о детях, проживающих в номере'),
(109, 1, 'Гость №'),
(110, 1, 'Ребенок №'),
(111, 1, 'Показать гостя №'),
(112, 1, 'Скрыть гостя №'),
(113, 1, 'Показать ребенка №'),
(114, 1, 'Скрыть ребенка №'),
(115, 1, 'Номер телефона'),
(116, 1, 'Код страны'),
(117, 1, 'Код оператора'),
(118, 1, 'Для комфортного отдыха выберите дополнительные услуги'),
(119, 1, 'с человека в день'),
(120, 1, 'дней'),
(121, 1, 'Добавить'),
(122, 1, 'Экскурсии'),
(123, 1, 'Понедельник'),
(124, 1, 'Вторник'),
(125, 1, 'Среда'),
(126, 1, 'Четверг'),
(127, 1, 'Пятница'),
(128, 1, 'Суббота'),
(129, 1, 'Воскресенье'),
(130, 1, 'Трансфер - прибытие'),
(131, 1, 'Трансфер - отправление'),
(132, 1, 'Стоимость услуг'),
(133, 1, 'Стоимость проживания с услугами'),
(134, 1, 'Предоплата за номер'),
(135, 1, 'Общая стоимость'),
(136, 1, 'Услуги'),
(137, 1, 'Проживание'),
(138, 1, 'Скидка'),
(139, 1, 'К оплате'),
(140, 1, 'Оплачено'),
(141, 1, 'Остаток к оплате'),
(142, 1, 'Оплатить общую стоимость'),
(143, 1, 'Оплатить услуги'),
(144, 1, 'Оплатить проживание'),
(145, 1, 'Оплатить проживание за весь период'),
(146, 1, 'Весь период'),
(147, 1, 'Оплатить стоимость первой ночи'),
(148, 1, 'Первая ночь'),
(149, 1, 'Печатать все'),
(150, 1, 'Печатать предложение'),
(151, 1, 'Отменить'),
(152, 1, 'Дата'),
(153, 1, 'Время'),
(154, 1, 'Рейс'),
(155, 1, 'Вагон'),
(156, 1, 'Поезд'),
(157, 1, 'Назад'),
(158, 1, 'Укажите свои дополнительные пожелания'),
(52, 1, 'Удалить'),
(159, 1, 'Удалить'),
(160, 1, 'Завершить бронирование'),
(161, 1, 'Сохранить'),
(162, 1, 'Вопрос'),
(163, 1, 'Отправить ваш вопрос'),
(164, 1, 'Ответ'),
(165, 1, 'Оставить отзыв'),
(167, 1, 'Ваше имя'),
(168, 1, 'Ваш email'),
(169, 1, 'Ваш отзыв'),
(170, 1, 'Текст с картинки'),
(171, 1, 'Отправить ваш отзыв'),
(172, 1, 'Неверно заполнены личные данные гостей'),
(173, 1, 'Ошибка при добавлении бронирования'),
(174, 1, 'Ошибка на сервере'),
(175, 1, 'Сообщение'),
(176, 1, 'Ошибка'),
(177, 1, 'Поздравляем! Ваше бронирование сохранено. Номер брони: '),
(178, 1, 'Заполните все поля, необходимые для сохранения трансфера!'),
(179, 1, 'Не задана дата заезда'),
(180, 1, 'Не задана дата выезда'),
(182, 1, 'Файл успешно удален'),
(183, 1, 'Изменения сохранены'),
(184, 1, 'Подтвердите отмену'),
(185, 1, 'Для подтверждения отмены бронирования введите свой пароль:'),
(186, 1, 'Услуги в Villa LaScala'),
(172, 2, 'wrongGuestData_en'),
(88, 2, 'enter_en'),
(35, 2, 'addQuestion_en'),
(187, 1, 'Продолжить'),
(188, 1, 'Вход в систему'),
(189, 1, 'Регистрация'),
(190, 1, 'Ваш пароль'),
(191, 1, 'Войти'),
(192, 1, 'Забыли пароль?'),
(193, 1, 'Ваш пароль (7 знаков)'),
(195, 1, 'Зарегистрировать'),
(196, 1, 'Зарегистрированный E-mail'),
(197, 1, 'Отправить новый пароль'),
(198, 1, 'Сохранить e-mail и пароль'),
(199, 1, 'Пароли не совпадают'),
(1, 1, 'Логин'),
(2, 1, 'Об отеле'),
(3, 1, 'Номера'),
(4, 1, 'Контакты'),
(5, 1, 'обратный звонок'),
(6, 1, 'перезвоните мне'),
(7, 1, 'Например'),
(8, 1, 'Карта в отдельном окне'),
(9, 1, 'Бронировать номер'),
(10, 1, 'Дата заезда'),
(11, 1, 'Дата выезда'),
(12, 1, 'Тип'),
(13, 1, 'Взрослые'),
(14, 1, 'Дети 3-7 лет'),
(15, 1, 'Имя'),
(16, 1, 'Фамилия'),
(17, 1, 'Email'),
(18, 1, 'Телефон'),
(19, 1, 'Комментарий'),
(20, 1, 'Стоимость'),
(21, 1, 'Стоимость'),
(22, 1, 'за весь период'),
(23, 1, 'Забронировать'),
(24, 1, 'Описание'),
(25, 1, 'Номер укомплектован'),
(26, 1, 'Бронировать'),
(27, 1, 'Бронирование'),
(28, 1, 'Заезд'),
(29, 1, 'Выезд'),
(32, 1, 'оставить отзыв'),
(33, 1, 'подробнее'),
(34, 1, 'Поиск'),
(35, 1, 'Задать вопрос'),
(36, 1, 'Введите Ваш E-mail'),
(37, 1, 'Введите Ваш пароль'),
(38, 1, 'Введите Ваш вопрос'),
(39, 1, 'поиск номеров'),
(40, 1, 'Заполните ваши данные'),
(41, 1, 'Пароль (7 знаков)'),
(42, 1, 'Повторите пароль'),
(43, 1, 'Введите имя'),
(44, 1, 'Введите фамилию'),
(45, 1, 'Введите телефон'),
(46, 1, 'Пароли не совпадают'),
(47, 1, 'Неверный E-mail!'),
(48, 1, 'E-mail не зарегистрирован!'),
(49, 1, 'Неверный формат пароля!'),
(50, 1, 'Неверный пароль!'),
(53, 1, 'Ред.'),
(54, 1, 'Активировать'),
(55, 1, 'Деактивировать'),
(56, 1, 'Сохранить'),
(57, 1, 'Сортировать по возрастанию'),
(58, 1, 'Сортировать по убыванию'),
(59, 1, 'Все'),
(60, 1, 'Фильтр'),
(61, 1, 'Обновить'),
(62, 1, 'Янв'),
(63, 1, 'Фев'),
(64, 1, 'Мар'),
(65, 1, 'Апр'),
(66, 1, 'Май'),
(67, 1, 'Июн'),
(68, 1, 'Июл'),
(69, 1, 'Авг'),
(70, 1, 'Сен'),
(71, 1, 'Окт'),
(72, 1, 'Ноя'),
(73, 1, 'Дек'),
(74, 1, 'Пн'),
(75, 1, 'Вт'),
(76, 1, 'Ср'),
(77, 1, 'Чт'),
(78, 1, 'Пт'),
(79, 1, 'Сб'),
(80, 1, 'Вс'),
(81, 1, 'Такого пользователя нет'),
(82, 1, 'Неверно набран пароль'),
(83, 1, 'Пользователь не активен. Обратитесь к администратору сайта.'),
(84, 1, 'Запрашиваемая страница не найдена'),
(51, 1, 'Добавить'),
(85, 1, 'Поиск Авиакомпании'),
(86, 1, 'Страна'),
(87, 1, 'Город'),
(88, 1, 'Вход'),
(89, 1, 'Выход'),
(90, 1, 'Мой профиль'),
(91, 1, 'Календарь цен'),
(92, 1, 'Оборудование номера'),
(93, 1, 'цены приведены на текущее время, для просмотра цен в интересующие вас даты воспользуйтесь календарем цен или заполните форму бронирования '),
(94, 1, 'форму бронирования'),
(95, 1, 'Общая информация'),
(96, 1, 'Количество ночей'),
(97, 1, 'Выберите тип номера и вид предоплаты'),
(98, 1, 'Выберите вид предоплаты'),
(99, 1, 'Цена'),
(100, 1, 'Цены приведены за номер в сутки на текущее время'),
(101, 1, 'Стоимость бронирования'),
(102, 1, 'Создайте тур своими руками'),
(103, 1, 'Стоимость номера (в сутки)'),
(104, 1, 'Взрослых в номере'),
(105, 1, 'Детей в номере'),
(106, 1, 'Общее количество проживающих в номере'),
(107, 1, 'Пожалуйста, проверьте данные проживающих в номере'),
(108, 1, 'Пожалуйста, проверьте данные о детях, проживающих в номере'),
(109, 1, 'Гость №'),
(110, 1, 'Ребенок №'),
(111, 1, 'Показать гостя №'),
(112, 1, 'Скрыть гостя №'),
(113, 1, 'Показать ребенка №'),
(114, 1, 'Скрыть ребенка №'),
(115, 1, 'Номер телефона'),
(116, 1, 'Код страны'),
(117, 1, 'Код оператора'),
(118, 1, 'Для комфортного отдыха выберите дополнительные услуги'),
(119, 1, 'с человека в день'),
(120, 1, 'дней'),
(121, 1, 'Добавить'),
(122, 1, 'Экскурсии'),
(123, 1, 'Понедельник'),
(124, 1, 'Вторник'),
(125, 1, 'Среда'),
(126, 1, 'Четверг'),
(127, 1, 'Пятница'),
(128, 1, 'Суббота'),
(129, 1, 'Воскресенье'),
(130, 1, 'Трансфер - прибытие'),
(131, 1, 'Трансфер - отправление'),
(132, 1, 'Стоимость услуг'),
(133, 1, 'Стоимость проживания с услугами'),
(134, 1, 'Предоплата за номер'),
(135, 1, 'Общая стоимость'),
(136, 1, 'Услуги'),
(137, 1, 'Проживание'),
(138, 1, 'Скидка'),
(139, 1, 'К оплате'),
(140, 1, 'Оплачено'),
(141, 1, 'Остаток к оплате'),
(142, 1, 'Оплатить общую стоимость'),
(143, 1, 'Оплатить услуги'),
(144, 1, 'Оплатить проживание'),
(145, 1, 'Оплатить проживание за весь период'),
(146, 1, 'Весь период'),
(147, 1, 'Оплатить стоимость первой ночи'),
(148, 1, 'Первая ночь'),
(149, 1, 'Печатать все'),
(150, 1, 'Печатать предложение'),
(151, 1, 'Отменить'),
(152, 1, 'Дата'),
(153, 1, 'Время'),
(154, 1, 'Рейс'),
(155, 1, 'Вагон'),
(156, 1, 'Поезд'),
(157, 1, 'Назад'),
(158, 1, 'Укажите свои дополнительные пожелания'),
(52, 1, 'Удалить'),
(159, 1, 'Удалить'),
(160, 1, 'Завершить бронирование'),
(161, 1, 'Сохранить'),
(162, 1, 'Вопрос'),
(163, 1, 'Отправить ваш вопрос'),
(164, 1, 'Ответ'),
(165, 1, 'Оставить отзыв'),
(167, 1, 'Ваше имя'),
(168, 1, 'Ваш email'),
(169, 1, 'Ваш отзыв'),
(170, 1, 'Текст с картинки'),
(171, 1, 'Отправить ваш отзыв'),
(172, 1, 'Неверно заполнены личные данные гостей'),
(173, 1, 'Ошибка при добавлении бронирования'),
(174, 1, 'Ошибка на сервере'),
(175, 1, 'Сообщение'),
(176, 1, 'Ошибка'),
(177, 1, 'Поздравляем! Ваше бронирование сохранено. Номер брони: '),
(178, 1, 'Заполните все поля, необходимые для сохранения трансфера!'),
(179, 1, 'Не задана дата заезда'),
(180, 1, 'Не задана дата выезда'),
(182, 1, 'Файл успешно удален'),
(183, 1, 'Изменения сохранены'),
(184, 1, 'Подтвердите отмену'),
(185, 1, 'Для подтверждения отмены бронирования введите свой пароль:'),
(186, 1, 'Услуги в Villa LaScala'),
(187, 1, 'Продолжить'),
(188, 1, 'Вход в систему'),
(189, 1, 'Регистрация'),
(190, 1, 'Ваш пароль'),
(191, 1, 'Войти'),
(192, 1, 'Забыли пароль?'),
(193, 1, 'Ваш пароль (7 знаков)'),
(195, 1, 'Зарегистрировать'),
(196, 1, 'Зарегистрированный E-mail'),
(197, 1, 'Отправить новый пароль'),
(198, 1, 'Сохранить e-mail и пароль'),
(199, 1, 'Пароли не совпадают'),
(200, 1, 'Ваш вопрос поступил администратору.'),
(201, 1, 'Ваш отзыв поступил администратору.'),
(202, 1, 'Ответ Вы получите на указанный E-mail'),
(203, 1, 'Вы успешно авторизованы'),
(204, 1, 'Мои бронирования'),
(205, 1, 'Личные данные'),
(206, 1, 'Скидки'),
(207, 1, 'Скан паспорта'),
(208, 1, 'Изменить E-mail'),
(209, 1, 'Изменить пароль'),
(210, 1, 'Данные турагента'),
(211, 1, 'Нет файла для загрузки'),
(212, 1, 'Страница доступна только зарегистированным пользователям'),
(213, 1, 'Неверный url'),
(214, 1, 'Отказано в доступе'),
(215, 1, 'Бронирование #{id} отменено'),
(216, 1, 'История бронирования'),
(217, 1, 'Дата создания'),
(218, 1, 'Дата изменения'),
(219, 1, 'Тип номера'),
(220, 1, 'Статус'),
(221, 1, 'Редактировать'),
(222, 1, 'Изменить дату заезда / выезда'),
(223, 1, 'Закрыть'),
(224, 1, 'Изменить'),
(225, 1, 'Загрузить файл'),
(226, 1, 'Удалить текущий'),
(227, 1, 'Платежные реквизиты'),
(228, 1, 'Логотип'),
(229, 1, 'Почтовый индекс'),
(230, 1, 'Адрес'),
(231, 1, 'Компания (место работы)'),
(232, 1, 'Текущий E-mail'),
(233, 1, 'Новый E-mail'),
(234, 1, 'Текущий пароль'),
(235, 1, 'Новый пароль'),
(238, 1, 'Введите текущий пароль'),
(239, 1, 'Введите новый пароль'),
(240, 1, 'Повторите новый пароль'),
(241, 1, 'Вы не можете изменить дату заезда при текущих условиях'),
(1, 1, 'Логин'),
(2, 1, 'Об отеле'),
(3, 1, 'Номера'),
(4, 1, 'Контакты'),
(5, 1, 'обратный звонок'),
(6, 1, 'перезвоните мне'),
(7, 1, 'Например'),
(8, 1, 'Карта в отдельном окне'),
(9, 1, 'Бронировать номер'),
(10, 1, 'Дата заезда'),
(11, 1, 'Дата выезда'),
(12, 1, 'Тип'),
(13, 1, 'Взрослые'),
(14, 1, 'Дети 3-7 лет'),
(15, 1, 'Имя'),
(16, 1, 'Фамилия'),
(17, 1, 'Email'),
(18, 1, 'Телефон'),
(19, 1, 'Комментарий'),
(20, 1, 'Стоимость'),
(21, 1, 'Стоимость'),
(22, 1, 'за весь период'),
(23, 1, 'Забронировать'),
(24, 1, 'Описание'),
(25, 1, 'Номер укомплектован'),
(26, 1, 'Бронировать'),
(27, 1, 'Бронирование'),
(28, 1, 'Заезд'),
(29, 1, 'Выезд'),
(32, 1, 'оставить отзыв'),
(33, 1, 'подробнее'),
(34, 1, 'Поиск'),
(35, 1, 'Задать вопрос'),
(36, 1, 'Введите Ваш E-mail'),
(37, 1, 'Введите Ваш пароль'),
(38, 1, 'Введите Ваш вопрос'),
(39, 1, 'поиск номеров'),
(40, 1, 'Заполните ваши данные'),
(41, 1, 'Пароль (7 знаков)'),
(42, 1, 'Повторите пароль'),
(43, 1, 'Введите имя'),
(44, 1, 'Введите фамилию'),
(45, 1, 'Введите телефон'),
(46, 1, 'Пароли не совпадают'),
(47, 1, 'Неверный E-mail!'),
(48, 1, 'E-mail не зарегистрирован!'),
(49, 1, 'Неверный формат пароля!'),
(50, 1, 'Неверный пароль!'),
(53, 1, 'Ред.'),
(54, 1, 'Активировать'),
(55, 1, 'Деактивировать'),
(56, 1, 'Сохранить'),
(57, 1, 'Сортировать по возрастанию'),
(58, 1, 'Сортировать по убыванию'),
(59, 1, 'Все'),
(60, 1, 'Фильтр'),
(61, 1, 'Обновить'),
(62, 1, 'Янв'),
(63, 1, 'Фев'),
(64, 1, 'Мар'),
(65, 1, 'Апр'),
(66, 1, 'Май'),
(67, 1, 'Июн'),
(68, 1, 'Июл'),
(69, 1, 'Авг'),
(70, 1, 'Сен'),
(71, 1, 'Окт'),
(72, 1, 'Ноя'),
(73, 1, 'Дек'),
(74, 1, 'Пн'),
(75, 1, 'Вт'),
(76, 1, 'Ср'),
(77, 1, 'Чт'),
(78, 1, 'Пт'),
(79, 1, 'Сб'),
(80, 1, 'Вс'),
(81, 1, 'Такого пользователя нет'),
(82, 1, 'Неверно набран пароль'),
(83, 1, 'Пользователь не активен. Обратитесь к администратору сайта.'),
(84, 1, 'Запрашиваемая страница не найдена'),
(51, 1, 'Добавить'),
(85, 1, 'Поиск Авиакомпании'),
(86, 1, 'Страна'),
(87, 1, 'Город'),
(88, 1, 'Вход'),
(89, 1, 'Выход'),
(90, 1, 'Мой профиль'),
(91, 1, 'Календарь цен'),
(92, 1, 'Оборудование номера'),
(93, 1, 'цены приведены на текущее время, для просмотра цен в интересующие вас даты воспользуйтесь календарем цен или заполните форму бронирования '),
(94, 1, 'форму бронирования'),
(95, 1, 'Общая информация'),
(96, 1, 'Количество ночей'),
(97, 1, 'Выберите тип номера и вид предоплаты'),
(98, 1, 'Выберите вид предоплаты'),
(99, 1, 'Цена'),
(100, 1, 'Цены приведены за номер в сутки на текущее время'),
(101, 1, 'Стоимость бронирования'),
(102, 1, 'Создайте тур своими руками'),
(103, 1, 'Стоимость номера (в сутки)'),
(104, 1, 'Взрослых в номере'),
(105, 1, 'Детей в номере'),
(106, 1, 'Общее количество проживающих в номере'),
(107, 1, 'Пожалуйста, проверьте данные проживающих в номере'),
(108, 1, 'Пожалуйста, проверьте данные о детях, проживающих в номере'),
(109, 1, 'Гость №'),
(110, 1, 'Ребенок №'),
(111, 1, 'Показать гостя №'),
(112, 1, 'Скрыть гостя №'),
(113, 1, 'Показать ребенка №'),
(114, 1, 'Скрыть ребенка №'),
(115, 1, 'Номер телефона'),
(116, 1, 'Код страны'),
(117, 1, 'Код оператора'),
(118, 1, 'Для комфортного отдыха выберите дополнительные услуги'),
(119, 1, 'с человека в день'),
(120, 1, 'дней'),
(121, 1, 'Добавить'),
(122, 1, 'Экскурсии'),
(123, 1, 'Понедельник'),
(124, 1, 'Вторник'),
(125, 1, 'Среда'),
(126, 1, 'Четверг'),
(127, 1, 'Пятница'),
(128, 1, 'Суббота'),
(129, 1, 'Воскресенье'),
(130, 1, 'Трансфер - прибытие'),
(131, 1, 'Трансфер - отправление'),
(132, 1, 'Стоимость услуг'),
(133, 1, 'Стоимость проживания с услугами'),
(134, 1, 'Предоплата за номер'),
(135, 1, 'Общая стоимость'),
(136, 1, 'Услуги'),
(137, 1, 'Проживание'),
(138, 1, 'Скидка'),
(139, 1, 'К оплате'),
(140, 1, 'Оплачено'),
(141, 1, 'Остаток к оплате'),
(142, 1, 'Оплатить общую стоимость'),
(143, 1, 'Оплатить услуги'),
(144, 1, 'Оплатить проживание'),
(145, 1, 'Оплатить проживание за весь период'),
(146, 1, 'Весь период'),
(147, 1, 'Оплатить стоимость первой ночи'),
(148, 1, 'Первая ночь'),
(149, 1, 'Печатать все'),
(150, 1, 'Печатать предложение'),
(151, 1, 'Отменить'),
(152, 1, 'Дата'),
(153, 1, 'Время'),
(154, 1, 'Рейс'),
(155, 1, 'Вагон'),
(156, 1, 'Поезд'),
(157, 1, 'Назад'),
(158, 1, 'Укажите свои дополнительные пожелания'),
(52, 1, 'Удалить'),
(159, 1, 'Удалить'),
(160, 1, 'Завершить бронирование'),
(161, 1, 'Сохранить'),
(162, 1, 'Вопрос'),
(163, 1, 'Отправить ваш вопрос'),
(164, 1, 'Ответ'),
(165, 1, 'Оставить отзыв'),
(167, 1, 'Ваше имя'),
(168, 1, 'Ваш email'),
(169, 1, 'Ваш отзыв'),
(170, 1, 'Текст с картинки'),
(171, 1, 'Отправить ваш отзыв'),
(172, 1, 'Неверно заполнены личные данные гостей'),
(173, 1, 'Ошибка при добавлении бронирования'),
(174, 1, 'Ошибка на сервере'),
(175, 1, 'Сообщение'),
(176, 1, 'Ошибка'),
(177, 1, 'Поздравляем! Ваше бронирование сохранено. Номер брони: '),
(178, 1, 'Заполните все поля, необходимые для сохранения трансфера!'),
(179, 1, 'Не задана дата заезда'),
(180, 1, 'Не задана дата выезда'),
(182, 1, 'Файл успешно удален'),
(183, 1, 'Изменения сохранены'),
(184, 1, 'Подтвердите отмену'),
(185, 1, 'Для подтверждения отмены бронирования введите свой пароль:'),
(186, 1, 'Услуги в Villa LaScala'),
(187, 1, 'Продолжить'),
(188, 1, 'Вход в систему'),
(189, 1, 'Регистрация'),
(190, 1, 'Ваш пароль'),
(191, 1, 'Войти'),
(192, 1, 'Забыли пароль?'),
(193, 1, 'Ваш пароль (7 знаков)'),
(195, 1, 'Зарегистрировать'),
(196, 1, 'Зарегистрированный E-mail'),
(197, 1, 'Отправить новый пароль'),
(198, 1, 'Сохранить e-mail и пароль'),
(199, 1, 'Пароли не совпадают'),
(200, 1, 'Ваш вопрос поступил администратору.'),
(201, 1, 'Ваш отзыв поступил администратору.'),
(202, 1, 'Ответ Вы получите на указанный E-mail'),
(203, 1, 'Вы успешно авторизованы'),
(204, 1, 'Мои бронирования'),
(205, 1, 'Личные данные'),
(206, 1, 'Скидки'),
(207, 1, 'Скан паспорта'),
(208, 1, 'Изменить E-mail'),
(209, 1, 'Изменить пароль'),
(210, 1, 'Данные турагента'),
(211, 1, 'Нет файла для загрузки'),
(212, 1, 'Страница доступна только зарегистированным пользователям'),
(213, 1, 'Неверный url'),
(214, 1, 'Отказано в доступе'),
(215, 1, 'Бронирование #{id} отменено'),
(216, 1, 'История бронирования'),
(217, 1, 'Дата создания'),
(218, 1, 'Дата изменения'),
(219, 1, 'Тип номера'),
(220, 1, 'Статус'),
(221, 1, 'Редактировать'),
(222, 1, 'Изменить дату заезда / выезда'),
(223, 1, 'Закрыть'),
(224, 1, 'Изменить'),
(225, 1, 'Загрузить файл'),
(226, 1, 'Удалить текущий'),
(227, 1, 'Платежные реквизиты'),
(228, 1, 'Логотип'),
(229, 1, 'Почтовый индекс'),
(230, 1, 'Адрес'),
(231, 1, 'Компания (место работы)'),
(232, 1, 'Текущий E-mail'),
(233, 1, 'Новый E-mail'),
(234, 1, 'Текущий пароль'),
(235, 1, 'Новый пароль'),
(238, 1, 'Введите текущий пароль'),
(239, 1, 'Введите новый пароль'),
(240, 1, 'Повторите новый пароль'),
(241, 1, 'Вы не можете изменить дату заезда при текущих условиях'),
(242, 1, 'Нажмите "Сохранить" для обновления даты заезда'),
(243, 1, 'Задайте другую дату заезда'),
(244, 1, 'Нет свободных номеров для выбранного периода'),
(245, 1, 'Поиск номеров успешно завершен'),
(246, 1, 'Часы'),
(247, 1, 'Минуты'),
(248, 1, 'Сохранение...'),
(249, 1, 'Пересчёт...'),
(250, 1, 'Не забудьте изменить даты и время трансферов'),
(251, 1, 'Сохранить и выйти'),
(248, 3, '请打给我');

-- --------------------------------------------------------

--
-- Table structure for table `tst_languages`
--

CREATE TABLE IF NOT EXISTS `tst_languages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `sys_name` varchar(128) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Языки' AUTO_INCREMENT=14 ;

--
-- Dumping data for table `tst_languages`
--

INSERT INTO `tst_languages` (`id`, `name`, `sys_name`, `image`) VALUES
(1, 'Русский', 'ru', '1468872753.jpg'),
(2, 'English', 'en', '1413355298.png'),
(3, 'Deutsch', 'de', '1413355304.png'),
(5, 'Italian', 'it', '1468872096.jpg'),
(6, 'French', 'fr', '1468872741.jpg'),
(7, 'English US', 'usa', '1468872114.jpg'),
(8, 'Polish', 'pl', '1468872122.jpg'),
(9, 'Spanish', 'es', '1468872130.jpg'),
(10, 'Dutch', 'nl', '1468872764.jpg'),
(11, 'Chinese', 'cn', '1468872079.jpg'),
(12, 'Turkish', 'tr', '1468872155.jpg'),
(13, 'Українська', 'ua', '1468872774.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tst_liqpay_log`
--

CREATE TABLE IF NOT EXISTS `tst_liqpay_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `public_key` varchar(100) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `sender_commission` decimal(10,2) NOT NULL DEFAULT '0.00',
  `receiver_commission` decimal(10,2) NOT NULL DEFAULT '0.00',
  `agent_commission` decimal(10,2) NOT NULL DEFAULT '0.00',
  `currency` varchar(10) NOT NULL,
  `description` varchar(100) NOT NULL,
  `order_id` int(11) NOT NULL,
  `liqpay_order_id` varchar(50) NOT NULL,
  `type` varchar(10) NOT NULL DEFAULT 'buy',
  `transaction_id` int(11) NOT NULL,
  `sender_phone` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `signature` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `transaction_id` (`transaction_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='История платежей LiqPay' AUTO_INCREMENT=39 ;

--
-- Dumping data for table `tst_liqpay_log`
--

INSERT INTO `tst_liqpay_log` (`id`, `public_key`, `amount`, `sender_commission`, `receiver_commission`, `agent_commission`, `currency`, `description`, `order_id`, `liqpay_order_id`, `type`, `transaction_id`, `sender_phone`, `status`, `time`, `signature`) VALUES
(1, 'i64710859726', 1.00, 0.00, 0.03, 0.00, 'UAH', 'payment', 1, '5169868u1425077020724440', 'buy', 51766888, '380938021614', 7, 1425077020, 'Wv/1HtCGya34jldHCtCkyDGJWlg='),
(2, 'i64710859726', 0.95, 0.00, 0.03, 0.00, 'UAH', 'Первая ночь (Предоплата за первую ночь) [S:1;U:64]', 271, '5169868u1425121285364514', 'buy', 51794822, '380938021614', 7, 1425121285, '2wLXScRMl6hJbxnZq6Vwnow55zQ='),
(3, 'i64710859726', 0.95, 0.00, 0.03, 0.00, 'UAH', 'Первая ночь (Предоплата за первую ночь) [S:1;U:64]', 275, '5169868u1425422760848402', 'buy', 52073617, '380938021614', 7, 1425422761, 'ECCj71B/yuUDepB2TmB2mMUqxdg='),
(4, 'i64710859726', 0.95, 0.00, 0.03, 0.00, 'UAH', 'Первая ночь (Предоплата за первую ночь) [S:1;U:64]', 276, '5169868u1425567875871551', 'buy', 52195289, '380938021614', 7, 1425567876, 'f+/sLUdQWmnPQxjUsprJugXPAr8='),
(5, 'i64710859726', 1.90, 0.00, 0.05, 0.00, 'UAH', 'Первая ночь (Предоплата за первую ночь) [S:1;U:31]', 279, '5169868u1426249109244139', 'buy', 52788987, '380938021614', 7, 1426249109, 'MOH8jurtedtET5mlC0KvmMTZvTU='),
(6, 'i64710859726', 0.09, 0.00, 0.07, 0.00, 'EUR', 'Первая ночь (Предоплата за первую ночь) [S:1;U:20]', 280, '1119318u1426259128618103', 'buy', 52802551, '380675641740', 7, 1426259128, 'BMPWJZ+nFje4SjuJZ+tWPpXz32Q='),
(7, 'i64710859726', 0.95, 0.00, 0.03, 0.00, 'UAH', 'Общая стоимость (Предоплата за первую ночь) [S:3;U:64]', 266, '5169868u1426259507035790', 'buy', 52803079, '380938021614', 7, 1426259507, 'Mrru398ZLsn7xgB/Jm1u5/7+ffU='),
(8, 'i64710859726', 1.90, 0.00, 0.05, 0.00, 'UAH', 'Первая ночь (Предоплата за первую ночь)  [S:1;U:67]', 281, '5169868u1426528541254974', 'buy', 53024573, '380938021614', 7, 1426528541, 'xPOpOKxnd59xFOyCKCTCMMQu+xI='),
(9, 'i64710859726', 20.90, 0.00, 0.57, 0.00, 'UAH', 'Весь период (Предоплата за весь период проживания) [S:1;U:49]', 287, '5430470u1427365089717089', 'buy', 53736381, '380669870610', 7, 1427365089, 'rwpzy1oAZes1zzlEarfSH/NR6PE='),
(10, 'i64710859726', 21.85, 0.00, 0.60, 0.00, 'UAH', 'Общая стоимость (Предоплата за первую ночь) [S:3;U:49]', 286, '5430470u1427365149644209', 'buy', 53736466, '380669870610', 7, 1427365149, 'Uuuc21AaUs8bETLWKQ5ysPWlUuc='),
(11, 'i64710859726', 21.85, 0.00, 0.60, 0.00, 'UAH', 'Общая стоимость (Предоплата за первую ночь) [S:3;U:49]', 288, '5430470u1427365662806650', 'buy', 53737075, '380669870610', 7, 1427365662, '9y0179J4QKjChiXd+nCk4pP647Q='),
(12, 'i64710859726', 1.90, 0.00, 0.05, 0.00, 'UAH', 'allResidence_en (All period prepayment) [S:1;U:73]', 296, '1119318u1428670146021423', 'buy', 54891985, '380675641740', 7, 1428670146, 'o+u4ppPNr8P6n0eesDXXIzGFA6M='),
(13, 'i64710859726', 0.10, 0.00, 0.07, 0.00, 'EUR', 'Весь период (Предоплата за весь период проживания) [S:1;U:73]', 298, '1119318u1428671673019536', 'buy', 54893889, '380675641740', 7, 1428671673, 'hruYmZkWb4HwyAVtltWiqo82mLY='),
(14, 'i64710859726', 1.71, 0.00, 0.05, 0.00, 'UAH', 'Весь период (Предоплата за весь период проживания) [S:1;U:20]', 299, '1119318u1428673138961289', 'buy', 54895614, '380675641740', 7, 1428673139, 'He9Ya8Pv29nBTAT93Alc/80WkBQ='),
(15, 'i64710859726', 1.71, 0.00, 0.05, 0.00, 'UAH', 'Весь период (Предоплата за весь период проживания) [S:1;U:20]', 306, '1119318u1428681385565344', 'buy', 54905233, '380675641740', 7, 1428681386, 'FXHde/2ctdjGhY7OiagXUYTsdUM='),
(16, 'i64710859726', 1.90, 0.00, 0.05, 0.00, 'UAH', 'Весь период (Предоплата за весь период проживания)  [S:1;U:79]', 315, '1119318u1429190353825036', 'buy', 55287316, '380675641740', 7, 1429190353, '8+yeB4pZUx6G3numo3zxmrA2s6I='),
(17, 'i64710859726', 132.83, 0.00, 83.97, 0.00, 'USD', 'totalCost_en (First night prepayment)  [S:3;U:59]', 318, '5430470u1430490376275697', 'buy', 56418423, '380669870610', 7, 1430490376, 'WmXFMILNltKAr9ufSRCp0Z25gQY='),
(18, 'i64710859726', 0.02, 0.00, 0.00, 0.00, 'UAH', 'Весь период (Предоплата за весь период проживания) [S:1;U:49]', 328, '5430470u1431024816270035', 'buy', 56922676, '380669870610', 7, 1431024816, 'gR1KbUjX8sY/0Mr5fI1DMS1i9BE='),
(19, 'i64710859726', 1.90, 0.00, 0.05, 0.00, 'UAH', 'allResidence_en (All period prepayment)  [S:1;U:20]', 330, '1119318u1431804960878354', 'buy', 57714434, '380675641740', 7, 1431804961, 'lF9qJOZ1XyC5GK0S2phmXeTyQJU='),
(20, 'i64710859726', 1.90, 0.00, 0.05, 0.00, 'UAH', 'Весь период (Предоплата за весь период проживания)  [S:1;U:83]', 333, '1119318u1431811168588242', 'buy', 57718525, '380675641740', 7, 1431811168, 'akPzXw1BhZCJQ+8aeQc0a0SgXUU='),
(21, 'i64710859726', 1.71, 0.00, 0.05, 0.00, 'UAH', 'Весь период (Предоплата за весь период проживания) [S:1;U:20]', 334, '1119318u1431813609677731', 'buy', 57719416, '380675641740', 7, 1431813609, '6cdES2tfA2Xi0lcu7x/2yMUabAk='),
(22, 'i64710859726', 1.90, 0.00, 0.05, 0.00, 'UAH', 'allResidence_en (All period prepayment)  [S:1;U:84]', 336, '5169868u1433855705327284', 'buy', 59770481, '380938021614', 7, 1433855705, '9cVeh+ebB4PLQjdAO3yWsJ8K2uw='),
(23, 'i64710859726', 38.00, 0.00, 1.05, 0.00, 'UAH', 'services_en (All period prepayment) [S:2;U:84]', 336, '5169868u1433857192632060', 'buy', 59772929, '380938021614', 7, 1433857192, 'qrthU2753lB09Iw8u7iy8pl9FnI='),
(24, 'i64710859726', 1.90, 0.00, 0.05, 0.00, 'UAH', 'Весь период (Предоплата за весь период проживания) [S:1;U:85]', 338, '5169868u1433940257917669', 'buy', 59885328, '380938021614', 7, 1433940258, 'EZsiU5DRodOm6TRF+lD0JkAS15k='),
(25, 'i64710859726', 0.44, 0.00, 0.01, 0.00, 'UAH', 'Услуги (Предоплата за весь период проживания) [S:2;U:49]', 339, '5430470u1434177112282410', 'buy', 60132505, '380669870610', 7, 1434177112, 'QhB31WQMHi2NwXUxNp7EqdBTiVM='),
(26, 'i64710859726', 0.02, 0.00, 0.00, 0.00, 'UAH', 'Общая стоимость (Предоплата за весь период проживания) [S:3;U:49]', 339, '5430470u1434179178439342', 'buy', 60135555, '380669870610', 7, 1434179178, 'wEweEXGbF3/yVQNfYKJqc52h9W8='),
(27, 'i64710859726', 2.20, 0.00, 0.06, 0.00, 'UAH', 'Весь период (Предоплата за весь период проживания)  [S:1;U:86]', 342, '5430470u1434182893137704', 'buy', 60141252, '380669870610', 7, 1434182893, '2c/kIj40y6coLOJB7DSvsBiL9Xk='),
(28, 'i64710859726', 6.60, 0.00, 0.18, 0.00, 'UAH', 'Весь период (Предоплата за весь период проживания)  [S:1;U:87]', 343, '5169868u1434474906415357', 'buy', 60413310, '380938021614', 7, 1434474906, 't7+uBmKm+Ww4ZudNAgL6/NGeVdg='),
(29, 'i64710859726', 2.20, 0.00, 0.06, 0.00, 'UAH', 'Проживание (Предоплата за весь период проживания)  [S:1;U:87]', 344, '5169868u1434475437384244', 'buy', 60414048, '380938021614', 7, 1434475437, 'D6lvTh1j7NEsmoGSHSOZ6BWXkts='),
(30, 'i64710859726', 2.20, 0.00, 0.06, 0.00, 'UAH', 'Весь период (Предоплата за весь период проживания)  [S:1;U:87]', 345, '5169868u1434476173257250', 'buy', 60414948, '380938021614', 7, 1434476173, 'Nc9MQLKI02OJBbI4CP47gKVIy+0='),
(31, 'i64710859726', 2.20, 0.00, 0.06, 0.00, 'UAH', 'Весь период (Предоплата за весь период проживания) [S:1;U:87]', 346, '5169868u1434477199112298', 'buy', 60416429, '380938021614', 7, 1434477199, 'dxYvhKdlQ9STaCAYrMcNrySSY98='),
(32, 'i64710859726', 2.20, 0.00, 0.06, 0.00, 'UAH', 'Проживание (Предоплата за весь период проживания) [S:1;U:87]', 347, '5169868u1434477735421227', 'buy', 60417186, '380938021614', 7, 1434477735, 'QB+Gawv5Kc79XvwORtZ//em74c8='),
(33, 'i64710859726', 2.20, 0.00, 0.06, 0.00, 'UAH', 'Весь период (Предоплата за весь период проживания) [S:1;U:87]', 348, '5169868u1434478657471050', 'buy', 60418445, '380938021614', 7, 1434478657, 'DlgQD+ofItivYpehVllD5dHqLpc='),
(34, 'i64710859726', 0.02, 0.00, 0.00, 0.00, 'UAH', 'Весь период (Предоплата за весь период проживания) [S:1;U:49]', 350, '5430470u1434568867396154', 'buy', 60509226, '380669870610', 7, 1434568867, 'Xzfjjhk1W2R63rn8E7TmemmDIm8='),
(35, 'i64710859726', 0.10, 0.00, 0.00, 0.00, 'USD', 'Весь период (Предоплата за весь период проживания) [S:1;U:20]', 366, '1119318u1436369372889520', 'buy', 62334166, '380675641740', 7, 1436369373, 'Hf4Roc4JjAQf/DTIk2df8cyjY74='),
(36, 'i64710859726', 8.80, 0.00, 0.24, 0.00, 'UAH', 'Весь период (Предоплата за весь период проживания) [S:1;U:96]', 373, '1119318u1437233631682284', 'buy', 63355588, '380675641740', 7, 1437233631, '/8xCoIslpPTDAJeP7vE1ZOPF7Jw='),
(37, 'i64710859726', 6.60, 0.00, 0.18, 0.00, 'UAH', 'Весь период (Предоплата за весь период проживания) [S:1;U:96]', 374, '1119318u1437295759233668', 'buy', 63403442, '380675641740', 7, 1437295759, '3wMKWu//w3kAaeNYaDsUk3sLYaU='),
(38, 'i64710859726', 13.86, 0.00, 0.38, 0.00, 'UAH', 'Весь период (Предоплата за весь период проживания) [S:1;U:20]', 377, '1119318u1440624759722356', 'buy', 67611895, '380675641740', 7, 1440624759, '+A2oFo3NAuH8NiBLZbTrM7gESzc=');

-- --------------------------------------------------------

--
-- Table structure for table `tst_liqpay_settings`
--

CREATE TABLE IF NOT EXISTS `tst_liqpay_settings` (
  `public_key` varchar(100) NOT NULL,
  `private_key` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tst_liqpay_settings`
--

INSERT INTO `tst_liqpay_settings` (`public_key`, `private_key`) VALUES
('i64710859726', 'qPZUIDH2aPBAeNSWfAufE9iAnkqhcQdCiQjwB7Q5');

-- --------------------------------------------------------

--
-- Table structure for table `tst_paypal_log`
--

CREATE TABLE IF NOT EXISTS `tst_paypal_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_type` varchar(50) DEFAULT NULL,
  `payment_date` varchar(50) DEFAULT NULL,
  `payment_status` varchar(50) DEFAULT NULL,
  `address_status` varchar(50) DEFAULT NULL,
  `payer_status` varchar(50) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `payer_email` varchar(50) DEFAULT NULL,
  `payer_id` varchar(50) DEFAULT NULL,
  `address_name` varchar(50) DEFAULT NULL,
  `address_country` varchar(50) DEFAULT NULL,
  `address_country_code` varchar(50) DEFAULT NULL,
  `address_zip` varchar(50) DEFAULT NULL,
  `address_state` varchar(50) DEFAULT NULL,
  `address_city` varchar(50) DEFAULT NULL,
  `address_street` varchar(100) DEFAULT NULL,
  `business` varchar(50) DEFAULT NULL,
  `receiver_email` varchar(50) DEFAULT NULL,
  `receiver_id` varchar(50) DEFAULT NULL,
  `residence_country` varchar(50) DEFAULT NULL,
  `item_name` varchar(50) DEFAULT NULL,
  `item_number` varchar(50) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `shipping` float(10,2) NOT NULL,
  `tax` float(10,2) NOT NULL,
  `mc_currency` varchar(50) DEFAULT NULL,
  `mc_fee` float(10,2) NOT NULL,
  `mc_gross` float(10,2) NOT NULL,
  `txn_type` varchar(50) DEFAULT NULL,
  `txn_id` int(11) DEFAULT NULL,
  `notify_version` varchar(50) DEFAULT NULL,
  `custom` varchar(50) DEFAULT NULL,
  `invoice` varchar(50) DEFAULT NULL,
  `charset` varchar(50) DEFAULT NULL,
  `verify_sign` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='paypal логи' AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tst_paypal_log`
--

INSERT INTO `tst_paypal_log` (`id`, `payment_type`, `payment_date`, `payment_status`, `address_status`, `payer_status`, `first_name`, `last_name`, `payer_email`, `payer_id`, `address_name`, `address_country`, `address_country_code`, `address_zip`, `address_state`, `address_city`, `address_street`, `business`, `receiver_email`, `receiver_id`, `residence_country`, `item_name`, `item_number`, `quantity`, `shipping`, `tax`, `mc_currency`, `mc_fee`, `mc_gross`, `txn_type`, `txn_id`, `notify_version`, `custom`, `invoice`, `charset`, `verify_sign`) VALUES
(1, 'echeck', '16:45:48 May 28, 2012 PDT', 'Completed', 'confirmed', 'verified', 'John', 'Smith', 'buyer@paypalsandbox.com', 'TESTBUYERID01', 'John Smith', 'United States', 'US', '95131', 'CA', 'San Jose', '123, any street', 'seller@paypalsandbox.com', 'seller@paypalsandbox.com', 'TESTSELLERID1', 'US', 'something', '246', 1, 0.00, 0.00, 'USD', 0.00, 73.21, 'web_accept', 485282345, '2.1', 's:3;u:49', 'abc1234', 'utf-8', 'AVGUa04bUtMm3j7yed7qpzQsUMbEAjf-R-uwAtkl4Rw-kxjQ07pNYaXE'),
(2, 'echeck', '16:45:48 May 28, 2012 PDT', 'Completed', 'confirmed', 'verified', 'John', 'Smith', 'buyer@paypalsandbox.com', 'TESTBUYERID01', 'John Smith', 'United States', 'US', '95131', 'CA', 'San Jose', '123, any street', 'seller@paypalsandbox.com', 'seller@paypalsandbox.com', 'TESTSELLERID1', 'US', 'something', '245', 1, 0.00, 0.00, 'USD', 0.00, 0.08, 'web_accept', 485282345, '2.1', 's:2;u:45', 'abc1234', 'utf-8', 'AVGUa04bUtMm3j7yed7qpzQsUMbEAjf-R-uwAtkl4Rw-kxjQ07pNYaXE'),
(3, 'echeck', '16:45:48 May 28, 2012 PDT', 'Completed', 'confirmed', 'verified', 'John', 'Smith', 'buyer@paypalsandbox.com', 'TESTBUYERID01', 'John Smith', 'United States', 'US', '95131', 'CA', 'San Jose', '123, any street', 'seller@paypalsandbox.com', 'seller@paypalsandbox.com', 'TESTSELLERID1', 'US', 'something', '245', 1, 0.00, 0.00, 'USD', 0.00, 72.19, 'web_accept', 485282345, '2.1', 's:3;u:49', 'abc1234', 'utf-8', 'AVGUa04bUtMm3j7yed7qpzQsUMbEAjf-R-uwAtkl4Rw-kxjQ07pNYaXE'),
(4, 'echeck', '16:45:48 May 28, 2012 PDT', 'Completed', 'confirmed', 'verified', 'John', 'Smith', 'buyer@paypalsandbox.com', 'TESTBUYERID01', 'John Smith', 'United States', 'US', '95131', 'CA', 'San Jose', '123, any street', 'seller@paypalsandbox.com', 'seller@paypalsandbox.com', 'TESTSELLERID1', 'US', 'something', '228', 1, 0.00, 0.00, 'EUR', 0.00, 150.00, 'web_accept', 485282345, '2.1', 's:3;u:49', 'abc1234', 'utf-8', 'AVGUa04bUtMm3j7yed7qpzQsUMbEAjf-R-uwAtkl4Rw-kxjQ07pNYaXE'),
(5, 'echeck', '16:45:48 May 28, 2012 PDT', 'Completed', 'confirmed', 'verified', 'John', 'Smith', 'buyer@paypalsandbox.com', 'TESTBUYERID01', 'John Smith', 'United States', 'US', '95131', 'CA', 'San Jose', '123, any street', 'seller@paypalsandbox.com', 'seller@paypalsandbox.com', 'TESTSELLERID1', 'US', 'something', '224', 1, 0.00, 0.00, 'EUR', 0.00, 40.00, 'web_accept', 485282345, '2.1', 's:2;u:49', 'abc1234', 'utf-8', 'AVGUa04bUtMm3j7yed7qpzQsUMbEAjf-R-uwAtkl4Rw-kxjQ07pNYaXE'),
(6, 'echeck', '16:45:48 May 28, 2012 PDT', 'Completed', 'confirmed', 'verified', 'John', 'Smith', 'buyer@paypalsandbox.com', 'TESTBUYERID01', 'John Smith', 'United States', 'US', '95131', 'CA', 'San Jose', '123, any street', 'seller@paypalsandbox.com', 'seller@paypalsandbox.com', 'TESTSELLERID1', 'US', 'something', '224', 1, 0.00, 0.00, 'EUR', 0.00, 100.00, 'web_accept', 485282345, '2.1', 's:1;u:49', 'abc1234', 'utf-8', 'AVGUa04bUtMm3j7yed7qpzQsUMbEAjf-R-uwAtkl4Rw-kxjQ07pNYaXE'),
(7, 'echeck', '16:45:48 May 28, 2012 PDT', 'Completed', 'confirmed', 'verified', 'John', 'Smith', 'buyer@paypalsandbox.com', 'TESTBUYERID01', 'John Smith', 'United States', 'US', '95131', 'CA', 'San Jose', '123, any street', 'seller@paypalsandbox.com', 'seller@paypalsandbox.com', 'TESTSELLERID1', 'US', 'something', '244', 1, 0.00, 0.00, 'USD', 0.00, 10.00, 'web_accept', 485282345, '2.1', 's:3;u:60', 'abc1234', 'utf-8', 'AVGUa04bUtMm3j7yed7qpzQsUMbEAjf-R-uwAtkl4Rw-kxjQ07pNYaXE');

-- --------------------------------------------------------

--
-- Table structure for table `tst_publications`
--

CREATE TABLE IF NOT EXISTS `tst_publications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(124) NOT NULL,
  `url` varchar(100) NOT NULL,
  `date` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `division` int(11) unsigned DEFAULT NULL,
  `auth_only` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_publications_uid` (`uid`),
  KEY `fk_publications_division` (`division`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='публикации' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tst_publications`
--

INSERT INTO `tst_publications` (`id`, `name`, `url`, `date`, `time`, `image`, `uid`, `division`, `auth_only`) VALUES
(1, 'Контакты', 'contacts.html', -10800, 55800, NULL, 18, NULL, 0),
(2, 'Об отеле', 'abouthotel.html', -10800, 45240, NULL, 18, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tst_publications_division`
--

CREATE TABLE IF NOT EXISTS `tst_publications_division` (
  `publications_id` int(11) NOT NULL,
  `division_id` int(11) unsigned NOT NULL,
  KEY `publications_id` (`publications_id`),
  KEY `division_id` (`division_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tst_publications_lang`
--

CREATE TABLE IF NOT EXISTS `tst_publications_lang` (
  `page_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `page_title` varchar(124) NOT NULL,
  `page_content` text NOT NULL,
  `page_preview` text NOT NULL,
  `title` text NOT NULL,
  `keywords` text NOT NULL,
  `description` text NOT NULL,
  KEY `fk_publications_lang_page_id` (`page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='публикации';

--
-- Dumping data for table `tst_publications_lang`
--

INSERT INTO `tst_publications_lang` (`page_id`, `lang_id`, `page_title`, `page_content`, `page_preview`, `title`, `keywords`, `description`) VALUES
(1, 2, 'Contacts', 'our contacts', '', '', '', ''),
(2, 1, 'Об отеле', '<p>+Добрый день, Вас приветствует отель Villa LaScala .</p>\r\n<p>Гости нашего отеля могут рассчитывать на качественное предоставление услуг, заботливое обслуживание и профессионализм англоязычного персонала. <br>Отель гарантирует безопасность и конфиденциальность своим гостям. Здесь присутствуют атмосфера гармонии и покоя. Номерной фонд отеля - 54 номерa, разных категорий: Люкс - 2 шт, Полулюкс - 4 шт , Стандарт Дабл - 38 шт. Отель оформлен в теплых тонах.</p>\r\n<p>Каждый номер оборудован современной мебелью и большой кроватью размера King-size. В номере имеется, LCD-телевизор с 42 дюймовым экраном, бесплатный высокоскоростной Wi-Fi, холодильник/мини-бар, электронный сейф с возможностью использования его под ноутбук, телефонная связь, электрочайник с посудой и все не маловажные мелочи, для комфортного длительного или краткосрочного проживания. <br>Все номера оборудованы ванной комнатой (туалет, душ, горячая и холодная вода, фен) с подогревом полов. Гостям предоставляются персональные наборы из трех полотенец, халата и тапочек.</p>\r\n<p>Все номера оборудованы электронными замками с магнитной карточкой для входной двери, что обеспечивает безопасность и удобства в использовании</p>\r\n<p>Благодаря централизированой климатической системе, с регулированием температурного режима, в помещениях зимой всегда тепло, а летом прохладно, что полностью избавляет от необходимости использования кондиционеров.</p>\r\n<p>В отеле работает круглосуточная стойка администратора, что позволяет гостю, заселится в любое удобное для него время суток.<br>Когда угодно(день ,ночь), не выходя из своего номера Вы можете воспользоваться Room servise (предоставление закусок в номера), и заказать понравившееся Вам блюда из меню и насладиться их вкусом .</p>\r\n<p>На территории гостиницы проходит постоянно действующая выставка- ярмарка работ знаменитых украинских фотографов. Постояльцы гостиницы имеют исключительную возможность приобретения любой понравившейся им работы.</p>\r\n<p>На территории гостиницы находится ресторан в украинском стиле.<br>Посетив который, вы сможете окунуться в глубину национальных обычаев и быта, насладиться национальной украинской кухней и полюбоваться громадной картиной известных украинских художников выполненной маслом (общая площадь картины составляет 45 кв. метров).</p>\r\n<p>Так же на территории гостиницы расположен фирменный французский магазин постельного белья LaScala. Где вы сможете приобрести постельное белье, покрывала, подушки, одеяла, простыни и полотенца из натуральных материалов высокого качества: хлопка, шёлка, шерсти, пуха, по доступной цене. Такое приобретении станет шикарным сувениром для Вас и всей Ваших близких. Ознакомится с ассортиментом магазина, вы можете заранее http://www.lascala.ua/.</p>\r\n<p>При необходимости, персонал отеля в любое время суток встретит Вас в аэропорту Борисполь с табличкой на выходе из зоны прилета, поможет с багажом и поселением в гостиницу.</p>\r\n<p>Мы всегда очень Вам рады! С Уважением дружный коллектив гостиницы Villa LaScala!</p>  ', '', '', '', 'Добрый день, Вас приветствует отель Villa LaScala .\r\n\r\nГости нашего отеля могут рассчитывать на качественное предоставление услуг, заботливое обслуживание и профессионализм англоязычного персонала. \r\nОтель гарантирует безопасность и конфиденциальность своим гостям. Здесь присутствуют атмосфера гармонии и покоя. Номерной фонд отеля - 54 номерa, разных категорий: Люкс - 2 шт, Полулюкс - 4 шт , Стандарт Дабл - 38 шт. Отель оформлен в теплых тонах.\r\n\r\nКаждый номер оборудован современной мебелью и большой кроватью размера King-size. В номере имеется, LCD-телевизор с 42 дюймовым экраном, бесплатный высокоскоростной Wi-Fi, холодильник/мини-бар, электронный сейф с возможностью использования его под ноутбук, телефонная связь, электрочайник с посудой и все не маловажные мелочи, для комфортного длительного или краткосрочного проживания. \r\nВсе номера оборудованы ванной комнатой (туалет, душ, горячая и холодная вода, фен) с подогревом полов. Гостям предоставляются персональные наборы из трех полотенец, халата и тапочек.\r\n\r\nВсе номера оборудованы электронными замками с магнитной карточкой для входной двери, что обеспечивает безопасность и удобства в использовании\r\n\r\nБлагодаря централизированой климатической системе, с регулированием температурного режима, в помещениях зимой всегда тепло, а летом прохладно, что полностью избавляет от необходимости использования кондиционеров.\r\n\r\nВ отеле работает круглосуточная стойка администратора, что позволяет гостю, заселится в любое удобное для него время суток.\r\nКогда угодно(день ,ночь), не выходя из своего номера Вы можете воспользоваться Room servise (предоставление закусок в номера), и заказать понравившееся Вам блюда из меню и насладиться их вкусом .\r\n\r\nНа территории гостиницы проходит постоянно действующая выставка- ярмарка работ знаменитых украинских фотографов. Постояльцы гостиницы имеют исключительную возможность приобретения любой понравившейся им работы.\r\n\r\nНа территории гостиницы находится ресторан в украинском стиле.\r\nПосетив который, вы сможете окунуться в глубину национальных обычаев и быта, насладиться национальной украинской кухней и полюбоваться громадной картиной известных украинских художников выполненной маслом (общая площадь картины составляет 45 кв. метров).\r\n\r\nТак же на территории гостиницы расположен фирменный французский магазин постельного белья LaScala. Где вы сможете приобрести постельное белье, покрывала, подушки, одеяла, простыни и полотенца из натуральных материалов высокого качества: хлопка, шёлка, шерсти, пуха, по доступной цене. Такое приобретении станет шикарным сувениром для Вас и всей Ваших близких. Ознакомится с ассортиментом магазина, вы можете заранее http://www.lascala.ua/.\r\n\r\nПри необходимости, персонал отеля в любое время суток встретит Вас в аэропорту Борисполь с табличкой на выходе из зоны прилета, поможет с багажом и поселением в гостиницу.\r\n\r\nМы всегда очень Вам рады! С Уважением дружный коллектив гостиницы Villa LaScala!'),
(1, 1, 'Контакты', 'Украина, \r\n                        город Киев, \r\n                        просп. Отрадный, дом 7.\r\n                    +<br><br>\r\n<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1270.614152541083!2d30.436243!3d50.436848!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xde05b4f6eed8fedd!2sHotel+Villa+LaScala!5e0!3m2!1sru!2sua!4v1413881278715" width="940" height="600" frameborder="0" style="border:0"></iframe>  ', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tst_rating`
--

CREATE TABLE IF NOT EXISTS `tst_rating` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rating` int(11) DEFAULT '0',
  `vote_count` int(11) DEFAULT '0',
  `entity_type_id` int(11) DEFAULT NULL,
  `entity_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Система рейтингов' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tst_rating_log`
--

CREATE TABLE IF NOT EXISTS `tst_rating_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `obj_id` int(11) unsigned DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `sid` varchar(32) DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_rating_log_obj_id` (`obj_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Лог рейтингов' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tst_sessions`
--

CREATE TABLE IF NOT EXISTS `tst_sessions` (
  `uid` int(11) NOT NULL,
  `session` varchar(32) NOT NULL,
  `last_update` int(11) NOT NULL,
  `ip` varchar(20) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tst_sessions`
--

INSERT INTO `tst_sessions` (`uid`, `session`, `last_update`, `ip`) VALUES
(18, 'e286aafa2bbcc199ee3ad02aa6b65828', 1470328578, '78.159.62.229'),
(19, '3960dd7385ce3e1885f26a71d155325c', 1417266484, '109.86.116.239'),
(20, '8eec073b99f8c53d720e27b87eeb052c', 1470327734, '78.159.62.229'),
(29, '5c561df75d251245d4272ab271582e9a', 1420011314, '94.244.55.136'),
(59, 'cf09382c2965d5af3639ec393e2f207f', 1422286131, '46.219.240.202'),
(61, '17a1efb487cbc5adddafff678cd71cf0', 1441710895, '194.0.88.214'),
(68, '8137d3b1910e8484c3471a828553f1df', 1426781409, '94.244.55.136'),
(71, 'f160a30e670f8e8a03a57c3e6064436d', 1428131398, '46.219.240.220'),
(76, 'eb63b11a092ca59e424e71688f84904b', 1429117948, '94.244.55.136'),
(83, '8fbbbf92f152a978049297c890c9a6cb', 1439394110, '78.159.62.229'),
(96, 'd32097b2f9ee039639b0327e35845391', 1437295114, '82.193.104.155'),
(97, 'a7cfc401edb5cd27463f6f49b80470d3', 1437499937, '94.244.55.136'),
(111, '32f650e3454d4ecb0d810bdaf40908ab', 1445333193, '194.0.88.214'),
(112, '06cd30658e0f0d9415e1b8125d944c87', 1445368828, '94.244.55.136'),
(116, '8e1c9e073bc4bdbe5daa055665847727', 1450289390, '46.219.240.78'),
(120, '5c49003d47e77a956fc9ac568b576d9e', 1455203706, '193.0.206.54'),
(122, '1a796ceebb86b40cdababb75256ae188', 1455213252, '193.0.206.54'),
(124, '7828a23df978033fa20229549af2c4fc', 1455651142, '46.219.249.57'),
(125, '055db4b6d631872c91205be14811f569', 1461407619, '82.193.104.155');

-- --------------------------------------------------------

--
-- Table structure for table `tst_slider`
--

CREATE TABLE IF NOT EXISTS `tst_slider` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `img_name` varchar(128) NOT NULL,
  `title` text,
  `url` varchar(128) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='слайдер' AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tst_slider`
--

INSERT INTO `tst_slider` (`id`, `img_name`, `title`, `url`, `is_active`) VALUES
(1, 'Shine_enl.jpg', 'картинка  ', NULL, 1),
(2, '11db73aee29d7eaf04b6c4cfb.jpg', NULL, NULL, 1),
(4, 'fddd9f68aab9aa67c58293423.jpg', NULL, NULL, 1),
(5, '5cedaaa4f2e4c89ab98a38c34.png', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tst_users`
--

CREATE TABLE IF NOT EXISTS `tst_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `rating` int(3) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `login` varchar(32) DEFAULT NULL,
  `restory_code` varchar(100) DEFAULT NULL COMMENT 'Поле кода доступа к генерации нового пароля',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=131 ;

--
-- Dumping data for table `tst_users`
--

INSERT INTO `tst_users` (`id`, `email`, `password`, `rating`, `is_active`, `login`, `restory_code`) VALUES
(13, 'paypall', 'paypall', 1, 0, NULL, NULL),
(18, 'admin', '202cb962ac59075b964b07152d234b70', 100, 1, NULL, NULL),
(19, 'doriangray@ukr.net', '202cb962ac59075b964b07152d234b70', 1, 1, NULL, '2IKd8Di4Kl'),
(20, '0675641740ua@gmail.com', '202cb962ac59075b964b07152d234b70', 1, 1, NULL, NULL),
(22, 'serg@qqq.com', '202cb962ac59075b964b07152d234b70', 1, 1, NULL, NULL),
(23, 'serg2', '202cb962ac59075b964b07152d234b70', 1, 1, NULL, NULL),
(24, 'serg3@dd.com', '202cb962ac59075b964b07152d234b70', 1, 1, NULL, NULL),
(29, 'email@jd.net', '202cb962ac59075b964b07152d234b70', 1, 1, NULL, NULL),
(31, 'test@test.net', '202cb962ac59075b964b07152d234b70', 1, 1, NULL, NULL),
(34, 'ivan@testmail.com', '202cb962ac59075b964b07152d234b70', 1, 0, NULL, NULL),
(35, 'lysakalexandr42@gmail.com', '113e56fcfc7e50c058d953d272ba1b3a', 1, 1, NULL, NULL),
(36, 'piotr@mail.ru', '202cb962ac59075b964b07152d234b70', 1, 0, NULL, NULL),
(37, 'ivan@mail.ru', '202cb962ac59075b964b07152d234b70', 1, 0, NULL, NULL),
(38, 'ivanov@mail.com', '202cb962ac59075b964b07152d234b70', 1, 0, NULL, NULL),
(41, 'ivanovalexandr@gmail.com', '202cb962ac59075b964b07152d234b70', 1, 1, NULL, NULL),
(42, 'dasalexx@xakep.ru', '9e5024f0e0e1895f8b02ef14fdf1963a', 1, 0, NULL, NULL),
(43, 'msdd@mail.com', '202cb962ac59075b964b07152d234b70', 1, 1, NULL, NULL),
(44, 'asas@mail.com', '202cb962ac59075b964b07152d234b70', 1, 1, NULL, NULL),
(45, 'erverf@hsd.com', '202cb962ac59075b964b07152d234b70', 1, 1, NULL, NULL),
(46, 'fgrgrt@erf.com', '202cb962ac59075b964b07152d234b70', 1, 1, NULL, NULL),
(47, 'evro78@mail.ua', 'af91eb314a569b07cf2396a16eeb8adf', 1, 0, NULL, NULL),
(49, 'vanquisher.ua@gmail.com', '202cb962ac59075b964b07152d234b70', 1, 1, NULL, NULL),
(54, 'xeh@sh.se', '6ca215d012d27fe093bc84e57cbc2d04', 1, 1, NULL, NULL),
(55, 'seh@sa.sq', '66042440db18143889533a9546f15abe', 1, 1, NULL, NULL),
(58, 'viuf@iug.wif', '7a33c95278c2cfdef96164732b5df950', 1, 1, NULL, NULL),
(59, '1', '8d71f0730970f44e84dcbbe56b5ae6e3', 1, 1, NULL, NULL),
(60, 'suif@isg.ak', 'adfd42c913e90a5ed078061519f2e6dc', 1, 1, NULL, NULL),
(61, 'nika.valkyria@gmail.com', '202cb962ac59075b964b07152d234b70', 1, 1, NULL, NULL),
(62, 'dd@dd.com', 'cf95acf8ccd36751bf94ce9a649cb595', 1, 1, NULL, NULL),
(66, 'цуцукцу@uydgf', '2bc5987742ea931fbb566e8fa916d6ae', 1, 1, NULL, NULL),
(67, 'ss@dd.dd', '26adc7f64abbbcd21fa9b51f57ca6aae', 1, 1, NULL, NULL),
(68, 'eeesh@se.aeg', 'bb86c4eb8ace32631536449f9c7be463', 1, 1, NULL, NULL),
(70, 'loginn333@gmail.com', 'b3e9523cd78f71bb7a34a354f64bc918', 1, 0, NULL, NULL),
(71, '11', 'f50cc23f015f89d05004451ad09bd717', 1, 1, NULL, NULL),
(72, 'aaa@gmail.com', '202cb962ac59075b964b07152d234b70', 1, 1, NULL, NULL),
(73, 'ivan@com.ua', '202cb962ac59075b964b07152d234b70', 1, 1, NULL, NULL),
(74, 'somebody@mail.com', '787caf0eaecc18880b883371590c64df', 1, 1, NULL, NULL),
(76, 'hhh@hhh.hhh', 'e10adc3949ba59abbe56e057f20f883e', 1, 1, NULL, NULL),
(77, 'awg@ag.aw', 'cc4bb24db5213a91f0bc6f311bb59eb1', 1, 1, NULL, NULL),
(78, 'awg@aaa.aa', '946892bcd35d7d7cb8e864dd1497f62c', 1, 1, NULL, NULL),
(83, 'huntercamer@gmail.com', '202cb962ac59075b964b07152d234b70', 1, 1, NULL, NULL),
(85, 'bbb@bbb.bbb', '8efed4066c6fbd7e544acba512e008b3', 1, 1, NULL, NULL),
(86, 'aka.uchiha.itachi@gmail.com', '202cb962ac59075b964b07152d234b70', 100, 1, NULL, NULL),
(88, 'ddd@dd.dd', '1c3964b0fd34e5e4bb0769de0a30f3e5', 1, 1, NULL, NULL),
(89, 'dasd@dd.dd', 'a965cda95f1788567a780b3fd6906ad9', 1, 1, NULL, NULL),
(90, 'reg@uigu', '6127a8c52f10d68b21555a5eeafbfdc5', 1, 1, NULL, NULL),
(91, '5482828@i.ua', 'a13f7eda16feb399ff1f9decc0ced7ea', 1, 1, NULL, NULL),
(92, 'asdad@dd.dd', 'deedcffbf5229690abd6df9c52734990', 1, 1, NULL, NULL),
(94, 'firewolf@i.ua', 'b5fea2f9bb3dbe52db6840ea60c1e798', 1, 1, NULL, NULL),
(95, 'firewolf@mail.i.ua', '126511e348b9f10e1820856d8af3cfa9', 1, 1, NULL, NULL),
(96, 'shantukovich@gmail.com', '202cb962ac59075b964b07152d234b70', 1, 1, NULL, NULL),
(97, 'ff@hh.jj', '64f4b1810d1fc38542af7a933309b8b5', 1, 1, NULL, NULL),
(99, 'hunter-dima@ukr.net', '202cb962ac59075b964b07152d234b70', 1, 1, NULL, NULL),
(100, 'seh@srh.seh', 'cfdf3698265cfd2f5d7ceb8291774611', 1, 1, NULL, NULL),
(101, 'gendir', '202cb962ac59075b964b07152d234b70', 100, 1, NULL, NULL),
(102, 'director', '202cb962ac59075b964b07152d234b70', 100, 1, NULL, NULL),
(103, 'zamdir', '202cb962ac59075b964b07152d234b70', 100, 1, NULL, NULL),
(106, 'test2', '202cb962ac59075b964b07152d234b70', 100, 1, NULL, NULL),
(108, 'test3', '202cb962ac59075b964b07152d234b70', 100, 1, NULL, NULL),
(109, 'abcd', '202cb962ac59075b964b07152d234b70', 100, 1, NULL, NULL),
(110, 'test', '202cb962ac59075b964b07152d234b70', 100, 1, NULL, NULL),
(111, 'aaaa@aaa.aaa', '4c31338157398f1ae6b97276fb8c0ad4', 1, 1, NULL, NULL),
(112, 'aaa@aaa.saa', '85e857e1113e2cbe60cc67547d9ce8fe', 1, 1, NULL, NULL),
(113, 'weytrwe@hhh.com', '3361fa02070f0b1e1a4c5208b2000c4a', 1, 1, NULL, NULL),
(114, 'asdasd', '4ca170a3472753ec9e91556747a09e4f', 1, 1, NULL, NULL),
(115, 'gg@dd.dd', 'cadbc9994b48059fc5eca66252c783cc', 1, 1, NULL, NULL),
(116, '123131.com', '738a0f95090cd3a2c43b001fa84211e0', 1, 1, NULL, NULL),
(117, 'ddd@dd.com', 'f64ca40711d4c8dc95d8cc2e607c1d11', 1, 1, NULL, NULL),
(118, 'burger', '202cb962ac59075b964b07152d234b70', 100, 1, NULL, NULL),
(119, '1@1.com', '8248b5c1f542b7e33664c2aa376e6195', 1, 1, NULL, NULL),
(120, '1@mail.ru', '299865d0eacb2a05890d72a97e49c549', 1, 1, NULL, NULL),
(121, '§§§§', '7cdfb97a3f1ac735a894de4d820fffc6', 1, 1, NULL, NULL),
(122, '2@mail.ru', 'fbe50cde87eb89daf25681ed0d22deae', 1, 1, NULL, NULL),
(123, '1111@mail.ru', 'c4d5da673c3cf6d699c6fc97c000e407', 1, 1, NULL, NULL),
(124, '1"1юсщь', '73ebeb9eea4709845485579693d22ebe', 1, 1, NULL, NULL),
(125, 'hj@him', '319dcec3c2efb3157e0918ad34b38c5e', 1, 1, NULL, NULL),
(126, 'asd', '6a43b5d8a8707a85e3ec7d9293bacee0', 1, 1, NULL, NULL),
(127, 'megbrimef@gmail.com', '202cb962ac59075b964b07152d234b70', 1, 1, NULL, NULL),
(128, 'asd@dd.dd', '745c471d26b4d5d5f151708d04275641', 1, 1, NULL, NULL),
(129, 'adsfadfs', '08a5ea85f5325b8e860c563c65fe4ce1', 1, 1, NULL, NULL),
(130, 'ddddd@ddddd.com', 'fa4c9aaa03c68703d1e0baf9c2e7a7c4', 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tst_user_groups`
--

CREATE TABLE IF NOT EXISTS `tst_user_groups` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  KEY `fk_user_groups_user_id` (`user_id`),
  KEY `fk_user_groups_group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tst_user_groups`
--

INSERT INTO `tst_user_groups` (`user_id`, `group_id`) VALUES
(13, 2),
(18, 3),
(35, 1),
(31, 4),
(49, 4),
(20, 6),
(29, 4),
(58, 1),
(22, 4),
(61, 6),
(72, 1),
(73, 5),
(83, 1),
(19, 4),
(23, 4),
(24, 4),
(34, 4),
(36, 4),
(37, 4),
(38, 4),
(41, 4),
(42, 4),
(43, 4),
(44, 4),
(45, 4),
(46, 4),
(47, 4),
(54, 4),
(55, 4),
(59, 4),
(60, 4),
(62, 4),
(66, 4),
(67, 4),
(68, 4),
(70, 4),
(71, 4),
(74, 4),
(76, 4),
(77, 4),
(78, 4),
(85, 4),
(86, 4),
(88, 4),
(89, 4),
(90, 4),
(91, 4),
(92, 4),
(94, 4),
(95, 4),
(96, 4),
(97, 4),
(99, 4),
(100, 4),
(101, 3),
(103, 7),
(102, 8),
(106, 11),
(108, 12),
(109, 13),
(118, 14);

-- --------------------------------------------------------

--
-- Table structure for table `tst_user_profile`
--

CREATE TABLE IF NOT EXISTS `tst_user_profile` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `surname` varchar(100) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `discount_options` int(2) NOT NULL,
  `discount_residence` int(2) NOT NULL,
  `scan` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `adress` varchar(100) NOT NULL,
  `town` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `index` varchar(50) NOT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `requisites` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=131 ;

--
-- Dumping data for table `tst_user_profile`
--

INSERT INTO `tst_user_profile` (`user_id`, `company`, `name`, `surname`, `phone`, `discount_options`, `discount_residence`, `scan`, `description`, `adress`, `town`, `country`, `index`, `logo`, `requisites`) VALUES
(13, '', '', '', '', 0, 0, '', '', '', '', '', '', NULL, NULL),
(18, '', '123', '123', '123', 15, 20, '', '', '', '', '', '', NULL, NULL),
(19, 'dsfds', 'dsfds', 'sdfsd', 'sds', 0, 0, 'scan_1416202325.jpg', '', 'sdfsd', 'fsdf', 'dsfds', 'dsfds', NULL, NULL),
(20, 'turagent', 'Dmytro', 'Sukachev', '380675641740', 20, 10, 'scan_1417786586.jpg', '', 'Закревского 13а', 'Киев', 'Украина', '02002', 'logo_1425137548.jpg', 'jhdgasgdg 1902389012839012, sfherhf, ejrfhef834837,  345348758934,89458378953 3845893345'),
(22, '', '', '', '', 0, 0, '', '', '', '', '', '', NULL, NULL),
(29, '', '', '', '', 10, 10, '', '', '', '', '', '', NULL, NULL),
(31, '', 'Test', 'edsse', '235235', 0, 0, '', '', '', '', '', '', NULL, NULL),
(35, '', 'Александр', 'Лысак', '+380995480166', 20, 30, '', '', 'Интернациональная 67а/81', 'Павлоград', 'Украина', '51400', NULL, NULL),
(49, 'company', 'FirstName', 'FamilyName', 'PhoneNumber', 99, 99, 'scan_1416686348.png', '', 'adress', 'kyiv', 'Ukraine', 'postId', 'logo_1428135626.jpg', ''),
(54, '', 'dxh', 'xeh', '141241', 0, 0, '', '', '', '', '', '', NULL, NULL),
(55, '', 'seg', 'esh', '3252511', 0, 0, '', '', '', '', '', '', NULL, NULL),
(58, '', 'iodhiog', 'oudfo', '3970370', 1, 1, '', '', '', '', '', '', NULL, NULL),
(59, '', '1', '1', '1', 0, 0, '', '', '', '', '', '', NULL, NULL),
(60, '', 'wgkui', 'ugfia', '9325025602', 0, 0, '', '', '', '', '', '', NULL, NULL),
(61, 'some company', 'dufi', 'kuf', '2808260', 0, 0, '', '', '', '', '', '', 'logo_1424270714.jpg', 'w ug wiugw wugwo wowh wo wwo owwo ow '),
(62, '', 'Sergey', 'Suzdaltsev', '1111', 0, 0, '', '', '', '', '', '', NULL, NULL),
(66, '', 'це34е', 'цуацуа', '4345345345', 0, 0, '', '', '', '', '', '', NULL, NULL),
(67, '', 'f', 'f', '12345', 0, 0, '', '', '', '', '', '', NULL, NULL),
(68, '', 'sh', 'esh', '32525', 0, 0, '', '', '', '', '', '', NULL, NULL),
(71, '', '1', '1', '1', 0, 0, 'scan_1428139015.jpg', '', '', '', '', '', NULL, NULL),
(72, '', '', '', '', 0, 0, '', '', '', '', '', '', '', ''),
(73, '', 'Иванов', 'Иван', '847323743', 0, 0, '', '', '', '', '', '', '', ''),
(74, '', 'sej', 'diho', '2352', 0, 0, '', '', '', '', '', '', NULL, NULL),
(77, '', 'g', 'awg', '325252', 0, 0, '', '', '', '', '', '', NULL, NULL),
(78, '', 'awt', 'awg', '3522', 0, 0, '', '', '', '', '', '', NULL, NULL),
(83, '', 'Dmytro', 'Sukachev 016261', '+380675641740', 0, 0, '', '', '', '', '', '', '', ''),
(85, '', 'bbbb', 'bbbbb', '1345676', 0, 0, '', '', '', '', '', '', NULL, NULL),
(86, '', 'й', 'й', 'й', 0, 0, '', '', '', '', '', '', '', ''),
(88, '', 'sadasd', 'asdasd', '123123', 0, 0, '', '', '', '', '', '', NULL, NULL),
(89, '', 'dddd', 'dddd', '12333', 0, 0, '', '', '', '', '', '', NULL, NULL),
(90, '', '454t54t', '34rfergdgdf', '23423423', 0, 0, '', '', '', '', '', '', NULL, NULL),
(91, '', 'Александр', 'Биличак', '0678888888', 0, 0, '', '', '', '', '', '', NULL, NULL),
(92, '', 'фыввв', 'saddd', '123123123', 0, 0, '', '', '', '', '', '', NULL, NULL),
(94, '', 'n', 'n', '8236532650', 0, 0, '', '', '', '', '', '', NULL, NULL),
(95, '', 'bn', 'ndj', '8365926', 0, 0, '', '', '', '', '', '', NULL, NULL),
(96, '', 'Иван', 'Иванов', '6544644846', 0, 0, '', '', '', 'Киев', '', '24234', NULL, NULL),
(97, '', 'sa', 'sd', '43636', 0, 0, '', '', '', '', '', '', NULL, NULL),
(99, '', 'Dim', 'Hunter', '0671234567', 20, 10, '', '', '', 'Kiev', '', '', '', ''),
(100, '', 'esg', 'hse', '25223', 0, 0, '', '', '', '', '', '', NULL, NULL),
(111, '', 'aaaa', 'aaaa', '1212412421', 0, 0, '', '', '', '', '', '', NULL, NULL),
(112, '', 'aaaa', 'aaa', '3252542', 0, 0, '', '', '', '', '', '', NULL, NULL),
(113, '', 'Иванов', 'Иван', '380675555555', 0, 0, '', '', '', '', '', '', NULL, NULL),
(114, '', 'asdasd', 'asdasd', '213', 0, 0, '', '', '', '', '', '', NULL, NULL),
(115, '', 'sdfgdfg', 'sdfg', '2123123', 0, 0, '', '', '', '', '', '', NULL, NULL),
(116, '', 'name', 'family', '3808352365936', 0, 0, '', '', '', '', '', '', NULL, NULL),
(117, '', 'Dorian', 'Gray', '380683573536', 0, 0, '', '', '', '', '', '', NULL, NULL),
(119, '', '1', '1', '380909909297', 0, 0, '', '', '', '', '', '', NULL, NULL),
(120, '', '11', '11', '111', 0, 0, '', '', '', '', '', '', NULL, NULL),
(121, '', '§§§', '§§§§', '§§§§', 0, 0, '', '', '', '', '', '', NULL, NULL),
(122, '', '222222', '222222', '22222', 0, 0, '', '', '', '', '', '', NULL, NULL),
(123, '', '1', '1', '1', 0, 0, '', '', '', '', '', '', NULL, NULL),
(124, '', '1', '1', '038947528752', 0, 0, '', '', '', '', '', '', NULL, NULL),
(125, '', 'Алекс', 'Рыжков', '2048923848234', 0, 0, '', '', '', '', '', '', NULL, NULL),
(126, '', 'asd', 'asd', '123123123', 0, 0, '', '', '', '', '', '', NULL, NULL),
(127, 'asd', 'Сергей', 'mort', '0632893585', 0, 0, '', '', 'some adress', 'Kiev', 'Ukraine', '02152', '', 'Платежные реквизиты'),
(128, '', 'Сергей', 'mort', '13123123', 0, 0, '', '', '', '', '', '', NULL, NULL),
(129, '', 'asddfas', 'dafsdafs', '1321231232', 0, 0, '', '', '', '', '', '', NULL, NULL),
(130, '', 'Sergey', 'Suzdaltsev', '380683573536', 0, 0, '', '', '', '', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tst_villa_about_page`
--

CREATE TABLE IF NOT EXISTS `tst_villa_about_page` (
  `lang_id` int(11) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `html` text,
  `footer_html` text,
  `images` text,
  `seo_title` text,
  `seo_description` text,
  `seo_keywords` text,
  KEY `fk_villa_about_page_lang_id` (`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tst_villa_about_page`
--

INSERT INTO `tst_villa_about_page` (`lang_id`, `title`, `html`, `footer_html`, `images`, `seo_title`, `seo_description`, `seo_keywords`) VALUES
(2, 'eng header+', '<p>eng content +</p>\n', '<p>eng addititonal+</p>\n', '', 'eng title', 'eng descr', 'eng keywords'),
(3, 'Seitenkopf', '<p>Hallo, Willkommen im Hotel Villa LaScala.</p>\n\n<p>Die G&auml;ste unseres Hotels k&ouml;nnen sich auf qualitativ hochwertige Dienstleistungen, aufmerksamen Service und der Professionalit&auml;t der Mitarbeiter sprechen Englisch.<br />\nDas Hotel garantiert die Sicherheit und Privatsph&auml;re der G&auml;ste. Hier gibt es eine Atmosph&auml;re der Harmonie und des Friedens. Zimmer - 54 Seriennummer, um verschiedene Kategorien: Suite - 2 St&uuml;ck, Junior Suite - 4-teilig, Standard-Doppelzimmer - 38 Stck. Das Hotel ist in warmen Farben gehalten.</p>\n\n<p>Jedes Zimmer ist mit modernen M&ouml;beln und einem gro&szlig;en Bett King-Size-Gr&ouml;&szlig;e ausgestattet. Das Zimmer hat, LCD-TV mit 42-Zoll-TV, kostenlosem High-Speed-Wi-Fi, K&uuml;hlschrank / Minibar, einen elektronischen Safe mit der F&auml;higkeit, es unter dem Laptop, Telefon, elektrischer Wasserkocher mit dem Geschirr zu benutzen und alle nicht unwichtig kleinen Dinge f&uuml;r einen komfortablen lang oder kurz Residenz.<br />\nAlle Zimmer sind mit eigenem Badezimmer (WC, Dusche, warmes und kaltes Wasser, F&ouml;n), Fu&szlig;bodenheizung ausgestattet. Es bietet pers&ouml;nliche S&auml;tze von drei Handt&uuml;cher, Bademantel und Pantoffeln.</p>\n\n<p>Alle Zimmer sind mit elektronischen Schl&ouml;ssern mit Magnetkarte f&uuml;r Eingangst&uuml;r ausgestattet, die die Sicherheit und einfache Bedienung gew&auml;hrleistet</p>\n\n<p>Dank tsentralizirovanoy Klimasystem mit Temperaturregelung, ist Indoor-Winter immer warm und im Sommer k&uuml;hl, die vollst&auml;ndig die Notwendigkeit f&uuml;r eine Klimaanlage &uuml;berfl&uuml;ssig macht.</p>\n\n<p>Das Hotel verf&uuml;gt &uuml;ber 24 Stunden Rezeption, so dass die G&auml;ste und zu jeder beliebigen Tageszeit besetzt.<br />\nJederzeit (Tag, Nacht), aus dem Komfort Ihres Zimmers k&ouml;nnen Sie mit Zimmer servise (Bereitstellung von Snacks im Raum), und Sie bestellen Sie Ihre Lieblingsspeisen aus dem Men&uuml; und genie&szlig;en ihren Geschmack.</p>\n\n<p>Im Hotel betreibt eine permanente Ausstellung und Messe von Werken der ber&uuml;hmten ukrainischen Fotografen. Hotelg&auml;ste haben die exklusive M&ouml;glichkeit, zu kaufen alle ihre Lieblings-Arbeit.</p>\n\n<p>Im Hotel gibt es ein Restaurant in der ukrainischen Stil.<br />\nDurch den Besuch, die Sie in der Lage sein wird, in die Tiefen der nationalen Zoll und Lebensweise einzutauchen, genie&szlig;en Sie die nationale ukrainische K&uuml;che und bewundern Sie die gro&szlig;en Gem&auml;lde von ber&uuml;hmten ukrainischen K&uuml;nstler mit &Ouml;l durchgef&uuml;hrt (die gesamte Bildfl&auml;che betr&auml;gt 45 Quadratmeter. Meter).</p>\n\n<p>Auch im Hotel ist die Franz&ouml;sisch Markenshop von Bettw&auml;sche LaScala. Wo Sie Bettw&auml;sche kaufen, Decken, Kissen, Decken, Bettw&auml;sche und Handt&uuml;cher sind aus nat&uuml;rlichen Materialien aus hochwertiger Baumwolle, Seide, Wolle, unten, zu einem erschwinglichen Preis. Diese Akquisition wird ein schickes Souvenir f&uuml;r Sie und alle Ihre Lieben. Lassen Sie sich mit dem Sortiment des Ladens kennen, k&ouml;nnen Sie voran http://www.lascala.ua/.</p>\n\n<p>Bei Bedarf werden die Mitarbeiter jederzeit des Tages holen Sie am Ausgang der Ankunftshalle mit einem Schild am Flughafen Borispol nach oben, mit Gep&auml;ck und Abrechnung im Hotel helfen.</p>\n\n<p>Wir sind immer sehr froh, Sie zu sehen! Mit freundlichen Gr&uuml;&szlig;en freundliches Personal Villa LaScala Hotels! +</p>\n', '<h3><strong><span style="color:#FF0000;">Wir garantieren</span></strong></h3>\n\n<p>Das Zimmer ist mit modernen M&ouml;beln Satz von der doppelten Gr&ouml;&szlig;e von 180x200 (Kingsize) Bett, zwei Nachttische, einen Schreibtisch mit St&uuml;hlen und einem Spiegelschrank. Eine fast Spiegelwand perfekt mit dem Innenraum und gibt ihm ein Gef&uuml;hl von Leichtigkeit und Unendlichkeit. Auch im Zimmer: LCD-Fernseher mit einer Diagonale 42 &quot;, einen kostenlosen High-Speed-Wi-Fi-Internetzugang, Telefon, elektronischen Safe, mit der F&auml;higkeit, einen Laptop zu speichern darin, K&uuml;hlschrank / Minibar sowie alles, was Sie f&uuml;r einen angenehmen Aufenthalt ben&ouml;tigen.</p>\n', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tst_villa_about_page_images`
--

CREATE TABLE IF NOT EXISTS `tst_villa_about_page_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` char(30) NOT NULL,
  `draw_order` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tst_villa_about_page_images`
--

INSERT INTO `tst_villa_about_page_images` (`id`, `image`, `draw_order`) VALUES
(1, '1410181447.jpg', 1),
(3, '1463334753.jpg', 2),
(4, '1463420979.png', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tst_villa_about_page_services`
--

CREATE TABLE IF NOT EXISTS `tst_villa_about_page_services` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lang_id` int(11) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `draw_order` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_villa_about_page_services_lang_id` (`lang_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `tst_villa_about_page_services`
--

INSERT INTO `tst_villa_about_page_services` (`id`, `lang_id`, `title`, `description`, `draw_order`) VALUES
(8, 2, 'eng service 1', 'eng service 1 descr', 5),
(9, 2, 'eng service 2', 'eng service 2 descr', 6),
(11, 2, 'uhuihijoi', 'жалмовамвамва Управление изображениями для галереи', 8),
(12, 2, 'uhuihijoi', 'жалмовамвамва Управление изображениями для галереи', 9),
(13, 2, 'uhuihijoi', 'жалмовамвамва Управление изображениями для галереи', 10),
(14, 2, 'uhuihijoi', 'жалмовамвамва Управление изображениями для галереи', 11),
(15, 2, 'service 5', 'service 5', 12),
(16, 2, 'service 5', 'service 5', 13),
(17, 2, 'service 5', 'service 5', 14),
(18, 2, 'service 5', 'service 5', 15),
(19, 2, 'service 5', 'service 5', 16),
(20, 2, 'service 5', 'service 5', 17),
(22, 1, 'test', 'test', 18);

-- --------------------------------------------------------

--
-- Table structure for table `tst_villa_booking_info`
--

CREATE TABLE IF NOT EXISTS `tst_villa_booking_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lang_id` int(11) unsigned NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `caption` varchar(200) DEFAULT NULL,
  `text` text,
  `draw_order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_booking_info_lang_id` (`lang_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tst_villa_booking_info`
--

INSERT INTO `tst_villa_booking_info` (`id`, `lang_id`, `type`, `caption`, `text`, `draw_order`) VALUES
(5, 2, 2, 'Block', NULL, 1),
(6, 2, 2, '1', NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tst_villa_booking_info_list`
--

CREATE TABLE IF NOT EXISTS `tst_villa_booking_info_list` (
  `block_id` int(11) unsigned NOT NULL,
  `row_id` int(11) unsigned NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `text` text,
  KEY `fk_booking_info_list_block_id` (`block_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tst_villa_contacts`
--

CREATE TABLE IF NOT EXISTS `tst_villa_contacts` (
  `lang_id` int(11) unsigned NOT NULL,
  `title` varchar(100) NOT NULL,
  `contacts` varchar(500) NOT NULL,
  `map_html` varchar(500) DEFAULT NULL,
  `seo_title` text,
  `seo_keywords` text,
  `seo_description` text,
  KEY `fk_villa_contacts_lang_id` (`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tst_villa_contacts`
--

INSERT INTO `tst_villa_contacts` (`lang_id`, `title`, `contacts`, `map_html`, `seo_title`, `seo_keywords`, `seo_description`) VALUES
(2, 'd', '', '', 'dss', 'dug', 'dss'),
(3, 'Kontakte', '<p>Ukraine, Stadt Kiew, ave. Erfreuliche, 7.</p>\n', '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1270.614152541083!2d30.436243!3d50.436848!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xde05b4f6eed8fedd!2sHotel+Villa+LaScala!5e0!3m2!1sru!2sua!4v1413881278715" width="940" height="600" frameborder="0" style="border:0"></iframe>', '', '', ''),
(1, '', '<p>контакты</p>\n', NULL, 'title2', 'keywords222', '2descr222');

-- --------------------------------------------------------

--
-- Table structure for table `tst_villa_homepage`
--

CREATE TABLE IF NOT EXISTS `tst_villa_homepage` (
  `lang_id` int(11) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `html` text,
  `seo_title` text,
  `seo_description` text,
  `seo_keywords` text,
  `full_descr` text,
  KEY `fk_villa_home_page_lang_id` (`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tst_villa_homepage`
--

INSERT INTO `tst_villa_homepage` (`lang_id`, `title`, `html`, `seo_title`, `seo_description`, `seo_keywords`, `full_descr`) VALUES
(2, ' homepage', '', '', '', '', ''),
(3, 'Deutsch', '<p>Deutsch</p>\r\n', 'Deutsch', 'Deutsch', 'Deutsch', '<p>Deutsch</p>\r\n'),
(1, '', '', 'villa1', 'villa2', '3villa3', '');

-- --------------------------------------------------------

--
-- Table structure for table `tst_villa_homepage_excursions`
--

CREATE TABLE IF NOT EXISTS `tst_villa_homepage_excursions` (
  `lang_id` int(11) unsigned NOT NULL,
  `day` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `html` text,
  `full_descr` text,
  `max_guests` int(11) DEFAULT NULL,
  `outer_id` int(11) DEFAULT NULL,
  KEY `fk_villa_home_page_excursions_lang_id` (`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tst_villa_homepage_excursions`
--

INSERT INTO `tst_villa_homepage_excursions` (`lang_id`, `day`, `title`, `html`, `full_descr`, `max_guests`, `outer_id`) VALUES
(2, 1, NULL, '', '', 3, 0),
(2, 2, NULL, '<p><strong>Holy sanctuary of Ukraine-Rus &ndash; Kyiv-Pechersk Lavra</strong></p>\r\n', '<p>You are welcome to visit the holy sanctuary of Ukraine-Rus &ndash; the monastery of Kyiv-Pechersk Lavra. This ancient sacred place is called Jerusalem of Rus, Ukrainian Vatican, and Orthodox Mecca. Pilgrims from a lot of countries walked to this place to be purged of sin and to get blessing. Even kings walked through the holy gates on foot only, leaving their carriages. It is believed that being here itself purifies and gives enlightenment.</p>\r\n\r\n<p>This is the highest place in historical part of Kyiv. You can admire the unique architectural complex of Kyiv Lavra, see legendary domes of 20 temples and hear crystal sound of the bells.</p>\r\n\r\n<p>In caves of Lavra you can touch the imperishable hallows of ancient hermits that may miraculously heal and assist in everyday life. Lavra museums give an opportunity to get acquainted with tangible and spiritual culture of Ukraine of thousands of years. You will visit the Museum of Historical Treasures of Ukraine, where you will see legendary gold of the Scythians; you will marvel at Ukrainian popular craftwork items in the Museum of Decorative Arts and even will see a shod flea at the exhibition of micro miniatures.</p>\r\n', 1, 0),
(2, 3, NULL, '<p>sehsehesh</p>\r\n', '<p>shhseeshs</p>\r\n', 1, 0),
(2, 4, NULL, '<p>sehshseh</p>\r\n', '<p>iug iug iugf iugf iu;g i; i;</p>\r\n', 1, 0),
(2, 5, NULL, '<p>sehsheshseh</p>\r\n', '<p>seheshesh</p>\r\n', 1, 0),
(2, 6, NULL, '<p>aaaaaaaaaa</p>\r\n', '<p>aaa</p>\r\n', 1, 0),
(2, 7, NULL, '<p>rrrrrrrrrrr</p>\r\n', '<p>rrrrrrr</p>\r\n', 123, 236),
(3, 1, NULL, '', '', 0, 0),
(3, 2, NULL, '<p><strong>Духовная святыня Украины-Руси &ndash; Киево-Печерская Лавра</strong></p>\r\n', '<p>Sie sind herzlich eingeladen, das geistliche Heiligtum der Ukraine-Rus&nbsp;&ndash; das Kiewer H&ouml;hlenkloster zu besuchen. Dieses alte heilige Kloster wird als &bdquo;Russisches Jerusalem&ldquo;, &bdquo;Ukrainischer Vatikan&ldquo; und &bdquo;Orthodoxes Mekka&ldquo; bezeichnet. Zum S&uuml;ndenerlass und Erhaltung eines Segens gingen hierher zu Fu&szlig; die Wallfahrer aus vielen L&auml;ndern. Sogar die Zaren stiegen hier aus ihren Kutschen aus und schritten durch die Heilige Pforte nur zu Fu&szlig;. Man behauptet, der Aufenthalt in diesen W&auml;nden als solcher reinigt die Seele und schenkt eine geistige Offenbarung.</p>\r\n\r\n<p>Hier befindet sich der h&ouml;chste historische Stadtteil Kiews. Sie k&ouml;nnen das einzigartige Architekturensemble des Kiewer H&ouml;hlenklosters bewundern, nebenbei werden Sie die legend&auml;ren Goldkuppeln der 20 Gottesh&auml;user erblicken und einen glockenfeinen Ton h&ouml;ren.</p>\r\n\r\n<p>In den Klosterh&ouml;hlen kann man unversehrte Reliquien der alten Einsiedler ber&uuml;hren, die Heilung und Unterst&uuml;tzung bei allen Taten schenken. Die Museen des H&ouml;hlenklosters bieten Ihnen eine M&ouml;glichkeit, materielle und geistliche Kultur der Ukraine im Lauf der mehrtausendj&auml;hrigen Geschichte kennenzulernen. Sie werden das Museum der historischen Kostbarkeiten der Ukraine besuchen und das legend&auml;re Gold der Skythen mit Ihren Augen sehen; im Museum des Dekors werden Sie auch die Kunstgegenst&auml;nde der ukrainischen Volkskunst bewundern und sogar einen mit Hufeisen beschlagenen Floh im Rahmen der Ausstellung von Mikrominiaturkunstwerken erblicken.</p>\r\n', 0, 0),
(3, 3, NULL, '', '', 0, 0),
(3, 4, NULL, '', '', 0, 0),
(3, 5, NULL, '', '', 0, 0),
(3, 6, NULL, '', '', 0, 0),
(3, 7, NULL, '', '', 0, 0),
(5, 1, NULL, '', '', 0, 0),
(5, 2, NULL, '<p><strong>Духовная святыня Украины-Руси &ndash; Киево-Печерская Лавра</strong></p>\r\n', '<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Vi invitiamo a visitare la reliquia spirituale della antica Rus&rsquo; Ucraina &ndash; Kievo-Pecherska Lavra. Questo santo antico monastero &egrave; stato soprannominato la Gerusalemme Russa, il Vaticano Ucraino, la Mecca ortodossa. Pellegrini da molti paesi vi andarono a piedi per purificarsi dai peccati ed essere benedetti. Persino i re dovevano lasciare le loro carrozze all&rsquo;ingresso perch&egrave; si poteva oltrepassare le porte sante solo a piedi. Si pensa che anche il solo soggiornare tra queste mura purifichi e di&agrave; l&rsquo;illuminazione.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Qui si trova la parte storica di kiev pi&ugrave; alta. Potrete ammirare un complesso architettonico unico, vedrete le leggendarie cupole d&rsquo;oro dei 20 templi e sentirete il rintocco cristallino delle loro campane.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nelle grotte di Lavra potrete toccare le spoglie imperiture degli antichi eremiti le quali favoriscono la guarigione e gli affari. I musei di Lavra vi daranno la possibilit&agrave; di conoscere la cultura materiale e spirituale dell&rsquo;Ucraina che vanta una storia multi millenaria. Visiterete il museo dei tesori storici dove potrete vedere con i vostri occhi il leggendario oro degli sciiti, ammirerete i lavori dell&rsquo;arte popolare dell&rsquo;Ucraina nel Museo del decoro ed inoltre potrete vedere la pulce ferrata alla mostra delle micro miniature.</p>\r\n', 0, 0),
(5, 3, NULL, '', '', 0, 0),
(5, 4, NULL, '', '', 0, 0),
(5, 5, NULL, '', '', 0, 0),
(5, 6, NULL, '', '', 0, 0),
(5, 7, NULL, '', '', 0, 0),
(1, 1, '', '', '', 50, 223),
(1, 2, '', '', '', 0, 0),
(1, 3, '', '', '', 0, 0),
(1, 4, '', '', '', 0, 0),
(1, 5, '', '', '', 0, 0),
(1, 6, '', '', '', 0, 0),
(1, 7, '', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tst_villa_homepage_gallery`
--

CREATE TABLE IF NOT EXISTS `tst_villa_homepage_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id` int(11) unsigned NOT NULL,
  `entity_id` int(11) NOT NULL,
  `image` varchar(50) NOT NULL,
  `draw_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_villa_homepage_gallery_entity_type_id` (`entity_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=44 ;

--
-- Dumping data for table `tst_villa_homepage_gallery`
--

INSERT INTO `tst_villa_homepage_gallery` (`id`, `entity_type_id`, `entity_id`, `image`, `draw_order`) VALUES
(1, 8, 1, '1411749588.jpg', 1),
(2, 8, 1, '1411749592.jpg', 2),
(4, 7, 1, '1413354376.jpg', 2),
(5, 7, 1, '1413354380.jpg', 3),
(6, 7, 1, '1413354382.jpg', 4),
(7, 7, 2, '1413354398.jpg', 1),
(8, 7, 2, '1413354403.jpg', 2),
(9, 7, 2, '1413354409.jpg', 3),
(10, 7, 3, '1413354420.jpg', 1),
(11, 7, 3, '1413354422.jpg', 2),
(12, 7, 4, '1413354428.jpg', 1),
(13, 7, 4, '1413354436.jpg', 2),
(14, 7, 5, '1413354462.jpg', 1),
(15, 7, 5, '1413354468.jpg', 2),
(16, 7, 7, '1413354486.jpg', 1),
(17, 7, 7, '1413354491.jpg', 2),
(18, 7, 7, '1413354501.jpg', 3),
(19, 7, 6, '1413354508.jpg', 1),
(20, 7, 6, '1413354512.jpg', 2),
(21, 7, 6, '1413354513.jpg', 3),
(35, 6, 1, '1411766093.jpg', 1),
(36, 6, 1, '1411766098.jpg', 2),
(37, 6, 1, '1411766101.jpg', 3),
(38, 6, 1, '1411766106.jpg', 4),
(39, 6, 1, '1411766110.jpg', 5),
(40, 6, 1, '1411766119.jpg', 6),
(42, 8, 1, '1428139646.jpg', 3),
(43, 7, 1, '1443624968.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tst_villa_homepage_transfer`
--

CREATE TABLE IF NOT EXISTS `tst_villa_homepage_transfer` (
  `lang_id` int(11) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `html` text,
  `full_descr` text,
  KEY `fk_villa_home_page_transfer_lang_id` (`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tst_villa_homepage_transfer`
--

INSERT INTO `tst_villa_homepage_transfer` (`lang_id`, `title`, `html`, `full_descr`) VALUES
(2, '', '', ''),
(3, 'Deutsch transfer', '<p>Deutsch</p>\r\n', '<p>Deutsch full</p>\r\n'),
(1, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tst_villa_infoblock`
--

CREATE TABLE IF NOT EXISTS `tst_villa_infoblock` (
  `lang_id` int(11) unsigned NOT NULL,
  `text` text,
  KEY `fk_infoblock_lang_id` (`lang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tst_villa_mail`
--

CREATE TABLE IF NOT EXISTS `tst_villa_mail` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` int(11) unsigned NOT NULL,
  `lang_id` int(11) unsigned NOT NULL,
  `subject` varchar(100) NOT NULL,
  `subject_2` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_villa_mails_type_id` (`type_id`),
  KEY `fk_villa_mails_lang_id` (`lang_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tst_villa_mail`
--

INSERT INTO `tst_villa_mail` (`id`, `type_id`, `lang_id`, `subject`, `subject_2`) VALUES
(5, 2, 2, 'Your booking', NULL),
(6, 1, 2, 'Registration', NULL),
(7, 4, 2, 'comment answer', NULL),
(8, 3, 2, 'question answer', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tst_villa_mail_parts`
--

CREATE TABLE IF NOT EXISTS `tst_villa_mail_parts` (
  `mail_id` int(11) unsigned NOT NULL,
  `part_id` varchar(50) NOT NULL,
  `text` varchar(200) DEFAULT NULL,
  KEY `fk_villa_mail_parts_mail_id` (`mail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tst_villa_mail_parts`
--

INSERT INTO `tst_villa_mail_parts` (`mail_id`, `part_id`, `text`) VALUES
(6, '1', 'You are successfully registered at'),
(6, '2', 'Your login data'),
(6, '3', 'Email'),
(6, '4', 'Password'),
(7, '1', 'you have left comment at'),
(7, '2', 'your comment'),
(7, '3', 'answer'),
(8, '1', 'you asked a question at'),
(8, '2', 'your question'),
(8, '3', 'answer');

-- --------------------------------------------------------

--
-- Table structure for table `tst_villa_mail_types`
--

CREATE TABLE IF NOT EXISTS `tst_villa_mail_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tst_villa_mail_types`
--

INSERT INTO `tst_villa_mail_types` (`id`, `name`) VALUES
(1, 'Регистрация'),
(2, 'Бронирование'),
(3, 'Ответ на вопрос'),
(4, 'Ответ на отзыв');

-- --------------------------------------------------------

--
-- Table structure for table `tst_villa_settings`
--

CREATE TABLE IF NOT EXISTS `tst_villa_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `discount_child` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tst_villa_settings`
--

INSERT INTO `tst_villa_settings` (`id`, `discount_child`) VALUES
(1, 50);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tst_airlines_flights`
--
ALTER TABLE `tst_airlines_flights`
  ADD CONSTRAINT `fk_airlines_flights_town_id` FOREIGN KEY (`town_id`) REFERENCES `tst_currencytowns` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_airlines_flights_country_id` FOREIGN KEY (`country_id`) REFERENCES `tst_currencycountries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_airlines_flights_id` FOREIGN KEY (`airline_id`) REFERENCES `tst_airlines` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_comment`
--
ALTER TABLE `tst_comment`
  ADD CONSTRAINT `fk_comment_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_comment_entity_type_id` FOREIGN KEY (`entity_type_id`) REFERENCES `tst_entity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_currencycountries`
--
ALTER TABLE `tst_currencycountries`
  ADD CONSTRAINT `fk_currencycountries_currency` FOREIGN KEY (`currency`) REFERENCES `tst_currency` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_currencycountries_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_currencycountries_lang`
--
ALTER TABLE `tst_currencycountries_lang`
  ADD CONSTRAINT `fk_currencycountries_lang_country_id` FOREIGN KEY (`country_id`) REFERENCES `tst_currencycountries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_currencycountries_lang_lang_id` FOREIGN KEY (`set_lang`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_currencytowns`
--
ALTER TABLE `tst_currencytowns`
  ADD CONSTRAINT `fk_currencytowns_country_id` FOREIGN KEY (`country_id`) REFERENCES `tst_currencycountries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_currencytowns_lang`
--
ALTER TABLE `tst_currencytowns_lang`
  ADD CONSTRAINT `fk_currencytowns_lang_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_currencytowns_lang_town_id` FOREIGN KEY (`town_id`) REFERENCES `tst_currencytowns` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_currency_lang`
--
ALTER TABLE `tst_currency_lang`
  ADD CONSTRAINT `fk_currency_lang_id` FOREIGN KEY (`id`) REFERENCES `tst_currency` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_currency_lang_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_division_access`
--
ALTER TABLE `tst_division_access`
  ADD CONSTRAINT `division_access_ibfk_1` FOREIGN KEY (`division_id`) REFERENCES `tst_division` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `division_access_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `tst_groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_division_groups`
--
ALTER TABLE `tst_division_groups`
  ADD CONSTRAINT `fk_division_groups_division_id` FOREIGN KEY (`division_id`) REFERENCES `tst_division` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_division_groups_group_id` FOREIGN KEY (`group_id`) REFERENCES `tst_groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_division_seo`
--
ALTER TABLE `tst_division_seo`
  ADD CONSTRAINT `fk_division_seo_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_gallery`
--
ALTER TABLE `tst_gallery`
  ADD CONSTRAINT `fk_gallery_entity` FOREIGN KEY (`entity_type_id`) REFERENCES `tst_entity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_gallery_uid` FOREIGN KEY (`uid`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_hotel_booking`
--
ALTER TABLE `tst_hotel_booking`
  ADD CONSTRAINT `fk_hotel_booking_prepayment_type` FOREIGN KEY (`prepayment_type`) REFERENCES `tst_hotel_prepayment_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_booking_currency_id` FOREIGN KEY (`currency_id`) REFERENCES `tst_currency` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hotel_booking_modified_by_user` FOREIGN KEY (`modified_by`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hotel_booking_room_id` FOREIGN KEY (`room`) REFERENCES `tst_hotel_rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hotel_booking_room_type_id` FOREIGN KEY (`room_type`) REFERENCES `tst_hotel_room_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hotel_booking_status` FOREIGN KEY (`status`) REFERENCES `tst_hotel_booking_statuses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hotel_booking_uid` FOREIGN KEY (`uid`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_hotel_booking_excursions`
--
ALTER TABLE `tst_hotel_booking_excursions`
  ADD CONSTRAINT `fk_hotel_booking_excursions_booking_id` FOREIGN KEY (`booking_id`) REFERENCES `tst_hotel_booking` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_hotel_booking_extra_services`
--
ALTER TABLE `tst_hotel_booking_extra_services`
  ADD CONSTRAINT `fk_booking_extra_services_booking_id` FOREIGN KEY (`booking_id`) REFERENCES `tst_hotel_booking` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_hotel_booking_guests`
--
ALTER TABLE `tst_hotel_booking_guests`
  ADD CONSTRAINT `fk_hotel_booking_guests_booking_id` FOREIGN KEY (`booking_id`) REFERENCES `tst_hotel_booking` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_hotel_booking_payment_history`
--
ALTER TABLE `tst_hotel_booking_payment_history`
  ADD CONSTRAINT `fk_hotel_booking_deposit_history_user_id` FOREIGN KEY (`user_id`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hotel_booking_payment_history_booking_id` FOREIGN KEY (`booking_id`) REFERENCES `tst_hotel_booking` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_hotel_booking_services`
--
ALTER TABLE `tst_hotel_booking_services`
  ADD CONSTRAINT `fk_hotel_booking_services_booking_id` FOREIGN KEY (`booking_id`) REFERENCES `tst_hotel_booking` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hotel_booking_services_service_id` FOREIGN KEY (`service_id`) REFERENCES `tst_hotel_services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_hotel_booking_transfer`
--
ALTER TABLE `tst_hotel_booking_transfer`
  ADD CONSTRAINT `fk_hotel_booking_transfer_booking_id` FOREIGN KEY (`booking_id`) REFERENCES `tst_hotel_booking` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hotel_booking_transfer_transfer_id` FOREIGN KEY (`transfer_id`) REFERENCES `tst_hotel_transfer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_hotel_booking_versions`
--
ALTER TABLE `tst_hotel_booking_versions`
  ADD CONSTRAINT `fk_hotel_booking_versions_uid` FOREIGN KEY (`uid`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_hotel_cash`
--
ALTER TABLE `tst_hotel_cash`
  ADD CONSTRAINT `fk_hotel_cash_user_id` FOREIGN KEY (`user_id`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_hotel_cash_transactions`
--
ALTER TABLE `tst_hotel_cash_transactions`
  ADD CONSTRAINT `fk_hotel_cash_transactions_user_from` FOREIGN KEY (`user_from`) REFERENCES `tst_hotel_cash` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hotel_cash_transactions_user_to` FOREIGN KEY (`user_to`) REFERENCES `tst_hotel_cash` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_hotel_equipment_lang`
--
ALTER TABLE `tst_hotel_equipment_lang`
  ADD CONSTRAINT `fk_hotel_equipment_lang_id` FOREIGN KEY (`id`) REFERENCES `tst_hotel_equipment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hotel_equipment_lang_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_hotel_prepayment_types_lang`
--
ALTER TABLE `tst_hotel_prepayment_types_lang`
  ADD CONSTRAINT `fk_hotel_prepayment_types_lang_type_id` FOREIGN KEY (`type_id`) REFERENCES `tst_hotel_prepayment_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hotel_prepayment_types_lang_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_hotel_prices`
--
ALTER TABLE `tst_hotel_prices`
  ADD CONSTRAINT `fk_hotel_prices_prep_type` FOREIGN KEY (`prep_type`) REFERENCES `tst_hotel_prepayment_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hotel_prices_entity_type` FOREIGN KEY (`entity_type_id`) REFERENCES `tst_entity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_hotel_prices_dates`
--
ALTER TABLE `tst_hotel_prices_dates`
  ADD CONSTRAINT `fk_hotel_prices_dates_price_id` FOREIGN KEY (`price_id`) REFERENCES `tst_hotel_prices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_hotel_rooms`
--
ALTER TABLE `tst_hotel_rooms`
  ADD CONSTRAINT `fk_hotel_rooms_type` FOREIGN KEY (`type`) REFERENCES `tst_hotel_room_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_hotel_room_types_equipment`
--
ALTER TABLE `tst_hotel_room_types_equipment`
  ADD CONSTRAINT `fk_hotel_room_types_equipment_type_id` FOREIGN KEY (`type_id`) REFERENCES `tst_hotel_room_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hotel_room_types_equipment_eq_id` FOREIGN KEY (`eq_id`) REFERENCES `tst_hotel_equipment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_hotel_room_types_images`
--
ALTER TABLE `tst_hotel_room_types_images`
  ADD CONSTRAINT `fk_hotel_room_types_images_type_id` FOREIGN KEY (`type_id`) REFERENCES `tst_hotel_room_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_hotel_room_types_lang`
--
ALTER TABLE `tst_hotel_room_types_lang`
  ADD CONSTRAINT `fk_hotel_room_types_type_id` FOREIGN KEY (`type_id`) REFERENCES `tst_hotel_room_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hotel_room_types_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_hotel_services_images`
--
ALTER TABLE `tst_hotel_services_images`
  ADD CONSTRAINT `fk_hotel_services_images_service_id` FOREIGN KEY (`service_id`) REFERENCES `tst_hotel_services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_hotel_services_lang`
--
ALTER TABLE `tst_hotel_services_lang`
  ADD CONSTRAINT `fk_hotel_services_lang_id` FOREIGN KEY (`service_id`) REFERENCES `tst_hotel_services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hotel_services_lang_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_hotel_transfer_lang`
--
ALTER TABLE `tst_hotel_transfer_lang`
  ADD CONSTRAINT `fk_hotel_transfer_lang_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hotel_transfer_lang_id` FOREIGN KEY (`transfer_id`) REFERENCES `tst_hotel_transfer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_labels_lang`
--
ALTER TABLE `tst_labels_lang`
  ADD CONSTRAINT `fk_labels_lang_id` FOREIGN KEY (`id`) REFERENCES `tst_labels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_labels_lang_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_publications`
--
ALTER TABLE `tst_publications`
  ADD CONSTRAINT `fk_publications_division` FOREIGN KEY (`division`) REFERENCES `tst_division` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_publications_uid` FOREIGN KEY (`uid`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_publications_division`
--
ALTER TABLE `tst_publications_division`
  ADD CONSTRAINT `publications_division_ibfk_1` FOREIGN KEY (`publications_id`) REFERENCES `tst_publications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `publications_division_ibfk_2` FOREIGN KEY (`division_id`) REFERENCES `tst_division` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_publications_lang`
--
ALTER TABLE `tst_publications_lang`
  ADD CONSTRAINT `fk_publications_lang_page_id` FOREIGN KEY (`page_id`) REFERENCES `tst_publications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_rating_log`
--
ALTER TABLE `tst_rating_log`
  ADD CONSTRAINT `fk_rating_log_obj_id` FOREIGN KEY (`obj_id`) REFERENCES `tst_rating` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_sessions`
--
ALTER TABLE `tst_sessions`
  ADD CONSTRAINT `fk_sessions_uid` FOREIGN KEY (`uid`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_user_groups`
--
ALTER TABLE `tst_user_groups`
  ADD CONSTRAINT `fk_user_groups_group_id` FOREIGN KEY (`group_id`) REFERENCES `tst_groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_groups_user_id` FOREIGN KEY (`user_id`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_user_profile`
--
ALTER TABLE `tst_user_profile`
  ADD CONSTRAINT `fk_user_profile_user_id` FOREIGN KEY (`user_id`) REFERENCES `tst_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_villa_about_page`
--
ALTER TABLE `tst_villa_about_page`
  ADD CONSTRAINT `fk_villa_about_page_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_villa_about_page_services`
--
ALTER TABLE `tst_villa_about_page_services`
  ADD CONSTRAINT `fk_villa_about_page_services_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_villa_booking_info`
--
ALTER TABLE `tst_villa_booking_info`
  ADD CONSTRAINT `fk_booking_info_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_villa_booking_info_list`
--
ALTER TABLE `tst_villa_booking_info_list`
  ADD CONSTRAINT `fk_booking_info_list_block_id` FOREIGN KEY (`block_id`) REFERENCES `tst_villa_booking_info` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_villa_contacts`
--
ALTER TABLE `tst_villa_contacts`
  ADD CONSTRAINT `fk_villa_contacts_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_villa_homepage`
--
ALTER TABLE `tst_villa_homepage`
  ADD CONSTRAINT `fk_villa_home_page_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_villa_homepage_excursions`
--
ALTER TABLE `tst_villa_homepage_excursions`
  ADD CONSTRAINT `fk_villa_home_page_excursions_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_villa_homepage_gallery`
--
ALTER TABLE `tst_villa_homepage_gallery`
  ADD CONSTRAINT `fk_villa_homepage_gallery_entity_type_id` FOREIGN KEY (`entity_type_id`) REFERENCES `tst_entity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_villa_homepage_transfer`
--
ALTER TABLE `tst_villa_homepage_transfer`
  ADD CONSTRAINT `fk_villa_home_page_transfer_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_villa_infoblock`
--
ALTER TABLE `tst_villa_infoblock`
  ADD CONSTRAINT `fk_infoblock_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_villa_mail`
--
ALTER TABLE `tst_villa_mail`
  ADD CONSTRAINT `fk_villa_mail_lang_id` FOREIGN KEY (`lang_id`) REFERENCES `tst_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_villa_mail_type_id` FOREIGN KEY (`type_id`) REFERENCES `tst_villa_mail_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tst_villa_mail_parts`
--
ALTER TABLE `tst_villa_mail_parts`
  ADD CONSTRAINT `fk_villa_mail_parts_mail_id` FOREIGN KEY (`mail_id`) REFERENCES `tst_villa_mail` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
