<?php
global $package;
$package = array(
    'version' => '1',
    'name' => 'lib.jdatepicker',
    'dependence' => array('lib.jquery.ui'),
    'js' => array(
        '../lib/jdatepicker/jquery.ui.datepicker.js',
        '../lib/jdatepicker/datepicker-locales.js',
    )
);