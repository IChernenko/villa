<?php

if (!function_exists('geoip_country_code_by_addr') ) {
    include_once('../lib/geoip/geoip.inc.php');
}


/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 15.05.12
 * Time: 16:53
 * To change this template use File | Settings | File Templates.
 */
class Lib_Geoip_Helper
{
    public function getCountryCode($ip) {
        $gi = geoip_open("../lib/geoip/GeoIP.dat", GEOIP_STANDARD);
        $code = geoip_country_code_by_addr($gi, $ip);
        geoip_close($gi);
        return $code;
    }
}