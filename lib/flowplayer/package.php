<?php
global $package;
$package = array(
    'version' => '1',
    'name' => 'lib.flowplayer',
    'dependence' => array(),
    'js' => array(
        '../lib/flowplayer/js/flowplayer-3.2.6.min.js'
    )
);