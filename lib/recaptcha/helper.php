<?php

require_once '../lib/recaptcha/recaptchalib.php';

class Lib_Recaptcha_Helper
{
    private $_publicKey = '6Lfqr-QSAAAAABJXG5AYRc8Qh5-dUhkFKnNuU2n8';
    private $_privateKey = '6Lfqr-QSAAAAAF5AdR0UDBi-KavDR6kBo1w9Pdzm';
    
    protected $_fields = array(
        'challenge' => 'recaptcha_challenge_field',
        'response' => 'recaptcha_response_field'
    );
    
    public function getFields()
    {
        return $this->_fields;
    }

    public function get()
    {
        return recaptcha_get_html($this->_publicKey);
    }
    
    public function check($challenge, $response)
    {
        $ip = $_SERVER["REMOTE_ADDR"];
        
        $resp = recaptcha_check_answer($this->_privateKey, $ip, $challenge, $response);
        
        if ($resp->is_valid) return true;
        else return false;
    }
            
}


?>