<?php
class Lib_Elfinder_Controller extends Dante_Controller_Base{
    protected function _defaultAction(){
        $path = 'media';
        $url = '/media/';
        $folder = $this->_getRequest()->get('folder');
        if($folder) 
        {
            $path .= '/'.$folder;
            $url .= $folder;
        }
        Dante_Lib_Fs_Helper::createDirByPath($path);
        $opts = array(
            'roots' => array(
                array(
                    'driver'        => 'localfilesystem',   // driver for accessing file system (REQUIRED)
                    'path'          => $path,         // path to files (REQUIRED)
                    'URL'           => $url, // URL to files (REQUIRED)
                    //'accessControl' => 'access'             // disable and hide dot starting files (OPTIONAL)
                    )
                )
            );
        $elfinder  = new Lib_Elfinder_Elfinder($opts);
        $connector = new Lib_Elfinder_Connector($elfinder);
        $connector->run();
    }
}