<?php
global $package;
$package = array(
    'version' => '0',
    'name' => 'lib.elfinder',
    'dependence' => array(
        'lib.jquery.ui'
    ),
    'js' => array(
        '../lib/elfinder/js/elfinder.min.js',
        '../lib/elfinder/js/i18n/elfinder.ru.js',
    ),
    'css' => array(
        '../lib/elfinder/css/elfinder.min.css',
        '../lib/elfinder/css/theme.css',
    )
);