<?php
global $package;
$package = array(
    'version' => '1',
    'name' => 'lib.lightbox',
    'dependence' => array(),
    'js' => array('../lib/lightbox/js/jquery.lightbox-0.5.min.js'),
    'css' => array('../lib/lightbox/css/jquery.lightbox-0.5.css')
);