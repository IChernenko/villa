<?php
/**
 * Пейджинатор. 
 */
class Lib_Paginator_Driver extends Dante_Controller_Base{
    
    /**
     * Свойства формы для пейджинатора
     * @var array 
     */
    protected $_form=array(
        'enable'=>false,
        'action'=>false,
        'id'=>'paginator',
        'name'=>false
    );
    /**
     * Сгенерированнй html
     * @var str 
     */
    public $html;
    /**
     * Если линк указан то успользуется клиенский тесмплейт вида /link/page1
     * @var type 
     */
    public $link=false;
    /**
     * Определяем использовать ли <input> пейджинатора, или будет назначен свой.
     * @var boolean 
     */
    public $useDefaultInputs=true;
    /**
     * Генерируем строку лимита
     * @var string 
     */
    public $limit=false;
    
    /**
     * Варианты значения "кол-во сток на странице"
     * @var type 
     */
    public $rowsInPageSelect = array(15, 30, 50, 100);
    /**
     * Указывает использовать ли свою форму для поиска и сортировок
     * Обрабатываемые параметры 
     * По умолчанию форма отключена , но  при использовании этой функции форма подключится.
     * action       = false     - экшн для формы, если есть то отобразится в виде инпута.
     * id           = paginator - id формы.
     * name         = false     - name формы. 
     * 
     * @var array
     */
    public function formEnable($params=false){
        $this->_form['enable']=true;
        foreach($params as $k=>$v){
            $this->_form[$k]=$v;
        }    
    }    
    
    /**
     * Добавляет значение в массив $rowsInPageSelect
     * @param type $item 
     */
    public function addRowsInPageSelectItem($item)
    {
        if(!in_array($item, $this->rowsInPageSelect))
        {
            array_push($this->rowsInPageSelect, $item);
            sort($this->rowsInPageSelect);
        }
    }
    /**
     * Пейджинатор - генерация html.
     * @param int $count       = null - общее количество строк
     * @param int $page        = 1 - Номер выбранной стрницы
     * @param int $rowInPage   = 15 - Количество строк
     * @param string|false $functionJS  = false - Испоняемая функция JS при клике на пейджинатор 
     * @param int $buttonInPage
     * @return string html-пейджинатор 
     */
    public function draw($count, $page=1, $rowInPage=15, $functionJS=false, $buttonInPage=false){
        
        //Количество кнопок пейджинатора помимо next и prev
        $buttonInPage=$buttonInPage?$buttonInPage:Dante_Lib_Config::get('paginator.buttonInPage', 5);    
        //Если записей нет то пейджинатор не нужен
        if(!$count){
            return Module_Lang_Helper::translate('noRowsWithSelectedParams');
        }
        //Сгенерируем limit для внешнего использования
        $this->limit    = "LIMIT {$rowInPage} OFFSET ".((($page-1)*$rowInPage));
        //всего страниц
        $count_pages= ceil($count/$rowInPage);
        //если страниц меньше чем заказана страница то назначаем последнюю
        if($count_pages<$page){
            $page=$count;
        }
        $first_btn=($page-(floor($buttonInPage/2)));
        $last_btn=($page+(floor($buttonInPage/2)));
        if(($z=($last_btn-$count_pages))>0){
            $first_btn=$first_btn-$z;
        }
        if($first_btn<1){
            $first_btn=1;
        }
        if($buttonInPage>$count_pages){
            $cools=$count_pages;
        }else{
           $cools=$buttonInPage;
        }
        
        $vars=array();
        $vars['rows_in_page']   = $rowInPage;
        $vars['rows_in_page_select'] = $this->rowsInPageSelect;
        $vars['page']           = $page;
        $vars['prev_num']       = ($page==1)?1:($page-1); 
        $vars['next_num']       = ($page==$count_pages)?$count_pages:($page+1);;
        $vars['functionJS']     = $functionJS;
        $vars['form']           = $this->_form;
        $vars['link']           = $this->link;
        $vars['useDefaultInputs']= $this->useDefaultInputs;
        $vars['next_button']=array(
            'num'=>($page<$count_pages?1:$count_pages+1),
            'link'=>($page<$count_pages?($this->link.'/page'.($page+1)):"false")
        );
        $vars['prev_button']=array(
            'num'=>($page>1?$page-1:$page),
            'link'=>($page>1?($this->link.'/page'.($page-1)):"false")
        );
        foreach (range($first_btn, ($first_btn+$cools-1)) as $num){
            if($num==$page){
                $class="active";
                $link="false";
            }else{
                $class="";
                $link=$this->link.'/page'.$num;
            }
            
            $vars['rows'][]=array('num'=>$num, 'class'=>$class, 'link'=>$link);
        }
        if($this->link){
            //Клиенский вариант
            $tpl = new Lib_Paginator_View_Client();
        }else{
            $tpl = new Lib_Paginator_View_Manage();
        }
        $this->html=$tpl->draw($vars);
        return $this->html;        
    }
    
}
?>
