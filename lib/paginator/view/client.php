<?php
class Lib_Paginator_View_Client{
   public function draw($vars){
        $tpl = new Dante_Lib_Template();
        $tpl->_vars=$vars;
        $fileName = '../lib/paginator/template/client.html';
        
        $customTpl = Dante_Helper_App::getWsPath().'/paginator/client.html';
        if (file_exists($customTpl)) {
            $fileName = $customTpl;
        }
        
        return $tpl->draw($fileName);
    }
}