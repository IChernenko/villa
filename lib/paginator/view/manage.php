<?php
class Lib_Paginator_View_Manage
{
   public function draw($vars)
    {
        $tpl = new Dante_Lib_Template();
                
        $tpl->_vars=$vars;
        $fileName = '../lib/paginator/template/manage.html';
        if (file_exists('template/paginator/manage.html')) {
            $fileName = 'template/paginator/manage.html';
        }
        
        return $tpl->draw($fileName);
    }
}