<?php
global $package;
$package = array(
    'version' => '1',
    'name' => 'lib.paginator',
    'dependence' => array(
        'module.lang',
        'module.labels'
    ),
    'js' => array('../lib/paginator/js/paginator.js'),
    'css' => array('../lib/paginator/css/paginator.css?1')
);