<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of uploader
 *
 * @author Valkyria
 */
class Lib_JqueryUpload_Uploader 
{
    public $globalKey = 'Filedata';
    
    public function upload($uploadDir, $newFileName = false, $returnAllName=FALSE) 
    {        
        if(!$uploadDir) $uploadDir = Dante_Lib_Config::get('app.uploadFolder').'/'; //папка для хранения файлов
        Component_Vcl_Checkpath::check($uploadDir);

        $maxFileSize = Dante_Helper_System::convertBytes(ini_get('upload_max_filesize')); //1024 * 1024 * 1024; //1 MB
        
        //если получен файл
        if (!isset($_FILES)) 
            throw new Exception('Нет файлов для загрузки', 7002);
                    
        //проверяем размер и тип файла
        $path_info = pathinfo(strtolower($_FILES[$this->globalKey]['name']));
        if (isset($path_info['extension']))  $ext = $path_info['extension'];
                
        if ($maxFileSize < $_FILES[$this->globalKey]['size']) 
        {
            Dante_Lib_Log_Factory::getLogger()->debug('exception : Размер файла должен быть меньше '.$maxFileSize);
            throw new Exception('Размер файла должен быть меньше '.$maxFileSize, 7000);
        }

        $tmpFileName = $_FILES[$this->globalKey]['tmp_name'];
        if (!is_uploaded_file($tmpFileName)) 
            throw new Exception('Файл не загрузился на сервер ', 7003);    

        $fileName = $_FILES[$this->globalKey]['name'];
        $oldFileName = $fileName;
        if ($newFileName)
            $fileName = $newFileName.'.'.$ext;
        
        
        //если файл с таким именем уже существует...
        if (file_exists($fileName)) 
        {
            //...добавляем текущее время к имени файла
            $nameParts = explode('.', $_FILES[$this->globalKey]['name']);
            $nameParts[count($nameParts)-2] .= time();
            $fileName = $uploadDir.implode('.', $nameParts);
        }

        if (!move_uploaded_file($tmpFileName, $uploadDir.$fileName))
            throw new Exception('Не могу сохранить файл на сервере  ', 7001);
        
        
        if($returnAllName)
            return array($oldFileName, $fileName);                      
        else
            return $fileName;   
    }
}

?>
