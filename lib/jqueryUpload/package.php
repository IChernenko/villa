<?php

global $package;
$package = array(
    'version' => '1',
    'name' => 'lib.jqueryUpload',
    'dependence' => array(),
    'js' => array(
        "../lib/jqueryUpload/js/cors/jquery.postmessage-transport.js",
        "../lib/jqueryUpload/js/cors/jquery.xdr-transport.js",
        "../lib/jqueryUpload/js/vendor/jquery.ui.widget.js",
        "../lib/jqueryUpload/js/jquery.fileupload.js",
//        "../lib/jqueryUpload/js/jquery.fileupload-process.js",
//        "../lib/jqueryUpload/js/jquery.fileupload-validate.js",
//        "../lib/jqueryUpload/js/jquery.fileupload-image.js",
//        "../lib/jqueryUpload/js/jquery.fileupload-audio.js",
//        "../lib/jqueryUpload/js/jquery.fileupload-video.js",
//        "../lib/jqueryUpload/js/jquery.fileupload-ui.js",
//        "../lib/jqueryUpload/js/jquery.fileupload-jquery-ui.js",
//        "../lib/jqueryUpload/js/jquery.fileupload-angular.js",
        "../lib/jqueryUpload/js/jquery.iframe-transport.js",
//        "../lib/jqueryUpload/js/app.js",
        "../lib/jqueryUpload/js/main.js"
    ),
    'css' => array(
        "../lib/jqueryUpload/css/jquery.fileupload.css",
        "../lib/jqueryUpload/css/jquery.fileupload-ui.css",
        "../lib/jqueryUpload/css/progress-bar.css",
//        "../lib/jqueryUpload/css/jquery.fileupload-noscript.css",
//        "../lib/jqueryUpload/css/jquery.fileupload-ui-noscript.css",
    )
);

?>
