<?php
global $package;
$package = array(
    'version' => '1',
    'name' => 'lib.ajaxfileupload',
    'dependence' => array('lib.jquery.ui'),
    'js' => array(
        '../lib/ajaxfileupload/ajaxfileupload.js'
    ),
    'css' => array(
        '../lib/ajaxfileupload/ajaxfileupload.css'
    )
);