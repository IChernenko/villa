<?php

/**
 * модель параметров грида
 *
 * @author Mort
 */
class Lib_Jgrid_Model_Base {
    
    /**
     * записи грида
     * @var array 
     */
    public $rows;
    
    /**
     * кол-во записей
     * @var int 
     */
    public $records;
    
    /**
     * страница
     * @var int
     */
    public $page;
    
    /**
     * всего записей
     * @var int 
     */
    public $total;
    
}

?>
