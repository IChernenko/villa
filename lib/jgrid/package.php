<?php
global $package;
$package = array(
    'version' => '1',
    'name' => 'lib.jgrid',
    'dependence' => array(),
    'js' => array(
        '../lib/jgrid/js/jgridCustom.js',
        '../lib/jgrid/js/jquery.jqGrid.min.js',
        '../lib/jgrid/js/i18n/grid.locale-ru.js',
    ),
    'css' => array(
        '../lib/jgrid/css/ui.multiselect.css',
        '../lib/jgrid/css/ui.jqgrid.css',
        //'../lib/jgrid/css/jquery-ui-1.8.2.custom.css'
        //'../lib/jgrid/css/jquery-ui-black.custom.css'
    )
);