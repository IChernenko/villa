<?php

class Lib_Jgrid_Mapper_Base {
    
    /**
     * выборка из БД по стандартному набору параметров
     * @param string $tableName
     * @param array $params
     * @return array 
     */
    public function getRowsByParams($tableName, $params, $getRowCount=1)
    {
        $params['rows'] = (int)$params['rows'];
        if(isset($params['sidx']) && isset($params['sord'])){
            $order = 'ORDER BY '.$params['sidx'].' '.$params['sord'];
        }else{
            $order = 'order by id';
        }
        $limit = ' LIMIT '.$params['rows'].' OFFSET '.($params['page']-1)*$params['rows'];

        if(isset($params['_search']) && $params['_search']=='true'){
            $whereArr = array();
            foreach ($params['searchParams'] as $key => $value) {
                if($value!=''){
                    $whereArr[]= $key.' LIKE "%'.$value.'%"';
                }
            }
            if(count($whereArr)>0){
                $where = implode(" AND ", $whereArr);
            }else{
                $where = '1';
            }
        }else{
            $where = '1';
        }
        
        //searchHardParams
        if(isset($params['searchHardParams'])){
            $whereArr = array();
            foreach ($params['searchHardParams'] as $key => $value) {
                if($value!=''){
                    $where .= ' AND '.$key.' = "'.$value.'"';
                }
            }
        }
        
        //сперва определяем кол-во строк в общем
        if($getRowCount==1){
            $sql = "select
                        count(*)
                    from ".$tableName."
                    where ".$where;

            $r = Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
            $rowCount = $r['count(*)'];
        }else{
            $rowCount = 0;
        }
        
        //теперь выгребаем нужные строки
        $sql = "select
                    *
                from ".$tableName."
                where ".$where."
                ".$order.
                $limit;

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        
        $rowsArray = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            if(isset($f['id'])){
                $rowsArray[$f['id']] = $f;
            }else{
                $rowsArray[current($f)] = $f;
            }
        }
        
        return array(
            'rowCount' => $rowCount,
            'rows' => $rowsArray
        );
    }
    
    /**
     * выборка из БД по стандартному набору параметров
     * @param string $tableName
     * @param array $params
     * @return array 
     */
    public function getRowsArrayByParams($tableName, $params)
    {
        if(isset($params['sidx']) && isset($params['sord'])){
            $order = 'ORDER BY '.$params['sidx'].' '.$params['sord'];
        }else{
            $order = 'order by id';
        }
        $limit = ' LIMIT '.$params['rows'].' OFFSET '.($params['page']-1)*$params['rows'];

        if(isset($params['_search']) && $params['_search']=='true'){
            $whereArr = array();
            foreach ($params['searchParams'] as $key => $value) {
                if($value!=''){
                    $whereArr[]= $key.' LIKE "%'.$value.'%"';
                }
            }
            if(count($whereArr)>0){
                $where = implode(" AND ", $whereArr);
            }else{
                $where = '1';
            }
        }else{
            $where = '1';
        }
        
        
        //теперь выгребаем нужные строки
        $sql = "select
                    *
                from ".$tableName."
                where ".$where."
                ".$order.
                $limit;

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        
        $rowsArray = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $rowsArray[$f['id']] = $f;
        }
        
        return $rowsArray;
    }
    
    /**
     * выборка из БД по стандартному набору параметров
     * @param string $tableName
     * @param array $params
     * @return array 
     */
    public function getRowsCountByParams($tableName, $params)
    {
        if(isset($params['sidx']) && isset($params['sord'])){
            $order = 'ORDER BY '.$params['sidx'].' '.$params['sord'];
        }else{
            $order = 'order by id';
        }
        $limit = ' LIMIT '.$params['rows'].' OFFSET '.($params['page']-1)*$params['rows'];

        if(isset($params['_search']) && $params['_search']=='true'){
            $whereArr = array();
            foreach ($params['searchParams'] as $key => $value) {
                if($value!=''){
                    $whereArr[]= $key.' LIKE "%'.$value.'%"';
                }
            }
            if(count($whereArr)>0){
                $where = implode(" AND ", $whereArr);
            }else{
                $where = '1';
            }
        }else{
            $where = '1';
        }
        
        //сперва определяем кол-во строк в общем
            $sql = "select
                        count(*)
                    from ".$tableName."
                    where ".$where;

            $r = Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
            $rowCount = $r['count(*)'];
        
        return $rowCount;
    }
}

?>