<?php

/**
 * системная логика построителя грида
 *
 * @author Mort
 */
class Lib_Jgrid_Controller_Base extends Dante_Controller_Base{

    /**
     * параметры запросов грида, возможные параметра поиска
     * @var array
     */
    protected $_requestParams = array(
        'rows', //кол-во выбираемых записей
        'page', // номер страницы для отображения(начало с 1)
        'sidx', // ключ для сортировки, ORDER BY $sidx
        'sord', // ASC/DESC
        '_search', // true/false, в зависимости от этого идет выборка или не идет по searchParams
        'searchParams' => array(
            
        ) //массив параметров для поиска
    );
    
    /**
     * ф-йия для задания параметров запросов
     * @param array $params 
     */
    public function setRequestParams($params){
        foreach ($params as $key => $value) {
            if(!is_array($value)){
                $this->_requestParams[] = $value;
            }else{
                $this->_requestParams[$key] = $value;
            }
        }
    }
    
    /**
     * по возможным ключам, смотрим, а собственно чего нам пришло
     * @return array 
     */
    public function getRequestByParams(){
        $params = array();
        foreach ($this->_requestParams as $key => $value) {
            if(!is_array($value)){
                //обрабатываем строку
                $params[$value] = $this->_getRequest()->get($value);
            }else{
                //обрабатываем массив
                foreach ($value as $keyA => $valueA) {
                    $params[$key][$valueA] = $this->_getRequest()->get($valueA);
                }
            }
        }
        
        return $params;
    }
    
    /**
     * опциональные параметры для чекбоксов/радио
     * @var array
     */
    protected $_customInputsParams = array(
    );
    
    /**
     * Html для чекбоксов/радио
     * @var string 
     */
    protected $_customInputsHtml = "<script>
function MultiCheckElem{prefix}( value, options )
{
   var ctl = '';
   var ckboxAry = options.list.split(';');

   for ( var i in ckboxAry )
   {
      var item = ckboxAry[i];
      var ckboxAryDet = item.split(':');
      ctl += '<input type=\"{inputType}\" ';

      //if ( value.indexOf(item + '|') != -1 )
      if ( value.indexOf(ckboxAryDet[1]) != -1 )
         ctl += 'checked=\"checked\" ';
      ctl += 'value=\"' + ckboxAryDet[0] + '\" class=\"' + ckboxAryDet[1] + '\"> ' + ckboxAryDet[1] + '</input><br />&nbsp;';
   }

   ctl = ctl.replace( /<br \/>&nbsp;$/, '' );
   return ctl;
}

function MultiCheckVal{prefix}(elem, action, val)
{
/*alert('elem='+elem);
alert('action='+action);
alert('val='+val);*/

   var items = '';
   if (action == 'get')
   {
      for (var i in elem)
      {
         if (elem[i].tagName == 'INPUT' && elem[i].checked )
            items += elem[i].value + ',';
      }

      items = items.replace(/,$/, '');
   }
   else
   {
      for (var i in elem)
      {
         if (elem[i].tagName == 'INPUT')
         {
            //if (val.indexOf(elem[i].value + '|') == -1)
            if (val.indexOf(elem[i].className.replace(' customelement','')) == -1)
               elem[i].checked = false;
            else
               elem[i].checked = true;
         }
      }
   }

   return items;
}</script>";

    /**
     * опции грида
     * @var array
     */
    protected $_params = array(
        'datatype' => 'json',
        'mtype' => 'POST',
        'navId' => 'navgrid',
        'pagerId' => 'pagernav',
        'colNames' => "'№','Название'",
        'colModel' => array(
            'id'=> array(
                'name'=>'id',
                'index'=>'id', 
                'width'=>25,
                'editable'=>'false',
                'editoptions'=>array(
                        'readonly' => 'true',
                        'size' => 10
                        )
                ),
            'name'=> array(
                'name'=>'name',
                'index'=>'name', 
                'width'=>300,
                'editable'=>'true',
                'editoptions'=>array(
                        'size' => 25
                        )
                )
            ),
            'rowNum' => '10',
            'rowList' => array(
                    10,
                    20,
                    30,
                    40,
                    50
                ),
            'sortname' => 'id',
            'viewrecords' => 'true',
            'sortorder' => 'asc',
            'caption' => 'Категории',
            'url' => 'ajax.php',
            'editurl' => 'ajax.php',
            'height' => 300,
            'multiselect' => 'false',
            'multiboxonly' => 'false',
            'gridview' => 'true',
            'search' => 'false',
            'onSelectRow'=>'""',
            'gridComplete'=>'""',
            'navGridOptions' => array(
                    'edit' => 'true',
                    'add' => 'true',
                    'del' => 'true',
                    'search' => 'false',
                    'refresh' => 'true'
                ),
            'editOptionsA' => array(
                    'width' => '450',
                    'height' => '410',
                    'reloadAfterSubmit' => 'true',
                    'closeAfterEdit' => 'true'
                ),
            'addOptionsA' => array(
                        'width' => '450',
                        'height' => '410',
                        'reloadAfterSubmit' => 'true',
                        'closeAfterAdd' => 'true'
                    ),
            'delOptionsA' => array(
                        'reloadAfterSubmit' => 'false'
                    )
            
    );

    /**
     * html код грида
     * @var string
     */
    protected $_htmlCode = "
                <table id=\"{navId}\"></table>
                <div id=\"{pagerId}\"></div>
                <script>

                var mygrid = jQuery(\"#{navId}\").jqGrid({ 
                url:'{url}', 
                datatype: \"{datatype}\",
                mtype: '{mtype}',
colNames:[{colNames}], 
                colModel:[{colModel}], 
                rowNum:{rowNum}, 
                rowList:[{rowList}],
                pager: '#{pagerId}', 
                sortname: '{sortname}', 
                viewrecords: {viewrecords}, 
                sortorder: \"{sortorder}\", 
                caption:\"{caption}\", 
                editurl:\"{editurl}\", 
                height:{height},
                viewsortcols: [true, 'vertical', true],
                multiselect: {multiselect},
                multiboxonly: {multiboxonly},
                gridview:{gridview},
                search:{search},
                onSelectRow:{onSelectRow},
                gridComplete:{gridComplete}
            })
            .navGrid('#{pagerId}',{navGridOptions},{editOptionsA},{addOptionsA},{delOptionsA})
            .navButtonAdd(\"#{pagerId}\",{caption:\"\",title:\"Поиск\", buttonicon :'ui-icon-search',
                    onClickButton:function(){
                            mygrid[0].toggleToolbar()
                    } 
            })
            ;

            jQuery(\"#{navId}\").jqGrid('navGrid','#{pagerId}', 
                {}, //options 
                {editOptionsA}, // edit options
                {addOptionsA}, // add options
                {delOptionsA}, // del options
                {} // search options
            );
            jQuery(\"#{navId}\").jqGrid('filterToolbar', { searchOnEnter: true, enableClear: false });
        
            mygrid[0].toggleToolbar();
        </script>";
    
    /**
     * настраиваем опции грида
     * @param array $params 
     */
    public function setParams($params){
        foreach ($params as $key => $value) {
            $this->_params[$key] = $value;
        }
    }
    
    /**
     * настраиваем опции чекбоксов/радио
     * @param array $params 
     */
    public function addCustomInputsParams($params){
        foreach ($params as $key => $value) {
            $this->_customInputsParams[$key] = $value;
        }
    }
    
    /**
     * формируем HTML на выдачу
     * @return string 
     */
    public function getHTML(){
        // сперва яваскрипт для нестандартных инпутов
        $customInputsHtml = '';
        foreach ($this->_customInputsParams as $key => $value) {
            $customInputsHtmlTemp = $this->_customInputsHtml;
            $customInputsHtmlTemp = str_replace('{prefix}', $value['prefix'], $customInputsHtmlTemp);
            $customInputsHtmlTemp = str_replace('{inputType}', $value['inputType'], $customInputsHtmlTemp);
            $customInputsHtml .= $customInputsHtmlTemp;
        }
        
        $html = $this->_htmlCode;
        
        foreach ($this->_params as $key => $value) {
            if(is_string($value)){
                $html = str_replace('{'.$key.'}', $value, $html);
            }
            if(is_array($value)){
                $replace = $this->formArray($value);
                $html = str_replace('{'.$key.'}', $replace, $html);
            }
        }
        return $customInputsHtml.$html;
    }
    
    /**
     * собираем массив в строку
     * @param array $array
     * @return string 
     */
    protected function formArray($array){
        $string = '';
        $i=0;
        $arrayType = 'numbered';
        foreach ($array as $key => $value) {
            if(is_array($value)){
                $value = $this->formArray($value);
            }
            
            if($i==0){
                $separator = '';
            }else{
                $separator = ',';
            }
            $i++;
            
            if(!is_string($key)){$key='';}else{$key.=':'; $arrayType = 'associative';}
            
            $string.= $separator.$key.$value.'
                ';
        }
        if($arrayType == 'associative') $string = '{'.$string.'}';
        
        return $string;
    }
}


/*
You can do this using a custom edit rule

This is the example in the documentation

function mypricecheckforvalue(value, colname) {
if (value < 0 || value >20) 
   return [false,"Please enter value between 0 and 20"];
else 
   return [true,""];
}
jQuery("#grid_id").jqGrid({
...
   colModel: [ 
      ... 
      {name:'price', ..., editrules:{custom:true, custom_func:mypricecheckforvalue....}, editable:true },
      ...
   ]
...
});



*/

?>