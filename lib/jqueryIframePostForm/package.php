<?php
global $package;
$package = array(
    'version' => '1',
    'name' => 'lib.jqueryIframePostForm',
    'dependence' => array('lib.jquery.ui'),
    'js' => array(
        '../lib/jqueryIframePostForm/jquery.iframe-post-form.js'
    )
);