﻿<?php

define('FS_DELIMITER', '/');

class Lib_Packer_Zip
{

    /**
     * function unpack_path
     * распаковывает архив в папку
     * @param <type> $zipname
     * @param <type> $unpack_path
     */
    public static function unpack_to($zipname,$unpack_path)
    {
        $zip = new ZipArchive($zipname, ZIPARCHIVE::OVERWRITE);
        $zip->open($zipname);
        $zip ->extractTo($unpack_path);
        $zip->close();
	}

    /**
     * function pack_dir_rec
     * запаковывает директорию в архив, работая рекурсивно
     * @param <type> $dirname
     * @param <type> $to
     * @param <type> $zip
     */
    public static function pack_dir_rec($dirname, $to, $zip)
    {
        $directory = dir($dirname);
        while (false !== ($entry = $directory->read())) 
        {
            if($entry == '.' || $entry == '..' || $entry == '.svn')
                continue;

            if(is_file($dirname . FS_DELIMITER . $entry))
                $zip->addFile($dirname . FS_DELIMITER . $entry, $to . FS_DELIMITER . $entry);

            if(is_dir($dirname . FS_DELIMITER .$entry))
            {
                $zip->addEmptyDir($to . FS_DELIMITER . $entry);
                self::pack_dir_rec($dirname . FS_DELIMITER .$entry, $to . FS_DELIMITER . $entry, $zip);
            }
        }
    }

    /**
     * function pack_dir
     * запаковывает директорию в zip-архив, используя pack_dir_rec
     *
     * @param String $dirname
     * @param String $zipname
     */
    public static function pack_dir($dirname, $zipname = null,$def_folder = null)
    {
        
        $zip = new ZipArchive();
        
        $name = is_null($zipname)? $dirname . '.zip':$zipname;
        if (is_file($name))
        {
            echo 'фаил "' . $name . ' существует! Не могу записать архив.';
            die;
        }
        
        if($zip -> open($name, ZIPARCHIVE::CREATE))  {    
            self::pack_dir_rec($dirname, is_null($def_folder)? '':$def_folder, $zip);
            return $zip->close();
        }
        return false;
    }
        /**
     * function unpack_path
     * Запаковывает список файлов
     * @param <type> $zipname
     * @param <type> $unpack_path
     */
    public static function pack_files_to($zipname, $files_array)
    {
        $zip = new ZipArchive($zipname);
        $zip->open($zipname, ZIPARCHIVE::CREATE);
        foreach($files_array as $filename =>$localname)
        {
            $zip ->  addFile($filename, $localname);
        }
        $zip->close();
	}

}
?>