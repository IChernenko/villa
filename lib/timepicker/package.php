<?php
global $package;
$package = array(
    'version' => '1',
    'name' => 'lib.timepicker',
    'dependence' => array('lib.jquery.ui'),
    'js' => array(
        '../lib/timepicker/jquery-ui-timepicker-addon.js'
    ),
    'css' => array(
        '../lib/timepicker/jquery-ui-timepicker-addon.css'
    )
);
?>