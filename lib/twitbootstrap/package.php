<?php
global $package;
$bootstripeVersion   = '2.2.2';
$package = array(
    'version' => '0',
    'name' => 'lib.twitbootstrap',
    'dependence' => array(),
    'js' => array("../lib/twitbootstrap/{$bootstripeVersion}/js/bootstrap.min.js"),
    'css' => array(
        "../lib/twitbootstrap/{$bootstripeVersion}/css/bootstrap.css",
        "../lib/twitbootstrap/{$bootstripeVersion}/css/bootstrap-responsive.min.css",
    )
);