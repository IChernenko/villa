<?php
global $package;
$package = array(
    'version' => '1',
    'name' => 'lib.prettyPhoto',
    'dependence' => array(),
    'js' => array(
        '../lib/prettyPhoto/js/jquery.prettyPhoto.js'
    ),
    'css' => array(
        '../lib/prettyPhoto/css/prettyPhoto.css'
    )
);