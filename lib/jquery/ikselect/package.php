<?php
global $package;
$package = array(
    'version' => '1',
    'name' => 'lib.jquery.ikselect',
    'description' => 'Библиотека для нестандартных <select>',
    'url' => 'http://igor10k.github.com/ikSelect/',
    'dependence' => array(),
    'js' => array('../lib/jquery/ikselect/js/jquery.ikselect.js'),
    'css' => array('../lib/jquery/ikselect/css/ikselect.css')
);