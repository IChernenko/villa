<?php

global $package;
$package = array(
    'version' => '0',
    'name' => 'lib.jquery.carousel',
    'description' => 'карусель',
    'dependence' => array(),
    'js' => array('../lib/jquery/carousel/js/jquery.carousel.js'),
    'css' => array('../lib/jquery/carousel/css/carousel.css')
);

?>