Карусель
==============

Простой слайдер для списка картинок с двумя кнопками для пролистывания.
Легко подключается и настраивается. Не требует специфической верстки.

Подключение
------------

Подключаем библиотеку jQuery и скрипт, содержащий плагин.

```html
<script type="text/javascript" src="../js/jquery-1.8.3.min.js"></script>	
<script type="text/javascript" src="../js/jquery.carousel.js"></script>
```

В HTML делаем список с картинками(хотя картинки далеко не обязательны).

```html
<ul id="test" style="list-style: none;">
    <li style="background: red;"></li>
    <li style="background: yellow;"></li>
    <li style="background: green;"></li>
    <li style="background: red;"></li>
    <li style="background: yellow;"></li>
    <li style="background: green;"></li>
    <li style="background: red;"></li>
    <li style="background: yellow;"></li>
    <li style="background: green;"></li>
</ul>
```

В CSS фиксируем ширину и высоту элементов списка, задаем отступы.

```css
#test li
{
    height: 50px;
    margin: 5px;
    padding: 5px;
    width: 50px;
}
```

ВАЖНО! Если Вы используете less, стили для списка нужно указать явно 
(через класс или id списка), нельзя пользоваться "вложенными стилями", иначе 
расчеты могут быть неправильными и ничего работать не будет.

И теперь, собственно, вешаем плагин на наш список.

```html
<script>
$(function() {
    $("#test").carousel();
});
</script>
```

Настройка
-----------

Ну, и конечно, настройки. В примере указаны стандартные настройки плагина.

```javascript
$("#test").carousel({
    elementsVisible: 5, //количество видимых элементов
    animationSpeed: 500, //скорость прокрутки
    buttonsHide: true, //определяет, будут ли скрываться кнопки при достижении конца или начала списка
    btnClass: 'carousel-btn' //пользовательский класс для стилизации кнопок
});
```