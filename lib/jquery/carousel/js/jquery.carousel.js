/**
 * @author Valkyria
 */

(function($){
    $.fn.carousel = function(options)
    {
        options = $.extend({
            elementsVisible: 5, //количество видимых элементов
            animationSpeed: 500, //скорость прокрутки
            buttonsHide: true, //определяет, будут ли скрываться кнопки при достижении конца или начала списка
            btnClass: 'carousel-btn' //пользовательский класс для стилизации кнопок
        }, options);
        var init = function()
        {
            var tempLi = false;
            // Html 
            var wrapHtml = $('<div></div>', {'class':'carousel-wrap'});
            var btnLeftHtml = $('<button></button>', {'class': options.btnClass+' btn-left'});
            var btnRightHtml = $('<button></button>', {'class': options.btnClass+' btn-right'});
            var btnWrapHtml = $('<div></div>', {'class':'carousel-btn-wrap'});
            // end
            
            var elems = $(this).children('li');
            //Если список пустой, добавляем временный элемент
            if(!elems.length) 
            {
                $(this).append('<li></li>');
                elems = $(this).children('li');
                tempLi = true;
            }
            var elemWidth = $(elems).first().outerWidth(true);
            var totalWidth = elems.length*elemWidth;
            
            $(elems).css('float', 'left');
            $(this).wrap(wrapHtml);
            var wrap = $(this).parent();     // ссылка на блок-обертку
            wrap.css({
                'float':'left', 
                'overflow':'hidden',
                'height':$(elems).first().outerHeight(true),
                'width':options.elementsVisible*elemWidth
            });            
            $(this).css({
                'float':'left',
                'width':totalWidth
            });
            var max = $(this).width() - $(wrap).width();
            
            wrap.before(btnLeftHtml);
            var btnLeft = wrap.prev('button');  //ссылка на левую кнопку
            if(options.buttonsHide) btnLeft.css('display', 'none');
            btnLeft.addClass('disabled');
                
            wrap.after(btnRightHtml);
            var btnRight = wrap.next('button'); //ссылка на правую кнопку
            if(elems.length <= options.elementsVisible)
            {
                if(options.buttonsHide) btnRight.css('display', 'none');
                btnRight.addClass('disabled');
            }
            
            if(options.buttonsHide)
            {
                btnRight.wrap(btnWrapHtml);
                btnLeft.wrap(btnWrapHtml);
            }
            
            //Убираем временный элемент
            if(tempLi)
            {
                $(this).empty().width(0);
            }
            
            var list = this;
            btnLeft.click(function(){
                max = $(list).width() - $(wrap).width();
                var value = wrap.scrollLeft();
                var check = value/elemWidth - Math.floor(value/elemWidth);
                if(value > 0)
                {            
                    if(check != 0) value -= value%elemWidth;
                    var newValue = value - elemWidth;
                    wrap.animate({scrollLeft: newValue}, options.animationSpeed);
                    if(newValue == (max-elemWidth))
                    {
                        btnRight.removeClass('disabled');
                        if(options.buttonsHide) btnRight.fadeIn(300);
                    }
                    if(newValue == 0)
                    {
                        btnLeft.addClass('disabled');
                        if(options.buttonsHide) btnLeft.fadeOut(300);
                    }                    
                }
            });
            
            btnRight.click(function(){ 
                max = $(list).width() - $(wrap).width();            
                var value = wrap.scrollLeft();
                var check = value/elemWidth - Math.floor(value/elemWidth);
                if(value < max)
                {   
                    if(check != 0) value += elemWidth-(value%elemWidth);
                    var newValue = value + elemWidth;
                    wrap.animate({scrollLeft: newValue}, options.animationSpeed);
                    if(newValue == elemWidth)
                    {
                        btnLeft.removeClass('disabled');
                        if(options.buttonsHide) btnLeft.fadeIn(300);
                    }
                    if(newValue == max)
                    {
                        btnRight.addClass('disabled');
                        if(options.buttonsHide) btnRight.fadeOut(300);
                    }
                }  
            });
        }
        this.each(init);
    };
})(jQuery);

