<?php

global $package;
$package = array(
    'version' => '0',
    'name' => 'lib.jquery.colorpicker',
    'dependence' => array(),
    'js' => array('../lib/jquery/colorpicker/js/colorpicker.js'),
    'css' => array('../lib/jquery/colorpicker/css/colorpicker.css')
);

?>