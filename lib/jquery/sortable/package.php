<?php
global $package;
$package = array(
    'version' => '0',
    'name' => 'lib.jquery.sortable',
    'description' => 'менеджер сортировки в древовидных массивах',
    'url' => 'https://github.com/mjsarfatti/nestedSortable , http://mjsarfatti.com/sandbox/nestedSortable/',
    'dependence' => array(
        'lib.jquery.ui'
    ),
    'js' => array(
        '../villa/template/villa/assets/js/jquery.mjs.nestedSortable.js?1',
        '../lib/jquery/sortable/js/cookies.js?1'
        ),
    'css'=> array('../lib/jquery/sortable/css/sortable.css'),
);
?>