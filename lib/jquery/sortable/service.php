<?
/**
 * Сервисы для менеджера сортировок
 * @author Elrafir
 */
class Lib_Jquery_Sortable_Service{
    /**
     * Формирует древовидный массив
     * @param array $array миссив сортируемых данных в формате array(array('id'=>2, 'parent_id'=>1, 'content'=>'text'));
     * @param int $parentId
     * @param string $parentIdColumnName ключ родителя в массиве
     * @param string $cildrenIdColumnName ключ потомка в массиве
     * @return array
     */
    public static function treeGenerator($array, $parentId , $parentIdColumnName = 'parent_id', $childrenIdColumnName = 'id'){
        $tree=false;
        foreach((array)$array as $row){
            if($row[$parentIdColumnName] != $parentId || $row[$parentIdColumnName]==$row[$childrenIdColumnName])continue;
            $tree[$row[$childrenIdColumnName]] = $row;
            //добавляем контент
            $tree[$row[$childrenIdColumnName]]['content'] = $row['content'];
            //добавляем дочерние записи
            $tree[$row[$childrenIdColumnName]]['childrens'] = self::treeGenerator($array, $row[$childrenIdColumnName], $parentIdColumnName, $childrenIdColumnName);
        }        
        return $tree;
    }
    
    public static function treeHtml($tree, $useJS=true){
        
        $html = "<ol class='".($useJS?'sortable dragAndDrop':'')."'>";
        foreach((array)$tree as $id=>$division){
            $class = $division['is_active']?'active':'noActive';
            $html.= "<li id='list_{$id}' class='{$class}' ><div ><span class='disclose'><span></span></span>";
            $html.= $division["content"]; 
            $html.= "</div>";
            if($division['childrens'])
                $html.= self::treeHtml($division['childrens'], false);
            $html.= "</li>";
        }
        $html.='</ol>';
        //Добавляем JS
        if($useJS){
        $html.="
            <script>
            $('.sortable').nestedSortable({
			forcePlaceholderSize: true,
			handle: 'div',
			helper:	'clone',
			items: 'li',
			opacity: .6,
			placeholder: 'placeholder',
			revert: 250,
			tabSize: 25,
			tolerance: 'pointer',
			toleranceElement: '> div',
			isTree: true ,
			expandOnHover: 700,
			startCollapsed: true                        
		});
                $('.disclose').click(function() {
                    obj = $(this).closest('li');
                    rowId = obj.attr('id')
                    if(obj.hasClass('mjs-nestedSortable-collapsed')){
                        cookies.setCookie('d'+rowId, 1);
                        obj.removeClass('mjs-nestedSortable-collapsed').addClass('mjs-nestedSortable-expanded');
                    }else{ 
                        cookies.setCookie('d'+rowId, '', 1);
                        obj.removeClass('mjs-nestedSortable-expanded').addClass('mjs-nestedSortable-collapsed');
                    }   
                    //$(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
		})
            </script>
            
            ";
        }
        return $html;
    }
}