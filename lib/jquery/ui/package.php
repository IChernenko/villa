<?php
global $package;
$package = array(
    'version' => '1',
    'name' => 'lib.jquery.ui',
    'dependence' => array(),
    'js' => array('../villa/template/villa/assets/js/jquery-ui.min.js'),
    'css'=> array(
        
        //'../lib/jquery/ui/css/smoothness/jquery-ui-1.8.2.custom.css',
        '../lib/jquery/ui/css/ui-lightness/jquery-ui-1.9.2.custom.min.css',
        //'../lib/jquery/ui/css/smoothness/jquery-ui-1.8.24.custom.css',
        ),
);
?>