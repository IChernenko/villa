<?php
global $package;
$package = array(
    'version' => '1',
    'name' => 'lib.jquery.validate',
    'description' => 'библиотека для работы валидации данных на форме',
    'url' => 'http://bassistance.de/jquery-plugins/jquery-plugin-validation/',
    'dependence' => array(
        'lib.jquery.form'
    ),
    'js' => array('../lib/jquery/validate/js/jquery.validate.min.js')
);