<?php

global $package;
$package = array(
    'version' => '0',
    'name' => 'lib.jquery.autocomplete',
    'dependence' => array(),
    'js' => array('../lib/jquery/autocomplete/js/jquery.autocomplete.js'),
    'css' => array('../lib/jquery/autocomplete/css/jquery.autocomplete.css')
);

?>