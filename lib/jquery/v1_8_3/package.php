<?php
global $package;
$package = array(
    'version' => '1',
    'name' => 'lib.jquery.v1_8_3',
    'dependence' => array(),
    'js' => array(
        '../lib/jquery/v1_8_3/jquery-1.8.3.min.js'
    )
);