<?php
global $package;
$package = array(
    'version' => '1',
    'name' => 'lib.jquery.fancytransitions',
    'dependence' => array(),
    'js' => array('../lib/jquery/fancytransitions/js/jqFancyTransitions.1.8.min.js'),
    'description' => 'Библиотека для организации слайдшоу',
    'url' => 'http://workshop.rs/2009/12/image-gallery-with-fancy-transitions-effects/'
);