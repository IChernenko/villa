<?php
global $package;
$package = array(
    'version' => '1',
    'name' => 'lib.jquery.v1_8_3',
    'dependence' => array(),
    'js' => array(
        '../lib/jquery/v2_1_0/jquery.js',
        '../lib/jquery/v2_1_0/jquery-migrate-1.4.1.min.js'
    )
);