<?php
global $package;
$package = array(
    'version' => '1',
    'name' => 'lib.jquery.elrte',
    'description' => '
        elRTE - это свободный WYSIWYG редактор для сайтов и систем управления контентом (CMS), написанный на JavaScript с использованием jQuery UI.
        Вы можете использовать его в любых коммерческих и некоммерческих проектах.
        Основная цель редактора - максимально упростить работу с текстом и разметкой (HTML) на сайтах, блогах, форумах и прочих online сервисах.',
    'url' => 'http://elrte.org/ru/',
    'dependence' => array(
        'lib.jquery.ui',
        'lib.elfinder',
        
    ),
    'js' => array(
        '../lib/jquery/elrte/js/elrte.min.js',
        '../lib/jquery/elrte/js/i18n/elrte.ru.js',        
    ),
    'css'=> array(
        '../lib/jquery/elrte/css/elrte.min.css',        
    )
);