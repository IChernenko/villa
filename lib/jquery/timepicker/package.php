<?php
global $package;
$package = array(
    'version' => '0',
    'name' => 'lib.jquery.timepicker',
    'dependence' => array(
        'lib.jquery.ui',
    ),
    'js' => array('../lib/jquery/timepicker/js/jquery.ui.timepicker.js'),
    'css' => array('../lib/jquery/timepicker/css/jquery.ui.timepicker.css'),
);
?>