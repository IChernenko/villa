<?php
class Lib_Jquery_Arcticmodal_Controller{
    
    public function draw($id='articModal'){
        $tpl= new Lib_Jquery_Arcticmodal_View();
        $vars['id']=$id;
        return $tpl->draw($vars);
    }
}