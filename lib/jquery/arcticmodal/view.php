<?php
class Lib_Jquery_Arcticmodal_View{
    public function draw($vars=false){
        $tpl = new Dante_Lib_Template();
        $tpl->_vars=$vars;
        $fileName = '../lib/jquery/arcticmodal/form.html';
        if (file_exists('template/default/jquery/arcticmodal/form.html')) {
            $fileName = 'template/default/jquery/arcticmodal/form.html';
        }
        return $tpl->draw($fileName);       
    }
}
?>
