<?php
global $package;
$package = array(
    'version' => '1',
    'name' => 'lib.jquery.arcticmodal',
    'description' => 'библиотека для работы с модальными окнами',
    'url' => 'http://arcticlab.ru/arcticmodal/',
    'dependence' => array(),
    'js' => array('../lib/jquery/arcticmodal/js/jquery.arcticmodal-0.2.min.js'),
    'css' => array('../lib/jquery/arcticmodal/css/jquery.arcticmodal-0.2.css')
);