<?php
global $package;
$package = array(
    'version' => '1',
    'name' => 'lib.jquery.webforms2',
    'description' => 'библиотека для работы валидации данных форм, с проверкой атрибутов стандарта webform 2',
    'url' => 'http://code.google.com/p/jquery-webforms-2/',
    'dependence' => array(
        'lib.jquery.ui',
        'lib.jquery.timepicker',        
    ),
    'js' => array('../lib/jquery/webform2/js/jquery.webforms2.js')
);