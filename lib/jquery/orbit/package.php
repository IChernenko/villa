<?php
global $package;
$package = array(
    'version' => '0',
    'name' => 'lib.jquery.orbit',
    'description' => 'слайдер',
    'dependence' => array(),
    'js' => array(
//        '../lib/jquery/orbit/js/jquery.orbit.js',
        '../lib/jquery/orbit/js/jquery.orbit.min.js',
    ),
    'css' => array(
        '../lib/jquery/orbit/css/orbit.css',
    )
);