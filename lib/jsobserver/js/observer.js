observer = {
    
    _events:{},
    _namedEvents:{},
    
    _fireEventInProgress: false,
    _fireEventsInProgress:[],
    _eventsQueue:[],
    
    addObserver: function(eventName, callback, name, global)
    {
        if(observer._fireEventInProgress){
            observer._eventsQueue.push([eventName, callback, name, global]);
            return false;
        }
        
        if(!global){
            var prefix = observer._getCurPrefix();
            eventName += prefix;
        }
        
        if(name){
            if(typeof(observer._namedEvents[eventName])=='undefined'){
                observer._namedEvents[eventName] = {};
            }
            
            observer._namedEvents[eventName][name] = callback;
        }else{
            if(typeof(observer._events[eventName])=='undefined'){
                observer._events[eventName] = [];
            }

            observer._events[eventName].push(callback);
        }
        
        return true;
    },
    
    removeObserver:function(eventName, callback, name, global)
    {
        var newQueue = [];
        for(var k in observer._eventsQueue)
        {
            var event = observer._eventsQueue[k];
            if(
                    eventName != event[0] 
                    || name != event[2] 
                    || global != event [3]
                    || callback.toString() != event[1].toString()
            ){
                    newQueue.push(event);
            }
        }
        observer._eventsQueue = newQueue;
        
        if(!global){
            var prefix = observer._getCurPrefix();
            eventName += prefix;
        }
        
        if(name){
            if(typeof(observer._namedEvents[eventName])=='undefined'){
                return false;
            }
            
            if(typeof(observer._namedEvents[eventName][name])=='undefined'){
                return false;
            }
            
            observer._namedEvents[eventName][name] = undefined;
            delete(observer._namedEvents[eventName][name]);
            
            if(lib.advanced.getObjSize(observer._namedEvents[eventName])==0)
                delete(observer._namedEvents[eventName]);
        }else{
            if(typeof(observer._events[eventName])=='undefined'){
                return false;
            }

            var index = observer._events[eventName].indexOf(callback);

            if(index != -1)
                observer._events[eventName].splice(index, 1);
            
            if(lib.advanced.getObjSize(observer._events[eventName])==0)
                delete(observer._events[eventName]);
        }
        
        return true;
    },
    
    clearObservers: function()
    {
        observer._events = {};
        observer._namedEvents = {};
        return true;
    },
            
    _getCurPrefix: function (){
        return '_default_';
    },
    
    fireEvent: function(eventName, params)
    {
        observer._fireEventInProgress = true;
        observer._fireEventsInProgress.push(eventName);
        var prefix = observer._getCurPrefix();
        
        if(typeof(observer._events[eventName])!='undefined'){
            for (var k in observer._events[eventName]) {
                var callback = observer._events[eventName][k];
                callback(params);
            }
        }
        
        if(typeof(observer._namedEvents[eventName])!='undefined'){
            for (var k in observer._namedEvents[eventName]) {
                var callback = observer._namedEvents[eventName][k];
                callback(params);
            }
        }
        
        //теперь то же самое для оконных событий
        eventName += prefix;
        if(typeof(observer._events[eventName])!='undefined'){
            for (var k in observer._events[eventName]) {
                var callback = observer._events[eventName][k];
                callback(params);
            }
        }
        
        if(typeof(observer._namedEvents[eventName])!='undefined'){
            for (var k in observer._namedEvents[eventName]) {
                var callback = observer._namedEvents[eventName][k];
                callback(params);
            }
        }
        
        observer._fireEventsInProgress.pop();
        if(observer._fireEventsInProgress.length == 0){
            observer._fireEventInProgress = false;
        
            //добавляем из очереди
            var queue = observer._eventsQueue;
            for(var k in queue){
                var q = queue[k];
                observer.addObserver(q[0], q[1], q[2], q[3]);
            }

            observer._eventsQueue = [];
        }
        
        return true;
    }
};