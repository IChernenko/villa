<?php
/**
 * Created by PhpStorm.
 * User: dorian
 * Date: 3/30/14
 * Time: 5:19 PM
 */

class Lib_Foundation_Helper {

    public static function drawAlert($message) {
        return '<div data-alert class="alert-box">'.$message.'<a href="#" class="close">&times;</a></div>';
    }

    public static function drawAlertError($message) {
        return '<div data-alert class="alert-box alert round">'.$message.'<a href="#" class="close">&times;</a></div>';
    }

    public static function drawAlertSuccess($message) {
        return '<div data-alert class="alert-box success">'.$message.'<a href="#" class="close">&times;</a></div>';
    }
} 