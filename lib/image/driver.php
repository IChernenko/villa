<?php

/**
 * Драйвер для работы с картинками
 */
class Lib_Image_Driver {
   
   protected $image = false;
   protected $image_type;
 
   /**
    * Загрузка картиинки
    * @param string $filename 
    */
   function load($filename) {
      $this->setType($filename);
      if( $this->image_type == IMAGETYPE_JPEG ) {
         $this->image = imagecreatefromjpeg($filename);
      } elseif( $this->image_type == IMAGETYPE_GIF ) {
         $this->image = imagecreatefromgif($filename);
      } elseif( $this->image_type == IMAGETYPE_PNG ) {
         $this->image = imagecreatefrompng($filename);
      }
      if (!$this->image) return false;
      return true;
   }
   function save($filename, $image_type=IMAGETYPE_JPEG, $compression=75, $permissions=null) {
      $ext = pathinfo($filename, PATHINFO_EXTENSION);
      Dante_Lib_Log_Factory::getLogger()->debug("ext:".$ext);

      if( $ext == 'jpg' || $ext == 'jpeg' ) {
         imagejpeg($this->image,$filename,$compression);
      } elseif( $ext == 'gif' ) {
         imagegif($this->image,$filename);         
      } 
      elseif( $ext == 'png' ) {
         imagealphablending($this->image, false);
         imagesavealpha($this->image, true);
         imagepng($this->image,$filename);
      }   
      if( $permissions != null) {
         chmod($filename,$permissions);
      }
   }
   function output($image_type=IMAGETYPE_JPEG) {
      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image);
      } elseif( $image_type == IMAGETYPE_GIF ) {
         imagegif($this->image);         
      } elseif( $image_type == IMAGETYPE_PNG ) {
         imagepng($this->image);
      }   
   }
   function getWidth() {
      return imagesx($this->image);
   }
   function getHeight() {
      return imagesy($this->image);
   }
   function resizeToHeight($height) {
      $ratio = $height / $this->getHeight();
      $width = $this->getWidth() * $ratio;
      $this->resize($width,$height);
   }
   function resizeToWidth($width) {
      $ratio = $width / $this->getWidth();
      $height = $this->getheight() * $ratio;
      $this->resize($width,$height);
   }
   function scale($scale) {
      $width = $this->getWidth() * $scale/100;
      $height = $this->getheight() * $scale/100; 
      $this->resize($width,$height);
   }
   function resize($width,$height) {
      $new_image = imagecreatetruecolor($width, $height);
      imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
      $this->image = $new_image;   
   } 
   function setType($filename)
   {
      //echo('getimagesize:'.$filename);
      $image_info = getimagesize($filename);
      $this->image_type = $image_info[2];
   }
   function getType()
   {
       return $this->image_type;
   }
}
?>