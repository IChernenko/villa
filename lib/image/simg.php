<?php
/**
 * Работы с файлами
 * @autor Elrafir
 */
class Lib_Image_Simg {
    public $imgName = false;
    public $imgDir	='media/gall_prev/';
    public $imgDirPrev	='media/gall_prev/prev/';
    public $imageTmp;
    public $image_type;
    public $prev_x = 150;
    public $prev_y = 100;
    
    public function upload($filename=FALSE){
        if(!$filename){
            //Добываем файлы своими руками из глобалки
           $name=$_FILES['Filedata']['name'];
           $tmp_name=$_FILES['Filedata']['tmp_name'];
           $this->load($tmp_name);
           $this->save($this->imgDir.$name,$this->image_type);
            
        }
    }

    public function load($filename) {
              
        
              $image_info = getimagesize($filename);
              
              $this->image_type = $image_info[2];
              ini_set('gd.jpeg_ignore_warning', 1);
              if( $this->image_type == IMAGETYPE_JPEG ) {
                     $this->imageTmp = imagecreatefromjpeg($filename);
              } elseif( $this->image_type == IMAGETYPE_GIF ) {
                     $this->imageTmp = imagecreatefromgif($filename);
              } elseif( $this->image_type == IMAGETYPE_PNG ) {
                     $this->imageTmp = imagecreatefrompng($filename);
              }else{
              }
    }
    public function save($filename, $image_type=IMAGETYPE_JPEG, $compression=75, $permissions=null) {
              if( $image_type == IMAGETYPE_JPEG ) {
                     imagejpeg($this->imageTmp,$filename,$compression);
              } elseif( $image_type == IMAGETYPE_GIF ) {
                     imagegif($this->imageTmp,$filename);         
              } elseif( $image_type == IMAGETYPE_PNG ) {
                     imagepng($this->imageTmp,$filename);
              }  
              if( $permissions != null) {
                     chmod($filename,$permissions);
              }
    }
    public function output($image_type=IMAGETYPE_JPEG) {
              if( $image_type == IMAGETYPE_JPEG ) {
                     imagejpeg($this->imageTmp);
              } elseif( $image_type == IMAGETYPE_GIF ) {
                     imagegif($this->imageTmp);         
              } elseif( $image_type == IMAGETYPE_PNG ) {
                     imagepng($this->imageTmp);
              }   
    }
    public function getWidth() {
            return imagesx($this->imageTmp);
    }
    public function getHeight() {
            return imagesy($this->imageTmp);
    }
    public function resizeToHeight($height) {
      $ratio = $height / $this->getHeight();
      $width = $this->getWidth() * $ratio;
      $this->resize($width,$height);
    }
    public function resizeToWidth($width) {
      $ratio = $width / $this->getWidth();
      $height = $this->getheight() * $ratio;
      $this->resize($width,$height);
    }
    public function scale($scale) {
      $width = $this->getWidth() * $scale/100;
      $height = $this->getheight() * $scale/100; 
      $this->resize($width,$height);
    }
    public function resize($width,$height) {
      $new_image = imagecreatetruecolor($width, $height);
      imagecopyresampled($new_image, $this->imageTmp, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
      $this->imageTmp = $new_image;   
    }
    public function crop($x1, $y1, $height, $width){
            $new_image = imagecreatetruecolor($width, $height);
                    imagecopyresampled($new_image, $this->imageTmp, 0, 0, $x1, $y1, $width, $height, $width, $height);
            $this->imageTmp = $new_image;  
    }
    public function get_prev($photos){
            foreach($photos as $photo){

                    if(!is_file($this->imgDirPrev.$photo['src'])){
                            //Провери есть ли вообще такое изображение
                            if(!is_file($this->imgDir.$photo['src'])){
                                    continue;
                            }
                            //Создаём превьюшку
                            $this->crop_from_prev($photo['src']);
                    }
            }
    }

    public function crop_from_prev($target , $x1=false, $y1=false){

            //echo "x1 = $x1, y1 = $y1";
            $target=basename($target);
            if($this->imgName){
                    $link=$this->imgName;
            }else{
                    $link=$target;
            }
            $this->imgName=false;
            $this->load($this->imgDir.$target);
            $w=$this->getWidth();
            $h=$this->getHeight();
            //Обрезаем файлик до нужного размера согласно правилам
            $zw=$w/$this->prev_x;
            $zh=$h/$this->prev_y;
            if($zw>$zh){
                    $newW=floor($h*($this->prev_x/$this->prev_y));
                    $newH=$h;
                    if(!isset($x1)){$x1=floor(($w-$newW)/2);}
            }else{
                    $newH=floor($w/($this->prev_x/$this->prev_y));
                    $newW=$w;
                    if(!isset($y1)){$y1=floor(($h-$newH)/2);}
            }
            $this->crop($x1, $y1, $newH, $newW);
            //Ресайзим резальтат полученный
            $this->resize($this->prev_x, $this->prev_y);
            //Сохраняем файлик в превью
            if(is_file($link)){
                    unlink($link);
            }
            $this->save($this->imgDirPrev.$link);

    }
}
?>