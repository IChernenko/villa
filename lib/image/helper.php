<?php



/**
 * Хелпер по работе с изображениями
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Lib_Image_Helper {
    
    /**
     * Изменение размера картинки
     * @param string $srcDir - Дирректория обрабатываемого файла
     * @param string $fileName - Имя файла
     * @param string $dstDir - Дирректория в которую сохранять файл
     * @param int $width - новая ширина
     * @param int $height - новая высота
     * @param string|false $newFileName - если заданно то файл будет сохранён с указанным в этом свойстве именем
     */
    public static function resize($srcDir, $fileName, $dstDir, $width, $height, $newName=false) {
        $image = new Lib_Image_Driver();
        if (!$image->load($srcDir.$fileName)) 
                throw new Exception("cant resize : $srcDir $fileName");
        
        $type = $image->getType();
        $widthTemp = $image->getWidth();
        $heightTemp = $image->getHeight();

        if($widthTemp > $width)  $image->resizeToWidth($width);
        elseif($heightTemp > $height) $image->resizeToHeight($height);
        
        //Dante_Lib_Log_Factory::getLogger()->debug("create folder: $dstPath");
        if (!is_dir($dstDir)) mkdir($dstDir, 0755, true);
        if($newName) $fileName = $newName;
        $image->save($dstDir.$fileName, $type);
    }    
    
    public static function getNoImage()
    {
        return '../dante/i/noimage.jpg';
    }
}

?>
