<?php



/**
 * Класс для работы с таблицами
 *
 * @author dorian
 */
class Dante_Lib_Orm_Table {
	
    /**
     * Имя таблицы
     * @var string
     */
    protected $_tableName;

    /**
     * Поля
     * @var array
     */
    protected $_fields = array();

    /**
     * Число записей
     * @var int
     */
    protected $_numRows;

    /**
     * @var Dante_Lib_Sql_Builder_Select
     */
    protected $_selectBuilder;

    protected $_sql = '';

    protected $_r;
    
    protected static $_instances = array();
    
    /**
     *
     * @var Dante_Lib_Sql_Metadata
     */
    protected $_metaData;
	
    function __construct($tableName = false, $tableAlias = false)
    {
        if ($tableName) $this->_tableName = $tableName;
        $this->_selectBuilder = new Dante_Lib_Sql_Builder_Select($this->_tableName, $tableAlias);
        
        // загрузим метаданные
        $this->_metaData = Dante_Lib_SQL_DB::get_instance()->getMetaData($this->_tableName);
    }
    
    /**
     * Получение мета-данных. 
     * @return Dante_Lib_Sql_Metadata 
     */
    public function getMetaData() {
        return $this->_metaData;
    }
	
    function __set($fieldName, $fieldValue) {
        $this->_fields[$fieldName] = $fieldValue;
    }
	
    function __get($name) {
        return (isset($this->_fields[$name])) ? $this->_fields[$name] : null;
    }
    
    /**
     *
     * @return Dante_Lib_Orm_Table 
     */
    final public static function getInstance(){ 
        $class = self::getClass(); 
        if (!isset(self::$_instances[$class])) {
            
            self::$_instances[$class] = new $class();
        }
        
        return self::$_instances[$class];
    } 
    
    final public static function getClass(){ 
        return get_called_class(); 
    } 
    
    public function getFields() {
        return $this->_fields;
    }

    /**
     * @return Dante_Lib_Sql_Builder_Select
     */
    public function get() {
        $this->_selectBuilder->select($this->_fields);
        return $this->_selectBuilder;
    }

    public function getByAttributes($condition) {
        $this->_selectBuilder->where($condition);
        return $this;
    }
    
    /**
     *
     * @param type $condition
     * @return Dante_Lib_Orm_Table 
     */
    public function getByAttribute($condition) {
        $this->_selectBuilder->addCondition($condition);
        return $this;
    }
    
    /**
     * @return Dante_Lib_Sql_Builder_Select
     */
    public function count() {
        $this->_selectBuilder->count();
        return $this->_selectBuilder;
    }
    
    public function loadBy($condition) {
        return $this->getByAttributes($condition)->fetch();
    }

    public function orderByAsc($fieldName) {
        $this->_selectBuilder->orderby($fieldName.' ASC');
    }

    public function fetch() {
        $sql = (string)$this->_selectBuilder;
        //echo($sql);
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        if ($r) {
            $this->_fields = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r);
            $this->_numRows = Dante_Lib_SQL_DB::get_instance()->getNumRows($r);
        }
        /*echo('<pre>');
        print_r($this);
        echo('</pre>');*/
        return $this; //->_numRows;
    }

    public function open() {
        $this->_sql = (string)$this->_selectBuilder;
        //echo($this->_sql);
        $this->_r = Dante_Lib_SQL_DB::get_instance()->open($this->_sql);
        return $this;
    }
    
    public function getSql() {
        return $this->_sql;
    }

    public function fetchAll($keyName) {
        $this->open();
        $list = array();
        while($this->fetchRecord()) {
            $list[$this->$keyName] = $this->_fields;
        }
        return $list;
    }
    
    /**
     * Фетчит одну запись согласно текущему указателю
     * @return type 
     */
    public function fetchRecord() {
        $this->_fields = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($this->_r);
        return $this->_fields;
    }

    public function debug() {
        return (string)$this->_selectBuilder;
    }
    /**
     * Возвращает массив одной строки таблицы по заданным условиям. Если строк несколько то возвращена будет первая.
     * @param array $cond
     * @return \Dante_Lib_Orm_Table
     * @throws Exception
     */
    public function select($cond, $order=false, $limit=false)
    {
        $sql = Dante_Lib_Sql_Builder_Select::build($this->_tableName, '*', $cond, $order, $limit);
        $this->_sql = $sql;
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        if (!$r) {
                throw new Exception('cant open sql '.$sql);
        }
        $this->_fields = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r);
        
        $this->_numRows = Dante_Lib_SQL_DB::get_instance()->getNumRows($r);

        return $this;
    }
    /**
     * Возвращает массив нескольких строк таблицы по заданным условиям.
     * @param array $cond
     * @return boolean|\Dante_Lib_Orm_Table
     */
    public function select_array($cond, $order=false, $limit=false)
    {
        $sql = Dante_Lib_Sql_Builder_Select::build($this->_tableName, '*', $cond, $order, $limit);
        $this->_sql = $sql;
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        if (!$r) {
                return FALSE;
        }
        $result=array();
        while($cur=Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
            $result[]=$cur;
        
        $this->_fields = $result;
        $this->_numRows = Dante_Lib_SQL_DB::get_instance()->getNumRows($r);

        return $this;
    }
        
    public function getNumRows()
    {
        return $this->_numRows;
    }

    public function exist($cond)
    {
            $sql = Dante_Lib_Sql_Builder_Select::build($this->_tableName, '*', $cond);
            $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
            if(!$r) return false;
            return (bool)Dante_Lib_SQL_DB::get_instance()->getNumRows($r);
    }	

    public function apply($cond = false)
    {
        $primaryKey = $this->getMetaData()->getPrimaryKey();
        if (!$cond) {
            if ($this->$primaryKey > 0) {
                $cond = array($primaryKey => $this->$primaryKey);
            }
        }
        
        if ($cond && $this->exist($cond)) {
            return $this->update($cond);
        }
        
        $this->$primaryKey = $this->insert();
                
        return $this->$primaryKey;
    }
	
    public function insert()
    {
        //Dante_Lib_Log_Factory::getLogger()->debug("orm intsert fields : ".var_export($this->_fields, true));
        $this->_sql = Dante_Lib_Sql_Builder_Insert::build($this->_tableName, $this->_fields);
        //Dante_Lib_Log_Factory::getLogger()->debug('orm insert : '.$this->_sql);
        //echo($this->_sql.'<br/>'); die();
        if (!Dante_Lib_SQL_DB::get_instance()->exec($this->_sql)) {
            return false;
        }
        return Dante_Lib_SQL_DB::get_instance()->get_last_insert_id();
    }

    public function update($cond)
    {
        Dante_Lib_Log_Factory::getLogger()->debug("orm update fields : ".var_export($this->_fields, true));
        $sql = Dante_Lib_Sql_Builder_Update::build($this->_tableName, $this->_fields, $cond);
        //echo $sql; die();
        return Dante_Lib_SQL_DB::get_instance()->exec($sql);
    }

    public function delete($cond=array())
    {
            $sql = Dante_Lib_Sql_Builder_Delete::build($this->_tableName, $cond);
            return Dante_Lib_SQL_DB::get_instance()->exec($sql);
    }
        
    public function fieldReset($fieldName)
	{
            unset($this->_fields[$fieldName]);
	}

    function __toString() {
        return $this->_sql;
    }
}

?>
