<?php
/**
 * Базовый класс для работы с dmo
 * User: dorian
 * Date: 12/15/13
 * Time: 2:52 PM
 * To change this template use File | Settings | File Templates.
 */

class Dante_Lib_Orm_Dmo extends Dante_Lib_Orm_Table{

    protected $_tableName = 'redeclare me';

    /**
     *
     * @return Module_Division_Dmo
     */
    public static function dmo() {
        return self::getInstance();
    }


    public function byId($id) {
        return $this->getByAttributes(array('id'=>$id));
    }

    /**
     * Получение радела по имени
     * @param string $name
     * @return $this
     */
    public function byName($name) {
        return $this->getByAttributes(array('name'=>$name));
    }
}