<?php
/**
 * Работа с пакетами
 * User: dorian
 * Date: 20.03.12
 * Time: 14:08
 * To change this template use File | Settings | File Templates.
 */
class Dante_Lib_Package
{
    /**
     * @var Dante_Lib_Db_Migrate
     */
    protected $_migration;

    /**
     * определяет можно ли накатывать обновления пакетов
     * @var bool
     */
    protected $_allowUpdate = true;

    /**
     * Пакеты, которые уже обработаны
     * @var array
     */
    protected $_includedPackages = array();

    function  __construct() {
        $this->_migration = new Dante_Lib_Db_Migrate();
    }

    public static function getPackageFolder($packageName) {
        return str_replace('.', '/', $packageName);
    }

    /**
     * Инклудит файл с описанием пакета и возвращает его описание
     * @param string $packageName
     * @return array|bool
     */
    protected function _includeFile($packageName) {
        $packageFolder = false;
        $fileName = false;
        if (!$packageName) {
            $fileName = 'package.php';
            $packageFolder = ''; // пакет в текущей директории - то есть пакет это сам проект
        }
        else {
            if (is_string($packageName)) {
                $packageFolder = Dante_Lib_Config::get('enviroment.root_path').Dante_Lib_Package::getPackageFolder($packageName);  // пакет в другой директории
                $fileName = $packageFolder.'/package.php';
            }
        }

        //echo("try to include : $fileName<br>");
        //Dante_Lib_Log_Factory::getLogger()->debug("try to include : $fileName");

        if (!file_exists($fileName)) return false;

        include_once($fileName);
        global $package;
        $package['folder'] = $packageFolder;

        return $package;
    }

    /**
     * Подключение пакета
     * @param string $packageName имя пакета
     * @return bool
     */
    public function includePackage($packageName = false) {
        if (in_array($packageName, $this->_includedPackages)) return false;
            //echo("include package : $packageName <br>");
        // подключаем файл пакета
        $package = $this->_includeFile($packageName);
        if (!$package) return false;


        // обрабатываем зависимости
        if (isset($package['dependence'])) $this->_processDependence($package['dependence']);
        

        if (isset($package['css'])) $this->_processCss($package['css']);
        if (isset($package['js'])) $this->_processJs($package);

        // запускаем обсерверы
        if($package['folder'])
        {
            if (file_exists($package['folder'].'/observers.php')) {
                include_once($package['folder'].'/observers.php');  // даем возможность модулю повесить свои обсерверы
            }

            // даем возможность модулю повесить свои правила роутинга
            if (file_exists($package['folder'].'/conf/route.php')) {
                include_once($package['folder'].'/conf/route.php');
            }
        }




        if ($package['folder'] != '') 
            $includeFile = $package['folder'].'/include.php';
        else $includeFile = 'include.php';
        
        if (file_exists($includeFile)) {
            include_once($includeFile);
        }

        $currentVersion = (int)$this->_migration->getVersion($package['name']);
        // накатываем миграции
        if ($this->_allowUpdate && ($currentVersion < (int)$package['version'])) {
            if (!$this->_migration->doMigration($package)) {
                throw new Exception("cant update package {$package['name']} : migration error : alter ".$this->_migration->getLastAlter().'sql '.$this->_migration->getLastSql());
            }
            // обновляем версию самого пакета
            $this->_migration->updateVersion($package['name'], $package['version']);
        }

        if($packageName == "villa.module.admins")
        {
            $a=1;
        }
        //file_put_contents("test.txt",$packageName."\r\n", FILE_APPEND);
        // запоминаем, что мы уже подключили этот пакет
        $this->_includedPackages[] = $packageName;
    }

    protected function _processDependence($dependence) {
        
        foreach($dependence as $index => $packageName) {
            // если задаются пакеты, спецефичные для рабочего окружения
            if ($index === 'ws') {
                $wsName = Dante_Helper_Workspace::getName();
                if (isset($packageName[$wsName])) {
                    $this->_processDependence($packageName[$wsName]);
                }
            }

            $this->includePackage($packageName);
        }
    }

    protected function _processCss($css) {
        foreach($css as $index => $cssFile) {
            if ($index == 'ws' && is_array($cssFile)) {
                $wsName = Dante_Helper_Workspace::getName();
                if (isset($cssFile[$wsName])) {
                    $this->_processCss($cssFile[$wsName]);
                }
            }
            else  Dante_Helper_App::addCss($cssFile);
        }
    }

    /**
     * Подключение javascript-ов пакета
     * @param array $package
     */
    protected function _processJs($package) {
        if (isset($package['js'])) $js = $package['js'];
        else $js = $package;

        if (is_string($js)) {
            $js = Dante_Lib_Config::get('enviroment.root_path').Dante_Lib_Package::getPackageFolder($package['name']).'/'.$js;
            return Dante_Helper_App::addJs($js);
        }

        foreach($js as $index => $jsFile) {
            if ($index == 'ws' && is_array($jsFile)) {
                $wsName = Dante_Helper_Workspace::getName();
                if (isset($jsFile[$wsName])) {
                    $this->_processJs($jsFile[$wsName]);
                }
            }
            else {
                //echo "Add js : $jsFile<br/>";
                Dante_Helper_App::addJs($jsFile);
            }
        }
    }
}
