<?php


/**
 * Класс для работы с коллекциями.
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Dante_Lib_Collection {
    
    /**
     * Элементы коллекции
     * @var array
     */
    protected $_items = array();
    
    public function add($key, $value) {
        $this->_items[$key] = $value;
    }
    
    public function get($key) {
        return (isset($this->_items[$key])) ? $this->_items[$key] : null;
    }
    
    public function delete($key) {
        unset($this->_items[$key]);
    }
}

?>
