<?php
/*
 * Класс реестр для работы с глобальными данными
 * @author
 */
class Dante_Lib_Registry
{

    private static $_registry = array(); 
    
    /**
     * сохраняем объект по ключу в реестр
     * 
     * @param integer|string $key
     * @param $var 
     * @return bool
     */
    public static function set($key, $var) {
        
        /*if (isset(self::$_registry[$key]) == true) {
                throw new Exception('Unable to set var `' . $key . '`. Already set.');
        }*/
        self::$_registry[$key] = $var;

        return true;
    }

    public static function add($key, $var) {
        self::$_registry[$key][] = $var;
    }
    
    /**
     * получаем значение реестра
     * @param string|int $key - ключ запрашиваемого реестра 
     * @param mixed $default - Возвращаемое по кмолчанию значение
     * @return Singleton registry instance
     * @autor editor Elrafir
     */
    public static function get($key, $default=NULL) {
        if (isset(self::$_registry[$key]) == false) {
                return $default;
        }
        return self::$_registry[$key];
    }
    
    
}
?>
