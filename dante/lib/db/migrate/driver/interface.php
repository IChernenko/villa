<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dorian
 */
interface Dante_Lib_Db_Migrate_Driver_Interface {
    
    public function open($sql);
    
    public function getNumRows($r);
    
    public function exec($sql);
    
    public function fetch($r);
}
?>
