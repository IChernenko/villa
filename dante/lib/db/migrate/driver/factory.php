<?php



/**
 * Description of factory
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Dante_Lib_Db_Migrate_Driver_Factory {
    
    /**
     *
     * @return Dante_Lib_Db_Migrate_Driver_Interface 
     */
    public static function create() {
        return new Dante_Lib_Db_Migrate_Driver_Dante();
    }
}

?>
