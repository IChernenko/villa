<?php



/**
 * Description of dante
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Dante_Lib_Db_Migrate_Driver_Dante implements Dante_Lib_Db_Migrate_Driver_Interface{
    
    public function open($sql) {
        return Dante_Lib_SQL_DB::get_instance()->open($sql);
    }
    
    public function getNumRows($r) {
        return Dante_Lib_SQL_DB::get_instance()->getNumRows($r);
    }
    
    public function exec($sql) {
        return Dante_Lib_SQL_DB::get_instance()->exec($sql);
    }
    
    public function fetch($r) {
        return Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r);
    }
}

?>
