<?php



/**
 * Description of db
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Dante_Lib_Db_Migrate_Adapter_Db implements Dante_Lib_Db_Migrate_Adapter_Interface{
    
    /**
     *
     * @var Dante_Lib_Db_Migrate_Driver_Interface 
     */
    protected $_driver;
    
    function __construct() {
        $this->_driver = Dante_Lib_Db_Migrate_Driver_Factory::create();
    }
    
    /**
     * @return Dante_Lib_Db_Migrate_Driver_Interface
     */
    protected function _getDriver() {
        return $this->_driver;
    }
    
    public function init() {
        if (!$this->_tableExist()) $this->_createTable();
    }
    
    /**
     * Проверяет есть ли системная таблица для хранения версий миграций
     * @return bool
     */
    protected function _tableExist() {
        $sql = 'show tables like "db_version"';
        $r = $this->_getDriver()->open($sql);
        return (bool)$this->_getDriver()->getNumRows($r);
    }

    /**
     * Возвращает версию
     */
    protected function _createTable() {
        $sql = "create table db_version (package varchar(255), version int(11))";
        $this->_getDriver()->exec($sql);
    }
    
    protected function _getVersion($packageName) {
        $sql = "select version from db_version where package='$packageName'";
        $r = $this->_getDriver()->open($sql);
        if (!$r) {
            $this->_createTable();
            $r = $this->_getDriver()->open($sql);
        }

        $f = $this->_getDriver()->fetch($r);
        if (!isset($f['version'])) return 0;
        return (int)$f['version'];
    }

    public function getVersion($packageName) {
        return $this->_getVersion($packageName);
    }
    
    public function updateVersion($packageName, $version) {
        return $this->_updateVersion($packageName, $version);
    }

    /**
     * Обновляет пакет до заданной версии
     * @param string $packageName
     * @param string $version
     * @return mixed
     */
    protected function _updateVersion($packageName, $version) {
        $sql = "select version from db_version where package='{$packageName}'";
        //echo($sql);
        $r = $this->_getDriver()->exec($sql);
        $numRows = $this->_getDriver()->getNumRows($r);
        if ($numRows == 0) {
            $sql = "insert into db_version (version, package) values ('$version', '{$packageName}')";
        }
        else {
            $sql = "update db_version set version='$version' where package='{$packageName}'";
        }

        return $this->_getDriver()->exec($sql);
    }
}

?>
