<?php



/**
 *
 * @author dorian
 */
interface Dante_Lib_Db_Migrate_Adapter_Interface {
    
    public function init();
    
    /**
     * Определение версии пакета
     */
    public function getVersion($packageName);
    
    /**
     * Обновить версию пакета
     */
    public function updateVersion($packageName, $version);
}
?>
