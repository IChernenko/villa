<?php
/**
 * Скрипт для миграции баз данных
 * User: dorian
 * Date: 20.03.12
 * Time: 17:05
 * To change this template use File | Settings | File Templates.
 */
class Dante_Lib_Db_Migrate
{
    protected $_pattern = "/^alter_(\d+)+/";

    protected $_lastSql = '';

    protected $_lastAlter = '';

    protected static $_adapter = null;
    
    function __construct() {
        if (is_null(self::$_adapter)) {
            self::$_adapter = new Dante_Lib_Db_Migrate_Adapter_Db();
            self::$_adapter->init();
        }
    }
    
    /**
     * @return Dante_Lib_Db_Migrate_Adapter_Interface
     */
    protected function _getAdapter() {
        return self::$_adapter;
    }
    
    protected function _getAlters($folder) {
        $alters = array();

        if (!is_dir($folder)){
            return false; // нечего инсталлировать
        }

        if ($handle = opendir($folder)) {
            while (false !== ($entry = readdir($handle))) {
                if (preg_match($this->_pattern, $entry, $m)) {
                    $alters[ (int)$m[1] ] = $entry;
                }
            }
            closedir($handle);
        }
        ksort($alters);
        return $alters;
    }

    

    protected function _execSql($sql) {
        $this->_lastSql = $sql;
        return Dante_Lib_SQL_DB::get_instance()->exec($sql);
    }

    public function getLastSql() {
        return $this->_lastSql;
    }

    public function getLastAlter() {
        return $this->_lastAlter;
    }

    /**
     * Выполнить файл альтера
     *
     * @param string $folder папка с альтером
     * @param string $alter имя файла с альтером
     * @param $version
     * @return bool
     */
    protected function _execAlter($folder, $alter, $version)   {
        $this->_lastAlter = $alter;

        $alterPath = "{$folder}/{$alter}";

        Dante_Lib_Log_Factory::getLogger()->debug("try to exec alter : $alterPath");

        // todo: проверить разрешение файла
        $ext = pathinfo($alterPath, PATHINFO_EXTENSION);
        // todo: если файл php то просто сделать его include
        if ($ext == 'php') {
            if (!file_exists($alterPath)) return false;
            return include_once($alterPath);
        }

        $content = file_get_contents($alterPath);
        $sqls = explode(";", $content);
        if (count($sqls) == 0) {
            return false;
        }

        foreach($sqls as $index=>$sql) {
            $sql = trim($sql);
            if (strlen($sql) == 0) continue;
            
            if (!$this->_execSql($sql)) {
                echo Dante_Lib_SQL_DB::get_instance()->getError();
                return false;
            }
        }

        return true;
    }
    
    // ??
    public function getVersion($packageName) {
        return $this->_getAdapter()->getVersion($packageName);
    }
    
    // ??
    public function updateVersion($packageName, $version) {
        return $this->_getAdapter()->updateVersion($packageName, $version);
    }

    public function doMigration($package) {
        $packageName = $package['name'];
        $packageVersion = $package['version'];


        // получить версию из базы
        $version = $this->_getAdapter()->getVersion($packageName);
        // если у нас актуальная версия
        if ($version == $packageVersion) {
            return true;
        }

        $alterFolder = '../'.Dante_Lib_Package::getPackageFolder($packageName).'/install';
        $alters = $this->_getAlters($alterFolder);

        if (!$alters) return true;
        // 1 прочитать директорию

        // выбрать файлы, у которых версия старше
        // выполнить их
        $version++;
        while(isset($alters[$version])) {
            $alter = $alters[$version];
            if ($this->_execAlter($alterFolder, $alter, $version)) {
                $this->_getAdapter()->updateVersion($packageName, $version);
            }
            else {
                return false;
            }
            $version++;
        }
        return true;
    }
}
