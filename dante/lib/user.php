<?php

/**
 * Description of user
 *
 * @author Администратор
 */
class Dante_Lib_User {
    
    /**
     * Получить идентификатор залогиненного пользователя.
     * @return int || null
     */
    public function get_active()
    {
        $user_id = Lib_Request::get('user_id');
        if ($user_id) {
            return $user_id;
        }
            
        if (!isset($_COOKIE['astral_session_id'])) {
            throw new Exception('astral session doesnt exists');
        }
        $session = $_COOKIE['astral_session_id'];
        
        return Component_Auth_Session::get_user_id_by_session($session);
    }
}

?>
