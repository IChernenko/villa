<?php

/**
 * Подключение необходимых App контроллеров
 *
 * @author Администратор
 */
class Dante_Lib_Command {

    /**
     * Имя контроллера по умолчанию
     * @var string
     */
    public $_default_controller = "";

    /**
     * Выполнение команды согласно заданному запросу
     * @param Lib_Request $request
     * @return type 
     */
    public function execute(Dante_Lib_Request $request) {
        $controllerAlias = $request->get('controller');
        $action = $request->get('action');
        
        $controllerName = false;
        
        // add router there
        $requestedUrl = $request->get_requested_url();
        
        // Даем возможность повеситься на событие перед обработкой правил роутинга
        Dante_Lib_Observer_Helper::fireEvent('router.before', $request);
        $routeInfo = Dante_Lib_Router::processRules($requestedUrl);
        
        // Переопределение имени конроллера и действий на основании правил роутинга
        if ($routeInfo) {
            $controllerName = $routeInfo['controller'];
            if (isset($routeInfo['action'])) {
                $action = $routeInfo['action'];
            }
        }
        
        if (!$controllerAlias)  $controllerAlias = $this->_default_controller;

        if (!$controllerName) $controllerName = Dante_Helper_App::aliasToClassName($controllerAlias);
        
        
        //echo('controller name ="'.$controllerName.'"');
        if (!$controllerName) throw new Exception('cant find name for alias '.$controllerAlias);
        
        // Если такого класса нет - запускаем старотовую страницу
        if (!class_exists($controllerName)) {
            $controllerName = Dante_Helper_App::aliasToClassName($this->_default_controller);
        }
            //echo($controllerName); die();
        $controller = new $controllerName();
        $controller->setRequest($request);
        if (isset($routeInfo['params'])) $controller->setParams($routeInfo['params']);
        
        return $controller->run($action);
    }

}

?>