<?php

/**
* Класс для работы с базой данных.
* 
* @package lib
* @subpackage sql
* @author Sergey Suzdaltsev
* @version $Id$
*/
class Dante_Lib_SQL_DB {

    /**
     * Драйвер для работы с БД.
     * @var Lib_SQL_Driver_Mysql
     */
    protected $_driver;

    /**
     * Наш инстанс.
     * @var Lib_SQL_DB
     */
    protected static $_instance = null;

    /**
     * Возвращает наш инстанс.
     * @return Dante_Lib_SQL_DB
     */
    public static function get_instance()
    {
        if (is_null(self::$_instance)) {
                self::$_instance = new Dante_Lib_SQL_DB();
        }

        return self::$_instance;
    }

    function __construct()
    {
        // TODO добавить поддержку pdo 
		$drvr = Dante_Lib_Config::get('DB.db_driver');
		switch ($drvr) {
			case "mysql":
				$this->_driver = new Dante_Lib_SQL_Driver_Mysql();
				break;
			case "mysqli":
				$this->_driver = new Dante_Lib_SQL_Driver_Mysqli();
				break;
			default:
				$this->_driver = new Dante_Lib_SQL_Driver_Mysql();
				break;
		}	
        $this->connect();
    }

    /**
     * Устанавливает соединение с базой данных.
     * 
     */
    public function connect()
    {
        $params = array();
        $params['user'] = Dante_Lib_Config::get('DB.db_login');
        $params['password'] = Dante_Lib_Config::get('DB.db_psw');
        $params['host'] = Dante_Lib_Config::get('DB.db_host');
        $params['db_name'] = Dante_Lib_Config::get('DB.db_dbname');

        $this->_driver->connect($params);
        
        $this->exec("set character_set_client='utf8'");
        $this->exec("set character_set_results='utf8'");
        $this->exec("set collation_connection='utf8_general_ci'");

    }

    /**
     * Выполняет sql запрос
     * @param string $sql
     * @return mixed
     */
    public function open($sql)
    {
        $sql = $this->_processTablePrefix($sql);  //echo($sql.'<hr>');
        return $this->_driver->open($sql);
    }
    
    public function open_and_fetch($sql)
    {
        $r = $this->open($sql);
        return $this->_driver->fetch_array($r);
    }

    public function fetchField($sql, $fieldName) {
        $f = $this->open_and_fetch($sql);
        if (!$f) return false;
        if (!isset($f[$fieldName])) return false;
        return $f[$fieldName];
    }
    
    public function exec($sql)
    {
        $sql = $this->_processTablePrefix($sql);
        return $this->_driver->open($sql);
    }

    /**
     *
     * @param type $tableName
     * @return Dante_Lib_Sql_Metadata 
     */
    public function getMetaData($tableName)
    {
        $tableName = $this->_processTablePrefix($tableName);
        return $this->_driver->getMetaData($tableName);
    }
    
    public function getFoundRows()
    {
        return $this->_driver->getFoundRows();
    }
    
    public function constraintExist($fkName)
    {
        $dbName = Dante_Lib_Config::get('DB.db_dbname');
        $sql = "SELECT NULL FROM information_schema.TABLE_CONSTRAINTS WHERE
                   CONSTRAINT_SCHEMA = '{$dbName}' AND
                   CONSTRAINT_NAME   = '{$fkName}' AND
                   CONSTRAINT_TYPE   = 'FOREIGN KEY'";
        
        return $this->exist($sql);
    }

    protected function _processTablePrefix($sql) {
        //Dante_Lib_Log_Factory::getLogger()->debug('replace sql = '.$sql.' with '.Dante_Lib_Config::get('DB.tablePrefix'));
        return str_replace('[%%]', Dante_Lib_Config::get('DB.tablePrefix'), $sql);
    }
	
	public function getNumRows($r)
    {
        return $this->_driver->get_num_rows($r);
    }
    
    /**
     * Проверяет запись на существования
     * 
     * @return bool
     */
    public function exist($sql)
    {
        return $this->_driver->exist($sql);
    }
    
    public function getError() {
        return $this->_driver->getError();
    }
    
    
    public function get_last_insert_id()
    {
        return $this->_driver->get_last_insert_id();
    }
    
    public function fetch_assoc($mysql_result)
    {
        return $this->_driver->fetch_assoc($mysql_result);
    }
    /**
     * Возвращает 
     * @param mysql_result $mysql_result
     */
    
    
    
    
}

?>
