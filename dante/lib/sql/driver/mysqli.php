<?php

/**
* Драйвер по работе с mysql
 * 
* @package sql
* @subpackage driver
* @author Alexey Greeschenko 
* @version $Id$
*/
class Dante_Lib_SQL_Driver_Mysqli {
    
    protected $_connected = false;

	protected $connection = false;

    protected $_lastSQL;
    
    protected $_error;
	//
    /**
     * Устанавливает соединение с базой данных.
     * @param array $params
     * @return boolean
     */
    public function connect($params)
    {
        $r = mysqli_connect($params['host'], $params['user'], $params['password'], $params['db_name']);

        if(($err = mysqli_error($r)) != ''){
            trigger_error('MySql returned error: '.$err);
        }

        if(($err = mysqli_error($r)) != ''){trigger_error('MySql returned error: '.$err);}

        mysqli_query ($r, "set character_set_client='utf8'");
        mysqli_query ($r, "set character_set_results='utf8'");
        mysqli_query ($r, "set collation_connection='utf8_general_ci'");
        
        $this->_connected = true;
		$this->connection = $r;
		//
            /*
            $this->_db = new mysqli($params['host'], $params['user'], $params['password'], $params['db_name']);

    if (mysqli_connect_errno())
    {
        throw new Exception("Connect failed: %s\n".mysqli_connect_error());
        return false;
    }

    if(!$this->_db)
    {
        return false;
    }*/

        return true;
    }

    /**
     * Выполняет sql запрос
     * @param string $sql
     * @return mixed
     */
    public function open($sql)
    {
        //echo($sql.'<br/>');
        Dante_Lib_Log_Factory::getLogger()->debug('open sql = '.$sql);
        $this->_lastSQL = $sql;
        $r = mysqli_query($this->connection,$sql);
		//
        //if (!$r) {echo($sql); die();}
        if(($err = mysqli_error($this->connection)) != '') $this->_error = $err;
        return $r;
    }
    
    public function getError() {
        return $this->_error;
    }
    
    public function get_last_insert_id()
    {
        return mysqli_insert_id($this->connection);
    }
    
    /**
     * Проверяет запись на существования
     * 
     * @return bool
     */
    public function exist($sql)
    {
        $r = $this->open($sql);
        return ($this->get_num_rows($r) > 0) ? true : false;
    }
    
    /**
     *
     * @param type $r
     * @return int
     */
    public function get_num_rows($r)
    {
        if (!$r) throw new Exception("get_num_rows : {$this->_lastSQL}");
        return mysqli_num_rows($r);
    }
    
    public function getFoundRows()
    {
        $sql = 'SELECT FOUND_ROWS() as count';
        $r = $this->open($sql);
        $f = $this->fetch_assoc($r);
        return $f['count'];
    }

    /**
     *
     * @param string $tableName
     * @return Dante_Lib_Sql_Metadata 
     */
    public function getMetaData($tableName) {
        $sql = "SHOW COLUMNS FROM {$tableName}";
        $r = $this->open($sql);
        if ($this->num_rows($r) == 0) return false;

        $metaData = new Dante_Lib_Sql_Metadata();
        while($f = $this->fetch_array($r)) {
            $field = array(
                'name'      => $f['Field'],
                'type'      => $f['Type'],
                'null'      => $f['Null'],      // NO
                'key'       => $f['Key'],       // PRI
                'default'   => $f['Default'],   // NULL
                'extra'     => $f['Extra']      // auto_increment
            );
            $metaData->setField($f['Field'], $field);
            if ($field['key'] == 'PRI') $metaData->setPrimaryKey($f['Field']);
            
        }

        return $metaData;
    }
    
  function count_only($query){
	if(!$this->_connected){trigger_error('Sql class not initialized, programmer`s error');}
	$res = mysqli_query($query);
	if(($err = mysqli_error()) != ''){trigger_error('MySql returned error: '.$err);}
	return mysqli_num_rows($res);
  }

  function get_last_autoincrement($table, $param = 'id'){
	  $query='SELECT `'.$param.'` FROM `'.$table.'` ORDER BY `'.$param.'` DESC LIMIT 1';
	  return mysqli_query($query);
  }

  function num_rows($mysqli_result){
      if (!$mysqli_result) {
          throw new Exception("sql: ".$this->_lastSQL);
      }
    return mysqli_num_rows($mysqli_result);
  }

  function affected_rows(){
    return mysqli_affected_rows();
  }

  function fetch_array($mysqli_result){
      if (!$mysqli_result) throw new Exception('invalid result for fetch : '.$this->_lastSQL);
    $f = mysqli_fetch_assoc($mysqli_result);
    return Dante_Lib_Strings::processString($f);
  }

  function result($mysqli_reult, $mv = 0, $param_name = 'id'){
    return mysqli_result($mysqli_reult, $mv, $param_name);
  }
  
  function fetch_assoc($mysqli_result)
  {
    if (!$mysqli_result) throw new Exception('invalid result for fetch : '.$this->_lastSQL);
    $f = mysqli_fetch_assoc($mysqli_result);
    if (!$f) return false;
    return Dante_Lib_Strings::processString($f);
  }
}

?>
