<?php

/**
* Драйвер по работе с mysql
 * 
* @package sql
* @subpackage driver
* @author Sergey Suzdaltsev
* @version $Id$
*/
class Dante_Lib_SQL_Driver_Mysql {
    
    protected $_connected = false;

    protected $_lastSQL;
    
    protected $_error;

    /**
     * Устанавливает соединение с базой данных.
     * @param array $params
     * @return boolean
     */
    public function connect($params)
    {
        $r = mysql_connect($params['host'], $params['user'], $params['password']);

        if(($err = mysql_error()) != ''){
            trigger_error('MySql returned error: '.$err);

        }
        mysql_select_db($params['db_name']);

        if(($err = mysql_error()) != ''){trigger_error('MySql returned error: '.$err);}

        mysql_query ("set character_set_client='utf8'");
        mysql_query ("set character_set_results='utf8'");
        mysql_query ("set collation_connection='utf8_general_ci'");
        
        $this->_connected = true;
            /*
            $this->_db = new mysqli($params['host'], $params['user'], $params['password'], $params['db_name']);

    if (mysql_connect_errno())
    {
        throw new Exception("Connect failed: %s\n".mysql_connect_error());
        return false;
    }

    if(!$this->_db)
    {
        return false;
    }*/

        return true;
    }

    /**
     * Выполняет sql запрос
     * @param string $sql
     * @return mixed
     */
    public function open($sql)
    {
        //echo($sql.'<br/>');
        //Dante_Lib_Log_Factory::getLogger()->debug('open sql = '.$sql);
        $this->_lastSQL = $sql;
        $r = mysql_query($sql);
        //if (!$r) {echo($sql); die();}
        if(($err = mysql_error()) != '') $this->_error = $err;
        return $r;
    }
    
    public function getError() {
        return $this->_error;
    }
    
    public function get_last_insert_id()
    {
        return mysql_insert_id();
    }
    
    /**
     * Проверяет запись на существования
     * 
     * @return bool
     */
    public function exist($sql)
    {
        $r = $this->open($sql);
        return ($this->get_num_rows($r) > 0) ? true : false;
    }
    
    /**
     *
     * @param type $r
     * @return int
     */
    public function get_num_rows($r)
    {
        if (!$r) throw new Exception("get_num_rows : {$this->_lastSQL}");
        return mysql_num_rows($r);
    }
    
    public function getFoundRows()
    {
        $sql = 'SELECT FOUND_ROWS() as count';
        $r = $this->open($sql);
        $f = $this->fetch_assoc($r);
        return $f['count'];
    }

    /**
     *
     * @param string $tableName
     * @return Dante_Lib_Sql_Metadata 
     */
    public function getMetaData($tableName) {
        $sql = "SHOW COLUMNS FROM {$tableName}";
        $r = $this->open($sql);
        if ($this->num_rows($r) == 0) return false;

        $metaData = new Dante_Lib_Sql_Metadata();
        while($f = $this->fetch_array($r)) {
            $field = array(
                'name'      => $f['Field'],
                'type'      => $f['Type'],
                'null'      => $f['Null'],      // NO
                'key'       => $f['Key'],       // PRI
                'default'   => $f['Default'],   // NULL
                'extra'     => $f['Extra']      // auto_increment
            );
            $metaData->setField($f['Field'], $field);
            if ($field['key'] == 'PRI') $metaData->setPrimaryKey($f['Field']);
            
        }

        return $metaData;
    }
    
      function count_only($query){
        if(!$this->_connected){trigger_error('Sql class not initialized, programmer`s error');}
        $res = mysql_query($query);
        if(($err = mysql_error()) != ''){trigger_error('MySql returned error: '.$err);}
        return mysql_num_rows($res);
      }

  function get_last_autoincrement($table, $param = 'id'){
	  $query='SELECT `'.$param.'` FROM `'.$table.'` ORDER BY `'.$param.'` DESC LIMIT 1';
	  return mysql_query($query);
  }

  function num_rows($mysql_result){
      if (!$mysql_result) {
          Dante_Lib_Log_Factory::getLogger()->debug("mysql last query : ".$this->_lastSQL);

      }
    return mysql_num_rows($mysql_result);
  }

  function affected_rows(){

    return mysql_affected_rows();
  }

  function fetch_array($mysql_result){
    if (!$mysql_result) {
        Dante_Lib_Log_Factory::getLogger()->debug("mysql last query : ".$this->_lastSQL);

    }
    $f = mysql_fetch_array($mysql_result, MYSQL_ASSOC);
    return Dante_Lib_Strings::processString($f);
  }

  function result($mysql_reult, $mv = 0, $param_name = 'id'){
    return mysql_result($mysql_reult, $mv, $param_name);
  }
  
  function fetch_assoc($mysql_result)
  {
    if (!$mysql_result) throw new Exception('invalid result for fetch : '.$this->_lastSQL);
    $f = mysql_fetch_assoc($mysql_result);
    if (!$f) return false;
    return Dante_Lib_Strings::processString($f);
  }
}

?>
