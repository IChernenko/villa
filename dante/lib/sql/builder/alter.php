<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 12/14/13
 * Time: 1:04 PM
 * To change this template use File | Settings | File Templates.
 */

class Dante_Lib_Sql_Builder_Alter {

    /**
     * Добавить колонку
     *
     * @param array $params
     *
     * array(
     *      'tableName' - string имя таблицы
     *      'columnName' - string имя добавляемой колонки
     *      'columnType' - string тип колонки
     *      'notNull' - bool
     *      'after' - string имя колонки после которой добавлять поле
     * );
     *
     * Example:
     *
     * Dante_Lib_Sql_Builder_Alter::addColumn(array(
     *      'tableName'     => '%%user_profile',
     *      'columnName'    => 'first_name',
     *      'columnType'    => 'varchar(64)'
     * ));
     */
    public static function addColumn($params) {
        $sql = "ALTER TABLE {$params['tableName']} ADD column `{$params['columnName']}` {$params['columnType']}";
        if (isset($params['notNull'])) {
            $sql .= ' NOT NULL ';
        }

        if (isset($params['after'])) {
            $sql .= ' AFTER '.$params['after'];
        }

        return $sql;
    }
}