<?php

/**
 * Построитель select-запросов
 *
 * @author Администратор
 */
class Dante_Lib_Sql_Builder_Select extends Dante_Lib_Sql_Builder_Base{
    
    
    protected $_fields = '*';
    
    protected $_calcFoundRows = false;
    
    protected $_tableName = false;

    protected $_tableAlias = false;
    
    protected $_condition = array();
    
    protected $_order = false;
    
    protected $_limit = false;

    protected $_join = array();

    function __construct($tableName = false, $tableAlias = false) {
        if ($tableName) $this->from($tableName);
        if ($tableAlias) $this->_tableAlias = $tableAlias;
    }
    
    public function select($fields)
    {
        $this->_fields = $fields;
        return $this;
    }

    public function count()
    {
        $this->_fields = ' count(*) as count ';
        return $this;
    }

    public function field($fieldName)
    {
        $this->_fields[] = $fieldName;
        return $this;
    }
    
    public function calcFoundRows()
    {
        $this->_calcFoundRows = true;
        return $this;
    }
    
    public function from($tableName)
    {
        $this->_tableName = $tableName;
        return $this;
    }
    
    public function where($condition)
    {
        $this->_condition = $condition;
        return $this;
    }
    
    /**
     * Добавить одиночное условие
     * @param string || array $field
     * @param string $value
     * @return Dante_Lib_Sql_Builder_Select 
     */
    public function addCondition($field, $value=false)
    {
        if (is_array($field)) {
            foreach($field as $fieldName => $fieldValue) {
                $this->_condition[$fieldName] = $fieldValue;
            }
            return $this;
        }
        
        $this->_condition[$field] = $value;
        return $this;
    }
    
    public function orderby($order)
    {
        $this->_order = $order;
        return $this;
    }
    
    public function limit($limit)
    {
        $this->_limit = $limit;
        return $this;
    }

    public function join($tableName, $alias, $condition) {
        $this->_join[$tableName]['type'] = 'LEFT';
        $this->_join[$tableName]['alias'] = $alias;
        $this->_join[$tableName]['condition'] = $condition;
        return $this;
    }

    protected function _processJoin() {
        if (count($this->_join) == 0) {
            return '';
        }

        $sql = '';
        foreach ($this->_join as $tableName => $info) {
            $type = $info['type'];
            $alias = $info['alias'];
            $condition = $info['condition'];
            $condition = self::_build_conditions($condition, FALSE);
            $sql .= " {$type} JOIN {$tableName} as {$alias} on ({$condition}) ";
        }

        return $sql;
    }

    
    function __toString() 
    {
        $sql = 'select ';
        
        if ($this->_calcFoundRows) {
            $sql .= ' SQL_CALC_FOUND_ROWS ';
        }
        
        if (is_string($this->_fields)) {
            $sql .= $this->_fields;
        }
        elseif (is_array($this->_fields)&&count($this->_fields)>0) {
            $sql .= implode(',', $this->_fields);
        }
        else {
            $sql .= ' * ';
        }
        
        $sql .= ' from '.$this->_tableName;
        if ($this->_tableAlias) {
            $sql .= ' as '.$this->_tableAlias.' ';
        }

        $sql .= $this->_processJoin();
        
        if ($this->_condition) {
            $sql .= self::_build_conditions($this->_condition);
        }
        
        if ($this->_order) {
            $sql .= ' order by '.$this->_order;
        }
        
        if ($this->_limit) {
            if (is_int($this->_limit)){
                $sql .= ' limit '.$this->_limit;
            }
            if (is_string($this->_limit)){
                $sql .= ' limit '.$this->_limit;
            }
            if (is_array($this->_limit)) {
                $count = array_pop($this->_limit);
                $offset = array_pop($this->_limit);
                $sql .= ' limit '.$offset.' ,'.$count;
            }
        }
        
        return $sql;
    }
    
    public static function build($table_name, $fields, $condition = false, $order = false, $limit = false)
    {
        
        $sql = 'select ';
        
        if (is_string($fields)) {
            $sql .= $fields;
        }
        
        if (is_array($fields)&&count($fields)>0) {
            $sql .= implode(',', $fields);
        }
        
        $sql .= ' from '.$table_name;
        $sql .= self::_build_conditions($condition);
        
        if ($order) {
            $sql .= ' order by '.$order;
        }
        
        if ($limit) {
            if (is_int($limit)){
                $sql .= ' limit '.$limit;
            }
            if (is_string($limit)){
                $sql .= ' limit '.$limit;
            }
            if (is_array($limit)) {
                $count = array_pop($limit);
                $offset = array_pop($limit);
                $sql .= ' limit '.$offset.' ,'.$count;
            }
        }
        
        return $sql;
    }
}

?>
