<?php


/**
 * Description of update
 *
 * @author Elrafir
 */
class Dante_Lib_Sql_Builder_Update extends Dante_Lib_Sql_Builder_Base{
    /**
     * Строим хапрос для апдейта согласно типам переменных
     * @param string $table_name
     * @param array|string $fields
     * @param array|string $condition
     * @return string
     */
    public static function build($table_name, $fields, $condition = false)
    {
        $table_name = self::replaceTablePrefix($table_name);
        $sql = 'UPDATE `'.$table_name.'` SET ';
        
        if (is_array($fields)&&count($fields)>0) {
            
            foreach ($fields as $field_name => $field_value) {
            
                if (is_int($field_value)) {
                    $vars[$field_name] = $field_name.'='.$field_value;
                }
                elseif (is_string($field_value)) {
                    $vars[$field_name] = "`{$field_name}`='".addslashes($field_value)."'";
                }
                elseif (is_object($field_value)) {
                    $vars[$field_name] = $field_name.(string)$field_value;
                }
                elseif (is_float($field_value)) {
                    $vars[$field_name] = "`{$field_name}`='{$field_value}'";
                }
                elseif (is_null($field_value)) {
                    $vars[$field_name] = "`$field_name` = NULL";
                }
            }
            $sql .= implode(', ', $vars);
        }   
        
        if (is_string($fields)) {
            $sql .= ' '.$fields.' ';
        }
        
        $sql .= self::_build_conditions($condition);
        //echo $sql;
        return $sql;
    }
}

?>
