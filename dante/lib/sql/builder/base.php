<?php

/**
 * Description of base
 *
 * @author Администратор
 * @author Elrafir - адаптация + доработка.
 */
class Dante_Lib_Sql_Builder_Base {
    
    public static function buildConditions($condition, $addWhere = true) {
        return self::_build_conditions($condition, $addWhere);
    }

    public static function replaceTablePrefix($table_name)
    {
        $prefix = Dante_Lib_Config::get('DB.tablePrefix');

        return $prefix
            ?
                $table_name = str_replace("[%%]",Dante_Lib_Config::get('DB.tablePrefix'),$table_name)
            :
                $table_name;
    }
    
    protected static function _build_conditions($condition, $addWhere = true)
    {
        $sql = '';  //var_dump($condition);
        if (is_array($condition)&& count($condition)>0) {
            // сборка кондишенов
            foreach ($condition as $field_name => $field_value) {
                
                // кондишен число
                if (is_int($field_value)) {
                    $condition[$field_name] = "`{$field_name}` = {$field_value}";
                }
                
                // кондишен строка
                if (is_string($field_value)) {
                    $condition[$field_name] = "`{$field_name}` = '{$field_value}'";
                }
                
                if (is_object($field_value)) {
                    $condition[$field_name] = (string)$field_value;
                }
                
                if (is_array($field_value)) {
                    if (isset($field_value['operation'])) {
                        $condition[$field_name] = $field_name.$field_value['operation'].$field_value['value'];
                    }
                    else {
                        if (count($field_value) > 0) {
                            $condition[$field_name] = "`{$field_name}` in (".implode(', ', $field_value).")";
                        }
                    }
                }
            }
            
            //Lib_Log_File::debug('condition='.var_export($condition, true));
            if ($addWhere) {
                $sql .= ' WHERE ';
            }
            $sql .= implode(' AND ', $condition);

        }elseif(is_string($condition)){
            $sql .= " {$condition} ";
        }
        return $sql;
    }
}

?>
