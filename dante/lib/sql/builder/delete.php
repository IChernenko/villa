<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of delete
 *
 * @author Администратор
 */
class Dante_Lib_Sql_Builder_Delete extends Dante_Lib_Sql_Builder_Base{
    
    public static function build($table, $condition)
    {
        $sql = 'DELETE FROM '.$table;
        $sql .= self::_build_conditions($condition);
        return $sql;
    }
}

?>
