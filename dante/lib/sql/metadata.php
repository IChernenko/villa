<?php


/**
 * Класс для работы с метаданными
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Dante_Lib_Sql_Metadata {
    
    /**
     * Первичные ключи 
     * @var array
     */
    protected $_primaryKey = false;
    
    /**
     * Поля таблицы
     * @var array
     */
    protected $_fields = array();
    
    function __construct() {
        
    }
    
    /**
     *
     * @param array $fields 
     */
    public function setFields($fields) {
        $this->_fields = $fields;
    }
    
    public function setField($fieldName, $fieldProperties) {
        $this->_fields[$fieldName] = $fieldProperties;
    }
    
    /**
     * Проверяет есть ли такое поле
     * @param string $fieldName
     * @return bool
     */
    public function fieldExists($fieldName) {
        if (isset($this->_fields[$fieldName])) return true;
        return false;
    }
    
    public function setPrimaryKey($fieldName) {
        $this->_primaryKey = $fieldName;
    }
    
    public function getPrimaryKey() {
        return $this->_primaryKey;
    }
}

?>
