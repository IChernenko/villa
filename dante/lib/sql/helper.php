<?php



/**
 * Хелпер по работе с базой данных.
 * 
 * @package sql
 * @subpackage lib
 *
 * @author Sergey Suzdaltsev
 */
class Dante_Lib_Sql_Helper {
    
    /**
     *
     * @param string $table_name
     * @param string $field_name
     * @param array $condition
     * @return mixed
     */
    public static function fetch_single_field($table_name, $field_name, $condition)
    {
        $sql = Lib_Sql_Builder_Select::build($table_name, $field_name,
                $condition);
        $f = Lib_SQL_DB::get_instance()->open_and_fetch($sql);

        return (isset($f[ $field_name ])) ? $f[ $field_name ] : null;
    }

    /**
     * @param $params
     * @return mixed
     *
     * example:
     *
     * Dante_Lib_Sql_Helper::addColumnToTable(array(
     *      'tableName'     => '[%%]user_profile',
     *      'columnName'    => 'last_name',
     *      'columnType'    => 'varchar(64)'
     * ));
     */
    public static function addColumnToTable($params) {
        $sql = Dante_Lib_Sql_Builder_Alter::addColumn($params);
        //echo("sql: $sql <br>");
        return Dante_Lib_SQL_DB::get_instance()->exec($sql);
    }
}

?>
