<?php



/**
 * Description of helper
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 * @author Elrafir <elrafir$gmail.com>
 */
class Dante_Lib_Fs_Helper {
    
    public static function createDirPath($path, $chmod = 0755) {
        $data = explode('/', $path);
        $fullPath = '';
        foreach($data as $folder) {
            $fullPath .= $folder;
            if (!is_dir($fullPath)) {
                mkdir($fullPath, $chmod);  
            } 
        }
    }
    /**
     * Каскадное создание дирректории по указанному пути. 
     * @param string $path - например media1/exemple2/folder3/
     * @author Elrafir
     */
    public static function createDirByPath($path, $chmod = 0755) {
        $data = explode('/', $path);
        $fullPath = '';
        foreach($data as $folder) {
            $fullPath .= $folder.'/';
            if (!is_dir($fullPath)) mkdir($fullPath, $chmod);
        }
    }
}

?>
