<?php



/**
 * Description of file
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Dante_Lib_Fs_File {
    
    public static function delete($fileName) {
        if (!file_exists($fileName)) return false;
        unlink($fileName);
    }
    
    /**
     * Копирование директории с файлами и подкаталогами
     * @param string $src откуда 
     * @param string $dst куда
     */
    public static function copy($src, $dst) {
        if (strstr($src, '.svn')) return false;
        
        //echo("copy $src to $dst");
        if (is_dir($src))  {
            if (!is_dir($dst))
            if (!mkdir($dst)) {
                echo("cant create directory : $dst");
            }
            
            $d = dir($src);
            while (FALSE !== ($entry = $d->read())) {
                if ($entry == '.' || $entry == '..') continue;
                Dante_Lib_Fs_File::copy("$src/$entry", "$dst/$entry");
            }
            $d->close();
        }
        else { 
            copy($src, $dst);
        }
    }
}

?>
