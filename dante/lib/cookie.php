<?php



/**
 * Класс по работе с куками
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Dante_Lib_Cookie {
    
    public static function set($key, $value, $params=false) {        
        $expire = 0;
        $path = NULL;
        $domain = NULL;
        if($params && is_array($params))
        {
            if(isset($params['expire'])) $expire = (int)$params['expire'];
            if(isset($params['path'])) $path = $params['path'];
            if(isset($params['domain'])) $domain = $params['domain'];
        }
        setcookie($key, $value, $expire, $path, $domain);
    }
    
    public static function get($key, $defaultValue=false) {
        if (isset($_COOKIE[$key])) {
            return $_COOKIE[$key];
        }
        
        return $defaultValue;
    }
}

?>
