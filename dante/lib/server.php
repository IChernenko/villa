<?php



/**
 * Класс для работы с сервером
 *
 * @author Sergey Suzdaltsev
 */
class Dante_Lib_Server {

    /**
     * Возвращает ip адрес сервера
     * @return string
     */
    public function get_ip()
    {
        return $_SERVER['SERVER_ADDR'];
    }
    
    /**
     * Возвращает ip адрес сервера
     * @return string
     */
    public static function get_remote_ip()
    {
        return $_SERVER['REMOTE_ADDR'];
    }
}

?>
