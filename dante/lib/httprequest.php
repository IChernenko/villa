<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of httprequest
 *
 * @author Администратор
 */
class Dante_Lib_Httprequest {
    
    public static function call_url($url, $params = array())
    {
        if (count($params)>0) {
            $url .=  '?'.http_build_query($params);
        }
        //echo($url."\n");
        $curl_handler = curl_init($url);
        curl_setopt($curl_handler, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl_handler, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($curl_handler);
        curl_close($curl_handler);
        return $response;
    }
    
    public static function call_character($url, $params = array())
    {
        $url = Lib_Config::get('url.www_character').'/'.$url;
        
        return self::call_url($url, $params);
    }
    
    public static function sendPost($url, $params)
    {
        $channel = curl_init( );
        
        // you might want the headers for http codes
        curl_setopt( $channel, CURLOPT_HEADER, true );
        
        // you may need to set the http useragent for curl to operate as
        curl_setopt( $channel, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        
        // you wanna follow stuff like meta and location headers
        curl_setopt( $channel, CURLOPT_FOLLOWLOCATION, true );
        
        // you want all the data back to test it for errors
        curl_setopt( $channel, CURLOPT_RETURNTRANSFER, true );
        
        
        // if the $vars are in an array then turn them into a usable string
        if( is_array( $params ) ) {
            //$params = implode( '&', $params );
        }
        
        // setup the url to post / get from / to
        curl_setopt( $channel, CURLOPT_URL, $url );
        
        // the actual post bit
        
        curl_setopt( $channel, CURLOPT_POST, true );
        curl_setopt( $channel, CURLOPT_POSTFIELDS, $params );
        
        // return data
        return curl_exec( $channel );
        
    }
}

?>
