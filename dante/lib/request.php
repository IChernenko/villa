<?php

/**
 * Класс для работы с запросами
 * 
 * @package lib
 * @author Sergey Suzdaltsev
 * @version $Id$
 **/ 
class Dante_Lib_Request {
	
	protected $_params = array();
  
  /**
   * проверка строки, в utf8 ли она
   * 
   * @param string $s
   * @return bool 
   */ 
  public static function detect_my_utf($s)
  {
     $s=urlencode($s); // в некоторых случаях - лишняя операция (закоментируйте)
     $res='0';
     $j=strlen($s);

     $s2=strtoupper($s);
     $s2=str_replace("%D0",'',$s2);
     $s2=str_replace("%D1",'',$s2);
     $k=strlen($s2);

     $m=1;
     if ($k>0){
      $m=$j/$k;
      if (($m>1.2)&&($m<2.2)){ $res='1'; }
     }
     return $res;
  }
  
  public function set($key, $value)
  {
      $this->_params[$key] = $value;
  }
  
  public function setParams($params)
  {
      $this->_params = $params;
  }
  
  /**
   *  Получение параметра
   *  @param string $name имя параметра
   *  @param  mixed $default_value значение по умолчанию
   *  @return mixed || null значение параметра если он есть      
   */     
  public function get($name, $default_value = null, $length = false)
  {
      // если у нас есть запомненный параметр - то возвращаем его
      if (isset($this->_params[$name])) return $this->_params[$name];
      
	  
      if (isset($_REQUEST[$name])) {
          if (is_array($_REQUEST[$name])) {
              return $_REQUEST[$name];
          }
      }
      //return isset($_REQUEST[$name]) ? iconv('UTF-8','windows-1251',   $_REQUEST[$name]) : $default_value;
     /* if(isset($_REQUEST[$name])){
        $charset_code = Lib_Request::detect_my_utf($_REQUEST[$name]);
        Lib_Log_File::debug('Lib_Request::get...windows-1251, detect_my_utf='.$charset_code );
        Lib_Log_File::debug_item($name);
      }*/
      
      /*if(isset($_REQUEST[$name]) && Lib_Request::detect_my_utf($_REQUEST[$name])==0 ){
          $_REQUEST[$name] = iconv('windows-1251', 'UTF-8',  $_REQUEST[$name]);
      }*/
      if  (isset($_REQUEST[$name])) {
        $value = $_REQUEST[$name];
        if ($length) {
            $value = substr($value, 0, $length);
        }
        return $value;
      }
      return $default_value;
  }
  
  /**
   * 
   * @param string $name имя параметра
   * @param string || array имя типа валидации либо массив с параметрами валидации
   * @param  mixed $default_value значение по умолчанию
   * @return mixed || null значение параметра если он есть
   */
  public function get_validate($name, $validate=false, $default_value = null, $length = false){
               
      $obj=$this->get($name, $default_value, $length);
      
      if($validate){          
        $validator=new Dante_Lib_Validator();
        $validate_result=$validator->getValidate($obj, $validate);
        switch ($validate_result){
            case false:                
                return $default_value;
                break;
            }
      }      
      return $obj;
  }
  
  public function dateToInt($date=FALSE){
      if(!$date) return null;
      $validator=new Dante_Lib_Validator();
      return $validator->dateToInt($date);
  }

  /**
   *  Проверка существует ли переменная
   *  @param string $name наименование переменной
   *  @return bool      
   */     
  public static function exists($name)
  {
       return isset($_REQUEST[$name]);
  }		  
  
  public static function get_requested_url()
  {
      if (isset($_SERVER['REQUEST_URI']))
        return $_SERVER['REQUEST_URI'];
      return '';
  }
  
  public static function get_first_subdomain()
  {
      $env = Lib_Request::get('env');
      if ($env) {
          return $env;
      }
      
        $url = self::get_requested_url();
        $url_info = parse_url($url);
        if (isset($url_info['host'])) {
            $host = $url_info['host'];
            $subdomains = explode('.', $host);
            $first_domain = array_pop($subdomains);
            return $first_domain;
        }
        
        return '?';
  }
}

?>