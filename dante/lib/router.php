<?php



/**
 * Description of router
 *
 * @author dorian
 */
class Dante_Lib_Router {
    
    /**
     * Правила для роутинга
     * @var array
     */
    protected static $_rules = array();
    
    /**
     * Добавить правила для роутинга
     * @param string $query
     * @param array $routeInfo 
     */
    public static function addRule($pattern, $routeInfo) {
        self::$_rules[] = array(
            'pattern' => $pattern,
            'info' => $routeInfo
        );
    }

    public static function onUrl($url, $routeInfo) {
        $url = '/^'.str_replace('/', '\/', $url).'$/';
        self::addRule($url, $routeInfo);
    }
    
    /**
     * Обработать правила для заданного запроса
     * @param string $query 
     */
    public static function processRules($query) {
        foreach (self::$_rules as $index => $rule) {
            //echo("match pattern {$rule['pattern']} with $query <br>");
            //Dante_Lib_Log_Factory::getLogger()->debug("match pattern {$rule['pattern']} with $query");
            if (preg_match($rule['pattern'], $query, $matches)) {

                //Dante_Lib_Log_Factory::getLogger()->debug("matched");
                //Dante_Lib_Log_Factory::getLogger()->debug("matches :".var_export($matches, true));

                if ($matches) {
                    foreach($matches as $index=>$match) {
                        $rule['info']['params']['%'.$index] = $match;
                    }
                }
                
                // даем возможность выполнить callback
                if (isset($rule['info']['callback'])) {
                    $rule['info']['callback']($rule['info']['params']);
                }
                
                return $rule['info'];
            }
        }
        
        return false;
    }
}

?>
