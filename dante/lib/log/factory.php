<?php



/**
 * Description of factory
 *
 * @author dorian
 */
class Dante_Lib_Log_Factory {
	
	/**
	 *
	 * @var Dante_Lib_Log_File
	 */
	protected static $_logger = false;
	
	/**
	 *
	 * @return Dante_Lib_Log_File
	 */
	public static function getLogger()
	{
		if (!self::$_logger) {
			self::$_logger = new Dante_Lib_Log_File();
		}
		
		return self::$_logger;
	}
}

?>
