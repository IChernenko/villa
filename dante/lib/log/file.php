<?php



/**
 * Логгирование в файл
 *
 * @author Администратор
 */
class Dante_Lib_Log_File {
    
    public function debug($str, $fileName = 'logs/debug.log')
    {
        if (!Dante_Lib_Config::get('app.enableLogger')) return false;
        
        if (!is_dir('logs')) {
            mkdir('logs', 0775);
        }

        $ip = 'localhost';
        if (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

		$str = '['.Dante_Helper_Date::converToDateType(false, 7).'] ip: '.$ip.' '.$str;
        $f = fopen($fileName, 'a+');
        fwrite($f, $str."\n");
        fclose($f);
    }
    
    public function debug_item($item)
    {
        $this->debug(var_export($item, true));
    }
}

?>