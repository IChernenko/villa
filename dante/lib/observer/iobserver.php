<?php

/**
 * обсервер
 *
 * @packages observer
 * @subpackage lib
 *
 * @author Mort
 */

interface Dante_Lib_Observer_Iobserver
{

  public function notify($objSource, $strEventType );

}

?>