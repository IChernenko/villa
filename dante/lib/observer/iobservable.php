<?php

/**
 * обсервер
 *
 * @packages observer
 * @subpackage lib
 *
 * @author Mort
 */

interface Dante_Lib_Observer_Iobservable
{

  public function addObserver( Dante_Lib_Observer_Iobserver $objObserver, $strEventType );

  public function fireEvent( $strEventType, $observableObject=false );

}

?>