<?php

/**
 * Базовый класс с поддержкой обсерверов.
 *
 * @packages observer
 * @subpackage lib
 *
 * @author Sergey Suzdaltsev
 */

class Dante_Lib_Observer_Observable implements Dante_Lib_Observer_IObservable {

    /**
     * Коллекция обсерверов.
     *
     * @var array
     */
    protected $_observers = array();

    /**
     * Регистрирует объект обсервера для заданного события.
     *
     * @param IObserver $objObserver
     * @param string $strEventType
     */
    public function addObserver( Dante_Lib_Observer_IObserver $objObserver, $strEventType )
    {
        $this->_observers[$strEventType][] = $objObserver;
    }

    /**
     * Запуск заданного события.
     *
     * @param string $strEventType
     * @param mixed $observableObject объект, который передается обсерверу
     *          при генерации события. По умолчания равен $this.
     */
    public function fireEvent( $strEventType, $observableObject=false)
    {
        if (!$observableObject) {
            $observableObject = $this;
        }

        // Если у нас есть такое событие
        if( isset($this->_observers[$strEventType]) && is_array( $this->_observers[$strEventType] ) )
        {
            // Пробегаем по всем зарегистрированным  для данного события обсерверам
            foreach ( $this->_observers[$strEventType] as $objObserver )
            {
                // Вызываем notify для текущего обсервера.
                $objObserver->notify( $observableObject, $strEventType );
            }
        }
    }
}

?>