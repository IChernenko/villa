<?php



/**
 * Description of helper
 *
 * @author Администратор
 */
class Dante_Lib_Observer_Helper {
    
    protected static $_instance = false;
    
    

    /**
     *
     * @return Lib_Observer_Observable
     */
    public static function getInstance()
    {
        
        if (!self::$_instance) {
            self::$_instance = new Dante_Lib_Observer_Observable();
        }
        
        //var_dump(self::$_instance);die();
        return self::$_instance;
    }
    
    public static function addObserver( Dante_Lib_Observer_Iobserver $objObserver, $strEventType )
    {
        return self::getInstance()->addObserver($objObserver, $strEventType);
    }        
    
    
    public static function fireEvent( $strEventType, $observableObject=false)
    {
        //self::getInstance();
        return self::getInstance()->fireEvent( $strEventType, $observableObject);
    }
}

?>
