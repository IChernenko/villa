<?php

# Автоматическая обработка ошибок
function WorkError($code, $msg, $file, $line){
    if (!is_dir('err_logs')) {
        mkdir('err_logs', 0755);
    }
  // Пишем лог
  $fh = fopen('err_logs/error.log', 'a');

  $ip = (isset($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : '';

  fputs($fh, '['.Dante_Helper_Date::converToDateType(false, 7).'] User '.$ip.' generated error "'.$msg.'" in '.$file.' at line '.$line.' Code '.$code."\n");
  fclose($fh);
  die();
}

function writeErrorLog($msg) {
    $fh = fopen('err_logs/error.log', 'a');
    $ip = (isset($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : '';
    fputs($fh, '['.Dante_Helper_Date::converToDateType(false, 7).'] User '.$ip.' generated error '.$msg);
    fclose($fh);
}

set_error_handler("WorkError");


function DanteObjectToArray($d) {
    if (is_object($d)) {
        // Gets the properties of the given object
        // with get_object_vars function
        $d = get_object_vars($d);
    }

    if (is_array($d)) {
        /*
        * Return array converted to object
        * Using __FUNCTION__ (Magic constant)
        * for recursive call
        */
        return array_map(__FUNCTION__, $d);
    }
    else {
        // Return array
        return $d;
    }
}

?>
