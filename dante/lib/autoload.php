<?php 

/**
 *
 * @param type $class_name  Lib_Dictionary_Record
 */
function __autoload($class_name) 
{
   //echo('c=='.$class_name.'<br/>');
  if (strstr($class_name, '_')) {
    $packages = explode('_', $class_name);
    $file_name = implode('/', $packages).'.php';
        
    $file_name = Dante_Lib_Config::get('enviroment.root_path').strtolower($file_name);
        //echo($file_name.'<br>');
    if (file_exists($file_name)) include_once($file_name);

  }        
}

function danteAutoload($class_name) {
    if (strstr($class_name, '_')) {
        $packages = explode('_', $class_name);
        $file_name = implode('/', $packages).'.php';

        $file_name = DANTE_ENV_ROOT_PATH.strtolower($file_name);
        //echo($file_name.'<br>');
        if (file_exists($file_name)) {
            include_once($file_name);
        }
        else {
            //echo("can't load file : $file_name");
        }
    }
}

spl_autoload_register('danteAutoload');

?>