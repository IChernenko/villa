<?
/**
 * @author Elrafir
 *  Валидатор проверки данных
 */
class Dante_Lib_Validator
{
    
    /**
     * дефолтный регэксп может быть переопределен на лету
     * @var string 
     */
    protected $text_regexp="/[a-zёіґїа-яА-Я0-9\-\_\ \.\,\?\!\/\"\'\]+$/iu";
    
    /**
     * Преобразование даты из строки
     * @param string $date
     * @param int||format $format
     * @return int|false
     * @author Elrafir edition
     */
    public  function dateToInt($date, $format=0){        
        return Dante_Helper_Date::strtotimef($date, $format);
    }
    /**
     * Преобразование даты из mm/dd/yyyy
     * @param $date
     * @return int
     * @author Elrafir edition
     */
    public  function dateToIntAm($date, $format = 8) {
        return $this->dateToInt($date, $format);
    }

    /**
    * Функция вызова валидации 
    * @param string|int|array $var - переменная для проверки, может быть как строкой так и массивом с параметрами (см. пример).
    * @param string|array|regexp $type - название типа проверки, либо регэксп для проверки.
    * 
    * @return boolean
    */
    public  function getValidate($var, $type='text'){        
        
        if(is_array($type)){
            //Передали массив параметров
            $params=$var['params'];
            $type_name=$var['name'];
            if(isset($params['required']) && $params['required']==true){
                if(!$this->_getRequired($var)){
                    return false;
                }
            }
        }  else {
            //Передали строковое название типа проверки.
            $type_name=$type;
            $params=array();
        }
        
        //если значение пустое но не обязательно вернём null
        if(!$this->_getRequired($var)){
            return null;
        }
        //Синонимы
        $type_name=='integer'?$type_name='int':$type_name=$type_name;
        $type_name=='str'?$type_name='text':$type_name=$type_name;
        
        //Задействуем нужный валидатор
        
        switch ($type_name){
            case 'date':
                
                return $this->_date($var);
                break;
            case 'array':
                if(!is_array($var))
                    return false;
                return $var;
                break;
            case 'password':                
                $min=6;
                $max=24;
                if(isset($params['min'])){
                    $min=$this->_int($params['min']);
                }
                if(isset($params['max'])){
                    $max=$this->_int($params['max']);
                }
                return $this->_password($var, $min, $max);
                break;
            case 'email':                
                return $this->_email($var);
                break;
            case 'int':                
                $min=false;
                $max=false;
                if(isset($params['min'])){
                    $min=$this->_int($params['min']);
                }
                if(isset($params['max'])){
                    $max=$this->_int($params['max']);
                }
                return $this->_int($var, $min, $max);
                break;
                
            case 'text':
                
                //Текст по стандартной фильтрации ... стандартные параметры заданы в свойствах класса
                $max=false;
                if(isset($params['max'])){
                    $max=$this->_int($params['max']);
                }
                return $this->_text($var, $max);
                break;
            default:                
                //В дефолте ждём регэкспа                 
                return @filter_var($var, FILTER_VALIDATE_REGEXP,
                        array(
                            "options"=>array(
                                "regexp"=>$type_name
                                )
                            )
                        );
                break;
          }
          return false;
    }
    /**
     * Проверка наличия содержимого в переменной
     * @param string|int $cur  
     * @return boolean
     */
    public  function _getRequired($cur){
        if(is_array($cur)) return true;
        if(!$cur || trim($cur)===''){
            return FALSE;
        }else{
            return true;
        }
    }
    
    ### Функции валидции
    /**
     * Проверка правильности введной Даты . Пропустит любой формат поддерживаемый strtotime() + формат dd/mm/YY
     * @param string $date - строка даты
     * @return boolean
     */
    public  function _date($date, $format=0){       
        if(Dante_Helper_Date::strtotimef($date, $format)){            
            return $date;
        }else{
            return false;
        }
        
    }
    /**
     * проверка пароля
     * @param string $cur - проверяемый пароль
     * @return type
     */
    public  function _password($cur, $min, $max){
        if($cur && is_string($cur)){
            return $cur;
        }else{
            return false;
        }        
        return $this->_text($cur);
    }
    
    /**
     * проверка email
     * @param string $cur - проверяемый email
     * @return type
     */
    public  function _email($cur){        
        return filter_var($cur, FILTER_VALIDATE_EMAIL);
    }
    
    /**
     * Проверка на предмет integer буть то реальный int либо string с возможностью указания диапазона проверки
     * @param string|int $cur - реальный int либо string. 
     * @param type $min - минимально допустимое число
     * @param type $max - максимально допустимое число
     * @return int|false
     */
    public  function _int($cur, $min=FALSE, $max=false){
        $int_options = array("options"=>
            array()
            );
        if($min !==FALSE){
            $int_options['options']['min_range']=$min;
        }
        if($max !==FALSE){
            $int_options['options']['max_range']=$max;
        }
        if(!$max && !$min){
            //Проверяем регуляркой
            if (is_string($cur))
            return (preg_match("|^[-]{0,1}[\d]+[.]{0,1}[\d]*$|", $cur))?$cur:false;
        }
        return filter_var($cur, FILTER_VALIDATE_INT, $int_options);
    }
    /**
     * Проверяет, что значение является корректным числом с плавающей точкой.
     * @param string|int $cur - реальный int либо string.
     * @param bool $delimeter - разрешает использовать запятую в качестве визального делителя третьего порядка (выключено по умолчанию)
     * @return type
     */
    public  function _float($cur, $delimeter=false){
        if($delimeter){
            return filter_var($cur, FILTER_VALIDATE_FLOAT, FILTER_FLAG_ALLOW_THOUSAND);
        }
        return filter_var($cur, FILTER_VALIDATE_FLOAT);
    }
    
    /**
     * Выполняет проверку строки по regexp. 
     * Стандартный формат "/[a-zA-Zа-яА-Я0-9\-\_\ \.\,]+$/" может бть переназначен в свойстве $this->text_regexp
     * @param string|int $cur - проверяемая строка
     * @return str|false
     */
    public function _text($cur){
        if($cur && is_string($cur)){
            return $cur;
        }else{
            return false;
        }  
        $regexpp=  $this->text_regexp;
        $regexp=array(
            "options"=>array(
                "regexp"=>($regexpp),
                )
            );
        $text = filter_var($cur, FILTER_VALIDATE_REGEXP, $regexp);
        return ($text);        
    }
    /**
     * Проверка на правиьность URL
     * @param string $cur
     * @return string|false
     */
    public  function _url($cur){        
        return @filter_var($cur, FILTER_VALIDATE_URL);
    }
    
}


