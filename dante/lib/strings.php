<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function stripslashes_deep($value)
            {
                $value = is_array($value) ?
                            array_map('stripslashes_deep', $value) :
                            stripslashes($value);

                return $value;
            }

/**
 * Description of strings
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Dante_Lib_Strings {
    
    public static function processString($value) {
        /*if (is_array($value)) {
            foreach($value as $k=>$v) {
                if (is_string($v))
                $value[$k] = mysql_real_escape_string($v);
            }
        }
        return $value;*/
        if (get_magic_quotes_gpc()) {
            
            
            return stripslashes_deep($value);
        }
        
        return $value;
    }
    
    public static function isSerialized( $data ) {
        // if it isn't a string, it isn't serialized
        if ( !is_string( $data ) )
            return false;
        $data = trim( $data );
        
        if(
            stristr($data, '{' ) != false &&
            stristr($data, '}' ) != false &&
            stristr($data, ';' ) != false &&
            stristr($data, ':' ) != false
            ){
            return true;
        }else{
            return false;
        }
        
        
        if ( 'N;' == $data )
            return true;
        if ( !preg_match( '/^([adObis]):/', $data, $badions ) )
            return false;
        switch ( $badions[1] ) {
            case 'a' :
            case 'O' :
            case 's' :
                if ( preg_match( "/^{$badions[1]}:[0-9]+:.*[;}]\$/s", $data ) )
                    return true;
                break;
            case 'b' :
            case 'i' :
            case 'd' :
                if ( preg_match( "/^{$badions[1]}:[0-9.E-]+;\$/", $data ) )
                    return true;
                break;
        }
        return false;
    }
}

?>
