<?php

/**
 * Класс шаблонизатора
 */
class Dante_Lib_Template {
    
    # Переменные уровня PHP ( $this )
    protected $_vars = array();
    # Переменные уровня шаблона ( {$varname} )
    protected $_assigned_vars = array();

    function __construct() {
        $this->_vars['_uid'] = (int)Dante_Helper_App::getUid();
        $this->_vars['_url'] = Dante_Controller_Front::getRequest()->get_requested_url();
        $this->_vars['_host'] = Dante_Helper_App::getAppDomain();
    }
    
    /**
     * Записываем переменную
     * @var string $name имя пременной
     * @var mixed $value содержимое переменной
     */
    function __set($name, $value) {
        if(is_array($value) && $name=='_vars'){
            foreach($value as $k=>$v){
                $this->_vars[$k] = $v;
            }
        }else{
            $this->_vars[$name] = $value;
        }
    }
    
    /**
     * Получение переменной
     * @var string $name имя пременной
     */
    function __get($name) {
        return (isset($this->_vars[$name])) ? $this->_vars[$name] : null;
    }
    
    /**
     * Присваивание переменной в шаблон
     * @var string $key имя пременной
     * @var mixed $value содержимое переменной
     */
    public function assign_var($key,$value)
    { 
        $arr = array();
        $arr[$key] = $value;
        array_push($this->_assigned_vars, $arr);
    }
    
    /**
     * Присваивание массива из элементов
     * @var array $vars массив переменных вида ключ=значение
     */
    public function assign_vars($vars)
    {
        if(!empty($vars))
        {
            foreach($vars as $key=>$value)
            {
                $this->assign_var($key,$value);
            }
        }
    }
    
    /**
     * Обрабатываем содержимое контента и заменяем {переменные} их содержимым
     * @var string $content содержимое шаблона
     */
    private function insert_vars($content)
    {
        $assigned_vars = $this->_assigned_vars;
        if(!empty($assigned_vars))
        {
            foreach($assigned_vars as $vars)
            {
                $key = key($vars);
                $value = $vars[$key];
                $content = str_replace("{".$key."}", $value, $content);
            }
        }
        
        return $content;
    }
    
    /**
     * Выводим содержимое шаблона
     * @var string $fileName путь к файлу шаблона
     */
    public function draw($fileName)
    {
        if (!file_exists($fileName))
        {
            throw new Exception('Cant open file ' . $fileName);
        }
        
        ob_start();
        
        include $fileName;
        
        $contents = $this->insert_vars(ob_get_contents());
        
        ob_end_clean();
        
        return $contents;
    }
    
    public function translate($code) {
        return Module_Lang_Helper::translate($code);
    }
    
    public function getAppDomen() {
        return Dante_Helper_App::getAppDomain();
    }
}
?>