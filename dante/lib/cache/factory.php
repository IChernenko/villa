<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 2/18/14
 * Time: 11:23 AM
 * To change this template use File | Settings | File Templates.
 */

class Dante_Lib_Cache_Factory {

    public static function create() {
        return new Dante_Lib_Cache_Driver_Local();
    }
}