<?php
/*
 * Класс для загрузки файлов
 *
 * @author gshnaider
 */

class Dante_Lib_Include {
    
    public static function includeFile($path)
    {
        if (!file_exists(Dante_Lib_Config::get('enviroment.root_path').$path)) {
            throw new Exception('Cant find file '.Dante_Lib_Config::get('enviroment.root_path').$path);
        }
        
        include_once(Dante_Lib_Config::get('enviroment.root_path').$path);
    }
}
    
?>
