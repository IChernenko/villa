/**
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 * User: dorian
 * Date: 12/22/13
 * Time: 11:49 PM
 */
dante.validator = {

    errorMessage: false,

    validateIsEmpty: function(val, caption) {
        if (val.length == 0) {
            this.errorMessage = 'Поле '+caption+' должно быть заполнено';
            return false;
        }
        return true;
    },

    validateLogin: function(login) {
        if (!this.validateIsEmpty(login, 'Логин')) return false;

        if(!/^\w+$/.test(login))
        {
            this.errorMessage = 'В полях вида "логин" - только латинские буквы, цифры и знак подчеркивания';
            return false;
        }
        return true;
    },

    validateEmail: function(email) {
        if (!this.validateIsEmpty(email, 'Email')) return false;

        var pattern = /\b^[\w\.\-]+\@[a-z]+\.[a-z]{2,3}\b/i;
        if(!pattern.test(email))
        {
            this.errorMessage = 'Неправильно введен Email';
            return false;
        }
        return true;
    },

    validatePassword: function(password) {
        if(password.length < 5)
        {
            this.errorMessage = 'Минимальная длина пароля - 5 символов';
            return false;
        }
        return true;
    },

    validatePhone: function(phone) {
        if (!this.validateIsEmpty(phone, 'Телефон')) return false;

        var pattern = /^\+\d{12}/;
        if(!pattern.test(phone))
        {
            this.errorMessage = 'Неправильно введен телефон';
            return false;
        }
        return true;
    },

    validatePhoneNumber: function(phone)
    {
        if (!this.validateIsEmpty(phone, 'Телефон')) return false;

        var pattern = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;

        if(!pattern.test(phone))
        {
            this.errorMessage = 'Введите номер телефона в формате: [Код страны][Код оператора][Номер телефона]';
            return false;
        }
        return true;
    },



    /**
     * Функция проверяющая формат строки для значения времени в формате HH:MM
     * @param time проверяемая строка
     * @returns {boolean} возвращает true если строка time корректна и false если нет
     */
    validateTime: function(time)
    {
        if (!this.validateIsEmpty(time, 'Время')) return false;

        var pattern = /^([0-1]\d|2[0-3])(:[0-5]\d){1}$/;
        if(!pattern.test(time))
        {
            this.errorMessage = 'Неправильно введено время. ';
            return false;
        }
        return true;
    }
}