dante = {
    ajaxContentLoadStart: function(container) {
        if (container == undefined) container = '#content';
        $(container).html("<img src='../dante/i/ajax-loader.gif'/>");
    },
    setCookie: function(name, value, expires, path, domain, secure) {
        var curCookie = name + "=" + escape(value) +
                ((expires) ? "; expires=" + expires.toGMTString() : "") +
                ((path) ? "; path=" + path : "") +
                ((domain) ? "; domain=" + domain : "") +
                ((secure) ? "; secure" : "")
        if ((name + "=" + escape(value)).length <= 4000)
                document.cookie = curCookie
        else
                if (confirm("Cookie РїСЂРµРІС‹С€Р°РµС‚ 4KB Рё Р±СѓРґРµС‚ РІС‹СЂРµР·Р°РЅ !"))
                        document.cookie = curCookie
    },
    deleteCookie:function (name, path, domain){
        if (getCookie(name)) {
                document.cookie = name + "=" + 
                ((path) ? "; path=" + path : "") +
                ((domain) ? "; domain=" + domain : "") +
                "; expires=Thu, 01-Jan-70 00:00:01 GMT"
        }
    },
    getCookie:function (name){
            var prefix = name + "="
            var cookieStartIndex = document.cookie.indexOf(prefix)
            if (cookieStartIndex == -1)
                    return null
            var cookieEndIndex = document.cookie.indexOf(";", cookieStartIndex + prefix.length)
            if (cookieEndIndex == -1)
                    cookieEndIndex = document.cookie.length
            return unescape(document.cookie.substring(cookieStartIndex + prefix.length, cookieEndIndex))
    },

    /**
     *
     * @param {string} id
     */
    disableElem: function(id) {
        $(id).attr("disabled", true);
    },

    enableElem: function(id) {
        $(id).attr("disabled", false);
    },

    isValidEmail: function(email) {
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return filter.test(email);
    }
}
