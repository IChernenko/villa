<?php
/**
 * Хелпер для работы с workspace
 * User: dorian
 * Date: 19.04.12
 * Time: 15:45
 * To change this template use File | Settings | File Templates.
 */
class Dante_Helper_Workspace
{
    public static function getName()  {
        //$default = Dante_Helper_App::getWsName();
        $default = Dante_Lib_Config::get('app.ws');

        // если у нас определены правила для воркспейсов
        if (file_exists('conf/ws.php')) {
            include_once('conf/ws.php');

            $uid = Dante_Helper_App::getUid();
            if ($uid) {
                $mapper = new Module_User_Mapper();
                $user = $mapper->get($uid);
                
                if ($user) {
                    $wsName = Dante_Lib_Config::get('ws.'.$user->rating);
                    if ($wsName) return $wsName;
                }
            }
            else {
                // у  нас смотрит страницу гость
                $wsName = Dante_Lib_Config::get('ws.default');
                if ($wsName) return $wsName;
            }
        }

        return $default;
    }
}
