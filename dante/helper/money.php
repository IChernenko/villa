<?php
/**
 * Работа с денежными значениями
 * @author Elrafir
 */
class Dante_Helper_Money{
    
    /**
     * Округление до 2-х цифр после запятой
     * @param int $number тело округления
     * @param int $mode выбор константы типа округления.
     * @param bool $format Определяем нужно ли форматировать цену
     * <code>
     * константы =  array(
     *  1=>PHP_ROUND_HALF_UP	//Округление к большему целому
        2=>PHP_ROUND_HALF_DOWN	//Округление к меньшему целому
        3=>PHP_ROUND_HALF_EVEN	//Округление к четному числу
        4=>PHP_ROUND_HALF_ODD	//Округление к нечетному числу
     * );
     * </code>
     */
    public static function round($number, $mode=1, $format=FALSE){
        switch($mode){
            case 1:
                $result = round($number, 2, PHP_ROUND_HALF_UP);
                break;
            case 2:
                $result = round($number, 2, PHP_ROUND_HALF_DOWN);
                break;
            case 3:
                $result = round($number, 2, PHP_ROUND_HALF_EVEN);
                break;
            case 4:
                $result = round($number, 2, PHP_ROUND_HALF_ODD);
                break;
            default :
                $result = $number;
                break;
        }
        if($format){
            return Dante_Helper_Money::numberFormat($result);
        }else{
            return $result;
        }
        
    }
    
    public static function numberFormat($number, $decimals=2, $delimiter='.', $millenium=' '){
        return number_format($number, $decimals, $delimiter, $millenium);
    }
    
}
?>
