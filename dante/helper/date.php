<?php
/**
 * Помощник работы с датой
 * @package app
 * @author Elrafir
 */

class Dante_Helper_Date {
    /**
     * Предустановленные шаблоны для конвертирования дат
     * @var array 
     */
    public static $types = array(
            0=>'d/m/Y',
            1=>'H:i:s d/m/Y',
            2=>'d-m-Y',
            3=>'d.m.Y',
            4=>'H:i:s d.m.Y',
            5=>'d.m.Y G:i',
            6=>'m/d',
            7=>'d.m.Y H:i',
            8=>'m/d/Y',
            9=>'Y.m.d H:i:s',
            10=>'H:i',
            11=>'Y-m-d',
            12=>'m/Y',
            13=>'d-m-Y H:i'
        );

    /**
     * Ковертируем строку
     * @param string $time
     * @param int $now time
     * @param int|string $type - предустановленный шаблон даты (пока не работает)
     * @return int|false timestamp
     */
    public static function strToTime($time, $now=false, $type=false){
        if($now){
            return strtotime($time, $now);
        }
        return strtotime($time);                    
    }

    public static function strToTimestamp($date, $format=0) {
        $arr = date_parse_from_format(self::$types[$format], $date);

        return mktime(0, 0, 0, $arr['month'], $arr['day'], $arr['year']);
    }
    
    public static function strtotimef($stime, $format=''){        
        if(is_int($format) && isset(self::$types[$format])){
            $format = self::$types[$format];  
        }
        if( trim($format)=='' )return strtotime($stime);
        $artimer = array(
            'd'=>'([0-9]{2})',
            'j'=>'([0-9]{1,2})',
            'F'=>'([a-z]{3,10})',
            'm'=>'([0-9]{2})',
            'M'=>'([a-z]{3})',
            'n'=>'([0-9]{1,2})',
            'Y'=>'([0-9]{4})',
            'y'=>'([0-9]{2})',
            'i'=>'([0-9]{2})',
            's'=>'([0-9]{2})',
            'h'=>'([0-9]{2})',
            'H'=>'([0-9]{2})',
            '#'=>'\\#',
            ' '=>'\\s',
        );
        $arttoval = array(
            'j'=>'d',
            'f'=>'m',
            'n'=>'m',
        );
        $reg_format = '#'.strtr($format,$artimer).'#Uis';
        if( preg_match_all('#[djFmMnYyishH]#',$format,$list) and preg_match($reg_format,$stime,$list1) ){
            $item = array('h'=>'00','i'=>'00','s'=>'00','m'=>0,'d'=>1,'y'=>1970);
            foreach($list[0] as $key=>$valkey){
                if( !isset($arttoval[strtolower($valkey)]) )
                    $item[strtolower($valkey)] = $list1[$key+1];
                else
                    $item[$arttoval[strtolower($valkey)]] = $list1[$key+1];
            }      
            if((int)($item['m'])>0){
                return strtotime($item['y'].'-'.$item['m'].'-'.$item['d']);
            }else{
                $item['m'] = 1;
                return strtotime($item['h'].':'.$item['i'].':'.$item['s'].' '.$item['d'].'.'.$item['m'].'.'.$item['y']);            
            }
        }else return false;
    }


    /**
     * конвертируем дату из unixtimestamp в строку dateTime
     * @param type $date
     * @return type 
     */    
    public static function convertToDateTime($date)
    {
        return date("H:i:s d.m.Y", $date);
    }

    public static function convertToDate($date)
    {
        return date("H:i:s", $date);
    }

    /**
     * Конвертируем Дату по запрошенному типу с предустановленными паттернами. 
     * @param int $date дата в формате timestamp
     * @param int $type тип формата даты из предустановленного набора. Может быть задан в конфиге сайта с ключем datetime.type.0
     * @param string $format Не стандартный формат заданный в ручную. Пример "d числа m месяца Y года"
     * @example $types=array(
            0=>'d/m/Y',
            1=>'H:i:s d/m/Y',
            2=>'d-m-Y',
            3=>'d.m.Y',
            4=>'H:i:s d.m.Y',
            5=>'d.m.Y G:i',
            6=>'m/d',
            7=>'"d.m.Y H:i"',
        ); Предустановленные типы.
     * @example Dante_Lib_Config::get('datetime.type.999', 'd/m/Y'); Dante_Helper_Date::converToDateType(time(), 999); Динамическая предустановка типов.
     */
    public static function converToDateType($date=false, $type=0, $format=FALSE){
                
        if(!$format){            
            if(!$format=Dante_Lib_Config::get("datetime.type.$type", FALSE)){
                $format = self::$types[(int)$type];
            }
            
        }        
        if(!$date){
            return date($format);
        }
        return date($format, (int)$date);
    }
    
    /**
     * Возвращает дату в виде ассоциативного массива 
     * @param int $date - дата timestamp
     * @return array
     */
    public static function getDateArray($date = false)
    {
        if(!$date) $date = time();
        return getdate($date);
    }
    
    /**
     * Возвращает метку времени сегодняшней даты,
     * без часов, минут и секунд
     */
    public static function mktimeToday()
    {
        return mktime(0, 0, 0, date("n"), date("j"), date("Y"));
    }
    
    /**
     * Возвращает номер дня недели
     * @param int $timestamp - метка времени
     * @param boolean $defaultFormat - метка стандартного формата. 
     *          Если false - от 1 (понедельник) до 7 (воскресенье)
     *          Если true - от 0 (воскресенье) до 6 (суббота)
     */
    public static function getDayNum($timestamp, $defaultFormat = false)
    {
        $day = date("w", $timestamp);
        if(!$defaultFormat && $day == 0) $day = 7;
        
        return $day;
    }
}

?>
