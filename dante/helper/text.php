<?
class Dante_Helper_Text{
    
    /**
    * Лимит слов в строке
    *
    * Сокращает строку до количества указанных слов. Если сокращение имело место то ставит ... в конце либо то что указаннов посленем параметре.
    *
    * @access	public static
    * @param	string
    * @param	integer
    * @param	string	суффикс, который добавится после обрезанной строки. По умолчанию добавляется многоточие.
    * @return	string 
    */
    public static function word_limiter($str, $limit = 100, $end_char = '&#8230;'){
        if (trim($str) == ''){
            return $str;
        }
        preg_match('/^\s*+(?:\S++\s*+){1,'.(int) $limit.'}/', $str, $matches);
        if (strlen($str) == strlen($matches[0])){
            $end_char = '';
        }
        return rtrim($matches[0]).$end_char;
    }
    /**
    * Character Limiter
    *
    * Limits the string based on the character count.  Preserves complete words
    * so the character count may not be exactly as specified.
    *
    * @access	public static
    * @param	string
    * @param	integer
    * @param	string	суффикс, который добавится после обрезанной строки. По умолчанию добавляется многоточие.
    * @return	string
    */
    public static function character_limiter($str, $n = 500, $end_char = '&#8230;')
    {
            if (strlen($str) < $n)
            {
                    return $str;
            }

            $str = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $str));

            if (strlen($str) <= $n)
            {
                    return $str;
            }

            $out = "";
            foreach (explode(' ', trim($str)) as $val)
            {
                    $out .= $val.' ';

                    if (strlen($out) >= $n)
                    {
                            $out = trim($out);
                            return (strlen($out) == strlen($str)) ? $out : $out.$end_char;
                    }
            }
    }
        
    /**
    * Word Censoring Function
    *
    * Включает цензор слов во входящей строке.
    * Первым параметром принимает оригинальную строку. 
    * Второй параметр должен содержать массив слов, которые должны быть запрещены.
    * Третий (опциональный) параметр может содержать строку, которой будут заменены найденные слова. Если он не указан, то будет происходить замена на следующие символы: ####.
    *
    * @access	public static
    * @param	string	the text string
    * @param	string	the array of censoered words
    * @param	string	the optional replacement value
    * @return	string
    */
    public static function word_censor($str, $censored, $replacement = '')
    {
            if ( ! is_array($censored))
            {
                    return $str;
            }

            $str = ' '.$str.' ';

            // \w, \b and a few others do not match on a unicode character
            // set for performance reasons. As a result words like Гјber
            // will not match on a word boundary. Instead, we'll assume that
            // a bad word will be bookeneded by any of these characters.
            $delim = '[-_\'\"`(){}<>\[\]|!?@#%&,.:;^~*+=\/ 0-9\n\r\t]';

            foreach ($censored as $badword)
            {
                    if ($replacement != '')
                    {
                            $str = preg_replace("/({$delim})(".str_replace('\*', '\w*?', preg_quote($badword, '/')).")({$delim})/i", "\\1{$replacement}\\3", $str);
                    }
                    else
                    {
                            $str = preg_replace("/({$delim})(".str_replace('\*', '\w*?', preg_quote($badword, '/')).")({$delim})/ie", "'\\1'.str_repeat('#', strlen('\\2')).'\\3'", $str);
                    }
            }

            return trim($str);
    }

    
    /**
    * Code Highlighter
    *
    * Функция подсвечивает синтаксис в строке кода (PHP, HTML и т.д.).
    * Используется функция PHP highlight_string(), поэтому используемые цвета необходимо изменять в файле php.ini.
    * 
    * @access	public static
    * @param	string	the text string
    * @return	string
    */
    public static function highlight_code($str)
    {
            // The highlight string function encodes and highlights
            // brackets so we need them to start raw
            $str = str_replace(array('&lt;', '&gt;'), array('<', '>'), $str);

            // Replace any existing PHP tags to temporary markers so they don't accidentally
            // break the string out of PHP, and thus, thwart the highlighting.

            $str = str_replace(array('<?', '?>', '<%', '%>', '\\', '</script>'),
                                                    array('phptagopen', 'phptagclose', 'asptagopen', 'asptagclose', 'backslashtmp', 'scriptclose'), $str);

            // The highlight_string function requires that the text be surrounded
            // by PHP tags, which we will remove later
            $str = '<?php '.$str.' ?>'; // <?

            // All the magic happens here, baby!
            $str = highlight_string($str, TRUE);

            // Prior to PHP 5, the highligh function used icky <font> tags
            // so we'll replace them with <span> tags.

            if (abs(PHP_VERSION) < 5)
            {
                    $str = str_replace(array('<font ', '</font>'), array('<span ', '</span>'), $str);
                    $str = preg_replace('#color="(.*?)"#', 'style="color: \\1"', $str);
            }

            // Remove our artificially added PHP, and the syntax highlighting that came with it
            $str = preg_replace('/<span style="color: #([A-Z0-9]+)">&lt;\?php(&nbsp;| )/i', '<span style="color: #$1">', $str);
            $str = preg_replace('/(<span style="color: #[A-Z0-9]+">.*?)\?&gt;<\/span>\n<\/span>\n<\/code>/is', "$1</span>\n</span>\n</code>", $str);
            $str = preg_replace('/<span style="color: #[A-Z0-9]+"\><\/span>/i', '', $str);

            // Replace our markers back to PHP tags.
            $str = str_replace(array('phptagopen', 'phptagclose', 'asptagopen', 'asptagclose', 'backslashtmp', 'scriptclose'),
                                                    array('&lt;?', '?&gt;', '&lt;%', '%&gt;', '\\', '&lt;/script&gt;'), $str);

            return $str;
    }
    
    /**
     * Phrase Highlighter
     *
     * Выделяет подстроку в строке, окружая ее указанными символами.
     *
     * @access	public
     * @param	string	Первый параметр должен содержать оригинальную строку
     * @param	string	Вторым параметром необходимо указать строку для подсветки
     * @param	string	Открывающий HTML тег, которым вы хотите выделить подстроку.
     * @param	string	Закрывающий HTML тег, которым вы хотите выделить подстроку.
     * @return	string
     */
    public static function highlight_phrase($str, $phrase, $tag_open = '<strong>', $tag_close = '</strong>')
    {
            if ($str == '')
            {
                    return '';
            }

            if ($phrase != '')
            {
                    return preg_replace('/('.preg_quote($phrase, '/').')/i', $tag_open."\\1".$tag_close, $str);
            }

            return $str;
    }
    
    /**
     * Word Wrap
     *
     * Разбиение текста на строки по указанному количеству символов.
     * ПОКА НЕ РАБОТАЕТ С UTF-8
     *
     * @access	public static
     * @param	string	Строка
     * @param	integer	Количество символов в строке
     * @return	string
     */
    public static function word_wrap($str, $charlim = '76')
    {
            // Se the character limit
            if ( ! is_numeric($charlim))
                    $charlim = 76;

            // Reduce multiple spaces
            $str = preg_replace("| +|", " ", $str);

            // Standardize newlines
            if (strpos($str, "\r") !== FALSE)
            {
                    $str = str_replace(array("\r\n", "\r"), "\n", $str);
            }

            // If the current word is surrounded by {unwrap} tags we'll
            // strip the entire chunk and replace it with a marker.
            $unwrap = array();
            if (preg_match_all("|(\{unwrap\}.+?\{/unwrap\})|s", $str, $matches))
            {
                    for ($i = 0; $i < count($matches['0']); $i++)
                    {
                            $unwrap[] = $matches['1'][$i];
                            $str = str_replace($matches['1'][$i], "{{unwrapped".$i."}}", $str);
                    }
            }

            // Use PHP's native function to do the initial wordwrap.
            // We set the cut flag to FALSE so that any individual words that are
            // too long get left alone.  In the next step we'll deal with them.
            $str = wordwrap($str, $charlim, "\n", FALSE);

            // Split the string into individual lines of text and cycle through them
            $output = "";
            foreach (explode("\n", $str) as $line)
            {
                    // Is the line within the allowed character count?
                    // If so we'll join it to the output and continue
                    if (strlen($line) <= $charlim)
                    {
                            $output .= $line."\n";
                            continue;
                    }

                    $temp = '';
                    while ((strlen($line)) > $charlim)
                    {
                            // If the over-length word is a URL we won't wrap it
                            if (preg_match("!\[url.+\]|://|wwww.!", $line))
                            {
                                    break;
                            }

                            // Trim the word down
                            $temp .= substr($line, 0, $charlim-1);
                            $line = substr($line, $charlim-1);
                    }

                    // If $temp contains data it means we had to split up an over-length
                    // word into smaller chunks so we'll add it back to our current line
                    if ($temp != '')
                    {
                            $output .= $temp."\n".$line;
                    }
                    else
                    {
                            $output .= $line;
                    }

                    $output .= "\n";
            }

            // Put our markers back
            if (count($unwrap) > 0)
            {
                    foreach ($unwrap as $key => $val)
                    {
                            $output = str_replace("{{unwrapped".$key."}}", $val, $output);
                    }
            }

            // Remove the unwrap tags
            $output = str_replace(array('{unwrap}', '{/unwrap}'), '', $output);

            return $output;
    }
    /**
   * проверка строки, в utf8 ли она
   * 
   * @param string $s
   * @return bool 
   */ 
    public static function detect_my_utf($s)
    {
        $s=urlencode($s); // в некоторых случаях - лишняя операция (закоментируйте)
        $res='0';
        $j=strlen($s);

        $s2=strtoupper($s);
        $s2=str_replace("%D0",'',$s2);
        $s2=str_replace("%D1",'',$s2);
        $k=strlen($s2);

        $m=1;
        if ($k>0){
         $m=$j/$k;
         if (($m>1.2)&&($m<2.2)){ $res='1'; }
        }
        return $res;
    }
    
    /**
     * Преобразует первый символ строки в UTF-8 в верхний регистр
     * 
     * @param string $str
     * @return string
     */
    public static function utf_ucfirst($str) {
	$string_array = explode(' ', $str, 2);
	$string_array['0'] =  mb_convert_case($string_array['0'], MB_CASE_TITLE, "UTF-8");
	return implode(' ', $string_array);
    }
    
    /**
     * Преобразует в верхний регистр первый символ каждого слова в строке в UTF-8
     * @param string $str
     * @return string
     */
    public static function utf_ucwords($str){
            return mb_convert_case($str, MB_CASE_TITLE, "UTF-8");
    }
}