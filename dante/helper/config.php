<?php

class Dante_Helper_Config {
    public static function load() {
        include_once('conf/rules.php');

        $configFile = false;
        $env = getenv('env');
        if ($env) {
            $configFile = $env.'.php';
        }
        else {
            if (isset($_SERVER['HTTP_HOST'])) {
                $host = $_SERVER['HTTP_HOST'];
                $configFile = Dante_Lib_Config::get('host:'.$host);
            }
        }


        if ($configFile) {
            include_once('conf/config/'.$configFile);
        }
        else {
            include_once('conf/config/default.php');
        }
    }
}