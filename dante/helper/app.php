<?php
/**
 * Description of app
 *
 * @author Администратор
 */

class Dante_Helper_App {
    
    # Если файл, к которому обращаются, явно не указан - назначаем "manager.php" нашим "индексом"
    private static $_default_file = ''; //"_Controller";

    private static $_cache = false;
    
    public static function aliasToClassName($alias)
    {
        $data = explode('.', $alias);
        $dots_num = count($data);
        //var_dump($data);
        if ($dots_num > 0) {
            
            //if($dots_num > 1) self::$_default_file = "";
            
            foreach($data as $index=>$value) {
                $data[$index] = ucfirst($value);
            }

            //'Module_'.

            $class_name = implode('_', $data);
            //echo($class_name);
            // если у нас не встречается слово контроллер
            if (self::$_default_file != '')
            if (strpos($class_name, self::$_default_file) == 0) {
                $class_name .= self::$_default_file;
            }
            
            return $class_name;
        }

            // tmp code
        //return 'Dante'.self::$_default_file.'_'.ucfirst($alias);
        
        return 'App_'.ucfirst($alias).self::$_default_file;
    }

    public static function getWsName() {
        return Dante_Helper_Workspace::getName();
    }

    /**
     * Возвращает путь к текущему рабочему окружению.
     * Пример: template/default
     * @return string
     */
    public static function getWsPath() {
        return 'template/'.self::getWsName();
    }

    public static function addCss($css) {
        Dante_Lib_Registry::add('css', $css);
    }

    public static function addJs($js) {
        Dante_Lib_Registry::add('js', $js);
    }

    public static function getCss() {
        return Dante_Lib_Registry::get('css');
    }

    public static function getJs() {
        return Dante_Lib_Registry::get('js');
    }

    public static function getSid() {
        return Dante_Lib_Session::get('sid');
    }

    public static function getUid() {
        return Dante_Lib_Session::get('uid');
    }

    public static function getLogin() {
        $uid = self::getUid();
        if (!$uid) return '';
        $mapper = new Module_User_Mapper();
        $profile = $mapper->get($uid);
        if (!$profile) return '';
        return $profile->login;
    }
    
    /**
     * Получение объекта запроса
     * @return Dante_Lib_Request 
     */
    public static function getRequest() {
        return Dante_Controller_Front::getRequest();
    }
    
    public static function getAppDomain() {
        return Dante_Lib_Config::get('url.domain_main');
    }

    public static function getCache() {
        if (!self::$_cache) self::$_cache = Dante_Lib_Cache_Factory::create();
        return self::$_cache;
    }

    /**
     * Возвращает папку проекта
     * @return String
     */
    public static function getProjectFolder() {
        return Dante_Lib_Config::get('projectFolder');
    }
}

?>
