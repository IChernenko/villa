<?php



/**
 * Загрзучик JavaScript
 *
 * @author dorian
 */
class Dante_Controller_Js extends Dante_Controller_Base{

    /**
     * Определяет какие джаваскрипты нужно загружать
     * @global array $js
     * @return array
     */
    protected function _getJs() {
        /*$js = $this->_packJs();
        if (count($js)>0) return $js;*/
        
        
        $rootPath = 'template/'.Dante_Helper_App::getWsName().'/';

        $js = array();
        if (file_exists($rootPath.'js/js.php')) {
            include_once($rootPath.'js/js.php');
            global $js;
        }

        $jsPackage = Dante_Helper_App::getJs();
        if (is_array($jsPackage)) {
            //$js = array_merge($js, $jsPackage);
            foreach($js as $index=>$jsFileName) {
                $jsPackage[] = $jsFileName;
            }

            return $jsPackage;
        }
        //var_dump($js); die();
        return $js;
    }
    
    protected function _packJs() {
        // проверим а не ли у нас уже запомненной информации
        $js = array();
        if (file_exists('js/min.files.dmp')) {
            $js = unserialize( file_get_contents('js/min.files.dmp') );
        }
        if (file_exists('js/all.js')) {
            $js[] = 'js/all.js';
        }
        if (count($js)>0) return $js;
        
        
        /* */
        $rootPath = 'template/'.Dante_Helper_App::getWsName().'/';
        $js = array();
        if (file_exists($rootPath.'js/js.php')) {
            include_once($rootPath.'js/js.php');
            global $js;
        }
        $jsPackage = Dante_Helper_App::getJs();
        if (is_array($jsPackage)) $js = array_merge($js, $jsPackage);
        /* */
        
        
        
        
        // соберем как информацию о не обфусцированных файлах
        $minFiles = array();
        $jsFiles = array();
        foreach($js as $jsFile) {
            if (strstr($jsFile, '.min.')) {
                $minFiles[] = $jsFile;
            }
            else {
                $jsFiles[] = $jsFile;
            }
        }
        if (count($minFiles) > 0) {
            file_put_contents('js/min.files.dmp', serialize($minFiles));
        }
        if (count($jsFiles) > 0) {
            $content = '';
            foreach($jsFiles as $jsFile) {
                if (file_exists($jsFile))
                $content.= file_get_contents($jsFile);
            }
            /*
            include_once('../lib/jspacker/class.JavaScriptPacker.php');
            $packer = new JavaScriptPacker($content, 'Normal', true, false);
            $content = $packer->pack();*/
            
            file_put_contents('js/all.js', $content);
            $minFiles[] = 'js/all.js';
        }
        
        return $minFiles;
    }
    
    protected function _defaultAction()
    {
        $tpl = new Dante_Lib_Template();
        $tpl->items = $this->_getJs();
        return $tpl->draw('../dante/template/system/js.html');
    }
}

?>
