<?php

/**
 * Базовый класс контроллера
 *
 * @author Администратор
 */


Abstract class Dante_Controller_Base {
    
    /**
     *
     * @var Dante_Lib_Request
     */
    protected $_request = false;
    
    protected $_params = array();

    function __construct() {
        $this->_init();
    }

    /**
     * Инициализация контроллера
     */
    protected function _init() {
        
    }
    
    public function setParams($params) {
        $this->_params = $params;
    }

    /**
     * Получение значения параметра
     * @param string $paramName
     * @return mixed
     */
    protected function _getParam($paramName, $defalt=null) {
        return (isset($this->_params[$paramName])) ? $this->_params[$paramName] : $defalt;
    }
    
    /**
     *
     * @return Dante_Lib_Request 
     */
    protected function _getRequest() {
        return $this->_request;
    }
    
    public function setRequest($request) {
        $this->_request = $request;
    }


    protected function _defaultAction()
    {
        
    }
    
    public function run($action = false, $params=array())
    {
        if (!$action) {
            return $this->_defaultAction();
        }
        
        $methodName = '_'.$action.'Action';
        if (!method_exists($this, $methodName)) {
            throw new Exception('cant find method for action '.$action);
        }
        
        return $this->$methodName($params);
    }
}

?>