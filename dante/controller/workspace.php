<?php
/**
 * Контроллер построения рабочего окружения
 *
 * @author dorian
 */
class Dante_Controller_Workspace {
	
    /**
     * Получение контролеров для отображения в workspace
     * @return string 
     */
    protected static function _getControllers($wsPath)
    {
        if (!file_exists($wsPath.'controllers.php')) return array();
        
        include_once($wsPath.'controllers.php');
        global $wsControllers;
        return $wsControllers;
    }

    /**
     * Получить имя воркспейса
     * @static
     * @return Singleton
     */
    protected static function _getWsName() {
        $request = Dante_Controller_Front::getRequest();
        $wsName = $request->get('wsName');        
        if ($wsName) return $wsName;
        
        return Dante_Helper_Workspace::getName();
    }
	
    public static function run()
    {
        $tpl = new Dante_Lib_Template();

        $wsName = self::_getWsName();
        $wsPath = "template/{$wsName}/";
        $tpl->rootPath = $wsPath;

        // получаем список контроллеров для ws
        $controllers = self::_getControllers($wsPath);

        $request = Dante_Controller_Front::getRequest();

        foreach($controllers as $controllerAlias => $controllerName) {
            $action = false;
            $className = $controllerName;
            // если нам переданы расширенные параметры относительно запуска контроллера
            if (is_array($controllerName)) {
                if (isset($controllerName['class'])) $className = $controllerName['class'];
                if (isset($controllerName['action'])) $action = $controllerName['action'];
            }

            $currentController = new $className();
            $currentController->setRequest($request);
            $tpl->$controllerAlias = $currentController->run($action);
        }
        return $tpl->draw($wsPath.'index.html');
    }
}

?>
