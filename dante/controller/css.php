<?php



/**
 * Description of css
 *
 * @author dorian
 */
class Dante_Controller_Css extends Dante_Controller_Base{
	
    protected function _defaultAction()
    {
        $rootPath = 'template/'.Dante_Helper_App::getWsName().'/';
        
        $css = array();
        if (file_exists($rootPath.'css/css.php')) {
            include_once($rootPath.'css/css.php');
            global $css;
        }

        $cssPackage = Dante_Helper_App::getCss();
        if (is_array($cssPackage)) {
            //$css = array_merge($css, $cssPackage);
            foreach($css as $index=>$cssFileName) {
                $cssPackage[] = $cssFileName;
            }
            $css = $cssPackage;
        }
		
        $tpl = new Dante_Lib_Template();
        $tpl->css = $css; 
        
        return $tpl->draw('../dante/template/system/css.html');
    }
}

?>
