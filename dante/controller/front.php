<?php
/**
 * Фронт контроллер
 *
 * @author dorian
 */

class Dante_Controller_Front {

    /**
     * Объект запроса
     * @var Dante_Lib_Request 
     */
    protected static $_request = false;
    
    /**
     * Основной метод запуска фронт контроллера
     * @return type 
     */
    public static function run()
    {
        try {
            
            return Dante_Controller_Workspace::run();
        }
        catch (Exception $e) {
            // логируем ошибку
            writeErrorLog($e->getMessage().' line : '.$e->getLine().' file : '.$e->getFile()."\n".$e->getTraceAsString());
            return 'Something go wrong...';
        }
    }

    /**
     * Получение объекта запроса
     * @return Dante_Lib_Request 
     */
    public static function getRequest()
    {
        if (self::$_request) return self::$_request;
        
        $request = new Dante_Lib_Request();

        $url = (isset($_GET['url'])) ? $_GET['url'] : false;
        if ($url) {
            $params = explode('/', $url);
            if (count($params) > 0) {
                $request->setParams($params);
                $request->set('controller', $params[0]);
            }
        }
        
        self::$_request = $request;

        return $request;
    }
}

?>