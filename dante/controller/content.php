<?php
/**
 * Description of content
 *
 * @author Администратор
 */
class Dante_Controller_Content extends Dante_Controller_Base {
    
    protected function _getDefaultController()
    {
        $defaultController = Dante_Lib_Config::get('defaultController');
        if ($defaultController) return $defaultController;

        return 'module.home';
    }
	
    protected function _defaultAction()
    {
        $command = new Dante_Lib_Command();
        
        // Если в меню ничего не выбрано, устанавливаем первый пункт меню по умолчанию
        $command->_default_controller = $this->_getDefaultController();
        
        return $command->execute(Dante_Helper_App::getRequest());
    }
}

?>
