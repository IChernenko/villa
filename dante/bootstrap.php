<?php

/*
if (!isset($_SERVER['HTTP_REFERER'])) {
	echo("ups");
    die();
}*/

include_once('../dante/bootstrap.min.php');
Dante_Lib_Log_Factory::getLogger()->debug("bootstrap start");

Dante_Lib_Config::set('enviroment.template_path', 'template/'.Dante_Lib_Config::get('app.ws').'/');


include_once('conf/observers.php');

// подключаем правила для роутинга
$url = Dante_Controller_Front::getRequest()->get_requested_url();

// генерируем событие роутинга, чтобы на него смогли подписаться другие модули
Dante_Lib_Observer_Helper::fireEvent('route.start', $url);
if (file_exists('conf/route.php')) {
    include_once('conf/route.php');
}

// start session
if (!Dante_Lib_Session::get('sid')) {
    Dante_Lib_Session::set('sid', Module_Session_Service::generateSid());
}

// Подключаем пакеты с учетом их зависемостей
Dante_Lib_Log_Factory::getLogger()->debug("start include package");
$packageManager = new Dante_Lib_Package();
$packageManager->includePackage();
Dante_Lib_Log_Factory::getLogger()->debug("stop include package");

// кастомизация
if (file_exists('custom.php')) {
    //echo('include : '.$package['folder'].'/include.php');
    include_once('custom.php');
}

Dante_Lib_Log_Factory::getLogger()->debug("bootstrap end");
?>