<?php
global $package;
$package = array(
    'version' => '1',
    'name' => 'dante',
    'js' => array(
        //'../dante/js/jquery-1.8.3.min.js',
        '../dante/js/dante.js',
        '../dante/js/dante.validator.js'
    )
);