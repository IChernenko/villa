<?php

/**
 * Контроллер для слайдшоу
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Slideshow_Controller extends Dante_Controller_Base{
    
    protected function _getImages() {
        
        $typeUrl = Dante_Lib_Registry::get('hotel.typeLink');
        if ($typeUrl) {
            $mapper = new Module_Hotel_Mapper_Appartmenttypes();
            $model = $mapper->getByLink($typeUrl, Module_Lang_Helper::getCurrent());
            
            $images = array();
            foreach ($model->images as $src) {
                $images[] = array('src'=>$src, 'alt'=>'');
            }
            
            return $images;
        }
        
        
        $images = array();
        /*$images[] = array('src'=>'media/1.jpg', 'alt'=>'img');
        $images[] = array('src'=>'media/2.jpg', 'alt'=>'img2');
        $images[] = array('src'=>'media/3.jpg', 'alt'=>'img3');*/
        
        return $images;
    }
    
    protected function _defaultAction()
    {
        $tpl = new Dante_Lib_Template();
        $tpl->images = $this->_getImages();
        return $tpl->draw('../module/slideshow/template/slideshow.html');
    }
}

?>
