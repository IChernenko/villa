<?php
/**
 * Контроллер платежной системы a1
 * User: dorian
 * Date: 29.04.12
 * Time: 21:32
 * To change this template use File | Settings | File Templates.
 */
class Module_A1_Controller extends Dante_Controller_Base
{
    /**
     * Обработка успешной оплаты
     * @return string
     */
    protected function _successAction() {
        return 'scu';
    }

    protected function _processAction() {
        return 'process';
    }

    protected function _errorAction() {
        return 'error';
    }

    protected function _defaultAction() {
        $view = new Module_A1_View();
        $params = array();
        $params['key'] = 'HNLubluPz2bwk48ua+tl+6eQtmsxKeX85zqG5ttuFpw=';
        $params['name'] = 'Оплата за получение диеты';
        $params['amount'] = 1;
        $params['email'] = '';
        return $view->draw($params);
    }
}
