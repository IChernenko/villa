<?php
global $package;
$package = array(
    'version' => '1',
    'name' => 'module.a1',
    'dependence' => array(
        'lib.geoip'
    ),
    'js' => array(
        '../module/a1/js/a1.js'
    )
);