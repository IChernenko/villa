<?php

$url = Dante_Controller_Front::getRequest()->get_requested_url();

Dante_Lib_Router::addRule("/a1\/success\//", array(
    'controller' => 'Module_A1_Controller',
    'action' => 'success'
));

Dante_Lib_Router::addRule("/a1\/process\//", array(
    'controller' => 'Module_A1_Controller',
    'action' => 'process'
));

Dante_Lib_Router::addRule("/a1\/error\//", array(
    'controller' => 'Module_A1_Controller',
    'action' => 'error'
));


?>