<?php
/**
 * Маппер для смс
 * User: dorian
 * Date: 02.05.12
 * Time: 19:06
 * To change this template use File | Settings | File Templates.
 */
class Module_A1_Mapper_Sms
{
    public function add(Module_A1_Model_Sms $model) {
        $sql = "delete from sms_payments where smsid={$model->smsid}";
        Dante_Lib_SQL_DB::get_instance()->exec($sql);

        $table = new Dante_Lib_Orm_Table('sms_payments');
        $table->date =  $model->date;
        $table->msg = $model->msg;
        $table->msg_trans = $model->msg_trans;
        $table->operator_id = $model->operator_id;

        $table->country_id = $model->country_id;
        $table->operator = $model->operator;
        $table->user_id = $model->user_id;
        $table->smsid = $model->smsid;
        $table->num = $model->num;
        $table->cost = $model->cost;
        $table->test = $model->test;
        $table->try = $model->try;
        $table->cost_rur = $model->cost_rur;
        $table->ran = $model->ran;
        $table->skey = $model->skey;
        $table->sign = $model->sign;
        $table->access_code = $model->accessCode;
        $model->id = $table->insert();
    }

    /**
     * @param $accessCode
     * @param $smsCode
     * @return int || bool
     */
    public function getByAccesCode($accessCode, $smsCode) {
        $sql = "select id from sms_payments where msg='{$smsCode}' and access_code='{$accessCode}'";
        //ECHO($sql);
        $f = Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
        return (isset($f['id'])) ? $f['id'] : false;
    }
}
