<?php
/**
 * View для смс оплаты
 * User: dorian
 * Date: 03.05.12
 * Time: 19:25
 * To change this template use File | Settings | File Templates.
 */
class Module_A1_View_Sms
{
    /**
     * Возвращает javascript, который запускается сразу после отображения формы оплаты
     * @return string
     */
    protected function _getJs() {
        return "<script>a1LinkSmsBtn();</script>";
    }

    /**
     * @param $params array(
     *      'number'
     *      'code'
     *      'service'
     *      'countryCode'
     * )
     * @return mixed|string
     */
    public function draw($params) {
        $tpl = new Dante_Lib_Template();
        $tpl->number = $params['number'];
        $tpl->code = $params['code'];
        $tpl->service = $params['service'];

        if (isset($params['price'])) {
            $tpl->price = $params['price'];
        }


        $fileName = '../module/a1/tpl/sms.html';

        if (isset($params['countryCode'])) {
            $fileName = '../module/a1/tpl/sms.'.$params['countryCode'].'.html';
        }

        $html = $tpl->draw($fileName);

        if (!isset($params['js']) || $params['js']==false) {
            $html .= $this->_getJs();
        }
        else {
            $html .= $params['js'];
        }

        Dante_Lib_Log_Factory::getLogger()->debug("payment view html: ".$html);
        return $html;
    }
}
