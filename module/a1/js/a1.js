// a1

function a1LinkSmsBtn() {
    $("#smsCodeAccuire").click(function(){
        var code = $('#smsCode').val();
        if (code.length == 0) {
            $("#smsCodeResult").html("Введите код");
            return false;
        }

        $.ajax({
            type: "POST",
            url: '/ajax.php',
            data: {
                controller: "module.a1.controller.sms",
                action: "validate",
                code: code
            },
            success: function(data) {
                if (data.result == 0) {
                    $("#smsCodeResult").html("Ваш код введен не верно");
                }
                if (data.result == 1) {
                    $("#questionary").html("Ваш код введен верно");
                    window.location = "/test/result/";
                }
            }
        });
    });
}
