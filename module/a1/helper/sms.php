<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 04.05.12
 * Time: 12:00
 * To change this template use File | Settings | File Templates.
 */
class Module_A1_Helper_Sms
{
    public function validate($accessCode=false, $smsCode=false) {
        if (!$accessCode) {
            $accessCode = Dante_Lib_Session::get('accessCode');
            if (!$accessCode) return false;
        }

        if ($accessCode == Dante_Lib_Config::get('a1.testCode')) {
            return true;
        }


        // код, который человек отправлял через sms
        if (!$smsCode) {
            $smsCode = Dante_Lib_Session::get('smsCode');
            if (!$smsCode) return false;
        }

        $mapper = new Module_A1_Mapper_Sms();
        $result = $mapper->getByAccesCode($accessCode, $smsCode);

        return $result;
    }
}
