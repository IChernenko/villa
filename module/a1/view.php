<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 02.05.12
 * Time: 14:05
 * To change this template use File | Settings | File Templates.
 */
class Module_A1_View
{
    public function draw($params) {
        $tpl = new Dante_Lib_Template();
        $tpl->key = $params['key'];
        $tpl->name = $params['name'];
        $tpl->amount = $params['amount'];
        $tpl->email = $params['email'];
        return $tpl->draw('../module/a1/tpl/button.html');
    }
}
