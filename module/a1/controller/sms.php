<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 03.05.12
 * Time: 19:24
 * To change this template use File | Settings | File Templates.
 */
class Module_A1_Controller_Sms extends Dante_Controller_Base
{
    /**
     * Проверка кода доступа
     * @return array
     */
    protected function _validateAction() {
        //var_dump($_SESSION);
        // код доступа, полученный по смс
        $accessCode = $this->_getRequest()->get('code');

        // тестовый доступ
        if ($accessCode == Dante_Lib_Config::get('a1.testCode')) {
            // запоминаем код доступа, по сути даем пользователю оплаченную сессию
            Dante_Lib_Session::set('accessCode', $accessCode);
            return array('result' => 1);
        }

        $helper = new Module_A1_Helper_Sms();
        $id = $helper->validate($accessCode);

        $result = 0;
        // была проплата и сессия у пользователя та же
        if ($id>0) {
            // запоминаем код доступа, по сути даем пользователю оплаченную сессию
            Dante_Lib_Session::set('accessCode', $accessCode);
            $result = 1;
        }

        return array(
            'result' => $result
        );
    }
}
