<?php
/**
 * Сервис для работы с СМС
 * User: dorian
 * Date: 03.05.12
 * Time: 21:50
 * To change this template use File | Settings | File Templates.
 */
abstract class Module_A1_Service_Sms
{
    /**
     * Генерация номера телефона
     * @abstract
     *
     */
    protected abstract function _getNumber();

    /**
     * Генерация кода для отсылки по смс
     * @abstract
     *
     */
    protected abstract function _getCode();

    protected function _instanceView($params = false) {
        return new Module_A1_View_Sms($params);
    }

    /**
     * Определение кода страны
     * @return string
     */
    protected function _getCountryCode() {
        $ip = Dante_Lib_Server::get_remote_ip();

        if ($ip == '127.0.0.1') {
            return 'ua';
        }
        else {
            $helper = new Lib_Geoip_Helper();
            return strtolower($helper->getCountryCode($ip));
        }
    }

    /**
     * Отрисовка формы для отправки и ввода кода по смс.
     * @return mixed|string
     */
    public function draw($params=array()) {
        //Dante_Lib_Log_Factory::getLogger()->debug("view params ".var_export($params, 1));

        $js = false;
        $view = $this->_instanceView($params);
        $code = $this->_getCode();
        Dante_Lib_Session::set('smsCode', $code);

        if (isset($params['js'])) {
            $js = $params['js'];
        }

        $viewParams = array(
            'service' => 'Для получения результатов тестирования ',
            'number' => $this->_getNumber(),
            'code' => $code,
            'js' => $js
        );

        if (!isset($params['countryCode'])) {
            $params['countryCode'] = $this->_getCountryCode();
        }

        if (isset($params['countryCode'])) {
            $viewParams['countryCode'] = $params['countryCode'];
            if ($params['countryCode'] == 'ua') {
                $viewParams['number'] = Dante_Lib_Config::get('a1.number.ua');
                $viewParams['price'] = Dante_Lib_Config::get('a1.price.ua');
            }
        }

        return $view->draw($viewParams);
    }


}