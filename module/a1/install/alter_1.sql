
drop table if exists `sms_payments`;

CREATE TABLE `sms_payments` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `date` datetime NOT NULL COMMENT 'дата получения sms',
  `msg` varchar(255) NOT NULL COMMENT 'сообщение абонента',
  `msg_trans` varchar(255) NOT NULL COMMENT 'транслитерированное сообщение',
  `operator` varchar(32) NOT NULL COMMENT 'оператор',
  `operator_id` int(10) unsigned NOT NULL COMMENT 'id оператора',
  `user_id` bigint(20) unsigned NOT NULL COMMENT 'номер абонента',
  `country_id` int(6) unsigned NOT NULL COMMENT 'код страны',
  `smsid` int(10) unsigned NOT NULL COMMENT 'id смс',
  `cost_rur` float(15,4) unsigned NOT NULL COMMENT 'сумма в рублях',
  `cost` float(15,4) unsigned NOT NULL COMMENT 'информационный параметр (по курсу последней выплаты)',
  `test` tinyint(1) unsigned NOT NULL COMMENT '1 - тестовая смс',
  `try` int(10) NOT NULL COMMENT 'попытка',
  `num` varchar(20) NOT NULL COMMENT 'короткий номер',
  `skey` char(32) NOT NULL COMMENT 'секретный ключ md5',
  `sign` char(32) NOT NULL COMMENT 'md5 от параметров',
  `ran` tinyint(1) unsigned NOT NULL,
  `answer` text NOT NULL COMMENT 'ответ сервиса без заголовка',
  `access_code` varchar(32) NOT NULL COMMENT 'код доступа',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `smsid` (`smsid`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT="Платежи по смс";


