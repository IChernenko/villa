<?php



/**
 * Обработка заголовков приложения
 *
 * @author dorian
 */
class Module_Title_Controller extends Dante_Controller_Base{
    
    protected function _defaultAction()
    {
        $pageUrl = Dante_Lib_Config::get('publication.url');
        if ($pageUrl) {
            $lang = Module_Lang_Helper::getCurrent();

            $mapper = new Module_Publications_Mapper();
            $publication = $mapper->getByLink($pageUrl, $lang);
            if ($publication)  return $publication->page_title;
        }

        $model = new Module_Title_Model();
        Dante_Lib_Observer_Helper::fireEvent('drawTitle', $model);
        if ($model->title) return $model->title;

        return Dante_Lib_Config::get('app.title');
    }
}

?>
