<?php
/**
 * Проверка прав доступа к разделу.
 * @author Elrafir <elrafir@gmail.com>
 */
class Module_Acl_Controller extends Dante_Controller_Base { 
    
    /**
     * Допуск только для администратора
     * @param array $exceptions исключения в формате array('actionName'=>'methodName')
     * @return boolean
     */
    public static function accessAdmin($exceptions=false){
        if(self::accessFromAction($exceptions))return true;
        self::access(false, false, false, false);
    }
    /**
     * Допуск только для гендиректора и админа
     * @param array $exceptions исключения в формате array('actionName'=>'methodName')
     * @return boolean
     */
    public static function accessGendir($exceptions=false){
        if(self::accessFromAction($exceptions))return true;
        self::access(false, false, false);
    }
    /**
     * Допуск для модераторов и выше
     * @param array $exceptions исключения в формате array('actionName'=>'methodName')
     * @return boolean
     */
    public static function accessModer($exceptions=false){
        if(self::accessFromAction($exceptions))return true;
        self::access(false, false);
    }
    /**
     * Допуск для авторизованных
     * @param array $exceptions исключения в формате array('actionName'=>'methodName')
     * @return boolean
     */
    public static function accessUser($exceptions=false){
        if(self::accessFromAction($exceptions))return true;
        self::access(false, true);
    }
    /**
     * Допуск для всех
     * @param array $exceptions исключения в формате array('actionName'=>'methodName')
     * @return boolean
     */
    public static function accessGuest($exceptions=false){
        if(self::accessFromAction($exceptions))return true;
        self::access(true, true);
    }
    /**
     * Проверка не входит ли запрашиваемый action  в список исключеиний. Если да то руковоствуемся указанным правилом
     * @param type $exceptions
     * @return boolean
     */
    public static function accessFromAction($exceptions=false){
        
        if(!$exceptions) return false;
        $controller = new Dante_Lib_Request;        
        $action = $controller->get('action');
        if(isset($exceptions[$action])){
            eval("self::{$exceptions[$action]}()");
            return true;
        }
        //Совпадения в исключениях не найдены. Возвращаем false для того чтоб продолжилась проверка
        return false;
    }
    /**
     * Вытаскивает из сессии id вторизованного юзера есть таковой есть.
     * @return int|null
     */
    protected static function getUid(){
        return (int)Dante_Helper_App::getUid();
    }
    /**
     * Вытаскивает сокращённые данные юзера по известному id
     * @param int $uid
     * @return array
     */
    public static function getUser(){        
        if(!$uid    = self::getUid()) return false;
        $mapper     = new Module_Acl_Mapper;
        $user       = $mapper->getUser($uid);        
        return $user;
    }
    /**
     * Проверка прав доступа согласно заданным параметрам
     * @param boolean $guest
     * @param boolean $authorized
     * @param boolean $moder
     * @param boolean $admin
     */
    public static function access($guest=false, $authorized=false, $moder=true, $gendir=true){
        $user = self::getUser();
        if(!$guest && !$user){
            self::_denied('Страница доступна только авторизованным пользователям.');
        }
        if(!$authorized && $user['group_name'] !='moder' && $user['group_name'] !='admin' && $user['group_name'] !='gendir'){            
            self::_denied('Страница доступна только для модераторв.');
        }
        if(!$moder && $user['group_name'] !='admin' && $user['group_name'] !='gendir'){
            self::_denied('Страница доступна только для Генерального директора.');
        }
        if(!$gendir && $user['group_name'] !='admin'){
            self::_denied('Страница доступна только для Администратора.');
        }
    }
    /**
     * Метод выполняемый в случае если доступ к странице запрещён
     * @param string $message
     */
    protected static function _denied($message='access denied'){
        $message='Доступ запрещён. '.$message;
        die($message);
        //throw new Exception($message);
    }
}
?>