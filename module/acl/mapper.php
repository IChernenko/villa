<?
class Module_Acl_Mapper{
    
    protected $_tableUsers      = "[%%]users";
    protected $_tableGroups     = "[%%]groups";
    protected $_tableUserGroups = "[%%]user_groups";

    public function getUser($uid=false){
        if(!$uid) return false;
        $sql = "SELECT * FROM {$this->_tableUsers} as `u` ".
                "LEFT JOIN {$this->_tableUserGroups} as `ug` ON ug.user_id = u.id ".
                "LEFT JOIN {$this->_tableGroups} as `g` ON ug.group_id = g.group_id ".
                "WHERE u.id = {$uid} LIMIT 1";
        $user = Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
        
        return $user;
    }
}