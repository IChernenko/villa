<?php



/**
 * Класс для работы с платежной системой LiqPay.
 *
 * @author dorian
 */
class Module_Liqpay_Helper {
    
    protected $_merchantId;

    protected $_merchantSignature;

    function __construct() {
        $this->_merchantId = Dante_Lib_Config::get('liqpay.merchantId');
        $this->_merchantSignature = Dante_Lib_Config::get('liqpay.signature');
    }
    
    protected function _createOperationXml($orderId, $amount, $currency, 
            $description, $goodId ) {
        
        $payWay = 'card';

        $resultUrl = Dante_Lib_Config::get('liqpay.resultUrl');
        $serverUrl = Dante_Lib_Config::get('liqpay.serverUrl');

        $xml="<request>      
                  <version>1.2</version>
                  <merchant_id>$this->_merchantId</merchant_id>
                  <result_url>$resultUrl</result_url>
                  <server_url>$serverUrl</server_url>
                  <order_id>$orderId</order_id>
                  <amount>$amount</amount>
                  <currency>$currency</currency>
                  <description>$description</description>
                  <default_phone></default_phone>
                  <pay_way>$payWay</pay_way>
                  <goods_id>$goodId</goods_id>
            </request>";
        return $xml;
        
    }

    protected function _createSignature($xml) {
        return base64_encode(sha1($this->_merchantSignature.$xml.$this->_merchantSignature, 1));
    }

    /**
     * Рисует форму для оплаты
     *
     * @param $orderId
     * @param $amount
     * @param $currency
     * @param $description
     * @param $goodId
     * @return string
     */
    public function drawForm($orderId, $amount, $currency, $description, $goodId) {
        
        $xml = $this->_createOperationXml($orderId, $amount, $currency, $description, $goodId);


        // формируем сигнатуру
        $sign = $this->_createSignature($xml);

        // запаковываем операцию
        $xml_encoded = base64_encode($xml);

        
        $tpl = new Dante_Lib_Template();
        $tpl->operationXml = $xml_encoded;
        $tpl->signature = $sign;
        $tpl->orderId = $orderId;
        return $tpl->draw('../module/liqpay/tpl/form.html');
    }

    public function verify($xml, $signature) {
        // 1. Декодируем xml
        $xml_decoded = base64_decode($xml);
        Dante_Lib_Log_Factory::getLogger()->debug("xml decoded : $xml_decoded");

        // формируем сигнатуру
        $sign = $this->_createSignature($xml_decoded);
        Dante_Lib_Log_Factory::getLogger()->debug("our sign : $sign");
        // проверяем сигнатуры
        if ($sign != $signature) {
            return false;
        }

        return $xml_decoded;
    }
}

?>