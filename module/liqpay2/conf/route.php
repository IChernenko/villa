<?php

Dante_Lib_Router::addRule('/^\/liqpay2\/pay\/amt=([\d\.]*)_ccy=(.*)_descr=(.*)_ord=(\d*)$/', array(
    'controller' => 'Module_Liqpay2_Controller',
    'params' => array('urlSource' => true)
));

//Следующие 2 правила - до окончания тестирования
Dante_Lib_Router::addRule('/^\/liqpay2\/result$/', array(
    'controller' => 'Module_Liqpay2_Controller',
    'action' => 'result'
));

Dante_Lib_Router::addRule('/^\/liqpay2\/callback$/', array(
    'controller' => 'Module_Liqpay2_Controller',
    'action' => 'callback'
));
//

Dante_Lib_Router::addRule('/^\/manage\/liqpay2\//', array(
    'controller' => 'Module_Liqpay2_Manage_Controller'
));

Dante_Lib_Router::addRule('/^\/manage\/liqpay2-settings\/$/', array(
    'controller' => 'Module_Liqpay2_Manage_Settings_Controller'
));

?>
