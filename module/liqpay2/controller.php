<?php
/**
 * Description of controller: test class for Liqpay2 API
 *
 * @author Valkyria
 */
class Module_Liqpay2_Controller extends Dante_Controller_Base
{    
    protected $_public_key;
    protected $_private_key;
    
    protected $_result_url;
    protected $_server_url;
    
    protected $_test_mode;

    protected function _init() 
    {
        parent::_init();
        $this->_public_key = Module_Liqpay2_Helper::getPublicKey();
        $this->_private_key = Module_Liqpay2_Helper::getPrivateKey();
        $this->_result_url = Dante_Lib_Config::get('liqpay2.result_url');
        $this->_server_url = Dante_Lib_Config::get('liqpay2.server_url'); 
        $this->_test_mode = Dante_Lib_Config::get('liqpay2.test_mode', 0); 
    }

    protected function _defaultAction()
    {
        $params = new Module_Liqpay2_Model();
        if($this->_getParam('urlSource', false))
        {
            $params->amount = (float)$this->_getParam('%1');
            $params->currency = $this->_getParam('%2');
            $params->description = rawurldecode($this->_getParam('%3'));
            $params->order_id = (int)$this->_getParam('%4');
        }
        else
        {
            $params->amount = (float)$this->_getParam('amount');
            $params->currency = $this->_getParam('currency');
            $params->description = rawurldecode($this->_getParam('description'));
            $params->order_id = (int)$this->_getParam('order_id');
        }
        
        return $this->_getForm($params);
    }

    protected function _defaultAjaxAction()
    {
        $model = new Module_Liqpay2_Model();
        $model->amount = $this->_getRequest()->get_validate('amount', 'int', 0);
        $model->currency = $this->_getRequest()->get_validate('currency', 'text', '');
        $model->description = $this->_getRequest()->get_validate('description', 'text', '');
        $model->order_id = $this->_getRequest()->get_validate('order_id', 'int', 0);
        
        return array(
            'result' => 1,
            'html' => $this->_getForm($model)
        );
    }
    
    protected function _getForm(Module_Liqpay2_Model $params)
    {        
        if(!$this->_public_key)
            return 'Ошибка конфигурации. Не установлен публичный ключ';
        
        if(!$this->_private_key)
            return 'Ошибка конфигурации. Не установлен приватный ключ';
                
        $params->public_key = $this->_public_key;
        $params->private_key = $this->_private_key;
        $params->result_url = $this->_result_url;
        $params->server_url = $this->_server_url; 
        $params->sandbox = $this->_test_mode;
                
        if(!Module_Liqpay2_Helper::checkCurrency($params->currency))
            return 'Невозможно завершить операцию. Неверно установлена валюта.';
                
        $params->data = Module_Liqpay2_Helper::data($params);
        $params->signature = Module_Liqpay2_Helper::signature($params);
               
        $view = new Module_Liqpay2_View();
        return $view->draw($params);
    }

    protected function _callbackAction()
    {
        $model = new Module_Liqpay2_Manage_Model();
                
        $model->public_key = $this->_public_key;
        $model->private_key = $this->_private_key;
        
        $model->data = $this->_getRequest()->get('data');
        $model->signature = $this->_getRequest()->get('signature');
        
        $model = Module_Liqpay2_Helper::parseData($model);               
        
        if(Module_Liqpay2_Helper::checkSignature($model))
        {            
            $stateList = Module_Liqpay2_Helper::getStatusList();
            $model->status = (int)array_search($model->status, $stateList);
            $model->time = time();
            
            $mapper = new Module_Liqpay2_Mapper();
            $mapper->apply($model);
            
            return $model;
        }
        
        return false;
    }
    
    protected function _resultAction()
    {
        return '<p>Платеж проведен успешно</p>';
    }
}

?>
