<?php

define("LIQPAY_STATUS_SUCCESS", 1);
define("LIQPAY_STATUS_WAIT", 2);
define("LIQPAY_STATUS_ERROR", 3);

Dante_Lib_Config::set('liqpay2.result_url',  Dante_Lib_Config::get('url.domain_main').'/liqpay2/result');
Dante_Lib_Config::set('liqpay2.server_url',  Dante_Lib_Config::get('url.domain_main').'/liqpay2/callback');

// REQUIRED!!!
Dante_Lib_Config::set('liqpay2.public_key', '');
Dante_Lib_Config::set('liqpay2.private_key', '');

// Для тестового режима 
 Dante_Lib_Config::set('liqpay2.test_mode', 1);
 
 Dante_Lib_Config::set('liqpay2.order_link', false);


?>
