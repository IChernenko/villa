<?php
/**
 * Description of mapper
 *
 * @author Valkyria
 */
class Module_Liqpay2_Manage_Settings_Mapper 
{
    protected $_tableName = '[%%]liqpay_settings';
    
    public function get()
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $settings = $table->select(false)->getFields();
        return $settings ? $settings : array('public_key' => '', 'private_key' => '');
    }
    
    public function apply($public, $private)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $settingsExist = (boolean)$table->select(false)->getNumRows();
        
        $table->public_key = $public;
        $table->private_key = $private;
        
        if(!$settingsExist) $table->insert();
        else $table->update(false);
    }
}

?>
