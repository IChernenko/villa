<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of controller
 *
 * @author Valkyria
 */
class Module_Liqpay2_Manage_Settings_Controller extends Dante_Controller_Base
{   
    protected function _init() 
    {
        parent::_init();        
        
        $helper = new Module_Division_Helper();
        $helper->checkAccess();        
    }
    
    protected function _defaultAction() 
    {
        $mapper = new Module_Liqpay2_Manage_Settings_Mapper();
        $settings = $mapper->get();
        
        $view = new Module_Liqpay2_Manage_Settings_View();
        return $view->draw($settings);
    }
    
    protected function _saveAction()
    {
        $publicKey = $this->_getRequest()->get('public_key');
        $privateKey = $this->_getRequest()->get('private_key');
        
        if(!$publicKey || !$privateKey)
            return array(
                'result' => 0,
                'message' => 'Заполните все поля'
            );
        
        $mapper = new Module_Liqpay2_Manage_Settings_Mapper();
        $mapper->apply($publicKey, $privateKey);
        
        return array(
            'result' => 1,
            'html' => $this->_defaultAction()
        );
    }
}

?>
