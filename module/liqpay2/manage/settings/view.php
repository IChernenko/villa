<?php
/**
 * Description of view
 *
 * @author Valkyria
 */
class Module_Liqpay2_Manage_Settings_View 
{
    public function draw($settings)
    {
        $fileName = '../module/liqpay2/manage/template/settings.html';
        $tpl = new Dante_Lib_Template();
        $tpl->public_key = $settings['public_key'];
        $tpl->private_key = $settings['private_key'];
        
        return $tpl->draw($fileName);
    }
}

?>
