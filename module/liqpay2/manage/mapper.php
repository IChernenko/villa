<?php
/**
 * Description of mapper
 *
 * @author Valkyria
 */
class Module_Liqpay2_Manage_Mapper extends Module_Handbooksgenerator_Mapper_Base
{
    protected $_tableName = '[%%]liqpay_log';
    
    public function getById($id)
    {
        $model = new Module_Liqpay2_Manage_Model();
        
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->select(array('id' => $id));
        
        $model->id = $table->id;
        $model->public_key = $table->public_key;
        $model->amount = $table->amount;
        $model->sender_commission = $table->sender_commission;
        $model->receiver_commission = $table->receiver_commission;
        $model->agent_commission = $table->agent_commission;
        $model->currency = $table->currency;
        $model->description = $table->description;
        $model->order_id = $table->order_id;
        $model->liqpay_order_id = $table->liqpay_order_id;
        $model->type = $table->type;
        $model->transaction_id = $table->transaction_id;
        $model->sender_phone = $table->sender_phone;
        $model->status = $table->status;
        $model->time = $table->time;
        $model->signature = $table->signature;    
        
        return $model;
    }
}
