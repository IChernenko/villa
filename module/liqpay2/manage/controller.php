<?php
/**
 * Description of controller
 *
 * @author Valkyria
 */
class Module_Liqpay2_Manage_Controller extends Module_Handbooksgenerator_Controller_Base
{
    protected $_statusList;
    
    protected function _instanceMapper()
    {
        return new Module_Liqpay2_Manage_Mapper();
    }
    
    protected function _instanceView()
    {
        return new Module_Liqpay2_Manage_View();
    }
    
    protected function _processRedirection()
    {
        $url = $this->_getRequest()->get_requested_url();
        if(preg_match('/open\-(\d+)/', $url, $matches)) return $matches[1];
        
        return false;
    }

    protected function _drawAction()
    {
        $redirect = $this->_processRedirection();
        
        $mapper = $this->_instanceMapper();
        $model = new Module_Liqpay2_Manage_Model();
        
        $params = $this->getParams($model);
        $list = $mapper->getRowsByParams($params);
        $this->_statusList = Module_Liqpay2_Helper::getStatusList();
        
        $this->formEnable('module.liqpay2.manage.controller');
        $this->_setHead();
        $this->_setBody($list['rows']);
        
        $this->tfoot = array(
            array($this->paginator($list['count'], $params))      
        );
        
        $this->modal = true;
        
        if($redirect)
        {
            $this->_eval = array(
                '$(function(){handbooks.edit('.$redirect.')})'
            );
        }
        
        return $this->generate();
    }
    
    protected function _setHead()
    {        
        $titles = array(
            array(
                'content' => '#',
                'sortable' => 'id'
            ),
            array(
                'content' => 'Дата и время',
                'sortable' => 'time'
            ),
            array(
                'content' => 'Сумма',
                'sortable' => 'amount'
            ),
            array(
                'content' => 'Валюта',
            ),
            array(
                'content' => 'Описание',
                'sortable' => 'details'
            ),
            array(
                'content' => 'ID заказа',
                'sortable' => 'order_id'
            ),
            array(
                'content' => 'Тел. плательщика',
                'sortable' => 'sender_phone'
            ),
            array(
                'content' => 'Статус',
                'sortable' => 'status'
            ),
            array(
                'content' => 'Функции',
                'params' => array(
                    'style' => array(
                        'width' => '98px'
                    )
                )
            )
        );
        
        $filters = array(
            NULL,
            NULL,
            'amount' => array(
                'type' => 'text',
                'value' => $this->_getRequest()->get('amount')
            ),
            NULL,
            'description' => array(
                'type' => 'text',
                'value' => $this->_getRequest()->get('description')
            ),
            'order_id' => array(
                'type' => 'text',
                'value' => $this->_getRequest()->get('order_id')
            ),
            'sender_phone' => array(
                'type' => 'text',
                'value' => $this->_getRequest()->get('sender_phone')
            ),
            'status' => array(
                'type' => 'combobox',
                'items' => $this->_statusList,
                'value' => $this->_getRequest()->get('status')
            ),
            array(
                'type' => 'button_filter'
            )            
        );
        
        $this->thead = array(
            $titles, $filters
        );
    }
    
    protected function _setBody($rows)
    {
        foreach($rows as $row)
        {     
            $cur = array();
            $cur['id'] = $row['id'];
            $cur['time'] = Dante_Helper_Date::converToDateType($row['time'], 1);
            $cur['amount'] = $row['amount'];
            $cur['currency'] = $row['currency'];
            $cur['description'] = $row['description'];
            
            if(Module_Liqpay2_Helper::orderLinkEnabled())
            {
                $cur['order_id'] = '<a href="'.Module_Liqpay2_Helper::getOrderLink($row['order_id']).
                                    '" title="Открыть форму бронирования">'.$row['order_id'].'</a>';
            }
            else 
                $cur['order_id'] = $row['order_id'];
            
            $cur['sender_phone'] = $row['sender_phone'];
            $cur['status'] = $this->_statusList[$row['status']];
            $cur['buttons'] = '<a class="btn btn-mini btn-info" onclick="handbooks.edit('.
                    $row['id'].')">Подробнее</a>';
            
            $this->tbody[] = $cur;
        }
    }
    
    protected function _drawFormAction()
    {
        $id = $this->_getRequest()->get('id');        
        $model = $this->_instanceMapper()->getById($id);
        
        return array(
            'result' => 1,
            'html' => $this->_instanceView()->drawForm($model)
        );
    }
}

?>
