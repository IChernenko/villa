<?php
/**
 * Description of model
 *
 * @author Valkyria
 */
class Module_Liqpay2_Manage_Model extends Module_Liqpay2_Model
{    
    public $sortable_rules=array(
        'name' => 'time',
        'type' => 'desc',
    );
    
    public $filter_rules=array(
        array(
            'name' => 'amount',
            'format' => '`t1`.`%1$s` = "%2$s"'
        ),
        array(
            'name' => 'description',
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ),
        array(
            'name' => 'order_id',
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ),
        array(
            'name' => 'transaction_id',
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ),
        array(
            'name' => 'sender_phone',
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ),
        array(
            'name' => 'status',
            'format' => '`t1`.`%1$s` = "%2$s"'
        ),
        array(
            'name' => 'date',
            'format' => 'date'
        ),
        array(
            'name' => 'signature',
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        )
    );
        
    public $time;
    
    public $transaction_id;
    
    public $status;
        
    public $sender_phone;
    
    public $liqpay_order_id;
    
    public $sender_commission;
    
    public $receiver_commission;
    
    public $agent_commission;
}

?>
