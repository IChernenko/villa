<?php

/**
 * Description of view
 *
 * @author Valkyria
 */
class Module_Liqpay2_Manage_View 
{
    public function drawForm(Module_Liqpay2_Manage_Model $model)
    {
        $statusList = Module_Liqpay2_Helper::getStatusList();
        $model->status = $statusList[$model->status];
        $model->time = Dante_Helper_Date::converToDateType($model->time, 1);
        
        $fileName = '../module/liqpay2/manage/template/form.html';
        
        $tpl = new Dante_Lib_Template();
        $tpl->model = $model;
        
        return $tpl->draw($fileName);
    }
}
