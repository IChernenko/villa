<?php
/**
 * Description of helper
 *
 * @author Valkyria
 */
class Module_Liqpay2_Helper 
{
    protected static $_publicKey;
    protected static $_privateKey;
    
    public static function getPublicKey()
    {
        if(!self::$_publicKey) self::_setKeys();
        
        return self::$_publicKey;
    }
    
    public static function getPrivateKey()
    {        
        if(!self::$_privateKey) self::_setKeys();
        
        return self::$_privateKey;
    }
    
    public static function orderLinkEnabled()
    {
        return (boolean)Dante_Lib_Config::get('liqpay2.order_link');
    }
    
    public static function getOrderLink($orderId)
    {
        return str_ireplace('{id}', $orderId, Dante_Lib_Config::get('liqpay2.order_link'));
    }

    protected static function _setKeys()
    {
        $keys = self::_instanceMapper()->get();
        self::$_publicKey = $keys['public_key'];
        self::$_privateKey = $keys['private_key'];
    }
    
    protected static function _instanceMapper()
    {
        return new Module_Liqpay2_Manage_Settings_Mapper();
    }

    public static function getStatusList()
    {
        $list = array(
            1 => 'success',
            2 => 'failure',
            3 => 'wait_secure',
            4 => 'wait_accept',
            5 => 'wait_lc',
            6 => 'processing',
            7 => 'sandbox',
            8 => 'reversed' 
        );
        return $list;
    }
    
    public static function getStatusType($statusId)
    {
        switch($statusId) 
        {
            case 1:
            case 7:
                $type = LIQPAY_STATUS_SUCCESS;
                break;
            
            case 2:
                $type = LIQPAY_STATUS_ERROR;
                break;
            
            case 3:
            case 4:
            case 5:
            case 6:
                $type = LIQPAY_STATUS_WAIT;
                break;
            
            default:
                $type = LIQPAY_STATUS_ERROR;
                break;
        }
        
        return $type;
    }
    
    public static function checkCurrency($code)
    {
        $list = array('USD', 'EUR', 'RUB', 'UAH', 'GEL');
        if(in_array($code, $list)) return true;
        else return false;
    }
    
    public static function data(Module_Liqpay2_Model $model)
    {
        $params = array(
            'version' => $model->version,
            'public_key' => $model->public_key,
            'amount' => $model->amount,
            'currency' => $model->currency,
            'description' => $model->description,
            'order_id' => self::uniqueValue($model->order_id),
            'type' => $model->type,
            'server_url' => $model->server_url,
            'result_url' => $model->result_url,
            'pay_way' => $model->pay_way,
            'language' => $model->language,
            'sandbox' => $model->sandbox
        );
             
        return self::dataEncode($params);
    }
    
    public static function dataEncode($dataObj)
    {
        return base64_encode(json_encode($dataObj));
    }
    
    public static function dataDecode($dataStr)
    {
        return json_decode(base64_decode($dataStr));
    }
    
    public static function uniqueValue($value)
    {
        return $value.'_'.time();
    }
    
    public static function parseUnique($value)
    {
        return substr($value, 0, strpos($value, "_"));
    }
    
    public static function signature(Module_Liqpay2_Model $model)
    {
        if(!$model->data) $model->data = self::data($model);
        
        $signature = self::_encrypt($model->private_key.$model->data.$model->private_key);

        return $signature;
    }
    
    public static function checkSignature(Module_Liqpay2_Manage_Model $model)
    {
        $signatureTest = self::_encrypt($model->private_key.$model->data.$model->private_key);
        
        if($signatureTest == $model->signature)
            return true;
        else
            return false;
    }
    
    private static function _encrypt($str)
    {
        return base64_encode(sha1($str,1));
    }    

    public static function parseData(Module_Liqpay2_Manage_Model $model)
    {
        $params = self::dataDecode($model->data);
        
        $model->version = $params->version;
        $model->public_key = $params->public_key;
        $model->amount = $params->amount;
        $model->sender_commission = $params->sender_commission;
        $model->receiver_commission = $params->receiver_commission;
        $model->agent_commission = $params->agent_commission;
        $model->currency = $params->currency;
        $model->description = $params->description;
        $model->order_id = self::parseUnique($params->order_id);
        $model->liqpay_order_id = $params->liqpay_order_id;
        $model->type = $params->type;
        $model->status = $params->status;
        $model->transaction_id = $params->transaction_id;
        $model->sender_phone = $params->sender_phone;
             
        return $model;
    }
}

?>
