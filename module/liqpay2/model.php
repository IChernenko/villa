<?php
/**
 * Description of model
 *
 * @author Valkyria
 */
class Module_Liqpay2_Model 
{    
    public $version = 3;

    public $public_key;
    
    public $private_key;
    
    public $amount;
    
    public $currency;
    
    public $description;
    
    public $order_id;
    
    public $type = 'buy'; //пока что будет один использоваться один тип
    
    public $pay_way = 'card'; //тут тоже
    
    public $language;
    
    public $sandbox = 0;
    
    public $result_url;
    
    public $server_url;
    
    public $signature;
    
    public $data;
}

?>
