ALTER TABLE `[%%]liqpay_log` DROP COLUMN `date`;

ALTER TABLE `[%%]liqpay_log` ADD COLUMN `liqpay_order_id` VARCHAR(50) NOT NULL AFTER `order_id`;
ALTER TABLE `[%%]liqpay_log` ADD COLUMN `agent_commission` DECIMAL(10,2) NOT NULL DEFAULT 0 AFTER `amount`;
ALTER TABLE `[%%]liqpay_log` ADD COLUMN `receiver_commission` DECIMAL(10,2) NOT NULL DEFAULT 0 AFTER `amount`;
ALTER TABLE `[%%]liqpay_log` ADD COLUMN `sender_commission` DECIMAL(10,2) NOT NULL DEFAULT 0 AFTER `amount`;
