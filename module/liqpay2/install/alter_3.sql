CREATE TABLE `[%%]liqpay_settings` (
    `public_key` VARCHAR(100) NOT NULL,
    `private_key` VARCHAR(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;