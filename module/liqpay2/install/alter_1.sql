CREATE TABLE IF NOT EXISTS `[%%]liqpay_log` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `public_key` VARCHAR(100) NOT NULL,
    `amount` DECIMAL(10,2) NOT NULL,
    `currency` VARCHAR(10) NOT NULL,
    `description` VARCHAR(100) NOT NULL,
    `order_id` INT(11) NOT NULL,
    `type` VARCHAR(10) NOT NULL DEFAULT 'buy',
    `transaction_id` INT(11) NOT NULL,
    `sender_phone` VARCHAR(100) NOT NULL,
    `status` INT(11) NOT NULL,
    `date` INT(11) NOT NULL,
    `time` INT(11) NOT NULL,
    `signature` VARCHAR(200) NOT NULL,   
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 comment 'История платежей LiqPay';

ALTER TABLE `[%%]liqpay_log` ADD INDEX `transaction_id` (`transaction_id`);