<?php
/**
 * Description of view
 *
 * @author Valkyria
 */
class Module_Liqpay2_View 
{
    public function draw($params)
    {
        $tpl = new Dante_Lib_Template();
        $tpl->params = $params;        
        $fileName = '../module/liqpay2/template/form.html';
        
        $customTpl = Dante_Helper_App::getWsPath().'/liqpay2/form.html';
        if (file_exists($customTpl)) $fileName = $customTpl;
        
        return $tpl->draw($fileName);
    }
}

?>
