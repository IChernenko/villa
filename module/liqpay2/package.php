<?php

global $package;
$package = array(
    'version' => '5',
    'name' => 'module.liqpay2',
    'description' => 'Поддержка LiqPay',
    'dependence' => array(
    ),
    'js' => array(
        '../module/liqpay2/js/settings.js'
    )
);

?>
