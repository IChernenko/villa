<?php
/**
 * Description of mapper
 *
 * @author Valkyria
 */
class Module_Liqpay2_Mapper
{
    protected $_tableName = '[%%]liqpay_log';
    
    public function apply(Module_Liqpay2_Manage_Model $model)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->public_key = $model->public_key;
        $table->amount = $model->amount;
        $table->sender_commission = $model->sender_commission;
        $table->receiver_commission = $model->receiver_commission;
        $table->agent_commission = $model->agent_commission;
        $table->currency = $model->currency;
        $table->description = $model->description;
        $table->order_id = $model->order_id;
        $table->liqpay_order_id = $model->liqpay_order_id;
        $table->type = $model->type;
        $table->transaction_id = $model->transaction_id;
        $table->sender_phone = $model->sender_phone;
        $table->status = $model->status;
        $table->time = $model->time;
        $table->signature = $model->signature;
        
        $cond = array(
            'transaction_id' => $model->transaction_id
        );
        
        if($table->exist($cond)) 
        {
            $table->update($cond);
            $model->id = $table->select($cond)->id;
        }
        else 
            $model->id = $table->insert();
    }
    
    public function getStatusByOrder($orderId)
    {
        $sql = "SELECT `status` FROM {$this->_tableName} WHERE `order_id` = {$orderId}
                ORDER BY `time` DESC LIMIT 1";
        return Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'status');
    }
}

?>
