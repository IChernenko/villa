liqpay_settings = {
    controller: 'module.liqpay2.manage.settings.controller',
    formEnable: function()
    {
        $('#liqpaySettingsTable').find('input').prop('disabled', false);
    },
    formDisable: function()
    {
        $('#liqpaySettingsTable').find('input').prop('disabled', true);
    },
    btnEnable: function()
    {
        $('#liqpaySettingsTable').find('a').removeClass('disabled');
    },
    btnDisable: function()
    {
        $('#liqpaySettingsTable').find('a').addClass('disabled');
    },
    edit: function()
    {
        this.formEnable();
        $('#btnEdit').hide();
        $('#btnSave, #btnCancel').show();
    },
    cancelEdit: function()
    {        
        $('#liqpaySettingsTable').find('input[type="text"]').each(function(){
            $(this).val($(this).siblings('.prev-value').val());
        });
        
        this.formDisable();
        $('#btnEdit').show();
        $('#btnSave, #btnCancel').hide();
    },
    save: function()
    {
        if(!this.validate()) return;
        this.btnDisable();
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            url: 'ajax.php',
            data: {
                controller: liqpay_settings.controller,
                action: 'save',
                public_key: $('#publicKey').val(),
                private_key: $('#privateKey').val()
            },
            success: function(data)
            {
                liqpay_settings.btnEnable();
                if(data.result == 1)
                {
                    $('#liqpaySettingsTable').replaceWith(data.html);
                }
                else
                {
                    jAlert(data.message, 'Ошибка');
                }
            }
        });
    },
    validate: function()
    {
        var inputs = $('#liqpaySettingsTable').find('input[type="text"]');
        for(var i=0; i<inputs.length; i++)
        {
            if(!inputs.eq(i).val())
            {
                jAlert('Заполните все поля', 'Ошибка', function(){
                    inputs.eq(i).focus();
                });
                return false;
            }
        }
        
        return true;
    }
}


