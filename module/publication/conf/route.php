<?php

$url = Dante_Controller_Front::getRequest()->get_requested_url();

Dante_Lib_Router::addRule('/page\/(.*)\//', array(
    'controller' => 'Module_Publication_Controller'
    //'params' => array('page'=>'%1')
));

Dante_Lib_Router::addRule('/page\/(.*)\.html/', array(
    'controller' => 'Module_Publication_Controller'
    //'params' => array('page'=>'%1')
));