<?php

/**
 * Контроллер отображения публикаций
 * @author Sergey Suzdaltsev sergey.suzdaltsev@gmail.com
 */
class Module_Publication_Controller extends Dante_Controller_Base {

    protected function _getPage() {
        $page = $this->_getParam('page');
        if (!$page) {
            $page = $this->_getRequest()->get('page');
        }

        if (!$page) {
            $page = $this->_getParam('%1');
        }

        return $page;
    }

    /**
     * Проверка прав на отображение публикации
     * @return bool
     */
    protected function _checkPermission($page) {
        return true;
    }

    protected function _noPermission($page) {
        return '';
    }

    protected function _getPagePath() {
        return 'page/';
    }

    protected function _defaultAction() {
        $page = $this->_getPage();

        if (!$this->_checkPermission($page)) {
            return $this->_noPermission($page);
        }

        if ($page) {
            return file_get_contents($this->_getPagePath()."{$page}.html");
        }
        return file_get_contents('page/news.html');
    }
}

?>