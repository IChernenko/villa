<?php
/**
 * Работа с Keywords
 * @author Dorian
 */

class Module_Seo_Controller_Keywords extends Dante_Controller_Base {

    /**
     * По дефолту вызвавшему возвращаем значение  Keywords для страницы
     * @return string
     */
	protected function _defaultAction() {

//        $model = new Module_Seo_Model();
//        Dante_Lib_Observer_Helper::fireEvent('seo.draw.keywords', $model);
//        if ($model->keywords) return $model->keywords;


        $lang = Module_Lang_Helper::getCurrent();

        $pieces = explode("/", $_SERVER['REQUEST_URI']);
        $urlLenght = count($pieces) - 2;
        $lang_size = strlen($pieces[$urlLenght]);
        //about page
        if ( $pieces[$urlLenght] == 'about-page' ) {
            $pageMapper = new Villa_Module_About_Mapper();
            $pageData = $pageMapper->getEntry($lang);
            return $pageData['seoKeywords'];
        }

        // main page
        if ( $pieces[$urlLenght] == '' || $lang_size == 2 || $lang_size == 3 ) {
            $pageMapper = new Villa_Module_Homepage_Mapper();
            $pageData = $pageMapper->getEntry($lang);
            return $pageData['seoKeywords'];
        }
        //contact page
        if ( $pieces[$urlLenght] == 'contacts'  ) {
            $pageMapper = new Villa_Module_Contacts_Mapper();
            $pageData = $pageMapper->get($lang);
            return $pageData->seo_keywords;
        }

        if ( $pieces[$urlLenght] == 'numbers'  ) {
            $pageMapper = new Villa_Module_About_Mapper();
            $pageData = $pageMapper->getNumberSeo($lang);
            return $pageData->keywords;
        }

        return Dante_Lib_Config::get('app.keywords');
	}
}

?>