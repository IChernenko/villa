<?php
/**
 * Работа с Title
 * @author Dorian
 */
class Module_Seo_Controller_Title extends Dante_Controller_Base {

    /**
     * По дефолту вызвавшему возвращаем значение  Title для страницы
     * @return string
     */
    protected function _defaultAction() {
//        $pageUrl = Dante_Lib_Config::get('publication.url');
//        if ($pageUrl) {
//            $lang = Module_Lang_Helper::getCurrent();
//
//            $mapper = new Module_Publications_Mapper();
//            $publication = $mapper->getByLink($pageUrl, $lang);
////            var_dump($publication);
//            if ($publication)  return $publication->page_title;
//        }
//
//        $model = new Module_Seo_Model();
//        Dante_Lib_Observer_Helper::fireEvent('seo.draw.title', $model);
//        if ($model->title) return $model->title;
//
//        return Dante_Lib_Config::get('app.title');






        $lang = Module_Lang_Helper::getCurrent();

        $pieces = explode("/", $_SERVER['REQUEST_URI']);
        $urlLenght = count($pieces) - 2;
        $lang_size = strlen($pieces[$urlLenght]);

        //about page
        if ( $pieces[$urlLenght] == 'about-page' ) {
            $pageMapper = new Villa_Module_About_Mapper();
            $pageData = $pageMapper->getEntry($lang);
            return $pageData['seoTitle'];
        }

        // main page
        if ( $pieces[$urlLenght] == '' || $lang_size == 2 || $lang_size == 3 ) {
            $pageMapper = new Villa_Module_Homepage_Mapper();
            $pageData = $pageMapper->getEntry($lang);
            return $pageData['seoTitle'];
        }
        //contact page
        if ( $pieces[$urlLenght] == 'contacts'  ) {
            $pageMapper = new Villa_Module_Contacts_Mapper();
            $pageData = $pageMapper->get($lang);
            return $pageData->seo_title;
        }

        if ( $pieces[$urlLenght] == 'numbers'  ) {
            $pageMapper = new Villa_Module_About_Mapper();
            $pageData = $pageMapper->getNumberSeo($lang);
            return $pageData->title;
        }
        return Dante_Lib_Config::get('app.title');



//        ob_start();
//        var_dump($pageData->seo_title);
//        $result = ob_get_clean();
//        return $result;
    }
}

?>