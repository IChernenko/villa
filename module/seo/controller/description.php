<?php

/**
 * Работа с Description
 * @author Dorian
 */
class Module_Seo_Controller_Description extends Dante_Controller_Base {

     /**
     * По дефолту вызвавшему возвращаем значение  Description для страницы
     * @return string
     */
	protected function _defaultAction() {


//        $model = new Module_Seo_Model();
//        Dante_Lib_Observer_Helper::fireEvent('seo.draw.description', $model);
//        if ($model->description) return $model->description;


        $lang = Module_Lang_Helper::getCurrent();

        $pieces = explode("/", $_SERVER['REQUEST_URI']);
        $urlLenght = count($pieces) - 2;
        $lang_size = strlen($pieces[$urlLenght]);
        //about page
        if ( $pieces[$urlLenght] == 'about-page' ) {
            $pageMapper = new Villa_Module_About_Mapper();
            $pageData = $pageMapper->getEntry($lang);
            return $pageData['seoDescription'];
        }

        // main page
        if ( $pieces[$urlLenght] == '' || $lang_size == 2 || $lang_size == 3 ) {
            $pageMapper = new Villa_Module_Homepage_Mapper();
            $pageData = $pageMapper->getEntry($lang);
            return $pageData['seoDescription'];
        }
        //contact page
        if ( $pieces[$urlLenght] == 'contacts'  ) {
            $pageMapper = new Villa_Module_Contacts_Mapper();
            $pageData = $pageMapper->get($lang);
            return $pageData->seo_description;
        }

        if ( $pieces[$urlLenght] == 'numbers'  ) {
            $pageMapper = new Villa_Module_About_Mapper();
            $pageData = $pageMapper->getNumberSeo($lang);
            return $pageData->description;
        }


        return Dante_Lib_Config::get('app.description');
	}
}
?>