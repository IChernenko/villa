<?php

/**
 * Description of controller
 *
 * @author Mort
 */

class Module_Config_Controller extends Dante_Controller_Base{
    
    protected function _defaultAction()
    {
        return array(
            'result' => '1'
        );
    }
    
    protected function _getUploadUrlAction()
    {
        $uploadFolder = Dante_Lib_Config::get('app.uploadFolderFullPath');
        Component_Vcl_Checkpath::check($uploadFolder);
        
        return array(
            'result' => '1',
            'uploadFolder' => $uploadFolder
        );
    }
}

?>
