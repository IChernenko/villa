languageJS = {
    switchMe: function(code) {
        $.ajax({
            type: "POST",
            url: '/ajax.php',
            data: {
                controller: "module.lang.controller",
                action: 'change',
                code: code,
                url: location.pathname
            },
            success: function(data) {
                if (data.result == 1) {
                    location.replace(data.url);
                    //$("#content").html(data.template);
                }
            }
        });
    },
    setDefault: function(obj)
    {
        if($(obj).hasClass('disabled')) return false;
        $('#editLanguageForm a').addClass('disabled');
        
        $.ajax({
            data: {
                controller: 'module.lang.manage.controller',
                action: 'setDefault',
                id: $('#editLanguageForm input[name="id"]').val()
            },
            url: '/ajax.php',
            dataType : "json",
            success: function (data){
                if(data.result == 1)
                {
                    jAlert('Язык установлен по умолчанию', 'Сообщение');
                    $('#editLanguageForm a').removeClass('disabled');
                }
            }
        });
    },
    editSubmit: function(obj)
    {
        if($(obj).hasClass('disabled')) return false;
        $(obj).addClass('disabled');
        
        var data=$('#editLanguageForm').serializeArray();
        data.push(
            {name: 'controller', value: 'module.lang.manage.controller'},
            {name: 'action', value: 'edit'}
        );
        
        $.ajax({
            type: 'POST',
            data: data,
            url: '/ajax.php',
            dataType : "json",
            success: function (data){
                if(data.result==1)
                {
                    $('#modalWindow').arcticmodal('close');
                    handbooks.formSubmit();                   
                }
                else  console.log(data);
            }
        });
    },
    load_elfinder:function (id) 
    {
        
        $('<div id="myelfinder"></div>').dialogelfinder({
            url: 'ajax.php?controller=lib.elfinder.controller&folder=lang',
            commandsOptions: {
              getfile: {
                oncomplete: 'destroy' // destroy elFinder after file selection
              }
            },
            getFileCallback: function(url){
                $('#'+id).val(url);
                $('#prev_'+id).attr({src:url});
            } // pass callback to file manager
          });
        return false;
    },

    init: function() {
        $('#langSwitcher').click(function(){
            $('#langList').toggle();
        });

        $("#langSwitcher").change(function(){
            $.ajax({
                type: "POST",
                url: '/ajax.php',
                data: {
                    controller: "module.lang.controller",
                    action: 'change',
                    langCode: $('#langSwitcher').val()
                },
                success: function(data) {
                    if (data.result == 1) {
                        //$("#content").html(data.template);
                    }
                }
            });
        });

        //обработчик "холостого" клика, языковой блок
        var langEnter = false;
        $('#langSwitcher').mouseenter(function() {
            langEnter = true;
        });
        $('#langSwitcher').mouseleave(function() {
            langEnter = false;
        });
        $(document).click(function() {
            if (!langEnter) {
                $('#langList').hide();
            }
        });
    }
}