<?php



/**
 * Description of view
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Lang_View {
    
    public $langCode;
    
    public function draw($items, $lang) {
        $tplFileName = '../module/lang/template/lang.html';

        $customTpl = Dante_Helper_App::getWsPath().'/lang/lang.html';
        if (file_exists($customTpl)) {
            $tplFileName = $customTpl;
        }

        $tpl = new Dante_Lib_Template();
        $tpl->items = $items;
        $tpl->lang = $lang;
        $tpl->langCode = $this->langCode;
        return $tpl->draw($tplFileName);
    }
}

?>
