<?php
/**
 * Контроллер многоязыковой поддержки
 * User: dorian
 * Date: 22.05.12
 * Time: 15:21
 * To change this template use File | Settings | File Templates.
 */
class Module_Lang_Controller extends Dante_Controller_Base
{
    /**
     * Выводит комбобокс выбора языка
     * @return string
     */
    protected function _defaultAction() {
        $mapper = new Module_Lang_Mapper();
        $languages = $mapper->getList();

        $langId = Module_Lang_Helper::getCurrent();
        $curLang = new Module_Lang_Model();
        $curLang->name = Module_Lang_Mapper::getNameById($langId);
        $curLang->sys_name = Module_Lang_Mapper::getCodeById($langId);
        $curLang->image = Module_Lang_Mapper::getImageById($langId);
        
        $view = new Module_Lang_View();
        $view->langCode = $curLang->sys_name;
        return $view->draw($languages, $curLang);
    }

    /**
     * Переключение языка
     */
    protected function _changeAction() {
        $langCode = $this->_getRequest()->get('code');
        $url = $this->_getRequest()->get('url');
        
//        Dante_Lib_Log_Factory::getLogger()->debug("lang change : $url", 'logs/lang.log');
        
        //if ($langCode == 0) throw new Exception('invalid lang code');
        $langId = (int)Module_Lang_Mapper::getByCode($langCode);
        if ($langId == 0) throw new Exception("cant find lang by code $langCode");
        
        Module_Lang_Helper::setCurrent($langId);
        
        // поменять url
        //$url = Dante_Helper_App::getRequest()->get_requested_url();
        $data = explode('/', $url);        
        $curLangCode = $data[1];
        
        if ($curLangCode == $langCode) return array('result' => 1); // нечего менять
        
        $default = Module_Lang_Helper::getDefaultLangId();        
        if($langId == $default) 
        {
            unset($data[1]);
            $url = implode('/', $data);  
        }
        else
        {
            // проверим а действительно ли первый элемент это код языка
            $curLangId = (int)Module_Lang_Mapper::getByCode($curLangCode);
            if ($curLangId > 0) // первый элемент ссылки действительно язык
            {   
                // пересоберем ссылку
                $data[1] = $langCode;
                $url = implode('/', $data);  
            }
            else // просто добавим код языка к текущей ссылке        
                $url = "/{$langCode}{$url}";
        }
        
        return array('result' => 1, 'url'=>$url);
    }
    
    protected function _translateAjaxAction()
    {
        $request = $this->_getRequest()->get('labels');
        if(!is_array($request)) 
            return array(
                'result' => 0,
                'message' => 'Wrong request!'
            );
        
        $labels = array();
        foreach($request as $code)
        {
            $labels[$code] = Module_Lang_Helper::translate($code);
        }
        
        return array(
            'result' => 1,
            'labels' => $labels
        );
    }
}
