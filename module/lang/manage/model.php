<?php

/**
 * Description of langs
 *
 * @author Mort
 */
class Module_Lang_Manage_Model {
    
    public $sortable_rules=array(
        'name'=>'`id`',
        'type'=>'asc',
    );
    
    public $id;

    public $name;
    
    public $sys_name;

    public $image;

    public $is_default = 0;
}

?>
