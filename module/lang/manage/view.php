<?php

class Module_Lang_Manage_View
{
    public $access;
    
    public function drawForm($lang)
    {
        $tpl = new Dante_Lib_Template();
        $filePath = '../module/lang/manage/template/form.html';
        
        $tpl->imgDir = Module_Lang_Helper::getFlagsDir();
        $tpl->lang = $lang;
        $tpl->access = $this->access;
        return $tpl->draw($filePath);
    }
}

?>