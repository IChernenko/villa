<?php
/**
 * Description of lang
 *
 * @author Mort
 */

class Module_Lang_Manage_Controller extends Module_Handbooksgenerator_Controller_Base 
{    
    protected function _init()
    {
        parent::_init();
        $this->_mapper = new Module_Lang_Manage_Mapper();
    }

    protected function _drawAction()
    {     
        if(is_null($this->_access))
        {
            $helper = new Module_Division_Helper();
            $this->_access = $helper->checkAccess();
        }
        
        $model = new Module_Lang_Manage_Model();
        
        $params = $this->getParams($model);
        
        $list = $this->_mapper->getRowsByParams($params);
        $rows = $list['rows'];
        
        $this->formEnable('module.lang.manage.controller');
        $this->caption = 'Языки';
        
        $titles = array(
            array(
                'content' => 'ID',
                'sortable' => 'id'
            ),
            array(
                'content' => 'Название',
                'sortable' => 'name',
            ),   
            array(
                'content' => 'Системное название',
                'sortable' => 'sys_name',
            ),            
            array(
                'content' => 'Изображение'
            ),              
            array(
                'content' => 'Функции'
            )  
        );
        
        $this->thead = array($titles);
        
        $table = array();
        $path = Module_Lang_Helper::getFlagsDir();
        foreach($rows as $k => $cur)
        {
            $row = array();
            $row['id'] = $cur['id'];
            $row['name'] = $cur['name'];
            $row['sys_name'] = $cur['sys_name'];            
            $row['image'] = $cur['image'] ? '<img style="height: 50px" src="'.$path.$cur['image'].'"/>' : '-';            
            $row['buttons']['id'] = $cur['id'];
            $row['buttons']['type'] = $this->_access['delete']?array('edit', 'del'):array('edit');
                        
            $table[]=array(
                'content'=>$row,
            );
        }
        
        $this->tbody = $table;    
        
        if($this->_access['create'])
        {
            $this->tfoot=array(
                array(
                    'button_filter'=>array(
                        "type"=>array(
                            'edit'
                        ),                    
                    )
                )  
            );
        }        
        
        $html = $this->generate();
        
        return $html;
    }
    
    
    protected function _drawFormAction()
    {
        $url = $this->_getRequest()->get('page_url');
        
        $helper = new Module_Division_Helper();
        $this->_access = $helper->checkAccess($url, true);
        
        $id = $this->_getRequest()->get_validate('id', 'int');
        
        $lang = $this->_mapper->getById($id);
        
        $view = new Module_Lang_Manage_View();
        if(is_array($this->_access))
        {
            if($id)
                $view->access = $this->_access['update'];
            else
                $view->access = $this->_access['create'];
        }
        
        return array(
            'result' => 1,
            'html' => $view->drawForm($lang)
        );
    }
    /**
     * редактирование/добавление/удаление записей грида
     */
    protected function _editAction() 
    {        
        $model = new Module_Lang_Manage_Model();
        $model->id = $this->_getRequest()->get('id');
        $model->name = $this->_getRequest()->get('name');
        $model->sys_name = $this->_getRequest()->get('sys_name');
        
        $image = $this->_getRequest()->get_validate('image', 'text');
        $curImage = $this->_mapper->getCurrentImage($model->id);
        $dir = Module_Lang_Helper::getFlagsDir();
        
        if(is_file($dir.$curImage) && $image != $curImage)
                unlink($dir.$curImage);
        
        $model->image = $image;
        
        $this->_mapper->update($model);
        return array(
            'result' => 1
        );
    }

    protected function _imageUploadAction()
    {
        if(!isset($_FILES['file']))
            return array(
                'result' => 0,
                'message' => 'Нет файла для загрузки'
            );
            
        $uploadDir = Module_Lang_Helper::getFlagsDir();
        if (!is_dir($uploadDir)) mkdir($uploadDir, 0755, true);             

        $newName = time();  

        $uploader = new Lib_JqueryUpload_Uploader();
        $uploader->globalKey = 'file';
        
        $image = $uploader->upload($uploadDir, $newName);   
        
        return array(
            'result' => 1,
            'image' => $image,
            'imageDir' => $uploadDir
        );
    } 
    
    protected function _setDefaultAction() 
    {        
        $id = $this->_getRequest()->get('id');        
        $this->_mapper->setDefault($id);        
        
        return array(
            'result' => 1
        );
    }
    
}

?>
