<?php

/**
 * Description of langs
 *
 * @author Mort
 */
class Module_Lang_Manage_Mapper extends Module_Handbooksgenerator_Mapper_Base{
    
    protected $_tableName = '[%%]languages';
        
    public function getById($id)
    {        
        $model = new Module_Lang_Manage_Model();
        if(!$id) return $model;
        
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->select(array('id'=>$id));
        
        $fields = $table->getFields();
        
        $model->id = $fields['id'];
        $model->name = $fields['name'];
        $model->sys_name = $fields['sys_name'];
        $model->image = $fields['image'];
        $model->is_default = $fields['is_default'];
        
        return $model;
    }
    
    /**
     * добавление номера
     * @param Module_Lang_Manage_Model $params 
     */
    public function add(Module_Lang_Manage_Model $params) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->name = $params->name;
        $table->sys_name = $params->sys_name;
        $table->image = $params->image;

        $table->id = $table->insert();
        return true;
    }
    
    /**
     * @param Module_Lang_Manage_Model $model 
     */
    public function update(Module_Lang_Manage_Model $model) 
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->id = $model->id;
        $table->name = $model->name;
        $table->sys_name = $model->sys_name;
        $table->image = $model->image;

        return $table->apply(array(
            'id' => $model->id
        ));
    }
    
    public function setDefault($id)
    {
        /*
        //сперва переносим запись с тукущим ид 1
        $sql = "SELECT `id` FROM {$this->_tableName} ORDER BY `id` DESC LIMIT 1";
        $lastId = Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'id');
        $newId = $lastId + 1;

        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->id = $newId;        
        $table->update(array(
            'id' => 1
        ));
        */
                
        //теперь устанавниваем ид 1 для выбранной записи
        $tableDefault = new Dante_Lib_Orm_Table($this->_tableName);
        $tableDefault->is_default = 1;
        $tableDefault->update(array('id' => $id));
    }
    
    public function getCurrentImage($id)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        return $table->select(array('id' => $id))->image;
    }
    
}

?>
