<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 22.05.12
 * Time: 12:35
 * To change this template use File | Settings | File Templates.
 */
class Module_Lang_Mapper
{
    protected $_langTableName = '[%%]languages';

    /**
     * Получение списка доступных языков
     * @return array
     */
    public function getList() {
        $sql = "select * from ".$this->_langTableName."
                order by id";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $list = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $model =  new Module_Lang_Model();
            $model->id = (int)$f['id'];
            $model->name = $f['name'];
            $model->sys_name = $f['sys_name'];
            $model->image = $f['image'];

            $list[$model->id] = $model;
        }

        return $list;
    }
    
    /**
     * Получение списка кодов языков
     * @return array
     */
    public static function getCodesList() {
        $sql = "select id, sys_name from [%%]languages";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $list = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $list[$f['id']] = $f['sys_name'];
        }

        return $list;
    }
    
    public function getNamesList()
    {
        $sql = "SELECT `id`, `name` FROM {$this->_langTableName}";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        
        $list = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            $list[$f['id']] = $f['name'];
        }
        
        return $list;
    }
    
    /**
     * Находит перевод для заданного кода
     * @param type $code
     * @return type 
     */
    public static function translate($code) {
        $langId = Module_Lang_Helper::getCurrent();
        
        $sql = "select 
                    ll.name
                from [%%]labels as l
                left join [%%]labels_lang as ll on (ll.id = l.id and ll.lang_id = {$langId})
                where l.sys_name='{$code}'";
        $f = Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
        
        return (isset($f['name'])) ? $f['name'] : false;
    }
    
    /**
     * Загружает все языковый константы
     * @return array
     */
    public static function loadAll($langId = false) {
        if(!$langId) $langId = Module_Lang_Helper::getCurrent();
        
        $sql = "select 
                    l.sys_name,
                    ll.name
                from [%%]labels as l
                left join [%%]labels_lang as ll on (ll.id = l.id and ll.lang_id = {$langId})";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $list = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $list[$f['sys_name']] = $f['name'];
        }
        
        return $list;
    }
    
    public static function getByCode($code) {
        $table = new Dante_Lib_Orm_Table('[%%]languages');
        $table->getByAttribute(array('sys_name'=>$code))->fetch();
        return $table->id;
    }
    
    public static function getNameById($id) {
        $table = new Dante_Lib_Orm_Table('[%%]languages');
        $table->getByAttribute(array('id'=>$id))->fetch();
        return $table->name;
    }
    
    public static function getCodeById($id) {
        $table = new Dante_Lib_Orm_Table('[%%]languages');
        $table->getByAttribute(array('id'=>$id))->fetch();
        return $table->sys_name;
    }
    
    public static function getImageById($id) {
        $table = new Dante_Lib_Orm_Table('[%%]languages');
        $table->getByAttribute(array('id'=>$id))->fetch();
        return $table->image;
    }
}
