<?php
global $package;
$package = array(
    'version' => '5',
    'name' => 'module.lang',
    'dependence' => array(
        'lib.geoip'
    ),
    'js' => array(
        '../module/lang/js/lang.js',
        //'../module/lang/js/dante-lang.js'
    )
);