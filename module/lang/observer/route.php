<?php


/**
 * Description of route
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Lang_Observer_Route implements Dante_Lib_Observer_Iobserver{
    
    public function notify($request, $strEventType ) {
        $url = $request->get_requested_url();
        if (!strstr($url, '/')) return false;
        
        $data = explode('/', $url);
        $langCode = $data[1];
        
        $langId = (int)Module_Lang_Mapper::getByCode($langCode);
        if ($langId == 0) return false;
        
        Dante_Lib_Session::set('lang', $langId);
        return true;
    }
}

?>
