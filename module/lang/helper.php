<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 23.05.12
 * Time: 15:00
 * To change this template use File | Settings | File Templates.
 */
class Module_Lang_Helper
{
    protected static $_labels = false;
    
    /**
     * Определение текущего языка
     * @static
     * @return int|null
     */
    public static function getCurrent() {
        $lang = Dante_Lib_Session::get('lang');
        if (!$lang) $lang = self::getDefaultLangId ();

        return $lang;
    }
    
    /**
     * Возвращает код текущего языка
     * @return string
     */
    public static function getCurrentCode() {
        return Module_Lang_Mapper::getCodeById(self::getCurrent());
    }    
    
    public static function getDefaultLangId() {
//        try {
            $user_ip = getenv('REMOTE_ADDR');
//
//            if ( $geo = file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_ip") ) {
//                $geo = unserialize( $geo );
//                $code = $geo["geoplugin_countryCode"];
//                $code =  $code ? $code : 'UA';
//
//                $table = new Dante_Lib_Orm_Table('tst_currencycountries');
//                $table->select(array('country_code' => $code));
//            } else {
//                return 12;
//            }
//
//        } catch (ErrorException $e) {
//            return 12;
//        }
//        return  $table->lang_id;

        $data = "http://www.geoplugin.net/php.gp?ip=$user_ip";
        $headers = get_headers($data);
        $responseCode = substr($headers[0], 9, 3);

        if( $responseCode != "200" ){
            return 12;
        } else {
            $geo = file_get_contents($data);
            $geo = unserialize($geo);
            $code = $geo["geoplugin_countryCode"];
            $code =  $code ? $code : 'UA';
            $table = new Dante_Lib_Orm_Table('tst_currencycountries');
            $table->select(array('country_code' => $code));
            return  $table->lang_id;
        }
    }
    
    /**
     * Находит перевод для заданного кода
     * @param type $code
     * @return type 
     */
    public static function translate($code) {
        if (!self::$_labels) {
            self::$_labels = Module_Lang_Mapper::loadAll();
        }
        
        return (isset(self::$_labels[$code])) ? self::$_labels[$code] : false;
    }
    
    /**
     * Формирует url в с кодом текущего языка
     * @param type $url 
     */
    public static function getLangUrl($url)
    {
        $url = ltrim($url, '/');
        
        $lang = self::getCurrent();
        if($lang == self::getDefaultLangId()) return "/{$url}";
        $code = Module_Lang_Mapper::getCodeById($lang);        
        
        return "/{$code}/{$url}";
    }
    
    public static function setCurrent($langId)
    {
        Dante_Lib_Session::set('lang', $langId);
    }
    
    public static function getFlagsDir()
    {
        return Dante_Lib_Config::get('app.uploadFolder').'/'.Dante_Lib_Config::get('lang.folder').'/';
    }
}
