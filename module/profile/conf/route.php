<?php

$url = Dante_Controller_Front::getRequest()->get_requested_url();

Dante_Lib_Router::addRule("/^\/profile\/(\w+)\/$/", array(
    'controller' => 'Module_Profile_Controller',
    'params' => array('login'=>'%1')
));


Dante_Lib_Router::addRule("/^\/profile\//", array(
    'controller' => 'Module_Profile_Controller'
));

Dante_Lib_Router::addRule("/\/manage\/profile\//", array(
    'controller' => 'Module_Profile_Manage_Controller'
));

?>
