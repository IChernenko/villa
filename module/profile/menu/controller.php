<?php



/**
 * Отображение меню в профиле пользователя
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Profile_Menu_Controller extends Dante_Controller_Base{
    
    protected function _getItems() {
        $items = array();
        $items[] = array(
            'url' => '/manage/profile/',
            'name' => 'Мой Профиль'
        );
        
        return $items;
    }
    
    protected function _defaultAction() {
        $uid = Dante_Helper_App::getUid();
        //if (!$uid) return '';
        
        $items = $this->_getItems();
        if (count($items)==0) return '';
        
        $html = "<ul id='profile_menu'>";
        foreach($items as $item) {
            if(isset($item['onlyuser']) && !$uid) continue;
            $html .= "<li><a href='{$item['url']}'>{$item['name']}</a></li>";
        }
        $html .= '</ul>';
        
        return $html;
    }
}

?>
