<?php



/**
 * Контроллер профиля
 *
 * @author dorian
 */
class Module_Profile_Manage_Controller extends Dante_Controller_Base{
	
    protected function _defaultAction()
    {
        $uid = Dante_Helper_App::getUid();
        if (!$uid) return 'Вы должны быть залогиненным пользователем';
        
        
        $user = Module_Profile_Model_User::getInstance()->byId($uid)->fetch()->getFields();
        $profile = Module_Profile_Dmo::dmo()->byUid($user['id'])->fetch()->getFields(); 
        $profile['email'] = $user['email'];
        $profile['login'] = $user['login'];
        
        $profile['link'] = Dante_Lib_Config::get('site.url').'profile/'.$user['login'].'/';
        
        $fileName = '../dante/i/noimage.jpg';
        if (isset($profile['avatar']) && $profile['avatar'] != '') {
            //$uploadDir = Dante_Lib_Config::get('app.uploadFolder');
            $site = Dante_Lib_Config::get('site.url');
            
            //$fileName = 'media/'.$user['id'].'/profile/'.$profile['avatar'];
            $fileName = $profile['avatar'];
            if (!file_exists($fileName)) $fileName = '../dante/i/noimage.jpg';
        }
        $profile['avatar'] = $fileName;
        
        //print_r($profile); die();
        
        $tpl = new Dante_Lib_Template();
        $tpl->profile = $profile;
        return $tpl->draw('../module/profile/template/manage/profile.html');
        
        return $tpl->draw('template/default/profile/manage/index.html');
    }
    
    protected function _saveAction()
    {
        try {
            $uid = Dante_Helper_App::getUid();
            if (!$uid) return 'Вы должны быть залогиненным пользователем';

            $profile = new Module_Profile_Dmo();
            $profile->loadBy(array('user_id'=>$uid));
            if ($profile->getNumRows() == 0)  throw new Exception('Немогу загрузить профиль');
            
            $profile->first_name = $this->_getRequest()->get('name');
            $profile->last_name = $this->_getRequest()->get('lastName');
            $profile->middle_name = $this->_getRequest()->get('middleName');
            $profile->about = $this->_getRequest()->get('about');
            $profile->avatar = $this->_getRequest()->get('avatar');
            $profile->skype = $this->_getRequest()->get('skype');
            $profile->phone = $this->_getRequest()->get('phone');
            $profile->update(array('user_id'=>$uid));
                
            $user = new Module_User_Dmo();
            $user->loadBy(array('id'=>$uid));
            if ($user->getNumRows() == 0)  throw new Exception('Немогу найти пользователя');
            $user->login = $this->_getRequest()->get('login');
            $user->email = $this->_getRequest()->get('email');
            $user->update(array('id'=>$uid));
               
            
        } catch (Exception $exc) {
            return array(
                'result' => 0,
                'message' => $exc->getMessage()
            );
        }

        return array('result' => 1, 'message' => 'Изменения успешено сохранены');
    }
    
    protected function _uploadAction() {
        $uid = Dante_Helper_App::getUid();
        if (!$uid) return 'Вы должны быть залогиненным пользователем';
        
        if(isset($_FILES['Filedata'])){
            $uploadDir = Dante_Lib_Config::get('app.uploadFolder').'/'.$uid.'/profile/';
            Dante_Lib_Log_Factory::getLogger()->debug("upload to folder :  $uploadDir");
            if (!file_exists($uploadDir)) mkdir($uploadDir, 0777, true);
            

            $uploader = new Component_Swfupload_Upload();
            $fileName = $uploader->upload($uploadDir);
            
            //ресайз начало
            $newname = $uploadDir.$fileName;
            $ext = pathinfo($newname, PATHINFO_EXTENSION);


            $image = new Lib_Image_Driver();
            $image->load($newname);
            
            $widthTemp = $image->getWidth();
            $heightTemp = $image->getHeight();
            
            if($widthTemp > Dante_Lib_Config::get('profile.maxImageWidth')){
                $image->resizeToWidth(Dante_Lib_Config::get('profile.maxImageWidth'));
            }elseif($heightTemp > Dante_Lib_Config::get('profile.maxImageHeight')){
                $image->resizeToHeight(Dante_Lib_Config::get('profile.maxImageHeight'));
            }
            
            Dante_Lib_Log_Factory::getLogger()->debug("before save $newname");
            $oldName = $newname;
            $newname = $uploadDir.time().'.'.$ext;
            $image->save($newname);
            unlink($oldName);
            //@chmod($newname, 0777);
            //ресайз конец
            
            //Dante_Lib_Log_Factory::getLogger()->debug("after save $newname: file: $fileName type: $typeId");
            /*$mapper = new Kia_Module_Publications_Manage_Mapper();
            $mapper->addImage($typeId, $fileName);*/
            
            return array("data" => $newname);
        }else{
            return false;
        }
    }
}

?>
