<?php



/**
 * DMO профиля пользователя
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Profile_Dmo extends Dante_Lib_Orm_Table{
    
    protected $_tableName = '[%%]user_profile';
    
    /**
     *
     * @return Module_Profile_Dmo 
     */
    public static function dmo() {
        return self::getInstance();
    }


    public function byId($id) {
        return $this->getByAttributes(array('id'=>$id));
    }
    
    public function byUid($id) {
        return $this->getByAttributes(array('user_id'=>$id));
    }
}

?>
