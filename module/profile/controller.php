<?php



/**
 * Контроллер профиля пользователя
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Profile_Controller extends Dante_Controller_Base{
    
    protected function _defaultAction() {
        $login = $this->_getParam('%1');
        
        $user = Module_Profile_Model_User::getInstance()->byLogin($login)->fetch()->getFields();
        if (!$user) {
            return 'Такого пользователя нет';
        }
        
        $profile = Module_Profile_Dmo::dmo()->byUid($user['id'])->fetch()->getFields(); 
        $profile['email'] = $user['email'];
        $profile['login'] = $user['login'];
        
        $site = Dante_Lib_Config::get('site.url');
        //$uploadDir = Dante_Lib_Config::get('app.uploadFolder');
        
        if ($profile['avatar'] == '')
            $fileName = '../dante/i/noimage.jpg';
        else {
            
            $fileName = $site.'/media/'.$user['id'].'/profile/'.$profile['avatar'];
        }
        //if (!file_exists($fileName)) $fileName = '../dante/i/noimage.jpg';
        $profile['avatar'] = $fileName;
        
        $tpl = new Dante_Lib_Template();
        $tpl->profile = $profile;
        return $tpl->draw('../module/profile/template/profile.html');
        
        /*$service = new Module_Hotel_Service_Bookinghistory();
        return $service->draw();
        return 'this is profile';*/
    }
}

?>
