<?php



/**
 * Description of user
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Profile_Model_User  extends Dante_Lib_Orm_Table{
    
    protected $_tableName = '[%%]users';
    
    public function byId($id) {
        return $this->getByAttributes(array('id'=>$id));
    }
    
    public function byLogin($login) {
        return $this->getByAttributes(array('login'=>$login));
    }

    public function issetUserEmail($email) {
            $data = $this->getByAttributes(array('email'=>$email));
            if ( count($data) == 1) {
                    return true;	
            }
            return false;
    }
}

?>
