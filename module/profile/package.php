<?php
global $package;
$package = array(
    'version' => '1',
    'name' => 'module.profile',
    'description' => 'профиль пользователя',
    'dependence' => array(
        'module.user',
        'lib.jquery.validate',
        'component.swfupload'
    )
);