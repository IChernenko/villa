<?php

/**
 * Description of callback
 *
 * @author Mort
 */
class Module_Transportcompanies_Manage_Mapper {
    
    protected $_tableName = '[%%]transportcompanies';
    protected $_tableNameTowns = '[%%]transportcompanies_towns';
    
    /**
     * выбираем номера
     * @param array $params
     * @return Lib_Jgrid_Model_Base 
     */
    public function getUsers($params) {
        $jgridMapper = new Lib_Jgrid_Mapper_Base();
        $rowsArray = $jgridMapper->getRowsByParams($this->_tableName, $params);
        $categoriesPrep = $rowsArray['rows'];
        $rowCount = $rowsArray['rowCount'];
        
        $categories = new Lib_Jgrid_Model_Base();
        $i=0;
        
        $helper = new Module_Transportcompanies_Manage_Helper();
        $types = $helper->getTypes();
        
        foreach ($categoriesPrep as $k=>$f){
            //id 	email 	password 	rating
            $categories->rows[$i]['id'] = $f['id'];
            $categories->rows[$i]['cell'] = array(
                                                $f['id'],
                                                $f['title'],
                                                $types[$f['type']],
                                                $f['image']
                                            );
            $i++;
        }
        
        $categories->records = $rowCount;
        
        return $categories;
    }
    
    /**
     * добавление номера
     * @param Module_Transportcompanies_Manage_Model $params 
     */
    public function add(Module_Transportcompanies_Manage_Model $params) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->title = $params->title;
        $table->image = $params->image;
        $table->type = $params->type;

        $table->id = $table->insert();
        return true;
    }
    
    /**
     * обновляем номер
     * @param Module_Transportcompanies_Manage_Model $params 
     */
    public function update(Module_Transportcompanies_Manage_Model $params) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->id = $params->id;
        $table->title = $params->title;
        $table->image = $params->image;
        $table->type = $params->type;


        return $table->apply(array(
            'id' => $params->id
        ));
    }
    
    /**
     * удаляем номер
     * @param int $id 
     */
    public function del($id) {
        $table = new Dante_Lib_Orm_Table($this->_tableNameTowns);
        $table->delete(array(
            'company_id' => $id
        ));
        
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->delete(array(
            'id' => $id
        ));
    }
    
    public function getCheckedByTown($id)
    {
        //выгребаем строки
        $sql = "select
                    *
                from ".$this->_tableNameTowns." WHERE company_id=".$id;

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        
        $rowsArray = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $rowsArray[$f['town_id']] = $f;
        }
        return $rowsArray;
    }
    
    public function saveTowns($id, $towns)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableNameTowns);
        $table->delete(array(
            'company_id' => $id
        ));
        
        $townsArr = explode(";", $towns);
        
        foreach ($townsArr as $town) {
            $table = new Dante_Lib_Orm_Table($this->_tableNameTowns);
            $table->company_id = $id;
            $table->town_id = $town;

            $table->insert();
        }
        
    }
    
}

?>
