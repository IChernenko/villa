<?php

/**
 * Description of helper
 *
 * @author Mort
 */

class Module_Transportcompanies_Manage_Helper {
    
    protected $_types = array(
        1 => 'Авиакомпания',
        2 => 'ЖД компания'
    );
    
    public function getTypes()
    {
        return $this->_types;
    }
}

?>
