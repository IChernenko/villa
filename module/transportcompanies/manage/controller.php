<?php
/**
 * Description of callback
 *
 * @author Mort
 */

class Module_Transportcompanies_Manage_Controller extends Lib_Jgrid_Controller_Base {


    
    /**
     * отрисовка стартового html
     * @return string 
     */
    protected function _defaultAction()
    {
        $hotelHelper = new Module_Division_Helper();
        $hotelHelper->checkAccess();
        
        $helper = new Module_Transportcompanies_Manage_Helper();
        $types = $helper->getTypes();
        foreach ($types as $key=>$value)
        {
            $roomTypes[] = $key.':'.$value;
        }
        $typesString = implode(';', $roomTypes);
        
        $params = array(
            'navId' => 'navgrid',
            'pagerId' => 'pagernav',
            'colNames' => "'№','Название','Тип','Изображение'",
            'colModel' => array(
                array(
                    'name'=>"'id'",
                    'index'=>"'id'", 
                    'width'=>50,
                    'align' => '"center"',
                    'formatter'=>"'integer'",
                    'editable'=>'false'
                    ),
                array(
                    'name'=>"'title'",
                    'index'=>"'title'", 
                    'width'=>450,
                    'align' => '"center"',
                    'editable'=>'true',
                    'editoptions'=>array(
                        'readonly' => 'false',
                        'size' => 10,
                        'maxlength' => 50
                        ),
                    'editrules'=>array(
                        'required'=>'true'
                        ),
                    'formoptions' => array(
                        'label'=> "'Название<font color=red>*</font>'"
                            )
                    ),
                array(
                    'name'=>"'type'",
                    'index'=>"'type'", 
                    'width'=>300,
                    'align' => '"left"',
                    'editable'=>'true',
                    'edittype'=>'"select"',
                    'editoptions'=>array(
                        'value' => "'$typesString'"
                        ),
                    'stype' => '"select"', 
                    'search'=>'false'
                    ),
                array(
                    'name'=>"'image'",
                    'index'=>"'image'", 
                    'width'=>200,
                    'align' => '"left"',
                    'editable'=>'true',
                    'hidden'=>'true',
                    'editrules'=>array(
                        'edithidden' => 'true'
                        ),
                    'sortable' => '"false"', 
                    'search'=>'false'
                    ),
                ),
                'sortname' => 'id',
                'caption' => 'Транспортные компании',
                'url' => 'ajax.php?action=draw&controller=module.transportcompanies.manage.controller&q=1&language=1',
                'editurl' => 'ajax.php?action=edit&controller=module.transportcompanies.manage.controller&q=1&language=1',
                'height' => '300',
            'onSelectRow'=>'function(id){ 
                            transportcompanies.setCurRow(id);
                        }',
            'gridComplete' => "function(){transportcompanies.hideNavButtons();}",
            'editOptionsA' => array(
                    'width' => '450',
                    'height' => '300',
                    'reloadAfterSubmit' => 'true',
                    'closeAfterEdit' => 'true',
                    'afterShowForm'=> 'function() {
                        transportcompanies.resizeInputs();
                        }'
                ),
            'addOptionsA' => array(
                        'width' => '450',
                        'height' => '300',
                        'reloadAfterSubmit' => 'true',
                        'closeAfterAdd' => 'true',
                        'afterShowForm'=> 'function() {
                                transportcompanies.resizeInputs();
                                }'
                    )
        );
        
        $this->setParams($params);
        $grid = $this->getHTML();
        
        //теперь ф-ции для редактирования
        $imagesHTML= '';
        //пошел яваскрипт
        $host = Dante_Lib_Config::get('url.domain_main');
        $imagesHTML .= '<script>
                var hostUrl = "'.$host.'";
                var swfu;
                curRowId = 0;
                </script>
                
            <div id="navButtons" style="margin: 0px auto;">
                <center>
                <input type="button" value ="Редактировать города" onclick="transportcompanies.editTowns();" class="btn">
                <br>
                <div id="navButtonsTowns"></div>

                </center>
            </div>
                ';
        
        return $grid.$imagesHTML;
    }
    
    /**
     * формируем модель грида
     * @return Lib_Jgrid_Model_Base 
     */
    protected function _drawAction() {
        
        $requestParams = array(
            'searchParams' => array(
                'id',
                'title'
            )
        );
        $this->setRequestParams($requestParams);
        $params = $this->getRequestByParams();
        
        $mapper = new Module_Transportcompanies_Manage_Mapper();
        $users = $mapper->getUsers($params);
        
        $users->page = $params['page']; 
        $users->total = ceil($users->records/$params['rows']);

        return $users;
    }
    
    /**
     * редактирование/добавление/удаление записей грида
     */
    protected function _editAction() {
        $hotelHelper = new Module_Division_Helper();
        $hotelHelper->checkAccess();
        
        $operationAction = $this->_getRequest()->get('oper'); // add/edit/del

        
        $table = new Module_Transportcompanies_Manage_Model();
        $table->id = $this->_getRequest()->get('id');
        $table->title = $this->_getRequest()->get('title');
        $table->image = $this->_getRequest()->get('image');
        $table->type = $this->_getRequest()->get('type');
        
        $mapper = new Module_Transportcompanies_Manage_Mapper();
        switch ($operationAction) {
            case 'add':
                $mapper->add($table);
                break;
            case 'edit':
                $mapper->update($table);
                break;
            case 'del':
                $mapper->del($table->id);
                break;
        }
    }
    
    /**
     * загрузка картинки
     * @return boolean 
     */
    protected function _uploadAction() {
        $typeId = $this->_getRequest()->get('typeId');

        Dante_Lib_Log_Factory::getLogger()->debug('$typeId = '.$typeId);
        
        if(isset($_FILES['Filedata'])){
            $uploadDir = Dante_Lib_Config::get('app.uploadFolder').'/transportcompanies/';
            
            Dante_Lib_Log_Factory::getLogger()->debug('$uploadDir = '.$uploadDir);
            
            $uploader = new Component_Swfupload_Upload();
            $fileName = $uploader->upload($uploadDir);
            
            //ресайз начало
            $newname = $uploadDir.$fileName;
            $image = new Lib_Image_Driver();
            $image->load($newname);
            
            $widthTemp = $image->getWidth();
            $heightTemp = $image->getHeight();
            
            if($widthTemp>800){
                $image->resizeToWidth(800);
            }elseif($heightTemp>550){
                $image->resizeToHeight(550);
            }
            
            Dante_Lib_Log_Factory::getLogger()->debug("before save $newname");
            $image->save($newname);
            
            return $fileName;
        }else{
            return false;
        }
    }
    
    protected function _getTownsAction() {
        $id = $this->_getRequest()->get('id');
        
        $countriesMapper = new Module_Currencycountries_Manage_Mapper();
        $countries = $countriesMapper->getLight();
        
        $townsMapper = new Module_Currencytowns_Manage_Mapper();
        $towns = $townsMapper->getLight();
        
        $mapper = new Module_Transportcompanies_Manage_Mapper();
        $checked = $mapper->getCheckedByTown($id);
        
        
        return array(
            'result' => 1,
            'countries' => $countries,
            'towns' => $towns,
            'checked' => $checked
        );
    }
    
    protected function _saveTownsAction() {
        $id = $this->_getRequest()->get('id');
        $towns = $this->_getRequest()->get('towns');

        $mapper = new Module_Transportcompanies_Manage_Mapper();
        $checked = $mapper->saveTowns($id, $towns);
        
        
        return array(
            'result' => 1
        );
    }
    
}

?>
