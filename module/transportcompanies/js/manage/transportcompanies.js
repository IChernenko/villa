transportcompanies = {
    
    resizeInputs:function()
    {
        $('#imagesEditForm').remove();
        $('#transportcompaniesImageTemp').remove();
        $('#tr_image .DataTD #image').hide();
        var filename = $('#tr_image .DataTD #image').val();
        var image = '';
        if(filename!=''){
            image = '<img src="/media/transportcompanies/'+filename+'" style="height:150px; float:left;" id="transportcompaniesImageTemp">';
        }
        $('#tr_image .DataTD').html( $('#tr_image .DataTD').html()+image+'<div id ="imagesEditForm" style="display:block;float:left;"></div>' );
        $('#tr_image .DataTD #image').css('visibility', 'hidden');
        transportcompanies.displayImages(curRowId);
    },
    
    displayImages: function (id){
        document.getElementById('imagesEditForm').innerHTML = ''+
        //'Изображения:'+
               // '<div id="images_html"></div>'+
                //'<br><br><br>'+
                //'Загрузить новое изображение:<br>'+
                '<div id="uploadButton"></div>'+
                //'<div id="status"></div>'+
        '';
        
        var typeId = id;

        swfUploadUrl = hostUrl+"/ajax.php?action=upload&controller=module.transportcompanies.manage.controller&q=1&ctype=html&typeId="+typeId;

        params = {
            uploadUrl:swfUploadUrl,
            uploadButton: "../../../../component/swfupload/button.png",
            uploadComplete: transportcompanies.uploadCallBack
            //typeId: typeId
        }
        delete swfu;

        initSwfUpload(params);
    },
    
    uploadCallBack: function(file)
    {
        $('#transportcompaniesImageTemp').remove();
        var image = '<img src="/media/transportcompanies/'+file.name+'" style="height:150px;">';
        $('#tr_image .DataTD #image').val(file.name);
        document.getElementById('imagesEditForm').innerHTML = image;
        //transportcompanies.hideNavButtons();
        //$("#navgrid").trigger("reloadGrid");
    },
    
    setCurRow: function (id){
        curRowId = id;
        transportcompanies.showNavButtons();
    },
    
    showNavButtons: function (){
        document.getElementById("navButtons").style.display="block";
        $('#navButtonsTowns').html('');
    },
    
    hideNavButtons: function (){
        document.getElementById("navButtons").style.display="none";
        $('#navButtonsTowns').html('');
    },
    
    editTowns: function (){
        $.ajax({
            data:{id:curRowId},
            url: '/ajax.php?action=getTowns&controller=module.transportcompanies.manage.controller',
            dataType : "json",
            success: function (data){
                if(data['result'] && data['result']==1){
                    transportcompanies.displayTowns(data);
                }
            }
        });
    },
    
    showChilds: function(parent_id) {
        $('#countryTowns_'+parent_id).show();
        
        $('#country_'+parent_id+' .icon-plus').addClass('icon-minus');
        $('#country_'+parent_id+' .icon-plus').removeClass('icon-plus');
        
        $('#country_'+parent_id+' .icon-minus').attr('onclick', 'transportcompanies.hideChilds('+parent_id+')');
    },
    
    hideChilds: function(parent_id) {
        $('#countryTowns_'+parent_id).hide();
        $('#country_'+parent_id+' .icon-minus').addClass('icon-plus');
        $('#country_'+parent_id+' .icon-minus').removeClass('icon-minus');
        
        $('#country_'+parent_id+' .icon-plus').attr('onclick', 'transportcompanies.showChilds('+parent_id+')');
    },

    displayTowns: function (data){
        //сперва строим страны
        this.drawCountries(data['countries']);
        //затем города
        this.drawTowns(data['towns']);
        //и напоследок отмечаем выбранные ранее
        this.drawСhecked(data['checked']);
    },
    
    drawCountries: function (countries){
        var html = '<br><br><br>';
        
        for (var countryKey in countries)
        {
            var country = countries[countryKey];
            html += '<div id="country_'+country['id']+'" style="min-height: 35px; display: block;">'+
                    '<span class="badge" style="width:250px;text-align:center;">'+
                    country['title']+
                    '</span>'+
                    '<i class="icon-plus" style="cursor:pointer;" onclick="transportcompanies.showChilds('+country['id']+')"></i>'+
                    
                    '<div id="countryTowns_'+country['id']+'" style="display: none;text-align:left;width:250px;">'+
                    '</div>'+
                    '</div>';
        }
        
        html += '<br>'+
                '<br>'+
                '<input type="button" value ="Сохранить" onclick="transportcompanies.saveTowns();" class="btn">&nbsp;&nbsp;&nbsp;'+
                '<input type="button" value ="Отмена" onclick="transportcompanies.showNavButtons();" class="btn">';
        
        $('#navButtonsTowns').html(html);
    },
    
    drawTowns: function (towns){
        for (var townKey in towns)
        {
            var html = '';
            var town = towns[townKey];
            html += '<div id="town_'+town['id']+'" style="height: 35px; display: block;">'+
                    '>&nbsp;&nbsp;&nbsp;<input type="checkbox" class="t_chk" id="t_chk_'+town['id']+'">'+
                    '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+town['title']+
                    '</div>';
                
            var countryTownsHtml = $('#countryTowns_'+town['country_id']).html();
            $('#countryTowns_'+town['country_id']).html(countryTownsHtml+html);
        }
        
    },
    
    drawСhecked: function (checked){
        for (var checkedKey in checked)
        {
            var check = checked[checkedKey];
            
            $('#t_chk_'+check['town_id']).attr('checked', 'checked');
        }
    },
    
    saveTowns: function ()
    {
        
        var t_ids_arr = new Array();
        $(".t_chk:checked").each(function(){
            var t_id_f = $(this).get(0).id;
            var t_id = jsphp.str_replace('t_chk_', '', t_id_f);
            t_ids_arr.push(t_id);
        });
        
        var t_ids_str = t_ids_arr.join(';');
        
        
        $.ajax({
            data:{id:curRowId,towns:t_ids_str},
            url: '/ajax.php?action=saveTowns&controller=module.transportcompanies.manage.controller',
            dataType : "json",
            success: function (data){
                if(data['result'] && data['result']==1){
                    transportcompanies.showNavButtons(data);
                    jAlert('Города были успешно сохранены', 'Сообщение');
                }
            }
        });
    }
}