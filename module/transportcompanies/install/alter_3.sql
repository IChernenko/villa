CREATE TABLE IF NOT EXISTS [%%]transportcompanies (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `image` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS [%%]transportcompanies_towns (
  `town_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE [%%]transportcompanies_towns ADD
      CONSTRAINT fk_transportcompanies_towns_town_id
      FOREIGN KEY (`town_id`)
      REFERENCES [%%]currencytowns(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE [%%]transportcompanies_towns ADD
      CONSTRAINT fk_transportcompanies_towns_company_id
      FOREIGN KEY (`company_id`)
      REFERENCES [%%]transportcompanies(id) ON DELETE CASCADE ON UPDATE CASCADE;