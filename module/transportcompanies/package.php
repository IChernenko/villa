<?php
global $package;
$package = array(
    'version' => '4',
    'name' => 'module.transportcompanies',
    'dependence' => array(
        'lib.jgrid',
        'lib.jsphp',
        'module.auth',
        'module.lang',
        'component.swfupload',
        'component.jalerts',
        'module.currencytowns'
    ),
    'js' => array(
        '../module/transportcompanies/js/manage/transportcompanies.js',
        )

);