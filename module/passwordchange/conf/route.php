<?php
$url = Dante_Controller_Front::getRequest()->get_requested_url();

Dante_Lib_Router::addRule('/^\/passwordchange\/$/', array(
    'controller' => 'Module_Passwordchange_Controller'
));

Dante_Lib_Router::addRule('/^\/passwordchange\/change\/?.*$/', array(
    'controller' => 'Module_Passwordchange_Controller',
    'action' => 'change',
));
