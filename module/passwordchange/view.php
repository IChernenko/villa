<?php

class Module_Passwordchange_View{
    /**
     * Отображаем выбранный файл
     * @param string $fileName
     * @param array|null $vars
     */
    protected function _draw($file, $vars=NULL){
        $tpl = new Dante_Lib_Template();
        $tpl->_vars=$vars;
        $ws = Dante_Helper_App::getWsName();
        $fileName = "../module/passwordchange/template/{$file}";
        if (file_exists("template/{$ws}/passwordchange/{$file}")) {
            $fileName = "template/{$ws}/passwordchange/{$file}";
        }
        return $tpl->draw($fileName);
    }
    /**
     * Файл отображения предложения разлогинится перед процедурой
     * @param type $vars
     */
    public function logout($vars=NULL){
        return $this->_draw('logout.html', $vars);
    }
    /**
     * Файл отображения поля для ввода email
     * @param type $vars
     */
    public function formEmail($vars=NULL){
        return $this->_draw('inputemail.html', $vars);
    }
    /**
     * Файл отображения формы ввода нового пароля
     * @param type $vars
     */
    public function formCode($vars=NULL){
        return $this->_draw('formcode.html', $vars);
    }
    /**
     * Файл отображения формы ввода нового пароля
     * @param type $vars
     */
    public function formPassword($vars=NULL){
        return $this->_draw('inputpassword.html', $vars);
    }
    /**
     * Файл отображения удачной смены пароля
     * @param type $vars
     */
    public function success($vars=NULL){
        return $this->_draw('success.html', $vars);
    }
    /**
     * Файл отображения не удачной смены пароля
     * @param type $vars
     */
    public function error($vars=NULL){
        return $this->_draw('error.html', $vars);
    }
    /**
     * Файл содержимого письма с кодом подтверждения процедуры
     * @param type $vars
     * @return type
     */
    public function mailText($vars=NULL){
        return $this->_draw('mailtext.html', $vars);
    }
    
}
?>