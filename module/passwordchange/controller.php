<?php


class Module_Passwordchange_Controller extends Dante_Controller_Base{
    /**
     * Дефолтно отображаем draw
     * @return type
     */
    protected function _defaultAction() {
        return $this->_drawAction();
    }
    /**
     * Выводим поле для ввода email на который нужно отослать ссылку для востановления
     * @return type
     */
    protected function _drawAction($error = null){
        $tpl = new Module_Passwordchange_View;
        //Если юзер авторизован то отсылаем его разлогинится.
        if(((int)Dante_Helper_App::getUid()) && !$_SERVER["REMOTE_ADDR"]!="127.0.0.1"){            
            return $tpl->logout();
        }
        $vars['error']=$error;
        return $tpl->formEmail($vars);        
    }
    
    protected function _sendAction($error = null){
        $tpl    = new Module_Passwordchange_View;        
        $mapper = new Module_Passwordchange_Mapper;
        //Проверяем наличе такого email
        $email = $this->_getRequest()->get_validate('email', 'text');
        $user = Module_User_Dmo::dmo()->byEmail($email)->fetch();
        if($user->getNumRows() <1 && !$error){
            //email такого нет - возвращаем ту же форму с оповещением об ошибке
            //если нужен ответ в json:
            if($this->_getRequest()->get('json'))
                return array(
                    'result' => 0,
                    'message' => 'Ошибка! E-mail не зарегистрирован!'
                );
            //если нет, возвращаем форму
            return $this->_drawAction("Email <strong><i>{$email}</i></strong> не зарегистрирован");
        }        
        //email есть - генерим код, сохраняем, отсылаем письмо, оповещаем об этом юзера и выводим поле для кода
        $code = Module_Passwordchange_Helper::codeGenerator();
        //$mapper->setCode($email, $code);
        $mapper->setPass($email, $code);
        $this->_sendMail($email, $code);
        $vars['email'] = $email;
        $vars['error'] = $error;
        //если нужен ответ в json:
        if($this->_getRequest()->get('json'))
            return array(
                'result' => 1,
                'message' => '<span>Новый пароль был выслан на указанный Email</span>'
            );
        //если нет:
        return $tpl->formCode($vars);
    }
    
    protected function _sendMail($email, $code){
        $service = new Module_Mail_Service_Sender();
        $tpl     = new Module_Passwordchange_View;
        
        $vars['code']=$code;
        $message    =$tpl->mailText($vars);
        $subject    = 'Запрос на смену пароля';
        $to         =$email;
        $service->send($to, $subject, $message, array(
            'fromName' => Dante_Lib_Config::get('passwordchange.fromName'),
            'fromEmail' => Dante_Lib_Config::get('passwordchange.fromEmail')
        ));
    }
    /**
     * Проверяем совпадают проверочные данные для замены пароля. Если нет то повторно выведем форму для ввода кода
     * @return type
     */
    protected function _changeAction($error=NULL){
        $tpl = new Module_Passwordchange_View;
        $request = $this->_getRequest();
        $email  = $request->get_validate('email', 'email');
        $code   = $request->get_validate('code', 'text');
        if(!$this->_validateAction($email, $code)){
            return $this->_sendAction("Введенные данные не верны. Повторите попытку.");
        }
        $vars['email'] = $email;
        $vars['code']  = $code;
        $vars['error'] = $error;
        return $tpl->formPassword($vars);
    }
    
    protected function _setPasswordAction(){
        $tpl = new Module_Passwordchange_View;
        $request = $this->_getRequest();
        $model  = new Module_Passwordchange_Model;
        $mapper = new Module_Passwordchange_Mapper;        
        $email  = $request->get_validate('email', 'email');
        $code   = $request->get_validate('code', 'text');
        if(!$this->_validateAction($email, $code)){
            return $this->_sendAction("Введенные данные не верны. Введите код повторно.");
        }
        
        $password = $request->get_validate('password1', 'password');
        $password2 = $request->get_validate('password1', 'password');
        if($password != $password2){
            return $this->_changeAction('Введенные пароли не совпадают. Введите данные повторно');
        }
        //Прошли провеки - меняем пароль и стираем код из БД
        $model->email       = $email;
        $model->password    = $password;
        $model->restory_code = null;
        $mapper->setNewPasswordByEmail($model);        
        return $tpl->success();
    }

    /**
    * проверяем наличие юзера по email И code
    */
    protected function _validateAction($email, $code){        
        return $user = Module_User_Dmo::dmo()->byCode($email, $code)->fetch()->getNumRows();
    }
    
}
?>