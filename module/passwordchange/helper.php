<?php
class Module_Passwordchange_Helper {
    
    /**
     * генерируем код
     * @return string
     */
    static function codeGenerator(){
        $chars="qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP";
        $code=null;
        $size=StrLen($chars)-1; 
        for($max=10;$max>0;$max--)
            $code.=$chars[rand(0,$size)];        
        return $code;
    }
} 
?>