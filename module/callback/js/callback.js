/**
 * Запомнить номер телефона
 */
function postCallback() {
    var phoneNumber = $('#phoneNumber').val();
    if (phoneNumber.length == 0) { 
        jAlert('Введите ваш номера', 'Операция неуспешна');
        return false;
    }

    if (!phoneNumberValidate(phoneNumber)) {
        jAlert('Неверный формат номера', 'Операция неуспешна');
        return false;
    }

    $.ajax({
        type: "POST",
        url: '/ajax.php',
        data: {
            controller: "module.callback.controller",
            action: 'add',
            phone: phoneNumber
        },
        success: function(data) {
            if (data.result == 1) {
                jAlert('Ваш номер записан', 'Операция успешна');
            }
        }
    });
}

function phoneNumberValidate(phoneNumber) {
    var pattern = /^\+\d{12}$/;
    var re = new RegExp(pattern);
    return re.test(phoneNumber);
}