<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 29.05.12
 * Time: 19:21
 * To change this template use File | Settings | File Templates.
 */
class Module_Callback_Controller extends Dante_Controller_Base
{
    protected function _addAction() {
        $phone = $this->_getRequest()->get('phone');

        $mapper = new Module_Callback_Manage_Mapper();

        $model = new Module_Callback_Manage_Model();
        $model->phone = $phone;

        return array(
            'result' => (int)$mapper->add($model)
        );
    }
}
