CREATE TABLE IF NOT EXISTS [%%]hotel_callback (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(20) NOT NULL,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='обратный звонок' AUTO_INCREMENT=1 ;