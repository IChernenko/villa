<?php
global $package;
$package = array(
    'version' => '3',
    'name' => 'module.callback',
    'dependence' => array(
        'lib.jdatepicker',
        'lib.jgrid',
        'module.auth',
        'module.lang',
        'component.tinymce',
        'component.jalerts'
    ),
    'js' => array('../module/callback/js/callback.js',
       '../module/callback/js/manage/callback.js' ),

);