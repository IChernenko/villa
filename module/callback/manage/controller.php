<?php
/**
 * Description of callback
 *
 * @author Mort
 */

class Module_Callback_Manage_Controller extends Lib_Jgrid_Controller_Base {
    
    /**
     * редактирование/добавление/удаление записей грида
     */
    protected function _checkNotSeenAction() {
        $mapper = new Module_Callback_Manage_Mapper();
        $count = $mapper->getNotSeenAction();
        
        return array(
            'result' => 1,
            'count' => $count
        );
    }
    
    /**
     * отрисовка стартового html
     * @return string 
     */
    protected function _defaultAction()
    {
        $mapper = new Module_Callback_Manage_Mapper();
        $mapper->updateSeenAction();
        
        
        $hotelHelper = new Module_Division_Helper();
        $hotelHelper->checkAccess();
        
        
        $params = array(
            'navId' => 'navgrid',
            'pagerId' => 'pagernav',
            'colNames' => "'№','Телефон','Статус'",
            'colModel' => array(
                array(
                    'name'=>"'id'",
                    'index'=>"'id'", 
                    'width'=>50,
                    'align' => '"center"',
                    'formatter'=>"'integer'",
                    'editable'=>'false'
                    ),
                array(
                    'name'=>"'phone'",
                    'index'=>"'phone'", 
                    'width'=>150,
                    'align' => '"center"',
                    'editable'=>'true',
                    'editoptions'=>array(
                        'readonly' => 'false',
                        'size' => 10,
                        'maxlength' => 20
                        ),
                    'editrules'=>array(
                        'required'=>'true',
                        'custom'=>'true',
                        'custom_func'=>'callback.checkFormat'
                        ),
                    'formoptions' => array(
                        'label'=> "'Телефон<font color=red>*</font>'"
                            )
                    ),
                array(
                    'name'=>"'status'",
                    'index'=>"'status'", 
                    'width'=>150,
                    'align' => '"center"',
                    'editable'=>'true',
                    'editoptions'=>array(
                        'readonly' => 'false',
                        'size' => 10,
                        'maxlength' => 50
                        )
                    )
                ),
                'sortname' => 'id',
                'caption' => 'Обратный звонок',
                'url' => 'ajax.php?action=draw&controller=module.callback.manage.controller&q=1&language=1',
                'editurl' => 'ajax.php?action=edit&controller=module.callback.manage.controller&q=1&language=1',
                'height' => '300',
            'editOptionsA' => array(
                    'width' => '450',
                    'height' => '250',
                    'reloadAfterSubmit' => 'true',
                    'closeAfterEdit' => 'true'
                ),
            'addOptionsA' => array(
                        'width' => '450',
                        'height' => '250',
                        'reloadAfterSubmit' => 'true',
                        'closeAfterAdd' => 'true'
                    )
        );
        
        $this->setParams($params);
        $grid = $this->getHTML();
        
        return $grid;
    }
    
    /**
     * формируем модель грида
     * @return Lib_Jgrid_Model_Base 
     */
    protected function _drawAction() {
        
        $requestParams = array(
            'searchParams' => array(
                'id',
                'phone',
                'status'
            )
        );
        $this->setRequestParams($requestParams);
        $params = $this->getRequestByParams();
        
        $mapper = new Module_Callback_Manage_Mapper();
        $users = $mapper->getUsers($params);
        
        $users->page = $params['page']; 
        $users->total = ceil($users->records/$params['rows']);

        return $users;
    }
    
    /**
     * редактирование/добавление/удаление записей грида
     */
    protected function _editAction() {
        $hotelHelper = new Module_Division_Helper();
        $hotelHelper->checkAccess();
        
        $operationAction = $this->_getRequest()->get('oper'); // add/edit/del

        
        $table = new Module_Callback_Manage_Model();
        $table->id = $this->_getRequest()->get('id');
        $table->phone = $this->_getRequest()->get('phone');
        $table->status = $this->_getRequest()->get('status');
        
        $mapper = new Module_Callback_Manage_Mapper();
        switch ($operationAction) {
            case 'add':
                $mapper->add($table);
                break;
            case 'edit':
                $mapper->update($table);
                break;
            case 'del':
                $mapper->del($table->id);
                break;
        }
    }
    
}

?>
