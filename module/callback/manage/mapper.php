<?php

/**
 * Description of callback
 *
 * @author Mort
 */
class Module_Callback_Manage_Mapper {
    
    protected $_tableName = '[%%]hotel_callback';
    
    public function updateSeenAction() {
        //теперь пополнить бы счет данного юзера
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $cond = array(
            'view_flag'=>0
            );
        
        //апдейтим запись
        $table->view_flag = 1;
        $table->update($cond);
        
    }
    
    public function getNotSeenAction() {
        $sql = "select id from ".$this->_tableName." WHERE view_flag = 0";

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $phones = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $phones[] = (int)$f['id'];
        }

        return count($phones);
    }
    /**
     * выбираем номера
     * @param array $params
     * @return Lib_Jgrid_Model_Base 
     */
    public function getUsers($params) {
        $jgridMapper = new Lib_Jgrid_Mapper_Base();
        $rowsArray = $jgridMapper->getRowsByParams($this->_tableName, $params);
        $categoriesPrep = $rowsArray['rows'];
        $rowCount = $rowsArray['rowCount'];
        
        $categories = new Lib_Jgrid_Model_Base();
        $i=0;
        foreach ($categoriesPrep as $k=>$f){
            //id 	email 	password 	rating
            $categories->rows[$i]['id'] = $f['id'];
            $categories->rows[$i]['cell'] = array(
                                                $f['id'],
                                                $f['phone'],
                                                $f['status']
                                            );
            $i++;
        }
        
        $categories->records = $rowCount;
        
        return $categories;
    }
    
    /**
     * добавление номера
     * @param Module_Hotel_Model_Manage_Apartments $params 
     */
    public function add(Module_Callback_Manage_Model $params) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->phone = $params->phone;
        $table->view_flag = 1;
        if ($params->status) {
        $table->status = $params->status;
        }

        $table->id = $table->insert();
        return true;
    }
    
    /**
     * обновляем номер
     * @param Module_Hotel_Model_Manage_Apartments $params 
     */
    public function update(Module_Callback_Manage_Model $params) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->id = $params->id;
        $table->phone = $params->phone;
        if ($params->status) {
            $table->status = $params->status;
        }

        return $table->apply(array(
            'id' => $params->id
        ));
    }
    
    /**
     * удаляем номер
     * @param int $id 
     */
    public function del($id) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->delete(array(
            'id' => $id
        ));
    }
    
}

?>