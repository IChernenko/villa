<?php

class Module_Airlines_Controller extends Dante_Controller_Base
{
    protected function _defaultAction()
    {

    }

    /**
     * Выбор городов и формирование html для заполнения селектора городов в разделе
     * "Каталог авиакомпаний, прибывающих в г. Киев" на стартовой странице.
     * @return array
     */
    protected function _getTownsAction()
    {
        $mapper = new Module_Currencytowns_Mapper();
        $lang = Module_Lang_Helper::getCurrent();
        $countryId = $this->_getRequest()->get('countryId');
        $townsArr = $mapper->getList($countryId, $lang);

        $townsSelectorOptions = '<option value="0">-- Выберите --</option>';
        foreach($townsArr as $id => $val)
        {
            $townsSelectorOptions .=
                "<option value='{$id}'>{$val}</option>";
        }
        return array(
            'result' => 1,
            'townsSelectorOptions' => $townsSelectorOptions
        );
    }

    /**
     * Формирование результата поиска (html) для заполнения области отображения результата в разделе
     * "Каталог авиакомпаний, прибывающих в г. Киев" на стартовой странице.
     * @return array
     */
    protected function _searchAirlinesAction()
    {
        $airlinesMapper = new Module_Airlines_Mapper();
        $countryId = $this->_getRequest()->get('countryId');
        $cityId = $this->_getRequest()->get('cityId');
        $airlinesInfo = $airlinesMapper->searchAirlines($countryId, $cityId);
        
        if(count($airlinesInfo) == 0)
            return array(
                'result' => 1,
                'searchResultHtml' => '<p>Авиакомпании не найдены</p>'
            );

        $searchResultHtml = '';
        if(count($airlinesInfo) < 5)
        {
            $searchResultHtml .= '<ul id="airlinesSearchResult">';
        }
        else
        {
            $searchResultHtml .= '<ul class="scrollable" id="airlinesSearchResult">';
        }

        foreach($airlinesInfo as $item)
        {
            $searchResultHtml .= '<li onclick="airlines.viewGalleryImage(event);">
                                      <a href="'.$item->website.'" target="_blank">
                                          <img src="'.$item->logo.'"/>
                                      </a>
                                  </li>';
        }
        $searchResultHtml .= "</ul>";

        return array(
            'result' => 1,
            'searchResultHtml' => $searchResultHtml
        );
    }
}