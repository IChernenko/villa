CREATE TABLE IF NOT EXISTS `[%%]airlines_destination` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `airline_id` INT(11) NOT NULL,
    `country_id` INT(11) NOT NULL,
    `town_id` INT(11) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

ALTER TABLE `[%%]airlines_destination` ADD CONSTRAINT fk_airlines_dest_id
    FOREIGN KEY (`airline_id`) REFERENCES `[%%]airlines`(`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `[%%]airlines_destination` ADD CONSTRAINT fk_airlines_dest_country_id
    FOREIGN KEY (`country_id`) REFERENCES `[%%]currencycountries`(`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `[%%]airlines_destination` ADD CONSTRAINT fk_airlines_dest_town_id
    FOREIGN KEY (`town_id`) REFERENCES `[%%]currencytowns`(`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;