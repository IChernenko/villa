DROP TABLE `[%%]airlines_destination`;

ALTER TABLE `[%%]airlines` DROP FOREIGN KEY `fk_airlines_country_id`;
ALTER TABLE `[%%]airlines` DROP FOREIGN KEY `fk_airlines_town_id`;

ALTER TABLE `[%%]airlines` DROP COLUMN `country_id`;
ALTER TABLE `[%%]airlines` DROP COLUMN `town_id`;

CREATE TABLE `[%%]airlines_flights` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `airline_id` INT(11) NOT NULL,
    `flight_number` INT(11) NOT NULL,
    `dep_country_id` INT(11) NOT NULL,
    `dep_town_id` INT(11) NOT NULL,
    `dest_country_id` INT(11) NOT NULL,
    `dest_town_id` INT(11) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

ALTER TABLE `[%%]airlines_flights` ADD CONSTRAINT fk_airlines_flights_id
    FOREIGN KEY (`airline_id`) REFERENCES `[%%]airlines`(`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `[%%]airlines_flights` ADD CONSTRAINT fk_airlines_flights_dep_country_id
    FOREIGN KEY (`dep_country_id`) REFERENCES `[%%]currencycountries`(`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `[%%]airlines_flights` ADD CONSTRAINT fk_airlines_flights_dep_town_id
    FOREIGN KEY (`dep_town_id`) REFERENCES `[%%]currencytowns`(`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `[%%]airlines_flights` ADD CONSTRAINT fk_airlines_flights_dest_country_id
    FOREIGN KEY (`dest_country_id`) REFERENCES `[%%]currencycountries`(`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `[%%]airlines_flights` ADD CONSTRAINT fk_airlines_flights_dest_town_id
    FOREIGN KEY (`dest_town_id`) REFERENCES `[%%]currencytowns`(`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;