CREATE TABLE IF NOT EXISTS `[%%]airlines` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `country_id` INT(11) NOT NULL,
    `town_id` INT(11) NOT NULL,
    `name` VARCHAR(100) NOT NULL,
    `logo` VARCHAR(100) DEFAULT NULL,
    `website` VARCHAR(100) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

ALTER TABLE `[%%]airlines` ADD CONSTRAINT fk_airlines_country_id
    FOREIGN KEY (`country_id`) REFERENCES `[%%]currencycountries`(`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `[%%]airlines` ADD CONSTRAINT fk_airlines_town_id
    FOREIGN KEY (`town_id`) REFERENCES `[%%]currencytowns`(`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;