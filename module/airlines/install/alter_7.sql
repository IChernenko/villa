ALTER TABLE `[%%]airlines_flights` DROP COLUMN `flight_number`;

ALTER TABLE `[%%]airlines_flights` DROP FOREIGN KEY `fk_airlines_flights_dest_country_id`;
ALTER TABLE `[%%]airlines_flights` DROP FOREIGN KEY `fk_airlines_flights_dest_town_id`;
ALTER TABLE `[%%]airlines_flights` DROP FOREIGN KEY `fk_airlines_flights_dep_country_id`;
ALTER TABLE `[%%]airlines_flights` DROP FOREIGN KEY `fk_airlines_flights_dep_town_id`;

ALTER TABLE `[%%]airlines_flights` DROP COLUMN `dest_country_id`;
ALTER TABLE `[%%]airlines_flights` DROP COLUMN `dest_town_id`;

ALTER TABLE `[%%]airlines_flights` CHANGE `dep_country_id` `country_id` INT(11) NOT NULL;
ALTER TABLE `[%%]airlines_flights` CHANGE `dep_town_id` `town_id` INT(11) NOT NULL;

ALTER TABLE `[%%]airlines_flights` ADD CONSTRAINT fk_airlines_flights_country_id
    FOREIGN KEY (`country_id`) REFERENCES `[%%]currencycountries`(`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `[%%]airlines_flights` ADD CONSTRAINT fk_airlines_flights_town_id
    FOREIGN KEY (`town_id`) REFERENCES `[%%]currencytowns`(`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;