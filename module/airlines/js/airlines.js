airlines = 
{
    controller: 'module.airlines.manage.controller',
    editSubmit: function(obj)
    {
        if($(obj).hasClass('disabled') || !this.validate($('#editAirline'))) return false;
        $('.func').addClass('disabled');
        
        var data = $('#editAirline').serializeArray();
        data.push(
            {name: 'controller', value: airlines.controller},
            {name: 'action', value: 'edit'}
        );
        $.ajax({
            type: 'POST',
            data: data,
            url: '/ajax.php',
            dataType : "json",
            success: function (data){
                if(data.result==1)
                {
                    $('#modelID').val(data.id);
                    jAlert('Изменения сохранены', 'Сообщение', function(){                        
                        $('.func').removeClass('disabled');
                    });                  
                }
                else console.log(data);
            }
        });
    },
    
    validate: function(form)
    {
        var inputs = $(form).find('.required:visible');
        for(var i=0; i<inputs.length; i++)
        {
            if(!inputs.eq(i).val() || inputs.eq(i).val() == 0)
            {
                jAlert('Не заполнено обязательное поле!', 'Ошибка', function(){
                    inputs.eq(i).focus();
                });
                return false;
            }
        }
        return true;
    },

    /**
     * Загрузка содержимого в панель редактирования авиакомпании
     * @param object
     * @returns {boolean}
     */
    loadContent: function(object)
    {
        var event_id = $(object).attr('href');
        var data = {
            controller: airlines.controller
        };

        if(!(data.action = airlines.getAction(event_id)))
            return false;

        data.airlineId = $('#airlineId').val();

        $(event_id).html(airlines.loaderHtml);

        $.ajax({
            type: 'POST',
            data: data,
            url: '/ajax.php',
            dataType : "json",
            success: function (data){
                if(data.result==1){
                    $(event_id).html(data.html);
                }else{
                    $(event_id).html(data.message);
                }
            }
        });
    },

    /**
     * Получить действие для вкладки панели редактирования авиакомпании
     * @param id
     * @returns {boolean}
     */
    getAction: function(id)
    {
        var action = false;

        switch(id)
        {
            case '#edit':
                break;
            case '#destinations':
                action = 'drawFlights';
                break;
        }
        return action;
    },

    /**
     * Обработчик изменения состояния селектора страны
     */
    searchFlights: function()
    {
        var country = $('#country').val();
        if(country == 0) return;
        
        var airlineId = $('#airlineId').val();

        $.ajax({
            type: 'POST',
            url: '/ajax.php',
            data:
            {
                controller: 'module.airlines.manage.controller',
                action: 'searchFlights',
                airline_id: airlineId,
                country_id: country
            },
            success: function(data)
            {
                if(data.result == 1)
                {
                    $('#cityTable').html(data.html);
                }
                else
                {
                    jAlert(data.message);
                    console.log(data.message);
                }
            }
        });
    },

    /**
     * Обработчик нажатия кнопки "Сохранить" на форме редактирования пунктов назначения
     * @param object
     */
    saveFlights: function()
    {
        var data = $('#editAirlineFlights').serializeArray();
        $.ajax({
            type: 'POST',
            url: '/ajax.php',
            dataType: 'JSON',
            data: data,
            success: function(data)
            {
                if(data.result == 1)
                {
                    if(data.html)
                        $('#cityTable').html(data.html);
                    
                    jAlert('Данные успешно сохранены');
                }
                else
                {
                    console.log(data);
                }
            }
        });
    },

    loaderHtml:'<h1>Loading...</h1>'+
        '<div class="progress progress-striped active" style="width:150px; margin:0 auto;">'+
        '<div class="bar" style="width: 100%;"></div></div>',

    /**
     * Функция-обработчик изменения селектора выбора страны в разделе
     * "Каталог авиакомпаний и поездов, прибывающих в г. Киев" на стартовой странице.
     * @param object - объект селектора
     */
    searchTownsByCountry: function(object)
    {
        var countryId = $(object).val();
        $.ajax({
            type: 'POST',
            url: '/ajax.php',
            data:
            {
                controller: 'module.airlines.controller',
                action: 'getTowns',
                countryId: countryId
            },
            success: function(data)
            {
                if(data.result == 1)
                {
                    $('#citySelect').html(data.townsSelectorOptions);
                }
                else
                {
                    jAlert(data.message);
                }
            }
        });
    },

    /**
     * Функция-обработчик нажатия кнопки "Поиск" в разделе
     * "Каталог авиакомпаний, прибывающих в г. Киев" на стартовой странице.
     * @param object - объект кнопки
     */
    searchAirlines: function(object)
    {
        var countryId = $('#countrySelect').val();
        var cityId = $(object).val();

        if(cityId == 0) 
        {
            jAlert('Выберите город', 'Сообщение');
            return;
        }

        $.ajax({
            type: 'POST',
            url: '/ajax.php',
            data:
            {
                controller: 'module.airlines.controller',
                action: 'searchAirlines',
                countryId: countryId,
                cityId: cityId
            },
            success: function(data)
            {
                if(data.result == 1)
                {
                    $('#searchResult').find('td').html(data.searchResultHtml);
                    /*$('.scrollable').smoothDivScroll({
                        mousewheelScrolling: true,
                        manualContinuousScrolling: true,
                        visibleHotSpotBackgrounds: "always"
                    });*/
                    //$('.scrollableArea').css('width', '1127px');
                    var height= $('body').height();
                    $('#searchResult').find('td ul').fadeIn('slow');
                    $('html, body').animate({"scrollTop":height},2000);
                }
                else
                {
                    jAlert(data.message, 'Ошибка');
                    console.log(data.message);
                }
            }
        });
    }
}