<?php
/**
 * Description of view
 *
 * @author Valkyria
 */
class Module_Airlines_Manage_View_Flights
{
    public $airline;    
    public $countryList;
    
    public $townList;
     

    /**
     * Отрисовка формы редактирования рейсов
     * @param $countryList - Страны
     * @param $airline - Текущая авиакомпания
     * @return mixed|string
     */   
    public function drawForm()
    {
        $tpl = new Dante_Lib_Template();
        $filePath = '../module/airlines/manage/template/form_flight.html';
        
        $tpl->airline = $this->airline;
        $tpl->countries = $this->countryList;
        return $tpl->draw($filePath);
    }
    
    /**
     * Отрисовка таблицы рейсов по выбранным странам
     * @return type 
     */
    public function drawFlights($flightList)
    {
        $tpl = new Dante_Lib_Template();
        $filePath = '../module/airlines/manage/template/table_flight.html';
        
        $tpl->townList = $this->townList;
        $tpl->flightsList = $flightList;
        return $tpl->draw($filePath);
        
    }
}

?>
