<?php
/**
 * Description of view
 *
 * @author Valkyria
 */
class Module_Airlines_Manage_View_Airlines 
{
    public $countryList;
    
    public $townList;
    
    public $upload;
    
    public function drawForm($model)
    {
        $tpl = new Dante_Lib_Template();
        $filePath = '../module/airlines/manage/template/form.html';
        $tpl->countryList = $this->countryList;
        $tpl->townList = $this->townList;
        $tpl->upload = $this->upload;
        $tpl->model = $model;
        return $tpl->draw($filePath);
    }
}

?>
