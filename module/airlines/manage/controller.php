<?php
/**
 * Description of controller
 *
 * @author Valkyria
 */

class Module_Airlines_Manage_Controller extends Module_Handbooksgenerator_Controller_Base 
{   
    protected function _init() 
    {
        parent::_init();
        $this->_mapper = new Module_Airlines_Manage_Mapper();
    }

    /**
     * отрисовка стартового html
     * @return string 
     */
    protected function _drawAction()
    {
        $model = new Module_Airlines_Manage_Model_Airlines();
                
        $params = $this->getParams($model);
        $list = $this->_mapper->getRowsByParams($params);
        
        $this->formEnable('module.airlines.manage.controller');
        
        $titles = array(            
            array(
                'content' => 'ID',
                'sortable' => 'id'
            ),
            array(
                'content' => 'Название',
                'sortable' => 'name'
            ),
            array(
                'content' => 'Логотип'
            ),
            array(
                'content' => 'Сайт',
                'sortable' => 'website'
            ),
            'Функции'
        );
        
        $filters = array(
            NULL,
            'name' => array(
                'type' => 'text',
                'value' => $this->_getRequest()->get('name')
            ),
            NULL,
            'website' => array(
                'type' => 'text',
                'value' => $this->_getRequest()->get('website')
            ),            
            'button_filter'=>array(
                'type'=>'button_filter'
            )
        );
        
        $this->thead = array($titles, $filters);
        
        foreach($list['rows'] as $row)
        {
            $logo = $row['logo']?
                    Module_Airlines_Helper::getImgPath().$row['logo']:
                    Lib_Image_Helper::getNoImage();
            
            $cur = array();
            $cur['id'] = $row['id'];
            $cur['name'] = $row['name'];
            $cur['logo'] = '<img src="'.$logo.'" style="height: 150px;"/>';
            $cur['website'] = $row['website'];
            $cur['buttons'] = array(
                'id' => $row['id'],
                'type' => array('edit', 'del'),                
            );  
            $this->tbody[$cur['id']] = $cur;
        }
        
        $this->tfoot = array(
            array(
                $this->paginator($list['count'], $params)
            ),
            array(
                'button_filter' => array(
                    'type' => array('edit'),                    
                )
             )  
        );
        
        return $this->generate();
    }
    
    protected function _drawFormAction()
    {
        $id = $this->_getRequest()->get_validate('id', 'int', 0); 
        $model = $this->_mapper->getById($id);
                
        $view = new Module_Airlines_Manage_View_Airlines();
        $uploader = new Module_Uploadify_Controller_Base();
        
        $view->upload = array(
            'swf_url' => $uploader->swf_url,
            'server_url' => $uploader->server_url
        );
        
        return array(
            'result' => 1,
            'html' => $view->drawForm($model)
        );
    }

    /**
     * Функция-загрузчик изображений для Авиалиний
     * @return array
     */
    protected function _imageUploadAction()
    {
        if(!isset($_FILES['file']))
            return array(
                'result' => 0,
                'message' => 'Нет файла для загрузки'
            );
        $uploadDir = Module_Airlines_Helper::getImgPath();
        if (!is_dir($uploadDir)) mkdir($uploadDir, 0755, true);

        $newName = md5(time());
        $uploader = new Lib_JqueryUpload_Uploader();
        $uploader->globalKey = 'file';
        $image = $uploader->upload($uploadDir, $newName);
        
        return array(
            'result' => 1,
            'image' => $image,
            'imageDir' => $uploadDir
        );
    } 


    /**
     * редактирование/добавление/удаление записей грида
     */
    protected function _editAction() 
    {
        $model = new Module_Airlines_Manage_Model_Airlines();
        $model->id = $this->_getRequest()->get('id');
        $model->name = $this->_getRequest()->get('name');
        $model->website = $this->_getRequest()->get('website');
        
        $model->logo = $this->_getRequest()->get('logo');
        if($model->id)
        {
            $curImage = $this->_mapper->getLogo($model->id);
            $imgDir = Module_Airlines_Helper::getImgPath();
            if($curImage != $model->logo && is_file($imgDir.$curImage))
            {
                unlink($imgDir.$curImage);
            }
        }
        
        $id = $this->_mapper->apply($model);
        
        return array(
            'result' => 1,
            'id' => $id
        );
    }

    /**
     * Формирование панели редактирования пунктов назначения
     * @param bool $airlineId - ID авиакомпании
     * @return array
     */
    protected function _drawFlightsAction($airlineId = false)
    {
        if(!$airlineId) $airlineId = $this->_getRequest()->get('airlineId');
        if(!$airlineId)
            return array(
                'result' => 0,
                'message' => 'incorrect id'
            );
        
        $countryMapper = new Module_Currencycountries_Manage_Mapper();
        $countries = $countryMapper->getNames();

        $airline = $this->_mapper->getById($airlineId);
        $airline->logo = Module_Airlines_Helper::getImgPath().$airline->logo;

        $view = new Module_Airlines_Manage_View_Flights();
        $view->countryList = $countries;
        $view->airline = $airline;

        $html = $view->drawForm();

        return array(
            'result' => 1,
            'html' => $html
        );
    }

    /**
     * Обработка изменения состояния селектора выбора страны на форме
     * редактирования рейсов для авиакомпании
     * @return array
     */
    protected function _searchFlightsAction()
    {
        $request = $this->_getRequest();
        $airlineId = $request->get('airline_id');
        $countryId = $request->get('country_id');

        $townMapper = new Module_Currencytowns_Manage_Mapper();        
        $towns = $townMapper->getNames($countryId);
                
        if(!count($towns))
        {
            $html = '<p class="lead text-error" style="width: 100%; text-align: center;">
                        Для выбранной страны не указаны города
                    </p>';
        }
        else
        {
            $flights = $this->_mapper->getFlightsTownsByCountry($airlineId, $countryId);
            
            $view = new Module_Airlines_Manage_View_Flights();
            $view->townList = $towns;
            $html = $view->drawFlights($flights);
        }
        
        return array(
            'result' => 1,
            'html' => $html
        );
    }

    /** 
     * Сохранение данных о пунктах назначения
     * @return array
     */
    protected function _saveFlightsAction()
    {
        $request = $this->_getRequest();
        $model = new Module_Airlines_Manage_Model_Flights();
        $model->airline_id = $request->get('airline_id');
        $model->flight_number = $request->get('flight_number');
        $model->country_id = $request->get('country_id');
        
        $flights = $request->get('flights');
        
        $this->_mapper->saveFlights($model, $flights);
        
        return $this->_searchFlightsAction();
    }
}

?>
