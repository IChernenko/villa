<?php

/**
 * Description of callback
 *
 * @author Mort
 */
class Module_Airlines_Manage_Model_Airlines 
{    
    public $sortable_rules = array(
        'name' => 'id',
        'type' => 'asc'
    );
    
    public $filter_rules = array(
        array(
            'name'=>'name',
            'format'=>'`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ),
        array(
            'name'=>'website',
            'format'=>'`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ),
    );
    
    public $id;
    
    public $name;
    
    public $logo;
    
    public $website;
}

?>
