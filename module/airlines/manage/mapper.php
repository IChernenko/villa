<?php

/**
 * Description of mapper
 *
 * @author Valkyria
 */
class Module_Airlines_Manage_Mapper extends Module_Handbooksgenerator_Mapper_Base
{    
    protected $_tableName = '[%%]airlines';
    protected $_tableFlights = '[%%]airlines_flights';
    
    public function getById($id)
    {
        $model = new Module_Airlines_Manage_Model_Airlines();
        if(!$id) return $model;
        
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->select(array('id' => $id));
        
        $model->id = $table->id;
        $model->name = $table->name;
        $model->logo = $table->logo;
        $model->website = $table->website;
        
        return $model;
    }

    public function getLogo($id)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        return $table->select(array('id' => $id))->logo;
    }
    
    public function apply(Module_Airlines_Manage_Model_Airlines $model)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->name = $model->name;
        $table->logo = $model->logo;
        $table->website = $model->website;
        
        if($model->id) $table->update(array('id' => $model->id));
        else $model->id = $table->insert();
        
        return $model->id;
    }
    
    public function getNames()
    {
        $this->paginator = false;
        
        $rows = $this->getRowsByParams();
        $list = array();
        foreach($rows['rows'] as $id => $item)
        {
            $list[$id] = $item['name'];
        }
        return $list;
        
        $this->paginator = true;
    }
    
    public function getFlightsTownsByCountry($airlineId, $countryId)
    {
        $sql = "SELECT `id`, `town_id` FROM {$this->_tableFlights} 
                    WHERE `airline_id` = {$airlineId}
                    AND `country_id` = {$countryId}
                    ORDER BY `id` ASC";
                    
        $towns = array();
        if(!$r = Dante_Lib_SQL_DB::get_instance()->open($sql)) return $towns;
        
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {            
            $towns[$f['id']] = $f['town_id'];
        }
        
        return $towns;
    }
    
    public function saveFlights($model, $flights)
    {
        if(!is_array($flights) || count($flights) == 0)
        {
            $sql = "DELETE FROM {$this->_tableFlights} WHERE `country_id` = {$model->country_id}";
            Dante_Lib_SQL_DB::get_instance()->exec($sql);
            
            return;
        }
        
        $townIds = array_keys($flights);        
        $strTownIds = implode(', ', $townIds);
        
        $sql = "DELETE FROM {$this->_tableFlights} 
                WHERE `country_id` = {$model->country_id} 
                AND `town_id` NOT IN ({$strTownIds})";
        Dante_Lib_SQL_DB::get_instance()->exec($sql);
        
        foreach($flights as $townId => $flightId)
        {
            if($flightId != 0) continue;
            
            $table = new Dante_Lib_Orm_Table($this->_tableFlights);
            $table->airline_id = $model->airline_id;
            $table->country_id = $model->country_id;
            $table->town_id = $townId;            
            $table->insert();
        }
    }
}

?>
