<?php
global $package;
$package = array(
    'version' => '7',
    'name' => 'module.airlines',
    'dependence' => array(
        'lib.jqueryUpload',
        'module.currencycountries',
        'module.currencytowns',
        'component.jalerts'
    ),
    'js' => array(
        '../module/airlines/js/airlines.js'
    )

);