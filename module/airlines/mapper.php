<?php

/**
 * Description of mapper
 *
 * @author Valkyria
 */

class Module_Airlines_Mapper 
{    
    protected $_tableName = '[%%]airlines';
    protected $_tableFlights = '[%%]airlines_flights';
    protected $_countriesLang = '[%%]currencycountries_lang';
    protected $_townsLang = '[%%]currencytowns_lang';
    
    /**
     * Выбрать информацию об авиакомпаниях согласно текущей локали сайта
     * @param $lang
     * @return array
     */
    public function getListByCountry($countryId)
    {
        $sql = "SELECT
                    `a`.`id`,
                    `a`.`name`,
                    `a`.`logo`,
                    `a`.`website`
                FROM {$this->_tableName} AS `a`
                LEFT JOIN {$this->_tableFlights} AS `af` ON(`a`.`id` = `af`.`airline_id`)
                WHERE `af`.`country_id` = {$countryId}";
                    
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $list = array();

        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) 
        {   
            if(!$f['name'] || !$f['logo']) continue;
            
            $model = new Module_Airlines_Manage_Model_Airlines();
            $model->id = $f['id'];
            $model->name = $f['name'];
            $model->logo = Module_Airlines_Helper::getImgPath().$f['logo'];
            $model->website = $f['website'];
            $list[$model->id] = $model;
        }
        return $list;
    }

    /**
     * Выбрать информацию о авиакомпаниях согласно выбранных параметров (страны и города)
     * @param $countryId - ID страны
     * @param $cityId - ID города
     * @param $lang - ID языка
     * @return array
     */
    public function searchAirlines($countryId, $cityId)
    {
        $sql = "SELECT 
                    DISTINCT(`a`.`id`),
                    `a`.`name`,
                    `a`.`logo`,
                    `a`.`website` 
                FROM {$this->_tableName} AS `a`
                LEFT JOIN {$this->_tableFlights} AS `af` ON(`a`.`id` = `af`.`airline_id`)
                WHERE `af`.`country_id` = {$countryId} AND `af`.`town_id` = {$cityId}";
                
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        
        $list = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            if(!$f['name'] || !$f['logo']) continue;
            
            $model = new Module_Airlines_Manage_Model_Airlines();
            $model->id = $f['id'];
            $model->name = $f['name'];
            $model->logo = Module_Airlines_Helper::getImgPath().$f['logo'];
            $model->website = $f['website'];
            $list[$model->id] = $model;
        }
        return $list;
    }
    
    public function getCountries($lang = false)
    {
        if(!$lang) $lang = 2;
        
        $sql = "SELECT * FROM {$this->_tableFlights} AS `f`
                LEFT JOIN {$this->_countriesLang} AS `cl`
                ON(`f`.`country_id` = `cl`.`country_id` AND `cl`.`set_lang` = {$lang})
                ORDER BY `cl`.`disp_name` ASC";
                
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $list = array();

        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) 
        {
            $list[$f['country_id']] = $f['disp_name'];
        }
        return $list;
    }
}

?>
