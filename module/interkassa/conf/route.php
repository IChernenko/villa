<?php

Dante_Lib_Router::addRule('/^\/interkassa\/test$/', array(
    'controller' => 'Module_Interkassa_Controller_Test'
));

Dante_Lib_Router::addRule('/^\/interkassa\/test\/status$/', array(
    'controller' => 'Module_Interkassa_Controller_Test',
    'action' => 'status'
));



Dante_Lib_Router::addRule('/^\/interkassa\/success/', array(
    'controller' => 'Module_Interkassa_Controller',
    'action' => 'success'
));

// статус платежа
Dante_Lib_Router::addRule('/^\/interkassa\/status/', array(
    'controller' => 'Module_Interkassa_Controller',
    'action' => 'status'
));

Dante_Lib_Router::addRule('/^\/interkassa\/fail/', array(
    'controller' => 'Module_Interkassa_Controller',
    'action' => 'fail'
));
