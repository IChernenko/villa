<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 10/24/13
 * Time: 4:48 PM
 * To change this template use File | Settings | File Templates.
 */

Dante_Lib_Config::set('interkassa.shopId',  null);
//Dante_Lib_Config::set('interkassa.secretKey',  null);

Dante_Lib_Config::set('interkassa.paysystem_alias',  '');

Dante_Lib_Config::set('interkassa.success_url',  Dante_Lib_Config::get('url.domain_main').'/interkassa/success');
Dante_Lib_Config::set('interkassa.success_method',  'POST');

Dante_Lib_Config::set('interkassa.fail_url',  Dante_Lib_Config::get('url.domain_main').'/interkassa/fail');
Dante_Lib_Config::set('interkassa.fail_method',  'POST');

Dante_Lib_Config::set('interkassa.status_url',  Dante_Lib_Config::get('url.domain_main').'/interkassa/status');
Dante_Lib_Config::set('interkassa.status_method',  'POST');

Dante_Lib_Config::set('interkassa.strategyClassName',  null);