<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 10/24/13
 * Time: 4:47 PM
 * To change this template use File | Settings | File Templates.
 */

class Module_Interkassa_Service {

    /**
     * Фомирует форму для оплаты через интеркассу
     * @param $params
     * <code>
     *  array(
     *      'amount' => float
     *      'id' => int
     *      'description'
     *      'ik_baggage_fields'
     *      'payBtnCaption'
     * )
     * </code>
     * @return mixed|string
     */
    public static function drawForm($params) {
        if (!isset($params['success_url'])) {
            $params['success_url'] = Dante_Lib_Config::get('interkassa.success_url');
        }
        if (!isset($params['success_method'])) {
            $params['success_method'] = Dante_Lib_Config::get('interkassa.success_method');
        }
        if (!isset($params['fail_url'])) {
            $params['fail_url'] = Dante_Lib_Config::get('interkassa.fail_url');
        }
        if (!isset($params['fail_method'])) {
            $params['fail_method'] = Dante_Lib_Config::get('interkassa.fail_method');
        }
        if (!isset($params['status_url'])) {
            $params['status_url'] = Dante_Lib_Config::get('interkassa.status_url');
        }
        if (!isset($params['status_method'])) {
            $params['status_method'] = Dante_Lib_Config::get('interkassa.status_method');
        }
        if (!isset($params['paysystem_alias'])) {
            $params['paysystem_alias'] = Dante_Lib_Config::get('interkassa.paysystem_alias');
        }
        if (!isset($params['ik_baggage_fields'])) {
            $params['ik_baggage_fields'] = '';
        }
        if (!isset($params['payBtnCaption'])) {
            $params['payBtnCaption'] = 'Оплатить';
        }
        if (!isset($params['payBtnClass'])) {
            $params['payBtnClass'] = '';
        }

        $tpl = new Dante_Lib_Template();
        $tpl->shopId                = Dante_Lib_Config::get('interkassa.shopId');
        $tpl->amount                = $params['amount'];
        $tpl->paymentId             = $params['id'];
        $tpl->paymentDescription    = $params['description'];
        $tpl->paysystemAlias        = $params['paysystem_alias'];
        $tpl->ik_baggage_fields     = $params['ik_baggage_fields'];

        $tpl->ik_success_url        = $params['success_url'];
        $tpl->ik_success_method     = $params['success_method'];

        $tpl->ik_fail_url           = $params['fail_url'];
        $tpl->ik_fail_method        = $params['fail_method'];

        $tpl->ik_status_url         = $params['status_url'];
        $tpl->ik_status_method      = $params['status_method'];
        $tpl->payBtnCaption         = $params['payBtnCaption'];
        $tpl->payBtnClass           = $params['payBtnClass'];



        return $tpl->draw('../module/interkassa/template/form.html');
    }

    /**
     * @param $status_data
     * @param $secretKey
     */
    public static function generateHash($status_data, $secretKey) {
        // генерируем хэш
        $sing_hash_str = $status_data['ik_shop_id'].':'.
            $status_data['ik_payment_amount'].':'.
            $status_data['ik_payment_id'].':'.
            $status_data['ik_paysystem_alias'].':'.
            $status_data['ik_baggage_fields'].':'.
            $status_data['ik_payment_state'].':'.
            $status_data['ik_trans_id'].':'.
            $status_data['ik_currency_exch'].':'.
            $status_data['ik_fees_payer'].':'.
            $secretKey;
        $sign_hash = strtoupper(md5($sing_hash_str));
        return $sign_hash;
    }
}