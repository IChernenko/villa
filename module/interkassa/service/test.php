<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 10/29/13
 * Time: 9:58 AM
 * To change this template use File | Settings | File Templates.
 */

class Module_Interkassa_Service_Test {

    /**
     * Эмулирование статус запроса
     * @param $params
     * @return mixed
     * @throws Exception
     */
    public function doStatusRequest($params) {
        $secretKey = Dante_Lib_Config::get('interkassa.secretKey');
        if (!$secretKey) throw new Exception("Не определен секретный ключ");

        $url = Dante_Lib_Config::get('interkassa.status_url');
        if (!$url) throw new Exception("Не определен status url");

        $status_data = array();
        $status_data['ik_shop_id']              = Dante_Lib_Config::get('interkassa.shopId');
        $status_data['ik_payment_amount']       = $params['amount'];
        $status_data['ik_payment_id']           = $params['paymentId'];
        $status_data['ik_payment_desc']         = $params['paymentDescription'];
        $status_data['ik_paysystem_alias']      = '';
        $status_data['ik_baggage_fields']       = json_encode($params['baggage']);
        $status_data['ik_payment_timestamp']    = time();
        $status_data['ik_payment_state']        = 'success';
        $status_data['ik_trans_id']             = $params['transactionId'];
        $status_data['ik_currency_exch']        = 1;
        $status_data['ik_fees_payer']           = 1;

        $generatedHash = Module_Interkassa_Service::generateHash($status_data, $secretKey);
        if (!$generatedHash) throw new Exception("Не могу сгенерировать хеш");
        $status_data['ik_sign_hash'] = $generatedHash;


        $ch = curl_init ($url);
        curl_setopt ($ch, CURLOPT_POST, true);
        curl_setopt ($ch, CURLOPT_POSTFIELDS, $status_data);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        $returndata = curl_exec ($ch);
        return $returndata;
    }
}