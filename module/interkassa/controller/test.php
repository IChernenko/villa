<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 10/24/13
 * Time: 8:28 PM
 * To change this template use File | Settings | File Templates.
 */

class Module_Interkassa_Controller_Test extends Dante_Controller_Base {
    protected function _defaultAction() {
        return Module_Interkassa_Service::drawForm(array());
    }



    /**
     * тестирование статуса платежа
     */
    protected function _statusAction() {
        $secretKey = Dante_Lib_Config::get('interkassa.secretKey');

        $baggage = array(
            'type' => 'registration',
            'login' => 'rad2'
        );

        $service = new Module_Interkassa_Service_Test();
        return $service->doStatusRequest(array(
            'amount' => Dante_Lib_Config::get('radast.registrationPrice'),
            'paymentId' => 1,
            'paymentDescription' => "Оплата регистрации пользователя serg55 через интеркассу",
            'baggage' => $baggage,
            'transactionId' => 'IK_'.time()
        ));

    }
}