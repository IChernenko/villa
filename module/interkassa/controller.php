<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 10/28/13
 * Time: 2:09 PM
 * To change this template use File | Settings | File Templates.
 */

class Module_Interkassa_Controller extends Dante_Controller_Base{

    /**
     * Стратегия обработки платежей
     * @var bool
     */
    protected $_strategy = false;

    protected function _init() {
        $strategyClassName = Dante_Lib_Config::get('interkassa.strategyClassName');

        Dante_Lib_Log_Factory::getLogger()->debug('interkassa init strategy : '.$strategyClassName);

        if (!$strategyClassName) throw new Exception('Не определена стратегия');
        $this->_strategy = new $strategyClassName();
    }

    protected function _successAction() {
        $status_data = array();
        $status_data['ik_shop_id'] = $this->_request->get('ik_shop_id');
        $status_data['ik_payment_id'] = $this->_request->get('ik_payment_id');
        $status_data['ik_paysystem_alias'] = $this->_request->get('ik_paysystem_alias');
        $status_data['ik_baggage_fields'] = $this->_request->get('ik_baggage_fields');
        $status_data['ik_payment_timestamp'] = $this->_request->get('ik_payment_timestamp');
        $status_data['ik_payment_state'] = $this->_request->get('ik_payment_state');
        $status_data['ik_trans_id'] = $this->_request->get('ik_trans_id');

        return $this->_strategy->onSuccess($status_data);
    }



    /**
     * обработка статуса выполнения платежа
     * @return string
     */
    protected function _statusAction() {
        $secretKey = Dante_Lib_Config::get('interkassa.secretKey');
        $shopId = Dante_Lib_Config::get('interkassa.shopId');

        $status_data = array();
        $status_data['ik_shop_id']              = $this->_request->get('ik_shop_id');
        $status_data['ik_payment_amount']       = $this->_request->get('ik_payment_amount');
        $status_data['ik_payment_id']           = $this->_request->get('ik_payment_id');
        $status_data['ik_payment_desc']         = $this->_request->get('ik_payment_desc');
        $status_data['ik_paysystem_alias']      = $this->_request->get('ik_paysystem_alias');
        $status_data['ik_baggage_fields']       = $this->_request->get('ik_baggage_fields');
        $status_data['ik_payment_timestamp']    = $this->_request->get('ik_payment_timestamp');
        $status_data['ik_payment_state']        = $this->_request->get('ik_payment_state');
        $status_data['ik_trans_id']             = $this->_request->get('ik_trans_id');
        $status_data['ik_currency_exch']        = $this->_request->get('ik_currency_exch');
        $status_data['ik_fees_payer']           = $this->_request->get('ik_fees_payer');
        $status_data['ik_sign_hash']            = $this->_request->get('ik_sign_hash');

        // todo: записать данные в базу
        Dante_Lib_Log_Factory::getLogger()->debug('interkassa');
        Dante_Lib_Log_Factory::getLogger()->debug_item($status_data);

        try {
            // проверим тот ли магазин нам вернули
            if ($status_data['ik_shop_id'] != $shopId) {
                throw new Exception('Передали не тот магазин');
            }

            //var_dump($status_data); die();

            // проверим хеш
            $generatedHash = Module_Interkassa_Service::generateHash($status_data, $secretKey);
            if ($generatedHash != $status_data['ik_sign_hash']) {
                // хеши не совпадают
                throw new Exception('Хеши не совпадают');
            }

            Dante_Lib_Log_Factory::getLogger()->debug('interkassa payment status : '.$status_data['ik_payment_state']);
            // проверить статус платежа
            if ($status_data['ik_payment_state'] == 'success') {
                return $this->_strategy->onStatusSuccess($status_data);
            }

            if ($status_data['ik_payment_state'] == 'fail') {
                return $this->_strategy->onStatusFail($status_data);
            }
        }
        catch(Exception $exc) {
            Dante_Lib_Log_Factory::getLogger()->debug('interkassa exception : '.$exc->getMessage());
            return $exc->getMessage();
        }

        // проверить сумма платежа - бизнес логика

        return 'success';
    }

    protected function _failAction() {
        $status_data = array();
        return $this->_strategy->onSuccess($status_data);
    }
}