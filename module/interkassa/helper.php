<?php

class Module_Interkassa_Helper {

    /**
     * Учет комиссии интеркассы в 3 процента
     * @param float $amount
     * @return float
     */
    public static function includeKomission($amount) {
        return round($amount + $amount*0.035, 2);
    }

    /**
     * Учет комиссии интеркассы в 3 процента
     * @param float $amount
     * @return float
     */
    public static function excludeKomission($amount) {
        return round($amount -  ($amount*0.03));
    }

}