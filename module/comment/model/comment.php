<?php
/**
 * Модель комментария
 * User: dorian
 * Date: 17.04.12
 * Time: 16:57
 * To change this template use File | Settings | File Templates.
 */
class Module_Comment_Model_Comment
{
    public $sortable_rules=array(
        'name'=>'`date`',
        'type'=>'desc',
    );
    
    public $id;

    public $date;

    public $name;

    public $email;

    public $message;

    public $entity_type_id;

    public $entity_id;
    
    public $is_read;
}
