<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 29.05.12
 * Time: 10:19
 * To change this template use File | Settings | File Templates.
 */

Dante_Lib_Config::set('comment.drawList',           false);
Dante_Lib_Config::set('comment.showApproved',       false);

Dante_Lib_Config::set('comment.defEntity', 1);
Dante_Lib_Config::set('comment.defEntityType', 1);

Dante_Lib_Config::set('comment.allowCaptcha', true);

Dante_Lib_Config::set('comment.displayId', true);
Dante_Lib_Config::set('comment.displayName', true);
Dante_Lib_Config::set('comment.allowDel', true);
Dante_Lib_Config::set('comment.msgWidth', false);

?>