<?php

$url = Dante_Controller_Front::getRequest()->get_requested_url();

Dante_Lib_Router::addRule("/manage\/comment\//", array(
    'controller' => 'Module_Comment_Controller_Manage_Comment'
));

Dante_Lib_Router::addRule('/comments\//', array(
    'controller' => 'Module_Comment_Controller_Comment'
));

?>