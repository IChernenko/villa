<?php

/**
 * управление комментариями
 *
 * @author Mort
 */
class Module_Comment_Controller_Manage_Comment extends Module_Handbooksgenerator_Controller_Base 
{    
    protected $_alias = 'module.comment.controller.manage.comment';

    protected function _instanceView()
    {
        return new Module_Comment_View_Manage_Comment();
    }
    
    protected function _instanceModel()
    {
        return new Module_Comment_Model_Comment();
    }

    protected $_isRead = array(
        0 => 'Нет',
        1 => 'Да'
    );

    protected function _init() 
    {
        parent::_init();
        $this->_mapper = new Module_Comment_Mapper_Manage_Comment();
    }
    
    protected function _drawAction()
    {        
        $model = $this->_instanceModel();
        
        $params = $this->getParams($model);
        
        $list = $this->_mapper->getRowsByParams($params);
        
        $this->formEnable($this->_alias);
        $this->caption = 'Управление комментариями';
        
        $this->_setHead();
        $this->_setBody($list['rows']);           
        
        $this->tfoot=array(
            array(
                'button_filter'=>array(
                    "type"=>array(
                        'edit'
                    ),                    
                )
             )  
        );
        
        $html = $this->generate();
        
        return $html;
    }
    
    protected function _setHead()
    {
        $titles = array(           
            array(
                'content' => 'Email',
                'sortable' => 'email'
            ),
            array(
                'content' => 'Дата',
                'sortable' => 'date'
            ),                 
            array(
                'content' => 'Сообщение',
                'params' => array(  
                    'style' => array(
                        'width' => Dante_Lib_Config::get('comment.msgWidth')?Dante_Lib_Config::get('comment.msgWidth'):'auto'
                    )
                )
            ),             
            array(
                'content' => 'Прочитано',
                'sortable' => 'is_read'
            ),                
            array(
                'content' => 'Функции',
                'params' => array(
                    'style' => array(
                        'width' => '95px'
                    )
                )
            )  
        );
        
        if(Dante_Lib_Config::get('comment.displayName'))
            array_unshift($titles, array(
                'content' => 'Имя',
                'sortable' => 'name'
            )); 
        if(Dante_Lib_Config::get('comment.displayId'))
            array_unshift($titles, array(
                'content' => 'ID',
                'sortable' => 'id'
            ));
        
        $this->thead = array($titles);
    }
    
    protected function _setBody($rows)
    {
        $table=array();
        
        foreach($rows as $k => $cur)
        {
            $row = array();
            if(Dante_Lib_Config::get('comment.displayId')) 
                $row['id'] = $cur['id'];
            if(Dante_Lib_Config::get('comment.displayName'))
                $row['name'] = $cur['name'];
            $row['email'] = $cur['email'];
            $row['date'] = Dante_Helper_Date::converToDateType($cur['date']);
            $row['message'] = $cur['message'];
            $row['is_read'] = $this->_isRead[$cur['is_read']];
            $row['buttons'] = array(
                'id'=>$cur['id'],
                'type'=>Dante_Lib_Config::get('comment.allowDel')?array('edit', 'del'):array('edit'),                
            );  
                        
            $table[]=array(
                'content' => $row,
            );
        }
        
        $this->tbody = $table; 
    }




    protected function _drawFormAction()
    {
        $id = $this->_getRequest()->get_validate('id', 'int');
        
        $model = $this->_mapper->getById($id);
        if($model->date) $model->date = Dante_Helper_Date::converToDateType($model->date);
        
        if($id && !$model->is_read) $this->_mapper->setRead($id);
        
        return array(
            'result' => 1,
            'html' => $this->_instanceView()->drawForm($model)
        );
    }
    /**
     * редактирование/добавление/удаление записей грида
     */
    protected function _editAction() 
    {        
        $model = $this->_getModel();
        $this->_mapper->apply($model);
        return array(
            'result' => 1
        );
    }
    
    protected function _getModel()
    {
        $model = $this->_instanceModel();
        $model->id = $this->_getRequest()->get('id');
        $model->date = Dante_Helper_Date::strtotimef($this->_getRequest()->get('date'), 0);
        $model->name = $this->_getRequest()->get('name');
        $model->email = $this->_getRequest()->get('email');
        $model->message = $this->_getRequest()->get('message');   
        
        return $model;
    }
    
}

?>
