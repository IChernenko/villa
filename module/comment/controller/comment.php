<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 17.04.12
 * Time: 17:37
 * To change this template use File | Settings | File Templates.
 */
class Module_Comment_Controller_Comment extends Dante_Controller_Base {

    protected $_drawList = false;
    
    protected $_view;
    
    protected $_mapper;

    protected function _init() 
    {
        parent::_init();
        $this->_drawList = Dante_Lib_Config::get('comment.drawList');
        $this->_mapper = new Module_Comment_Mapper_Comment();
        $this->_view = new Module_Comment_View_Comment(); 
    }

    /**
     * Добавление комментария
     */
    protected function _addAction() 
    {
        if(Dante_Lib_Config::get('comment.allowCaptcha') && !Dante_Helper_App::getUid()) 
        {
            $service = new Module_Captcha_Service();
            $response = $this->_getRequest()->get('commentCaptcha');
        
            if(!$service->validateCaptcha($response))
            {
                return array(
                    'result' => 0,
                    'message' => 'Teкст с картинки введен неправильно'
                );
            }
        }
        
        $model = $this->_populateModel();

        return array(
            'result'=>(int)$this->_mapper->add($model),
            'html' => 'Комментарий добавлен'
        );
    }
    
    protected function _populateModel()
    {
        $model = new Module_Comment_Model_Comment();
        $model->name            = $this->_getRequest()->get('commentName');
        $model->email           = $this->_getRequest()->get('commentEmail');
        $model->message         = $this->_getRequest()->get('commentMessage');
        $model->entity_id        = (int)$this->_getRequest()->get('comment_entity_id', 1);
        $model->entity_type_id    = (int)$this->_getRequest()->get('comment_entity_type_id', 1);
        
        return $model;
    }

    /**
     * Отображение списка комментариев
     * @return string
     */
    protected function _defaultAction() {
        $entityId = (int)$this->_getRequest()->get('entityId');
        if(!$entityId) $entityId = Dante_Lib_Config::get('comment.defEntity');
        
        $entityTypeId = (int)$this->_getRequest()->get('entityTypeId');
        if(!$entityTypeId) $entityTypeId = Dante_Lib_Config::get('comment.defEntityType');
        
        return $this->draw($entityId, $entityTypeId);
    }
    
    protected function _getCommentsList($entityId, $entityTypeId) {
        $condition = array();
        if (Dante_Lib_Config::get('comment.showApproved')) {
            // определить владельца сущности
            $owner = Module_Entity_Helper::getEntityOwner($entityId, $entityTypeId);
            $uid = Dante_Helper_App::getUid();
            if ($uid != $owner)
                $condition['approved'] = 1;
        }
        return $this->_mapper->getList($entityId, $entityTypeId, $condition);
    }

    public function draw($entityId, $entityTypeId) {
        $list = $this->_getCommentsList($entityId, $entityTypeId);       
        
        $html = $this->_view->draw($list, $entityId, $entityTypeId);

        $captcha = '';
        $user = array('email'=>'', 'fio'=>'');
        
        if(Dante_Lib_Config::get('comment.allowCaptcha'))
        {
            $service = new Module_Captcha_Service();
            $captcha = $service->showCaptchaImage();
        }
        
        if($uid = Dante_Helper_App::getUid())
        {
            $userMapper = new Module_User_Mapper();
            $userModel = $userMapper->get($uid);
            $profile = $userMapper->getProfile($uid);
            $user['email'] = $userModel->email;
            $user['fio'] = isset($profile['fio'])?$profile['fio']:'';
            
        }
        $form = $this->_view->drawForm($captcha, $user);
        
        return $html.$form;
    }

    /**
     * Отображение формы комментария
     * @return array
     */
    protected function _drawFormAction() {
        $view = new Module_Comment_View_Comment();
        
        $captcha = '';
        $user = array('email'=>'', 'fio'=>'');
        if(Dante_Lib_Config::get('comment.allowCaptcha'))
        {
            $service = new Module_Captcha_Service();
            $captcha = $service->showCaptchaImage();
        }
        if($uid = Dante_Helper_App::getUid())
        {
            $userMapper = new Module_User_Mapper();
            $userModel = $userMapper->get($uid);
            $profile = $userMapper->getProfile($uid);
            $user['email'] = $userModel->email;
            $user['fio'] = isset($profile['fio'])?$profile['fio']:'';
            
        }
        
        $html = $view->drawForm($captcha, $user);
        
        return array(
            'result' => 1,
            'template' => $html
        );
    }

    protected function _drawFormHtmlAction() {
        $view = new Module_Comment_View_Comment();
        $html = $view->drawForm();

        if ($this->_drawList) {
            $html .= $this->draw(1, 1);
        }

        return $html;
    }
    
    protected function _removeAction() {
        $id = (int)$this->_getRequest()->get('id');
        $mapper = new Module_Comment_Mapper_Comment();
        $mapper->remove($id);
        return array(
            'result' => 1,
            'id' => $id
        );
    }
}
