drop table if exists `[%%]comment`;

CREATE TABLE `[%%]comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `entity_type_id` int(11) unsigned NOT NULL,
  `entity_id` int(11),
  `date` int(11),
  `name` varchar(128) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT="Комментарии";

ALTER TABLE [%%]comment ADD
      CONSTRAINT fk_comment_entity_type_id
      FOREIGN KEY (entity_type_id)
      REFERENCES [%%]entity(id) ON DELETE CASCADE ON UPDATE CASCADE;
