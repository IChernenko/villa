<?php
global $package;
$package = array(
    'version' => '4',
    'name' => 'module.comment',
    'js' => array('../module/comment/js/comment.js'),
    'dependence' => array(
        'module.division',
        'module.entity',
        'module.rating',
        'module.captcha',
        'module.user',
        'lib.jquery.validate',
        'lib.recaptcha',
        'lib.jsphp'
    )
);