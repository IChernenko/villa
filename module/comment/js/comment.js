
comment = {

    /**
     * Сохранение комментария
     * @param params {
     *     success: function // обработчик успешности постинга коммента
     * }
     */
    save: function(params) 
    {
        if(!this.validate()) return false;
        var data = $('#commentForm').serializeArray();      
        data.push(
            {name: 'comment_entity_id', value: $('#comment_entity_id').val()},
            {name: 'comment_entity_type_id', value: $('#comment_entity_type_id').val()}
        );
        
        $.ajax({
            type: "POST",
            url: '/ajax.php',
            data: data,      
            success: function(data) {
                if (data.result == 1) 
                {
//                    $('#commentForm').html(data.html);
                    jAlert(data.html, 'Сообщение', function(){location.reload();});
                }
                else $('div.error').text(data.message);
            }
        });
    },
    
    drawForm: function() {
        $.ajax({
            type: "GET",
            url: '/ajax.php',
            data: {
                controller: "module.comment.controller.comment",
                action: "drawForm"
            },
            success: function(data) {
                $('#commentForm').html(data.template);
            }
        });
    },
    
    remove: function(id) {
        $.ajax({
            type: "POST",
            url: '/ajax.php',
            data: {
                controller: "module.comment.controller.comment",
                action: "remove",
                id: id
            },
            success: function(data) {
                if (data.result == 1) {
                    $('#comment-item-'+data.id).fadeOut();
                }
            }
        });
    }, 
    
    validate: function()
    {
        $('div.error').empty();
        var inputs = $('#commentForm .validate');
        for(var i=0; i<inputs.length; i++)
        {
            var val = $(inputs[i]).val();
            if(!val)
            {
                $('div.error').text('Все поля обязательны к заполнению');
                this.showInvalid(inputs[i]);
                return false;
            }
            if(val && $(inputs[i]).hasClass('email') == 'commentEmail')
            {
                var mailReg = /\b^[\w\.\-]+\@[\w\.\-]+[\.a-z]*\.[a-z]{2,4}\b/i;
                if(!mailReg.test(val))
                {
                    $('div.error').text('Е-mail введен неправильно!');
                    this.showInvalid(inputs[i]);
                    return false;
                } 
            }
        }
        return true;
    },
    
    showInvalid: function(elem)
    {
        $(elem).addClass('invalid');
        $(elem).focus(function(){
            $(this).removeClass('invalid');
        });
    },
    editSubmit: function(obj)
    {
        if($(obj).hasClass('disabled')) return false;
        $(obj).addClass('disabled');
        
        var data=$('#editCommentForm').serializeArray();
        $.ajax({
            type: 'POST',
            data: data,
            url: '/ajax.php',
            dataType : "json",
            success: function (data){
                if(data.result==1)
                {
                    $('#modalWindow').arcticmodal('close');
                    handbooks.formSubmit();                   
                }
                else  console.log(data);
            }
        });
    }
}