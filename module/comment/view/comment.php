<?php
/**
 * View комментариев
 * User: dorian
 * Date: 18.04.12
 * Time: 19:42
 * To change this template use File | Settings | File Templates.
 */
class Module_Comment_View_Comment
{
    /**
     * Отображение списка комментариев
     * @param array $list
     * @return mixed|string
     */
    public function draw($list, $entityId, $entityTypeId) {
        $tplFileName = '../module/comment/template/list2.html';

        $customTpl = Dante_Helper_App::getWsPath().'/comment/list.html';
        
        if (file_exists($customTpl)) {
            $tplFileName = $customTpl;
        }

        $tpl = new Dante_Lib_Template();
        $tpl->comments = $list;
        $tpl->entityId = $entityId;
        $tpl->entityTypeId = $entityTypeId;
        
        $uid = Dante_Helper_App::getUid();
        if ($uid > 0)
        $tpl->uid = $uid;
        
        //var_dump($tpl->uid);
        
        return $tpl->draw($tplFileName);
    }

    public function drawForm($captcha, $user) { 
        $tplFileName = '../module/comment/template/form.html';
        $customTpl = Dante_Helper_App::getWsPath().'/comment/form.html';
        if (file_exists($customTpl)) {
            $tplFileName = $customTpl;
        }

        $tpl = new Dante_Lib_Template();
        $tpl->captcha = $captcha;
        $tpl->user = $user;
        
        return $tpl->draw($tplFileName);
    }
}
