<?php

class Module_Comment_View_Manage_Comment
{
    public function drawForm($model)
    {
        $tpl = new Dante_Lib_Template();
        $filePath = '../module/comment/template/manage/form.html';
        
        $tpl->comment = $model;
        return $tpl->draw($filePath);
    }
}

?>