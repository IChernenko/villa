<?php

/**
 * управление комментариями
 *
 * @author Mort
 */
class Module_Comment_Mapper_Manage_Comment extends Module_Handbooksgenerator_Mapper_Base
{    
    protected $_tableName = '[%%]comment';    
    
    protected function _instanceModel()
    {
        return new Module_Comment_Model_Comment();
    }

    public function getById($id)
    {        
        if(!$id) return $this->_instanceModel();
        
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->select(array('id'=>$id));
        
        $fields = $table->getFields();
        return $this->_arrayToModel($fields);
    }
    
    protected function _arrayToModel($fields)
    {        
        $model = $this->_instanceModel();
        
        $model->id = $fields['id'];
        $model->date = $fields['date'];
        $model->name = $fields['name'];
        $model->email = $fields['email'];
        $model->message = $fields['message'];
        
        return $model;
    }
    
    /**
     * обновление комментария
     * @param Module_Comment_Model_Comment $params 
     */
    public function apply($params) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->date = $params->date;
        $table->name = $params->name;
        $table->email = $params->email;
        $table->message = $params->message;
        
        if($params->id) 
            $table->update(array('id'=>$params->id));
        else 
        {
            $table->entity_id = 1;
            $table->entity_type_id = 1;
            $table->insert();
        }
    }
    
    public function setRead($id) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->is_read = 1;
        $table->apply(array(
            'id' => $id
        ));
    }
    
}

?>
