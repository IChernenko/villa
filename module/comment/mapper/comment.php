<?php
/**
 * Маппер комментария
 * User: dorian
 * Date: 17.04.12
 * Time: 16:59
 * To change this template use File | Settings | File Templates.
 */
class Module_Comment_Mapper_Comment
{
    /**
     * Добавления комментария
     * @param Module_Comment_Model_Comment $comment
     */
    public function add(Module_Comment_Model_Comment $comment) 
    {
        $table = new Dante_Lib_Orm_Table('[%%]comment');
        $table->date = time();
        $table->name = $comment->name;
        $table->email = $comment->email;
        $table->message = $comment->message;
        $table->entity_id = $comment->entity_id;
        $table->entity_type_id = $comment->entity_type_id;
        $comment->id = (int)$table->insert();
        return true;
    }
    
    public function remove($id) {
        $table = new Dante_Lib_Orm_Table('[%%]comment');
        return $table->delete(array('id'=>$id));
    }

    /**
     * Получение списка комментариев для заданой сущности
     * @param int $entityId
     * @param int $entityTypeId
     * @return array of Module_Comment_Model_Comment
     */
    public function getList($entityId, $entityTypeId, $conditions=array()) {

        $table = new Dante_Lib_Orm_Table('[%%]comment');
        $conditions['entity_type_id']   = $entityTypeId;
        $conditions['entity_id']        = $entityId;
        $table->get()->where($conditions)->orderby('date desc');
        $table->open();
        
        

        $list = array();
        while($f = $table->fetchRecord()) {
            $comment = new Module_Comment_Model_Comment();
            $comment->id            = (int)$f['id'];
            $comment->entity_type_id  = (int)$f['entity_type_id'];
            $comment->entity_id      = (int)$f['entity_id'];
            $comment->date          = Dante_Helper_Date::convertToDateTime($f['date']);
            $comment->name          = $f['name'];
            $comment->email         = $f['email'];
            $comment->message       = $f['message'];

            $list[] = $comment;
        }

        return $list;
    }
}
