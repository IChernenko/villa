<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 19.04.12
 * Time: 13:02
 * To change this template use File | Settings | File Templates.
 */
class Module_Entity_Helper
{
    public static function getBySysName($sysName) {
        $mapper = new Module_Entity_Mapper();
        return $mapper->getBySysName($sysName);
    }
    
    public static function getEntityOwner($entityId, $entityTypeId) {
        
        $entityMapper = new Module_Entity_Mapper();
        $entity = $entityMapper->getById($entityTypeId);
        if (!$entity) return false;
        if ($entity->sys_name == 'publication') {
            $publicationMapper = new Module_Publications_Mapper();
            $publication = $publicationMapper->getById($entityId, Module_Lang_Helper::getCurrent());
            if ($publication) return $publication->uid;
        }
        
        //Dante_Lib_Observer_Helper::fireEvent('entity.getOwner', $user);
    }
}
