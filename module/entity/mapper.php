<?php
/**
 * Маппер сущности
 * User: dorian
 * Date: 17.04.12
 * Time: 17:17
 * To change this template use File | Settings | File Templates.
 */
class Module_Entity_Mapper
{
    /**
     * Добавление новой сущности
     * @param Module_Entity_Model $entity
     * @return bool
     */
    public function add(Module_Entity_Model $entity) {
        $table = new Dante_Lib_Orm_Table('[%%]entity');
        $table->name = $entity->name;
        $table->sys_name = $entity->sys_name;

        $entity->id = (int)$table->insert();
        return true;
    }

    /**
     * @param $sysName
     * @return Module_Entity_Model
     */
    public function getBySysName($sysName) {
        $table = new Dante_Lib_Orm_Table('[%%]entity');
        $table->select(array('sys_name'=>$sysName));

        $entity = new Module_Entity_Model();

        $entity->id = (int)$table->id;
        $entity->name = $table->name;
        $entity->sys_name = $table->sys_name;

        return $entity;
    }
    
    /**
     * 
     * @param type $id
     * @return boolean|\Module_Entity_Model
     */
    public function getById($id) {
        $table = new Dante_Lib_Orm_Table('[%%]entity');
        $table->select(array('id'=>$id));
        if ($table->getNumRows() == 0) return false;

        $entity = new Module_Entity_Model();
        $entity->id = (int)$table->id;
        $entity->name = $table->name;
        $entity->sys_name = $table->sys_name;

        return $entity;
    }
}
