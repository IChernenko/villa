<?php
global $package;
$package = array(
    'version' => '0',
    'name' => 'module.news',
    'description' => 'Модуль новостной ленты',
    'dependence' => array(
        'module.publications'
    ),
    'js' => array(
        '../module/news/js/news.js'
    )
);