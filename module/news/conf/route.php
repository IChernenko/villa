<?php

$url = Dante_Controller_Front::getRequest()->get_requested_url();

Dante_Lib_Router::addRule('/^\/manage\/news\/$/', array(
    'controller' => 'Module_News_Manage_Controller'
));

Dante_Lib_Router::addRule('/^\/news\/$/', array(
    'controller' => 'Module_News_Controller'
));



?>