news = {
    publish: function() {
        $.ajax({
            type: "POST",
            url: '/ajax.php',
            data: {
                controller: "module.news.manage.controller",
                action: "publish",
                title: $('#newsTitle').val(),
                message: $('#newsMessage').val(),
                id: $('#newsId').val()
            },
            success: function(data) {
                if (data.result == 1) {
                    $('#newsStatus').addClass('alert alert-success');    
                    $('#newsStatus').html("Новость размещена");
                }
                
            }
        });
    },
    
    remove: function(id) {
        
        $.ajax({
            type: "POST",
            url: '/ajax.php',
            data: {
                controller: "module.news.manage.controller",
                action: "remove",
                id: id
            },
            success: function(data) {
                $('#news-manage-list-item-'+id).fadeOut();
            }
        });
    },
    
    edit: function(id) {
        
        $.ajax({
            type: "POST",
            url: '/ajax.php',
            data: {
                controller: "module.news.manage.controller",
                action: "edit",
                id: id
            },
            success: function(data) {
                if (data.result == 1) {
                    $('#newsTitle').val(data.data.title);
                    $('#newsMessage').val(data.data.content);
                    $('#newsId').val(data.data.id);
                }
                //$('#news-manage-list-item-'+id).fadeOut();
            }
        });
    }
}