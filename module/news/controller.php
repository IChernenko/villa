<?php



/**
 * Контроллер списка новостей
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_News_Controller extends Dante_Controller_Base{
    
    protected function _defaultAction() {
//        $uid = (int)Dante_Helper_App::getUid();

//        if (!$uid) {
//            $login = Radast_App_Helper::getLoginByPath();
//            if ($login) {
//                $userMapper = new Module_User_Mapper();
//                $uid = (int)$userMapper->getUidByLogin($login);
//            }
//        }

        
        $url = Dante_Controller_Front::getRequest()->get_requested_url();
        if ($url[0] == '/') $url = substr($url, 1);

        // определить id раздела по url
        $divisionMapper = new Module_Division_Mapper();
        $divisionId = (int)$divisionMapper->getByLink($url);
        
        
        
        
        $params = array();
//        if ($uid > 0 ) $params['filter']['uid'] = $uid;
        if ($divisionId > 0 ) $params['filter']['division'] = $divisionId;
        $params['maxWords'] = Dante_Lib_Config::get('news.previewMaxWords');
        
        
        $mapper = new Module_Publications_Mapper();
        $list = $mapper->getList($params);
        if (!$list) $list = array();
        
        $view = new Module_News_View();
        return $view->draw($list);
    }
}

?>
