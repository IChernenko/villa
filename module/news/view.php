<?php

/**
 * View для отображения новостей
 */
class Module_News_View {
    
    public function draw($list) {
        $tpl = new Dante_Lib_Template();
        $tpl->news = $list;
        $ws = Dante_Helper_App::getWsName();
        $fileName = '../module/news/template/list.html';
        if (file_exists("template/{$ws}/news/list.html")) {
            $fileName = "template/{$ws}/news/list.html";
        }
        return $tpl->draw($fileName);
    }
}
?>
