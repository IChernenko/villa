<?php



/**
 * Контроллер новостей
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_News_Manage_Controller extends Dante_Controller_Base{
    
    protected function _defaultAction() {
        $uid = (int)Dante_Helper_App::getUid();
        if ($uid == 0) return 'Вы должны быть залогинены в систему';
        
        $mapper = new Module_Publications_Mapper();
        $list = $mapper->getByUid($uid);
        if (!$list) $list = array();
        
        $view = new Module_News_Manage_View();
        return $view->draw($list);
    }
    
    /**
     * Размещение новости
     * @return type 
     */
    protected function _publishAction() {
        try {
            $uid = (int)Dante_Helper_App::getUid();
            if ($uid == 0) throw new Exception('Вы должны быть залогинены в систему');

            $title = $this->_getRequest()->get('title');
            $message = $this->_getRequest()->get('message');
            $id = (int)$this->_getRequest()->get('id');
            
            $link = Dante_Lib_Transliteration::transform($title);
            if (!strstr($link, '.html')) {
                $link .= '.html';
            }

            //$message = strip_tags($message, '<p><b><a><i><u>');
            $cleaner = new htmlcleaner();
            $message = $cleaner->cleanup($message);
            $message = str_replace('&nbsp;', ' ', $message);

            $model = new Module_Publications_Manage_Model();
            $model->uid = $uid;
            $model->page_title = $title;
            $model->page_content = $message;
            $model->date = time();
            $model->type = 2;
            $model->url = $link;
            $model->id = $id;

            $mapper = new Module_Publications_Mapper();
            if ($id >0) {
                $mapper->update($model);
            }
            else {
                $mapper->add($model);
            }
            
            return array('result' => 1);
        } catch (Exception $exc) {
            return array('result' => 0, 'message' => $exc->getMessage());
        }
    }
    
    protected function _removeAction() {
        $uid = (int)Dante_Helper_App::getUid();
        if ($uid == 0) throw new Exception('Вы должны быть залогинены в систему');
        
        $id = (int)$this->_getRequest()->get('id');
        
        $dmo = new Module_Publications_Dmo();
        $dmo->delete(array(
            'id' => $id,
            'uid' => $uid
        ));
        
        return array('result' => 1);
    }
    
    protected function _editAction() {
        $uid = (int)Dante_Helper_App::getUid();
        if ($uid == 0) throw new Exception('Вы должны быть залогинены в систему');
        
        $id = (int)$this->_getRequest()->get('id');
        
        $mapper = new Module_Publications_Mapper();
        $model = $mapper->getById($id, 1, array(
            'filter' => array('uid'=>$uid)
        ));
        if (!$model) throw new Exception('Немогу загрузить новость');
        
        $result = array();
        $result['result'] = 1;
        $result['data']['id']       = $model->id;
        $result['data']['title']    = $model->page_title;
        $result['data']['content']  = $model->page_content;
        return $result;
    }
}

?>
