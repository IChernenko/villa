<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 9/27/13
 * Time: 2:47 PM
 * To change this template use File | Settings | File Templates.
 */

class Module_News_View_Block {

    public function draw($list) {
        $tpl = new Dante_Lib_Template();
        $tpl->news = $list;
        $ws = Dante_Helper_App::getWsName();
        $fileName = '../module/news/template/news_block.html';
        if (file_exists("template/{$ws}/news/news_block.html")) {
            $fileName = "template/{$ws}/news/news_block.html";
        }
        return $tpl->draw($fileName);
    }
}