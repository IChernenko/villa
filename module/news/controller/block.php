<?php

/**
 * Контроллер блока превью новостей
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_News_Controller_Block extends Dante_Controller_Base{

    protected function _defaultAction() {

        $params = array();
        $params['maxWords'] = Dante_Lib_Config::get('news.previewMaxWords');

        $divisionMapper = new Module_Division_Mapper();
        $divisionId = (int)$divisionMapper->getByLink('news.html');
        if ($divisionId == 0) {
            //нет раздела с новостями - нечего выводить
            return 'no display';
        }
        
        $blockLimit = Dante_Lib_Config::get('news.blockLimit');

        $mapper = new Module_Publications_Mapper();
        if($blockLimit) $params['limit'] = "LIMIT {$blockLimit}";
        if ($divisionId > 0 ) $params['filter']['division'] = $divisionId;
        $list = $mapper->getList($params);
        if (!$list) $list = array();
            /*echo('<pre>');
            print_r($list);
        echo('</pre>');
        die();*/
        $view = new Module_News_View_Block();
        return $view->draw($list);
    }
}