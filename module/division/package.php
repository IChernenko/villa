<?php
global $package;
$package = array(
    'version' => '8',
    'name' => 'module.division',
    'dependence' => array(
        'lib.jdatepicker',
        'lib.jgrid',
        'lib.jquery.sortable',
        'module.auth',
        'module.group',
        'module.lang',
        'component.jalerts',
        'component.vcl',
        'ws' =>  array(
            'admin' => array(
                'lib.jquery.sortable'
                )
            )
    ),
    'js' => array(
        '../module/division/js/manage/division.js?2'
    )
);