<?php

class Module_Division_Manage_Service {
    /**
     * добавление/ записей грида
     *  DEPRECATED
     */
    public static function add($params) {        
        if($params[3]=='root'){
            $parent_id = 1;
        }else if($params[3]=='adminka'){
            $parent_id = 2;
        }else{
            $parent_id = 3;
        }        
        $mapper = new Module_Division_Manage_Mapper();
        $check = $mapper->getRowByName($params[0]);
        
        
        if($check->id > 0){
            //запись с таким именем уже имеется, не будем плодить одноименные разделы
            return;
        }
        
        $table = new Module_Division_Manage_Model();
        $table->name        = $params[0];
        $table->parent_id   = $parent_id;
        $table->link        = $params[1];
        $table->is_active   = true;
        
        $tableSeo = new Module_Division_Manage_Seomodel();

        $groups = $params[2];
        
        $mapper->add($table, $tableSeo, $groups);
    }

    public static function create($params) {
        $divisionMapper = new Module_Division_Manage_Mapper();
        $check = $divisionMapper->getRowByName($params['name']);


        if($check->id > 0){
            return;
            throw new Exception("Уже существует раздел : ".$params['name']);
            //запись с таким именем уже имеется, не будем плодить одноименные разделы
            return;
        }

        $parentId = false;
        if (isset($params['parent'])) {
            $parent = $divisionMapper->getRowByName($params['parent']);
            $parentId = $parent->id;
            if (!$parentId) throw new Exception("cant find division with name ".$params['parent']);
        }

        $divisionModel = new Module_Division_Manage_Model();
        $seoModel = new Module_Division_Manage_Seomodel();
        
        $divisionModel->name        = $params['name'];        
        
        $seoModel->lang_id = 1;
        $seoModel->name = $divisionModel->name;
        
        if ($parentId) {
            $divisionModel->parent_id = $parentId;
        }
        if (isset($params['url'])) {
            $divisionModel->link = $params['url'];
        }
        $divisionModel->is_active   = (int)$params['is_active'];

        $groups = false;
        if (isset($params['groups'])) $groups = $params['groups'];
        $divisionMapper->add($divisionModel, $seoModel, $groups);
    }
    
    /**
     * Редактируем/добавляем запись по предутановленным условиям
     * @param Module_Division_Model $model
     * @param array|string $condition - если string то подразумевается что это системное имя.
     */
    public static function applyDivision(Module_Division_Manage_Model $model, $cond=false){
        $mapper = new Module_Division_Manage_Mapper();
        foreach($model as &$v){
            
            if(is_null($v) || is_array($v)){
                $v=FALSE;
            }
        }
        //Тянем редактируемую запись        
        if(is_string($cond)){
            $cond = array('name'=>$cond);
        }
        if($cond){
            $model->id = $mapper->getIdByCondition($cond);
        }
        $mapper->apply($model);
        
        
    }

    /**
     * Удаление записи о разделе из базы данных.
     * @param string|int $value - значение по которому удаляем
     * @param type $field - поле по которому удаляем . По умолчанию по link
     * @author Elrafir
     */
    public static function delDivision($value, $field = 'link'){
        $orm = new Dante_Lib_Orm_Table('[%%]division');
        $orm -> delete(array($field=>$value));
    }
}

?>
