<?php
/**
 * Description of display
 *
 * @author Mort
 */
class Module_Division_Manage_Display extends Dante_Controller_Base{
    
    /**
     * отрисовка меню разделов
     * @return string 
     */
    protected function _defaultAction()
    {
        $userHelper = new Module_Division_Helper();
        $groupId = $userHelper->getGroupId();
        if (is_null($groupId) ) 
        {
            if(!Dante_Helper_App::getUid()) return '';
            return 'Не задана группа для пользователя';
        }
        
        $divisionsMapper = new Module_Division_Manage_Mapper();
        $divisions = $divisionsMapper->getAvailableDivisions($groupId);

        $test = 0;

        $url = Dante_Controller_Front::getRequest()->get_requested_url();
        
        $view = new Module_Division_Manage_View();
        return $view->display($divisions, $url);
    }
    
}

?>
