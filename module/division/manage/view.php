<?php

class Module_Division_Manage_View
{
    public function display($divisions, $url)
    {        
        $html = '';
        $html .= '<ul class="nav nav-pills">';
        $stringParams = '<li><a href="{link}">{separator}{name}</a></li>';
        
        $libTree = new Component_Vcl_Tree();
        $drawTreeParams = array(
            'array' => $divisions,
            'stringParams' => $stringParams,
            'parentKey' => 'parent_id',
            'separatorString' => '--'
        );
        $divisionIdEdit = $libTree->drawTree($drawTreeParams, 2);
        
        foreach ($divisionIdEdit as $key => $divisionString)
        {
            if(strpos($divisionString, $url) ){
                $divisionIdEdit[$key] = str_replace('<li>', '<li class="active">', $divisionString);
                break;
            }
        }
        
        $divisionsString = implode(" ", $divisionIdEdit);
        
        $html .= $divisionsString;
        $html .= '</ul>';
        return $html;
    }
    
    public function form($vars=false)
    {
        $tpl = new Dante_Lib_Template();
        $tpl->_vars=$vars;
        $fileName = '../module/division/template/form.html';
        
        $ws = Dante_Helper_App::getWsName();
        if (file_exists("template/{$ws}/division/form.html")) {
            $fileName = "template/{$ws}/division/form.html";
        }
        
        return $tpl->draw($fileName);        
    }
    
    public function accessFrom($vars=false)
    {
        $tpl = new Dante_Lib_Template();
        $tpl->_vars=$vars;
        $fileName = '../module/division/template/accessform.html';
        return $tpl->draw($fileName);
    }
}

?>