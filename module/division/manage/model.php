<?php

/**
 * Description of model
 *
 * @author Mort
 */
class Module_Division_Manage_Model extends Module_Division_Manage_Egridmodel {
    
    public $id;
    
    public $name;
    
    public $image;

    public $parent_id;

    public $link;

    public $draw_order;
    
    public $d_type;
    
    public $category_id;

    public $lang_id=1;

    public $seoName;

    public $title;

    public $keywords;

    public $description;

    public $is_active;
}

?>
