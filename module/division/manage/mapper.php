<?php
/**
 * Description of mapper
 *
 * @author Admin
 */

class Module_Division_Manage_Mapper extends Module_Handbooksgenerator_Mapper_Base {    
    protected $_tableName = '[%%]division';    
    protected $_divisionGroupsTableName = '[%%]division_groups';    
    protected $_groupsTableName = '[%%]groups';
    protected $_tableAccess = '[%%]division_access';
    protected $_seoTableName = '[%%]division_seo';
    protected $_tableLang = '[%%]languages';


    
    /**
     * Собраем строки для таблицы справочника
     * @param array $params
     * @return array Result
     * @author Elrafir
     */
    public function getDivisions($params, $langId=1){
        $this->paginator=false;
        //$this->debug=true;
        $join=array(
            "left join ".$this->_divisionGroupsTableName." as t2 on t2.division_id=t1.id",
            "left join ".$this->_groupsTableName." as t3 on t3.group_id=t2.group_id",
            "left join ".$this->_seoTableName." as t4 on t4.menu_id=t1.id AND t4.lang_id={$langId}",            
        );
         $colls=array(
            "t1.*",            
            "t3.*",           
            "t4.name as seo_name, t4.title, t4.description, t4.lang_id, t4.keywords ",
           
        );
        $rowsArray = $this->getRowsByParams($params, $colls, $join);
        return $rowsArray;        
    }
    /**
     * Вытаскиваем один дивижн по параметрам
     * @param type $params
     * @param type $langId
     */
    public function getDivisionsByParams($params, $langId){
        $result = $this->getDivisions($params, $langId);
        return current($result['rows']);
    }
    /**
     * 
     * @param type $condition
     */
    public function getIdByCondition($cond){
        $orm = new Dante_Lib_Orm_Table($this->_tableName);
        $orm->select($cond);
        return $orm->id;
    }

    /**
     * Достаём перечень доступных языков
     * @return array
     */
    public function getLanguages(){
        $orm  = new Dante_Lib_Orm_Table($this->_tableLang);
        return $orm->select_array(false)->getFields();
    }

    /**
     * выбираем последний ид ордера
     * @param int $parentId
     * @return int 
     */
    public function getLastOrderIdByParent($parentId)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        
        $table->get()->where(array('parent_id'=>$parentId))
            ->limit(array(0, 1))->orderby('draw_order desc');
        $table->fetch();

        return $table->draw_order;
    }
    
    /**
     * выбираем запись по ид
     * @param int $id
     * @return Dante_Lib_Orm_Table 
     */
    public function getRowById($id){
        $table = new Dante_Lib_Orm_Table($this->_tableName);        
        $table->get()->where(array('id'=>$id))
            ->limit(array(0, 1))->orderby('draw_order desc');
        $table->fetch();
        return $table;
    }
    
    /**
     * выбираем запись по name
     * @param string $name
     * @return Dante_Lib_Orm_Table 
     */
    public function getRowByName($name){
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        
        $table->get()->where(array('name'=>$name))
            ->limit(array(0, 1))->orderby('draw_order desc');
        $table->fetch();

        return $table;
    }
    
    /**
     * определяем ид раздела по порядке отображения в заданном родительском дереве
     * @param int $orderId
     * @param int $parentId
     * @return int 
     */
    public function getIdByOrder($orderId, $parentId)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        
        $table->get()->where(array('parent_id'=>$parentId, 'draw_order'=>$orderId))
            ->limit(array(0, 1))->orderby('draw_order desc');
        $table->fetch();

        return $table->id;
    }
    
    /**
     * выбираем разделы
     * @param array $params
     * @return Lib_Jgrid_Model_Base 
     */
    public function getCategories($params) 
    {
        $jgridMapper = new Lib_Jgrid_Mapper_Base();
        $rowsArray = $jgridMapper->getRowsByParams($this->_tableName, $params);
        $categoriesPrep = $rowsArray['rows'];
        $rowCount = $rowsArray['rowCount'];
        
        
        $groupsWhereArr = array();
        foreach ($categoriesPrep as $k=>$f){
            $groupsWhereArr[] = $f['id'];
        }
        $groupsWhere = 'division_id IN ('.implode(',', $groupsWhereArr).')';
        
        $sql = "select
                    *
                from ".$this->_divisionGroupsTableName." INNER JOIN ".$this->_groupsTableName." 
                    ON ".$this->_divisionGroupsTableName.".group_id = ".$this->_groupsTableName.".group_id
                        WHERE ".$groupsWhere;

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $groupsB = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            if(isset($categoriesPrep[$f['division_id']])){
                if(!isset ($groupsB[$f['division_id']])){
                    $groupsB[$f['division_id']] = $f['group_caption'];
                }else{
                    $groupsB[$f['division_id']] .= '<br> '.$f['group_caption'];
                }
            }
        }
        
        foreach ($categoriesPrep as $k=>$f){
            if(isset($groupsB[$f['id']])){
                $categoriesPrep[$k]['groupsB'] = $groupsB[$f['id']];
            }else{
                $categoriesPrep[$k]['groupsB'] = '';
            }
        }
        
        
        $sql = "select
                    *
                from ".$this->_seoTableName."
                where lang_id =".$params['language'];

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            if(isset($categoriesPrep[$f['menu_id']])){
                $categoriesSeoPrep[$f['menu_id']] = $f;
            }
        }
        
        $parents = $this->getParents();
        $categories = new Lib_Jgrid_Model_Base();
        $i=0;
        foreach ($categoriesPrep as $k=>$f){
            if($f['id']>3){
                if(!isset($categoriesSeoPrep[$f['id']])){
                    $f['seoName'] = '';
                    $f['seoTitle'] = '';
                    $f['seoKeywords'] = '';
                    $f['seoDescription'] = '';
                }else{
                    $f['seoName'] = $categoriesSeoPrep[$f['id']]['name'];
                    $f['seoTitle'] = $categoriesSeoPrep[$f['id']]['title'];
                    $f['seoKeywords'] = $categoriesSeoPrep[$f['id']]['keywords'];
                    $f['seoDescription'] = $categoriesSeoPrep[$f['id']]['description'];
                }


                switch ($f['d_type']) {
                    case 'url':
                        $f['d_type']='ссылка';
                        break;
                    case 'publications':
                        $f['d_type']='публикации';
                        break;
                    case 'empty':
                        $f['d_type']='без ссылки';
                        break;
                }

                $categories->rows[$i]['id'] = $f['id'];
                $categories->rows[$i]['cell'] = array(
                                                    $f['id'],
                                                    $f['name'],
                                                    (isset($parents[$f['parent_id']])?$parents[$f['parent_id']]['name']:$f['parent_id']),
                                                    $f['link'],
                                                    $f['draw_order'],
                                                    $f['seoName'],
                                                    $f['seoTitle'],
                                                    $f['seoKeywords'],
                                                    $f['seoDescription'],
                    $f['groupsB'],
                    $f['d_type']
                                                );
                $i++;
            }
        }
        
        $categories->records = $rowCount;
        
        return $categories;
    }
    
    /**
     * выбираем родителей
     * @return array 
     */
    public function getParents() {
        $sql = "select
                    id, name, parent_id
                from ".$this->_tableName."
                order by id";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);

        $parents = array(0=>array('id'=>0, 'name'=>'нет','parent_id'=>0));
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $parents[$f['id']] = $f;
        }
        
        return $parents;
    }
    
    /**
     * удаление раздела
     * @param int $id 
     */
    public function del($id) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->delete(array(
            'id' => $id
        ));
        
        $table = new Dante_Lib_Orm_Table($this->_seoTableName);
        $table->delete(array(
            'menu_id' => $id
        ));
        
        $this->delGroups($id);
    }
    
    /**
     * удаляем группы пользователей
     * @param int $id 
     */
    public function delGroups($id) {
        $table = new Dante_Lib_Orm_Table($this->_divisionGroupsTableName);
        $table->delete(array(
            'division_id' => $id
        ));
    }
    
    /**
     * добавление раздела
     * @param Module_Division_Manage_Model $params
     * @param Module_Division_Manage_Seomodel $paramsSeo 
     */
    public function add(Module_Division_Manage_Model $divisionModel, Module_Division_Manage_Seomodel $seoModel, $groups) {

        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->name        = $divisionModel->name;
        $table->parent_id   = $divisionModel->parent_id;
        $table->link        = $divisionModel->link;
        $table->d_type      = $divisionModel->d_type;
        $table->draw_order  = (int)$divisionModel->draw_order;
        $table->is_active   = (int)$divisionModel->is_active;
        
        if(!$table->draw_order || $table->draw_order == 0) $table->draw_order = $this->getLastOrderIdByParent($divisionModel->parent_id)+1;

        $divisionModel->id = (int)$table->insert();
        if (!$divisionModel->id) {
           $sql =  $table->getSql();
            echo('sql : '.$sql);
            throw new Exception("cant create division : ".$divisionModel->name);
        }


        $seoModel->menu_id = $divisionModel->id;
        
        $this->addSeo($seoModel);

        if ($groups)
        $this->addGroups($divisionModel, $groups);
    }
    
    /**
     * добавляем группы пользователей
     * @param Module_Division_Manage_Model $params
     * @param array $groups
     */
    public function addGroups(Module_Division_Manage_Model $params, $groups) 
    {
        if(count($groups)==0) return;

        $groupMapper = new Module_Group_Mapper();

        foreach ($groups as $groupId) 
        {
            if (is_string($groupId)) {
                $groupId = $groupMapper->getByName($groupId);
            }

            $table = new Dante_Lib_Orm_Table($this->_divisionGroupsTableName);
            $table->division_id = $params->id;
            $table->group_id    = $groupId;
            $table->insert();
            
            $tableAccess = new Dante_Lib_Orm_Table($this->_tableAccess);
            $tableAccess->division_id = $params->id;
            $tableAccess->group_id = $groupId;
            $tableAccess->read = 1;
            $tableAccess->insert();
        }
    }
    /**
     * добавление локализации раздела
     * @param Module_Division_Manage_Seomodel $paramsSeo 
     */
    public function addSeo(Module_Division_Manage_Seomodel $seoModel) {
        $table = new Dante_Lib_Orm_Table($this->_seoTableName);
        $table->menu_id     = $seoModel->menu_id;
        $table->lang_id     = $seoModel->lang_id;
        $table->name        = $seoModel->name;
        $table->title       = $seoModel->title;
        $table->keywords    = $seoModel->keywords;
        $table->description = $seoModel->description;
        
        return $table->insert();
    }
    
    /**
     * обновляем раздел
     * @param Module_Division_Manage_Model $params
     * @param Module_Division_Manage_Seomodel $paramsSeo 
     */
    public function update(Module_Division_Manage_Model $params, Module_Division_Manage_Seomodel $paramsSeo, $groupsBArray) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->id = $params->id;
        $table->name = $params->name;
        $table->parent_id = $params->parent_id;
        $table->link = $params->link;
        $table->d_type = $params->d_type;
        $table->draw_order = (int)$params->draw_order;
        
        $table->apply(array(
            'id' => $params->id
        ));
        
        //а есть ли запись сео для данной штуки ? 
        $tableSeo = new Dante_Lib_Orm_Table($this->_seoTableName);
        if($tableSeo->exist(array(
            'menu_id' => $paramsSeo->menu_id,
            'lang_id' => $paramsSeo->lang_id
        ))){
            $tableSeo->menu_id = $paramsSeo->menu_id;
            $tableSeo->lang_id = $paramsSeo->lang_id;
            $tableSeo->name = $paramsSeo->name;
            $tableSeo->title = $paramsSeo->title;
            $tableSeo->keywords = $paramsSeo->keywords;
            $tableSeo->description = $paramsSeo->description;

            $tableSeo->apply(array(
                'menu_id' => $paramsSeo->menu_id,
                'lang_id' => $paramsSeo->lang_id
            ));
        }else{
            //создаем новую
            $this->addSeo($paramsSeo);
        }
        
        $this->delGroups($params->id);
        $this->addGroups($params, $groupsBArray);
    }
    
    /**
     * меняем порядок отображения раздела
     * @param int $id
     * @param int $orderFactor 
     */
    public function changeOrder($id, $orderFactor)
    {
        $currentRow = $this->getRowById($id);
        
        if(!$currentRow || !$currentRow->draw_order ){
            throw new Exception('division manage mapper changeOrder error');
        }
        $opponentId = $this->getIdByOrder((int)$currentRow->draw_order+$orderFactor, $currentRow->parent_id);
        
        if($opponentId){
            $table = new Dante_Lib_Orm_Table($this->_tableName);
            $table->draw_order = (int)$currentRow->draw_order+$orderFactor;

            $table->apply(array(
                'id' => $id
            ));
            
            $table = new Dante_Lib_Orm_Table($this->_tableName);
            $table->draw_order = (int)$currentRow->draw_order;

            $table->apply(array(
                'id' => $opponentId
            ));
        }
    }
    
    /**
     * выборка доступных групп
     * @param int $groupId
     * @return array
     * 
     */
    public function getAvailableDivisions($groupId, $lang_id = 1)
    {
        /**
         * @autor editor Elrafir
         */
        if(!is_array($groupId)){
            $groupId=array($groupId);
        }
        
        $sql = "SELECT `d`.*, `dg`.`group_id`, `ds`.`name` AS `name_seo` 
                FROM {$this->_tableName} AS `d` 
                INNER JOIN {$this->_divisionGroupsTableName} AS `dg`
                ON `d`.`id` = `dg`.`division_id`
                LEFT JOIN {$this->_seoTableName} AS `ds`
                ON (`ds`.`menu_id` = `d`.`id` AND `ds`.`lang_id` = {$lang_id})
                WHERE `group_id` IN (".join(', ', $groupId).") AND `is_active` = 1
                ORDER BY `draw_order`";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $divisions = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $divisions[$f['id']] = $f;
        }
        
        return $divisions;
    }
    /**
     * Выборка записей прав доступа разных групп для конкретного дивижина 
     * @param int $id
     */
    public function getAccessById($id){
        $sql = "SELECT *, groups.group_id as id FROM {$this->_groupsTableName} as groups "
        ."LEFT JOIN {$this->_tableAccess} as access ON access.group_id=groups.group_id AND access.division_id={$id} "
        ."WHERE 1"
        ;
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $rows = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $rows[$f['id']] = $f;
        }
        return $rows;
    }
    /**
     * Удаление записей прав доступа разных групп для конкретного дивижина
     * @param Int $id
     */
    public function delAccessById($id){
        if(!$id) return;
        $orm = new Dante_Lib_Orm_Table($this->_tableAccess);
        $orm->delete(array('division_id'=>$id));
        $ormGroup = new Dante_Lib_Orm_Table($this->_divisionGroupsTableName);
        $ormGroup->delete(array('division_id'=>$id));
    }
    /**
     * Удаление записей прав доступа разных групп для конкретного дивижина
     * @param int $id
     * @param array $vars
     */
    public function addAccess($id, $vars){
        foreach((array)$vars as $group=>$fields){
           $orm = new Dante_Lib_Orm_Table($this->_tableAccess);
           $orm->division_id = $id;
           $orm->group_id = $group;
           foreach($fields as $field=>$value){
               $orm->$field = $value;
           }
           $orm->insert(); 
           $ormGroup = new Dante_Lib_Orm_Table($this->_divisionGroupsTableName);
           $ormGroup->division_id = $id;
           $ormGroup->group_id = $group;
           $ormGroup->insert();
        }
    }

    /**
     * Добавление / редактирование раздела
     * @param Module_Division_Manage_Model $model
     * @param array|false кондишн присланный в  ручную , либо false (тогда по id будет)
     */
    public function apply(Module_Division_Manage_Model $model, $condition=false){
        
        $orm = new Dante_Lib_Orm_Table($this->_tableName);
        $orm->id     = $model->id;
        $orm->link   = $model->link;
        $orm->name   = $model->name;        
        $orm->image  = $model->image;
        $orm->d_type = $model->d_type;
        if(!$model ->id){
            $orm->parent_id = $model->parent_id?$model->parent_id:1;
            $id = $orm->apply($condition);
        }else{
            $orm->update(array('id'=>$model->id));
            $id = $model->id;
        }        
        //Добавляем SEO
        $ormSeo =  new Dante_Lib_Orm_Table($this->_seoTableName);
        $ormSeo->menu_id    = $id;
        $ormSeo->lang_id    = $model->lang_id;
        $ormSeo->name       = $model->seoName;
        $ormSeo->title      = $model->title;
        $ormSeo->keywords   = $model->keywords;
        $ormSeo->description = $model->description;      
        $ormSeo->apply(array('menu_id'=>$id, 'lang_id'=>$ormSeo->lang_id ));
        return $id;
    }
    /**
     * СОхраняем родителя и позицию для раздела.
     * @param type $id - Id редактируемой записи
     * @param type $parrent - Id родителя 
     * @param type $order - Сортировочная позиция  в списке
     */
    public function setDivisionPosition($id, $parentId, $order = false){
        $orm = new Dante_Lib_Orm_Table($this->_tableName);
        $orm->draw_order = $order;
        $orm->parent_id  = $parentId;
        $orm->update(array('id'=>$id));
    }
}

?>
