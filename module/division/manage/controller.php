<?
/**
 * Контроллер управления разделами
 * @author Elrafir <elrafir@gmail.com>
 */
class Module_Division_Manage_Controller extends Module_Handbooksgenerator_Controller_Base{
    
    public function _init() {
        parent::_init();
        $this->_mapper = new Module_Division_Manage_Mapper();
    }
    
    protected function _drawAction()
    {
        $helper = new Module_Division_Helper();
        $helper->checkAccess();
        //Подключаем вспомогательные классы        
        $this->_mapper = new Module_Division_Manage_Mapper;
        //Вытаскиваем записи из таблицы
        $cond = array(
            'sort'=> array(
                'name' => 'draw_order',
                'type' => 'asc'
            )
        );
        $result = $this->_mapper->getDivisions($cond);
        //Сортируем записи в древовидный массив
        $rows = $this->_rowContent($result['rows']);
        $tree = Lib_Jquery_Sortable_Service::treeGenerator($rows, 1);
        $divisions = Lib_Jquery_Sortable_Service::treeHtml($tree);
        $this->formEnable('module.division.manage.controller');
        $this->tableParams['class']['table-bordered']=false;
        $this->tableParams['class']['table-hover']=false;
        $this->thead = array(
            'buttons' => array(
                array(
                    'type'=>'add',
                    'content'=>'
                        <button type="button" class="btn btn-success" onclick="division.saveTree()">Сохранить изменения в иерархии</button>
                        <button type="button" class="btn btn-link" onclick=\'handbooks.formSubmit(this); return false\'>Отменить изменения</button>
                    ',
                )                
            )
        );
        $this->tbody = array(
            array(
                $divisions
            )            
        );
        $html = $this->generate();
        
        //Возвращаем HTML
        return $html;
    }
    /**
     * Подготавливаем контент строк в формате древовидной сортировки
     * @param array $rows обрабатываемые данные разделов
     * @return array масив даных в формате array(array('id'=>2, 'parent_id'=>1, 'content'=>'html'));
     */
    protected function _rowContent($rows){
        foreach($rows as &$cur){
            $cur['type'] = array('edit', 'del', 'active');
            $row = ($cur['seo_name']?$cur['seo_name']:$cur['name']);
            $row.= $this->_parseByPattern($cur);            
            $row.= "<b class='pull-right' style='width:90px;'>&nbsp;{$cur['d_type']}</b>";
            $row.= "<b class='pull-right' style='width:400px;'>&nbsp;{$cur['link']}</b>";
            $cur['content']=$row;
        }             
        return $rows;
    }
    /**
     * Отображаем форму для добавления/редактирования записей
     */
    protected function _drawFormAction($id = false){        
        $mapper = new Module_Division_Manage_Mapper();
        $model  = new Module_Division_Manage_Model;
        $tpl    = new Module_Division_Manage_View();
        $request = $this->_getRequest();
        $langId = $request->get_validate('language', 'int', 1);
        if($id = $request->get_validate('id', 'int', $id)){
            $request->set('id', $id);
            $params = $this->getParams($model);
            $vars = $mapper->getDivisionsByParams($params, $langId);
        }
        $vars['lang_array'] = $mapper->getLanguages();
        $vars['lang_id'] = $langId;
        $html = $tpl->form($vars);
        //Модальное окно для прав доступа
        $modal = new Lib_Jquery_Arcticmodal_Controller;
        $html.=$modal->draw('divisionAccess');
        return array(
            'result'=>1,
            'html'=>$html
        );
    }
    /**
     * редактирование/добавление разделов.
     * @author Elrafir
     */
    protected function _editAction(){
        
        $model  = new Module_Division_Manage_Model;
        $mapper = new Module_Division_Manage_Mapper;
        $requets = $this->_getRequest();
        //Присваеваем значения полей. Системные
        $model->id      = $requets->get_validate('id', 'int');
        $model->d_type  = $requets->get_validate('d_type', 'text', 'url');
        $model->name    = $requets->get_validate('name', 'text');
        $model->link    = $requets->get_validate('link', 'text');
        $model->image   = $requets->get_validate('image', 'text');
        //Присваеваем значения полей. SEO
        $model->seoName = $requets->get_validate('seoName', 'text');
        $model->title   = $requets->get_validate('title', 'text');
        $model->description = $requets->get_validate('description', 'text');
        $model->keywords    = $requets->get_validate('keywords', 'text');
        $model->lang_id = $requets->get_validate('language', 'text', 1);
        //подгоняем URL
        if(!$model->link) $model->link = $model->name;
        $urlText  = str_replace('.html', '', $model->link);
        $model->url  = Dante_Lib_Transliteration::transform($urlText).'.html';
        //Сохраняем
        $id = $mapper->apply($model);        
        //Возвращаем html форму        
        return $this->_drawFormAction($id);        
    }
    /**
     * Сохранение древа расположения разделов
     * @return array массив json
     */
    protected function _saveTreeAction(){        
        $mapper = new Module_Division_Manage_Mapper;
        $request    = $this->_getRequest();        
        $list      = $request->get_validate('list', 'array');
        $order = 1;
        foreach ($list as $id=>$parent){
            $parentId = $parent=='null'?'1':$parent;           
            $mapper->setDivisionPosition($id, $parentId, $order);
            $order++;
        }
        $html = $this->_drawAction();
        return array(
            'result'=>1,
            'html'=>$html
        );
    }
    
    protected function _accessFormAction(){
        $mapper = new Module_Division_Manage_Mapper;
        $tpl    = new Module_Division_Manage_View();
        $id = $this->_getRequest()->get_validate('id', 'int', false);
        $vars['rows']   = $mapper->getAccessById($id);
        $vars['id']     = $id;
        
        $html = $tpl->accessFrom($vars);
        return array(
            'result'=>1,
            'html'=>$html
        );
    }
    /**
     * Сохранение данных о правах доступа
     */
    protected function _accessEditAction(){
        $mapper = new Module_Division_Manage_Mapper;
        $request = $this->_getRequest();
        $id = $request->get_validate('id', 'int');
        $vars = $request->get_validate('vars', 'array');
        //Удаляем записи существующие
        $mapper->delAccessById($id);
        //Добавляем записи новые
        $mapper->addAccess($id, $vars);
        //возвращаем новый грид с правами доступа
        return $this->_accessFormAction();
    }
}