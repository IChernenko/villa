<?php



/**
 * Description of dmo
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Division_Dmo extends Dante_Lib_Orm_Table{
    
    protected $_tableName = '[%%]division';
    
    /**
     *
     * @return Module_Division_Dmo 
     */
    public static function dmo() {
        return self::getInstance();
    }


    public function byId($id) {
        return $this->getByAttributes(array('id'=>$id));
    }

    /**
     * Получение радела по имени
     * @param string $name
     * @return $this
     */
    public function byName($name) {
        return $this->getByAttributes(array('name'=>$name));
    }
}

?>
