<?php

/**
 * Description of manage
 *
 * @author Mort
 */
class Module_Division_Helper {
    
    /**
     * возвращаем ид группы юзера
     * @return int 
     */
    public function getGroupId()
    {
        $userId = Dante_Helper_App::getUid();
        
        $groupsMapper = new Module_User_Mapper_Manage_Groups();
        $groupId = $groupsMapper->getUserGroupId($userId);
        
        return $groupId;
    }
    
    /**
     * проверяем, есть ли доступ по заданному урлу
     * @return boolean 
     */
    public function checkAccess($url = false, $res = false)
    {
        if(!$url)
            $url = Dante_Controller_Front::getRequest()->get_requested_url();
        
        $mapper = new Module_Division_Mapper();
        $division = $mapper->getByLink($url);
        if(!$division){
            //нет записи для данного урл
            return true;
        }
        $groupId = $this->getGroupId();
        
        $access = $mapper->checkDivisionAccess($division, $groupId);
        
        if(!$access)
        {
            if(!$res)
            {
                echo 'Access is denied';
                die();
            }
            else return 0;
        }
        
        return $access;
    }
    
    
}

?>
