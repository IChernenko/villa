<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 * Date: 9/22/13
 * Time: 12:39 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Description of dmo
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Division_Dmo_Groups extends Dante_Lib_Orm_Table{

    protected $_tableName = '[%%]division_groups';

    /**
     *
     * @return Module_Division_Dmo
     */
    public static function dmo() {
        return self::getInstance();
    }
}