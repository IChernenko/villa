<?php



/**
 * Description of dmo
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Division_Dmo_Seo extends Dante_Lib_Orm_Table{
    
    protected $_tableName = '[%%]division_seo';
    
    /**
     *
     * @return Module_Division_Dmo 
     */
    public static function dmo() {
        return self::getInstance();
    }


    public function byId($id) {
        return $this->getByAttributes(array('id'=>$id));
    }
    
    /**
     * 
     * @param type $id
     * @return Module_Division_Dmo_Seo
     */
    public function byDivision($id) {
        return $this->getByAttribute(array('menu_id'=>$id));
    }
    
    /**
     * 
     * @param type $langId
     * @return Module_Division_Dmo_Seo
     */
    public function byLang($langId) {
        return $this->getByAttribute(array('lang_id'=>$langId));
    }
    

}

?>
