<?php



/**
 * Description of mapper
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Division_Mapper 
{    
    protected $_tableName = '[%%]division';    
    protected $_divisionGroupsTableName = '[%%]division_groups';    
    protected $_tableAccess = '[%%]division_access';
    
    public function getAll($langId, $filter = false, $active = false) {
        $sql = "select 
                    ds.name,
                    d.link,
                    d.id,
                    d.parent_id,
                    d.d_type,
                    d.is_active
                from [%%]division as d
                left join [%%]division_seo as ds on (ds.menu_id = d.id and ds.lang_id={$langId}) 
                WHERE `is_active` = 1";
                
        if (is_array($filter) && count($filter)>0) 
        {
            foreach($filter as $field => $value) 
            {
                $sql .= " AND {$field} = {$value}";
            }
        } 
        
        $sql .= " order by d.draw_order asc";
                //echo($sql);
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);        
        
        $list = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $model = new Module_Division_Model();
            $model->id = (int)$f['id'];
            $model->name = $f['name'];
            $model->link = $f['link'];
            $model->parent = (int)$f['parent_id'];
            $model->d_type = $f['d_type'];
            $model->active = $f['is_active'];

            //if ($active && $model->id == $active) $model->active = true;
            
            $list[(int)$f['parent_id']][(int)$f['id']] = $model;
        }
        //print_r($list);
        return $list;
    }
    
    public function getByLink($link) {
        $sql = "select id from [%%]division where link='$link'";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);     
        if (!$r) return false;
        
        $f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r);
        if (isset($f['id'])) return (int)$f['id'];
        return false;
    }
    
    public function getByName($name) {
        $sql = "select id from [%%]division where name='$name'";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);     
        if (!$r) return false;
        
        $f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r);
        if (isset($f['id'])) return (int)$f['id'];
        return false;
    }
    
    /**
     * 
     * @param type $id
     * @return Module_Division_Model
     */
    public function getById($id) {
        $dmo = new Module_Division_Dmo();
        $dmo->byId($id)->fetch();
        
        $division = new Module_Division_Model();
        $division->name         = $dmo->name;
        $division->parent       = $dmo->parent_id;
        $division->link         = $dmo->link;
        $division->drawOrder    = $dmo->draw_order;
        $division->link         = $dmo->link;
        $division->is_active    = $dmo->is_active;
        
        $dmoSeo = new Module_Division_Dmo_Seo();
        $dmoSeo->byDivision($id)->byLang(Module_Lang_Helper::getCurrent())->fetch();
        
        $division->name = $dmoSeo->name;
        $division->lang = $dmoSeo->lang_id;
        $division->title = $dmoSeo->title;
        $division->description = $dmoSeo->description;
        $division->keywords = $dmoSeo->keywords;
        
        return $division;
    }
    
    public function add(Module_Division_Model $model) {
        $dmo = new Module_Division_Dmo();
        $dmo->name          = $model->name;
        $dmo->parent_id     = $model->parent;
        $dmo->link          = $model->link;
        $dmo->draw_order    = $model->drawOrder;
        $dmo->link          = $model->link;
        $model->id = $dmo->insert();
        if (!$model->id) throw new Exception("cant insert division");
        
        // сохарним seo
        $dmoSeo = new Module_Division_Dmo_Seo();
        $dmoSeo->name = $model->name;
        $dmoSeo->lang_id = $model->lang;
        $dmoSeo->title = $model->title;
    }     
    
    /**
     * проверяем, есть ли доступ
     * @param int $division
     * @param int $groupId
     * @return boolean 
     */
    public function checkDivisionAccess($division, $groupId)
    {
        //а есть ли запись для данной штуки ? 
        $table = new Dante_Lib_Orm_Table($this->_divisionGroupsTableName);
        $cond = array(
            'division_id' => $division,
            'group_id' => $groupId
        );
        if($table->exist($cond))
            return $this->getAccessOptions($division, $groupId);
        
        return false;
    }
    
    public function getAccessOptions($division, $group)
    {
        $sql = "SELECT `create`, `read`, `update`, `delete` FROM {$this->_tableAccess} 
                WHERE `division_id` = {$division} AND `group_id` = {$group}";
            
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        if(!$r) return true;
        
        $f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r);
        if(!$f) return true;
        
        return $f;
    }
}

?>
