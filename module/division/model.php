<?php


/**
 * Модель раздела
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Division_Model {
    /**
     * id системный идентификатор  генерируется ai 
     * @var int 
     */
    public $id;
    /**
     * 
     * @var type 
     */
    public $name;
    
    public $link;
    
    public $parent;
    
    public $is_active;
    
    public $image;
    
    public $drawOrder;
    
    public $lang;
    
    public $title;
    
    public $keywords;
    
    public $description;
    
}

?>
