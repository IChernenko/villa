division = {
    
    controller:"module.division.manage.controller",
    formId:"editHandbookForm",
    //Отправляем форму с данными
    edit: function(obj, closed){        
        publications.performer(obj);
        $("textarea.editor").elrte('updateSource');
        var data=$('#editDivision').serialize();
        $.ajax({
            type: 'POST',
            data:data,
            url: '/ajax.php'+(!closed?'?returntype=1':''),
            dataType : "json",
            success: function (data){
                if(data.result==1){
                    if(closed){
                        $('#modalWindow').arcticmodal('close');
                        handbooks.formSubmit();
                    }else{                                               
                        $('#modalWindow .modal-body').html(data.html);
                    }                    
                }else{
                    alert(data.message)
                }
            }
        });
        
    },
    performer: function(obj){
        $(obj).removeClass('btn-success btn-info btn-normal btn-danger btn-warning').text('\u0412ыполняю');
    },
    saveTree: function(){
        handbooks.buttonsDisabled()	
        var data = $('ol.sortable').nestedSortable('serialize');
        $.ajax({
            type: 'POST',
            data:data,
            url: '/ajax.php?controller='+division.controller+'&action=saveTree',
            dataType : "json",
            success: function (data){                
                if(data.result==1){
                   $('#'+division.formId).parent().html(data.html);
                }else{
                    alert(data.message)
                }
                handbooks.buttonsEnabled()
            }
        });
    },
    
    changeLang: function(obg){
        handbooks.buttonsDisabled()
        $('input#action').val('drawForm');
        var data=$('#editDivision').serialize();
        $.ajax({
            type: 'POST',
            data:data,
            url: '/ajax.php',
            dataType : "json",
            success: function (data){
                if(data.result==1){                                                                  
                    $('#modalWindow .modal-body').html(data.html);                                        
                }else{
                    alert(data.message)
                }
                handbooks.buttonsEnabled();
            }
        });        
    },
    
    openAccess: function(id){
        handbooks.buttonsDisabled();
        var params={
            id:id,
            action:'accessForm',
            controller:'module.division.manage.controller'            
        }              
        $('#divisionAccess').arcticmodal({
            type: 'ajax',
            url: 'ajax.php',
            ajax: {
                type:'get',
                dataType:"json",
                q:1,
                data:params,
                success:function(data,el,responce){                    
                    data.body.html(el);
                    $(el).find('.modal-body').html(responce.html);
                    handbooks.buttonsEnabled();                    
                }
            },
            beforeClose: function(data, el) {
                handbooks.buttonsEnabled();
            }
        });
    },
    
    editAccess: function(){
        handbooks.buttonsDisabled();        
        data = $('#divisionAccessForm').serializeArray();
        $.ajax({
            type: 'POST',
            data:data,
            url: '/ajax.php',
            dataType : "json",
            success: function (data){
                if(data.result==1){
                    $('#divisionAccess .modal-body')
                    alert = $('<div class="alert alert-success">Изменения сохранены<button type="button" class="close" data-dismiss="alert">×</button></div>').alert().appendTo('#divisionAccess .modal-body').animate({opacity:0.9}, 2000).slideUp(function(){$(this).remove()});                    
                }else{
                    alert(data.message)
                }
                handbooks.buttonsEnabled();
            }
        });
    }
}