<?php

Module_Division_Manage_Service::create(array(
    'name'      => 'root',
    'is_active' => true
));

Module_Division_Manage_Service::create(array(
    'name'      => 'adminka',
    'parent'    => 'root',
    'is_active' => true
));

Module_Division_Manage_Service::create(array(
    'name'      => 'client',
    'parent'    => 'root',
    'is_active' => true
));

Module_Division_Manage_Service::create(array(
    'name'      => 'Разделы',
    'parent'    => 'adminka',
    'url'       => '/manage/division/',
    'is_active' => true,
    'groups'    => array('admin')
));

Module_Division_Manage_Service::create(array(
    'name'      => 'Языки',
    'parent'    => 'adminka',
    'url'       => '/manage/languages/',
    'is_active' => true,
    'groups'    => array('admin')
));

Module_Division_Manage_Service::add(array(
    'Метки',
    '/manage/labels/',
    array(
        3,
        2,
        1
    ),
    'adminka'
));


?>