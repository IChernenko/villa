ALTER TABLE [%%]division_seo modify column `menu_id` int(11) unsigned NOT NULL;
-- Удаляем записи из сео таблицы которых нет в основной таблице
DELETE FROM `[%%]division_seo` WHERE `menu_id` NOT IN (SELECT `id` FROM [%%]division);
-- ALTER TABLE  `[%%]division_seo` DROP FOREIGN KEY  `fk_division_seo_menu_id` ;
-- ALTER TABLE [%%]division_seo ADD
     -- CONSTRAINT fk_division_seo_menu_id
     -- FOREIGN KEY (`menu_id`)
     -- REFERENCES [%%]division(id) ON DELETE CASCADE ON UPDATE CASCADE;
