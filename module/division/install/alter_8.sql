
ALTER TABLE `[%%]division_access` CHANGE `delite` `delete` TINYINT(1) DEFAULT 0 COMMENT 'право удаления';
SET foreign_key_checks = 0;
ALTER TABLE `[%%]division_access` MODIFY COLUMN `division_id` INT(11) UNSIGNED NOT NULL COMMENT 'id раздела';
ALTER TABLE `[%%]division_access` MODIFY COLUMN `group_id` INT(11) NOT NULL COMMENT 'id группы доступа';
ALTER TABLE `[%%]division_access` MODIFY COLUMN `create` TINYINT(1)  DEFAULT 0 COMMENT 'право добавления';
ALTER TABLE `[%%]division_access` MODIFY COLUMN `read` TINYINT(1)  DEFAULT 0 COMMENT 'право чтения';
ALTER TABLE `[%%]division_access` MODIFY COLUMN `update` TINYINT(1)  DEFAULT 0 COMMENT 'право редактирования';
SET foreign_key_checks = 1;