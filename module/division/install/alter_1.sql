CREATE TABLE IF NOT EXISTS [%%]division (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `parent_id` int(11) unsigned DEFAULT '0',
  `link` varchar(128) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `draw_order` int(11) DEFAULT NULL,
  `d_type` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Категории' AUTO_INCREMENT=1 ;

ALTER TABLE `[%%]division` add column `is_active` TINYINT(1) NOT NULL DEFAULT 0;

CREATE TABLE IF NOT EXISTS [%%]division_seo (
  `menu_id` int(11) unsigned DEFAULT NULL,
  `lang_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='SEO данные категорий';

ALTER TABLE [%%]division_seo ADD
      CONSTRAINT fk_division_seo_lang_id
      FOREIGN KEY (`lang_id`)
      REFERENCES [%%]languages(id) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS [%%]division_groups (
  `division_id` int(11) unsigned DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='соответствие категорий и групп пользователей';

ALTER TABLE [%%]division_groups ADD
      CONSTRAINT fk_division_groups_division_id
      FOREIGN KEY (`division_id`)
      REFERENCES [%%]division(id) ON DELETE CASCADE ON UPDATE CASCADE;
      
ALTER TABLE [%%]division_groups ADD
      CONSTRAINT fk_division_groups_group_id
      FOREIGN KEY (`group_id`)
      REFERENCES [%%]groups(group_id) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS `[%%]division_access` (
  `division_id` int(11) unsigned DEFAULT NULL COMMENT 'id раздела',
  `group_id` int(11) DEFAULT NULL COMMENT 'id группы доступа',
  `create` tinyint(1) DEFAULT NULL COMMENT 'право добавления',
  `read` tinyint(1) DEFAULT NULL COMMENT 'право чтения',
  `update` tinyint(1) DEFAULT NULL COMMENT 'право редактирования',
  `delite` tinyint(1) DEFAULT NULL COMMENT 'право удаления',
  KEY `division_id` (`division_id`,`group_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `division_access`
--
ALTER TABLE `[%%]division_access`
  ADD CONSTRAINT `division_access_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `[%%]groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `division_access_ibfk_1` FOREIGN KEY (`division_id`) REFERENCES `[%%]division` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


