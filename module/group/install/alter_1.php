<?php

// установка первичных групп
$groupMapper = new Module_Group_Mapper();

$group = new Module_Group_Model();
$group->name = 'guest';
$group->caption = 'Гость';
$groupMapper->add($group);

$group->name = 'user';
$group->caption = 'Зарегистрированный пользователь';
$groupMapper->add($group);

$group->name = 'admin';
$group->caption = 'Администратор';
$groupMapper->add($group);

?>
