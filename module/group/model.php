<?php



/**
 * Модель группы
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Group_Model {
    
    /**
     *
     * @var int 
     */
    public $id;
    
    /**
     *
     * @var string 
     */
    public $name;
    
    /**
     *
     * @var string 
     */
    public $caption;
}

?>
