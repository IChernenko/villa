<?php



/**
 * Description of mapper
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Group_Mapper {
    
    /**
     * Добавление группы
     * @param Module_Group_Model $group 
     */
    public function add(Module_Group_Model $group) {
        $dmo = new Module_Group_Dmo();
        
        // проверим а вдруг такое уже есть
        if ($dmo->exist(array('group_name'=>$group->name))) {
            return false;
        }
        
        $dmo->group_name     = $group->name;
        $dmo->group_caption  = $group->caption;
        $group->id = $dmo->insert();
        return true;
    }
    
    public function getByName($name) {
        $dmo = new Module_Group_Dmo();
        $dmo->byName($name)->fetch();
        return $dmo->group_id; 
    }
}

?>
