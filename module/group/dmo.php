<?php



/**
 * Description of dmo
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Group_Dmo extends Dante_Lib_Orm_Table{
    
    protected $_tableName = '[%%]groups';
    
    public function byId($id) {
        return $this->getByAttributes(array('id'=>$id));
    }
    
    public function byName($name) {
        return $this->getByAttributes(array('group_name'=>$name));
    }
}

?>
