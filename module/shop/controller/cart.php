<?php
/**
 * Контроллер корзины
 * User: dorian
 * Date: 03.04.12
 * Time: 15:34
 * To change this template use File | Settings | File Templates.
 */
class Module_Shop_Controller_Cart extends Dante_Controller_Base {

    /**
     * Добавить товар в корзину
     */
    protected function _addAction() {
        $id = (int)$this->_getRequest()->get('id');
        $quantity = (int)$this->_getRequest()->get('quantity');
        if ($quantity == 0) $quantity = 1;
        $type = (int)$this->_getRequest()->get('type');
        
        $mapper = new Module_Shop_Mapper_Cart();
        $insert = $mapper->add($id, $type, $quantity);

        return array('result'=>1, 'insert'=>$insert);
    }

    /**
     * Удалить товар из корзины
     * @return array
     */
    protected function _removeAction() {
        $id = (int)$this->_getRequest()->get('id');

        $mapper = new Module_Shop_Mapper_Cart();
        $mapper->remove($id);

        return array('result'=>1);
    }

    protected function _updateAction() {
        $id = (int)$this->_getRequest()->get('id');
        $quantity = (int)$this->_getRequest()->get('quantity');
        if ($quantity<0) {
            return array('result'=>0);
        }

        $mapper = new Module_Shop_Mapper_Cart();
        $mapper->update($id, $quantity);

        return array('result'=>1);
    }

    protected function _defaultAction() {

        $mapper = new Module_Shop_Mapper_Cart();
        $cart = $mapper->get();

        $view = new Module_Shop_View_Cart();
        return $view->draw($cart);
    }
}
