<?php
/**
 * Превью для корзины
 * User: dorian
 * Date: 09.04.12
 * Time: 18:00
 * To change this template use File | Settings | File Templates.
 */
class Module_Shop_Controller_Cartpreview extends Dante_Controller_Base {

    protected function _defaultAction() {
        $mapper = new Module_Shop_Mapper_Cart();
        $cart = $mapper->get();
        
        $view = new Module_Shop_View_Cartpreview();
        return $view->draw($cart);
    }
}
