<?php
/**
 * Контроллер последних просмотренных товаров
 * User: dorian
 * Date: 12.04.12
 * Time: 18:59
 * To change this template use File | Settings | File Templates.
 */
class Module_Shop_Controller_Lastvisited extends Dante_Controller_Base {

    protected function _defaultAction() {

        $mapper = new Module_Shop_Mapper_Lastvisited();
        $list = $mapper->getList();
        


        $view = new Module_Shop_View_Lastvisited();
        return $view->draw($list);
    }
}
