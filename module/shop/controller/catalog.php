<?php
/**
 * Контроллер отображения каталога продукции
 *
 * User: dorian
 * Date: 22.03.12
 * Time: 12:30
 * To change this template use File | Settings | File Templates.
 */
class Module_Shop_Controller_Catalog extends Dante_Controller_Base {

    protected function _defaultAction() 
    {
        
        $mapper = new Module_Shop_Mapper_Catalog();
        $view = new Module_Shop_View_Catalog();
        // todo: получить id категории
        $categoryId = (int)$this->_getParam('%1');
        if ($categoryId == 0) throw new Exception("cant get category");
                       
        ##Определяем, нужна ли сортировка
        $order = false;
        $pageParam = '%3';
        if($this->_getParam('%3') == 'order')
        {
            $orderField = $this->_getParam('%4');
            $orderType = 'ASC';
            $view->sort = array(
                'type' => 0
            );
            if(stristr($orderField, '-rev')) 
            {
                $view->sort = array(
                    'type' => 1
                );
                $orderType = 'DESC';
                $orderField = str_ireplace('-rev', '', $orderField);
            }
            $view->sort['field'] = $orderField;
            $order = "ORDER BY `{$orderField}` {$orderType}";    
            $pageParam = '%5';
        }
        elseif(Dante_Lib_Config::get('shop.catalogSortDefault'))
        {
            $field = Dante_Lib_Config::get('shop.sortDefField');
            $type = Dante_Lib_Config::get('shop.sortDefType');
            if($field)
                $order = "ORDER BY `{$field}` {$type}";
        }
        
        ##Собираем paginator
        $paginator = new Lib_Paginator_Driver();
        $page = (int)$this->_getParam($pageParam, 1);
        $countRows = $mapper->getCountList($categoryId);
        $paginator->link = str_replace('/page'.$this->_getParam($pageParam), '', $this->_getParam('%0'));
        $paginator->draw($countRows, $page, Dante_Lib_Config::get('shop.catalog.productsPerPage'));
        
        
        $categoryMapper = new Module_Shop_Mapper_Category();
        $category = $categoryMapper->get($categoryId);
        $categList = $categoryMapper->getList($categoryId);

        $list = $mapper->getList($categoryId, $order, $paginator->limit);
        //$var
        $currency = Module_Currency_Helper::getCurrentCurrency();
        foreach($list as &$product)
        {
            $product->price = Module_Currency_Helper::convert($product->price);
        }
        $view->currency = $currency['reduction'];

        $view->category = $category;
        $view->productCount = count($list);
        return $view->draw($list, $paginator->html, $categList);
    }


}
