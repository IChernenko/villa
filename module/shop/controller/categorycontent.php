<?php

/**
 * Контроллер древовидных категорий.
 * Обычно используется в контентной части
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Shop_Controller_Categorycontent extends Dante_Controller_Base {

    protected function _defaultAction() {
        $mapper = new Module_Shop_Mapper_Category();
        $list = $mapper->getListTree();
        
        $view = new Module_Shop_View_Categorycontent();
        $html = $view->draw($list, Module_Shop_Helper_Shop::CATEGORY_ROOT);
        return '<div id="catalog">'.$html.'<div class="separator"></div></div>';
    }
}

?>
