<?php
/**
 * Контроллер отображения товара
 * User: dorian
 * Date: 02.04.12
 * Time: 18:32
 * To change this template use File | Settings | File Templates.
 */
class Module_Shop_Controller_Product extends Dante_Controller_Base {

    protected function _getProductIdFromUrl($url) {
        if (preg_match_all("/^\/product\/(\d+)\-(.*?)/", $url, $matches)) {
            return $matches[1][0];
        }
        return false;
    }

    protected function _defaultAction() {
        
        //$productId = (int)$this->_getParam('%1');
        $link = $this->_getParam('url');

        $id = $this->_getProductIdFromUrl($link);
        if (!$id) throw new Exception("cant find product id for url : $link");

        //$link = str_ireplace('/product/', '', $link);
        
        // todo: получить id продукта
        //$productCode = $this->_getParam('%1');

        $productMapper = new Module_Shop_Mapper_Catalog();
        //$product = $productMapper->getByCode($productCode);
        //$id = $productMapper->getIdByLink($link);
        $product = $productMapper->getById($id);
        
        $currency = Module_Currency_Helper::getCurrentCurrency();
        if ($currency['factor'] == 0) $currency['factor'] = 1;
        $product->price = Module_Currency_Helper::convert($product->price);

        /*echo('<pre>');
        print_r($product);
        echo('</pre>');*/

        $images = $productMapper->getProductImagesInCategory($product->category);
        

        // запомним, что мы смотрели этот товар
        $lastVisited = new Module_Shop_Mapper_Lastvisited();
        $lastVisited->add($product->id);



        $view = new Module_Shop_View_Product();
        $view->setImages($images);

        $productEntity = Module_Entity_Helper::getBySysName('product');
        if (!$productEntity) throw new Exception('cant find entity product');

        // поддержка комментариев
        $comments = new Module_Comment_Controller_Comment();
        $view->getTpl()->comments = $comments->draw($product->id, $productEntity->id);

        if (!isset($currency['reduction'])) $currency['reduction'] = 'грн';
        return $view->draw($product, $currency['reduction']);
    }
}
