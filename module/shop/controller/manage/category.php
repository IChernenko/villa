<?php
/**
 * Управление каталогами магазина
 * @author Elrafir <elrafir@gmail.cm>
 */

class Module_Shop_Controller_Manage_Category extends Module_Handbooksgenerator_Controller_Base {
    
    function __construct() 
    {
        parent::__construct();        
        Module_Acl_Controller::accessModer();
    }
    
    protected $_list = false;
    protected $_category = false;
    protected $_parentId = false;
    
    protected function _drawAction() 
    {
        $mapper = new Module_Shop_Mapper_Manage_Category();
        $model = new Module_Shop_Model_Manage_Category();   
        
        $params = $this->getParams($model);
        $this->_list = $mapper->getCategories($params); 
                
        $this->formEnable('module.shop.controller.manage.category');
        $titles = array(
            array(
                'content'=>'Название каталога',
                'sortable'=>'name',
            ),
            array(
                'content'=>'Каталог-родитель',
                'sortable'=>'parent_name',
            ),
            array(
                'content'=>'URL-псевдоним',
                'sortable'=>'link',
            ),                    
            'Изображение',
            array(
                'content'=>'Позиция',
                'sortable'=>'draw_order',
                'sortable_def'=>'',
            ), 
            'Функции'
        );        
        
        $this->_parentId = $this->_getRequest()->get_validate('parent_id', 'int');
        $this->_getRequest()->set('parent_id', false);
        
        $categoriesFull = $mapper->getCategories(false, true); 
        $combobox = $this->_selectGenerate($categoriesFull['rows'], false, true);
        
        $filters = array(
            'name'=>array(
                'type'=>'text',
                'value'=>$this->_getRequest()->get_validate('name', 'int')
            ),
            'parent_id'=>array(
                'content' => $combobox,
                'params' => array(
                    'eval'=>"$('#categoryParentSelect').ikSelect({autoWidth: true, ddFullWidth: true});"
                )
            ),
            'link'=>array(
                'type'=>'text',
                'value'=>$this->_getRequest()->get_validate('link', 'text')
            ),
            NULL,
            NULL,
            'button_filter'=>array(
                'type'=>'button_filter'
            )
        );
        /**
         * параметры формы
         */
        $this->thead=array($titles, $filters);
        
        $this->tbody=$this->_genRows();
       
        $this->tfoot=array(
            array(
                $this->paginator($this->_list['rowCount'], $params)                              
            ),
            array(
                'button_filter'=>array(
                    'type' => array('edit')
                )
            )
        );
        
        return $this->generate();
    }
    
    protected function _genRows()
    {        
        $table = array();
        $path = Module_Shop_Helper_Shop::getCategoriesFolder();
        foreach($this->_list['rows'] as $cur)
        {
            $row = array();            
            $row['name'] = $cur['name'];
            $row['parent_name'] = $cur['parent_name']?$cur['parent_name']:'-';
            $row['link'] = $cur['link'];
            $row['image'] = !$cur['image']?'нет изображения':"<img src='".$path.$cur['image']."' class='category_prev'>";
            $row['draw_order'] = $cur['draw_order'];
            $row['buttons'] = array(
                'id'=>$cur['id'],
                'type'=>array('edit', 'del'),                
            );
            
            $table[] = $row;
        }
        return $table;
    }
    
    protected function _drawFormAction()
    {        
        $model = new Module_Shop_Model_Category();
        $mapper = new Module_Shop_Mapper_Manage_Category();
        $uploader = new Module_Uploadify_Controller_Base();
        
        if($id = $this->_getRequest()->get_validate('id', 'int', '0'))
        {
            $this->_category = $mapper->getCategory($id);
            $model = $this->_category;
        }
        
        $categoryList = $mapper->getCategories(false, true);
        $combobox = $this->_selectGenerate($categoryList['rows']);

        $uploadParams=array(
            'swf_url'=>$uploader->swf_url,
            'server_url'=>$uploader->server_url
        );
        
        $view = new Module_Shop_View_Manage_Category();        
        $html = $view->drawForm($model, $combobox, $uploadParams);
        
        return array(
            'result' => 1,
            'html' => $html
        );
    }
    
    /**
     * Заполнение модели категории
     * @author Dorian
     * @return Module_Shop_Model_Category
     */
    protected function _populateModel() 
    {
        $category = new Module_Shop_Model_Category();
        $category->id       = (int)$this->_getRequest()->get_validate('id', 'int');
        $category->name     = $this->_getRequest()->get_validate('name', 'text');
        $category->image    = rawurldecode(basename($this->_getRequest()->get_validate('image', 'text', false)));        
        $category->url      = $this->_getRequest()->get_validate('url', 'text', '');
        if(!$category->url) $category->url = Dante_Lib_Transliteration::transform($category->name);
        
        $category->parent   = (int)$this->_getRequest()->get_validate('parent_id', 'int');
        $category->draw_order    = (int)$this->_getRequest()->get_validate('draw_order', 'int');
        return $category;
    }
    
    /**
     * Редактировани категории
     * @return type 
     */
    protected function _editAction()
    {
        $category = $this->_populateModel();
        $mapper = new Module_Shop_Mapper_Manage_Category();  
        
        $curImage = $mapper->getCurrentImage($category->id);
        $imgDir = Module_Shop_Helper_Shop::getCategoriesFolder();
        if(is_file($imgDir.$curImage) && $curImage !== $category->image) 
            unlink($imgDir.$curImage);
        
        $mapper->editCategory($category);        
        return array(
            'result'=>1
        );
    }
    
    protected function _delAction()
    {        
        if(!$id = $this->_getRequest()->get_validate('id', 'int')){
            return array(
                    'result'=>0,
                    'No valide category ID '
               );
        }
        $mapper = new Module_Shop_Mapper_Manage_Category();
        $mapper->del($id);
        return array(
            'result'=>1,
            'html'=>mysql_error()
        );
    }
    
    protected function _imageUploadAction()
    {
        if(isset($_FILES['Filedata']))
        {    
            $uploadDir = Module_Shop_Helper_Shop::getCategoriesFolder();                       
            $newName = md5(time());                         
            
            $uploader = new Component_Swfupload_Upload();
            $image = $uploader->upload($uploadDir, $newName);             
        }
        
        return array(
            'result' => 1,
            'image' => $image,
            'imageDir' => $uploadDir
        );
    }
     
    /**
     * 
     * @param array $rows 
     * @param boolean $endText - указывает добавлять ли в конце категории запись "В каталоге - "
     * @return type
     */    
    protected function _selectGenerate($rows, $endText = true, $filter = false)
    {
        $service = new Module_Shop_Service_Manage_Category();
        $service->category = $this->_category;
        
        if($this->_category){
            $selected =  $this->_category->parent;
        }elseif($this->_parentId !== false){
            $selected =  $this->_parentId;
        }else{
            $selected = false;
        }
        
        return $service->comboboxGenerate($rows, $selected, 'parent_id', $endText, $filter);
    }

    protected function _drawExtInfoAction() {
        $categoryId = (int)$this->_getRequest()->get('id');
        if (!$categoryId) throw new Exception("Не задана категория");

        $mapper = new Module_Shop_Mapper_Manage_Category();
        $category = $mapper->getCategorySeo(array(
            'category_id' => $categoryId,
            'lang_id' => Module_Lang_Helper::getCurrent()
        ));

        if (!$category) {
            $category = new Module_Shop_Model_Category();
            $category->id = $categoryId;
        }

        $tplFileName = '../module/shop/template/manage/category/extinfo.html';

        $tpl = new Dante_Lib_Template();
        $tpl->category = $category;
        $html = $tpl->draw($tplFileName);

        return array(
            'result' => 1,
            'message' => $html
        );
    }

    protected function _saveExtInfoAction() {

        $category = new Module_Shop_Model_Category();
        $category->id = (int)$this->_getRequest()->get('id');
        $category->title = $this->_getRequest()->get('title');
        $category->keywords = $this->_getRequest()->get('keywords');
        $category->description = $this->_getRequest()->get('description');

        $mapper = new Module_Shop_Mapper_Manage_Category();
        $mapper->updateSeo($category);

        return array('result'=>1);
    }
}
?>
