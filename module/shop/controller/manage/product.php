<?php
/**
 * Управление товарами магазина
 * @author Elrafir <elrafir@gmail.cm>
 */
class Module_Shop_Controller_Manage_Product extends Module_Handbooksgenerator_Controller_Base {
    
    function __construct() 
    {
        parent::__construct();        
        Module_Acl_Controller::accessModer();
    }
    
    protected $_productsListing=false;
    protected $_product=false;
    
    protected function _drawAction() 
    {
        $mapper = new Module_Shop_Mapper_Manage_Products();
        $model = new Module_Shop_Model_Manage_Products();
        
        $params=$this->getParams($model);
        $this->_productsListing=$mapper->getAll($params);
        
        //Добавляем селект для выбора родителя
        $cat_mapper=new Module_Shop_Mapper_Manage_Category();
        $categories=$cat_mapper->getCategories(false, true);
        $parent_id=$this->_getRequest()->get_validate('category_id', 'int', '');
        $this->formEnable('module.shop.controller.manage.product');
        
        $titles=array(
                    array(
                        'content'=>'Название продукта',
                        'sortable'=>'name',
                    ),
                    array(
                        'content'=>'URL',
                        'sortable'=>'link',
                        'sortable_def'=>'',
                    ),
                    array(
                        'content'=>'Код',
                        'sortable'=>'code',
                    ),
                    array(
                        'content'=>'Цена',
                        'sortable'=>'price',
                    ),
                    array(
                        'content'=>'Категория',
                        'sortable'=>'category_id',
                    ),
                    'Активация',                    
                    'Функции',
                );
        
        
        $filters=array(
                'button_filter'=>array(
                    'type'=>'button_filter'
                ),
                'name'=>array(
                    'type'=>'text',
                    'value'=>$this->_getRequest()->get_validate('name', 'int')
                ),
                
                'link'=>array(
                    'type'=>'text',
                    'value'=>$this->_getRequest()->get_validate('link', 'text')
                ),
                'code'=>array(
                    'type'=>'text',
                    'value'=>$this->_getRequest()->get_validate('code', 'text')
                ),                
                'price'=>array(
                    'type'=>'text',
                    'value'=>$this->_getRequest()->get_validate('price', 'int')
                ),
                'parent_id'=>array(
                    'content'=>($this->_categorySelectGenerate($categories['rows'], $parent_id, true)),
                    'params'=>array(
                         'eval'=>"$('#categoryParentSelect').ikSelect({autoWidth: true, ddFullWidth: true});"
                            )

                ),
                NULL,
                NULL,
            );
        $this->thead=array(
                        $titles,
                        $filters
                        );
        $this->tbody=$this->_genRows($this->_productsListing['rows']);
       
        $this->tfoot=array(
            array(
                $this->paginator($this->_productsListing['rowCount'], $params)                              
            ),
            array(
                'button_filter'=>array(
                    'type' => array('edit')
                )
            )
        );
        $html=$this->generate();
        
        return $html;
    }
    
    protected function _genRows($rows)
    {
        
        $new_rows=array();
        //print_r($rows);
        foreach($rows as $cur)
        {
            $row=array();
            $row['name']=$cur['name'];
            $row['link']=$cur['link'];
            $row['code']=$cur['code'];
            $row['price']=$cur['price'];
            $row['category_name']=$cur['category_name'];
            $row['active']="<a class='btn-active btn btn-info btn-mini ".(!$cur['active']?'hidden':'')."'
                             onclick='productadmin.toggleActive({$cur['id']}, 0)'>Запретить</button>
                            <a class='btn-active btn btn-success btn-mini ".($cur['active']?'hidden':'')."' 
                             onclick='productadmin.toggleActive({$cur['id']}, 1)'>Разрешить</button>";
            $row['buttons'] = array(
                'id'=>$cur['id'],
                'type'=>array('edit', 'del'),                
            );

            $new_rows[]=array(
                'content'=>$row,
                'params'=>array(
                    'class'=>($cur['active']?'success':'error'),
                    'id'=>'row_'.$cur['id'],
                )
            );
        }
        return $new_rows;
    }
    
    

    protected function _formEditAjaxAction()
    {

        //Достаём всю инфу по продукту и передаём далее по цепочке
        return array(
            'result'=>1,
            'html'=>$this->_formEditAction(),
            'title'=>'Редактор товара',
        );
    }
    

    protected function _drawFormAction(){
        
        $mapper=new Module_Shop_Mapper_Manage_Products();
        if($id=$this->_getRequest()->get_validate('id', 'int', 0)){
            $vars=$mapper->getOne($id);
        }
        $vars['id']=$id;
        //Добавляем селект для выбора родителя
        $cat_mapper=new Module_Shop_Mapper_Manage_Category();
        $categories=$cat_mapper->getCategories(FALSE, true);        
        
        $vars['category_select'] =  $this->_categorySelectGenerate($categories['rows'], ((isset($vars['category_id'])&&$vars['category_id'])?$vars['category_id']:1));  
        $view = new Module_Shop_View_Manage_Product();
        
        return array(
            'result' => 1,
            'html' => $view->drawForm($vars)
        );
    }
    protected function _drawDescFormAction(){
        
        $mapper =new Module_Shop_Mapper_Manage_Products();
        $model  =new Module_Shop_Model_Manage_Products();
        
        $model->product_id=$this->_getRequest()->get_validate('id', 'int', false);
        if(!$model->product_id) 
            return array(
                'result' => 1,
                'html'=> 'Этот раздел будет доступен после сохранения товара'
            );
        $lang_default=Module_Lang_Helper::getCurrent();
        
        $model->lang_id=$this->_getRequest()->get_validate('lang_id', 'int', $lang_default);
        $vars=$mapper->getOneDesc($model);

        //Достаём дескрипшн по конкретному товару
        $tpl=new Module_Shop_View_Manage_Product();
        
        $vars['multilanguage']=  Dante_Lib_Config::get('shop.enableMultilanguage');
        if($vars['shortDescr'] = Dante_Lib_Config::get('shop.allowShortDescription'))
            $vars['shortDescrMax'] = Dante_Lib_Config::get('shop.shortDescriptionMax');
        else $vars['shortDescrMax'] = false;
        
        return array(
            'result'=>1,
            'html'=>$tpl->drawFormDesc($vars)
        );
        
    }
    
    protected function _drawImagesAction()
    {
        $mapper     =new Module_Shop_Mapper_Manage_Products();
        $model      =new Module_Shop_Model_Manage_Products();
        $uploader   =new Module_Uploadify_Controller_Base();
        $tpl        =new Module_Shop_View_Manage_Product();

        $model->product_id=$this->_getRequest()->get_validate('id', 'int', false);
        $images=$mapper->getImages($model);
        $this->tbody=array(
            'fields'=>array()
        );
        $i=1;
        foreach($images as $row){

            $productFolder = Module_Shop_Helper_Shop::getProductsFolder($row['product_id']);
            $originalImgUrl = $productFolder.'original/';
            $thumbImgUrl = $productFolder.'thumb/';
			if ( $i != 1 ) {
				$upbtn = "<a class='btn btn-success' onclick='productadmin.upImg({$row['id']})'>Вверх</a>";
			}else{
				$upbtn = '';
			}
			if ( $i != count($images) ) {
				$downbtn = "<a class='btn btn-success' onclick='productadmin.downImg({$row['id']})'>Вниз</a>";
			}else{
				$downbtn = '';
			}

            $cur=array();
            $cur['content']['num']=$i++;
            $cur['content']['image']="<a class='contest_images_lightbox' href='{$originalImgUrl}{$row['image']}'><img src='{$thumbImgUrl}{$row['image']}' class='admin_img_prev'></a>";
            $cur['content']['img_access']="<a class='btn btn-danger' onclick='productadmin.delImg({$row['id']})'>Удалить</a>".' '.$upbtn.' '.$downbtn;
            $cur['params']=array(
                    'id'=>'img_row_'.$row['id']
                );
            $this->tbody['content'][]=$cur;
        }
        $this->tableParams['id']='imagesTable';
        $this->tableParams['class']='table table-stripes img_prev_admin_table';
        $upload_params=array(
            'swf_url'=>$uploader->swf_url,
            'server_url'=>$uploader->server_url,
            'id'=>$model->product_id,
        );
        $this->tfoot=array(
                'fields'=>array(
                    $tpl->drawUploader($upload_params)
                )

            );
        $html=$this->generate();
        return array(
            'result'=>1,
            'html'=>$html
        );
    }

    
    /**
     * Добавление или редактирование данных
     */
    protected function _editAction(){
        $mapper =new Module_Shop_Mapper_Manage_Products();
        $model  =new Module_Shop_Model_Manage_Products();
        $model->id=$this->_getRequest()->get_validate('id', 'int', false);
        $model->product_id = $model->id;
        $model->name=$this->_getRequest()->get_validate('name', 'text', NULL);
        $model->link=$this->_getRequest()->get_validate('link', 'text', NULL);
        $model->code=$this->_getRequest()->get_validate('code', 'text', NULL);
        $model->price=$this->_getRequest()->get_validate('price', 'text', 0);
        $model->category_id=$this->_getRequest()->get_validate('category_id', 'int', 1);
        
        if ($model->link == '') {
            $model->link = Dante_Lib_Transliteration::transform($model->name);
        }
        
        if($model->id){
            $id=$mapper->edit($model);
        }else{
            $id=$mapper->add($model);           
        }
        return array(
                'result'=>1,
                'id'=>$id,
            );
    }
    protected function _editDescAction(){
        
        $mapper =new Module_Shop_Mapper_Manage_Products();
        $model  =new Module_Shop_Model_Manage_Products();
        $model->product_id=$this->_getRequest()->get_validate('product_id', 'int', false);
        $model->name=$this->_getRequest()->get_validate('name', 'text', false);
        $lang_default=Module_Lang_Helper::getCurrent();
        $model->lang_id=$this->_getRequest()->get_validate('lang_id', 'int', $lang_default);
        $model->description=$this->_getRequest()->get_validate('description', 'text', false);
        $model->shortDescription = $this->_getRequest()->get_validate('short_descr', 'text', false);

        $model->seoTitle = $this->_getRequest()->get_validate('seoTitle', 'text', false);
        $model->seoKeywords = $this->_getRequest()->get_validate('seoKeywords', 'text', false);
        $model->seoDescription = $this->_getRequest()->get_validate('seoDescription', 'text', false);


        $mapper->editDecription($model);
        return array(
                'result'=>1,
                
            );
    }

    protected function _toggleActiveAction(){
        $mapper =new Module_Shop_Mapper_Manage_Products();
        $model  =new Module_Shop_Model_Manage_Products();
        $model->id=$this->_getRequest()->get_validate('id', 'int', false);
        $model->active=$this->_getRequest()->get_validate('active', 'int', 0);
        if($id=$mapper->toggleActive($model)){
            return array(
                'result'=>1,
                'id'=>$id,
            );
        }else{
            return array(
                'result'=>0,
                'error'=>'Неверно указан ID продукта',
            );
        }
        
    }

    /**
     * Удаление продукта
     */
    protected function _delAction(){
        $mapper =new Module_Shop_Mapper_Manage_Products();
        $id=$this->_getRequest()->get_validate('id', 'int', false);
        if(!$id){
            return array(
                'result'=>0,
                'error'=>'Неверно указан ID продукта',
            );
        }
        $mapper->del($id);
        return array(
            'result'=>1,
            'id'=>$id,
        );
    }
    protected function _delImageAction(){
        $mapper =new Module_Shop_Mapper_Manage_Products();
        $id = $this->_getRequest()->get_validate('id', 'int', false);
        
        $imageInfo = $mapper->getImageInfo($id);
        
        if(!$id || !$mapper->delImage($id)){
            return array(
                'result'=>0,
                'error'=>'Операция не удалась',
            );
        }
        
        // удалим файл
        $filePath = Dante_Lib_Config::get('app.uploadFolder').'/'.Dante_Lib_Config::get('shop.productsFolder');
        $filePath .= '/'.$imageInfo['productId'].'/'.$imageInfo['image'];
        if (file_exists($filePath)) unlink($filePath);
        
        return array(
            'result'=>1,
            'id'=>$id,
        );
    }

	protected function _upImageAction(){
        $mapper =new Module_Shop_Mapper_Manage_Products();
        $id = $this->_getRequest()->get_validate('id', 'int', false);
        
        if(!$id || !$mapper->upImage($id)){
            return array(
                'result'=>0,
                'error'=>'Операция не удалась',
            );
        }
        
        return array(
            'result'=>1,
            'id'=>$id,
        );
    }

	protected function _downImageAction(){
        $mapper =new Module_Shop_Mapper_Manage_Products();
        $id = $this->_getRequest()->get_validate('id', 'int', false);
        
        if(!$id || !$mapper->downImage($id)){
            return array(
                'result'=>0,
                'error'=>'Операция не удалась',
            );
        }
        
        return array(
            'result'=>1,
            'id'=>$id,
        );
    }

	protected function _sortImageAction(){
        $mapper =new Module_Shop_Mapper_Manage_Products();
        $id = $this->_getRequest()->get_validate('prod_id', 'int', false);
        
        if(!$id || !$mapper->downImage($id)){
            return array(
                'result'=>0,
                'error'=>'Операция не удалась',
            );
        }
        
        return array(
            'result'=>1,
            'id'=>$id,
        );
    }

    /**
     * Загрузка изображений
     * @return type 
     */
    protected function _addImageAction(){
        Dante_Lib_Log_Factory::getLogger()->debug('_addImageAction');
        if(!isset($_FILES['Filedata'])) {
            Dante_Lib_Log_Factory::getLogger()->debug('no files for upload');
            return;
        }
        
        ini_set("gd.jpeg_ignore_warning", 1);

        if(!$productId=$this->_getRequest()->get_validate('product_id', 'int')){
            Dante_Lib_Log_Factory::getLogger()->debug('wrong product id');
           return ;
        }

        
        $productFolder = Module_Shop_Helper_Shop::getProductsFolder($productId);
        Dante_Lib_Log_Factory::getLogger()->debug("productFolder : $productFolder");
        $uploadDir = $productFolder.'original/';
        
        if (!is_dir($uploadDir)) {
            if (!mkdir($uploadDir, 0755, true)) {
                Dante_Lib_Log_Factory::getLogger()->debug("exception : cant create dir : $uploadDir");
                throw new Exception("cant create dir : $uploadDir");
            }
        }
        
        //Dante_Lib_Log_Factory::getLogger()->debug('$uploadDir = '.$uploadDir);

        $uploader = new Component_Swfupload_Upload();
        $newFileName = $productId.'-'.md5(time().rand(1,100000000));
        Dante_Lib_Log_Factory::getLogger()->debug("upload to dir $uploadDir file $newFileName");
        try {
            list($fileName, $newFileName)=$uploader->upload($uploadDir, $newFileName, TRUE);
        } catch (Exception $e) {
            Dante_Lib_Log_Factory::getLogger()->debug('Caught exception: '.$e->getMessage());

        }

        Dante_Lib_Log_Factory::getLogger()->debug("uploaded filename : $fileName");
        if($fileName){
            $mapper =new Module_Shop_Mapper_Manage_Products();    
            $mapper->addImage($productId, $newFileName);
        }

        // Dorian: Сделаем thumbnail
        $dstDir = $productFolder.'catalog/';
        Lib_Image_Helper::resize($uploadDir, $newFileName, $dstDir, 
                Dante_Lib_Config::get('shop.catalog.imgWidth'), 
                Dante_Lib_Config::get('shop.catalog.imgHeight')
        );
        
        // Сделаем превью для списка картинок
        $dstDir = $productFolder.'thumb/';
        Lib_Image_Helper::resize($uploadDir, $newFileName, $dstDir, 
                Dante_Lib_Config::get('shop.thumb.imgWidth'), 
                Dante_Lib_Config::get('shop.thumb.imgHeight')
        );
        
        // Сделаем превью для страницы детализации
        $dstDir = $productFolder.'preview/';
        Lib_Image_Helper::resize($uploadDir, $newFileName, $dstDir, 
                Dante_Lib_Config::get('shop.preview.imgWidth'), 
                Dante_Lib_Config::get('shop.preview.imgHeight')
        );
        
        // Сделаем картинку для слайдера
        $dstDir = $productFolder.'slider/';
        Lib_Image_Helper::resize($uploadDir, $newFileName, $dstDir, 
                Dante_Lib_Config::get('shop.slider.imgWidth'), 
                Dante_Lib_Config::get('shop.slider.imgHeight')
        );
    }

    protected function _categorySelectGenerate($rows, $parent_id=1, $filter=false){
        
        $service = new Module_Shop_Service_Manage_Category();        
        return $service->comboboxGenerate($rows, $parent_id, 'category_id', FALSE, $filter);
        
    }
}
?>
