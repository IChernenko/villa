<?php

class Module_Shop_Controller_Manage_Bestseller extends Module_Handbooksgenerator_Controller_Base
{
    protected function _drawAction()
    {
        $helper = new Module_Division_Helper();
        $helper->checkAccess();
        
        $mapper = new Module_Shop_Mapper_Bestseller();
        $prodMapper = new Module_Shop_Mapper_Catalog();
        
        $list = $mapper->getAll();         
        $prodList = $prodMapper->getByList($list);
        $images = $prodMapper->getImagesByIds($list);
        
        foreach($list as $key => $val)
        {
            $prodId = $val;
            $newVal = array();
            $newVal['name'] = $prodList[$prodId]->name;
            $newVal['image'] = isset($images[$prodId])?
                    Module_Shop_Helper_Shop::getProductsFolder($prodList[$prodId]->id).'original/'.$images[$prodId]['image']:
                    Lib_Image_Helper::getNoImage();
            $list[$key] = $newVal;
        }
        
        $params = $this->getParams();
        
        $this->formEnable('module.shop.controller.manage.bestseller');
        
        $this->caption='Блок "Хит продаж"';
        $this->_setHeader();
        $this->_setBody($list);
        $this->tfoot=array(
            array($this->paginator(count($list), $params)),
            array(
                'button_filter'=>array(
                    "type"=>array(
                        'edit'
                    ),                    
                )
             ),       
        );
        
        $html=$this->generate();        
        return $html;
        
    }
    
    protected function _setHeader()
    { 
        $title = array(
            array(
                'content' => "#",
                'params' => array(
                    'style' => array(
                        'width'=>'15px'
                    )
                )
            ),
            array(
                'content' =>'Название',
                'params' => array(
                    'style' => array(
                        'width' => '350px'
                    )
                )
            ),
            array(
                'content' =>'Изображение',
                'params' => array(
                    'style' => array(
                        'width' => '200px'
                    )
                )
            ),
            array(
                'content' =>'Функции',
                'params' => array(
                    'style' => array(
                        'width' => '120px'
                    )
                )                
            ),
        );
        
        $this->thead = array(
            $title
        );
    }
    
    protected function _setBody($list)
    {          
        foreach($list as $id => $row){  
            $cur = array();
            $cur['id'] = $id;
            $cur['name'] = $row['name'];
            $cur['image'] = '<img src="'.$row['image'].'" style="max-height: 200px; max-width: 200px;"/>';
            
            $cur['buttons'] = array(
                'id'=>$id,
                'type'=>array('edit', 'del'),                
            );
            $this->tbody[$id] = $cur;
        }
    } 

    protected function _drawFormAction($id = false)
    {        
        if(!$id) $id = $this->_getRequest()->get_validate('id', 'int');
        
        if($id)
        {
            $mapper = new Module_Shop_Mapper_Bestseller();
            $prodMapper = new Module_Shop_Mapper_Catalog();
            $prodId = $mapper->getProductId($id);        
            $product = $prodMapper->getById($prodId);        
        }
        else 
        {
            $id = 0;
            $product = new Module_Shop_Model_Product();
            $product->id = 0;
            $product->images[] = Lib_Image_Helper::getNoImage();
        }
        
        $view = new Module_Shop_View_Manage_Bestseller();
        $html = $view->drawForm($id, $product);
        return array(
            'result' => 1,
            'html' => $html
        );
    }
    
    protected function _drawProductFormAction()
    {
        $prodId = $this->_getRequest()->get_validate('prod_id', 'int', 0);
        
        $catMapper = new Module_Shop_Mapper_Manage_Category();
        $prodMapper = new Module_Shop_Mapper_Catalog();
                
        $categories = $catMapper->getCategories();
        $categId = $this->_getRequest()->get('category_id');
        unset($categories['rows'][1]);
        if(!$categId) 
        {
            $cur = current($categories['rows']);
            $categId = $cur['id'];
        }        
        
        $page = $this->_getRequest()->get('page');
        if(!$page) $page = 1;
        
        $rowInPage = 5;
        
        $offset = $rowInPage*($page-1);
        $limit = 'LIMIT '.$rowInPage.' OFFSET '.$offset;
        $products = $prodMapper->getList($categId, $limit);
        $count = $prodMapper->getCountList($categId);
        
        $products = $prodMapper->getList($categId, null, $limit);
        
        $view = new Module_Shop_View_Manage_Bestseller();
        
        $paginator = new Lib_Paginator_Driver();
        $functionJS = "bestselleradmin.changeContent(false)";
        
        $pgn = $paginator->draw($count, $page, $rowInPage, $functionJS);
        
        $html = $view->drawProductForm($prodId, $categId, $products, $categories['rows']);    
        
        return array(
            'result' => 1,
            'html' => $html.$pgn
        );
    }
    
    protected function _editAction()
    {
        $mapper = new Module_Shop_Mapper_Bestseller();
        
        $row = array(
            'id' => $this->_getRequest()->get('id'),
            'product_id' => $this->_getRequest()->get('product_id')
        );
        $mapper->edit($row);
        
        return array(
            'result' => 1
        );
    }
    
    protected function _delAction()
    {
        $mapper = new Module_Slider_Manage_Mapper();
        $id = $this->_getRequest()->get_validate('id', 'int');
        $mapper->del($id);
        
        return array(
           'result'=>1 
        );
    }
}

?>
