<?php

class Module_Shop_Controller_Bestseller extends Dante_Controller_Base
{
    protected function _defaultAction() 
    {
        $mapper = new Module_Shop_Mapper_Bestseller();
        $prodMapper = new Module_Shop_Mapper_Catalog();
        
        $prodIds = $mapper->getProductList();
        if(!$prodIds) return '';
        $list = $prodMapper->getByList($prodIds);
        $images = $prodMapper->getImagesByIds($prodIds);
        
        $currency = Module_Currency_Helper::getCurrentCurrency();
        foreach($list as &$product)
        {
            $product->price = Module_Currency_Helper::convert($product->price);
        }
        
        $view = new Module_Shop_View_Bestseller();
        $view->currency = $currency['reduction'];
        
        return $view->draw($list, $images);
    }
}

?>
