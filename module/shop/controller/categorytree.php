<?php


/**
 * Контроллер древовидных категорий.
 * Обычно используется для построения dropdown menu.
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Shop_Controller_Categorytree extends Dante_Controller_Base {

    protected function _instance_view() {
        return new Module_Shop_View_Categorytree();
    }
    
    protected function _defaultAction() {
        $mapper = new Module_Shop_Mapper_Category();
        $list = $mapper->getListTree();        
//        var_dump($list);        
        
        $view = $this->_instance_view();
        $html = $view->draw($list, Module_Shop_Helper_Shop::CATEGORY_ROOT);
        
        return $html;
    }
}

?>
