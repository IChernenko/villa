<?php
/**
 * Контроллер категорий
 * User: dorian
 * Date: 18.03.12
 * Time: 13:44
 * To change this template use File | Settings | File Templates.
 */
class Module_Shop_Controller_Categorypreview extends Dante_Controller_Base {

    protected function _defaultAction() {
        $mapper = new Module_Shop_Mapper_Category();
        $list = $mapper->getList();

        $view = new Module_Shop_View_Category();
        return $view->drawPreview($list);
    }
}
