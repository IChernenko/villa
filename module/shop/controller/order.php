<?php
/**
 * Контроллер заказа
 * User: dorian
 * Date: 04.04.12
 * Time: 15:20
 * To change this template use File | Settings | File Templates.
 */
class Module_Shop_Controller_Order extends Dante_Controller_Base {

    protected function _defaultAction() {

        $mapper = new Module_Shop_Mapper_Cart();
        $cart = $mapper->get();

        $view = new Module_Shop_View_Order();
        return $view->draw($cart);
    }

    /**
     * Провести заказ
     */
    protected function _orderAction() {

        // todo: регистрация пользователя
        $userMapper = new Module_User_Mapper();
        $user = new Module_User_Model();
        $user->email = $this->_getRequest()->get('email');
        $user->password = '123';
        $userMapper->add($user);

        // todo: регистрация покупателя
        // todo: записать заказ

        $order = new Module_Shop_Model_Order();
        $order->fio = $this->_getRequest()->get('firstName').' '.$this->_getRequest()->get('lastName').' '.$this->_getRequest()->get('middleName');
        $order->address = $this->_getRequest()->get('address');
        $order->email = $this->_getRequest()->get('email');
        $order->address = $this->_getRequest()->get('address');
        $order->phone = $this->_getRequest()->get('phone');
        $order->comment = $this->_getRequest()->get('comment');
        $order->country = 1;
        $order->city = 1;
        $orderMapper = new Module_Shop_Mapper_Order();
        $orderMapper->add($order);
        // todo: записать состав заказа
        $orderMapper->addDetail($order->id);

        // todo: очистить корзину
        $cartMapper = new Module_Shop_Mapper_Cart();
        $cartMapper->clear();

        return array(
            'result' => 1,
            'message' => 'Ваш заказ успешно добавлен'
        );
    }
}
