<?php



/**
 * Description of slider
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Shop_Controller_Slider extends Dante_Controller_Base{
    
    protected function _defaultAction() {
        $mapper = new Module_Shop_Mapper_Catalog();
        $list = $mapper->getList();
                
        $view = new Module_Shop_View_Slider();
        return $view->draw($list);
    }
}

?>
