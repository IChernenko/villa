<?php

class Module_Shop_Service_Manage_Category 
{
    public $category = false;
    public function comboboxGenerate($rows, $selected = false, $name = 'select', $endText = true, $filter = false)
    {
        $combobox=new Component_Vcl_Combobox;
        $combobox->addAttributeArray(array(
            'id' => 'categoryParentSelect',
            'name'=> $name,
        ));
        if($filter){
            $combobox->addItem('', 'Без фильтра');
        }
        if(!$endText){
            $combobox->addItem(1, 'Корневой каталог');
        }
        if($selected){
            $combobox->selected_value=  $selected;
        }

        //Генерируем массив
        $this->_positionGenerate($combobox, $rows , $endText);
        //генерируем html
        
        return $combobox->draw();
    }
    
    protected function _positionGenerate($combobox, $rows, $end_text, $parent_id=1, $cascade_num=1, $disabled=false)
    {        
        $i=0;
        $childs=false;
        foreach($rows as $cur){
            if($cur['id']==1){
                //пропускаем корневую позицию
                continue;
            }
            
            if($parent_id==$cur['parent_id']){
                $i++;
                $row=array();
                //Добавляем запись в options чтоб занять очередь в выводе
                $combobox->addItem($cur['id'], '');
                //Проверим  не является выбранная позиция уже выбранной
                if($this->category  && $this->category->id==$cur['id']){
                    //Да , есть. Дизайблим дочерние записи во избежание рекурсии
                    $disabled_new=TRUE;
                }else{
                    //Оставляем всё как есть
                    $disabled_new=$disabled;
                }
                //Проработаем дочерние элементы
                if($childs=$this->_positionGenerate($combobox, $rows, $end_text, $cur['id'], $cascade_num+1, $disabled_new)){
                    //Есть дочерние элементы 
                    //если это не параграф добавим класс italic
                    if($parent_id>1){
                        $row['class'][]='italic';
                    }
                }
                //Обновим запись по текущей строке с новыми параметрами
                if($cur['parent_id']==1){
                    //Если парент корневой то делаем жирным
                    $row['class'][]='bold';
                }
                if($disabled){
                    //Дизейблим запись
                    $row['disabled']='disabled="disabled"';
                }
                $row['class'][]="padding_".$cascade_num;
                
                $row['text']=$cur['name'];
                $combobox->addItem($cur['id'], $row);
            }
            
        }
        
        if(($i>0 || $cascade_num==1) && $end_text){
            $row=array(
                'text'=>'В каталоге  - '.((isset($rows[$parent_id]))?$rows[$parent_id]['name']:'Корень'),
                'class'=>array(
                    'grey',
                    'padding_'.($cascade_num),
                    'small',
                    'italic'
                ),
                'value'=>$parent_id,
            );
            if($this->category  && $this->category->id==$parent_id){
               $row['disabled']='disabled="disabled"';
            }
            $combobox->addItem($parent_id.'_'.$i, $row);
            return TRUE;
        }else{
            return FALSE;
        }
        
    }
}

?>