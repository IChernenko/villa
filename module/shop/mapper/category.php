<?php
/**
 * Маппер категорий
 * User: dorian
 * Date: 18.03.12
 * Time: 16:47
 * To change this template use File | Settings | File Templates.
 */
class Module_Shop_Mapper_Category
{
    /**
     * Формирует запрос на выборку списка категорий
     * @return string
     */
    protected function _getListSql($root = false, $all = false) {
        if(!$root) $root = Module_Shop_Helper_Shop::CATEGORY_ROOT;
        $sql = "select
                    c.id,
                    c.name, 
                    c.link, 
                    c.image,
                    c.parent_id
                from [%%]shop_categories as c 
                LEFT JOIN [%%]shop_categories_seo as s on ( s.category_id = c.id)";
                
        if(!$all)
            $sql .= " WHERE `parent_id` = {$root} group by id ";
            
        $sql .= " ORDER BY `draw_order` ASC";
        //echo($sql);
        return $sql;
    }
    
    /**
     * Заполнение модели на основании массива fetch
     * @param array $f
     * @return Module_Shop_Model_Category 
     */
    protected function _populateModel($f) {
        $model = new Module_Shop_Model_Category();
        $model->id      = (int)$f['id'];
        $model->name    = $f['name'];
        $model->url     = $f['link'];
        $model->image   = $f['image'];
        $model->parent  = (int)$f['parent_id'];
        return $model;
    }
    
    /**
     * Возвращает список категорий
     * @return array
     */
    public function getList($root = false) {
        $r = Dante_Lib_SQL_DB::get_instance()->open($this->_getListSql($root));
        if (!$r) return false;//throw new Exception("не могу получить список категорий");
        $list = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $list[] = $this->_populateModel($f);
        }
        return $list;
    }
    
    /**
     * Возвращает список категорий
     * @return array
     */
    public function getListTree() {
        $r = Dante_Lib_SQL_DB::get_instance()->open($this->_getListSql(false, true));
        if (!$r) throw new Exception("не могу получить список категорий");
        $list = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $model = $this->_populateModel($f);
            $list[$model->parent][$model->id] = $model;
        }
        return $list;
    }

    public function getAllList() {
        $r = Dante_Lib_SQL_DB::get_instance()->open($this->_getListSql(false, true));
        if (!$r) throw new Exception("не могу получить список категорий");
        $list = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $model = $this->_populateModel($f);
            $list[$model->id] = $model;
        }
        return $list;
    }

    /**
     * @param $id
     * @return bool|Module_Shop_Model_Category
     */
    public function get($id) {
        $langId = 1;

        $sql = "select
            c.id,
            c.name,
            c.link,
            c.image,
            cs.title,
            cs.keywords,
            cs.description
        from [%%]shop_categories as c
        LEFT JOIN [%%]shop_categories_seo as cs on ( cs.category_id = c.id and cs.lang_id = {$langId} )
        WHERE c.id = {$id}";
        //echo($sql);
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);


        $f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r);
        if (!$f) return false;

        $model = new Module_Shop_Model_Category();
        $model->id          = (int)$f['id'];
        $model->name        = $f['name'];
        $model->url         = $f['link'];
        $model->image       = $f['image'];
        $model->title       = $f['title'];
        $model->keywords    = $f['keywords'];
        $model->description = $f['description'];

        return $model;
    }

    /**
     * Получение идентификатора категории по ее ссылке
     * @param string $link
     * @return mixed
     */
    public function getIdByLink($link) {
        $table = new Dante_Lib_Orm_Table('[%%]shop_categories');
        $table->select(array('link' => $link));

        if ($table->getNumRows() == 0) return false;
        return $table->id;
    }
    
    /**
     * Добавление категории
     * @param Module_Shop_Model_Category $category 
     */
    public function add(Module_Shop_Model_Category $category) {
        if ($category->lang == 0) {
            $category->lang = Module_Lang_Helper::getCurrent();
        }
        
        $dmo = new Module_Shop_Dmo_Category();
        $dmo->name          = $category->name;
        $dmo->parent_id     = $category->parent;
        $dmo->link          = $category->url;
        $dmo->image         = $category->image;
        $dmo->draw_order    = $category->draw_order;
        $category->id = (int)$dmo->insert();
        if ($category->id == 0)
            throw new Exception("cant insert category");
        
        $dmoSeo = new Module_Shop_Dmo_Categoryseo();
        $dmoSeo->category_id    = $category->id;
        $dmoSeo->lang_id        = $category->lang;
        $dmoSeo->name           = $category->name;
        $dmoSeo->title          = $category->title;
        $dmoSeo->keywords       = $category->keywords;
        $dmoSeo->description    = $category->description;
        $dmoSeo->insert();
    }  
}
