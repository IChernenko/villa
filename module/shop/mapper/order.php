<?php
/**
 * Маппер заказа
 * User: dorian
 * Date: 05.04.12
 * Time: 16:25
 * To change this template use File | Settings | File Templates.
 */
class Module_Shop_Mapper_Order
{
    protected $_tableName = '[%%]shop_order';

    /**
     * @param Module_Shop_Model_Order $order
     */
    public function add(Module_Shop_Model_Order $order) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->fio = $order->fio;
        $table->country = $order->country;
        $table->city = $order->city;
        $table->address = $order->address;
        $table->phone = $order->phone;
        $table->email = $order->email;
        $table->comment = $order->comment;
        $order->id = $table->insert();
    }

    public function addDetail($orderId) {

        $sid = Dante_Helper_App::getSid();

        $sql = "insert into [%%]shop_order_detail (order_id, product_id, quantity, price)
        select $orderId, c.product_id, c.quantity, p.price from [%%]shop_cart as c
        left join [%%]shop_products as p on (p.id = c.product_id)
        where c.sid='{$sid}'";

        return Dante_Lib_SQL_DB::get_instance()->exec($sql);
    }
}
