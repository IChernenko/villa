<?php

class Module_Shop_Mapper_Bestseller
{
    protected $_tableName = '[%%]shop_bestseller';
    
    public function getAll()
    {
        $sql = "SELECT * FROM {$this->_tableName}";
        if(!$r = Dante_Lib_SQL_DB::get_instance()->open($sql)) 
            return false;
        
        $rows = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
            $rows[$f['id']] = $f['product_id'];
        
        return $rows;
    }    
    
    public function getProductId($rowId)
    {
        $sql = "SELECT * FROM {$this->_tableName} WHERE `id` = {$rowId}";
        return Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'product_id');
    }
    
    public function edit($row)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->id = $row['id'];
        $table->product_id = $row['product_id'];
        $table->apply(array('id'=>$row['id']));
    }
    
    public function getProductList()
    {
        $sql = "SELECT `product_id` FROM {$this->_tableName}";
        if(!$r = Dante_Lib_SQL_DB::get_instance()->open($sql)) 
            return false;
        
        $productIds = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
            $productIds[] = $f['product_id'];
        
        return $productIds;
    }    
}

?>
