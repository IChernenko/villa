<?php
/**
 * Маппер для корзины
 * User: dorian
 * Date: 03.04.12
 * Time: 17:00
 * To change this template use File | Settings | File Templates.
 */
class Module_Shop_Mapper_Cart
{
    protected $_tableName = '[%%]shop_cart';

    /**
     * Добавление продукта в корзину
     * @param int $productId
     * @param int $quantity
     */
    public function add($productId, $productType, $quantity = 1) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $sid = Dante_Helper_App::getSid();
        $cond = array(
            'sid' => $sid,
            'product_id' => $productId,
            'product_type' => $productType
        );
        if($table->exist($cond)) 
        {
            $quantity += $table->select($cond)->quantity;
            $insert = false;
        }
        else $insert = true;
        
        $table->sid = $sid;
        $table->product_id = $productId;
        $table->product_type = $productType;
        $table->quantity = $quantity;
        $table->apply($cond);
        return $insert;

    }

    public function remove($productId, $productType) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->delete(array(
            'sid' => Dante_Helper_App::getSid(),
            'product_id' => $productId,
            'product_type' => $productType
        ));
    }

    public function update($productId, $productType, $quantity) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->quantity = $quantity;
        $table->update(array(
            'sid' => Dante_Helper_App::getSid(),
            'product_id' => $productId,
            'product_type' => $productType
        ));
    }

    /**
     * Очистить корзину
     */
    public function clear() {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->delete(array('sid' => Dante_Helper_App::getSid()));
    }

    /**
     * Получить список товаров в корзине
     *
     * @return Module_Shop_Model_Cart
     */
    public function get() {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->get()->where(array('sid'=>Dante_Helper_App::getSid()));
        $table->open();
        //echo((string)$table);
        $list = array();
        $productIds = array();
        while($table->fetchRecord()) {
            $list[] = array(
                'productId' => (int)$table->product_id,
                'productType' => (int)$table->product_type,
                'quantity' => (int)$table->quantity
                //'code' => $table->code
            );
            $productIds[] = (int)$table->product_id;
        }

        // возьмем информацию по заданным продуктам
        $productMapper = new Module_Shop_Mapper_Catalog();
        $products = $productMapper->getByList($productIds);

        // получить картинки продуктов
        $images = $productMapper->getImagesByIds($productIds);
        
        // теперь собираем все вместе
        $cart = new Module_Shop_Model_Cart();
        foreach($list as $productInfo) {
            $productId = $productInfo['productId'];
            $productInfo['productType'] = $productMapper->getTypeById($productInfo['productType']);
            $productInfo['code'] = $products[$productId]->code;
            $productInfo['price'] = $products[$productId]->price+$productInfo['productType']['diff_price'];
            $productInfo['name'] = $products[$productId]->name;

            if (isset($images[$productId]))
                $productInfo['image'] = $images[$productId]['image'];

            $cart->add($productInfo);
        }
        $cart->recalc();

        return $cart;
    }
}
