<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 12.04.12
 * Time: 19:00
 * To change this template use File | Settings | File Templates.
 */
class Module_Shop_Mapper_Lastvisited
{
    /**
     * Добавление продукта как просмотренного
     * @param int $productId
     */
    public function add($productId) {
        $table = new Dante_Lib_Orm_Table('[%%]shop_last_visited');
        $table->product_id = $productId;
        $table->sid = Dante_Helper_App::getSid();
        $table->view_date = time();
        $table->insert();
    }

    public function getList() {
        $sid = Dante_Helper_App::getSid();
        $sql = "select
                    p.id,
                    p.code
                from [%%]shop_last_visited as l
                left join [%%]shop_products as p on (p.id = l.product_id)
                where l.sid='{$sid}'";

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);

        $list = array();
        $products = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $productId = (int)$f['id'];
            $products[] = $productId;
            $list[$productId] = array(
                'id' => $productId,
                'code' => $f['code']
            );
        }

        // загружаем картинки
        $mapper = new Module_Shop_Mapper_Catalog();
        $images = $mapper->getImagesByIds($products);
        foreach($images as $productId => $image) {
            $list[$productId]['image'] = $image['image'];
        }

        return $list;
    }
}
