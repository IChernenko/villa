<?php
/**
 * Маппер для управления категориями
 *
 * @author Mort
 */
class Module_Shop_Mapper_Manage_Categoryold {
   
    protected $_tableName = '[%%]shop_categories';
    
    protected $_seoTableName = '[%%]shop_categories_seo';
    
    public function getCategories($params) {
        $jgridMapper = new Lib_Jgrid_Mapper_Base();
        $rowsArray = $jgridMapper->getRowsByParams($this->_tableName, $params);
        $categoriesPrep = $rowsArray['rows'];
        $rowCount = $rowsArray['rowCount'];
        
        $sql = "select
                    *
                from ".$this->_seoTableName."
                where lang_id =".$params['language'];

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            if(isset($categoriesPrep[$f['category_id']])){
                $categoriesSeoPrep[$f['category_id']] = $f;
            }
        }
        
        $parents = $this->getParents();
        $categories = new Lib_Jgrid_Model_Base();
        $i=0;
        foreach ($categoriesPrep as $k=>$f){
            if(!isset($categoriesSeoPrep[$f['id']])){
                $f['seoName'] = '';
                $f['seoTitle'] = '';
                $f['seoKeywords'] = '';
                $f['seoDescription'] = '';
            }else{
                $f['seoName'] = $categoriesSeoPrep[$f['id']]['name'];
                $f['seoTitle'] = $categoriesSeoPrep[$f['id']]['title'];
                $f['seoKeywords'] = $categoriesSeoPrep[$f['id']]['keywords'];
                $f['seoDescription'] = $categoriesSeoPrep[$f['id']]['description'];
            }
            
            $categories->rows[$i]['id'] = $f['id'];
            $categories->rows[$i]['cell'] = array(
                                                $f['id'],
                                                $f['name'],
                                                $parents[$f['parent_id']]['name'],
                                                $f['link'],
                                                $f['image'],
                                                $f['draw_order'],
                                                $f['seoName'],
                                                $f['seoTitle'],
                                                $f['seoKeywords'],
                                                $f['seoDescription']
                                            );
            $i++;
        }
        
        $categories->records = $rowCount;
        
        return $categories;
    }
    
    public function getParents() {
        $sql = "select
                    id, name, parent_id
                from ".$this->_tableName."
                order by id";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);

        $parents = array(0=>array('id'=>0, 'name'=>'нет','parent_id'=>0));
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $parents[$f['id']] = $f;
        }
        
        return $parents;
    }
    
    public function del($id) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->delete(array(
            'id' => $id
        ));
        
        $table = new Dante_Lib_Orm_Table($this->_seoTableName);
        $table->delete(array(
            'category_id' => $id
        ));
    }
    
    public function add(Module_Shop_Model_Manage_Category $params, Module_Shop_Model_Manage_Seocategory $paramsSeo) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->name = $params->name;
        $table->parent_id = $params->parent_id;
        $table->link = $params->link;
        $table->image = $params->image;
        $table->draw_order = $params->draw_order;
        
        $paramsSeo->category_id = $table->insert();
        
        
        $this->addSeo($paramsSeo);
    }
    
    public function addSeo(Module_Shop_Model_Manage_Seocategory $paramsSeo) {
        $table = new Dante_Lib_Orm_Table($this->_seoTableName);
        $table->category_id = $paramsSeo->category_id;
        $table->lang_id = $paramsSeo->lang_id;
        $table->name = $paramsSeo->name;
        $table->title = $paramsSeo->title;
        $table->keywords = $paramsSeo->keywords;
        $table->description = $paramsSeo->description;
        
        $table->insert();
    }
    
    public function update(Module_Shop_Model_Manage_Category $params, Module_Shop_Model_Manage_Seocategory $paramsSeo) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->id = $params->id;
        $table->name = $params->name;
        $table->parent_id = $params->parent_id;
        $table->link = $params->link;
        $table->image = $params->image;
        $table->draw_order = (int)$params->draw_order;
        
        $table->apply(array(
            'id' => $params->id
        ));
        
        //а есть ли запись сео для данной штуки ? 
        $tableSeo = new Dante_Lib_Orm_Table($this->_seoTableName);
        if($tableSeo->exist(array(
            'category_id' => $paramsSeo->category_id,
            'lang_id' => $paramsSeo->lang_id
        ))){
            $tableSeo->category_id = $paramsSeo->category_id;
            $tableSeo->lang_id = $paramsSeo->lang_id;
            $tableSeo->name = $paramsSeo->name;
            $tableSeo->title = $paramsSeo->title;
            $tableSeo->keywords = $paramsSeo->keywords;
            $tableSeo->description = $paramsSeo->description;

            $tableSeo->apply(array(
                'category_id' => $paramsSeo->category_id,
                'lang_id' => $paramsSeo->lang_id
            ));
        }else{
            //создаем новую
            $this->addSeo($paramsSeo);
        }
    }
}

?>