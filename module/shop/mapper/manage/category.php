<?php
/**
 * MAPPER CATEGORY
 * @author Elrafir
 */

class Module_Shop_Mapper_Manage_Category extends Module_Handbooksgenerator_Mapper_Base {
    public $_tableName = '[%%]shop_categories';
        
    public function getCategory($id)
    {                
        $model = new Module_Shop_Model_Category();
        $sql = "select * from {$this->_tableName} where `id`='{$id}' limit 1";
        $f = Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
        $model->id = $id;
        $model->name = $f['name'];
        $model->url = $f['link'];
        $model->parent = $f['parent_id'];
        $model->image = $f['image'];
        $model->draw_order = $f['draw_order'];
        
        return $model;
    }

    /**
     * @param $cond
     * @return bool|Module_Shop_Model_Category
     */
    public function getCategorySeo($cond) {
        $dmoSeo = new Module_Shop_Dmo_Categoryseo();
        $dmoSeo->loadBy($cond);
        if ($dmoSeo->getNumRows() == 0) return false;

        $category = new Module_Shop_Model_Category();
        $category->id = (int)$dmoSeo->category_id;
        $category->lang = (int)$dmoSeo->lang_id;
        $category->title = $dmoSeo->title;
        $category->keywords = $dmoSeo->keywords;
        $category->description = $dmoSeo->description;
        return $category;
    }

    public function getCategories($params=false, $allRows = false){
        if($allRows){
            $params['rows_in_page']=1000000;
        }
        $join=array(
            "left join ".$this->_tableName." as t2 on t2.id=t1.parent_id"
            
        );
         $cols=array(
            "t1.*",
            "t2.name as parent_name",
        );
        
        return $this->getRowsByParams($params, $cols, $join);
    }  
    
    public function editCategory(Module_Shop_Model_Category $category)
    {        
//        $this->_nextStepPosition($category->draw_order, $category->parent, $this->_tableName, $category->id);  
        
        $dmo = new Module_Shop_Dmo_Category();
        $dmo->name          = $category->name;
        $dmo->parent_id     = $category->parent;
        $dmo->link          = $category->url;
        $dmo->image         = $category->image;
        $dmo->draw_order    = $category->draw_order;
        if($category->id)
            $dmo->update(array('id'=>$category->id));
        else 
            $category->id = $dmo->insert();
        
        if ($category->id == 0)
            throw new Exception("cant insert category");

        $this->updateSeo($category);

        
//        $this->sortablePosition($category->parent, $this->_tableName);
    }

    public function updateSeo(Module_Shop_Model_Category $category) {

        if (!$category->lang) {
            $category->lang = Module_Lang_Helper::getCurrent();
        }

        $cond = array(
            'category_id'=>$category->id,
            'lang_id' => $category->lang
        );

        $dmoSeo = new Module_Shop_Dmo_Categoryseo();
        $dmoSeo->category_id    = $category->id;
        $dmoSeo->lang_id        = $category->lang;
        $dmoSeo->name           = $category->name;
        $dmoSeo->title          = $category->title;
        $dmoSeo->keywords       = $category->keywords;
        $dmoSeo->description    = $category->description;
        if ($dmoSeo->exist($cond)) {
            $dmoSeo->update($cond);
        }
        $dmoSeo->insert();

    }
    
    public function getCurrentImage($id)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        return $table->select(array('id'=>$id))->image;
    }
    
    
    protected function _nextStepPosition($position, $parrent , $table, $update_id=false){
       
        $where="where `id`>0 and `Category_parrent`='{$parrent}'"; 
        if($update_id){           
            //Достаём данные редактируемого этапа
            $Category=  $this->getCategory($update_id);
            //перемещаем из ряда редактируемую строку в нейтральное положение
            $sql="update from {$table} set `draw_order`=99999 where `id`='{$update_id}'";
            Dante_Lib_SQL_DB::get_instance()->open($sql);
            //смещаем всё вниз
            $sql="update {$table} set `draw_order`=`draw_order`-1 {$where} and `draw_order`>'{$Category['draw_order']}'";
            Dante_Lib_SQL_DB::get_instance()->open($sql);
        }
        
        $sql="update {$table} set `draw_order`=`draw_order`+1 {$where} and `draw_order`>='{$position}'";
        Dante_Lib_SQL_DB::get_instance()->open($sql);
        //Если апдейт
    }
    public function sortablePosition($parrent, $table){
        //Достанем этапы с паррентом
        
        $sql="select * from {$table} where `parent_id`='{$parrent}' and `id`>1 order by `draw_order` asc";
        
        $r=Dante_Lib_SQL_DB::get_instance()->open($sql);
        
        for ($i=1;$cur=Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r);$i++){
            //апдейтим поле
            $sql="update {$table} set `draw_order`='{$i}' where `id`='{$cur['id']}'";
            Dante_Lib_SQL_DB::get_instance()->open($sql);
        }
        //Пересортируем общую позицию в списке
        $this->_sortableTurns($table);
        //Переназвачим флаг принадлености к паррентам
        $this->_getCategoryIsParrents($table);
        
    }
    
    protected function _getCategoryIsParrents($table){
        
        //Меняем всем флаг на 0
        $sql="update `{$table}` set `Category_is_parrent`='0' where 1";
        Dante_Lib_SQL_DB::get_instance()->open($sql);
        //Тянем записи являющиеся паррентом
        $sql="select `parent_id` from {$table} where 1 group by `parent_id`";
        $r=Dante_Lib_SQL_DB::get_instance()->open($sql);
        $parrents=array();
        while ($cur=Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)){
            $parrents[]=$cur['parent_id'];
        }
        $parrentstr=  join(', ', $parrents);
         //Меняем флаги на 1
        $sql="update `{$table}` set `Category_is_parrent`='1' where `id` in ({$parrentstr})";
        //echo $sql;
        Dante_Lib_SQL_DB::get_instance()->open($sql);
       
    }

    protected $_turn_num=2;
    
    protected function _sortableTurns($table=FALSE, $parrent=1){
        
        //Достанем данные по паренту
        $sql="SELECT * FROM {$table} WHERE `parent_id`='{$parrent}' AND `id`>'1' ORDER BY `draw_order` asc";
        $r=Dante_Lib_SQL_DB::get_instance()->open($sql);
        //прокручиваем каждую позицию
        for ($this->_turn_num;$cur=Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r);''){
            //апдейтим поле
            $sql="update {$table} set `turn`='{$this->_turn_num}' where `id`='{$cur['id']}'";
            Dante_Lib_SQL_DB::get_instance()->open($sql);
            $this->_turn_num++;
            //работаем с дочерними строками
            $this->_sortableTurns($table, $cur['id']);
        }
        return;
    }
}
?>
