<?php

class Module_Shop_Mapper_Manage_Products extends Module_Handbooksgenerator_Mapper_Base {
    
    protected $_tableName           = '[%%]shop_products';
    protected $_tableCategories     = '[%%]shop_categories';
    protected $_tableImages         = '[%%]shop_product_images';
    protected $_tableDescription    = '[%%]shop_product_description';


    public function getAll($params){
   
        $join=array(
            "left join ".$this->_tableCategories." as t2 on t2.id=t1.category_id"
            
        );
         $colls=array(
            "t1.*",
            "t2.name as category_name",
        );
        
        $rowsArray=$this->getRowsByParams($params, $colls, $join);
        return $rowsArray;
    }
    
    public function getOne($id=FALSE){
        if(!$id)    return false;
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->select(array('id' => $id));
        return($table->getFields());
    }
    public function getOneDesc(Module_Shop_Model_Manage_Products $product){
        $table = new Dante_Lib_Orm_Table($this->_tableDescription);
        $table->select(array('product_id' =>$product->product_id, 'lang_id'=>$product->lang_id));
        return($table->getFields());
    }
    public function getImages(Module_Shop_Model_Manage_Products $images){
        $table = new Dante_Lib_Orm_Table($this->_tableImages);
        $table->select_array(array('product_id' =>$images->product_id), ' `order` DESC ');
        return ($table->getFields());
    }

    public function add(Module_Shop_Model_Manage_Products $product) {
        if ($product->lang_id == 0) {
            $product->lang_id = Module_Lang_Helper::getCurrent();
        }
        $dmo = new Dante_Lib_Orm_Table($this->_tableName);
        $dmo->name          = $product->name;
        $dmo->code          = $product->code;
        $dmo->price         = $product->price;
        $dmo->link          = $product->link;
        $dmo->order         = $product->order;
        $dmo->active        = 0;
        $dmo->category_id   = $product->category_id;
        $product->id        = (int)$dmo->insert();
       
        if ($product->id == 0)
            throw new Exception("cant insert category");
        //Добавим запись в дескрипшн
        $dmoDesc = new Dante_Lib_Orm_Table($this->_tableDescription);
        $dmoDesc->product_id   = $product->id;
        $dmoDesc->name          = $product->name;
        $dmoDesc->lang_id       = $product->lang_id;
        $dmoDesc->description   =NULL;
        $dmoDesc->insert();
        
        return $product->id;
    }
    public function edit(Module_Shop_Model_Manage_Products $product){
        $dmo = new Dante_Lib_Orm_Table($this->_tableName);
        $dmo->name          = $product->name;
        $dmo->code          = $product->code;
        $dmo->price         = $product->price;
        $dmo->link          = $product->link;
        $dmo->order         = $product->order;
        $dmo->category_id   = $product->category_id;
        $dmo->update(array('id'=>$product->id));
        
        
        return $this->editDecription($product, false);
        
    }
    public function del($id){
        $dmo = new Dante_Lib_Orm_Table($this->_tableName);
        $dmo->delete(array('id'=>$id));
    }
    public function toggleActive(Module_Shop_Model_Manage_Products $product){
        $dmo = new Dante_Lib_Orm_Table($this->_tableName);
        $dmo->active=$product->active;
        if($dmo->update(array('id'=>$product->id))){
            return $product->id;
        }
        return FALSE;
    }

    public function editDecription(Module_Shop_Model_Manage_Products $product, $editDesc = true){
        if ($product->lang_id == 0) {
            $product->lang_id = Module_Lang_Helper::getCurrent();
        }
        $dmoDesc = new Dante_Lib_Orm_Table($this->_tableDescription);
        if($product->name!==FALSE)
            $dmoDesc->name      = $product->name;
        if($editDesc)
        {
            $dmoDesc->description   = $product->description;
            $dmoDesc->short_descr   = $product->shortDescription;
            $dmoDesc->seo_title = $product->seoTitle;
            $dmoDesc->seo_keywords = $product->seoKeywords;
            $dmoDesc->seo_description = $product->seoDescription;
        }
        $dmoDesc->update(array('product_id' =>$product->product_id, 'lang_id'=>$product->lang_id));
       
        return true;
    }

    public function addImage($product_id, $fileName){
        $sql="insert into {$this->_tableImages} SET `product_id`='{$product_id}' , `image`='{$fileName}'";
        Dante_Lib_SQL_DB::get_instance()->open($sql);
    }
   
    public function delImage($id){
        $dmo = new Dante_Lib_Orm_Table($this->_tableImages);
        return $dmo->delete(array('id'=>$id));        
    }

	public function upImage($id)
	{
        $dmo = new Dante_Lib_Orm_Table($this->_tableImages);
        $dmo->select(array('id' => $id));
		$res = $dmo->getFields();

        $table = new Dante_Lib_Orm_Table($this->_tableImages);
        $table->select_array(array('product_id' =>$res['product_id']), ' `order` ');
        $all = $table->getFields();

		$n = 0;
		for ( $i = 0; $i < count($all); $i++ ) {
			$order = $i;
			if($n == 1){
				$n = 0;
				$order = $i - 1;
			}elseif( $all[$i]['id'] == $id) {
				$n = 1;
				$order = $i + 1;
			}
			$sql="update {$this->_tableImages} SET `order` =  {$order} where `id`={$all[$i]['id']}";
			Dante_Lib_SQL_DB::get_instance()->open($sql);
		}
		return true;
	}

	public function downImage($id)
	{
        $dmo = new Dante_Lib_Orm_Table($this->_tableImages);
        $dmo->select(array('id' => $id));
		$res = $dmo->getFields();

        $table = new Dante_Lib_Orm_Table($this->_tableImages);
        $table->select_array(array('product_id' =>$res['product_id']), ' `order` ');
        $all = $table->getFields();
		for ( $i = 0; $i < count($all); $i++ ) {
			$order = $i;
			$c = $i - 1;
			if( $all[$i]['id'] == $id) {
				$order = $i - 1;
				if ( isset($all[$c]) ) {
					$order1 = $i;
					$sql="update {$this->_tableImages} SET `order` =  {$order1} where `id`={$all[$c]['id']}";
					Dante_Lib_SQL_DB::get_instance()->open($sql);
				}
			}
			$sql="update {$this->_tableImages} SET `order` =  {$order} where `id`={$all[$i]['id']}";
			Dante_Lib_SQL_DB::get_instance()->open($sql);
		}
		return true;
	}
    
    /**
     * Определение информации о изображении
     * @param int $id
     * @return array(
     *      'productId' => int
     *      'image'     => string
     *  )
     */
    public function getImageInfo($id) {
        $dmo = new Dante_Lib_Orm_Table($this->_tableImages);
        $dmo->loadBy(array('id'=>$id))->fetch();
        
        return array(
            'productId' => $dmo->product_id, 
            'image'     => $dmo->image
        );
    }
    
    
}
?>
