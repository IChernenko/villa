<?php
/**
 * Маппер для отображения каталога
 * User: dorian
 * Date: 18.03.12
 * Time: 16:47
 * To change this template use File | Settings | File Templates.
 */
class Module_Shop_Mapper_Catalog
{
    protected $_tableName = '[%%]shop_products';
    protected $_tableDescr = '[%%]shop_product_description';
    protected $_tableCategory = '[%%]shop_categories';


    /**
     * Добавление продукта
     * @param Module_Shop_Model_Product $product
     */
    public function add(Module_Shop_Model_Product $product) {
        $table = new Dante_Lib_Orm_Table('[%%]shop_products');
        $table->code = $product->code;
        $table->price = (float)$product->price;
        $table->category_id = $product->category;
        $product->id = (int)$table->insert();

        // add description
        $table = new Dante_Lib_Orm_Table('[%%]shop_product_description');
        $table->name = $product->name;
        $table->product_id = $product->id;
        $table->description = $product->description;
        $table->lang_id = (int)$product->lang;
        $table->insert();

        // add images
        foreach($product->images as $image) {
            $this->_addImage($product->id, $image);
        }
    }
    
    
    /**
     * Изменить запись в таблице продукции
     * @param Module_Shop_Model_Product $product 
     */
    public function update(Module_Shop_Model_Product $product) {
        $table = new Dante_Lib_Orm_Table('[%%]shop_products');
        $table->id              = (int)$product->id;
        $table->code            = $product->code;
        $table->price           = (float)$product->price;
        $table->category_id     = (int)$product->category;
        $table->apply();
        
        $table = new Dante_Lib_Orm_Table('[%%]shop_product_description');
        $table->name        = $product->name;
        $table->description = $product->description;
        $table->lang_id     = (int)$product->lang;
        $table->update(array('id' => $product->id));
    }
    
    /**
     * Удаление продукта
     * @param Module_Shop_Model_Product $product модель продукта
     */
    public function delete(Module_Shop_Model_Product $product) {
        $table = new Dante_Lib_Orm_Table('[%%]shop_products');
        $table->delete(array('id' => $product->id));
    }

    protected function _addImage($productId, $image) {
        $table = new Dante_Lib_Orm_Table('[%%]shop_product_images');
        $table->product_id = $productId;
        $table->image = $image;
        return $table->insert();
    }
    
    public function deleteImage($imageId) {
        $table = new Dante_Lib_Orm_Table('[%%]shop_product_images');
        return $table->delete(array('id' => $imageId));
    }
    /**
     * получение количества наименований продуктов по категории
     * @param int $categoryId
     * @author Elrafir
     */
    public function getCountList($categoryId=0){
        $dmo=new Dante_Lib_Orm_Table($this->_tableName);
        $dmo->count();
        $dmo->loadBy(array('category_id'=>$categoryId))->fetch();
        return $dmo->count;        
    }
    
    protected function _getProductsImages($list, $productIds) {
        // згружаем картинки продуктов
        $productIds = implode(',', $productIds);
        $sql = "select
                    i.product_id,
                    i.image
                from [%%]shop_product_images as i
                where i.product_id in ({$productIds})
                group by i.product_id
                order by i.order";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);

        
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $productId = (int)$f['product_id'];
            $image = $f['image'];
            
            
                
            
            
            $list[$productId]->folder = Module_Shop_Helper_Shop::getProductsFolder($productId);
            $productFolder = Module_Shop_Helper_Shop::getProductsFolder($productId).'catalog';
            $list[$productId]->images[] = $image;  //$productFolder.'/'.

        }
        
        // проверим картинки продуктов
        foreach($list as $product) {
            if (count($product->images)==0) {
                $product->images[] = Lib_Image_Helper::getNoImage();
            }
        }
        
        return $list;          
    }

    /**
     * Отображает список продуктов в заданной категории
     *
     * @param int $categoryId
     * @return array
     */
    public function getList($categoryId=0, $order = NULL, $limit = NULL) {
        $langId = Dante_Lib_Lang_Helper::getCurrent();

        $sql = "select
            p.id,
            p.code,
            p.link,
            p.price,
            pd.name,
            pd.description,
            pd.short_descr
        from [%%]shop_products as p
        LEFT JOIN [%%]shop_product_description as pd on ( pd.product_id = p.id and lang_id = {$langId} )";
        
        if ($categoryId > 0) $sql .= " where category_id = {$categoryId} and active != 0";
        if($order) $sql .= " {$order}";
        if($limit) $sql .=" {$limit}";
        
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);

        $list = array();
        $productIds = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $model = new Module_Shop_Model_Product();
            $model->id          = (int)$f['id'];
            $model->name        = $f['name'];
            $model->code        = $f['code'];
            $model->price       = (int)$f['price'];
            $model->description = $f['description'];
            $model->shortDescription = $f['short_descr'];
            $model->link        = $f['link'];
           
            $model->types = array();
            $sqlTypes = 'select `type_id` from `[%%]shop_product_types_relations` where `product_id`='.$model->id;
            $res = Dante_Lib_SQL_DB::get_instance()->open($sqlTypes);
            if(!$res) $model->types = false;
            
            while($tf = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($res))
            {
                $type_id = $tf['type_id'];
                $sqlType = 'select * from `[%%]shop_product_types` where `id`='.$type_id;
                $typeInfo = Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sqlType);
                array_push($model->types, $typeInfo);
            }
            
            $list[$model->id] = $model;
            $productIds[] = $model->id;
        }

        if (count($list) == 0) return $list;
      
        // загрузим изображения товаров
        return $this->_getProductsImages($list, $productIds);
    }

    /**
     * Получить список картинок товаров в категории
     * @param int $categoryId
     */
    public function getProductImagesInCategory($categoryId) {
        $sql = "select
                  p.code,
                  i.image
                from [%%]shop_products as p
                left join [%%]shop_product_images as i on (i.product_id = p.id)
                where p.category_id = {$categoryId}
                group by i.product_id
                order by i.order";

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $list = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $list[] = array(
                'code' => $f['code'],
                'image' => $f['image'],
            );
        }

        return $list;
    }

    /**
     * Получение списка продуктов по заданным id
     * @param array $products
     */
    public function getByList($products) {
        if (count($products) == 0) {
            return array();
        }
        $productList = implode(',', $products);

        $langId = Dante_Lib_Lang_Helper::getCurrent();

        $sql = "select
            p.id,
            p.code,
            p.link,
            p.price,
            p.category_id,
            pd.name
        from [%%]shop_products as p
        LEFT JOIN [%%]shop_product_description as pd on ( pd.product_id = p.id and lang_id = {$langId} )
        WHERE p.id in ({$productList})";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);

        $list = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $model = new Module_Shop_Model_Product();
            $model->id = (int)$f['id'];
            $model->code = $f['code'];
            $model->link = $f['link'];
            $model->price = (float)$f['price'];
            $model->category = (int)$f['category_id'];
            $model->name = $f['name'];

            $list[$model->id] = $model;
        }

        return $list;
    }

    /**
     * Получить картинки для заданного набора продуктов
     * @param array $products
     * @return array
     */
    public function getImagesByIds($products) {
        if (count($products) == 0) {
            return array();
        }
        $productList = implode(',', $products);

        $sql = "select
                  i.product_id,
                  i.image
                from [%%]shop_product_images as i
                where i.product_id in ({$productList})
                group by i.product_id
                order by i.order";

        Dante_Lib_Log_Factory::getLogger()->debug("cart sql : $sql");

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $list = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $list[(int)$f['product_id']] = array(
                'product_id' => (int)$f['product_id'],
                'image' => $f['image'],
            );
        }

        return $list;
    }

    /**
     * Загрузить товар по его коду
     * @param string $code
     */
    public function getByCode($code) {
        $langId = Dante_Lib_Lang_Helper::getCurrent();

        $sql = "select
            p.id,
            p.code,
            p.link,
            p.price,
            p.category_id,
            pd.name,
            pd.description
        from [%%]shop_products as p
        LEFT JOIN [%%]shop_product_description as pd on ( pd.product_id = p.id and lang_id = {$langId} )
        WHERE p.code = '{$code}'";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        if (Dante_Lib_SQL_DB::get_instance()->getNumRows($r) == 0) {
            return 0;
        }

        $f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r);
        $model = new Module_Shop_Model_Product();
        $model->id = (int)$f['id'];
        $model->name = $f['name'];
        $model->code = $f['code'];
        $model->price = $f['price'];
        $model->description = $f['description'];
        $model->category = (int)$f['category_id'];
        $model->lang = Module_Lang_Helper::getCurrent();

        // загрузка картинок
        $sql = "select
                    i.image
                from [%%]shop_product_images as i
                where i.product_id = {$model->id}
                order by i.order";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $model->images = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $model->images[] = $f['image'];
        }
        
        
        if (count($model->images)==0) {
            // добавим no-image
            $model->images[] = 'noimage.jpg';
        }

        return $model;
    }
    
    public function getById($id) {
        $langId = Dante_Lib_Lang_Helper::getCurrent();

        $sql = "select
            p.id,
            p.code,
            p.link,
            p.price,
            p.category_id,
            pd.name,
            pd.description,
            pd.seo_title,
            pd.seo_keywords,
            pd.seo_description
        from [%%]shop_products as p
        LEFT JOIN [%%]shop_product_description as pd on ( pd.product_id = p.id and lang_id = {$langId} )
        WHERE p.id = '{$id}'";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        if (Dante_Lib_SQL_DB::get_instance()->getNumRows($r) == 0) {
            return 0;
        }

        $f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r);
        $model = new Module_Shop_Model_Product();
        $model->id = (int)$f['id'];
        $model->name = $f['name'];
        $model->code = $f['code'];
        $model->price = $f['price'];
        $model->description = $f['description'];
        $model->category = (int)$f['category_id'];
        $model->lang = Module_Lang_Helper::getCurrent();
        $model->seoTitle = $f['seo_title'];
        $model->seoKeywords = $f['seo_keywords'];
        $model->seoDescription = $f['seo_description'];


        // загрузка картинок
        $sql = "select image
                from [%%]shop_product_images 
                where `product_id` = {$model->id}
                order by `order` DESC";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $model->images = array();
        
        $productFolder = Module_Shop_Helper_Shop::getProductsFolder($id).'preview';
        $thumbFolder = Module_Shop_Helper_Shop::getProductsFolder($id).'thumb';
        $originalFolder = Module_Shop_Helper_Shop::getProductsFolder($id).'original';
        $index = 0;
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            if ($index == 0) {
				if ( is_file($productFolder.'/'.$f['image']) ) {
					$model->image = $productFolder.'/'.$f['image'];
				}
            }    
			if ( is_file($thumbFolder.'/'.$f['image']) ) {
				$model->images[] = $thumbFolder.'/'.$f['image'];
			}
			if ( is_file($originalFolder.'/'.$f['image']) ) {
				$model->originalImages[] = $originalFolder.'/'.$f['image'];
			}

            $index++;
        }
        
        if (count($model->images)==0) {
            // добавим no-image
            $model->images[] = Lib_Image_Helper::getNoImage();
        }
        if (count($model->originalImages)==0) {
            // добавим no-image
            $model->originalImages[] = Lib_Image_Helper::getNoImage();
        }
        
        
        return $model;
    }
    
    public function getIdByLink($link)
    {
        $sql = "select * from `[%%]shop_products` where `link`='".$link."'";
        $r = Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
        return $r['id'];
    }


    public function getTypeById($id)
    {
        $sql = 'select * from `[%%]shop_product_types` where `id`='.$id;
        return Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
    }
    
    public function getSearchCount($request)
    {
        $langId = Dante_Lib_Lang_Helper::getCurrent();
        $sqlCount = "SELECT COUNT(*) AS `count` FROM {$this->_tableName} AS `p` 
                LEFT JOIN {$this->_tableDescr} AS `pd` ON(`pd`.`product_id` = `p`.`id` AND `lang_id` = {$langId})
                LEFT JOIN {$this->_tableCategory} AS `pc` ON(`pc`.`id` = `p`.`category_id`)
                WHERE `p`.`name` LIKE '%{$request}%' OR `p`.`code` LIKE '%{$request}%' 
                OR `pd`.`short_descr` LIKE '%{$request}%' OR `pd`.`description` LIKE '%{$request}%'
                OR `pc`.`name` LIKE '%{$request}%'";
                
        $count = Dante_Lib_SQL_DB::get_instance()->fetchField($sqlCount, 'count');
        return $count;
    }
    
    public function searchProducts($request, $limit = false)
    {
        $langId = Dante_Lib_Lang_Helper::getCurrent();
        
        $sql = "SELECT `p`.`id`, `p`.`name`, `p`.`link` FROM {$this->_tableName} AS `p` 
                LEFT JOIN {$this->_tableDescr} AS `pd` ON(`pd`.`product_id` = `p`.`id` AND `lang_id` = {$langId})
                LEFT JOIN {$this->_tableCategory} AS `pc` ON(`pc`.`id` = `p`.`category_id`)
                WHERE `p`.`name` LIKE '%{$request}%' OR `p`.`code` LIKE '%{$request}%' 
                OR `pd`.`short_descr` LIKE '%{$request}%' OR `pd`.`description` LIKE '%{$request}%'
                OR `pc`.`name` LIKE '%{$request}%'";
        
        if($limit !== false) $sql .= " {$limit}";
                
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        if(!$r) return false;
        
        $result = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            $item = array();
            $item['link'] = "/product/{$f['id']}-{$f['link']}";
            $item['title'] = $f['name'];
            $result[] = $item;
        }
        
        return $result;
    }
}
