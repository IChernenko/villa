/**
 * contestCategory
 * @autor Elrafir
 */
categoryadmin={
    openEditor:function Function(id, obj){
             
        $(obj).toggleClass('btn-success btn-warning disabled');
        var params={}
        params.action='formEditAjax';
        params.id=id;
        params.controller='module.shop.controller.manage.category'
                
        $('#contestCategoryModal').arcticmodal({
            type: 'ajax',
            url: 'ajax.php',
            ajax: {
                type:'get',
                dataType:"json",
                q:1,
                data:params,
                success:function(data,el,responce){
                    $(obj).toggleClass('btn-success btn-warning disabled');
                    data.body.html(el);
                    $(el).find('.modal-title').html(responce.title);
                    $(el).find('.modal-text').html(responce.html);
                    
                }
            },
            beforeClose: function(data, el) {
                $('#tab_Category').click();
            }
        });
        
        
        
    },
    editSubmit:function(){
        
        var params=$('#editCategory').serializeArray();
        $.ajax({
            url:"ajax.php",
            type:'POST',
            dataType:"json",
            q:1,
            data:params,
            enctype: 'multipart/form-data',
            success:function(data){
                if(data.result==1){
                    $('#modalWindow').arcticmodal('close');
                    handbooks.formSubmit();
                }
                else console.log(data);
            }
        })
        
    },
    delCategory:function(id){
        var params=$
        params={
            controller:'module.shop.controller.manage.category',
            action:'delCategory',
            id:id
        };
        
        $.ajax({
            url:"ajax.php",
            type:'get',
            dataType:"json",
            q:1,
            data:params,
            success:function(data){
                window.location.href=window.location.href;
            }
        })
    },

    showTab: function(obj) {
        //debugger;
        $(obj).tab('show');
        var tabId = obj.hash;

        if (tabId == '#categoryMain') return;

        if (tabId == '#categoryExtInfo') {

            var childCount = $("#categoryExtInfo").children().length;
            if (childCount > 0) return;

            var id = $('#categoryManageRowId').val();

            var params={
                controller:'module.shop.controller.manage.category',
                action:'drawExtInfo',
                id:id
            };

            $.ajax({
                url:"ajax.php",
                type:'get',
                dataType:"json",
                data:params,
                success:function(data){
                    $('#categoryExtInfo').html(data.message);
                    $('#saveExtInfoBtn').click(function(e){
                        var btn = e.target;
                        btn.innerHTML = "Сохранение...";
                        btn.disabled = true;

                        categoryadmin.saveExtInfo();

                        console.log("aaa");
                        return false;
                    });
                }
            })

        }
    },

    saveExtInfo: function() {

        var data = {
            controller:'module.shop.controller.manage.category',
            action:'saveExtInfo'
        };
        data.id = $('#categoryManageRowId').val();
        data.title = $('#categoryManageTitle').val();
        data.keywords = $('#categoryManageKeywords').val();
        data.description = $('#categoryManageDescription').val();



        $.ajax({
            url:"ajax.php",
            type:'post',
            dataType:"json",
            data:data,
            success:function(data){
                if (data.result == 1) {
                    $('#saveExtInfoBtn').html("Сохранить");
                    $('#saveExtInfoBtn').removeAttr('disabled');
                }
            }
        });
    }
}