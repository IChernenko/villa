/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var productadmin={
    id:0,
    controller:"module.shop.controller.manage.product",
    formSubmit:function(){
        if(!this.formValidate()) return false;
        if(productadmin.validate('#addProductForm')){
            data=$('#addProductForm').serializeArray();
            $("#buttonSubmit").toggleClass('btn-success btn-warning disabled').text('Loading...');
            data.push(
                {name:'controller', value:productadmin.controller},
                {name:'id', value:productadmin.id},
                {name:'action', value:'edit'}
            );
            $.ajax({
            type: 'POST',
            data:data,
            url: '/ajax.php',
            dataType : "json",
            success: function (data){
                
                if (data.result == 1) {
                    //window.location.href=location.href
                    $('#contests_edit_raport').stop(true,true).attr('class', 'alert alert-success').fadeIn(1).html(data.raport).fadeOut(5000);
                    productadmin.tabStatus();
                    if (data.id !== undefined) {
                        productadmin.id=data.id;
                    }else{
                        
                    }

                }else{
                    $('#contests_edit_raport').stop(true, true).attr('class', 'alert alert-error').fadeIn(1).html(data.raport).fadeOut(5000);
                }
               $("#buttonSubmit").toggleClass('btn-success btn-warning disabled').text('Сохранить');
               if(data.eval){
                   eval(data.eval);
               }
              
            }
            })
        }
    },
    formValidate: function()
    {        
        if(!$('#productName').val())
        {
            jAlert('Введите название товара', 'Ошибка');
            return false;
        }
        return true;
    },
    formDescSubmit:function(product_id){
        if(productadmin.validate('#productDescEdit')){
            $('#desc_description').elrte('updateSource');
            data=$('#productDescEdit').serializeArray();
            $("#buttonSubmit").toggleClass('btn-success btn-warning disabled').text('Loading...');
            data.push(
                {name:'controller', value:productadmin.controller},
                {name:'product_id', value:product_id},
                {name:'action', value:'editDesc'}
            );
            $.ajax({
            type: 'POST',
            data:data,
            url: '/ajax.php',
            dataType : "json",
            success: function (data){
                if (data.result == 1) {
                   $("#tab_stages").click(); 
                }else{
                    $('#contests_edit_raport').stop(true, true).attr('class', 'alert alert-error').fadeIn(1).html(data.error).fadeOut(5000);
                }
               $("#buttonSubmit").toggleClass('btn-success btn-warning disabled').text('Сохранить');
               if(data.eval){
                   eval(data.eval);
               }
              
            }
            })
                
        }
    },
    validate:function(form_id){
        return true;
    },
    toggleActive:function(id, active){
        var data={
            controller:productadmin.controller,
            action:'toggleActive',
            id:id,
            active:active
        }
        $.ajax({
            type: 'POST',
            url: '/ajax.php',
            dataType : "json",
            data:data,
            success:function(data){
                if(data.result==1){
                    $('#row_'+data.id).toggleClass("error success").find('.btn-active').toggleClass("hidden");
                }
            }
        })
    },
    /**
     * 
     */
    delProduct:function(id){
        //Удаление продукта
        var data={
            controller:productadmin.controller,
            action:'delProduct',
            id:id
        }
        $.ajax({
            type: 'POST',
            url: '/ajax.php',
            dataType : "json",
            data:data,
            success:function(data){
                if(data.result==1){
                    $('#row_'+data.id).fadeOut(300, function(){
                        $(this).remove();
                    })
                }
            }
        })
    },
    delImg:function(id){
        //Удаление картинки
        var data={
            controller:productadmin.controller,
            action:'delImage',
            id:id
        }
        $.ajax({
            type: 'POST',
            url: '/ajax.php',
            dataType : "json",
            data:data,
            success:function(data){
                if(data.result==1){
                    $('#img_row_'+data.id).fadeOut(300, function(){
                        $(this).remove();
                    })
                }
            }
        });
    },
    upImg:function(id){
        var data={
            controller:productadmin.controller,
            action:'upImage',
            id:id
        }
        $.ajax({
            type: 'POST',
            url: '/ajax.php',
            dataType : "json",
            data:data,
            success:function(data){
                if(data.result==1){
					$("#tab_images").click();
                }
            }
        });
    },
	downImg:function(id){
        var data={
            controller:productadmin.controller,
            action:'downImage',
            id:id
        }
        $.ajax({
            type: 'POST',
            url: '/ajax.php',
            dataType : "json",
            data:data,
            success:function(data){
                if(data.result==1){
					$("#tab_images").click();
                }
            }
        });
    },
    tabStatus:function(){
        if($('#inputAction').val()=='add'){
           $('.nav-tabs .editonly').addClass('hide');
           $('.nav-tabs .addOnly').removeClass('hide');
           
        }else{
           $('.nav-tabs .editonly').removeClass('hide');
           $('.nav-tabs .addOnly').addClass('hide');
           $('#actionTitle').html('Основна інформація');
        }
    },
    loadContent:function(obj){        
        var event_id=$(obj).attr('href');
        var data = {};
        if(!(data=productadmin.controllerChanger(event_id))){            
            return false;
        }
        $(event_id).html(productadmin.loaderHtml);
        data.id=productadmin.id;
        $.ajax({
            type: 'GET',
            data: data,
            url: '/ajax.php',
            dataType : "json",
            success: function (data){ 
                if(data.result==1){
                    $(event_id).html(data.html);
                }else{
                    $(event_id).html(data.message);
                }
                if(data.eval){
                   eval(data.eval);
                }
            }
        })
        
    },
    controllerChanger:function(id){
        
        switch(id){
            case '#productsAdd':
                return false;
                break;
            case '#productsDescription':
                controller=productadmin.controller;
                action='drawDescForm';
                break;
            case '#productsImages':
                controller=productadmin.controller;
                action='drawImages';
                break;
            default:
                return false;
            break;
        }
        return {controller:controller, action:action};
    },
    loaderHtml:'<h1>Loading...</h1>'+
        '<div class="progress progress-striped active" style="width:150px; margin:0 auto;">'+
        '<div class="bar" style="width: 100%;"></div></div>'
}
