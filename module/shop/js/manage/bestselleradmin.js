/**
 * @author Valkyria
 */
bestselleradmin = {
    formSubmit: function()
    {
        var data = $('#editBestseller').serializeArray();
        $.ajax({
            url: "ajax.php",
            type: 'POST',
            dataType: "json",
            data: data,
            success:function(data){
                if(data.result == 1){
                    $('#modalWindow').arcticmodal('close');
                    handbooks.formSubmit();
                }
                else console.log(data);
            }
        })
    },
    openProductWindow: function(id)
    {
        $('#productModal').arcticmodal({
            type: 'ajax',
            url: 'ajax.php',
            ajax: {
                type:'get',
                dataType:"json",
                q:1,
                data: {
                    controller: 'module.shop.controller.manage.bestseller',
                    action: 'drawProductForm',
                    prod_id: $('#productID').val()
                },
                success:function(data,el,response){ 
                    data.body.html(el);
                    $(el).find('.modal-body').html(response.html);
                    $(el).find('#rows_in_page').remove();
                }
            }
        });
    },
    changeContent: function(elem)
    {
        if(elem && $(elem).prop('disabled')) return false;
        $('#productModal button, #productModal select').prop('disabled', true);
        if(!elem) elem = $('#categorySelect');
        $.ajax({
            type: 'post',
            url: 'ajax.php',
            data: {
                controller: 'module.shop.controller.manage.bestseller',
                action: 'drawProductForm',
                prod_id: $('#productID').val(),
                category_id: $(elem).val(),
                page: $('#paginator_page').val()
            },
            success: function(data)
            {
                if(data.result == 1) 
                {
                    $('#productModal .modal-body').html(data.html);                    
                    $('#rows_in_page').remove();
                }
                else console.log(data);
            }
        });
    },
    selectProduct: function(obj, id)
    {
        var image = $(obj).parents('tr').find('td.image img').attr('src');
        var info = $(obj).parents('tr').find('td.info').html();
        $('#productModal').arcticmodal('close');
        $('#bestsellerTable td.image img').attr('src', image);
        $('#bestsellerTable td.info').html(info);
        $('#bestsellerTable input#productID').val(id);
    }
}


