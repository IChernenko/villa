<?php
global $package;
$package = array(
    'version' => '17',
    'name' => 'module.shop',
    'dependence' => array(
        'module.auth',
        'module.comment',
        'module.lang',
        'lib.lightbox',
        'component.imageScroll',
        'ws' =>  array(
            'admin' => array(
                'module.uploadify',
                'lib.jquery.ikselect',
                'module.handbooksgenerator',
                'lib.twitbootstrap',
                'lib.jquery.elrte'
             )
        ),
    ),    
    'js' => array(
        'ws' =>  array(
            'admin' => array(
                '../module/shop/js/manage/categoryadmin.js',
                '../module/shop/js/manage/productadmin.js',
                '../module/shop/js/manage/bestselleradmin.js',
            )
       )     
    ),    

    'css'=>array(
        'ws' =>  array(
            'admin' => array(
                '../module/shop/css/manage/categoryadmin.css',
                '../module/shop/css/manage/productsadmin.css',
             )
         )   
    )
    
);