CREATE TABLE `[%%]shop_cart` (
  `id` int(11) unsigned DEFAULT NULL AUTO_INCREMENT,
  `sid` varchar(32) DEFAULT NULL,
  `product_id` int(11) unsigned DEFAULT NULL,
  `quantity` int(11) DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Корзина покупателя';

ALTER TABLE [%%]shop_cart ADD
      CONSTRAINT fk_cart_product_id
      FOREIGN    KEY (product_id)
      REFERENCES [%%]shop_products(id) ON DELETE CASCADE ON UPDATE CASCADE;
