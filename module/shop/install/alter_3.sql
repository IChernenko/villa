CREATE TABLE `[%%]shop_order` (
  `id` int(11) unsigned DEFAULT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned DEFAULT NULL,
  `fio` varchar(128) DEFAULT NULL,
  `country` varchar(32) DEFAULT NULL,
  `city` varchar(48) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Заказ';

CREATE TABLE `[%%]shop_order_detail` (
  `id` int(11) unsigned DEFAULT NULL AUTO_INCREMENT,
  `order_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(10, 2) NOT NULL,

  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Состав заказа';

ALTER TABLE [%%]shop_order_detail ADD
      CONSTRAINT fk_shop_order_detail_product_id
      FOREIGN    KEY (product_id)
      REFERENCES [%%]shop_products(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE [%%]shop_order_detail ADD
      CONSTRAINT fk_shop_order_detail_order_id
      FOREIGN    KEY (order_id)
      REFERENCES [%%]shop_order(id) ON DELETE CASCADE ON UPDATE CASCADE;