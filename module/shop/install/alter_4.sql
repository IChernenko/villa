CREATE TABLE `[%%]shop_last_visited` (
  `id` int(11) unsigned DEFAULT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned DEFAULT NULL,
  `sid` varchar(32) DEFAULT NULL,
  `product_id` int(11) unsigned DEFAULT NULL,
  `view_date` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Последние просмотренные товары';

ALTER TABLE `[%%]shop_last_visited` ADD
      CONSTRAINT fk_last_visited_product_id
      FOREIGN KEY (product_id)
      REFERENCES [%%]shop_products(id) ON DELETE CASCADE ON UPDATE CASCADE;