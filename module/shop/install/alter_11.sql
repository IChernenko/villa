DROP TABLE IF EXISTS `[%%]shop_product_types`;

CREATE TABLE `[%%]shop_product_types` (
  `id` int(11)  NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `diff_price` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `[%%]shop_product_types_relations`;
CREATE TABLE `[%%]shop_product_types_relations` (
  `product_id` int(11) UNSIGNED NOT NULL,
  `type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE [%%]shop_product_types_relations ADD
      CONSTRAINT fk_shop_product_types_relations_product_id
      FOREIGN KEY (`product_id`)
      REFERENCES [%%]shop_products(id) ON DELETE CASCADE ON UPDATE CASCADE;
      
ALTER TABLE [%%]shop_product_types_relations ADD
      CONSTRAINT fk_shop_product_types_relations_type_id
      FOREIGN KEY (`type_id`)
      REFERENCES [%%]shop_product_types(id) ON DELETE CASCADE ON UPDATE CASCADE;