alter table [%%]shop_product_description add column seo_title varchar(255);
alter table [%%]shop_product_description add column seo_keywords text;
alter table [%%]shop_product_description add column seo_description text;