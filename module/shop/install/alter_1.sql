CREATE TABLE `[%%]shop_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `parent_id` int(11) unsigned DEFAULT '0',
  `link` varchar(128) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT="Категории";


CREATE TABLE `[%%]shop_categories_seo` (
  `category_id` int(11) unsigned DEFAULT NULL,
  `lang_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` text,
  KEY `[%%]shop2_categories_seo_category_id_fk` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='SEO данные категорий продуктов';

ALTER TABLE [%%]shop_categories_seo ADD
      CONSTRAINT fk_categories_seo_category_id
      FOREIGN    KEY (category_id)
      REFERENCES [%%]shop_categories(id) ON DELETE CASCADE ON UPDATE CASCADE;


CREATE TABLE `[%%]shop_products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `price` decimal(10, 2),
  `link` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `active` tinyint(1),
  `category_id` int(11) unsigned not null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='Продукты';

ALTER TABLE [%%]shop_products ADD
      CONSTRAINT fk_products_category_id
      FOREIGN  KEY (category_id)
      REFERENCES [%%]shop_categories(id) ON DELETE CASCADE ON UPDATE CASCADE;


CREATE TABLE `[%%]shop_product_description` (
  `product_id` int(11) unsigned NOT NULL,
  `lang_id` int(11) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='Описания продуктов';

ALTER TABLE [%%]shop_product_description ADD
      CONSTRAINT fk_product_description_product_id
      FOREIGN  KEY (product_id)
      REFERENCES [%%]shop_products(id) ON DELETE CASCADE ON UPDATE CASCADE;


CREATE TABLE `[%%]shop_product_images` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `order` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='Картинки продуктов';

ALTER TABLE [%%]shop_product_images ADD
      CONSTRAINT fk_product_images_product_id
      FOREIGN  KEY (product_id)
      REFERENCES [%%]shop_products(id) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE `[%%]shop_customer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL,
  `country_id` int(11) unsigned NOT NULL,
  `city` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='Покупатель';