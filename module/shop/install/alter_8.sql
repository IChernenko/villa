ALTER TABLE  `[%%]shop_categories` CHANGE  `parent_id`  `parent_id` INT( 11 ) UNSIGNED NULL DEFAULT NULL;
ALTER TABLE  `[%%]shop_categories` ADD  `turn` INT NOT NULL;
UPDATE  `[%%]shop_categories` SET  `parent_id` = NULL WHERE  `[%%]shop_categories`.`id` =1;
ALTER TABLE  `[%%]shop_categories` ADD INDEX (  `parent_id` );

ALTER TABLE  `[%%]shop_categories` ADD FOREIGN KEY (  `parent_id` ) REFERENCES  `[%%]shop_categories` (
`id`) ON DELETE CASCADE ON UPDATE CASCADE ;