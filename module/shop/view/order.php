<?php
/**
 * View для отображения оформления заказа
 * User: dorian
 * Date: 27.03.12
 * Time: 18:27
 * To change this template use File | Settings | File Templates.
 */
class Module_Shop_View_Order
{
    public function draw($cart) {
        $tplFileName = '../module/shop/template/order.html';

        $customTpl = Dante_Helper_App::getWsPath().'/shop/order.html';
        
        if (file_exists($customTpl)) {
            $tplFileName = $customTpl;
        }

        $tpl = new Dante_Lib_Template();
        $tpl->cart = $cart;
        return $tpl->draw($tplFileName);
    }


}
