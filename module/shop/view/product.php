<?php
/**
 * View для отображения детализации товара
 * User: dorian
 * Date: 27.03.12
 * Time: 18:27
 * To change this template use File | Settings | File Templates.
 */
class Module_Shop_View_Product
{
    protected $_images = array();

    /**
     * @var Dante_Lib_Template
     */
    protected $_tpl;

    function __construct() {
        $this->_tpl = new Dante_Lib_Template();
    }

    public function getTpl() {
        return $this->_tpl;
    }

    public function setImages($images) {
        $this->_images = $images;
    }

    public function draw(Module_Shop_Model_Product $product, $currency) {
        $tplFileName = '../module/shop/template/product.html';
        $customTpl = Dante_Helper_App::getWsPath().'/shop/product.html';
        if (file_exists($customTpl)) $tplFileName = $customTpl;
        

        $this->_tpl->product = $product;
        $this->_tpl->images = $this->_images;
        $this->_tpl->currency = $currency;
        return $this->_tpl->draw($tplFileName);
    }


}
