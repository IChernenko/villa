<?php



/**
 * Description of categorytree
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Shop_View_Categorytree extends Module_Dropdown_View{
    
    public $id = false;
    
    protected function _drawItem($item, $level) {
        return "<li><a href=\"/category/{$item->id}-{$item->url}\">{$item->name}</a>";
    }
}

?>
