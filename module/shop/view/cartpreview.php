<?php
/**
 * View для отображения превью корзины
 * User: dorian
 * Date: 09.04.12
 * Time: 18:16
 * To change this template use File | Settings | File Templates.
 */
class Module_Shop_View_Cartpreview
{
    public function draw($cart) {
        $tplFileName = '../module/shop/template/cartpreview.html';

        $customTpl = Dante_Helper_App::getWsPath().'/shop/cartpreview.html';
        
        if (file_exists($customTpl)) {
            $tplFileName = $customTpl;
        }

        $tpl = new Dante_Lib_Template();
        $tpl->cart = $cart;
        return $tpl->draw($tplFileName);
    }


}
