<?php
/**
 * View для отображения последних просматриваемых товаров
 * User: dorian
 * Date: 12.04.12
 * Time: 20:07
 * To change this template use File | Settings | File Templates.
 */
class Module_Shop_View_Lastvisited
{
    public function draw($items) {
        $tplFileName = '../module/shop/template/lastvisited.html';

        $customTpl = Dante_Helper_App::getWsPath().'/shop/lastvisited.html';
        
        if (file_exists($customTpl)) {
            $tplFileName = $customTpl;
        }

        $tpl = new Dante_Lib_Template();
        $tpl->items = $items;
        return $tpl->draw($tplFileName);
    }
}
