<?php
/**
 * View для отображения детализации товара
 * User: dorian
 * Date: 27.03.12
 * Time: 18:27
 * To change this template use File | Settings | File Templates.
 */
class Module_Shop_View_Slider
{
    protected $_images = array();

    /**
     * @var Dante_Lib_Template
     */
    protected $_tpl;

    function __construct() {
        $this->_tpl = new Dante_Lib_Template();
    }

    public function getTpl() {
        return $this->_tpl;
    }


    public function draw($products) {
        $tplFileName = '../module/shop/template/slider.html';
        $customTpl = Dante_Helper_App::getWsPath().'/shop/slider.html';
        if (file_exists($customTpl)) $tplFileName = $customTpl;
        

        $this->_tpl->products = $products;
        //$this->_tpl->images = $this->_images;
        return $this->_tpl->draw($tplFileName);
    }


}
