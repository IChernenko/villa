<?php
class Module_Shop_View_Manage_Category
{    
    public function drawForm($model, $combobox, $uploadParams)
    {
        $tpl = new Dante_Lib_Template();
        $tpl->category = $model;
        $tpl->combobox = $combobox;
        $tpl->upload = $uploadParams;
        
        $fileName = '../module/shop/template/manage/category/form.html';
        if (file_exists('template/default/manage/category/form.html')) {
            $fileName = 'template/default/manage/category/form.html';
        }
        
        return $tpl->draw($fileName);
    }
}
?>