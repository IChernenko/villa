<?php
class Module_Shop_View_Manage_Bestseller
{    
    public function drawForm($id, $product)
    {
        $tpl = new Dante_Lib_Template();
        $tpl->id = $id;
        $tpl->product = $product;
        
        $fileName = '../module/shop/template/manage/bestseller/form.html';
        if (file_exists('template/default/manage/bestseller/form.html')) {
            $fileName = 'template/default/manage/bestseller/form.html';
        }
        
        return $tpl->draw($fileName);
    }
    
    public function drawProductForm($prodId, $categId, $products, $categories)
    {
        $tpl = new Dante_Lib_Template();
        $filePath = '../module/shop/template/manage/bestseller/productForm.html';
        
        $tpl->prod_id = $prodId;
        $tpl->categ_id = $categId;
        $tpl->products = $products;
        $tpl->categories = $categories;
        return $tpl->draw($filePath);
    }
}
?>