<?php
class Module_Shop_View_Manage_Product
{    
    public function drawForm($vars=false)
    {
        $tpl = new Dante_Lib_Template();
        $tpl->_vars=$vars;
        $fileName = '../module/shop/template/manage/products/form.html';
        if (file_exists('template/default/manage/products/form.html')) {
            $fileName = 'template/default/manage/products/form.html';
        }
        //echo$_SERVER['SCRIPT_FILENAME'];
        return $tpl->draw($fileName);
    }
    
    public function drawFormDesc($vars=false)
    {
        $tpl = new Dante_Lib_Template();
        $tpl->_vars=$vars;
        $fileName = '../module/shop/template/manage/products/formdesc.html';
        if (file_exists('template/default/manage/products/formdesc.html')) {
            $fileName = 'template/default/manage/products/formdesc.html';
        }
        //echo$_SERVER['SCRIPT_FILENAME'];
        return $tpl->draw($fileName);        
    }
    
    public function drawUploader($vars=false)
    {
        $tpl = new Dante_Lib_Template();
        $tpl->_vars=$vars;
        $fileName = '../module/shop/template/manage/products/uploader.html';
        if (file_exists('template/default/manage/products/uploader.html')) {
            $fileName = 'template/default/manage/products/uploader.html';
        }
        //echo$_SERVER['SCRIPT_FILENAME'];
        return $tpl->draw($fileName);
    }
}
?>