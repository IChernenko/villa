<?php
/**
 * View для отображения каталога товаров
 * User: dorian
 * Date: 27.03.12
 * Time: 18:27
 * To change this template use File | Settings | File Templates.
 */
class Module_Shop_View_Catalog
{
    public $sort = array();
    
    public $category = false;

    public $productCount = '';

    public $currency = '';
    
    public function draw($list,  $paginator, $categList) {
        $tplFileName = '../module/shop/template/products.html';

        $customTpl = Dante_Helper_App::getWsPath().'/shop/products.html';
        
        if (file_exists($customTpl)) {
            $tplFileName = $customTpl;
        }

        $tpl = new Dante_Lib_Template();
        $tpl->categList = $categList;
        $tpl->sort = $this->sort;
        $tpl->currency = $this->currency;
        $tpl->products  = $list;
        $tpl->paginator = $paginator;
        $tpl->category = $this->category;
        $tpl->productCount = $this->productCount;
        return $tpl->draw($tplFileName);
    }


}
