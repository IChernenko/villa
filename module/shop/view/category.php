<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 27.03.12
 * Time: 18:27
 * To change this template use File | Settings | File Templates.
 */
class Module_Shop_View_Category
{
    public function draw($list) {
        $tpl = new Dante_Lib_Template();
        $tpl->categories = $list;

        $tplFileName = '../module/shop/template/categories.html';
        $customTpl = Dante_Helper_App::getWsPath().'/shop/categories.html';
        if (file_exists($customTpl)) {
            $tplFileName = $customTpl;
        }

        return $tpl->draw($tplFileName);
    }

    public function drawPreview($list) {
        $tpl = new Dante_Lib_Template();
        $tpl->categories = $list;

        $tplFileName = '../module/shop/template/categories_preview.html';
        $customTpl = Dante_Helper_App::getWsPath().'/shop/categories_preview.html';
        if (file_exists($customTpl)) {
            $tplFileName = $customTpl;
        }

        return $tpl->draw($tplFileName);
    }
}
