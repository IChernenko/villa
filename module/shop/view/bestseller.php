<?php

class Module_Shop_View_Bestseller
{
    public $currency = '';
    
    public function draw($list, $images)
    {
        $tpl = new Dante_Lib_Template();
        $tpl->list = $list;
        $tpl->images = $images;
        $tpl->currency = $this->currency;
        
        $fileName = '../module/shop/template/bestseller.html';        
        $customTpl = Dante_Helper_App::getWsPath().'/shop/bestseller.html';
        
        if (file_exists($customTpl)) $fileName = $customTpl;
        
        return $tpl->draw($fileName);
    }
}

?>
