<?php

/**
 * Description of categorytree
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Shop_View_Categorycontent extends Module_Dropdown_View{
    
    protected function _drawItem($item, $level) {
        if ($level == 1)
            return "<li><h2><a href=\"/category/{$item->id}-{$item->url}\">{$item->name}</a></h2>";
        
        return "<li><h3><a href=\"/category/{$item->id}-{$item->url}\">{$item->name}</a></h3>";
    }
}

?>
