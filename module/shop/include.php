<?php

Dante_Lib_Config::set('shop.enableMultilanguage',  false);
Dante_Lib_Config::set('app.uploadFolder', 'media');
Dante_Lib_Config::set('shop.productsFolder', 'products');
Dante_Lib_Config::set('shop.categoryFolder', 'categories');

Dante_Lib_Config::set('shop.categoryPreview.imgWidth', 280);
Dante_Lib_Config::set('shop.categoryPreview.imgHeight', 180);

Dante_Lib_Config::set('shop.catalog.imgWidth', 280);
Dante_Lib_Config::set('shop.catalog.imgHeight', 180);
Dante_Lib_Config::set('shop.catalog.productsPerPage', 10); // колличество продуктов на странице

Dante_Lib_Config::set('shop.preview.imgWidth', 445);
Dante_Lib_Config::set('shop.preview.imgHeight', 315);

Dante_Lib_Config::set('shop.thumb.imgWidth', 95);
Dante_Lib_Config::set('shop.thumb.imgHeight', 95);

Dante_Lib_Config::set('shop.slider.imgWidth', 280);
Dante_Lib_Config::set('shop.slider.imgHeight', 180);

Dante_Lib_Config::set('shop.allowShortDescription', true);
Dante_Lib_Config::set('shop.shortDescriptionMax', 150);

Dante_Lib_Config::set('shop.catalogSortDefault', false);

Dante_Lib_Observer_Helper::addObserver(new Module_Shop_Observer_Breadcrumbs(), 'breadcrumbs.draw');
Dante_Lib_Observer_Helper::addObserver(new Module_Shop_Observer_Search(), 'search');

Dante_Lib_Observer_Helper::addObserver(new Module_Shop_Observer_Seo(), 'seo.draw.title');
Dante_Lib_Observer_Helper::addObserver(new Module_Shop_Observer_Seo(), 'seo.draw.keywords');
Dante_Lib_Observer_Helper::addObserver(new Module_Shop_Observer_Seo(), 'seo.draw.description');

?>