<?php

class Module_Shop_Observer_Search implements Dante_Lib_Observer_Iobserver
{
    public function notify($model, $strEventType) 
    {
        $mapper = new Module_Shop_Mapper_Catalog(); 
        if($model->limit)
            $model->total = $mapper->getSearchCount($model->request);
        $products = $mapper->searchProducts($model->request, $model->limit);
        if($products !== false && count($products) != 0)
            $model->result['products'] = $products;
        
        return true;
    }
}


?>
