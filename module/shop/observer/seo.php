<?php
/**
 * Обработка сео
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Shop_Observer_Seo implements Dante_Lib_Observer_Iobserver{


    protected function _loadCategory($categoryId, $object) {

        $storedObject = Dante_Helper_App::getCache()->get('seo.c'.$categoryId);
        if ($storedObject) {
            $object->title = $storedObject->title;
            $object->keywords = $storedObject->keywords;
            $object->description = $storedObject->description;
            return;
        }

        $mapper = new Module_Shop_Mapper_Category();
        $category = $mapper->get($categoryId);
        if (!$category) return false;
        $object->title = $category->title;
        $object->keywords = $category->keywords;
        $object->description = $category->description;

        Dante_Helper_App::getCache()->set('seo.c'.$categoryId, $object);
    }

    protected function _loadProduct($productId, $object) {
        $storedObject = Dante_Helper_App::getCache()->get('seo.p'.$productId);
        if ($storedObject) {
            $object->title = $storedObject->title;
            $object->keywords = $storedObject->keywords;
            $object->description = $storedObject->description;
            return;
        }

        $mapper = new Module_Shop_Mapper_Catalog();
        $product = $mapper->getById($productId);
        if (!$product) return false;

        $object->title = $product->seoTitle;
        $object->keywords = $product->seoKeywords;
        $object->description = $product->seoDescription;

        Dante_Helper_App::getCache()->set('seo.p'.$productId, $object);
    }

    protected function _loadSeoData($object) {
        $url = Dante_Controller_Front::getRequest()->get_requested_url();

        $categoryId = Module_Shop_Helper_Shop::getCategoryIdFromUrl($url);
        if ($categoryId > 0) $this->_loadCategory($categoryId, $object);

        $productId = Module_Shop_Helper_Shop::getProductIdFromUrl($url);
        if ($productId > 0) $this->_loadProduct($productId, $object);
    }

    public function notify($object, $eventType ) {
        $seoEvents = array(
            'seo.draw.title',
            'seo.draw.keywords',
            'seo.draw.description'
        );

        if (in_array($eventType, $seoEvents)) {
            $this->_loadSeoData($object);
        }
    }
}