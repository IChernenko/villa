<?php
/**
* Description of auth
*
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Shop_Observer_Breadcrumbs implements Dante_Lib_Observer_Iobserver{

    protected function _buildBreadCrumbs($categoryId) {
        $categoryMapper = new Module_Shop_Mapper_Category();
        $list = $categoryMapper->getAllList();

        $categories = array();

        $curCategoryId = $categoryId;

        while( isset($list[$curCategoryId]) &&  $list[$curCategoryId]->parent >= 1) {
            $categories[] = array(
                'url' => '/category/'.$list[$curCategoryId]->id.'-'.$list[$curCategoryId]->url,
                'caption' => $list[$curCategoryId]->name
            );
            $curCategoryId = $list[$curCategoryId]->parent;
        }

        return $categories;
    }

    /**
     * Определяет категорию из url
     * @param string $url
     * @return bool|int
     */
    protected function _getCategoryIdFromUrl($url) {
        $pattern = '/^\/category\/(\d+)\-/';
        if(!preg_match_all($pattern, $url, $matches)) return false; // не можем определить категорию
        return (int)$matches[1][0];
    }

    protected function _getProductIdFromUrl($url) {
        $pattern = '/^\/product\/(\d+)\-/';
        if(!preg_match_all($pattern, $url, $matches)) return false; // не можем определить категорию
        return (int)$matches[1][0];
    }

    public function notify($object, $strEventType ) {

        $url = Dante_Controller_Front::getRequest()->get_requested_url();
        $categoryId = $this->_getCategoryIdFromUrl($url);
        if (!$categoryId) {
            $productId = $this->_getProductIdFromUrl($url);
            if (!$productId) return;
            $productMapper = new Module_Shop_Mapper_Catalog();
            $product = $productMapper->getById($productId);
            $categoryId = $product->category;

        }

        if (!$categoryId) return;

        $categories = $this->_buildBreadCrumbs($categoryId);
        foreach($categories as $category) {
            //var_dump($category);
            $object->add($category['url'], $category['caption']);
        }

        //var_dump($object);
        //var_dump($strEventType);
    }
}