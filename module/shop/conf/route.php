<?php

$url = Dante_Controller_Front::getRequest()->get_requested_url();

Dante_Lib_Router::addRule('/^\/category\/(\d+)\-([A-Za-zА-Яа-я\-\_\.]+)\/page(\d+)$/', array(
    'controller' => 'Module_Shop_Controller_Catalog'
));

Dante_Lib_Router::addRule('/^\/category\/(\d+)\-([A-Za-zА-Яа-я\-\_\.]+)$/', array(
    'controller' => 'Module_Shop_Controller_Catalog'
));

Dante_Lib_Router::addRule('/^\/category\/(\d+)\-([A-Za-zА-Яа-я\-\_\.]+)\/(order)\/([A-Za-z\_\-]+)$/', array(
    'controller' => 'Module_Shop_Controller_Catalog'
));

Dante_Lib_Router::addRule('/^\/category\/(\d+)\-([A-Za-zА-Яа-я\-\_\.]+)\/(order)\/([A-Za-z\_\-]+)\/page(\d+)$/', array(
    'controller' => 'Module_Shop_Controller_Catalog'
));

//Dante_Lib_Router::addRule('/^\/([a-z]+)\.html$/', array(
//    'controller' => 'Module_Shop_Controller_Catalog'
//));

Dante_Lib_Router::addRule("/^\/product\/[A-Za-z0-9\-\_\.]+$/", array(
    'controller' => 'Module_Shop_Controller_Product',
    'params' => array('url'=>$url)
));

Dante_Lib_Router::addRule("/^\/cart\/$/", array(
    'controller' => 'Module_Shop_Controller_Cart'
));

Dante_Lib_Router::addRule("/^\/order\/$/", array(
    'controller' => 'Module_Shop_Controller_Order'
));

Dante_Lib_Router::addRule("/^\/manage\/category\/$/", array(
    'controller' => 'Module_Shop_Controller_Manage_Category'
));
Dante_Lib_Router::addRule("/^\/manage\/products\/$/", array(
    'controller' => 'Module_Shop_Controller_Manage_Product'
));
Dante_Lib_Router::addRule("/^\/manage\/bestseller\/$/", array(
    'controller' => 'Module_Shop_Controller_Manage_Bestseller'
));


?>