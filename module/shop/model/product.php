<?php
/**
 * Модель товара
 * User: dorian
 * Date: 22.03.12
 * Time: 14:06
 * To change this template use File | Settings | File Templates.
 */
class Module_Shop_Model_Product
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $description;

    public $shortDescription;

    /**
     * @var float
     */
    public $price;

    /**
     * Код товара
     * @var string
     */
    public $code;
    
    
    /**
     * Типы товара
     * @var type 
     */
    public $types;


    public $image;

    /**
     * Изображения продукта
     * @var array
     */
    public $images = array();
    
    public $originalImages = array();

    /**
     * Иднетификатор языка
     * @var int
     */
    public $lang;

    /**
     * Категория продукта    
     * @var int
     */
    public $category;
    
    /**
     * Ссылка на продукт
     * @var string      
     */         
    public $link;

    /**
     *  Папка продукта    
     *  @var string
     */             
    public $folder;

    public $seoTitle;

    public $seoKeywords;

    public $seoDescription;
}
