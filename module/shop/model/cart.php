<?php
/**
 * Модель корзины
 * User: dorian
 * Date: 04.04.12
 * Time: 13:27
 * To change this template use File | Settings | File Templates.
 */
class Module_Shop_Model_Cart
{
    /**
     * Элемента корзины
     * @var array
     */
    public $items = array();

    /**
     * Общая сумма в корзине
     * @var int
     */
    protected $_total = 0;

    /**
     * Добавить элемент в корзину
     * @param $item
     */
    public function add($item) {
        $this->items[] = $item;
    }

    public function recalc() {
        $this->_total = 0;
        foreach($this->items as $index=>$item) {
            $this->items[$index]['sum'] = $this->items[$index]['price'] * $this->items[$index]['quantity'];
            $this->_total += $this->items[$index]['sum'];
        }
    }

    /**
     * Получиить общую сумму в корзине
     * @return int
     */
    public function getTotal() {
        return $this->_total;
    }

    public function getCount() {
        return count($this->items);
    }
}
