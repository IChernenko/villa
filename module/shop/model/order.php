<?php
/**
 * Модель заказа
 * User: dorian
 * Date: 05.04.12
 * Time: 14:38
 * To change this template use File | Settings | File Templates.
 */
class Module_Shop_Model_Order
{
    public $id;

    public $uid;

    public $fio;

    public $country;

    public $city;

    public $address;

    public $phone;

    public $email;

    public $comment;
}
