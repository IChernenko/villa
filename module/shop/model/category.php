<?php
/**
 * Модель категории
 * User: dorian
 * Date: 18.03.12
 * Time: 16:47
 * To change this template use File | Settings | File Templates.
 */
class Module_Shop_Model_Category
{
    /**
     * Идентификатор категории
     * @var int
     */
    public $id;

    /**
     * Нименование категории
     * @var string
     */
    public $name;

    /**
     * Родительская категория
     * @var int
     */
    public $parent;

    /**
     * Url категории
     * @var string
     */
    public $url;

    /**
     * Картинка категории
     * @var string
     */
    public $image;

    /**
     * Порядок отображения категории
     * @var int
     */
    public $draw_order;

    /**
     *
     * @var string
     */
    public $title;

    /**
     * Сео-заголовок категории
     * @var string
     */
    public $keywords;

    /**
     * Сео описание категории
     * @var string
     */
    public $description;
    
    /**
     * Язык категории
     * @var int
     */
    public $lang = 0;
}
