<?php
class Module_Shop_Model_Manage_Products extends Module_Shop_Model_Manage_Productsgrid{
    
    /**
     *
     * @var int 
     */
    public $id;
    /**
     *
     * @var string 
     */
    public $name;
    /**
     *
     * @var string 
     */
    public $code;
    /**
     *
     * @var string 
     */
    public $price;
    /**
     *
     * @var string 
     */
    public $link;
    /**
     *
     * @var int 
     */
    public $order;
    /**
     *
     * @var int 0|1
     */
    public $active;
    /**
     *
     * @var int 
     */
    public $category_id;
    /**
     * @var int
     */
    public $lang_id;
    /**
     *
     * @var int 
     */
    public $product_id;
    /**
     *
     * @var string 
     */
    public $description;
    /**
     *
     * @var string 
     */
    public $shortDescription;
    /**
     *
     * @var string 
     */
    public $image;

    public $seoTitle;

    public $seoKeywords;

    public $seoDescription;
}
?>
