<?php


class Module_Shop_Model_Manage_Category {
    
    public $sortable_rules=array(
        'name'=>'turn',
        'type'=>'asc',
    );
    public $filter_rules=array(
        array(
            'name'=>'id',
            'format'=>'`t1`.`%1$s` = %2$s',
            'format_default'=>'`t1`.`%1$s` > "1"',
            ),
        
        array(
            'name'=>'name',
            'format'=>'`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
            ),
        array(
            'name'=>'parent_id',
            'format'=>'`t1`.`parent_id` LIKE "%3$s%2$s%3$s"',
            'format_default'=>'`t1`.`%1$s` > "0"',
            ),
        array(
            'name'=>'link',
            'format'=>'`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
            ),
        );
    
}
?>
