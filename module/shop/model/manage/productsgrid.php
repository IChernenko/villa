<?php


class Module_Shop_Model_Manage_Productsgrid {
    public $sortable_rules=array(
        'name'=>'`order`',
        'type'=>'asc',
    );
    public $filter_rules=array(
        
        array(
            'name'=>'name',
            'format'=>'`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
            ),
        array(
            'name'=>'link',
            'format'=>'`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
            ),
        array(
            'name'=>'code',
            'format'=>'`t1`.`%1$s` = "%2$s"'
            ),
        
        array(
            'name'=>'price',
            'format'=>'`t1`.`%1$s` = "%2$s"'
            ),
        array(
            'name'=>'category_id',
            'format'=>'`t1`.`%1$s` = "%2$s"'
            ),
        
        );
}
?>