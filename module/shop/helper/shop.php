<?php



/**
 * Description of shop
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Shop_Helper_Shop {
    
    const CATEGORY_ROOT = 1;
    
    /**
     * Возвращает папку, где лежат продукты
     * @param int $productId
     * @return string
     */
    public static function getProductsFolder($productId) {
        $uploadFolder = Dante_Lib_Config::get('app.uploadFolder');
        Dante_Lib_Log_Factory::getLogger()->debug("uploadFolder : $uploadFolder");

        $productsFolder = Dante_Lib_Config::get('shop.productsFolder');
        return "{$uploadFolder}/shop/{$productsFolder}/{$productId}/";
    }
    
    public static function getCategoriesFolder() 
    {
        $uploadFolder = Dante_Lib_Config::get('app.uploadFolder');
        $productsFolder = Dante_Lib_Config::get('shop.categoryFolder');
        return "{$uploadFolder}/shop/{$productsFolder}/";
    }

    public static function getCategoryIdFromUrl($url) {
        $pattern = '/^\/category\/(\d+)\-/';
        if(!preg_match_all($pattern, $url, $matches)) return false; // не можем определить категорию
        return (int)$matches[1][0];
    }

    public static function getProductIdFromUrl($url) {
        $pattern = '/^\/product\/(\d+)\-/';
        if(!preg_match_all($pattern, $url, $matches)) return false; // не можем определить категорию
        return (int)$matches[1][0];
    }
}

?>
