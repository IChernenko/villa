video = {
    init: function() {
        var swfUploadUrl = "/ajax.php?action=upload&controller=module.video.manage.controller&q=1&ctype=json";

        var params = {
            buttonPlaceholderId:    'btnVideoUpload',
            uploadUrl:              swfUploadUrl,
            uploadButton:           "../../../../component/swfupload/button.png",
            fileTypes: "*.mp4; *.mov; *.avi;",
            fileSizeLimit: "32 MB",
            progressBlock: 'uploadProgress',
            uploadComplete: function(obj, data)    {
                data =  JSON.parse(data);
                if (data.result == 0) {
                    alert(data.message);
                }
                if (data.result == 1) {
                    $('#addVideoParams').fadeIn();
                    $('#videoId').val(data.id);
                }
            }
        }
        initSwfUpload(params);
        
        //
        $("#video_items  a.video").each(function(index, elem) {
            flowplayer(elem.id, "../lib/flowplayer/js/flowplayer-3.2.7.swf", {clip: {
                autoPlay: false
            }} );
        });
        
        $('#btnVideoAdd').click(video.add);
    },
    
    initList: function() {
        $("#video_items  a.video").each(function(index, elem) {
            flowplayer(elem.id, "../lib/flowplayer/js/flowplayer-3.2.7.swf", {clip: {
                autoPlay: false
            }} );
        });
    },
    
    /**
     * Добавление видео
     */
    add: function(self) {
        self.disabled = true;
        
        $.ajax({
            type: "POST",
            url: '/ajax.php',
            data: {
                controller: "module.video.manage.controller",
                action: "add",
                id: $('#videoId').val(),
                name: $('#videoName').val(),
                description: $('#videoDescription').val()
            },
            success: function(data) {
                if (data.result == 0) {
                    $('#videoMessage').html(data.message);
                }
                if (data.result == 1) {
                    $('#addVideoParams').fadeOut();
                    
                    // Добавим видео
                    var tpl = "<li id='video_item_{{id}}'>"+
                                "<a href='{{src}}' class='video' id='video_{{id}}'></a>"+
                                "<a href='javascript:video.remove({{id}});'>Удалить</a>"+
                                "</li>";
                    var tplData = {
                      id: data.id,
                      src: data.src
                    };
                    var output = Mustache.render(tpl, data);
                    $('#video_items').append(output);
                    
                    flowplayer('video_'+data.id, "../lib/flowplayer/js/flowplayer-3.2.7.swf", {clip: {
                        autoPlay: false
                    }} );
                }
            }
        });
    },
    
    /**
     * Удаление video
     */
    remove: function(id) {
        $('#video_item_'+id).fadeOut();
        $.ajax({
            type: "POST",
            url: '/ajax.php',
            data: {
                controller: "module.video.manage.controller",
                action: "remove",
                id: id
            },
            success: function(data) {
                if (data.result == 1) {
                    if (data.id > 0) $('#video_item_'+data.id).remove();
                }
            }
        });
    }
}