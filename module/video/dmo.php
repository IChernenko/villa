<?php


/**
 * Description of dmo
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Video_Dmo extends Dante_Lib_Orm_Table{
    
    protected $_tableName = '[%%]video';
    
    /**
     *
     * @return Module_Audio_Dmo 
     */
    public static function dmo() {
        return self::getInstance();
    }

    /**
     *
     * @param type $id
     * @return Module_Audio_Dmo 
     */
    public function byId($id) {
        return $this->getByAttribute(array('id'=>$id));
    }
    
    /**
     *
     * @param type $id
     * @return Module_Audio_Dmo 
     */
    public function byUid($id) {
        return $this->getByAttribute(array('uid'=>$id));
    }
}

?>
