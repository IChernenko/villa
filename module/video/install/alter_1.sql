drop table if exists [%%]video;
CREATE TABLE [%%]video (
  `id`                  int(11) NOT NULL AUTO_INCREMENT,
  `uid`                 int(11),
  `file_name`           varchar(128),
  `adding_date`         int(11),  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 comment="видеозаписи";


ALTER TABLE [%%]video ADD
      CONSTRAINT fk_video_uid
      FOREIGN KEY (`uid`)
      REFERENCES [%%]users(id) ON DELETE CASCADE ON UPDATE CASCADE;
