<?php
global $package;
$package = array(
    'version' => '2',
    'name' => 'module.video',
    'description' => 'Модуль для работы с видео',
    'dependence' => array(
        'lib.flowplayer',
        'lib.mustache'
    ),
    'js' => array(
        '../module/video/js/video.js'
    )
);