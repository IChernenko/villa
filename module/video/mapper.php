<?php



/**
 * Description of mapper
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Video_Mapper {
    
    public function getByUid($uid) {
        return Module_Video_Dmo::dmo()->byUid($uid)->fetchAll('id');
    }
}

?>
