<?php
$url = Dante_Controller_Front::getRequest()->get_requested_url();

Dante_Lib_Router::addRule('/^\/manage\/video\/$/', array(
    'controller' => 'Module_Video_Manage_Controller'
));