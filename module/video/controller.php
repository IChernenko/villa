<?php



/**
 * Description of controller
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Video_Controller extends Dante_Controller_Base{
    
    protected function _defaultAction() {
        $uid = (int)Dante_Helper_App::getUid();
        if (!$uid) {
            $login = Radast_App_Helper::getLoginByPath();
            $userMapper = new Module_User_Mapper();
            $uid = $userMapper->getUidByLogin($login);
        }
        //if ($uid == 0) throw new Exception("неверный вызов");
        
        $tpl = new Dante_Lib_Template();
        
        $mapper = new Module_Video_Mapper();
        $items = $mapper->getByUid($uid);
        
        $tpl->items = $items;
        return $tpl->draw('../module/video/template/list.html');
    }
}

?>
