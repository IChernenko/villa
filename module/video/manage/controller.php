<?php



/**
 * Контроллер управления video
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Video_Manage_Controller extends Dante_Controller_Base{
    
    protected function _defaultAction() {
        $uid = (int)Dante_Helper_App::getUid();
        if ($uid == 0) return "неверный вызов";
        
        $tpl = new Dante_Lib_Template();
        
        $mapper = new Module_Video_Mapper();
        $items = $mapper->getByUid($uid);
        $tpl->items = $items;
        return $tpl->draw('../module/video/template/manage.html');
    }
    
    /**
     * Сохранение файла
     * @return type 
     */
    protected function _uploadAction() {
        
        Dante_Lib_Log_Factory::getLogger()->debug("upload called");
        
        $uid = (int)Dante_Helper_App::getUid();
        if ($uid == 0) throw new Exception("неверный вызов");
        
        if(!isset($_FILES['Filedata'])) throw new Exception("нечего загружать");
        
        $uploadFolder = Dante_Lib_Config::get('app.uploadFolder');
        
        $userFolder = "{$uploadFolder}/{$uid}";
        Dante_Lib_Log_Factory::getLogger()->debug("check folder : $userFolder");
        if (!is_dir($userFolder)) {
            Dante_Lib_Log_Factory::getLogger()->debug("create folder : $userFolder");
            mkdir($userFolder);
            @chmod($userFolder, 0755);
        }
        
        $uploadDir = "{$userFolder}/video";
        Dante_Lib_Log_Factory::getLogger()->debug("check folder : $uploadDir");
        if (!is_dir($uploadDir)) {
            Dante_Lib_Log_Factory::getLogger()->debug("create folder : $uploadDir");
            mkdir($uploadDir);
            @chmod($uploadDir, 0755);
        }

        $uploader = new Component_Swfupload_Upload();
        $fileName = $uploader->upload($uploadDir.'/', $uid.'-'.md5(time()));
        Dante_Lib_Log_Factory::getLogger()->debug("filename is : $fileName");
        
        // сохраним картинку
        $dmo = new Module_Video_Dmo();
        $dmo->uid           = $uid;
        $dmo->file_name     = $fileName;
        $dmo->adding_date   = time();
        $id = (int)$dmo->insert();

        return array(
            'result' => 1,
            'fileName' => $fileName,
            'id' => $id
        );
    }
    
    /**
     * Удаление аудиозаписи
     * @return array
     */
    protected function _removeAction() {
        $uid = (int)Dante_Helper_App::getUid();
        if ($uid == 0) throw new Exception("неверный вызов");
        
        $id = (int)$this->_getRequest()->get('id');
        
        // удалить файл с диска
        $dmo = Module_Video_Dmo::dmo()->byId($id)->byUid($uid)->fetch();
        $fileName = $dmo->file_name;
        if ($fileName) {
            $uploadFolder = Dante_Lib_Config::get('app.uploadFolder');
            $uploadPath = "{$uploadFolder}/{$uid}/video";
            Dante_Lib_Fs_File::delete($uploadPath.'/'.$fileName);
        }
        
        
        $dmo = new Module_Video_Dmo();
        $dmo->delete(array('id'=>$id, 'uid'=>$uid));
        return array(
            'result' => 1,
            'id' => $id
        );
    }
    
    /**
     * Добавление видео
     */
    protected function _addAction() {
        $uid = (int)Dante_Helper_App::getUid();
        if ($uid == 0) throw new Exception("неверный вызов");
        
        $id = (int)$this->_getRequest()->get('id');
        if ($id == 0) throw new Exception("Сначала загрузите видео файл");
        
        $dmo = Module_Video_Dmo::dmo()->byId($id)->byUid($uid)->fetch();
        if ($dmo->getNumRows() == 0) throw new Exception("Сначала загрузите видео файл");
        
        $name = $this->_getRequest()->get_validate('name', 'text');
        if (!$name) throw new Exception("Неверно задано поле наименование");
            
        $description = $this->_getRequest()->get_validate('description', 'text');
        $dmo->title = $name;
        $dmo->description = $description;
        $dmo->apply();
        
        $id = $dmo->id;
        
        $src = "http://radast.dante2.net.ua/media/{$id}/video/{$dmo->file_name}";
        
        return array(
            'result' => 1,
            'id' => $dmo->id,
            'name' => $dmo->file_name,
            'src' => $src
        );
    }
}

?>
