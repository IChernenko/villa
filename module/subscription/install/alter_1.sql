CREATE TABLE `[%%]subscription`(
    `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
    `email` VARCHAR(128) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;