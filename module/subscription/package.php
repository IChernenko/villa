<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 9/26/13
 * Time: 11:34 PM
 * To change this template use File | Settings | File Templates.
 */
global $package;
$package = array(
    'version' => '2',
    'name' => 'module.subscription',
    'description' => 'Модуль подписки',
    'dependence' => array(),
    'js' => array(
        '../module/subscription/js/subscription.js',
        '../module/subscription/js/subscriptionadmin.js'
    )
);