<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 9/27/13
 * Time: 12:18 AM
 * To change this template use File | Settings | File Templates.
 */

class Module_Subscription_Controller extends Dante_Controller_Base{
    protected function _subscribeAction() {
        $email = $this->_getRequest()->get('email');

        // todo: валидация емейл
        $table = new Dante_Lib_Orm_Table('[%%]subscription');
        $table->email = $email;
        // todo: проверка на сущестования записи
        $table->insert();
        return array(
            'result' => 1
        );
    }
}