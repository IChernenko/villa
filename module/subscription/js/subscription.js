/**
 * Created with JetBrains PhpStorm.
 * User: dorian
 * Date: 9/26/13
 * Time: 11:36 PM
 * To change this template use File | Settings | File Templates.
 */
subscription = {

    init: function() {
        $('#subscribeBtn').click(function(){
            subscription.subscribe($('#subscribeInput').val());
        });
    },

    subscribe: function(email) {
        dante.disableElem('#subscribeBtn');
        if (email.length == 0) {
            alert("Не задан email");
            return dante.enableElem('#subscribeBtn');
        }

        if (!dante.isValidEmail(email)) {
            alert("Введен некорректный email");
            return dante.enableElem('#subscribeBtn');
        }

        $.ajax({
            type: 'POST',
            data: {
                controller: 'module.subscription.controller',
                action: 'subscribe',
                email: email
            },
            url: '/ajax.php',
            dataType : "json",
            success: function (data){
                if (data.result == 1) {
                    alert("Вы успешно подписаны на рассылку");
                }
                dante.enableElem('#subscribeBtn');

            }
        });
        //alert("subscr : "+email);
    }
}
