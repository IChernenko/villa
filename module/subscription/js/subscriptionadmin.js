subscriptionadmin = {
    controller: 'module.subscription.manage.controller',
    
    btnDisable: function()
    {
        $('#handbookTable').find('a').each(function(){
            $(this).attr('disabled', true);
        });        
    },
    
    btnEnable: function()
    {
        $('#handbookTable').find('a').each(function(){
            $(this).attr('disabled', false);
        });        
    },
    
    editField: function(obj, id)
    {
        if($(obj).attr('disabled')) return false;
        this.btnDisable();
        var td = $(obj).parents('td').prev('td');
        var text = $(td).html();
        var html = '<input type="text" value="'+text+'" style="padding: 0 5px; width: 95%;"/>'+
            '<a class="btn btn-mini btn-success" style="margin-top: 5px;" onclick="subscriptionadmin.editSubmit(this, '+id+')">Сохранить</a>'+
            '<a class="btn btn-mini btn-warning" style="margin: 5px 0 0 5px;" onclick="subscriptionadmin.cancelEdit(this, '+"'"+text+"'"+')">Отмена</a>';
        $(td).html(html);
    },
    
    cancelEdit: function(obj, text)
    {
        if($(obj).attr('disabled')) return false;
        var td = $(obj).parents('td');
        $(td).html(text);
        this.btnEnable();
    },
    
    editSubmit: function(obj, id)
    {
        if($(obj).attr('disabled')) return false;
        this.btnDisable();
        
        var td = $(obj).parents('td')
        var email = $(td).find('input').val();
        
        var data = {
            controller: this.controller,
            action: 'edit',
            id: id,
            email: email
        }
        
        $.ajax({
            url: 'ajax.php',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(data){
                if(data.result == 1)
                {
                    $(td).html(email);
                    subscriptionadmin.btnEnable();
                }
                else
                {
                    console.log(data);
                }
            }
        });
    },
    
    delField: function(obj, id)
    {
        if($(obj).attr('disabled')) return false;
        
        if(!confirm('Подтвердите удаление записи'))
            return false;
        
        this.btnDisable();
        
        var data = {
            controller: this.controller,
            action: 'del',
            id: id
        }
        
        $.ajax({
            url: 'ajax.php',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(data){
                if(data.result == 1)
                {
                    $(obj).parents('tr').remove();
                    subscriptionadmin.btnEnable();
                }
                else
                {
                    console.log(data);
                }
            }
        });
    },
    
    addField: function(obj)
    {
        if($(obj).attr('disabled')) return false;
        
        var html = '<input type="text" placeholder="E-mail" style="padding: 0 5px; width: 70%;"/>'+
            '<a class="btn btn-mini btn-success" style="margin-left: 5px;" onclick="subscriptionadmin.addSubmit(this)">Сохранить</a>'+
            '<a class="btn btn-mini btn-warning" style="margin-left: 5px;" onclick="subscriptionadmin.cancelAdd(this)">Отмена</a>';
        $(obj).parents('td').html(html);
    },
    
    cancelAdd: function(obj)
    {
        if($(obj).attr('disabled')) return false;
        
        var html = '<a class="btn btn-success" onclick="subscriptionadmin.addField(this);">Добавить</a>';
        $(obj).parents('td').html(html);
    },
    
    addSubmit: function(obj)
    {        
        if($(obj).attr('disabled')) return false;
        this.btnDisable();
        
        var email = $(obj).parents('td').find('input').val();
        
        var data = {
            controller: this.controller,
            action: 'add',
            email: email
        }
        
        $.ajax({
            url: 'ajax.php',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(data){
                if(data.result == 1)
                {
                    handbooks.formSubmit();
                }
                else
                {
                    console.log(data);
                }
            }
        });
    }
}

