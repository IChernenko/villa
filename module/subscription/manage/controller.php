<?php 

class Module_Subscription_Manage_Controller extends Module_Handbooksgenerator_Controller_Base
{
    /**
     * отрисовка стартового html
     * @return string 
     */
    protected function _drawAction()
    {
        $helper = new Module_Division_Helper();
        $helper->checkAccess();
        
        $mapper = new Module_Subscription_Manage_Mapper();
        $model = new Module_Subscription_Manage_Model();
        $params = $this->getParams($model);
        
        $list = $mapper->getRowsByParams($params);
        
        $this->formEnable('module.subscription.manage.controller');
        
        $titles = array(
            array(
                'content' => '#',
                'sortable' => 'id'
            ),            
            array(
                'content' => 'E-mail',
                'sortable' => 'email',
                'params' => array(
                    'style' => array(
                        'width' => '50%'
                    )
                )
            ), 
            array(
                'content' => 'Функции',
                'params' => array(
                    'style' => array(
                        'width' => '30%'
                    )
                )
            )   
        );
        
        $this->thead = array($titles);
        
        foreach($list['rows'] as $row)
        {
            $cur = array();            
            $cur['content']['id'] = $row['id'];
            $cur['content']['email'] = $row['email'];        
            $cur['content']['buttons']['content'] = '<a class="btn btn-mini btn-info" style="margin-left: 5px;" 
                onclick="subscriptionadmin.editField(this, '.$row['id'].');">Редактировать</a>';   
            $cur['content']['buttons']['content'] .= '<a class="btn btn-mini btn-danger" style="margin-left: 5px;"
                onclick="subscriptionadmin.delField(this, '.$row['id'].');">Удалить</a>';    
                
            $this->tbody['content'][$row['id']]=$cur;
        }        
        
        $this->tfoot=array(  
            'fields'=>array(
                '<a class="btn btn-success" onclick="subscriptionadmin.addField(this);">Добавить</a>'                  
            ),          
            'paginator' => array(
                array(
                    'content' => $this->paginator($list['rowCount'], $params)
                )
            )                           
        );
        
        $html = '<style> .table{width: 600px;}</style>'.$this->generate();
        
        return $html;
    }
    
    protected function _addAction()
    {
        $email = $this->_getRequest()->get('email');
        
        $mapper = new Module_Subscription_Manage_Mapper();
        $mapper->add($email);
        
        return array('result'=>1);
    }
    
    protected function _editAction()
    {
        $id = $this->_getRequest()->get('id');
        $email = $this->_getRequest()->get('email');
        
        $mapper = new Module_Subscription_Manage_Mapper();
        $email = $mapper->edit($id, $email);
        
        return array(
            'result' => 1,
            'email' => $email
        );
    }
    
    protected function _delAction()
    {
        $id = $this->_getRequest()->get('id');
        
        $mapper = new Module_Subscription_Manage_Mapper();
        $mapper->del($id);
        
        return array('result'=>1);
    }
}

?>