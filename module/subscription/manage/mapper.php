<?php

class Module_Subscription_Manage_Mapper extends Module_Handbooksgenerator_Mapper_Base
{
    protected $_tableName = '[%%]subscription';        
    
    public function add($email)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->email = $email;
        
        return $table->insert();
    }
    
    public function edit($id, $email)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->email = $email;
        
        $table->apply(array('id'=>$id));
        return $email;
    }
}

?>
