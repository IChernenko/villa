drop table if exists [%%]audio;
CREATE TABLE [%%]audio (
  `id`                  int(11) NOT NULL AUTO_INCREMENT,
  `uid`                 int(11),
  `file_name`           varchar(128),
  `adding_date`         int(11),  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 comment="аудиозаписи";


ALTER TABLE [%%]audio ADD
      CONSTRAINT fk_audio_uid
      FOREIGN KEY (`uid`)
      REFERENCES [%%]users(id) ON DELETE CASCADE ON UPDATE CASCADE;
