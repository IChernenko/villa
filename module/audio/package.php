<?php
global $package;
$package = array(
    'version' => '2',
    'name' => 'module.audio',
    'description' => 'Модуль для работы с аудио',
    'dependence' => array(),
    'js' => array(
        'http://api.html5media.info/1.1.5/html5media.min.js',
        '../module/audio/js/audio.js'
    )
);