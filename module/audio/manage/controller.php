<?php



/**
 * Контроллер управления аудио
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Audio_Manage_Controller extends Dante_Controller_Base{
    
    protected function _defaultAction() {
        $uid = (int)Dante_Helper_App::getUid();
        if ($uid == 0) return "Вы должны быть залогинены";
        
        $tpl = new Dante_Lib_Template();
        
        $mapper = new Module_Audio_Mapper();
        $items = $mapper->getByUid($uid);
        $tpl->items = $items;
        $tpl->sid = Dante_Helper_App::getSid();
        return $tpl->draw('../module/audio/template/manage.html');
    }
    
    /**
     * Сохранение файла
     * @return type 
     */
    protected function _uploadAction() {
        $sid = $this->_getRequest()->get('sid', false);
        
        Dante_Lib_Log_Factory::getLogger()->debug("upload called sid : {$sid}");
        
        //Dante_Lib_Log_Factory::getLogger()->debug("post : ".var_export($_POST, true));
        //Dante_Lib_Log_Factory::getLogger()->debug("files : ".var_export($_FILES, true));
        if ($sid) {
            $mapper = new Module_Session_Mapper();
            $uid = $mapper->getUidBySid($sid);
        }
        else {
            $uid = (int)Dante_Helper_App::getUid();
            if ($uid == 0) throw new Exception("неверный вызов");
        }
        
        if(!isset($_FILES['Filedata'])) throw new Exception("нечего загружать");
        
        $uploadFolder = Dante_Lib_Config::get('app.uploadFolder');
        
        $userFolder = "{$uploadFolder}/{$uid}";
        Dante_Lib_Log_Factory::getLogger()->debug("check folder : $userFolder");
        if (!is_dir($userFolder)) {
            Dante_Lib_Log_Factory::getLogger()->debug("create folder : $userFolder");
            mkdir($userFolder);
            @chmod($userFolder, 0755);
        }
        
        $uploadDir = "{$userFolder}/audio";
        Dante_Lib_Log_Factory::getLogger()->debug("check folder : $uploadDir");
        if (!is_dir($uploadDir)) {
            Dante_Lib_Log_Factory::getLogger()->debug("create folder : $uploadDir");
            mkdir($uploadDir);
            @chmod($uploadDir, 0755);
        }

        $uploader = new Component_Swfupload_Upload();
        $fileName = $uploader->upload($uploadDir.'/');
        Dante_Lib_Log_Factory::getLogger()->debug("filename is : $fileName");
        
        // сохраним картинку
        $dmo = new Module_Audio_Dmo();
        $dmo->uid = $uid;
        $dmo->file_name = $fileName;
        $dmo->adding_date = time();
        $id = $dmo->insert();
        


        return array(
            'result' => 1,
            'fileName' => $fileName,
            'id' => $id
        );
    }
    
    /**
     * Удаление аудиозаписи
     * @return array
     */
    protected function _removeAction() {
        $uid = (int)Dante_Helper_App::getUid();
        if ($uid == 0) throw new Exception("неверный вызов");
        
        $id = (int)$this->_getRequest()->get('id');
        
        // удалить файл с диска
        $dmo = Module_Audio_Dmo::dmo()->byId($id)->byUid($uid)->fetch();
        $fileName = $dmo->file_name;
        if ($fileName) {
            $uploadFolder = Dante_Lib_Config::get('app.uploadFolder');
            $uploadPath = "{$uploadFolder}/{$uid}/audio";
            Dante_Lib_Fs_File::delete($uploadPath.'/'.$fileName);
        }
        
        
        $dmo = new Module_Audio_Dmo();
        $dmo->delete(array('id'=>$id, 'uid'=>$uid));
        return array(
            'result' => 1,
            'id' => $id
        );
    }
    
    /**
     * Обновление аудиозаписи
     */
    protected function _updateAction() {
        $uid = (int)Dante_Helper_App::getUid();
        if ($uid == 0) throw new Exception("неверный вызов");
        
        $id = (int)$this->_getRequest()->get('id');
        $title = $this->_getRequest()->get('title');
        $description = $this->_getRequest()->get('description');
        
        $dmo = new Module_Audio_Dmo();
        $dmo->title = $title;
        $dmo->description = $description;
        $dmo->update(array(
            'id' => $id,
            'uid' => $uid
        ));
        
        $dmo->byId($id)->fetch();
        $fileName = 'media/'.$uid.'/audio/'.$dmo->file_name;
        
        return array(
            'result' => 1,
            'id' => $id,
            'src' => $fileName
        );
    }
}

?>
