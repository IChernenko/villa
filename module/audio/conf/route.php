<?php
$url = Dante_Controller_Front::getRequest()->get_requested_url();

Dante_Lib_Router::addRule('/^\/manage\/audio\/$/', array(
    'controller' => 'Module_Audio_Manage_Controller'
));