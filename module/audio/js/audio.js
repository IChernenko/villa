audio = {
    init: function(sid) {
        var swfUploadUrl = "/ajax.php?action=upload&controller=module.audio.manage.controller&q=1&ctype=json&sid="+sid;

        var params = {
            buttonPlaceholderId:    'btnAudioUpload',
            uploadUrl:              swfUploadUrl,
            uploadButton:           "../../../../component/swfupload/button.png",
            fileTypes: "*.mp3",
            fileTypesDescription: "Music",
            fileSizeLimit: "32 MB",
            progressBlock: 'uploadProgress',
            uploadComplete: function(obj, data)    {
                data =  JSON.parse(data);
                
                if (data.result == 0) {
                    alert(data.message);
                }
                
                if (data.result == 1) {
                    $('#audio-id').val(data.id);
                    $('#audio-manage-form').fadeIn();
                }
            }
        }
        initSwfUpload(params);
    },
    
    /**
     * Обновление аудиозаписи
     */
    update: function() {
        $('#audio-manage-form').fadeOut();
        $.ajax({
            type: "POST",
            url: '/ajax.php',
            data: {
                controller: "module.audio.manage.controller",
                action: "update",
                id: $('#audio-id').val(),
                title: $('#audio-title').val(),
                description: $('#audio-description').val()
            },
            success: function(data) {
                if (data.result == 1) {
                    
                    var tpl = "<li id=\"audio_item_{{id}}\">"+
                        "<audio src=\"{{src}}\" controls preload></audio>"+
                        "<button onclick=\"javascript:audio.remove({{id}});\" class=\"btn btn-danger\">Удалить</button>"+
                    "</li>";
                
                    var tplData = {
                      id: data.id,
                      src: data.src
                    };
                    var output = Mustache.render(tpl, data);
                    $('#audio_items').append(output);
                    window.html5media.createFallback('audio', $("#audio_item_"+data.id+" audio").get());
                }
            }
        });
    },
    
    /**
     * Удаление аудиозаписи
     */
    remove: function(id) {
        $('#audio_item_'+id).fadeOut();
        $.ajax({
            type: "POST",
            url: '/ajax.php',
            data: {
                controller: "module.audio.manage.controller",
                action: "remove",
                id: id
            },
            success: function(data) {
                if (data.result == 1) {
                    if (data.id > 0) $('#audio_item_'+data.id).remove();
                }
            }
        });
    }
}