<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dmo
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Audio_Dmo extends Dante_Lib_Orm_Table{
    
    protected $_tableName = '[%%]audio';
    
    /**
     *
     * @return Module_Audio_Dmo 
     */
    public static function dmo() {
        return self::getInstance();
    }


    public function byId($id) {
        return $this->getByAttribute(array('id'=>$id));
    }
    
    public function byUid($id) {
        return $this->getByAttribute(array('uid'=>$id));
    }
}

?>
