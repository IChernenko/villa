<?php

Dante_Lib_Router::addRule('/^\/interkassa2\/test$/', array(
    'controller' => 'Module_Interkassa2_Controller_Test'
));

Dante_Lib_Router::addRule('/^\/interkassa2\/test\/status$/', array(
    'controller' => 'Module_Interkassa2_Controller_Test',
    'action' => 'status'
));



Dante_Lib_Router::addRule('/^\/interkassa2\/success/', array(
    'controller' => 'Module_Interkassa2_Controller',
    'action' => 'success'
));

// статус платежа
Dante_Lib_Router::addRule('/^\/interkassa2\/pending/', array(
    'controller' => 'Module_Interkassa2_Controller',
    'action' => 'pending'
));

Dante_Lib_Router::addRule('/^\/interkassa2\/fail/', array(
    'controller' => 'Module_Interkassa2_Controller',
    'action' => 'fail'
));

Dante_Lib_Router::addRule('/^\/interkassa2\/interaction/', array(
    'controller' => 'Module_Interkassa2_Controller',
    'action' => 'interaction'
));