<?php
/**
 * Created by PhpStorm.
 * User: dorian
 * Date: 3/16/14
 * Time: 10:20 AM
 */

//namespace tests;


class InteractionTest extends PHPUnit_Framework_TestCase
{

    protected function _getData() {
        $data = array (
            'ik_co_id' => '531eca1dbf4efcfc54cc6917',
            'ik_co_prs_id' => '204932947957',
            'ik_inv_id' => '26311295',
            'ik_inv_st' => 'success',
            'ik_inv_crt' => '2014-03-16 10:12:42',
            'ik_inv_prc' => '2014-03-16 10:14:11',
            'ik_trn_id' => '502410',
            'ik_pm_no' => '1394957550',
            'ik_desc' => 'Поезд Вебрадаст - ввод средств 0.1 долларов через Интеркасса от пользователя radast1',
            'ik_pw_via' => 'privat24_privat24_merchant_usd',
            'ik_am' => '1.00',
            'ik_cur' => 'USD',
            'ik_co_rfn' => '1.0000',
            'ik_ps_price' => '1.20',
            'ik_x_action' => 'moneyIn',
            'ik_x_login' => 'radast1',
            'ik_sign' => 'KRk/ctVZjYGnnjljy3GGxQ==',
        );

        return $data;
    }

    /**
     * Тестирование алгоритма формирования цифровой подписи
     */
    public function testSign() {
        $data = $this->_getData();
        $sign = $data['ik_sign'];

        $service = new Module_Interkassa2_Service();
        $generatedSign = $service->generateSign($data);
        //echo("generatedSign: $generatedSign");

        $this->assertEquals($sign, $generatedSign);
    }

    /**
     * Тест регистрации пользователя
     */
    public function testInteraction()
    {
        $login = 'radast1';
        $userMapper = new Webradasttrain_App_User_Mapper();
        $uid = $userMapper->getUidByLogin($login);
        $balanceBefore = $userMapper->getBalance($uid);

        $url =  Dante_Lib_Config::get('url.domain_main').'/interkassa2/interaction';
        //echo($url);

        $fields = http_build_query($this->_getData());


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $fields);
        $out = curl_exec($curl);
        //echo $out;
        curl_close($curl);

        $balanceAfter = $userMapper->getBalance($uid);


        $result = true;

        /*$service = new Module_Interkassa2_Service();
        $result = $service->interaction($this->_getData());
        */
        $this->assertEquals($balanceAfter, $balanceBefore + 1);

    }

}