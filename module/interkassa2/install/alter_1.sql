CREATE TABLE IF NOT EXISTS [%%]interkassa2_log (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ik_co_id` varchar(36) NOT NULL comment 'идентификатор кассы',
  `ik_co_prs_id` varchar(64) NOT NULL comment 'Идентификатор кошелька кассы',
  `ik_inv_id` varchar(64) NOT NULL comment 'Идентификатор платежа',
  `ik_inv_st` varchar(16) NOT NULL comment 'Состоание платежа',
  `ik_inv_crt` datetime NOT NULL comment 'Invoice Created',
  `ik_inv_prc` datetime NOT NULL comment 'Invoice Processed',
  `ik_trn_id` varchar(16) NOT NULL comment 'Идентификатор транзакции',
  `ik_pm_no` varchar(32) NOT NULL comment 'Номер платежа',
  `ik_desc` tinytext comment 'Payment Description',
  `ik_pw_via` varchar(62) comment 'Выбранный способ оплаты',
  `ik_am` decimal(10,2) comment 'Сумма платежа',
  `ik_cur` varchar(3) comment 'Валюта платежа',
  `ik_co_rfn` decimal(10,4) comment 'Сумма зачисления на счет кассы',
  `ik_ps_price` decimal(10, 2) comment 'Сумма платежа в платежной системе',
  `ik_sign` varchar(24) comment 'Цифровая подпись',

  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 comment 'Логи от интеркассы';