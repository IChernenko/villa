<?php
/**
 * Created by PhpStorm.
 * User: dorian
 * Date: 3/17/14
 * Time: 1:17 AM
 */

/**
 * Стратегия обработки интеркассы
 * Class Module_Interkassa2_Strategy
 */
class Module_Interkassa2_Strategy {

    /**
     * Вызывается при успешной операции в interaction
     * @param array $data
     * @return bool
     */
    public function onInteractionSuccess($data) {
        // бизнесс логика
        return true;
    }

    /**
     * просто отображение успешной страницы
     * @param $data
     * @return string
     */
    public function onSuccess($data) {
        return 'Ваш платеж проведен успешно';
    }

    public function onPending($data) {
        return 'Ваш платеж находится в обработке';
    }

    public function onFail($data) {
        return 'Ваш платеж завершился неудачно';
    }
} 