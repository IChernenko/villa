<?php
/**
 * Created by PhpStorm.
 * User: dorian
 * Date: 3/14/14
 * Time: 7:30 PM
 */

Dante_Lib_Config::set('interkassa2.interactionUrl',  Dante_Lib_Config::get('url.domain_main').'/interkassa2/interaction');
Dante_Lib_Config::set('interkassa2.successUrl',  Dante_Lib_Config::get('url.domain_main').'/interkassa2/success');
Dante_Lib_Config::set('interkassa2.pendingUrl',  Dante_Lib_Config::get('url.domain_main').'/interkassa2/pending');
Dante_Lib_Config::set('interkassa2.failUrl',  Dante_Lib_Config::get('url.domain_main').'/interkassa2/fail');

Dante_Lib_Config::set('interkassa2.secretKey',  'secretKey');

Dante_Lib_Config::set('interkassa2.strategyClassName',  'Module_Interkassa2_Strategy');