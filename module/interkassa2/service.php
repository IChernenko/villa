<?php
/**
 * Created by PhpStorm.
 * User: dorian
 * Date: 3/14/14
 * Time: 12:50 PM
 */

/**
 * Сервис для работы с Интеркассой версии 2
 * Class Module_Interkassa2_Service
 */
class Module_Interkassa2_Service {

    /**
     * Класс стратегии
     * @var Module_Interkassa2_Strategy
     */
    protected $_strategy;

    function __construct() {
        $strategyClassName = Dante_Lib_Config::get('interkassa2.strategyClassName');
        if (!class_exists($strategyClassName)) throw new Exception("Не определен класс стратегии $strategyClassName");

        $this->_strategy = new $strategyClassName();
    }

    public function getStrategy() {
        return $this->_strategy;
    }

    /**
     * Фомирует форму для оплаты через интеркассу
     * @param $params
     * <code>
     *  array(
     *      'amount' => float
     *      'id' => int
     *      'description'
     *      'ik_baggage_fields'
     *      'payBtnCaption'
     * )
     * </code>
     * @return mixed|string
     */
    public static function drawForm($params) {

        if (!isset($params['currency'])) $params['currency'] = 'USD';
        if (!isset($params['interactionMethod'])) $params['interactionMethod'] = 'POST';
        if (!isset($params['successMethod'])) $params['successMethod'] = 'POST';
        if (!isset($params['pendingMethod'])) $params['pendingMethod'] = 'POST';
        if (!isset($params['failMethod'])) $params['failMethod'] = 'POST';
        if (!isset($params['customFields'])) $params['customFields'] = array();
        if (!isset($params['user'])) $params['user'] = '';
        if (!isset($params['interactionUrl'])) $params['interactionUrl'] = Dante_Lib_Config::get('interkassa2.interactionUrl');
        if (!isset($params['successUrl'])) $params['successUrl'] = Dante_Lib_Config::get('interkassa2.successUrl');
        if (!isset($params['pendingUrl'])) $params['pendingUrl'] = Dante_Lib_Config::get('interkassa2.pendingUrl');
        if (!isset($params['failUrl'])) $params['failUrl'] = Dante_Lib_Config::get('interkassa2.failUrl');

        $tpl = new Dante_Lib_Template();
        $tpl->ik_co_id      = Dante_Lib_Config::get('interkassa2.co_id'); // Идентификатор кассы.
        $tpl->ik_am         = $params['amount']; // Сумма платежа
        $tpl->ik_pm_no      = $params['id']; // Номер платежа.
        $tpl->ik_desc       = $params['description']; // описание платежа
        $tpl->ik_cur        = $params['currency']; // валюта  платежа
        $tpl->ik_usr        = $params['user']; // Контактная информация клиента.
        $tpl->ik_ia_u       = $params['interactionUrl']; // URL страницы взаимодействия.
        $tpl->ik_ia_m       = $params['interactionMethod']; // Метод запроса страницы взаимодействия.
        $tpl->ik_suc_u      = $params['successUrl']; // URL страницы проведенного платежа
        $tpl->ik_suc_m      = $params['successMethod']; // POST, GET
        $tpl->ik_pnd_u      = $params['pendingUrl']; // URL страницы ожидания проведения платежа.
        $tpl->ik_pnd_m      = $params['pendingMethod']; // POST, GET
        $tpl->ik_fal_u      = $params['failUrl']; // URL страницы непроведенного платежа.
        $tpl->ik_fal_m      = $params['failMethod']; // Метод запроса страницы непроведенного платежа.
        $tpl->customFields = $params['customFields'];
        $tpl->payBtnCaption = $params['payBtnCaption'];
        $tpl->payBtnClass   = $params['payBtnClass'];


        //var_dump($tpl->customFilelds); die();



        return $tpl->draw('../module/interkassa2/template/form.html');
    }

    /**
     * Обработка данных от интеркассы
     * @param $data
     */
    public function interaction($data) {
        $kassaId = $data['ik_co_id'];
        if ($kassaId != Dante_Lib_Config::get('interkassa2.co_id')) {
            throw new Exception("Несовпадение номера кассы");
        }

        if (!isset($data['ik_sign'])) throw new Exception("Отсутствует цифровая подпись");
        $sign = $data['ik_sign'];

        $generatedSign = $this->generateSign($data);
        if ($generatedSign != $sign) throw new Exception("Подписи не совпадают");

        // сохранить лог
        $this->_saveData($data);

        $paymentNumber = $data['ik_pm_no'];

        // @todo: проверить сумму платежа
        $amount = $data['ik_am'];

        //  проверить состояние платежа
        $state = $data['ik_inv_st'];
        if ($state == 'success') {
            // вызвать стратегию
            return $this->getStrategy()->onInteractionSuccess($data);
        }
    }

    /**
     * Обработка случая успешного платежа
     * @param $data
     * @return string
     */
    public function onSuccess($data) {
        return $this->getStrategy()->onSuccess($data);
    }

    /**
     * Обработка случая платежа в ожидании
     * @param $data
     * @return string
     */
    public function onPending($data) {
        return $this->getStrategy()->onPending($data);
    }

    /**
     * Обработка случая неудачного платежа
     * @param $data
     * @return string
     */
    public function onFail($data) {
        return $this->getStrategy()->onFail($data);
    }

    /**
     * Сохраняет данные от интеркассы в лог
     * @param $data
     */
    protected function _saveData($data) {
        $orm = new Dante_Lib_Orm_Table('[%%]interkassa2_log');
        $orm->ik_co_id      = $data['ik_co_id'];        // идентификатор кассы
        $orm->ik_co_prs_id  = $data['ik_co_prs_id'];    // Идентификатор кошелька кассы.
        $orm->ik_inv_id     = $data['ik_inv_id'];       // Идентификатор платежа.
        $orm->ik_inv_st     = $data['ik_inv_st'];       // состояние платежа
        $orm->ik_inv_crt    = $data['ik_inv_crt'];      // Invoice Created
        $orm->ik_inv_prc    = $data['ik_inv_prc'];      // Invoice Processed
        $orm->ik_trn_id     = $data['ik_trn_id'];       // Идентификатор транзакции.
        $orm->ik_pm_no      = $data['ik_pm_no'];        // Номер платежа
        $orm->ik_desc       = $data['ik_desc'];         // назначение платежа
        $orm->ik_pw_via     = $data['ik_pw_via'];       // Выбранный способ оплаты.
        $orm->ik_am         = $data['ik_am'];           // сумма платежа
        $orm->ik_cur        = $data['ik_cur'];          // валюта платежа
        $orm->ik_co_rfn     = $data['ik_co_rfn'];       // Сумма зачисления на счет кассы
        $orm->ik_ps_price   = $data['ik_ps_price'];     // Сумма платежа в платежной системе.
        $orm->ik_sign       = $data['ik_sign'];         // подпись

        return $orm->insert();
    }

    /**
     * Формирование цифровой подписи
     * @param $dataSet
     * @param $secretKey
     * @return string
     */
    public function generateSign($dataSet, $secretKey=false) {
        if (isset($dataSet['ik_sign'])) unset($dataSet['ik_sign']);

        if (!$secretKey) $secretKey = Dante_Lib_Config::get('interkassa2.secretKey');

        ksort($dataSet, SORT_STRING); // сортируем по ключам в алфавитном порядке элементы массива
        array_push($dataSet, $secretKey); // добавляем в конец массива "секретный ключ"
        $signString = implode(':', $dataSet); // конкатенируем значения через символ ":"
        $sign = base64_encode(md5($signString, true)); // берем MD5 хэш в бинарном виде по сформированной строке и кодируем в BASE64
        return $sign; // возвращаем результат
    }
}