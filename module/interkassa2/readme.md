# Пример использования

## 1. Отрисовка формы оплаты через Интеркассу


    $form = Module_Interkassa2_Service::drawForm(array(
        'amount'                => $amount,
        'id'                    => time(),
        'description'           => "Поезд Вебрадаст - ввод средств {$amount} долларов через Интеркасса от пользователя {$user->login}",
        'customFields'          => array(
            'action' => 'moneyIn',
            'login' => $user->login
        ),
        'payBtnCaption'         => 'Ввести средства',
        'payBtnClass'           => 'btn'
    ));
    


## 2. Переорпеделение базовой стратегии

Нужно добавить свою бизнесс-логику.


class ${your-project}_App_Interkassa2_Strategy extends Module_Interkassa2_Strategy {
    public function onInteractionSuccess($data) {
        // бизнесс логика
        return true;
    }
}
