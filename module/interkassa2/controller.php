<?php
/**
 * Created by PhpStorm.
 * User: dorian
 * Date: 3/15/14
 * Time: 5:33 PM
 */

/**
 * Контроллер для обработки событий от интеркассы
 *
 * Class Module_Interkassa2_Controller
 */
class Module_Interkassa2_Controller extends Dante_Controller_Base {

    /**
     * @var Module_Interkassa2_Service
     */
    protected $_service;

    protected function _init() {
        $this->_service = new Module_Interkassa2_Service();
    }

    /**
     * @return Module_Interkassa2_Service
     */
    protected function _getService() {
        return $this->_service;
    }

    protected function _getData() {
        return $_POST;
    }

    /**
     * Обработка interaction действия от интеркассы
     * @return bool
     */
    protected function _interactionAction() {
        $data  = $this->_getData();
        Dante_Lib_Log_Factory::getLogger()->debug("Interkassa2: interaction");
        Dante_Lib_Log_Factory::getLogger()->debug_item($data);

        return $this->_getService()->interaction($data);
    }

    protected function _successAction() {
        $data  = $this->_getData();
        Dante_Lib_Log_Factory::getLogger()->debug("Interkassa2: success");
        Dante_Lib_Log_Factory::getLogger()->debug_item($data);

        return $this->_getService()->onSuccess($data);
    }

    protected function _pendingAction() {
        $data  = $this->_getData();
        Dante_Lib_Log_Factory::getLogger()->debug("Interkassa2: pending");
        Dante_Lib_Log_Factory::getLogger()->debug_item($data);
        return $this->_getService()->onPending($data);
    }

    protected function _failAction() {
        $data  = $this->_getData();
        Dante_Lib_Log_Factory::getLogger()->debug("Interkassa2: fail");
        Dante_Lib_Log_Factory::getLogger()->debug_item($data);
        return $this->_getService()->onFail($data);
    }
}
/*
Interkassa2: interaction
[16.03.2014 10:14] ip: 85.10.225.99 array (
    'ik_co_id' => '531eca1dbf4efcfc54cc6917', // идентификатор кассы
    'ik_co_prs_id' => '204932947957',  // Идентификатор кошелька кассы.
    'ik_inv_id' => '26311295', // Идентификатор платежа.
    'ik_inv_st' => 'success', // состояние платежа
    'ik_inv_crt' => '2014-03-16 10:12:42',  // Invoice Created
    'ik_inv_prc' => '2014-03-16 10:14:11', // Invoice Processed
    'ik_trn_id' => '502410', // Идентификатор транзакции.
    'ik_pm_no' => '1394957550',  // Номер платежа
    'ik_desc' => '?^?оезд ?^?еб?^?ада?^??^? - ввод ?^??^?ед?^??^?в 0.1 долла?^?ов ?^?е?^?ез ?^?н?^?е?^?ка?^??^?а о?^? пол?^?зова?^?ел?^? radast$
  'ik_pw_via' => 'privat24_privat24_merchant_usd',  // Выбранный способ оплаты.
  'ik_am' => '0.10', // сумма платежа
  'ik_cur' => 'USD',
  'ik_co_rfn' => '0.1000',  // Сумма зачисления на счет кассы
  'ik_ps_price' => '0.12', // Сумма платежа в платежной системе.

  'ik_x_action' => 'moneyIn',
  'ik_x_login' => 'radast1',
  'ik_sign' => '7XAQsaFNPoKBvABvbAUKmA==',
)
*/