<?php
/**
 * Description of mapper
 *
 * @author Admin
 */
class Module_Paypal_Mapper extends Module_Handbooksgenerator_Mapper_Base
{    
    protected $_tableName = '[%%]paypal_log';
    /**
     * добавление отчета
     * @param Module_Paypal_Model $paypallModel 
     */
    public function add(Module_Paypal_Model $paypallModel) 
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        
        $table->payment_type = $paypallModel->payment_type;
        $table->payment_date = $paypallModel->payment_date;
        $table->payment_status = $paypallModel->payment_status;
        $table->address_status = $paypallModel->address_status;
        $table->payer_status = $paypallModel->payer_status;
        $table->first_name = $paypallModel->first_name;
        $table->last_name = $paypallModel->last_name;
        $table->payer_email = $paypallModel->payer_email;
        $table->payer_id = $paypallModel->payer_id;
        $table->address_name = $paypallModel->address_name;
        $table->address_country = $paypallModel->address_country;
        $table->address_country_code = $paypallModel->address_country_code;
        $table->address_zip = $paypallModel->address_zip;
        $table->address_state = $paypallModel->address_state;
        $table->address_city = $paypallModel->address_city;
        $table->address_street = $paypallModel->address_street;
        $table->business = $paypallModel->business;
        $table->receiver_email = $paypallModel->receiver_email;
        $table->receiver_id = $paypallModel->receiver_id;
        $table->residence_country = $paypallModel->residence_country;
        $table->item_name = $paypallModel->item_name;
        $table->item_number = $paypallModel->item_number;
        $table->quantity = $paypallModel->quantity;
        $table->shipping = $paypallModel->shipping;
        $table->tax = $paypallModel->tax;
        $table->mc_currency = $paypallModel->mc_currency;
        $table->mc_fee = $paypallModel->mc_fee;
        $table->mc_gross = $paypallModel->mc_gross;
        $table->txn_type = $paypallModel->txn_type;
        $table->txn_id = $paypallModel->txn_id;
        $table->notify_version = $paypallModel->notify_version;
        $table->custom = $paypallModel->custom;
        $table->invoice = $paypallModel->invoice;
        $table->charset = $paypallModel->charset;
        $table->verify_sign = $paypallModel->verify_sign;

        $paypallModel->id = $table->insert();
    }  
    
    public function getStatusByItemNum($itemNum)
    {
        $sql = "SELECT `payment_status` FROM {$this->_tableName} WHERE `order_id` = '{$itemNum}'
                ORDER BY `time` DESC LIMIT 1";
        return Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'payment_status');
    }
}

?>
