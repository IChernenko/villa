<?php
/**
 * Description of view
 *
 * @author Valkyria
 */
class Module_Paypal_View 
{
    public function draw($params, $items = false)
    {
        $fileName = '../module/paypal/template/form.html';
        
        $customTpl = Dante_Helper_App::getWsPath().'/paypal/form.html';
        if (file_exists($customTpl)) $fileName = $customTpl;
        
        $tpl = new Dante_Lib_Template();
        
        $tpl->params = $params;
        $tpl->items = $items;
        
        return $tpl->draw($fileName);
    }
}

?>
