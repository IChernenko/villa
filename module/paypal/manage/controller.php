<?php
/**
 * Description of controller
 *
 * @author Mort
 */
class Module_Paypal_Manage_Controller  extends Module_Handbooksgenerator_Controller_Base 
{    
    protected function _drawAction()
    {
        $helper = new Module_Division_Helper();
        $helper->checkAccess();
        
        $mapper = new Module_Paypal_Mapper();
        $model = new Module_Paypal_Manage_Model();
        
        $params = $this->getParams($model);
        $list = $mapper->getRowsByParams($params);
        
        $this->formEnable('module.paypal.manage.controller');
        $this->_setHead();
        $this->_setBody($list['rows']);
        
        $this->tfoot = array(
            array($this->paginator($list['count'], $params))      
        );
        
        return $this->generate();
    }    
    
    protected function _setHead()
    {        
        $titles = array(
            array(
                'content' => '#',
                'sortable' => 'id',
                'params' => array(
                    'style' => array(
                        'width' => '90px'
                    )
                )
            ),
            array('content' => 'payment_type', 'sortable' => 'payment_type'),
            array('content' => 'payment_date', 'sortable' => 'payment_date'),
            array('content' => 'payment_status', 'sortable' => 'payment_status'),
            array('content' => 'address_status', 'sortable' => 'address_status'),
            array('content' => 'payer_status', 'sortable' => 'payer_status'),
            array('content' => 'first_content', 'sortable' => 'first_content'),
            array('content' => 'last_content', 'sortable' => 'last_content'),
            array('content' => 'payer_email', 'sortable' => 'payer_email'),
            array('content' => 'payer_id', 'sortable' => 'payer_id'),
            array('content' => 'address_content', 'sortable' => 'address_content'),
            array('content' => 'address_country', 'sortable' => 'address_country'),
            array('content' => 'address_country_code', 'sortable' => 'address_country_code'),
            array('content' => 'address_zip', 'sortable' => 'address_zip'),
            array('content' => 'address_state', 'sortable' => 'address_state'),
            array('content' => 'address_city', 'sortable' => 'address_city'),
            array('content' => 'address_street', 'sortable' => 'address_street'),
            array('content' => 'business', 'sortable' => 'business'),
            array('content' => 'receiver_email', 'sortable' => 'receiver_email'),
            array('content' => 'receiver_id', 'sortable' => 'receiver_id'),
            array('content' => 'residence_country', 'sortable' => 'residence_country'),
            array('content' => 'item_content', 'sortable' => 'item_content'),
            array('content' => 'item_number', 'sortable' => 'item_number'),
            array('content' => 'quantity', 'sortable' => 'quantity'),
            array('content' => 'shipping', 'sortable' => 'shipping'),
            array('content' => 'tax', 'sortable' => 'tax'),
            array('content' => 'mc_currency', 'sortable' => 'mc_currency'),
            array('content' => 'mc_fee', 'sortable' => 'mc_fee'),
            array('content' => 'mc_gross', 'sortable' => 'mc_gross'),
            array('content' => 'txn_type', 'sortable' => 'txn_type'),
            array('content' => 'txn_id', 'sortable' => 'txn_id'),
            array('content' => 'notify_version', 'sortable' => 'notify_version'),
            array('content' => 'custom', 'sortable' => 'custom'),
            array('content' => 'invoice', 'sortable' => 'invoice'),
            array('content' => 'charset', 'sortable' => 'charset'),
            array('content' => 'verify_sign', 'sortable' => 'verify_sign')
        );
        
        $request = $this->_getRequest();
        $filters = array(
            array('type' => 'button_filter'),
            'payment_type' => array(
                'type' => 'text', 
                'value' => $request->get('payment_type')
            ),
            'payment_date' => array(
                'type' => 'text', 
                'value' => $request->get('payment_date')
            ),
            'payment_status' => array(
                'type' => 'text', 
                'value' => $request->get('payment_status')
            ),
            'address_status' => array(
                'type' => 'text', 
                'value' => $request->get('address_status')
            ),
            'payer_status' => array(
                'type' => 'text', 
                'value' => $request->get('payer_status')
            ),
            'first_name' => array(
                'type' => 'text', 
                'value' => $request->get('first_name')
            ),
            'last_name' => array(
                'type' => 'text', 
                'value' => $request->get('last_name')
            ),
            'payer_email' => array(
                'type' => 'text', 
                'value' => $request->get('payer_email')
            ),
            'payer_id' => array(
                'type' => 'text', 
                'value' => $request->get('payer_id')
            ),
            'address_name' => array(
                'type' => 'text', 
                'value' => $request->get('address_name')
            ),
            'address_country' => array(
                'type' => 'text', 
                'value' => $request->get('address_country')
            ),
            'address_country_code' => array(
                'type' => 'text', 
                'value' => $request->get('address_country_code')
            ),
            'address_zip' => array(
                'type' => 'text', 
                'value' => $request->get('address_zip')
            ),
            'address_state' => array(
                'type' => 'text', 
                'value' => $request->get('address_state')
            ),
            'address_city' => array(
                'type' => 'text', 
                'value' => $request->get('address_city')
            ),
            'address_street' => array(
                'type' => 'text', 
                'value' => $request->get('address_street')
            ),
            'business' => array(
                'type' => 'text', 
                'value' => $request->get('business')
            ),
            'receiver_email' => array(
                'type' => 'text', 
                'value' => $request->get('receiver_email')
            ),
            'receiver_id' => array(
                'type' => 'text', 
                'value' => $request->get('receiver_id')
            ),
            'residence_country' => array(
                'type' => 'text', 
                'value' => $request->get('residence_country')
            ),
            'item_name' => array(
                'type' => 'text', 
                'value' => $request->get('item_name')
            ),
            'item_number' => array(
                'type' => 'text', 
                'value' => $request->get('item_number')
            ),
            'quantity' => array(
                'type' => 'text', 
                'value' => $request->get('quantity')
            ),
            'shipping' => array(
                'type' => 'text', 
                'value' => $request->get('shipping')
            ),
            'tax' => array(
                'type' => 'text', 
                'value' => $request->get('tax')
            ),
            'mc_currency' => array(
                'type' => 'text', 
                'value' => $request->get('mc_currency')
            ),
            'mc_fee' => array(
                'type' => 'text', 
                'value' => $request->get('mc_fee')
            ),
            'mc_gross' => array(
                'type' => 'text', 
                'value' => $request->get('mc_gross')
            ),
            'txn_type' => array(
                'type' => 'text', 
                'value' => $request->get('txn_type')
            ),
            'txn_id' => array(
                'type' => 'text', 
                'value' => $request->get('txn_id')
            ),
            'notify_version' => array(
                'type' => 'text', 
                'value' => $request->get('notify_version')
            ),
            'custom' => array(
                'type' => 'text', 
                'value' => $request->get('custom')
            ),
            'invoice' => array(
                'type' => 'text', 
                'value' => $request->get('invoice')
            ),
            'charset' => array(
                'type' => 'text', 
                'value' => $request->get('charset')
            ),
            'verify_sign' => array(
                'type' => 'text', 
                'value' => $request->get('verify_sign')
            )                
        );
        
        $this->thead = array(
            $titles, $filters
        );
    }
    
    protected function _setBody($rows)
    {
        foreach($rows as $row)
        {    
            $cur = array();
            $cur['id'] = $row['id'];
            $cur['payment_type'] = $row['payment_type'];
            $cur['payment_date'] = $row['payment_date'];
            $cur['payment_status'] = $row['payment_status'];
            $cur['address_status'] = $row['address_status'];
            $cur['payer_status'] = $row['payer_status'];
            $cur['first_name'] = $row['first_name'];
            $cur['last_name'] = $row['last_name'];
            $cur['payer_email'] = $row['payer_email'];
            $cur['payer_id'] = $row['payer_id'];
            $cur['address_name'] = $row['address_name'];
            $cur['address_country'] = $row['address_country'];
            $cur['address_country_code'] = $row['address_country_code'];
            $cur['address_zip'] = $row['address_zip'];
            $cur['address_state'] = $row['address_state'];
            $cur['address_city'] = $row['address_city'];
            $cur['address_street'] = $row['address_street'];
            $cur['business'] = $row['business'];
            $cur['receiver_email'] = $row['receiver_email'];
            $cur['receiver_id'] = $row['receiver_id'];
            $cur['residence_country'] = $row['residence_country'];
            $cur['item_name'] = $row['item_name'];
            $cur['item_number'] = $row['item_number'];
            $cur['quantity'] = $row['quantity'];
            $cur['shipping'] = $row['shipping'];
            $cur['tax'] = $row['tax'];
            $cur['mc_currency'] = $row['mc_currency'];
            $cur['mc_fee'] = $row['mc_fee'];
            $cur['mc_gross'] = $row['mc_gross'];
            $cur['txn_type'] = $row['txn_type'];
            $cur['txn_id'] = $row['txn_id'];
            $cur['notify_version'] = $row['notify_version'];
            $cur['custom'] = $row['custom'];
            $cur['invoice'] = $row['invoice'];
            $cur['charset'] = $row['charset'];
            $cur['verify_sign'] = $row['verify_sign'];
            $this->tbody[] = $cur;    
        }
    }
}

?>
