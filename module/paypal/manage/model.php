<?php
/**
 * Description of model
 *
 * @author Valkyria
 */
class Module_Paypal_Manage_Model 
{
    public $sortable_rules = array(
        'name' => 'id',
        'type' => 'desc',
    );
    
    public $filter_rules = array(
        array(
            'name' => 'payment_type', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'payment_date', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'payment_status', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'address_status', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'payer_status', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'first_name', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'last_name', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'payer_email', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'payer_id', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'address_name', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'address_country', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'address_country_code', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'address_zip', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'address_state', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'address_city', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'address_street', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'business', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'receiver_email', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'receiver_id', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'residence_country', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'item_name', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'item_number', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'quantity', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'shipping', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'tax', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'mc_currency', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'mc_fee', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'mc_gross', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'txn_type', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'txn_id', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'notify_version', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'custom', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'invoice', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'charset', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ), 
        array(
            'name' => 'verify_sign', 
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        )            
    );
}

?>
