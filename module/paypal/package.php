<?php
global $package;
$package = array(
    'version' => '2',
    'name' => 'module.paypal',
    'dependence' => array(
        'lib.jdatepicker',
        'lib.jgrid',
        'module.lang',
        'component.jalerts'
    )
);