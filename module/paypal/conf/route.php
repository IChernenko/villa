<?php

$url = Dante_Controller_Front::getRequest()->get_requested_url();

Dante_Lib_Router::addRule("/^\/paypal\/notify\/$/", array(
    'controller' => 'Module_Paypal_Controller',
    'action' => 'receive'
));

Dante_Lib_Router::addRule("/^\/paypal\/return\/$/", array(
    'controller' => 'Module_Paypal_Controller',
    'action' => 'return'
));

Dante_Lib_Router::addRule("/manage\/paypal-logs\//", array(
    'controller' => 'Module_Paypal_Manage_Controller'
));

?>