DELETE FROM `db_version` WHERE `package` = 'lib.paypall';
DELETE FROM `[%%]division` WHERE `link` = '/manage/paylogs/';

DROP TABLE IF EXISTS `[%%]paypall_log`;

CREATE TABLE IF NOT EXISTS `[%%]paypal_log` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `payment_type` varchar(50) DEFAULT NULL,
    `payment_date` varchar(50) DEFAULT NULL,
    `payment_status` varchar(50) DEFAULT NULL,
    `address_status` varchar(50) DEFAULT NULL,
    `payer_status` varchar(50) DEFAULT NULL,
    `first_name` varchar(50) DEFAULT NULL,
    `last_name` varchar(50) DEFAULT NULL,
    `payer_email` varchar(50) DEFAULT NULL,
    `payer_id` varchar(50) DEFAULT NULL,
    `address_name` varchar(50) DEFAULT NULL,
    `address_country` varchar(50) DEFAULT NULL,
    `address_country_code` varchar(50) DEFAULT NULL,
    `address_zip` varchar(50) DEFAULT NULL,
    `address_state` varchar(50) DEFAULT NULL,
    `address_city` varchar(50) DEFAULT NULL,
    `address_street` varchar(100) DEFAULT NULL,
    `business` varchar(50) DEFAULT NULL,
    `receiver_email` varchar(50) DEFAULT NULL,
    `receiver_id` varchar(50) DEFAULT NULL,
    `residence_country` varchar(50) DEFAULT NULL,
    `item_name` varchar(50) DEFAULT NULL,
    `item_number` varchar(50) DEFAULT NULL,
    `quantity` int(11) DEFAULT NULL,
    `shipping` float(10,2) NOT NULL,
    `tax` float(10,2) NOT NULL,
    `mc_currency` varchar(50) DEFAULT NULL,
    `mc_fee` float(10,2) NOT NULL,
    `mc_gross` float(10,2) NOT NULL,
    `txn_type` varchar(50) DEFAULT NULL,
    `txn_id` int(11) DEFAULT NULL,
    `notify_version` varchar(50) DEFAULT NULL,
    `custom` varchar(50) DEFAULT NULL,
    `invoice` varchar(50) DEFAULT NULL,
    `charset` varchar(50) DEFAULT NULL,
    `verify_sign` varchar(100) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='paypal логи' AUTO_INCREMENT=1;
