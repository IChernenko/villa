<?php

/**
 * Description of model
 *
 * @author Mort
 */
class Module_Paypal_Model 
{
    public $id;
    public $payment_type; 
    public $payment_date; 
    public $payment_status; 
    public $address_status; 
    public $payer_status; 
    public $first_name; 
    public $last_name; 
    public $payer_email; 
    public $payer_id; 
    public $address_name; 
    public $address_country; 
    public $address_country_code; 
    public $address_zip; 
    public $address_state; 
    public $address_city; 
    public $address_street; 
    public $business;
    public $receiver_email;
    public $receiver_id;
    public $residence_country;
    public $item_name;
    public $item_number;
    public $quantity;
    public $shipping;
    public $tax;
    public $mc_currency;
    //Сумма коммиссионных. Сумма, зачисленная на счет продавца, определяется как mc_gross–mc_fee 
    public $mc_fee;
    //Сумма платежа. 
    public $mc_gross;
    public $txn_type;
    public $txn_id;
    public $notify_version;
    public $custom;
    public $invoice;
    public $charset;
    public $verify_sign;
    
    // Url
    public $notify_url;
    public $return_url;
    
    public function fillFromRequest(Dante_Lib_Request $request)
    {
        $this->payment_type = $request->get_validate('payment_type', 'text', NULL);
        $this->payment_date = $request->get_validate('payment_date', 'text', NULL);
        $this->payment_status = $request->get_validate('payment_status', 'text', NULL);
        $this->address_status = $request->get_validate('address_status', 'text', NULL);
        $this->payer_status = $request->get_validate('payer_status', 'text', NULL);
        $this->first_name = $request->get_validate('first_name', 'text', NULL);
        $this->last_name = $request->get_validate('last_name', 'text', NULL);
        $this->payer_email = $request->get_validate('payer_email', 'text', NULL);
        $this->payer_id = $request->get_validate('payer_id', 'text', NULL);
        $this->address_name = $request->get_validate('address_name', 'text', NULL);
        $this->address_country = $request->get_validate('address_country', 'text', NULL);
        $this->address_country_code = $request->get_validate('address_country_code', 'text', NULL);
        $this->address_zip = $request->get_validate('address_zip', 'text', NULL);
        $this->address_state = $request->get_validate('address_state', 'text', NULL);
        $this->address_city = $request->get_validate('address_city', 'text', NULL);
        $this->address_street = $request->get_validate('address_street', 'text', NULL);
        $this->business = $request->get_validate('business', 'text', NULL);
        $this->receiver_email = $request->get_validate('receiver_email', 'text', NULL);
        $this->receiver_id = $request->get_validate('receiver_id', 'text', NULL);
        $this->residence_country = $request->get_validate('residence_country', 'text', NULL);
        $this->item_name = $request->get_validate('item_name', 'text', NULL);
        $this->item_number = $request->get_validate('item_number', 'text', NULL);
        $this->quantity = $request->get_validate('quantity', 'int', NULL);
        $this->shipping = $request->get_validate('shipping', 'int', 0);
        $this->tax = $request->get_validate('tax', 'int', 0);
        $this->mc_currency = $request->get_validate('mc_currency', 'text', NULL);
        $this->mc_fee = $request->get_validate('mc_fee', 'int', 0);
        $this->mc_gross = $request->get_validate('mc_gross', 'int', 0);
        $this->txn_type = $request->get_validate('txn_type', 'text', NULL);
        $this->txn_id = $request->get_validate('txn_id', 'int', NULL);
        $this->notify_version = $request->get_validate('notify_version', 'text', NULL);
        $this->custom = $request->get_validate('custom', 'text', NULL);
        $this->invoice = $request->get_validate('invoice', 'text', NULL);
        $this->charset = $request->get_validate('charset', 'text', NULL);
        $this->verify_sign = $request->get_validate('verify_sign', 'text', NULL);
    }
}

?>
