<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 07.06.12
 * Time: 12:39
 * To change this template use File | Settings | File Templates.
 */
class Module_Paypal_Controller extends Dante_Controller_Base
{      
    protected $_business;
    
    protected $_notify_url;
    protected $_return_url;
    
    protected $_model;

    protected function _init() 
    {
        parent::_init();
        $this->_business = Dante_Lib_Config::get('paypal.business');
        $this->_notify_url = Dante_Lib_Config::get('paypal.notify_url');
        $this->_return_url = Dante_Lib_Config::get('paypal.return_url');  
    }
    
    protected function _instanceView()
    {
        return new Module_Paypal_View();
    }

    protected function _defaultAction() 
    {
        $params = new Module_Paypal_Model();
        $params->mc_gross = (float)$this->_getParam('amount');
        $params->mc_currency = $this->_getParam('currency');
        $params->item_name = rawurldecode($this->_getParam('item_name'));
        $params->item_number = (int)$this->_getParam('item_number');
        $params->custom = $this->_getParam('custom');
        
        return $this->_getForm($params);
    }
    
    protected function _getForm(Module_Paypal_Model $params)
    {        
        if(!$this->_business)
            return 'Ошибка конфигурации. Не установлено поле business';
                        
        $params->business = $this->_business;
        $params->notify_url = $this->_notify_url;
        $params->return_url = $this->_return_url; 
                                               
        $view = $this->_instanceView();
        return $view->draw($params);
    }

    protected function _receiveAction() 
    {
        $request = $this->_getRequest();
        
        $this->_model = new Module_Paypal_Model();
        $this->_model->fillFromRequest($request);

        Dante_Lib_Log_Factory::getLogger()->debug('paypal model : '.var_export($this->_model, true), 'logs/my.log');

        //заносим данные в БД
        $mapper = new Module_Paypal_Mapper();
        $mapper->add($this->_model);
    }
    
    protected function _returnAction() 
    {
        return 'Информация о вашей оплате поступила нашему администратору.';
    }
}
