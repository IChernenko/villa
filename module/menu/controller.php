<?php
/**
 * Контроллер меню
 * 
 * @package menu
 * @subpackage app
 *
 * @author Администратор
 */
class Module_Menu_Controller extends Dante_Controller_Base{
    
    /**
     * Идентификатор меню
     * @var string || bool
     */
    protected $id = false;
    
    protected $_class = false;
    
    protected function _init() {
        parent::_init();
        $this->id = Dante_Lib_Config::get('menu.id');
    }
    
    
    
    protected function _getTemplateFileName() {
        $tplFileName = '../module/menu/template/menu.html';
        $ws = Dante_Lib_Config::get('app.ws');
        if (file_exists("template/{$ws}/menu/menu.html")) $tplFileName = "template/{$ws}/menu/menu.html";
        
        return $tplFileName;
    }

    /**
     * Определеить родительский раздел
     * @return type 
     */
    protected function _getParentDivision() {
        return 3;
    }

    protected function _defaultAction()
    {
        $parentDivision = $this->_getParentDivision();
        
        $mapper = new Module_Division_Mapper();
        $items = $mapper->getAll(Module_Lang_Helper::getCurrent());
        if (!isset($items[$parentDivision])) return '';
        
        //var_dump($items);
        
        // определить активное меню
        $url = $this->_getRequest()->get_requested_url();
        if ($url[0] == '/') {
            $url = substr($url, 1);
        }
        
        $divisionId = $mapper->getByLink($url);
        if ($divisionId) {
            if (isset($items[$parentDivision][$divisionId]) && $items[$parentDivision][$divisionId]) {
                $items[$parentDivision][$divisionId]->active = true;
            }
            
        }
        
        
        if ($this->id===false) $this->id = Dante_Lib_Config::get('menu.id');
        $tpl = new Dante_Lib_Template();
        $tpl->items = $items[$parentDivision];
        if ($this->id) {
            $tpl->id = 'id="'.$this->id.'"';
        }
        
        if (is_null($this->id)) $tpl->id = '';
        
        if ($this->_class) {
            $tpl->class = 'class="'.$this->_class.'"';
        }
        
        $tpl->lang = Module_Lang_Helper::getCurrentCode();
        
        return $tpl->draw($this->_getTemplateFileName());
    }
}

?>
