<?php
global $package;
$package = array(
    'version' => '2',
    'name' => 'module.menu',
    'dependence' => array(
        'lib.jdatepicker',
        'lib.jgrid',
        'module.lang'
    )
);