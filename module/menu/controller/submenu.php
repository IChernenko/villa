<?php



/**
 * Контроллер подменю
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Menu_Controller_Submenu extends Module_Menu_Controller{
    
    protected $_class = 'secondMenu';
    
    protected $id = null;
    
    protected function _getParentDivision() {
        $url = $this->_getRequest()->get_requested_url();
        if ($url[0] == '/') {
            $url = substr($url, 1);
        }
        // определить id раздела по url
        $divisionMapper = new Module_Division_Mapper();
        return $divisionMapper->getByLink($url);
    }
}

?>
