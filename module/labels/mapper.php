<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of mapper
 *
 * @author Valkyria
 */
class Module_Labels_Mapper 
{ 
    protected $_tableName = '[%%]labels';
    
    protected $_tableNameLang = '[%%]labels_lang';
    
    
    public function getByCode($code)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->select(array('sys_name' => $code));
        return $table->id;
    }

    
    /**
     * @param Module_Labels_Model $params 
     * @return int
     */
    public function add(Module_Labels_Model $params) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->sys_name = $params->sys_name;
        $table->is_system = $params->is_system;

        $params->id = $table->insert();
        
        return $this->addLang($params);
    }
    
    /**
     * добавление локализации
     * @param Module_Labels_Model $params
     * @return int
     */
    public function addLang(Module_Labels_Model $params) {
        $table = new Dante_Lib_Orm_Table($this->_tableNameLang);
        $table->id = $params->id;
        $table->lang_id = $params->lang_id;
        $table->name = $params->name;
        
        $table->insert();
        return $params->id;
    }
}

?>
