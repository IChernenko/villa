labelsAdmin = {
    editSubmit: function(obj)
    {
        if($(obj).hasClass('disabled')) return false;
        $('.func').addClass('disabled');
        
        var data = $('#editLabelForm').serializeArray();
        $.ajax({
            type: 'POST',
            data:data,
            url: '/ajax.php',
            dataType : "json",
            success: function (data)
            {
                $('.func').removeClass('disabled');
                if (data.result == 1) 
                {       
                    jAlert('Изменения сохранены', 'Сообщение');
                    if(data.id)
                    {
                        $('#editLabelForm #id').val(data.id);
                    }   
                }
                else 
                {   
                    jAlert(data.message, 'Ошибка'); 
                }
            }
        });
    },
    changeLang: function(obj)
    {
        if($(obj).hasClass('disabled')) return false;
        $('.func').addClass('disabled');
         
        var id = $('#editLabelForm #id').val();
        if(!id) return;
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'ajax.php',
            data: {                
                controller: 'module.labels.manage.controller',
                action: 'drawForm',
                id: id,
                lang_id: $(obj).val()
            },
            success: function(data){
                if(data.result==1)
                {
                    $('.modal-body').html(data.html);
                }
                else
                {
                    console.log(data);
                }
            }
        });
    }
}