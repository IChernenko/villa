<?php



/**
 * Хелпер по работе с многоязыковыми метками
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Labels_Helper {
    
    /**
     * Добавить языковую константу
     * @param Module_Labels_Model $model
     * @return type 
     */
    public static function add(Module_Labels_Model $model) {
        $mapper = new Module_Labels_Mapper();
        return $mapper->add($model);
    }
}

?>
