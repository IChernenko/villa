<?php
global $package;
$package = array(
    'version' => '4',
    'name' => 'module.labels',
    'dependence' => array(
        'lib.jdatepicker',
        'lib.jgrid',
        'module.lang',
        'component.jalerts'
    ),
    'js' => array(
        '../module/labels/js/labelsAdmin.js'
    )
);