<?php
/**
 * Description of controller
 *
 * @author Mort
 */
class Module_Labels_Manage_Controller  extends Module_Handbooksgenerator_Controller_Base
{
    protected function _init() 
    {
        parent::_init();
        $this->_mapper = new Module_Labels_Manage_Mapper();
    }

    /**
     * отрисовка стартового html
     * @return string 
     */
    protected function _drawAction()
    {
        $model = new Module_Labels_Manage_Model();
        $params = $this->getParams($model);
        
        $list = $this->_mapper->getList($params);
        
        $this->formEnable('module.labels.manage.controller');
        $this->tableParams['style'] = array(
            'width' => '700px'
        );
        
        $this->_setHead();
        $this->_setBody($list['rows']);
        
        $this->tfoot = array(
            array(
                $this->paginator($list['count'], $params)
            ),
            array(
                'button_filter' => array(
                    'type' => array(
                        'edit'
                    ),                    
                )
             ),       
        );
        
        return $this->generate();
    }
    
    protected function _setHead()
    {
        $titles = array(
            array(
                'content' => 'ID',
                'sortable' => 't1.id'
            ),
            array(
                'content' => 'Сист. название',
                'sortable' => 'sys_name'
            ),
            array(
                'content' => 'Метка (язык по умолчанию)',
                'sortable' => 'sys_name'
            ),
            array(
                'content' => 'Сист. метка',
                'sortable' => 'is_system'
            ),
            array(
                'content' => 'Функции',
                'params' => array(
                    'style' => array(
                        'width' => '100px'
                    )
                )
            )
        );
        
        $filters = array(
            NULL,
            'sys_name' => array(
                'type' => 'text',
                'value' => $this->_getRequest()->get('sys_name')
            ),
            'name' => array(
                'type' => 'text',
                'value' => $this->_getRequest()->get('name')
            ),
            NULL,
            'button_filter' => array(
                'type' => 'button_filter'
            )
        );
        
        $this->thead = array(
            $titles, $filters
        );
    }
    
    protected function _setBody($rows)
    {
        foreach($rows as $cur)
        {
            $content['id'] = $cur['id'];
            $content['sys_name'] = $cur['sys_name'];
            $content['name'] = $cur['name'];
            $content['is_system'] = $cur['is_system']?'Да':'Нет';
            $content['buttons'] = array(
                'id' => $cur['id'],
                'type' => $cur['is_system']?array('edit'):array('edit', 'del')    
            );
            
            $row['content'] = $content;
            if($cur['is_system']) $row['params']['class'] = 'error';
            $this->tbody[] = $row;
        }
    }
    
    protected function _drawFormAction()
    {
        $langMapper = new Module_Lang_Mapper();
        $view = new Module_Labels_Manage_View();
        
        $id = $this->_getRequest()->get('id');
        $lang = $this->_getRequest()->get_validate('lang_id', 'int', 1);
        
        $model = $this->_mapper->getById($id, $lang);
        $view->langList = $langMapper->getList();
        
        return array(
            'result' => 1,
            'html' => $view->drawForm($model)
        );
    }

    protected function _editAction() 
    {
        $model = new Module_Labels_Manage_Model();
        $model->id = $this->_getRequest()->get_validate('id', 'int', false);
        $model->lang_id = $this->_getRequest()->get_validate('lang_id', 'int', 1);
        $model->sys_name = $this->_getRequest()->get('sys_name');
        $model->name = $this->_getRequest()->get('name');
        
        $id = $this->_mapper->apply($model);
        
        return array(
            'result' => 1,
            'id' => $id
        );
    }
    
    protected function _delAction() 
    {
        $id = $this->_getRequest()->get_validate('id', 'int');
        $model = $this->_mapper->getById($id);
        
        if(!$model->is_system)
            $this->_mapper->del($id);
        
        return array(
            'result' => 1
        );
    }
}

?>
