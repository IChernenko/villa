<?php

/**
 * Description of model
 *
 * @author Mort
 */
class Module_Labels_Manage_Model extends Module_Labels_Model
{    
    public $sortable_rules = array(
        'name'=>'`t1`.`id`',
        'type'=>'asc',
    );
    
    public $filter_rules = array(
        array(
            'name'=>'sys_name',
            'format'=>'`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ),
        array(
            'name'=>'name',
            'format'=>'`t2`.`%1$s` LIKE "%3$s%2$s%3$s"'
        )
    );
}

?>
