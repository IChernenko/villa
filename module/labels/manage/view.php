<?php
/**
 * Description of view
 *
 * @author Valkyria
 */
class Module_Labels_Manage_View 
{
    public $langList;
    
    public function drawForm($model)
    {
        $tpl = new Dante_Lib_Template();
        $filePath = '../module/labels/manage/template/form.html';
        
        $tpl->model = $model;
        $tpl->langList = $this->langList;
        return $tpl->draw($filePath);
    }
}

?>
