<?php
/**
 * Description of mapper
 *
 * @author mort, dorian
 */

class Module_Labels_Manage_Mapper extends Module_Handbooksgenerator_Mapper_Base
{    
    protected $_tableName = '[%%]labels';
    
    protected $_tableNameLang = '[%%]labels_lang';
    
    public function getList($params)
    {
        $join = "LEFT JOIN {$this->_tableNameLang} AS `t2` ON(`t1`.`id` = `t2`.`id` AND `t2`.`lang_id` = 1)";
        return $this->getRowsByParams($params, '*', $join);
    }
    
    public function getById($id, $lang = 1)
    {
        $model = new Module_Labels_Model();
        if(!$id) return $model;
        
        $model->id = $id;
        $model->lang_id = $lang;
        
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->select(array('id' => $id));
        $model->sys_name = $table->sys_name;
        
        $tableLang = new Dante_Lib_Orm_Table($this->_tableNameLang);
        $tableLang->select(array('id' => $id, 'lang_id' => $lang));        
        $model->name = $tableLang->name;
        
        return $model;
    }
    
    /**
     * @param Module_Labels_Model $model 
     */
    public function apply(Module_Labels_Model $model) 
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->id = $model->id;
        $table->sys_name = $model->sys_name;
        
        if($model->id)
            $table->update(array('id' => $model->id));
        else
            $model->id = $table->insert();
        
        $tableLang = new Dante_Lib_Orm_Table($this->_tableNameLang);
        $tableLang->id = $model->id;
        $tableLang->lang_id = $model->lang_id;
        $tableLang->name = $model->name;
        
        $cond = array(
            'id' => $model->id,
            'lang_id' => $model->lang_id
        );
        
        if($tableLang->exist($cond))
            $tableLang->update($cond);
        else
            $tableLang->insert();
        
        return $model->id;
    }
}

?>
