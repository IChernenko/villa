CREATE TABLE IF NOT EXISTS [%%]labels (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sys_name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='labels' AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS [%%]labels_lang (
  `id` int(11) unsigned DEFAULT NULL,
  `lang_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='языковые данные labels';

ALTER TABLE [%%]labels_lang ADD
      CONSTRAINT fk_labels_lang_lang_id
      FOREIGN KEY (`lang_id`)
      REFERENCES [%%]languages(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE [%%]labels_lang ADD
      CONSTRAINT fk_labels_lang_id
      FOREIGN KEY (`id`)
      REFERENCES [%%]labels(id) ON DELETE CASCADE ON UPDATE CASCADE;