<?php



/**
 * View для dropdown меню
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Dropdown_View {
    
    public $id = 'nav';
    
    public $class = '';
    
    public $lang = false;
    
    /**
     *
     * @param mixed $item
     * @return string
     */
    protected function _drawItem($item, $level) {
        $attr = '';
        if ($item->is_active) {
            $attr = 'class="active"';
        }
        //print_r($item);
        if ($this->lang) $item->link = $this->lang.'/'.$item->link;
        $onclick = ($item->d_type==='empty')?'onclick="return false;"':'';
        return "<li {$attr}><a href=\"{$item->link}\" {$onclick} >{$item->name}</a>";
    }
    
    /**
     * Проверяет есть ли у заданного среза разделов активный элемент
     * @param array $items
     * @param int $key
     * @return bool
     */
    protected function _hasActiveItem($items, $key) {
        foreach($items[$key] as $item) {
            if (isset($item->is_active) && $item->is_active) return true;
        }
        return false;
    }

    protected function _drawAttr($items, $key, $level) {
        $attr = '';
        if ($level == 1) {
            if ($this->id) $attr = 'id="'.$this->id.'"';
            if ($this->class) $attr .= ' class="'.$this->class.'"';
        }    
        
        if ($this->_hasActiveItem($items, $key))
                $attr .= ' class="active"';
                
        return $attr;
    }

    public function draw($items, $key, $level=1) {
        if(!isset($items[$key]) || !is_array($items[$key])) return '';
        
        $attr = $this->_drawAttr($items, $key, $level);        
        $html = "<ul {$attr}>";
        foreach($items[$key] as $item) {
            if(!$item->name) continue;
            $html .= $this->_drawItem($item, $level);
            // проверяем есть ли дети
            if (isset($items[$item->id])) {
                $nextLevel = $level + 1;
                $html.= $this->draw($items, $item->id, $nextLevel);
            }
            $html .= "</li>";        
        }                    
        $html .= "</ul>";
        return $html;            
    }
}

?>
