<?php



/**
 * Контроллер dropdown меню
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Dropdown_Controller extends Dante_Controller_Base{
    
    protected function _instanceView() {
        return new Module_Dropdown_View();
    }
    
    protected function _defaultAction() {
        $mapper = new Module_Division_Mapper();
        
        // определить активное меню
        $url = $this->_getRequest()->get_requested_url();
        $url = explode('/', $url);
        $url = array_pop($url);
     
        
        //if ($url[0] == '/') $url = substr($url, 1);
        
        $divisionId = $mapper->getByLink($url);
        $langId = Module_Lang_Helper::getCurrent();
        $list = $mapper->getAll($langId, false, $divisionId);
        
        if (!isset($list[3])) return '';
        
        
        
        
        
        $view = $this->_instanceView();
        
        if (Dante_Lib_Config::get('dropdown.enableMultilanguage') === true) {
            $langCode = Module_Lang_Helper::getCurrentCode();
            $view->lang = $langCode;
        }
        return $view->draw($list, 3);
    }
}

?>
