imageviewer = {
    imgVal: 10,
    thumbVal: 0,
    
    imgMax: 0,
    thumbMax: 0,
    
    imgStep: 940,
    thumbStep: 161,
    
    descrList: false,
    descrVal: 0,
    descrStep: 890,
    
    setWidth: function()
    {
        var ulScroll = $('ul.image-viewer-scroll, #imageViewerDescription ul');
        if(ulScroll.length > 0)
        {
            for(var i=0; i < ulScroll.length; i++)
            {
                var listItems = $(ulScroll[i]).children('li');
                var listWidth = 0;
                for(var j=0; j < listItems.length; j++)
                {
                    listWidth += $(listItems[j]).outerWidth(true);
                }
                $(ulScroll[i]).width(listWidth);
            }
        }
    },
    
    setDescript: function()
    {
        if($('#imageViewerDescription ul').length > 0) this.descrList = true;
    },
    
    setButtons: function()
    {
        if($('#imageViewerImgList li').length == 1)
            $('#imageViewer .arr-b-right').css('display', 'none');
        if($('#imageViewerThumbsList li').length < 6)
            $('#imageViewer .arr-s-right').css('display', 'none');
    },
    
    setMax: function()
    {
        this.imgMax = $('#imageViewerImgList').width() - $('#imageViewerImgWrap').width() - 10;
        this.thumbMax = $('#imageViewerThumbsList').width() - $('#imageViewerThumbsWrap').width();
    },
    
    scroll: function(list, direct)
    {
        if(list == 'img') this.scrollImg(direct);
        if(list == 'thumbs') this.scrollThumb(direct);
    },
    
    scrollImg: function(direct)
    {
        if(direct == 'right' && this.imgVal < this.imgMax)
        {
            this.imgVal += this.imgStep;
            $('#imageViewerImgWrap').animate({scrollLeft: this.imgVal}, 1000);
            if(this.descrList)
            {
                this.descrVal += this.descrStep;
                this.changeDescript();
            }
               
            if(this.imgVal == (this.imgStep+10)) $('button.arr-b-left').fadeIn(300);
            if(this.imgVal == this.imgMax) $('button.arr-b-right').fadeOut(300);
        }
        if(direct == 'left' && this.imgVal > 10)
        {
            this.imgVal -= this.imgStep;
            $('#imageViewerImgWrap').animate({scrollLeft: this.imgVal}, 1000);
            if(this.descrList)
            {
                this.descrVal -= this.descrStep;
                this.changeDescript();
            }
            
            if(this.imgVal == 10) $('button.arr-b-left').fadeOut(300);
            if(this.imgVal == (this.imgMax-this.imgStep)) $('button.arr-b-right').fadeIn(300);
        }
    },
    
    changeDescript: function(animate)
    {
        if(animate === undefined) animate = true;
        
        if(animate)
        {
            $('#imageViewerDescription ul').fadeOut(100);
            $('#imageViewerDescription div').delay(100).animate({scrollLeft: this.descrVal}, 0);
            $('#imageViewerDescription ul').fadeIn(300);            
        }
        else
        {
            $('#imageViewerDescription div').scrollLeft(this.descrVal);
        }
    },
    
    scrollThumb: function(direct)
    {
        if(direct == 'right' && this.thumbVal < this.thumbMax)
        {
            this.thumbVal += this.thumbStep;
            $('#imageViewerThumbsWrap').animate({scrollLeft: this.thumbVal}, 500);
            if(this.thumbVal == this.thumbMax) $('button.arr-s-right').fadeOut(300);
            if(this.thumbVal == this.thumbStep) $('button.arr-s-left').fadeIn(300);
        }
        if(direct == 'left' && this.thumbVal > 0)
        {
            this.thumbVal -= this.thumbStep;
            $('#imageViewerThumbsWrap').animate({scrollLeft: this.thumbVal}, 500);
            if(this.thumbVal == 0) $('button.arr-s-left').fadeOut(300);
            if(this.thumbVal == (this.thumbMax-this.thumbStep)) $('button.arr-s-right').fadeIn(300);
        }
    },
    
    showSelected: function(item)
    {
        var index = $(item).attr('index');
        var scroll = 10 + this.imgStep*index;
        $("#imageViewerImgList").fadeOut(100);
        $("#imageViewerImgWrap").delay(100).animate({scrollLeft: scroll}, 0);
        $("#imageViewerImgList").fadeIn(300);
        if(this.descrList)
        {
            this.descrVal = this.descrStep*index;
            this.changeDescript();
        }
        var prevVal = this.imgVal;
        this.imgVal = scroll;
        
        if(prevVal == 10) $('button.arr-b-left').delay(300).fadeIn(300);
        if(prevVal == this.imgMax) $('button.arr-b-right').delay(300).fadeIn(300);
        if(scroll == 10) $('button.arr-b-left').delay(300).fadeOut(300);
        if(scroll == this.imgMax) $('button.arr-b-right').delay(300).fadeOut(300);
    },
    
    showActive: function(index)
    {
        var scroll = 10 + this.imgStep*index;
        $("#imageViewerImgWrap").scrollLeft(scroll);
        if(this.descrList)
        {
            this.descrVal = this.descrStep*index;
            this.changeDescript(false);
        }
        var prevVal = this.imgVal;
        this.imgVal = scroll;
        
        if(prevVal == 10) $('button.arr-b-left').fadeIn(0);
        if(prevVal == this.imgMax) $('button.arr-b-right').fadeIn(0);
        if(scroll == 10) $('button.arr-b-left').fadeOut(0);
        if(scroll == this.imgMax) $('button.arr-b-right').fadeOut(0);
    },
    
    valuesReset: function()
    {
        this.imgVal = 10;
        this.thumbVal = 0;
        this.descrList = false;
        this.descrVal = 0;
    }
}

function scroll(list, direct)
{
    imageviewer.scroll(list, direct);
}

function activateViewer(activeIndex)
{
    $('#imageViewerWrap').arcticmodal({
        afterClose: (function(){            
            imageviewer.valuesReset();
            $('#imageViewerHide').remove();
        })
    });
    
    $('.b-modal_close').click(function(){
        $('#imageViewerWrap').arcticmodal('close');
    });
    
    $('.arcticmodal-overlay, .arcticmodal-container').css('z-index', '3000');
    imageviewer.setDescript();
    imageviewer.setWidth();
    imageviewer.setButtons();
    imageviewer.setMax();
        
    $('#imageViewerImgWrap').scrollLeft(10);
    
    if(activeIndex >= 0)
        imageviewer.showActive(activeIndex);
    
    $('#imageViewerThumbsList li').click(function(){
        imageviewer.showSelected(this);
    });
}

function showViewer(params, location, activeIndex)
{
    if(activeIndex === undefined) activeIndex = -1;
    
    jQuery.ajax({
        type: 'POST',
        url: '/ajax.php',
        data:
        {
            controller: 'module.imageviewer.controller.imageviewer',
            action: 'show',
            images: params['images'],
            thumbs: params['thumbs'],
            title: params['title'],
            descript: params['descript']

        },
        success: function(data)
        {
            if(data.result == 1)
            {
                jQuery(location).append(data['html']);
                activateViewer(activeIndex);
            }
            else console.log(data);
        }
    });
}