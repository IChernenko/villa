<?php
global $package;
$package = array(
    'version' => '0',
    'name'=> 'module.imageviewer',
    'dependence' => array(
        'lib.jquery.arcticmodal'
    ),
    'js' => array(
        '../module/imageviewer/js/imageviewer.js'
    ),
    'css' => array(
        '../module/imageviewer/css/imageviewer.css'
    )
);