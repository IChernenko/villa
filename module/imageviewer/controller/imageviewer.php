<?php
/**
 * Контроллер модуля просмотра изображений
 * 
 * @author Valkyria 
 */
class Module_Imageviewer_Controller_Imageviewer extends Dante_Controller_Base
{
    /**
     * Отрисовка формы
     */
    protected function _showAction()
    {
        $params = array(
            'images' => $this->_getRequest()->get('images'),
            'thumbs' => $this->_getRequest()->get('thumbs'),
            'title' => $this->_getRequest()->get('title'),
            'descript' => $this->_getRequest()->get('descript')
        );
        if(!isset($params['thumbs']) || !$params['thumbs'])
            $params['thumbs'] = $params['images'];

        $viewer = new Module_Imageviewer_View_Imageviewer();
        $html = $viewer->generateViewer($params);

        return array(
            'result' => 1,
            'html' => $html
        );
    }
}
?>