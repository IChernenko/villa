<?php
/**
 * Вью модуля просмотра изображений
 * 
 * @author Valkyria 
 */
class Module_Imageviewer_View_Imageviewer
{
    public function generateViewer($params)
    {
        $html = '<div id="imageViewerHide">
                 <div id="imageViewerWrap" class="b-modal">
                   <div id="imageViewer">
                     <div class="b-modal_close">X</div>';
        
        $html .='<div id="imageViewerImg">
                   <button class="arr-b-left" onclick="scroll('."'img', 'left'".');"></button>
                   <button class="arr-b-right" onclick="scroll('."'img', 'right'".');"></button>
                   <div id="imageViewerImgWrap">
                     <ul id="imageViewerImgList" class="image-viewer-scroll">';
        $images = $params['images'];
        
        foreach($images as $img)
        {
            if(is_array($img) && isset($img['img']))
                $html .= '<li><img src="'.$img['img'].'"/></li>';
            else
                $html .= '<li><img src="'.$img.'"/></li>';
        }
        
        $html .= '</ul></div></div>';
        
        $html .='<div id="imageViewerDescription">';
        $title = trim($params['title']);
        $html .='<span>'.$title.'</span>';
        if(isset($params['descript']) && $params['descript']) $descript = trim($params['descript']);
        else
        {
            $descript = '<div><ul>';
            foreach($images as $img)
            {
                if(is_array($img) && isset($img['comment']))
                    $descript .= '<li>'.$img['comment'].'</li>';
                else
                    $descript .= '<li></li>';
            }
            $descript .= '</ul></div>';
        }
        $html .= $descript.'</div>';
        
        $html .= '<div id="imageViewerThumbs">
                    <div class="image-viewer-button-wrap">
                      <button class="arr-s-left" onclick="scroll('."'thumbs', 'left'".');"></button>
                    </div>';
        
        $html .= '<div id="imageViewerThumbsWrap">
                    <ul id="imageViewerThumbsList" class="image-viewer-scroll">';
        $thumbs = $params['thumbs'];
        foreach($thumbs as $key => $thumb)
        {
            if(is_array($thumb) && isset($thumb['img']))
                $html .= '<li index="'.$key.'"><img src="'.$thumb['img'].'"/></li>';
            else
                $html .= '<li index="'.$key.'"><img src="'.$thumb.'"/></li>';
        }
        $html .= '</ul></div>';
        
        $html .= '<div class="image-viewer-button-wrap">
                    <button class="arr-s-right" onclick="scroll('."'thumbs', 'right'".');"></button>
                  </div></div></div></div></div></div>';
        return $html;
    }
}
?>