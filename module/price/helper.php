<?php

class Module_Price_Helper
{
    public static function getFolder()
    {
        $folder = Dante_Lib_Config::get('app.uploadFolder').'/'.Dante_Lib_Config::get('price.folder').'/';
        return $folder;
    }
    
    public static function getFileName()
    { 
        $folder = self::getFolder();
        if(!is_dir($folder)) mkdir($folder, 0755);
        
        $dir = opendir($folder);
        if(!$dir) return false;
        
        $fileName = false;
        while(false !== ($file = readdir($dir)))  
            if(is_file($folder.$file)) 
            {
                $fileName = $file;
                break;
            }
        
        closedir($dir);
        return $fileName;
    }
    
    public static function removeCurrent()
    {
        if(!self::getFileName()) return false;
        
        $folder = self::getFolder();        
        $file = self::getFileName();
        unlink($folder.$file);
        
        return false;
    }
}

?>
