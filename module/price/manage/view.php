<?php

class Module_Price_Manage_View
{
    public function draw($vars)
    {
        $tpl = new Dante_Lib_Template();
        $tpl->_vars = $vars;
        
        $fileName = '../module/price/manage/template/table.html';
        return $tpl->draw($fileName);
    }
}

?>
