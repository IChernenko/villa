<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 9/30/13
 * Time: 4:50 PM
 * To change this template use File | Settings | File Templates.
 */

class Module_Price_Manage_Controller extends Dante_Controller_Base
{
    protected function _defaultAction() 
    {
        $uploader = new Module_Uploadify_Controller_Base();
        $view = new Module_Price_Manage_View();
        
        $file = Module_Price_Helper::getFileName();
        $vars = array(
            'swf_url' => $uploader->swf_url,
            'server_url' => $uploader->server_url,
            'file' => $file?$file:'-'
        );
        
        return $view->draw($vars);        
    }
    
    protected function _uploadAction()
    {
        if(isset($_FILES['Filedata']))
        {     
            Module_Price_Helper::removeCurrent();
            
            $uploadDir = Module_Price_Helper::getFolder();
            $newName = false;
            $pattern = '/[a-я]+/i';
            if(preg_match($pattern, $_FILES['Filedata']['name']))
            {
                $nameParts = explode('.', $_FILES['Filedata']['name']);
                $curName = $nameParts[count($nameParts)-2];
                $newName = Dante_Lib_Transliteration::transform($curName);
            }  
            
            $uploader = new Component_Swfupload_Upload();
            $uploader->upload($uploadDir);             
        }
        
        return array(
            'result' => 1
        );
    }
}