<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 9/30/13
 * Time: 1:00 AM
 * To change this template use File | Settings | File Templates.
 */

Module_Division_Manage_Service::create(array(
    'name'      => 'Прайс',
    'parent'    => 'adminka',
    'url'       => '/manage/price/',
    'is_active' => true,
    'groups'    => array('admin')
));