<?php

$url = Dante_Controller_Front::getRequest()->get_requested_url();

Dante_Lib_Router::addRule("/^\/manage\/price\/$/", array(
    'controller' => 'Module_Price_Manage_Controller'
));