<?php

class Module_Price_View
{
    public function draw($url)
    {
        $tpl = new Dante_Lib_Template();
        $tpl->url = $url;        
        $fileName = '../module/price/manage/template/table.html';
        
        $customTpl = Dante_Helper_App::getWsPath().'/price/link.html';
        if (file_exists($customTpl)) $fileName = $customTpl;
        
        return $tpl->draw($fileName);
    }
}

?>
