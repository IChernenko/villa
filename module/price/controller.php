<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 9/30/13
 * Time: 12:22 AM
 * To change this template use File | Settings | File Templates.
 */

class Module_Price_Controller extends Dante_Controller_Base{

    protected function _defaultAction() 
    {
        $folder = Module_Price_Helper::getFolder();
        $file = Module_Price_Helper::getFileName();
        if(!$file) return '';
        
        $url = $folder.$file;
        $view = new Module_Price_View();
        
        return $view->draw($url);        
    }
}