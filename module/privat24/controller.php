<?php
/**
 * Description of controller: test class for Privat24 API
 *
 * @author Valkyria
 */
class Module_Privat24_Controller extends Dante_Controller_Base
{    
    protected function _defaultAction()
    {
        $params = new Module_Privat24_Model();
        if($this->_getParam('urlSource', false))
        {
            $params->amount = (float)$this->_getParam('%1');
            $params->details = rawurldecode($this->_getParam('%2'));
            $params->ext_details = rawurldecode($this->_getParam('%3'));
        }
        else
        {
            $params->amount = (float)$this->_getParam('amount');
            $params->details = $this->_getParam('details');
            $params->ext_details = $this->_getParam('ext_details');
        }
        
        return $this->_getForm($params);
    }

    protected function _defaultAjaxAction()
    {
        $model = new Module_Privat24_Model();
        $model->amount = $this->_getRequest()->get_validate('amount', 'int', 0);
        $model->details = $this->_getRequest()->get_validate('details', 'text', '');
        $model->ext_details = $this->_getRequest()->get_validate('ext_details', 'text', '');
        
        return array(
            'result' => 1,
            'html' => $this->_getForm($model)
        );
    }
    
    protected function _getForm(Module_Privat24_Model $params)
    {        
        $params->currency = Dante_Lib_Config::get('privat24.currency');
        $params->merchant = Dante_Lib_Config::get('privat24.merchant_id');
        $params->order = time();
        $params->return_url = Dante_Lib_Config::get('privat24.return_url');
        $params->server_url = Dante_Lib_Config::get('privat24.server_url');   
        
        if(!Module_Privat24_Helper::checkCurrency($params->currency))
            return 'Невозможно завершить операцию. Неверно установлена валюта.';
        
        $view = new Module_Privat24_View();
        return $view->draw($params);
    }


    protected function _getResultAction()
    {
        return $this->_returnAction(false);
    }
    
    protected function _returnAction($return = true)
    {
        //Получение результата запроса. Временный код
        $pass = Dante_Lib_Config::get('privat24.merchant_pass');
        $payment = 'amt=1.0&ccy=UAH&details=назначение&ext_details=расширенное_назначение&'.
                    'pay_way=privat24&order=1&merchant=1&state=test&date=010514232610&ref=референс&'.
                    'sender_phone=+380971234567&payCountry=UA';
        $signature1 = sha1(md5($payment.$pass));
        //
        
        $signature2 = sha1(md5($payment.$pass));
        if($signature1 == $signature2)
        {
            $params = Module_Privat24_Helper::parsePayment($payment);
            $stateList = Module_Privat24_Helper::getStateList();
            $mapper = new Module_Privat24_Mapper();
            
            $model = new Module_Privat24_Manage_Model();
            $model->amount = $params['amt'];
            $model->currency = $params['ccy'];
            $model->details = $params['details'];
            $model->ext_details = $params['ext_details'];
            $model->order = $params['order'];
            $model->merchant = $params['merchant'];
            $model->state = (int)array_search($params['state'], $stateList);
            $model->date = $params['date'];
            $model->time = $params['time'];
            $model->ref = $params['ref'];
            $model->sender_phone = $params['sender_phone'];
            $model->pay_country = $params['payCountry'];
            
            $mapper->add($model);
            
            if($return) return 'Платеж проведен успешно';
        }
        else return 'Неверный ответ системы';
    }
}

?>
