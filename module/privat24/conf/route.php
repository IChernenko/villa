<?php

Dante_Lib_Router::addRule('/^\/privat24\/pay\/amt=([\d\.]*)_det=(.*)_ext=(.*)$/', array(
    'controller' => 'Module_Privat24_Controller',
    'params' => array('urlSource' => true)
));

Dante_Lib_Router::addRule('/^\/privat24\/result$/', array(
    'controller' => 'Module_Privat24_Controller',
    'action' => 'getResult'
));

Dante_Lib_Router::addRule('/^\/privat24\/return$/', array(
    'controller' => 'Module_Privat24_Controller',
    'action' => 'return'
));

Dante_Lib_Router::addRule('/^\/manage\/privat24\/$/', array(
    'controller' => 'Module_Privat24_Manage_Controller'
));

?>
