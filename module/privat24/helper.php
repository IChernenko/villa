<?php
/**
 * Description of helper
 *
 * @author Valkyria
 */
class Module_Privat24_Helper 
{
    public static function getStateList()
    {
        $list = array(
            1 => 'ok',
            2 => 'fail',
            3 => 'test'
        );
        return $list;
    }
    
    public static function checkCurrency($code)
    {
        $list = array('UAH', 'USD', 'EUR');
        if(in_array($code, $list)) return true;
        else return false;
    }
    
    public static function parsePayment($payment)
    {
        $varArray = explode('&', $payment);
        
        $result = array();
        foreach($varArray as $varStr)
        {
            $strParts = explode('=', $varStr);
            $key = trim($strParts[0]);
            $value = $strParts[1];
            if($key == 'date')
            {
                // Получаем дату из формата ddMMyyHHmmss
                $valArr = str_split($value, 2); 
                $day = $valArr[0];
                $month = $valArr[1];
                $year = '20'.$valArr[2];
                $hour = $valArr[3];
                $minute = $valArr[4];
                $second = $valArr[5];
                $valueTime = mktime($hour, $minute, $second, $month, $day, $year);
                $result['time'] = $valueTime;
                $value = mktime(0, 0, 0, $month, $day, $year);
            }
            $result[$key] = $value;
        }
        
        return $result;
    }
}

?>
