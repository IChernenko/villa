<?php
/**
 * Description of mapper
 *
 * @author Valkyria
 */
class Module_Privat24_Mapper extends Module_Handbooksgenerator_Mapper_Base
{
    protected $_tableName = '[%%]privat24_log';
    
    public function add(Module_Privat24_Manage_Model $model)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->amount = $model->amount;
        $table->currency = $model->currency;
        $table->details = $model->details;
        $table->ext_details = $model->ext_details;
        $table->order = $model->order;
        $table->merchant = $model->merchant;
        $table->date = $model->date;
        $table->time = $model->time;
        $table->state = $model->state;
        $table->ref = $model->ref;
        $table->sender_phone = $model->sender_phone;
        $table->pay_country = $model->pay_country;
        $table->insert();
    }
}

?>
