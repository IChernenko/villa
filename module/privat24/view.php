<?php
/**
 * Description of view
 *
 * @author Valkyria
 */
class Module_Privat24_View 
{
    public function draw($params)
    {
        $tpl = new Dante_Lib_Template();
        $tpl->params = $params;        
        $fileName = '../module/privat24/template/form.html';
        
        $customTpl = Dante_Helper_App::getWsPath().'/privat24/form.html';
        if (file_exists($customTpl)) $fileName = $customTpl;
        
        return $tpl->draw($fileName);
    }
}

?>
