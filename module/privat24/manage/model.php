<?php
/**
 * Description of model
 *
 * @author Valkyria
 */
class Module_Privat24_Manage_Model extends Module_Privat24_Model
{    
    public $sortable_rules=array(
        'name' => 'time',
        'type' => 'desc',
    );
    
    public $filter_rules=array(
        array(
            'name'=>'amount',
            'format'=>'`t1`.`%1$s` = "%2$s"'
        ),
        array(
            'name' => 'order',
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ),
        array(
            'name'=>'details',
            'format'=>'`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ),
        array(
            'name'=>'ext_details',
            'format'=>'`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ),
        array(
            'name' => 'date',
            'format' => 'date'
        ),
        array(
            'name' => 'state',
            'format' => '`t1`.`%1$s` = "%2$s"'
        ),
        array(
            'name' => 'ref',
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ),
        array(
            'name' => 'sender_phone',
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ),
        array(
            'name' => 'pay_country',
            'format' => '`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ),
    );
    
    public $date;
    
    public $time;
    
    public $state;
    
    public $ref;
    
    public $sender_phone;
    
    public $pay_country;
}

?>
