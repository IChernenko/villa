<?php
/**
 * Description of controller
 *
 * @author Valkyria
 */
class Module_Privat24_Manage_Controller extends Module_Handbooksgenerator_Controller_Base
{
    protected $_stateList;
    
    protected function _drawAction()
    {
        $mapper = new Module_Privat24_Mapper();
        $model = new Module_Privat24_Manage_Model();
        
        $params = $this->getParams($model);
        $list = $mapper->getRowsByParams($params);
        $this->_stateList = Module_Privat24_Helper::getStateList();
        
        $this->formEnable('module.privat24.manage.controller');
        $this->_setHead();
        $this->_setBody($list['rows']);
        
        $this->tfoot = array(
            array($this->paginator($list['count'], $params))      
        );
        
        return $this->generate();
    }
    
    protected function _setHead()
    {        
        $titles = array(
            array(
                'content' => '#',
                'sortable' => 'id',
                'params' => array(
                    'style' => array(
                        'width' => '90px'
                    )
                )
            ),
            array(
                'content' => 'Сумма',
                'sortable' => 'amount'
            ),
            array(
                'content' => 'Валюта',
            ),
            array(
                'content' => 'Назначение',
                'sortable' => 'details'
            ),
            array(
                'content' => 'Назначение (расширенное)',
                'sortable' => 'ext_details'
            ),
            array(
                'content' => 'Код операции',
                'sortable' => 'order'
            ),
            array(
                'content' => 'ID мерчанта',
            ),
            array(
                'content' => 'Статус',
                'sortable' => 'state'
            ),
            array(
                'content' => 'Дата',
                'sortable' => 'date'
            ),
            array(
                'content' => 'Референс',
                'sortable' => 'ref'
            ),
            array(
                'content' => 'Телефон плательщика',
                'sortable' => 'sender_phone'
            ),
            array(
                'content' => 'Страна',
                'sortable' => 'pay_country'
            ),
        );
        
        $filters = array(
            array(
                'type' => 'button_filter'
            ),
            'amount' => array(
                'type' => 'text',
                'value' => $this->_getRequest()->get('amount')
            ),
            NULL,
            'details' => array(
                'type' => 'text',
                'value' => $this->_getRequest()->get('details')
            ),
            'ext_details' => array(
                'type' => 'text',
                'value' => $this->_getRequest()->get('ext_details')
            ),
            'order' => array(
                'type' => 'text',
                'value' => $this->_getRequest()->get('order')
            ),
            NULL,
            'state' => array(
                'type' => 'combobox',
                'items' => $this->_stateList,
                'value' => $this->_getRequest()->get('state')
            ),
            'date' => array(
                'type' => 'datepick',
                'value' => $this->_getRequest()->get('date')
            ),
            'ref' => array(
                'type' => 'text',
                'value' => $this->_getRequest()->get('ref')
            ),
            'sender_phone' => array(
                'type' => 'text',
                'value' => $this->_getRequest()->get('sender_phone')
            ),
            'pay_country' => array(
                'type' => 'text',
                'value' => $this->_getRequest()->get('pay_country')
            ),
        );
        
        $this->thead = array(
            $titles, $filters
        );
    }
    
    protected function _setBody($rows)
    {
        foreach($rows as $row)
        {                      
            $cur = array();
            $cur['id'] = $row['id'];
            $cur['amount'] = $row['amount'];
            $cur['currency'] = $row['currency'];
            $cur['details'] = $row['details'];
            $cur['ext_details'] = $row['ext_details'];
            $cur['order'] = $row['order'];
            $cur['merchant'] = $row['merchant'];
            $cur['state'] = $this->_stateList[$row['state']];
            $cur['date'] = Dante_Helper_Date::converToDateType($row['time'], 1);
            $cur['ref'] = $row['ref'];
            $cur['sender_phone'] = $row['sender_phone'];
            $cur['pay_country'] = $row['pay_country'];
            
            $this->tbody[] = $cur;
        }
    }
}

?>
