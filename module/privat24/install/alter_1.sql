CREATE TABLE IF NOT EXISTS [%%]privat24_log (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `amount` DECIMAL(10,2) NOT NULL,
    `currency` VARCHAR(10) NOT NULL,
    `details` VARCHAR(100) NOT NULL,
    `ext_details` VARCHAR(100) NOT NULL,
    `order` VARCHAR(100) NOT NULL,
    `merchant` VARCHAR(100) NOT NULL,
    `state` VARCHAR(10) NOT NULL,
    `date` INT(11) NOT NULL,
    `time` INT(11) NOT NULL,
    `ref` VARCHAR(100) NOT NULL,
    `sender_phone` VARCHAR(100) NOT NULL,
    `pay_country` VARCHAR(50) NOT NULL,    
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 comment 'История платежей Приват24';