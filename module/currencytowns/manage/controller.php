<?php
/**
 * Description of controller
 *
 * @author Valkyria
 */

class Module_Currencytowns_Manage_Controller extends Module_Handbooksgenerator_Controller_Base 
{   
    protected function _init() 
    {
        parent::_init();
        $this->_mapper = new Module_Currencytowns_Manage_Mapper();
    }

    /**
     * отрисовка стартового html
     * @return string 
     */
    protected function _drawAction()
    {
        $model = new Module_Currencytowns_Manage_Model();
        $countryMapper = new Module_Currencycountries_Manage_Mapper();
        $countryList = $countryMapper->getNames();          
        
        $params = $this->getParams($model);
        $list = $this->_mapper->getRowsByParams($params);
        
        $this->formEnable('module.currencytowns.manage.controller');
        
        $titles = array(            
            array(
                'content' => 'ID',
//                'sortable' => 'id'
            ),
            array(
                'content' => 'Название',
//                'sortable' => 'title'
            ),
            array(
                'content' => 'Страна',
            ),
            'Функции'
        );
        
        $filters = array(
            NULL,
            'title' => array(
                'type' => 'text',
                'value' => $this->_getRequest()->get('title')
            ),
            'country_id' => array(
                'type' => 'combobox',
                'items' => $countryList,
                'value' => $this->_getRequest()->get('country_id')
            ),
            'button_filter'=>array(
                'type'=>'button_filter'
            )
        );
        
        $this->thead = array($titles, $filters);
        
        foreach($list['rows'] as $row)
        {
            $cur = array();
            $cur['id'] = $row['id'];
            $cur['title'] = $row['title'];
            $cur['country'] = $countryList[$row['country_id']];     
            $cur['buttons'] = array(
                'id' => $row['id'],
                'type' => array('edit', 'del'),
            );  
            $this->tbody[$cur['id']] = $cur;
        }
        
        $this->tfoot = array(
            array(
                $this->paginator($list['count'], $params)
            ),
            array(
                'button_filter' => array(
                    'type' => array('edit'),                    
                )
             )  
        );
        
        return $this->generate();
    }
    
    protected function _drawFormAction()
    {
        $id = $this->_getRequest()->get_validate('id', 'int', 0);   
        $lang = $this->_getRequest()->get_validate('set_lang', 'int', 1);
        
        $model = $this->_mapper->getById($id, $lang);
        $view = new Module_Currencytowns_Manage_View();
        
        $countryMapper = new Module_Currencycountries_Manage_Mapper();
        $langMapper = new Module_Lang_Mapper();
        $view->langList = $langMapper->getList();
        $view->countryList = $countryMapper->getNames();   
        
        return array(
            'result' => 1,
            'html' => $view->drawForm($model)
        );
    }


    /**
     * редактирование/добавление/удаление записей грида
     */
    protected function _editAction() 
    {
        $model = new Module_Currencytowns_Manage_Model();
        $model->id = $this->_getRequest()->get('id');
        $model->title = $this->_getRequest()->get('disp_name');
        $model->country_id = $this->_getRequest()->get('country_id');
        $model->lang_id = $this->_getRequest()->get('set_lang');
        $model->disp_name = $this->_getRequest()->get('disp_name');
        
        $id = $this->_mapper->apply($model);

        for ( $i = 0; $i < 13;  $i++ ) {
            $model = new Module_Currencytowns_Manage_Model();
            $model->id = $this->_getRequest()->get('id');
            $model->title = $this->_getRequest()->get('disp_name');
            $model->country_id = $this->_getRequest()->get('country_id');
            $model->lang_id = $i;
            $model->disp_name = $this->_getRequest()->get('disp_name');

            $this->_mapper->apply($model);
        }

        $table = new Dante_Lib_Orm_Table('tst_currencytowns');

        $table->title = $this->_getRequest()->get('disp_name');
        $model->id = $table->update(array('id' => $this->_getRequest()->get('id')));

        
        return array(
            'result' => 1,
            'id' => $id
        );
    }

    // Добавление города в админке
    protected function _addTownAction()
    {
        $request = $this->_getRequest();
        $langId = $this->_getRequest()->get_validate('set_lang', 'int', 1);
        $model = new Module_Currencytowns_Manage_Model();

        $model->title = $request->get('townDispName');
        $model->country_id = $request->get('townCountryId');
        $model->lang_id = $langId;
        $model->disp_name = $request->get('townDispName');

        $id = $this->_mapper->addTown($model);

        for ( $i = 0; $i < 13;  $i++ ) {
            $model = new Module_Currencytowns_Manage_Model();
            $model->id = $id;
            $model->title = $request->get('townDispName');
            $model->country_id = $request->get('townCountryId');
            $model->lang_id = $i;
            $model->disp_name = $request->get('townDispName');

            $this->_mapper->apply($model);
        }



        return array(
            'result' => 1,
            'id' => $id
        );
    }

    
}

?>
