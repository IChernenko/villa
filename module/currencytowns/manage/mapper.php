<?php

/**
 * Description of mapper
 *
 * @author Valkyria
 */
class Module_Currencytowns_Manage_Mapper extends Module_Handbooksgenerator_Mapper_Base
{    
    protected $_tableName = '[%%]currencytowns';
    protected $_tableNameLang = '[%%]currencytowns_lang';
    
    public function getById($id, $lang = 1)
    {
        $model = new Module_Currencytowns_Manage_Model();
        if(!$id) return $model;

        $sql = "
          SELECT * FROM ".$this->_tableName." AS a, ".$this->_tableNameLang." AS b
          WHERE
              a.id =".$id." AND
              a.id=b.town_id AND
              b.lang_id = ".$lang."";

        $f = Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
        
        $model->id = $f['id'];
        $model->title = $f['title'];
        $model->country_id = $f['country_id'];
        $model->lang_id = $lang;
        $model->disp_name = $f['disp_name'];
        
        return $model;
    }
    
    public function apply(Module_Currencytowns_Manage_Model $model)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->title = $model->title;
        $table->country_id = $model->country_id;
        
        if($model->id) $table->update(array('id' => $model->id));
        else $model->id = $table->insert();
        
        $tableLang = new Dante_Lib_Orm_Table($this->_tableNameLang);
        $tableLang->town_id = $model->id;
        $tableLang->lang_id = $model->lang_id;
        $tableLang->disp_name = $model->disp_name;
        
        $tableLang->apply(array(
            'town_id' => $model->id,
            'lang_id' => $model->lang_id
        ));
        
        return $model->id;
    }

    // Добавление записей для добавляемого города
    // (по одной для каждой языковой локали сайта)
    public function addTown(Module_Currencytowns_Manage_Model $model)
    {
        $langMapper = new Module_Lang_Mapper();
        $langList = $langMapper->getList();
        $table = new Dante_Lib_Orm_Table($this->_tableName);

        $table->title = $model->title;
        $table->country_id = $model->country_id;
        $model->id = $table->insert();

        foreach($langList as $item)
        {
            $tableLang = new Dante_Lib_Orm_Table($this->_tableNameLang);
            $languageId = $item->id;
            if($model->lang_id == $languageId)
            {
                $tableLang->town_id = $model->id;
                $tableLang->lang_id = $model->lang_id;
                $tableLang->disp_name = $model->disp_name;
            }
            else
            {
                $tableLang->town_id = $model->id;
                $tableLang->lang_id = $languageId;
                $tableLang->disp_name = '';
            }
            $tableLang->insert();
        }
        return $model->id;
    }
    
    public function getNames($countryId = false)
    {
        $this->paginator = false;
        
        $params = false;
        if($countryId)
        {
            $params = array(
                'where' => array(
                    "`t1`.`country_id` = {$countryId}"
                )
            );
        }
        
        $rows = $this->getRowsByParams($params);
        
        $list = array();
        foreach($rows['rows'] as $id => $country)
        {
            $list[$id] = $country['title'];
        }
        return $list;
        
        $this->paginator = true;
    }
}

?>
