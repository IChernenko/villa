<?php
/**
 * Description of view
 *
 * @author Valkyria
 */
class Module_Currencytowns_Manage_View 
{
    public $langList;
    
    public $countryList;
    
    public function drawForm($model)
    {
        $tpl = new Dante_Lib_Template();
        $filePath = '../module/currencytowns/manage/template/form.html';
        
        $tpl->langList = $this->langList;
        $tpl->countryList = $this->countryList;
        $tpl->model = $model;
        return $tpl->draw($filePath);
    }
}

?>
