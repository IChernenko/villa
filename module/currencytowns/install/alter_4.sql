CREATE TABLE IF NOT EXISTS [%%]currencytowns_lang (
  `town_id` INT(11) NOT NULL,
  `lang_id` INT(11) UNSIGNED NOT NULL,
  `disp_name` VARCHAR(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `[%%]currencytowns_lang` ADD CONSTRAINT `fk_currencytowns_lang_town_id`
    FOREIGN KEY (`town_id`) REFERENCES `[%%]currencytowns`(`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `[%%]currencytowns_lang` ADD CONSTRAINT `fk_currencytowns_lang_lang_id`
    FOREIGN KEY (`lang_id`) REFERENCES `[%%]languages`(`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;