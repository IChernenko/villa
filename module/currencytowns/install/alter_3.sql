CREATE TABLE IF NOT EXISTS [%%]currencytowns (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE [%%]currencytowns ADD
      CONSTRAINT fk_currencytowns_country_id
      FOREIGN KEY (`country_id`)
      REFERENCES [%%]currencycountries(id) ON DELETE CASCADE ON UPDATE CASCADE;