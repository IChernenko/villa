<?php
/**
 * Description of mapper
 *
 * @author Valkyria
 */
class Module_Currencytowns_Mapper 
{    
    protected $_tableName = '[%%]currencytowns';
    protected $_tableNameLang = '[%%]currencytowns_lang';
           
    public function getByName($name)
    {
        $sql = "SELECT `id` FROM {$this->_tableName} WHERE `title`";
        if(is_array($name))
        {
            $nameStr = implode($name, "', '");
            $sql .= " IN ('{$nameStr}')";
        }
        else
            $sql .= " = {$name}";
        
        return Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'id');
    }
    
    public function getList($countryId = false, $lang = false)
    {
        if(!$lang) $lang = Module_Lang_Helper::getCurrent();
        
        $sql = "SELECT * FROM {$this->_tableName} AS `t`
                LEFT JOIN {$this->_tableNameLang} AS `tl`
                ON(`t`.`id` = `tl`.`town_id` AND `tl`.`lang_id` = {$lang})";
        
        if($countryId)
            $sql .= " WHERE `t`.`country_id` = {$countryId}";
        
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $list = array();

        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) 
        {
            $list[$f['id']] = $f['disp_name']?$f['disp_name']:$f['title'];
        }
        return $list;
    }
}

?>
