currencytowns =
{
    editSubmit: function(obj)
    {
        if($(obj).hasClass('disabled')) return false;
        $('.func').addClass('disabled');

        var data = $('#editTown').serializeArray();
        $.ajax({
            type: 'POST',
            data: data,
            url: '/ajax.php',
            dataType : "json",
            success: function (data){
                if(data.result==1)
                {
                    $('#modelID').val(data.id);
                    jAlert('Изменения сохранены', 'Сообщение', function(){
                        $('.func').removeClass('disabled');
                    });
                }
                else console.log(data);
            }
        });
    },

    // Добавление города в справочник
    addTown: function()
    {
        // Проверка корректности введенныъх данных
        var isValidate = true;
        jQuery('#editTown table input').each(function()
        {
           if(jQuery(this).val() == '')
           {
               jQuery(this).removeClass('successInput');
               jQuery(this).addClass('errorInput');
               isValidate = false;
           }
           else if(jQuery(this).val())
           {
               jQuery(this).removeClass('errorInput');
               jQuery(this).addClass('successInput');
           }
        });

        if(!isValidate)
        {
            jQuery('#alertBlock #alertMessage').text('Заполните все обязательные поля');
            jQuery('#alertBlock').fadeIn('slow');
            return false;
        }
        jQuery('#alertBlock #alertMessage').text();
        jQuery('#alertBlock').fadeOut('slow');

        var townTitle = jQuery('#townTitle').val();
        var townDispName = jQuery('#townDispName').val();
        var townCountryId = jQuery('#townCountryId').val();

        jQuery.ajax({
            url: 'ajax.php',
            type: 'post',
            dataType: 'json',
            data:
            {
                controller: 'module.currencytowns.manage.controller',
                action: 'addTown',

                townTitle: townTitle,
                townDispName: townDispName,
                townCountryId: townCountryId
            },
            success:function(data)
            {
                if(data.result == 1)
                {
                    jAlert('Изменения сохранены', 'Сообщение');
                    jQuery('#modalWindow').arcticmodal('close');
                }
                else console.log(data);
            }
        });

    },

    changeLang: function(obj)
    {
        if($(obj).hasClass('disabled')) return false;
        $('.func').addClass('disabled');

        var data = {
            controller: 'module.currencytowns.manage.controller',
            action: 'drawForm',
            id: $('#modelID').val(),
            set_lang: $(obj).val()
        }

        $.ajax({
            url: 'ajax.php',
            type: 'post',
            dataType: 'json',
            data: data,
            success:function(data)
            {
                if(data.result == 1)
                {
                    $('.modal-body').html(data.html);
                }
                else console.log(data);
            }
        });
    }
}