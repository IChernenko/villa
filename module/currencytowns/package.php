<?php
global $package;
$package = array(
    'version' => '4',
    'name' => 'module.currencytowns',
    'dependence' => array(
        'lib.jgrid',
        'module.auth',
        'module.lang',
        'component.jalerts',
        'module.currencycountries'
    ),
    'js' => array(
        '../module/currencytowns/js/currencytowns.js'
    )

);