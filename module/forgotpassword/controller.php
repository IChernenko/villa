<?php
/**
 * Created by PhpStorm.
 * User: dorian
 * Date: 4/23/14
 * Time: 5:00 PM
 */

class Module_Forgotpassword_Controller extends Dante_Controller_Base {

    protected function _defaultAction() {
        $tpl = new Dante_Lib_Template();
        return $tpl->draw('../module/forgotpassword/tpl/form.html');
    }

    protected function _forgotAction() {
        $mail = $this->_getRequest()->get('email');
        if($mail != '')
        {
            $profileMapper = new Module_Forgotpassword_Mapper();
            if($profileMapper->CheckByEmail($mail))
            {
                $pass = $profileMapper->changePassword($mail);
                $service = new Module_Mail_Service_Sender();
                $message    = "<h2>Новый пароль от webradast!</h2></br><b>".$pass."</b></br><i>Не отвечайте на это письмо</i>";
                $subject    = 'Новый пароль от webradast';
                $to         =$mail;
                $service->send($to, $subject, $message, array(
                    'fromName' => Dante_Lib_Config::get('smtp.from'),
                    'fromEmail' => Dante_Lib_Config::get('smtp.email')
                ));
                $result = 1;
                $msg = 'Пароль изменень и выслан вам по почте.';
            }
            else
            {
                $result = 0;
                $msg = 'Такого пользователя не существует.';
            }
        }
        else
        {
            $result = 0;
            $msg = 'Не введен email.';
        }
        return Array('result' => $result, 'msg' => $msg);
    }
} 