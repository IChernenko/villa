<?php
$url = Dante_Controller_Front::getRequest()->get_requested_url();

Dante_Lib_Router::addRule('/^\/forgot_password\/$/', array(
    'controller' => 'Module_Forgotpassword_Controller'
));