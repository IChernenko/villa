 forgotpassword = {
    change: function(){
        var mail = $('#email').val();

        $.ajax({
            type: 'POST',
            data: {
                controller: 'module.forgotpassword.controller',
                action: 'forgot',
                email: mail
            },
            url: '/ajax.php',
            dataType : "json",
            success: function (data){
                if (data.result == 1) {
                    $('#cap').html(data.msg);
                    $('#but').html('Войти с новым паролем');
                    $('#but').attr('href','/login/');
                    $('#but').attr('onclick','');
                }
                else
                {
                    $('#cap').html("<b>"+data.msg + "</b>" + "</br>"+ $('#cap').html());
                }
            }
        });
    }
}