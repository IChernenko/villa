<?php
/**
 * Маппер рейтингов
 * User: dorian
 * Date: 17.04.12
 * Time: 20:20
 * To change this template use File | Settings | File Templates.
 */
class Module_Rating_Mapper_Rating
{
    /**
     * Сохранение рейтинга
     * @param Module_Rating_Model_Rating $rating
     */
    public function save(Module_Rating_Model_Rating $rating) {
        // проверяем а нет ли у нас уже такого рейтинга
        $exitedRating = $this->get($rating->entityId, $rating->entityTypeId);

        $table = new Dante_Lib_Orm_Table('[%%]rating');
        if ($rating->id) $table->id = $rating->id;
        $table->rating = $rating->rating;
        $table->entity_type_id = $rating->entityTypeId;
        $table->entity_id = $rating->entityId;
        $table->apply(
            array('entity_type_id' => $rating->entityTypeId, 'entity_id' => $rating->entityId)
        );

        $rating->id = $table->id;

        // логируем что человек уже голосовал
        $this->_addLog($rating->id);
    }

    protected function _addLog($objId) {
        $table = new Dante_Lib_Orm_Table('[%%]rating_log');
        $table->obj_id = $objId;
        $table->uid = Dante_Helper_App::getUid();
        $table->sid = Dante_Helper_App::getSid();
        $table->ip = Dante_Lib_Server::get_remote_ip();
        return $table->insert();
    }

    /**
     *
     * @param int $entityId
     * @param int $entityTypeId
     * @return bool|Module_Rating_Model_Rating
     */
    public function get($entityId, $entityTypeId) {
        $table = new Dante_Lib_Orm_Table('[%%]rating');
        $table->select(array('entity_type_id' => $entityTypeId, 'entity_id' => $entityId));
        if ($table->getNumRows() == 0) return false;

        $rating = new Module_Rating_Model_Rating();
        $rating->id = $table->id;
        $rating->rating = $table->rating;
        $rating->entityTypeId = $table->entity_type_id;
        $rating->entityId = $table->entity_id;

        return $rating;
    }
}
