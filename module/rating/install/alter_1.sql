drop table if exists `[%%]rating`;

CREATE TABLE `[%%]rating` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rating` int(11) DEFAULT 0,
  `vote_count` int(11) DEFAULT 0,
  `entity_type_id` int(11) DEFAULT NULL,
  `entity_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT="Система рейтингов";

drop table if exists `[%%]rating_log`;
CREATE TABLE `[%%]rating_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `obj_id` int(11) unsigned DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `sid` varchar(32) DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT="Лог рейтингов";

ALTER TABLE `[%%]rating_log` ADD
      CONSTRAINT fk_rating_log_obj_id
      FOREIGN  KEY (obj_id)
      REFERENCES [%%]rating(id) ON DELETE CASCADE ON UPDATE CASCADE;
