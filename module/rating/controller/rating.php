<?php
/**
 * Контроллер рейтингов
 * User: dorian
 * Date: 17.04.12
 * Time: 20:05
 * To change this template use File | Settings | File Templates.
 */
class Module_Rating_Controller_Rating extends Dante_Controller_Base {

    /**
     * Сохранение рейтинга
     */
    protected function _saveAction() {
        $value = (int)$this->_getRequest()->get('value');
        $entityId = (int)$this->_getRequest()->get('entityId');

        $rating = new Module_Rating_Model_Rating();
        $rating->rating = $value;
        $rating->entityId = $entityId;
        $rating->entityTypeId = 1;

        $mapper = new Module_Rating_Mapper_Rating();

        return array('result'=>$mapper->save($rating));
    }

}
