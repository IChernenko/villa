<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 9/27/13
 * Time: 5:54 PM
 * To change this template use File | Settings | File Templates.
 */

class Module_Breadcrumbs_Controller extends Dante_Controller_Base {
    protected function _defaultAction() {

        $items = new Module_Breadcrumbs_Items();
        $items->add('/', 'Главная');


        Dante_Lib_Observer_Helper::fireEvent('breadcrumbs.draw', $items);

        $view = new Module_Breadcrumbs_View();
        return $view->draw($items);
        return 'breadcrumbs2';
    }
}