<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 9/27/13
 * Time: 6:25 PM
 * To change this template use File | Settings | File Templates.
 */

class Module_Breadcrumbs_Items {
    protected $_items = array();

    public function add($url, $caption, $isLink=true) {
        $this->_items[] = array(
            'url'       => $url,
            'caption'   => $caption,
            'link'      => $isLink
        );
    }

    public function get() {
        return $this->_items;
    }
}