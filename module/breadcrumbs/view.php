<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 9/27/13
 * Time: 6:08 PM
 * To change this template use File | Settings | File Templates.
 */

class Module_Breadcrumbs_View {

    public function draw($items) {

        $tplFileName = '../module/breadcrumbs/template/breadcrumbs.html';
        $customTpl = Dante_Helper_App::getWsPath().'/breadcrumbs/breadcrumbs.html';
        if (file_exists($customTpl)) $tplFileName = $customTpl;

        $tpl = new Dante_Lib_Template();
        $tpl->items = $items;

        return $tpl->draw($tplFileName);
    }
}