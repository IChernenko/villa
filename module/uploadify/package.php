<?php
global $package;
$package = array(
    'version' => '1',
    'name'=>'module.uploadify',
    'js' => array(
        '../module/uploadify/js/jquery.uploadify.min.js',
        
    ),
    'css' => array(
        '../module/uploadify/css/uploadify.css',
    )
);