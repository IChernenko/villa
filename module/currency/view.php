<?php

class Module_Currency_View
{
    public function draw($list, $current)
    {
        $tplFileName = '../module/currency/template/currency.html';
        
        $customTpl = Dante_Helper_App::getWsPath().'/currency/currency.html';
        if (file_exists($customTpl)) 
            $tplFileName = $customTpl;        
        
        $tpl = new Dante_Lib_Template();
        $tpl->list = $list;
        $tpl->current = $current;
        
        return $tpl->draw($tplFileName);
    }
}

?>