currency = {
    formInit: function()
    {
        if($('#is_basic').is(':checked'))
        {
            $('#is_basic').change(function(){
                var msg = '<b>Вы не можете сбросить значение базовой валюты, так как это нарушит работу системы.</b><br/>'+
                    'Чтобы изменить базовую валюту, откройте окно нужной валюты и установите ее в качестве базовой.';
                jAlert(msg, 'Ошибка', function(){
                    $('#is_basic').prop('checked', true);
                });
            });
        }
        
        if($('#is_default').is(':checked'))
        {
            $('#is_default').change(function(){
                var msg = '<b>Вы не можете сбросить значение валюты по умолчанию, так как это нарушит работу системы.</b><br/>'+
                    'Чтобы изменить валюту по умолчанию, откройте окно нужной валюты и поставьте галочку в соответствующем поле.';
                jAlert(msg, 'Ошибка', function(){
                    $('#is_default').prop('checked', true);
                });
            });
        }
    },
    formSubmit: function()
    {
        var data = $('#editCurrency').serializeArray();
        $.ajax({
            type: 'POST',
            url: '/ajax.php',
            data: data,
            dataType : "json",
            success: function(data){
                if(data.result == 1)
                {
                    $('#modalWindow').arcticmodal('close');
                }
                else jAlert(data.message, 'Ошибка');
            }
        });
    },
    openExchange: function(elem, opened)
    {
        data = {            
            controller: 'module.currency.manage.controller',
            action: 'drawExchange'
        };
        if(elem) data.exchange_base = $(elem).val();
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            dataType:"json",
            data: data,
            success:function(data)
            {
                if(data.result == 1)
                {
                    $('#modalWindow .modal-body').html(data.html);
                    if(!opened) 
                        $('#modalWindow').arcticmodal({
                            afterClose: function(){
                                handbooks.formSubmit();
                            }
                        });
                }                
            }
        });
    },
    submitExchange: function()
    {
        var data = $('#editExchange').serializeArray();
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: data,
            dataType: 'JSON',
            success: function(data)
            {
                if(data.result == 1)
                {
                    jAlert('Курс валют успешно изменен', 'Сообщение');
                }
                else console.log(data);
            }
        });
    }
}