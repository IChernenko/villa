/**
 *  Объект для работы с валютой со стороны клиента
 */
currencyClient = {
    /**
     * Функция смены валюты
     * 
     * @param {string} code - Код валюты
     */
    change: function(code)
    {
        $.ajax({
            type: "POST",
            url: '/ajax.php',
            data: {
                controller: "module.currency.controller",
                action: 'change',
                code: code
            },
            success: function(data) {
                if (data.result == 1) {
                    location.reload();
                }
            }
        });
    }
}