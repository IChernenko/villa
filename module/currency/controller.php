<?php

class Module_Currency_Controller extends Dante_Controller_Base
{
    protected function _defaultAction() 
    {
        $mapper = new Module_Currency_Mapper();
        $session = new Dante_Lib_Session();
        
        $list = $mapper->getList();
        if($session->get('currency')) 
        {
            $current = $session->get('currency');
        }
        else
        {
            $current = $mapper->getDefaultCurrency();
            $session->set('currency', $current);
        } 
        
        $view = new Module_Currency_View();
        return $view->draw($list, $current);
    }
    
    protected function _changeAction()
    {
        $code = $this->_getRequest()->get('code');
        $mapper = new Module_Currency_Mapper();
        $newCurrency = $mapper->getByCode($code);
        
        $session = new Dante_Lib_Session;
        $session->set('currency', $newCurrency);
        
        return array(
            'result' => 1
        );
    }
}

?>