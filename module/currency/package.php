<?php
global $package;
$package = array(
    'version' => '9',
    'name' => 'module.currency',
    'dependence' => array(
        'lib.jgrid',
        'module.auth',
        'module.lang',
        'component.jalerts'
    ),
    'js' => array(
        '../module/currency/js/manage/currency.js',
        '../module/currency/js/currencyClient.js'
    )

);