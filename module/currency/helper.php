<?php



/**
 * Description of helper
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Currency_Helper 
{
    protected static $_lastCustomConvertId = false;
    protected static $_lastCustomConvertCode = '';
    protected static $_lastCustomConvertFactor = 1;

    /**
     * Выборка валют
     * @return array
     */
    public static function getInfo() {
        $mapper = new Module_Currency_Mapper();
        return $mapper->getInfo(Module_Lang_Helper::getCurrent());
    }
    
    public static function getDefaultCurrency(){
        $mapper = new Module_Currency_Mapper();
        return $mapper->getDefaultCurrency();
    }
    
    public static function getCurrentCurrency()
    {
        $session = new Dante_Lib_Session();
        return $session->get('currency');
    }
    
    public static function getBasicCurrency()
    {
        $mapper = new Module_Currency_Mapper();
        return $mapper->getBasicCurrency();
    }
    
    public static function getExchangeBase()
    {
        $mapper = new Module_Currency_Mapper();
        return $mapper->getExchangeBase();
    }

    public static function convert($value)
    {
        $current = self::getCurrentCurrency();
        if(!$current) $current = self::getDefaultCurrency();
        if(!$current) $current = array('factor' => 1);
        
        $pres = (int)Dante_Lib_Config::get('currency.precision');
        
        return round($value * $current['factor'], $pres);
    }
    
    /**
     * Конвертация в указанную валюту
     * @param float $value
     * @param int | string $currency - валюта (ид или сокращение)
     * @param boolean $byId - по ид или по сокращению искать валюту
     * @return type 
     */
    public static function convertTo($value, $currency, $byId = false)
    {               
        $factor = self::_getCustomFactor($currency, $byId);
        $pres = (int)Dante_Lib_Config::get('currency.precision');
        
        return round($value * $factor, $pres);
    }
    
    public static function deconvert($value)
    {
        $current = self::getCurrentCurrency();
        if(!$current) $current = self::getDefaultCurrency();
        if(!$current) $current = array('factor' => 1);
        
        $pres = (int)Dante_Lib_Config::get('currency.precision');
        
        return round($value / $current['factor'], $pres);
    }
    
    public static function deconvertFrom($value, $currency, $byId = false)
    {
        $factor = self::_getCustomFactor($currency, $byId);
        $pres = (int)Dante_Lib_Config::get('currency.precision');
        
        return round($value / $factor, $pres);
    }
    
    protected static function _getCustomFactor($currency, $byId)
    {
        $mapper = new Module_Currency_Mapper();
        $factor = false;
        
        if(($byId && $currency == self::$_lastCustomConvertId) || (!$byId && $currency == self::$_lastCustomConvertCode))
        {
            $factor = self::$_lastCustomConvertFactor;
        }
        else
        {
            if($byId)
            {
                $custom = $mapper->getById($currency);
                self::$_lastCustomConvertId = $currency;
            }
            else
            {
                $custom = $mapper->getByCode($currency);
                self::$_lastCustomConvertCode = $currency;
            }            
            $factor = $custom['factor'];
        }
        
        if(!$factor) $factor = 1;
        
        self::$_lastCustomConvertFactor = $factor;
        
        return $factor;
    }
}

?>
