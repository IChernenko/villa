<?php

class Module_Currency_Mapper
{    
    protected $_tableName = '[%%]currency';
    protected $_tableLang = '[%%]currency_lang';
    
    public function getList()
    {
        //выгребаем строки
        $sql = "SELECT * FROM ".$this->_tableName;

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        if(!$r) return false;
        
        $rowsArray = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $rowsArray[$f['id']] = $f;
        }
        return $rowsArray;
    }
    
    public function getCodesList()
    {
        $list = $this->getList();
        foreach($list as &$item)
        {
            $item = $item['reduction'];
        }
        return $list;
    }
    
    public function getInfo($langId) 
    {
        $table = new Dante_Lib_Orm_Table('[%%]currency_lang');
        $table->select(array('lang_id'=>$langId));
        $currency = $table->currency;
        if (!$currency){
            $currency = $this->getDefaultCurrency();
            $currency['name'] = $currency['reduction'];
            return $currency;
        }
        $table = new Dante_Lib_Orm_Table('[%%]currency');
        $table->select(array('id'=>$currency));
        return array(
            'name' => $table->reduction,
            'factor' => (float)$table->factor
        );
    }
    
    public function getDefaultCurrency() {
        $orm = $table = new Dante_Lib_Orm_Table($this->_tableName);
        return $orm->select(array('is_default'=>1))->getFields();
    }
    
    public function getBasicCurrency(){
        $orm = $table = new Dante_Lib_Orm_Table($this->_tableName);
        return $orm->select(array('is_basic'=>1))->getFields();
    }
    
    public function getExchangeBase(){
        $orm = $table = new Dante_Lib_Orm_Table($this->_tableName);
        return $orm->select(array('exchange_base'=>1))->getFields();
    }
    
    public function getByCode($code){
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        return $table->select(array('reduction'=>$code))->getFields();
    }
    
    public function getById($id){
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        return $table->select(array('id'=>$id))->getFields();
    }
}

?>
