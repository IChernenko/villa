<?php

/**
 * Description of callback
 *
 * @author Mort
 */
class Module_Currency_Manage_Mapper extends Module_Handbooksgenerator_Mapper_Base{
    
    protected $_tableName = '[%%]currency';
    protected $_tableLang = '[%%]currency_lang';   
    
    public function getList()
    {
        $full = $this->getRowsByParams();
        return $full['rows'];
    }
    
    public function getById($id)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->select(array('id'=>$id));
        
        $model = new Module_Currency_Manage_Model();
        $model->id = $table->id;
        $model->title = $table->title;
        $model->reduction = $table->reduction;
        $model->factor = $table->factor;
        $model->is_basic = $table->is_basic;
        $model->is_default = $table->is_default;
        $model->exchange_base = $table->exchange_base;
        
        return $model;
    }
    /**
     * добавление номера
     * @param Module_Currency_Manage_Model $params 
     */
    public function edit(Module_Currency_Manage_Model $model) 
    {     
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->id = $model->id;
        $table->title = $model->title;
        $table->reduction = $model->reduction;
        $table->is_basic = $model->is_basic;
        $table->is_default = $model->is_default;
        
        if($model->is_basic)
        {
            $this->clearBasic();
            
            $currentBasic = Module_Currency_Helper::getBasicCurrency();
            if($currentBasic['id'] != $model->id) $this->resetExchange();
            
            $table->factor = $model->factor;
        }
        if($model->is_default)
            $this->clearDefault();        

        $table->apply();
        
        return true;
    }
    

    public function clearDefault()
    {
        $sql = "UPDATE {$this->_tableName} SET `is_default` = 0";
        Dante_Lib_SQL_DB::get_instance()->open($sql);       
    }
    
    public function clearBasic()
    {
        $sql = "UPDATE {$this->_tableName} SET `is_basic` = 0";
        Dante_Lib_SQL_DB::get_instance()->open($sql);
    }
    
    public function resetExchange()
    {
        $sql = "UPDATE {$this->_tableName} SET `factor` = 1";
        Dante_Lib_SQL_DB::get_instance()->open($sql);
    }
    
    public function editFactor($id, $factor)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->factor = $factor;
        $table->update(array('id'=>$id));
    }
    
    public function saveExchangeBase($id)
    {
        $sql = "UPDATE {$this->_tableName} SET `exchange_base` = 0";
        Dante_Lib_SQL_DB::get_instance()->open($sql);
        
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->exchange_base = 1;
        $table->update(array('id'=>$id));
    }
    
    public function getNamesList()
    {
        $sql = "SELECT `id`, `name` FROM {$this->_tableName}";
        if(!$r = Dante_Lib_SQL_DB::get_instance()->open($sql)) return false;
        
        $list = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
            $list[$f['id']] = $f['name'];
        
        return $list;
    }
}

?>
