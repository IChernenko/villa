<?php
/**
 * Description of callback
 *
 * @author Mort
 */

class Module_Currency_Manage_Controller extends Module_Handbooksgenerator_Controller_Base {

    protected function _instanceMapper()
    {
        return new Module_Currency_Manage_Mapper();
    }
    /**
     * отрисовка стартового html
     * @return string 
     */
    protected function _drawAction()
    {
        $helper = new Module_Division_Helper();
        $helper->checkAccess();
        
        $mapper = $this->_instanceMapper();
        $list = $mapper->getList();
        
        $this->formEnable('module.currency.manage.controller');
        $this->_setHeader();
        $this->_setBody($list);
        
        $this->tfoot = array(
            array(
                '<a class="btn btn-info" onclick="currency.openExchange(false, false);">Курс валют</a>',
                'button_filter' => array(
                    "type" => array(
                        'edit'
                    ),                    
                )
            )
        );
        
        return $this->generate();
    }
    
    protected function _setHeader()
    { 
        $title = array(
            "#",
            'Название',
            'Cокр. название',
            'Базовая валюта',
            'По умолчанию',
            array(
                'content' =>'Функции',
                'params' => array(
                    'style' => array(
                        'width' => '120px'
                    )
                )                
            ),
        );
        
        $this->thead = array(
            $title
        );
    }
    
    protected function _setBody($list)
    {          
        foreach($list as $row){  
            $vars = array();
            $vars['id'] = $row['id'];
            $vars['title'] = $row['title'];
            $vars['reduction'] = $row['reduction'];
            $vars['is_basic'] = $row['is_basic']?'Да':'-';
            $vars['is_default'] = $row['is_default']?'Да':'-';
            
            $vars['buttons'] = array(
                'id'=>$row['id'],
                'type'=>array('edit', 'del'),                
            );
            $this->tbody[$row['id']] = $vars;
        }
    }
    
    protected function _drawFormAction() 
    {
        $mapper = new Module_Currency_Manage_Mapper();
        $id = $this->_getRequest()->get('id');
        if($id) $model = $mapper->getById($id);
        else $model = new Module_Currency_Manage_Model();

        $view = new Module_Currency_Manage_View();

        return array(
            'result' => 1,
            'html' => $view->drawForm($model)
        );
    }
    
    protected function _editAction() 
    {        
        $model = new Module_Currency_Manage_Model();
        $model->id = $this->_getRequest()->get('id');
        $model->title = $this->_getRequest()->get('title');
        $model->reduction = $this->_getRequest()->get('reduction');
        $model->is_basic = $this->_getRequest()->get('is_basic')?1:0; 
        $model->is_default = $this->_getRequest()->get('is_default')?1:0; 
        
        if($model->is_basic) $model->factor = 1;
        
        $mapper = new Module_Currency_Manage_Mapper();
        $mapper->edit($model);

        Dante_Lib_Observer_Helper::fireEvent('edit_exchange', $this->_instanceMapper()->getList());

        return array(
            'result' => 1
        );
    }
    

    protected function _delAction() 
    {
        $id = $this->_getRequest()->get('id');
        
        $mapper = new Module_Currency_Manage_Mapper();
        $mapper->del($id);
        
        return array(
            'result' => 1
        );
    }
    
    protected function _drawExchangeAction()
    {
        if(!$basic = Module_Currency_Helper::getBasicCurrency())
            return array(
                'result' => 1,
                'html' => 'Базовая валюта не выбрана. Установите базовую валюту перед тем, как вводить курс.'
            );
        
        $mapper = new Module_Currency_Manage_Mapper();
        $list = $mapper->getRowsByParams();          
        
        $basic = Module_Currency_Helper::getBasicCurrency();
        
        $exchangeBaseId = $this->_getRequest()->get('exchange_base');
        if($exchangeBaseId)
            $exchangeBase = $list['rows'][$exchangeBaseId];
        elseif($exchangeBaseRow = Module_Currency_Helper::getExchangeBase())
            $exchangeBase = $exchangeBaseRow;
        else
            $exchangeBase = current($list['rows']);
        
        $currencyList = array();
        foreach($list['rows'] as &$row)
        {
            if($row['id'] != $exchangeBase['id'])
                $row['factor'] = $exchangeBase['factor']/$row['factor']; 
            
            $row['factor'] = round($row['factor'], 5);
            
            $currencyList[$row['id']] = $row;
        }
        
        $view = new Module_Currency_Manage_View();
        return array(
            'result' => 1,
            'html' => $view->drawExchange($currencyList, $exchangeBase)
        );
    }
    
    protected function _editExchangeAction()
    {
        $mapper = new Module_Currency_Manage_Mapper();
        
        $basic = Module_Currency_Helper::getBasicCurrency();
        $exchangeBaseId = $this->_getRequest()->get('exchange_base');
                
        $currencyArr = $this->_getRequest()->get('currency');        
        
        $baseFactor = $currencyArr[$basic['id']];
        foreach($currencyArr as $id => $factor)
        {            
            if($id == $exchangeBaseId)
                $newFactor = $baseFactor;
            else
                $newFactor = $baseFactor/$factor;            
            
            $mapper->editFactor($id, $newFactor);
        }
        
        Dante_Lib_Observer_Helper::fireEvent('edit_exchange', $this->_instanceMapper()->getList());
        
        $mapper->saveExchangeBase($exchangeBaseId);
        
        return array(
            'result' => 1
        );
    }
    
}

?>