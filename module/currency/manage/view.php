<?php

class Module_Currency_Manage_View
{
    public function drawForm($model)
    {
        $tplName = '../module/currency/manage/template/form.html';
        $tpl = new Dante_Lib_Template();
        $tpl->model = $model;
        
        return $tpl->draw($tplName);
    }
    
    public function drawExchange($list, $base)
    {
        $tplName = '../module/currency/manage/template/exchange.html';
        $tpl = new Dante_Lib_Template();
        $tpl->list = $list;
        $tpl->base = $base;
        
        return $tpl->draw($tplName);
    }
}

?>
