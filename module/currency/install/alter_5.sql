ALTER TABLE  `[%%]currency` 
ADD  `default` TINYINT( 1 ) UNSIGNED NULL DEFAULT NULL 
COMMENT  'Определяем дефотную валюту';

INSERT INTO  `[%%]currency` (
`id` ,
`title` ,
`reduction` ,
`factor` ,
`default`
)
VALUES (
2 ,  '$',  'USD',  '1',  '1'
);