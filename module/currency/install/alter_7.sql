UPDATE `[%%]currency` SET `default` = 0 WHERE `default` = NULL;

ALTER TABLE `[%%]currency` CHANGE `default` `is_default` TINYINT(1) NOT NULL DEFAULT 0;
ALTER TABLE `[%%]currency` ADD `is_basic` TINYINT(1) NOT NULL DEFAULT 0 AFTER `is_default`;