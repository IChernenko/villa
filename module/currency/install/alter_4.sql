CREATE TABLE IF NOT EXISTS [%%]currency_lang (
  `id` int(11) NOT NULL,
  `lang_id` int(11) unsigned NOT NULL,
  `title` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='локализация типов валют';

ALTER TABLE [%%]currency_lang ADD
      CONSTRAINT fk_currency_lang_id
      FOREIGN KEY (`id`)
      REFERENCES [%%]currency(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE [%%]currency_lang ADD
      CONSTRAINT fk_currency_lang_lang_id
      FOREIGN KEY (`lang_id`)
      REFERENCES [%%]languages(id) ON DELETE CASCADE ON UPDATE CASCADE;