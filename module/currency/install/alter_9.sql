ALTER TABLE `[%%]currency` ADD `exchange_base` TINYINT(1) NOT NULL DEFAULT 0;

UPDATE `[%%]currency` SET `exchange_base` = 1 WHERE `id` = 1;