<?php
/**
 * Сервис для отправки писем.
 * @package service
 * @subpackage mail
 * @author
 */
class Module_Mail_Service_Sender{
    
    /**
     * отправляем письмо
     * @param string $to
     * @param string $subject
     * @param string $message
     * @return array
     */
    public function send($to, $subject, $message, $params=array())
    {

        if(Dante_Lib_Config::get('mail.disabled')) return array('result' => 1);

        $mime_type = 'text/html';
        if (isset($params['mimeType'])) $mime_type = $params['mimeType'];
        $charset = 'UTF-8';

        if (isset($params['fromName'])) {
            $from_name = $params['fromName'];
        }
        else $from_name = Dante_Lib_Config::get('smtp.from');

        if (isset($params['fromEmail'])) {
            $from_email = $params['fromEmail'];
        }
        else $from_email = Dante_Lib_Config::get('smtp.email');


        $un        = strtoupper(uniqid(time()));
        $from_name = '=?'.$charset.'?B?'.base64_encode($from_name).'?=';
        $head      = "From: {$from_name} <{$from_email}>\r\n";
        $head     .= "To: <{$to}>\r\n";
        $head     .= "Subject: {$subject}\r\n";
        $head     .= "X-Mailer: PHPMail Tool\r\n";
        $head     .= "Reply-To: {$from_name} <{$from_email}>\r\n";
        $head     .= "Mime-Version: 1.0\r\n";
        $head     .= "Content-Type: {$mime_type}; charset={$charset};";
        $head     .= "boundary=\"----------{$un}\"\r\n";

        @mail($to, $subject, $message, $head);

        return array('result' => 1);

//        $smtp = new Module_Mail_Transport_Smtp(
//            Dante_Lib_Config::get('smtp.host'),
//            Dante_Lib_Config::get('smtp.port'),
//            Dante_Lib_Config::get('smtp.login'),
//            Dante_Lib_Config::get('smtp.pass')
//        );
//
//        $smtp->setSender(Dante_Lib_Config::get('smtp.email'));
//        $smtp->setSenderName(Dante_Lib_Config::get('smtp.from'));
//
//        if (!$smtp->connect()) {
//            Lib_Log_File::debug('can not smtp->connect');
//            return array('result' => -2);
//        }
//
//        if (!$smtp->send($to, $subject, $message)) {
//            Lib_Log_File::debug('can not smtp->send');
//            return array('result' => -3);
//        }
//
//        return array('result' => 1);
    }
    
    public function send_mail($to, $from_mail, $from_name, $subject, $message, $file_name=false, $isHTML=true)
    {
        if(Dante_Lib_Config::get('mail.disabled')) return true;
        
        $type = $isHTML?'text/html':'text/plain';
        
        $charset = 'UTF-8';
        
        $from_name = "=?{$charset}?B?".base64_encode(convert_cyr_string($from_name, "w","k"))."?=";
        
        $bound = "*****SendMail*****";
        
        $headers = "From: ".$from_name." <".$from_mail.">\r\n";
        $headers .= "To: <$to>\r\n";
        $headers .= "Subject: $subject\r\n";
        $headers .= "Mime-Version: 1.0\r\n";
        
        if($file_name != false)
            $headers .= "Content-Type: multipart/mixed; boundary=\"".$bound."\"";
        
        $body = "\r\n\r\n--$bound\r\n";
        $body .= "Content-Type: {$type}; charset={$charset}\r\n";
        $body .= "Content-Transfer-Encoding: quoted-printable\r\n\r\n";
        $body .= $message;
       // Проверяем, есть ли файлы вложений       
       if (is_array($file_name) )
       {
           foreach ( $file_name as $value )
           {
               if($file = fopen($value,"rb"))
               {
                   $body .= "\r\n\r\n--$bound\r\n";
                   $body .= "Content-Type: application/octet-stream; ";
                   $body .= "name=\"".basename($value)."\"\r\n";
                   $body .= "Content-Transfer-Encoding:base64\r\n";
                   $body .= "Content-Disposition:attachment\r\n\r\n";
                   $body .= base64_encode(fread($file,filesize($value)));
               }
           }
       }
       $body .= "\r\n\r\n--$bound--\r\n\r\n";

       if(mail($to, $subject, $body, $headers)) return true;       
       return false;
    }

}