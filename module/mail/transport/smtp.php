<?php
/**
 * Компонент для отправки писем с помощью SMTP.
 * @package transport
 * @subpackage mail
 * @author Александр Бойко
 */
class Module_Mail_Transport_Smtp
{
    const CRLF = "\r\n";

    const MIME_VERSION = '1.0';

    const SECURITY_TYPE_NONE = 0;
    const SECURITY_TYPE_SSL  = 1;
    const SECURITY_TYPE_TLS  = 2;

    const CONTENT_TYPE_TEXT  = 'text/plain';
    const CONTENT_TYPE_HTML  = 'text/html';
    const CONTENT_TYPE_MULTI = 'multipart/alternative'; // NYI

    protected $_connection = null;

    protected $_server;
    protected $_port;
    protected $_username = null;
    protected $_password = null;
    protected $_security = null;

    protected $_sender     = 'mailer@localhost';
    protected $_senderName = null;
    protected $_headers    = array();

    public $charset     = 'utf8';
    public $contentType = self::CONTENT_TYPE_HTML;
    public $timeout     = 45;
    public $identity    = 'localhost';

    public function __construct($server, $port, $username = null, $password = null, $security = self::SECURITY_TYPE_NONE)
    {
        $this->_server = $server;
        $this->_port = $port;
        $this->_username = $username;
        $this->_password = $password;
        $this->_security = $security;
    }

    function __destruct()
    {
        if (!is_null($this->_connection)) {
            $this->_communicate('QUIT');
            fclose($this->_connection);
        }
    }

    public function connect()
    {
        if($this->_security == self::SECURITY_TYPE_SSL && strpos($this->_server, 'ssl://') !== 0) {
            $this->_server = "ssl://{$this->_server}";
        }

        $this->_connection = fsockopen($this->_server, $this->_port, $errno, $errstr, $this->timeout);

        if ($this->_readCode($this->_readServerResponse()) != '220') {
            return false;
        }

        return $this->_auth();
    }

    protected function _auth()
    {
        $this->_communicate("HELO {$this->identity}");

        if($this->_security == self::SECURITY_TYPE_TLS) {
            if ($this->_communicate('STARTTLS') != 220) {
                return false;
            }

            stream_socket_enable_crypto($this->_connection, true, STREAM_CRYPTO_METHOD_TLS_CLIENT);

            if ($this->_communicate("HELO {$this->identity}") != 250) {
                return false;
            }
        }

        if ($this->_username) {
            if ($this->_communicate('AUTH LOGIN') != 334) {
                return false;
            }

            if ($this->_communicate(base64_encode($this->_username)) != 334) {
                return false;
            }

            if ($this->_communicate(base64_encode($this->_password)) != 235) {
                return false;
            }
        }

        return true;
    }

    public function setSender($value)
    {
        $this->_sender = $value;
    }

    public function setSenderName($value)
    {
        $this->_senderName = $value;
    }

    public function addHeader($key, $header)
    {
        $this->_headers[$key] = $header;
    }

    public function getFullSender()
    {
        return is_null($this->_senderName) ? $this->_sender : "{$this->_senderName} <{$this->_sender}>";
    }

    public function send($to, $subject, $message)
    {
        $this->addHeader('Date',         Dante_Helper_Date::converToDateType(FALSE, FALSE, 'D, j M Y G:i:s +0000'));
        $this->addHeader('From',         $this->getFullSender());
        $this->addHeader('Reply-To',     $this->getFullSender());
        $this->addHeader('To',           $to);
        $this->addHeader('Subject',      $subject);
        $this->addHeader('MIME-Version', self::MIME_VERSION);

        /*
        if($this->contentType == "multipart/alternative") {
            $boundary = $this->generateBoundary();
            $message = $this->multipartMessage($message,$boundary);

            $this->addHeader('Content-Type', "{$this->contentType};");
            $email .= "    boundary=\"$boundary\"";
        } else {
        }
        */

        $this->addHeader('Content-Type', "{$this->contentType}; charset={$this->charset}");

        $this->_communicate("MAIL FROM: <{$this->_sender}>");
        $this->_communicate("RCPT TO: <{$to}>");
        $this->_communicate("DATA");
        
        $this->_communicate($this->_collectHeaders(), false);
        $this->_communicate($message, false);
        
        
        $communicate_result = $this->_communicate('.');
        
        Lib_Log_File::debug('communicate debug');
        Lib_Log_File::debug_item($communicate_result);
        
        if ($communicate_result != 250) {
            return false;
        }
                                    
        return true;
    }

    protected function _collectHeaders()
    {
        $result = '';

        foreach ($this->_headers as $key => $value) {
            $result .= "{$key}: {$value}" . self::CRLF;
        }

        return $result;
    }

    protected function _readServerResponse()
    {
        $data = '';

        while($str = fgets($this->_connection, 4096)) {
            $data .= $str;

            if(substr($str, 3, 1) == " ") {
                break;
            }
        }

        return $data;
    }

    protected function _readCode($text)
    {
        return substr($text, 0, strpos($text, ' '));
    }

    protected function _communicate($text, $read = true, $crlf = true)
    {
        if ($crlf) {
            $text .= self::CRLF;
        }

        fputs($this->_connection, $text);

        if ($read) {
            return $this->_readCode($this->_readServerResponse());
        }
    }
}
