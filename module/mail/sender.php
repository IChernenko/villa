<?php
/**
 * Компонент для отправки писем.
 * @package mail
 * @subpackage module
 * @author Александр Бойко
 */
class Module_Mail_Sender extends API_Module_Base {
    
    
    protected function _send_action()
    {
        $to      = Lib_Request::get('to');
        $subject = Lib_Request::get('subject');
        $message = Lib_Request::get('message');
        
        $service = new Module_Mail_Service_Sender();
        return $service->send($to, $subject, $message);
    }
}