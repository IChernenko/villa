<?php



/**
 * Description of controller
 *
 * @author dorian
 */
class Module_Home_Controller extends Dante_Controller_Base {

    protected function _getControllers() {
        if (file_exists('conf/home.php')) {
            include_once 'conf/home.php';
            global $homeControllers;
            return $homeControllers;
        }
        
        return array();
    }
    
    protected function _defaultAction() {
        $tpl = new Dante_Lib_Template();

        $controllers = $this->_getControllers();

        $request = Dante_Controller_Front::getRequest();

        foreach($controllers as $controllerAlias => $controllerName) {
            $currentController = new $controllerName();
            $currentController->setRequest($request);
            $tpl->$controllerAlias = $currentController->run();
        }
        
        return $tpl->draw('template/home.html');
    }
}

?>
