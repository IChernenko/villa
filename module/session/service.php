<?php


/**
 * Сервис для работы с сессиями
 *
 * @author dorian
 */
class Module_Session_Service {

    public static function generateSid() {
        return md5(rand(1, 1000000));
    }
	
	/**
	 * Запуск сессии для пользователя
	 * @param Module_User_Model $user 
	 */
	public function start(Module_User_Model $user)
	{
            $session = new Module_Session_Model();
            $session->uid = (int)$user->id;
            $session->session = Dante_Lib_Session::get('sid');
            $session->lastUpdate = time();
            $session->ip = $_SERVER['REMOTE_ADDR'];

            // стартуем сессию для пользователя
            $mapper = new Module_Session_Mapper();
            if (!$mapper->add($session)) {
                //throw new Exception('cant start session');
            }
            
            Dante_Lib_Session::set('sid', $session->session);
            Dante_Lib_Session::set('uid', $session->uid);
            
            return true;
	}
        
        public static function hasSession()
        {
            if (Dante_Lib_Session::get('uid') > 0 ) {
                return true;
            }
            
            return false;
        }

        /**
         * Получить идентификатор залогиненного пользователя
         * @return int
         */
        public function getUid() {
            return Dante_Lib_Session::get('uid');
        }

        public function getSid() {
            return Dante_Lib_Session::get('sid');
        }
        
        /**
         * Остановка сессии для заданного пользователя
         * @param Module_User_Model $user 
         */
        public function destroy()  // Module_User_Model $user
        {
            $session = new Module_Session_Model();
            $session->uid = (int)Dante_Lib_Session::get('uid');
            /*if ($user) {
                $session->uid = (int)$user->id;
            }
            else {
                $session->uid = (int)Lib_Session::get('uid');
            }*/
            
            $mapper = new Module_Session_Mapper();
            $mapper->delete($session);

            Dante_Lib_Session::remove('uid');
            Dante_Lib_Session::remove('sid');
            return true;
        }
}

?>