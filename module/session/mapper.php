<?php



/**
 * Маппер для работы с сессиями пользователя
 *
 * @author dorian
 */
class Module_Session_Mapper {
    
        protected $_tableName = '[%%]sessions';
	
        /**
         * Добавление новой сессии пользователя
         * 
         * @param App_Session_Model $session 
         */
	public function add(Module_Session_Model $session)
	{
            $table = new Dante_Lib_Orm_Table($this->_tableName);
            $table->uid = $session->uid;
            $table->session = $session->session;
            $table->last_update = $session->lastUpdate;
            $table->ip = $session->ip;
            return $table->apply(array('uid'=>$session->uid));
	}
        
        public function getUidBySid($sid) {
            $table = new Dante_Lib_Orm_Table($this->_tableName);
            $table->getByAttribute(array('session' => $sid))->fetch();
            return $table->uid;
        }
        
        /**
         * Удаление сессии
         * @param App_Session_Model $session 
         */
        public function delete(Module_Session_Model $session)
        {
            $table = new Dante_Lib_Orm_Table($this->_tableName);
            $table->delete(array('uid'=>$session->uid));
        }
}

?>