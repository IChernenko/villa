<?php
/**
 * Description of service
 *
 * @author Valkyria
 */
class Module_Registration_Service 
{
    public function sendRegEmail(Module_User_Model $user)
    {

        $service = new Module_Mail_Service_Sender();
        
        $from_name = Dante_Lib_Config::get('smtp.from');
        $from_mail = Dante_Lib_Config::get('smtp.email');  
        
        $subject = Dante_Lib_Config::get('registration.mailSubject');
        
        $tpl = new Dante_Lib_Template();
        $tpl->user = $user;
        
        $fileName = '../module/registration/template/mail.html';
        $customTpl = Dante_Helper_App::getWsPath().'/registration/mail.html';
        if (file_exists($customTpl)) {
            $fileName = $customTpl;
        }
        
        $message = $tpl->draw($fileName);
        
        $service->send_mail($user->email, $from_mail, $from_name, $subject, $message);
    }
}

?>
