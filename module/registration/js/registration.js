/**
 * Объект авторизации
 */
registration = {
	/**
	 * Вход в систему
	 */
	process: function() {
		doAjaxRequest( {
			   controller: "registration",
			   action: "register",
			   params: {
				   name: gbid('reg.name').value,
				   country: gbid('reg.country').value,
				   city: gbid('reg.city').value,
				   email: gbid('reg.email').value,
				   password: gbid('reg.password').value,
				   howKnow: gbid('reg.howKnow').value
			   },

				callback: function(ar)
				{
					if (ar.result == 1) {
						$("#reg").html(ar.message);
						//$("#reg").hide("slow");
					}
					
					//alert(ar);
				}
		});
	}
}