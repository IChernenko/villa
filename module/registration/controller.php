<?php

/**
 * Регистрация на сайте
 *
 * @author dorian
 */
class Module_Registration_Controller extends Dante_Controller_Base 
{
    protected function _instanceService()
    {
        return new Module_Registration_Service();
    }

    /**
     * Регистрация пользователя
     * @return type 
     */
    protected function _registerAction() {
        try {
            $request = Dante_Controller_Front::getRequest();

            $user = new Module_User_Model();
            $user->login = $request->get('login');
            $user->email = $request->get('email');
            $user->password = $request->get('password');
            $user->rating = 1;
            if (Dante_Lib_Config::get('registration.activateAfterReg') === true) {
                $user->active = 1;
            }
            
            $groupMapper = new Module_Group_Mapper();
            $user->group_id = $groupMapper->getByName('user');
            
            $mapper = new Module_User_Mapper();

            if ($mapper->getUidByEmail($user->email)) {
                throw new Exception("Пользователь с емейл {$user->email} cуществует");
            }

            $user->id = $mapper->add($user);
            if (!$user->id) throw new Exception('Невозможно зарегистрировать');
            
            // Добавим данные в профиль
            $profile =  new Module_Profile_Dmo();
            $profile->user_id = $user->id;
            $profile->middle_name = $request->get('middleName');
            $profile->first_name = $request->get('name');
            $profile->last_name = $request->get('lastName');
            $profile->insert();
            
            Dante_Lib_Observer_Helper::fireEvent('registration.done', $user);
            
            $this->_instanceService()->sendRegEmail($user);
            
        
        } catch (Exception $exc) {
            return array(
                'result' => 0,
                'message' => $exc->getMessage()
            );
        }

        return array(
            'result' => 1,
            'message' => 'Регистрация прошла успешно'
        );
        
        
        
        
        
    }

    protected function _defaultAction() {
        if (Dante_Lib_Config::get('registration.enable') === false) {
            return 'Регистрация закрыта';
        }
        
        $tpl = new Dante_Lib_Template();

        $fileName = '../module/registration/template/form.html';
        if (file_exists('template/default/registration/form.html')) {
            $fileName = 'template/default/registration/form.html';
        }
        
        return $tpl->draw($fileName);
    }

}

?>