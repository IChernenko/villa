<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of label
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Auth_Label extends Dante_Controller_Base{
    
    protected function _defaultAction() {
        $uid = (int)Dante_Helper_App::getUid();
        $result = '';
        if ($uid > 0) {
        	//return 'Выход';
        	$result = array('name'=> Module_Lang_Helper::translate('exit'), 'click'=>'auth.logout();return false;');
        }	
        else {
        	//return 'Вход';
        	$result = array('name'=>  Module_Lang_Helper::translate('enter'), 'click'=>'');
        } 
        //var_dump($result);
        return $result;
    }
}

?>
