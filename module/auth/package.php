<?php
global $package;
$package = array(
    'version' => '5',
    'name' => 'module.auth',
    'js' => 'js/auth.js',
    'dependence' => array(
        'module.session',
        'module.labels'
    ),
    'css' => array(
        'ws' => array(
            'admin' => array(
                '../module/auth/css/auth.css'
            )
        )
    )
);