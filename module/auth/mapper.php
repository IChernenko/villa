<?php



/**
 * Маппер для авторизации пользователей
 *
 * @author Администратор
 */
class Module_Auth_Mapper {

    protected $_tableName = '[%%]users';
    
    /**
     * Авторизация пользователя в системе
     * @param App_User_Model $user 
     * @return 
     */
    public function login(Module_User_Model $user)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        
        $condition = array(
            'email' => $user->email,
            'password' => $user->password,
            'is_active' => 1
        );
        
        if (Dante_Lib_Config::get('auth.byLogin') === true) {
            $condition = array(
                'login' => $user->login,
                'password' => $user->password,
                'is_active' => 1
            );
        }
        
        //var_dump($condition);
        $table->select($condition);	
		// такого пользователя нет
        if ($table->getNumRows() == 0) {
            return false;
        }
        
        $user->id = $table->id;
        $user->rating = $table->rating;
        return true;
    }
    
    /*
     * Вытаскиваем данные по юзеру по логину
     * @param $user - Модель запрашиваемого юзера Module_User_Model
     * @return object
     */
    public function getUserByLogin(Module_User_Model $user){        
        $orm = new Dante_Lib_Orm_Table($this->_tableName);
        return $orm -> select(array('login' => $user->login));        
    }
    
    /*
     * Вытаскиваем данные по юзеру по логину
     * @param $user - Модель запрашиваемого юзера Module_User_Model
     * @return object
     */
    public function getUserByEmail(Module_User_Model $user){        
        $orm = new Dante_Lib_Orm_Table($this->_tableName);
        return $orm -> select(array('email' => $user->email));        
    }

    /**
     * Определяет есть ли пользователь с заданным логином
     * @param string $login
     * @return bool
     */
    public function loginExists($login) {
        return (bool)Module_User_Dmo::dmo()->byLogin($login)->fetch()->getNumRows();
    }

}

?>