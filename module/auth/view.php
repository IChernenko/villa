<?php



/**
 * View для отображения формы авторизации
 *
 * @author Администратор
 */
class Module_Auth_View {
    
    /**
     *
     * @return string
     */
    public function drawLoginForm($login = false)
    {
        $tpl = new Dante_Lib_Template();
        $tplFileName = Dante_Helper_App::getWsPath().'/auth/login.html';
        if (!file_exists($tplFileName)) {
            $tplFileName = '../module/auth/template/login.html';
        }
        
        $tpl->login = $login;
        
        return $tpl->draw($tplFileName);
    }
    
    /**
     *
     * @return string
     */
    public function drawLogoutForm($user=false)
    {
        $tpl = new Dante_Lib_Template();
        $tplFileName = Dante_Helper_App::getWsPath().'/auth/logout.html';
        if (!file_exists($tplFileName)) {
            $tplFileName = '../module/auth/template/logout.html';
        }
        
        if (!$user) $user = new Module_User_Model();
        
        
        $tpl->user = $user;
        return $tpl->draw($tplFileName);
    }
}

?>
