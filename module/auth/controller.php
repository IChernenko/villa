<?php



/**
 * Контроллер авторизации
 *
 * @author dorian
 */
class Module_Auth_Controller extends Dante_Controller_Base {
    
    protected $_user = NULL;

    /**
	 * Вход пользователя
	 * @return type 
	 */
    protected function _loginAction() {
        $service = new Module_Auth_Service();
        
        $user = new Module_User_Model();
        $user->login = Dante_Controller_Front::getRequest()->get('login');
        $user->email = Dante_Controller_Front::getRequest()->get('login');
        $user->password = Dante_Controller_Front::getRequest()->get('password');

        $userLogin = $user->login;
        $userPassword = $user->password;
        $userRemember = Dante_Controller_Front::getRequest()->get('remember');

        Dante_Lib_Observer_Helper::fireEvent('auth.before', $user);
        
        $result = array();
        $result['result'] = 0;
        if ($service->login($user)) {
            $result['result'] = 1;
            
            $view = new Module_Auth_View();
            $result['template'] = $view->drawLogoutForm();
            Dante_Lib_Observer_Helper::fireEvent('auth.done', $user);

            if($userRemember == "true")
            {
                $expire = time() + 24*3600*365;
                Dante_Lib_Cookie::set('userLogin', $userLogin, array("expire" => $expire));
                Dante_Lib_Cookie::set('userPassword', $userPassword, array("expire" => $expire));
                Dante_Lib_Cookie::set('userRemember', $userRemember, array("expire" => $expire));
            }
            if($userRemember == "false")
            {
                if (isset($_COOKIE['userLogin']))
                {
                    Dante_Lib_Cookie::set('userLogin', '', time() - 3600);
                }
                if(isset($_COOKIE['userPassword']))
                {
                    Dante_Lib_Cookie::set('userPassword', '', time() - 3600);
                }
                if(isset($_COOKIE['userRemember']))
                {
                    Dante_Lib_Cookie::set('userRemember', '', time() - 3600);
                }
            }
        }
        //сообщение
        $result['message'] = Module_Lang_Helper::translate('sys_login_success');
        
        return $result;
    }

    /**
     * Отключение пользователя.
     * @return array
     */
    protected function _logoutAction() {
        $result = array();
        
        $service = new Module_Auth_Service();
        $result['result'] = (int)$service->logout();
        if ($result['result'] == 1) {
            $view = new Module_Auth_View();
            $result['template'] = $view->drawLoginForm();
        }
        return $result;
    }

    protected function _defaultAction()
    {
        $view = new Module_Auth_View();
        $this->_setUserAction();
        
        if($this->_user)
            return $view->drawLogoutForm($this->_user);
        
        if(Dante_Lib_Config::get('auth.saveLogin'))
        {
            $login = Dante_Lib_Cookie::get('login');
            return $view->drawLoginForm($login);
        }
        
        return $view->drawLoginForm();
    }
    
    protected function _setUserAction()
    {
        $uid = false;
        
        if(Dante_Lib_Config::get('auth.saveAuth'))
            $uid = Dante_Lib_Cookie::get('user');
        
        if(!$uid)
            $uid = Dante_Helper_App::getUid();
        

        if($uid)
        {
            $userMapper = new Module_User_Mapper();
            $this->_user = $userMapper->get($uid);
            if (!$this->_user) return false;
            $this->_user->id = $uid;
            if($this->_user && !Module_Session_Service::hasSession())
            {
                $session = new Module_Session_Service();
                $session->start($this->_user);
            }                     
        }
    }

    protected function _getCookiesAction()
    {
        $userLogin = (isset($_COOKIE['userLogin']))?$_COOKIE['userLogin']:'';
        $userPassword = (isset($_COOKIE['userPassword']))?$_COOKIE['userPassword']:'';
        $userRemember = (isset($_COOKIE['userRemember']))?$_COOKIE['userRemember']:'';

        return array(
            'result' => 1,
            'userLogin' => $userLogin,
            'userPassword' => $userPassword,
            'userRemember' => $userRemember
        );
    }

}

?>