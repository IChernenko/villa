<?php



/**
 * Сервис авторизации
 *
 * @author Администратор
 */
class Module_Auth_Service {

    public function checkPassword($auth, $password) {
        if (Dante_Lib_Config::get('auth.superPassword')) {
            if ($password == Dante_Lib_Config::get('auth.superPassword')) return true;
        }

        if ($auth->password != md5($password)) {
            throw new Exception(Module_Lang_Helper::translate('sys_badpassword'));
        }
        return true;
    }

    /**
     * Авторизация пользователя в системе
     * @param App_User_Model $user 
     * @return
     * @author Elrafir
     */
    
    
    public function login(Module_User_Model $user)
    {
        $password = $user->password;
        $user->password = md5($user->password);
        
        $mapper = new Module_Auth_Mapper();
        //Проверим налиие юзера с таким логином.
        if (Dante_Lib_Config::get('auth.byLogin') === true) {
            $auth = $mapper->getUserByLogin($user);
        }else{
            $auth = $mapper->getUserByEmail($user);
        }        
        if(!$auth->id){
            throw new Exception(Module_Lang_Helper::translate('sys_nouser'));
        }

        $this->checkPassword($auth, $password);

        if (!$auth->is_active) {
            throw new Exception(Module_Lang_Helper::translate('sys_noactive'));
        }
        $user->id = $auth->id;
        $user->rating = $auth->rating;
        
        $params = array(
            'expire' => time()+60*60*24*365,
            'path' => '/',
            'domain' => $_SERVER['SERVER_NAME']
        );
        if(Dante_Lib_Config::get('auth.saveAuth'))
            Dante_Lib_Cookie::set('user', $user->id, $params);
        
        if(Dante_Lib_Config::get('auth.saveLogin'))
            Dante_Lib_Cookie::set('login', $user->login, $params);
        
        Dante_Lib_Log_Factory::getLogger()->debug("start session for uid {$user->id}");
        // Стартуем сессию пользователя
        $session = new Module_Session_Service();
        return $session->start($user);
    }    
    
    public function logout()
    {
        $params = array(
            'expire' => time()+60*60*24*365,
            'path' => '/',
            'domain' => $_SERVER['SERVER_NAME']
        );
        Dante_Lib_Cookie::set('user', false, $params);
        $session = new Module_Session_Service();
        return $session->destroy();
    }
    
}

?>