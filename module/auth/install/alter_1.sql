CREATE TABLE `[%%]users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `rating` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

CREATE TABLE `[%%]sessions` (
  `uid` int(11) NOT NULL,
  `session` varchar(32) NOT NULL,
  `last_update` int(11) NOT NULL,
  `ip` varchar(20) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE [%%]sessions ADD
      CONSTRAINT fk_sessions_uid
      FOREIGN    KEY (uid)
      REFERENCES [%%]users(id) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE [%%]groups ( 
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(32) DEFAULT NULL,
  `group_caption` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE [%%]user_groups (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE [%%]user_groups ADD
      CONSTRAINT fk_user_groups_user_id
      FOREIGN KEY (`user_id`)
      REFERENCES [%%]users(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE [%%]user_groups ADD
      CONSTRAINT fk_user_groups_group_id
      FOREIGN KEY (`group_id`)
      REFERENCES [%%]groups(group_id) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS [%%]user_profile (
    `user_id` int(11) NOT NULL AUTO_INCREMENT,
    `fio` varchar(100) NOT NULL,
    PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE [%%]user_profile ADD
      CONSTRAINT fk_user_profile_user_id
      FOREIGN KEY (`user_id`)
      REFERENCES [%%]users(id) ON DELETE CASCADE ON UPDATE CASCADE;