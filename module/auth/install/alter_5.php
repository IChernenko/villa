<?php

$lang = Module_Lang_Mapper::getByCode('rus');
if(!$lang) $lang = 1;

$labels = array(
    'sys_nouser'        => 'Такого пользователя нет',
    'sys_badpassword'   => 'Неверно набран пароль',
    'sys_noactive'      => 'Пользователь не активен. Обратитесь к администратору сайта.',
    'sys_login_success' => 'Вы успешно авторизованы!'
);

$sql = "SELECT `id` FROM `[%%]labels` ORDER BY `id` DESC LIMIT 1";
$id = Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'id');

$i = $id+1;

foreach($labels as $label => $value)
{
    $sql = "INSERT INTO `[%%]labels` SET `id` = {$i}, `sys_name` = '{$label}', `is_system` = 1";
    Dante_Lib_SQL_DB::get_instance()->exec($sql);
    
    $sql = "INSERT INTO `[%%]labels_lang` SET `id` = {$i}, `lang_id` = {$lang}, `name` = '{$value}'";
    Dante_Lib_SQL_DB::get_instance()->exec($sql);
    $i++;
}

?>
