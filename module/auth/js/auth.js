/**
 * Объект авторизации
 */
auth = {
    /**
     * Вход в систему
     */
    login: function() {
        $('#auth_do').addClass('disabled');
        $("#auth_message").html('');
        $.ajax({
            type: "POST",
            url: '/ajax.php',
            data: {
                controller: "module.auth.controller",
                action: "login",
                login: $('#auth_login').val(),
                password: $('#auth_password').val()
            },
            success: function(data) {
                if (auth.onAfterLogin != undefined) {
                    return auth.onAfterLogin(data);
                }

                if (data.result == 0) {
                    $('#auth_do').removeClass('disabled');
                    $("#auth_message").html(data.message);
                }
                if (data.result == 1) {
                    $("#auth").html(data.template);
                    //window.location.reload();
                    window.location.href = "/";
                }
            }
        });
    },

    /**
     * Выход из системы
     */
    logout: function() {
        $.ajax({
            type: "POST",
            url: '/ajax.php',
            data: {
                controller: "module.auth.controller",
                action: "logout"
            },
            success: function(data) {
                if (data.result == 1) {
                    $("#auth").html(data.template);
                    window.location.replace('/');
                }
            }
        });

    }
}