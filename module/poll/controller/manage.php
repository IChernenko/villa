<?php

class Module_Poll_Controller_Manage extends Module_Handbooksgenerator_Controller_Base{
    
    protected function _defaultAction() { 
        $helper = new Module_Division_Helper();
        $helper->checkAccess();
        
        return $this->_drawAction();
    }
    /**
     * грид с перечнем опросов
     * @return string HTML
     */
    protected function _drawAction(){
        $model      = new Module_Poll_Model_Manage();
        $mapper     = new Module_Poll_Mapper_Poll();
        $tpl        = new Module_Poll_View_Manage();
        
        $params = $this->getParams($model);
        $polls = $mapper->getPolls($params);
        
        $this->formEnable('module.poll.controller.manage');
        $this->tableParams['style']['table-layout']='fixed';
        $this->caption='Опросы';
        $this->thead=array(
            'titles'=>array(
                array(
                    'content'=>'ID',
                    'params'=>array(
                        'style'=>"width:15px"
                    ),
                    'sortable'=>'id',
                    "sortable_def"=>"desc"                    
                ),
                array(
                    'content'=>'Вопрос',
                    'params'=>array(
                        'style'=>"width:200px"
                    ),
                    'sortable'=>'query'
                    
                ),
                array(
                    'content'=>'Ответы: <i>голоса</i>',
                    'params'=>array(
                        'style'=>"width:180px"
                    ),
                    'sortable'=>'votes'
                ),
                array(
                    'content'=>'Категории',
                    'params'=>array(
                        'style'=>"width:250px"
                    ),
                    
                ),
                array(
                    'content'=>'Начат',
                    'params'=>array(
                        'style'=>"width:74px"
                    ),
                    'sortable'=>'date_start'
                ),
                array(
                    'content'=>'Завершен',
                    'params'=>array(
                        'style'=>"width:74px"
                    ),                    
                    'sortable'=>'date_stop'
                ),
                array(
                    'content'=>'Статус',
                    'params'=>array(
                        'style'=>"width:100px"
                    ),                    
                    'sortable'=>'status'
                ),
                array(
                    'content'=>'Функции',
                    'params'=>array(
                        'style'=>"width:65px"
                    ),
                    
                ),
                            
            ),
            
        );
        foreach($polls['rows'] as $poll){
            
            $id=$poll['id'];
            $cur=array();
            $cur['id']          = $id;
            $cur['query']       = $poll['query'];
            $cur['answers']     = $this->_drawAnswers($id, $poll['votes']);
            $cur['divisions']   = $this->_drawDivisions($id);
            $cur['date_start']  = $poll['date_start']?Dante_Helper_Date::converToDateType($poll['date_start']):'---';
            $cur['date_stop']   = $poll['date_stop']?Dante_Helper_Date::converToDateType($poll['date_stop']):'---';
            $cur['status_name'] = $poll['status_name'];
            
            $cur['buttons']['content']          = $tpl->buttons($poll);
            $cur['buttons']['params']['class']  = 'buttons';
            $this->tbody[$id]['content']=$cur;
            switch ($poll['status']){
                case 1:
                    $this->tbody[$id]['params']['class']='';
                    break;
                case 2:
                    $this->tbody[$id]['params']['class']='success';
                    break;
                case 3:
                    $this->tbody[$id]['params']['class']='error';
                    break;                
            }
        }
        $this->tfoot=array(
            'paginator'=>array(
                $this->paginator($polls['count'], $params),
            ),  
            'field'=>array(            
                "
                    Всего опросов найдено: <b>{$polls['count']}</b>
                    <a class='btn btn-success pull-right' onclick='pollmanage.editForm()'>Добавить опрос</a>"
            )
        );
        if(!Dante_Lib_Config::get('poll.multiDivision')){
            $this->kill[] = 4;
        }
        $modal=new Lib_Jquery_Arcticmodal_Controller();
        
        return $this->generate().$modal->draw('pollForm');
    }
    /**
     * грид с перечнем опросов запрошенных ajax-ом
     * @return array ('result'=>, 'html'=>$html)
     */
    protected function _drawAjaxAction(){
        $html=  $this->_drawAction();
        
        return array(
            'result'=>1,
            'html'=>$html
        );
    }
    /**
     * Форма добавления/редактирования опроса
     * @param int $id - идентификатор редактируемого опроса. Если не задан то форма считается формой для добавления опроса.
     * @return type
     */
    protected function _drawFormAction($id = 0){
        
        $mapper = new Module_Poll_Mapper_Poll();
        $view = new Module_Poll_View_Manage();
        $divisions = new Module_Division_Manage_Mapper();
        $langMapper = new Module_Lang_Mapper();
        
        if(!$id) $id = $this->_getRequest()->get_validate('id', 'int', 0);        
        $lang = $this->_getRequest()->get_validate('lang_id', 'int', 1);
        
        $model = $mapper->getPollModel($id, $lang);
        $view->divisions = $divisions->getAvailableDivisions(array(1,4,5));
        $view->pollDivisions = $mapper->getPollDivisions($id);
        $view->langList = $langMapper->getList();
        
        return array(
            'result'=>1,
            'html'=> $view->drawForm($model)
        );
    }    
    
    /**
     * функция добавления/редактирования голосования
     * @return array array('result'=>1, id=>$poll_id)|array('result'=>0, message=>$error) при удачном результате возвращаемый ID идентификатор опроса для обновления формы
     */
    protected function _editPollAction(){
        $mapper     = new Module_Poll_Mapper_Poll();
        $model      = new Module_Poll_Model_Manage();
        $poll_id = $this->_getRequest()->get_validate('id', 'int', 0);        
        
        $model->id          = $poll_id;
        $model->query       = $this->_getRequest()->get_validate('query', 'text');
        $model->answer      = $this->_getRequest()->get('answer');
        $model->answer_new  = $this->_getRequest()->get('answer_new');
        $model->divisions   = $this->_getRequest()->get_validate('divisions', 'array');
        
        if(Dante_Lib_Config::get('multilang'))
        {        
            $model->lang_id = $this->_getRequest()->get_validate('lang_id', 'int', 1);
            $model->query_lang = $this->_getRequest()->get('query_lang');
            $model->answer_lang = $this->_getRequest()->get('answer_lang');
            $model->answer_lang_new  = $this->_getRequest()->get('answer_lang_new');
        } 
        
        $id = $mapper->editPoll($model);
        
        if($this->_getRequest()->get('returntype')==1){
            return $this->_drawFormAction($id);
        }
        return array(
            'result' => 1,
            'id' => $id
        );
        
    }
    /**
     * Удаление опроса
     * @return array('result'=>1)
     */
    protected function _delPollAction(){
        $mapper     = new Module_Poll_Mapper_Poll();
        $poll_id=$this->_getRequest()->get_validate('id', 'int');
        $mapper->del($poll_id);
        return array(
           'result'=>1 
        );
    }
    /**
     * Меняет статус опроса на 3 (завершенный).
     * @return array('result'=>1)
     */
    protected function _stopPollAction(){
        $mapper     = new Module_Poll_Mapper_Poll();
        $poll_id=$this->_getRequest()->get_validate('id', 'int');
        $mapper->stop($poll_id);
        return array(
           'result'=>1 
        );
    }

    /**
     * Старт опроса
     * Меняет статус опроса на 2 (активный).
     * @return array('result'=>1)
     */
    protected function _startPollAction(){
        $mapper     = new Module_Poll_Mapper_Poll();
        $poll_id=$this->_getRequest()->get_validate('id', 'int');
        $mapper->start($poll_id);
        return array(
           'result'=>1 
        );
    }
    /**
     * Возвращает ответы по конкретному опросу с результатами.   
     * @param type $poll_id
     * @param type $votes - количество проголосовавших
     * @return string HTML
     */
    protected function _drawAnswers($poll_id, $votes=0){
        $mapper     = new Module_Poll_Mapper_Poll();
        $tpl        = new Module_Poll_View_Manage();
        
        $answers = $mapper->getAnswers($poll_id);
        //Вычисляем процентное соотношение        
        foreach ($answers as &$cur){
            if($votes){
                $cur['procent']=  round($cur['votes']*100/$votes);
            }else{
                $cur['procent']=0;
            }
        }
        $vars['answers']=$answers;
        $vars['count']=$votes;
        return $tpl->answers($vars);        
    }
    /**
     * Возвращает перечень категорий к которым привязан опрос
     * @param type $poll_id
     * @return type
     */
    protected function _drawDivisions($poll_id){
        $mapper     = new Module_Poll_Mapper_Poll();
        $tpl        = new Module_Poll_View_Manage();
        
        $vars['divisions'] = $mapper->getPollDivisions($poll_id);
        return $tpl->divisions($vars);
        
    }
}