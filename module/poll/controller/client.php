<?
class Module_Poll_Controller_Client extends Module_Handbooksgenerator_Controller_Base
{        
    protected function _defaultAction(){
        
        return $this->drawBlock();
    }
    /**
     * Отображение списка архивных опросов запрошенных ajax-ом
     * @return array ('result'=>, 'html'=>$html)
     */
    protected function _drawAjaxAction(){
        $return=array(
            'result'=>1,
            'html'=>$this->_drawAction()
        );
        return $return;
    }
    /**
     * Отображение списка архивных опросов
     * @return string html
     */
    protected function _drawAction()
    {       
        $mapper = new Module_Poll_Mapper_Poll();
        $model  = new Module_Poll_Model_Clientarhive();        
        
        $params = $this->getParams($model);   
        $lang = Module_Lang_Helper::getCurrent();
        $rows = $mapper->getPollsClient($params, $lang);
        $this->formEnable('module.poll.controller.client');
        $titles = array(
            array(
                "content" => Module_Lang_Helper::translate('sys_query'),
                "sortable" => "query"
            ),
            array(
                "content" => Module_Lang_Helper::translate('sys_date_start'),
                "sortable"=>"date_start",
                "sortable_def"=>"desc"
            ),
            array(
                "content"=> Module_Lang_Helper::translate('sys_date_stop'),
                "sortable"=>"date_stop"
                
            ),
            array(
                "content"=>Module_Lang_Helper::translate('sys_votes'),
                "sortable"=>"votes"
            )
        );
        $this->tableParams['class'][]='table-striped';
        $this->thead=array(
            $titles
        );       
        
        $this->tfoot=array(
            'paginator'=>array(
                $this->paginator($rows['count'], $params)
            )
        );
         $this->_setBody($rows['rows']);
         return $this->generate();
    }
    /**
     * Блок добавления одного голоса для конкретного опроса
     * @return array ('result'=>, 'html'=>$html) thml со статистикой для данного опроса
     */
    protected function _addVoteAction(){        
        $mapper     = new Module_Poll_Mapper_Poll();
        $cookie     = new Dante_Lib_Cookie();
        
        $poll_id    = (int)$this->_getRequest()->get('poll_id');
        $answer_id  = (int)$this->_getRequest()->get('answer_id');
        
        if(!$poll_id || !$answer_id)
            return array('result'=>0);
        
        $mapper->addVote($poll_id, $answer_id);
        $cookie->set("poll_{$poll_id}", 1);        
        $poll   =$mapper->getPoll($poll_id);
        return array(
            'result'=>1,
            'html'=>$this->_drawAnswers($poll_id, $poll['votes'])
        );
    }
    /**
     * Возвращает HTML блок активных активных опросов предопределённых для текущего URL.
     * @param bool $statistic - по дефолту тип возвразаемого контета (форма опросаили результат) зависит от кукисов. Этот флаг принудительно определет тип как "ститистика"
     * @return string html
     */
    public function drawBlock($statistic=false){
        $mapper = new Module_Poll_Mapper_Poll();
        $tpl    = new Module_Poll_View_Client();
        $cookie = new Dante_Lib_Cookie();
        $lang = Module_Lang_Helper::getCurrent();
        
        $url = Dante_Lib_Request::get_requested_url();
        
        //TEMP!!!
        $langMapper = new Module_Lang_Mapper();
        $langCode = $langMapper->getCodeById($lang);
        if(strpos($url, "/{$langCode}/") !== false)
        {
            $url = str_replace("/{$langCode}/", '', $url);
        } 
        //end TEMP
        
        $url = substr($url, 1);
        
        
        $polls = $mapper->getPollsByDivision($url, $lang);
        $pollsArray = array();       
        foreach($polls as $poll){
            //Достаём ответы по оросу.
            if($cookie->get('poll_'.$poll['poll_id']) || $statistic){               
                $vars['answers']= $this->_drawAnswers($poll['poll_id'], $poll['votes']);
                $vars['poll']   = $poll;
                $pollsArray[]=$tpl->pollBlock($vars);                
            }else{
                //Выводим опросник
                $pollsArray[]=$this->_drawPollFromVote($poll);
            }
        }       
        $html = join('', $pollsArray);
        return $html;
    }
    /**
     * Вбиваем контент tbody для клиента.
     * @param array $rows
     */
    protected function _setBody($rows){        
        foreach($rows as $row){
            $id=$row['id'];
            $cur=array();
            $cur['query']       = $row['query_lang'];
            $cur['date_start']  = Dante_Helper_Date::converToDateType($row['date_start']);
            $cur['date_stop']   = Dante_Helper_Date::converToDateType($row['date_stop']);
            $cur['answers']     = $this->_drawAnswers($id, $row['votes']);
            
            $this->tbody[$id]=$cur;
        }
    }
    /**
     * Возвращает ответы по конкретному опросу с результатами.   
     * @param type $poll_id
     * @param type $votes - количество проголосовавших
     * @return string HTML
     */
    protected function _drawAnswers($poll_id, $votes = 0)
    {
        $mapper     = new Module_Poll_Mapper_Poll();
        $tpl        = new Module_Poll_View_Client();
        
        $lang = Module_Lang_Helper::getCurrent();
        
        $answers = $mapper->getAnswers($poll_id, $lang);
        //Вычисляем процентное соотношение        
        foreach ($answers as &$cur){
            if($votes){
                $cur['procent']=  round($cur['votes']*100/$votes);
            }else{
                $cur['procent']=0;
            }
        }
        $vars['answers'] = $answers;
        $vars['count'] = $votes;
        return $tpl->answers($vars);        
    }
    /**
     * Возвращет сгенерированний HTML с формой для выбора варианта ответа для голосования
     * @param array $poll
     * @return string html
     */
    protected function _drawPollFromVote($poll){
        $mapper     = new Module_Poll_Mapper_Poll();
        $tpl        = new Module_Poll_View_Client();
        $lang = Module_Lang_Helper::getCurrent();
        
        $vars['answers']    = $mapper->getAnswers($poll['poll_id'], $lang);
        $vars['poll']       = $poll;
        $vars['answers']    = $tpl->pollFromVotes($vars);
        return $tpl->pollBlock($vars);        
    }
    
    
}
