<?php
class Module_Poll_Model_Manage extends Module_Poll_Model_Managegrid {
    
    /**
     * Идентификатор опроса
     * @var int 
     */
    public $id;
    
    public $lang_id;
    /**
     * Задаваемый вопрос
     * @var string 
     */
    public $query;
    
    public $query_lang;
    /**
     * Варианты ответов
     * @var array 
     */
    public $answer = array();
    
    public $answer_lang = array();
    /**
     * Новы варианты ответов
     * @var array 
     */
    public $answer_new = array();
    
    public $answer_lang_new = array();
    /**
     * Категории для отображения конкурса
     * @var array 
     */
    public $divisions;
    
}
?>
