<?php
global $package;
$package = array(
    'version' => '5',
    'name' => 'module.poll',
    'description' => 'Модуль "опросы"',
    'dependence' => array(
        'module.division',
        'lib.paginator',
        'ws'=>array(
            'admin'=>array(
                'module.handbooksgenerator',
                'lib.twitbootstrap',
                'lib.jquery.arcticmodal',
                
            )
        )
    ),
    'js' => array(
        '../module/poll/js/poll_client.js',
        'ws'=>array(
            'admin'=>array(
                '../module/poll/js/poll_manage.js',
            
            )
        )
    ),
    'css' => array(
        '../module/poll/css/poll.css',
    )
);