

--
-- Структура таблицы `[%%]poll_answers`
--

DROP TABLE IF EXISTS `[%%]poll_answers`;
CREATE TABLE IF NOT EXISTS `[%%]poll_answers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `poll_id` int(10) unsigned NOT NULL,
  `answer` varchar(200) NOT NULL,
  `lang_id` tinyint(2) unsigned DEFAULT NULL,
  `votes` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `poll_id` (`poll_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

--
-- Дамп данных таблицы `[%%]poll_answers`
--

INSERT INTO `[%%]poll_answers` (`id`, `poll_id`, `answer`, `lang_id`, `votes`) VALUES
(1, 1, 'Сто рублей', NULL, 133),
(2, 1, 'Двести рублей', NULL, 23),
(3, 1, 'Три доллара', NULL, 169);

-- --------------------------------------------------------

--
-- Структура таблицы `[%%]poll_division`
--

DROP TABLE IF EXISTS `[%%]poll_division`;
CREATE TABLE IF NOT EXISTS `[%%]poll_division` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `poll_id` int(10) unsigned NOT NULL,
  `division_link` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `division_link` (`division_link`),
  KEY `poll_id` (`poll_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;



-- --------------------------------------------------------

--
-- Структура таблицы `[%%]poll`
--

DROP TABLE IF EXISTS `[%%]poll`;
CREATE TABLE IF NOT EXISTS `[%%]poll` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `query` varchar(200) NOT NULL,
  `status` int(10) unsigned NOT NULL DEFAULT '1',
  `date_start` int(11) DEFAULT NULL,
  `date_stop` int(11) DEFAULT NULL,
  `votes` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

--
-- Дамп данных таблицы `[%%]poll`
--

INSERT INTO `[%%]poll` (`id`, `query`, `status`, `date_start`, `date_stop`, `votes`) VALUES
(1, 'Сколько стоит стог сена?', 2, 1353681736, NULL, 325);

-- --------------------------------------------------------

--
-- Структура таблицы `[%%]poll_status`
--

DROP TABLE IF EXISTS `[%%]poll_status`;
CREATE TABLE IF NOT EXISTS `[%%]poll_status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

--
-- Дамп данных таблицы `[%%]poll_status`
--

INSERT INTO `[%%]poll_status` (`id`, `name`) VALUES
(1, 'Новый'),
(2, 'Активный'),
(3, 'Завершенный');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `[%%]poll`
--
ALTER TABLE `[%%]poll`
  ADD CONSTRAINT `poll_ibfk_2` FOREIGN KEY (`status`) REFERENCES `[%%]poll_status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `[%%]poll_answers`
--
ALTER TABLE `[%%]poll_answers`
  ADD CONSTRAINT `poll_answers_ibfk_1` FOREIGN KEY (`poll_id`) REFERENCES `[%%]poll` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `[%%]poll_division`
--
ALTER TABLE `[%%]poll_division`
  ADD CONSTRAINT `poll_division_ibfk_1` FOREIGN KEY (`poll_id`) REFERENCES `[%%]poll` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `poll_division_ibfk_2` FOREIGN KEY (`division_link`) REFERENCES `[%%]division` (`link`) ON DELETE CASCADE ON UPDATE CASCADE;
