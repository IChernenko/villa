DROP TABLE IF EXISTS `[%%]poll_division`;
CREATE TABLE IF NOT EXISTS `poll_division` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `poll_id` int(10) unsigned NOT NULL,
  `division_id` int(11) unsigned NOT NULL DEFAULT '3',
  PRIMARY KEY (`id`),
  KEY `poll_id` (`poll_id`),
  KEY `division_id` (`division_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE `[%%]poll_division`
  ADD CONSTRAINT `poll_division_ibfk_2` FOREIGN KEY (`division_id`) REFERENCES `[%%]division` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `[%%]poll_division`
  ADD CONSTRAINT `poll_division_ibfk_1` FOREIGN KEY (`poll_id`) REFERENCES `[%%]poll` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;