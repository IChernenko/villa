<?php

$lang = Module_Lang_Mapper::getByCode('rus');
if(!$lang) $lang = 1;

$labels = array(
    'sys_query'        => 'Вопрос',
    'sys_date_start'   => 'Дата начала',
    'sys_date_stop'    => 'Дата завершения',
    'sys_votes'        => 'Результат опроса',
);

$sql = "SELECT `id` FROM `[%%]labels` ORDER BY `id` DESC LIMIT 1";
$id = Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'id');

$i = $id+1;

foreach($labels as $label => $value)
{
    $sql = "INSERT INTO `[%%]labels` SET `id` = {$i}, `sys_name` = '{$label}', `is_system` = 1";
    Dante_Lib_SQL_DB::get_instance()->exec($sql);
    
    $sql = "INSERT INTO `[%%]labels_lang` SET `id` = {$i}, `lang_id` = {$lang}, `name` = '{$value}'";
    Dante_Lib_SQL_DB::get_instance()->exec($sql);
    $i++;
}

?>
