ALTER TABLE `[%%]poll_answers` DROP COLUMN `lang_id`;

CREATE TABLE `[%%]poll_lang`(
    `poll_id` INT(11) UNSIGNED NOT NULL,
    `lang_id` INT(11) UNSIGNED NOT NULL,
    `query_lang` VARCHAR(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `[%%]poll_lang` ADD CONSTRAINT `fk_poll_lang_poll_id`
    FOREIGN KEY(`poll_id`) REFERENCES `[%%]poll`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `[%%]poll_lang` ADD CONSTRAINT `fk_poll_lang_lang_id`
    FOREIGN KEY(`lang_id`) REFERENCES `[%%]languages`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;

INSERT INTO `[%%]poll_lang`(`poll_id`, `lang_id`, `query_lang`)
    SELECT `id`, 1, `query` FROM `[%%]poll`;

CREATE TABLE `[%%]poll_answers_lang`(
    `ans_id` INT(11) UNSIGNED NOT NULL,
    `lang_id` INT(11) UNSIGNED NOT NULL,
    `answer_lang` VARCHAR(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `[%%]poll_answers_lang` ADD CONSTRAINT `fk_poll_ans_lang_ans_id`
    FOREIGN KEY(`ans_id`) REFERENCES `[%%]poll_answers`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `[%%]poll_answers_lang` ADD CONSTRAINT `fk_poll_ans_lang_lang_id`
    FOREIGN KEY(`lang_id`) REFERENCES `[%%]languages`(`id`)
    ON DELETE CASCADE ON UPDATE CASCADE;

INSERT INTO `[%%]poll_answers_lang`(`ans_id`, `lang_id`, `answer_lang`)
    SELECT `id`, 1, `answer` FROM `[%%]poll_answers`;