<?php
class Module_Poll_View_Manage
{
    public $langList;
    public $divisions;
    public $pollDivisions;
    
    public function answers($vars){
        $tpl = new Dante_Lib_Template();
        $tpl->_vars=$vars;
        $fileName = '../module/poll/template/manage/answers.html';
        if (file_exists('template/default/poll/manage/answers.html')) {
            $fileName = 'template/default/poll/manage/answers.html';
        }
        return $tpl->draw($fileName);
    }
    
    public function buttons($vars){
        $tpl = new Dante_Lib_Template();
        $tpl->_vars=$vars;
        $fileName = '../module/poll/template/manage/buttons.html';
        if (file_exists('template/default/poll/manage/buttons.html')) {
            $fileName = 'template/default/poll/manage/buttons.html';
        }
        return $tpl->draw($fileName);
    }
    
    public function divisions($vars){
        $tpl = new Dante_Lib_Template();
        $tpl->_vars=$vars;
        $fileName = '../module/poll/template/manage/divisions.html';
        if (file_exists('template/default/poll/manage/divisions.html')) {
            $fileName = 'template/default/poll/manage/divisions.html';
        }
        return $tpl->draw($fileName);
    }
    
    public function drawForm($model)
    {
        $tpl = new Dante_Lib_Template();
        $tpl->model = $model;
        $tpl->divisions = $this->divisions;
        $tpl->poll_divisions = $this->pollDivisions;
        $tpl->langList = $this->langList;
        $tpl->multilang = Dante_Lib_Config::get('multilang');
        
        $fileName = '../module/poll/template/manage/form.html';
        if (file_exists('template/default/poll/manage/form.html')) {
            $fileName = 'template/default/poll/manage/form.html';
        }
        return $tpl->draw($fileName);
    }
    
    
}
?>