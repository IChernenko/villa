<?php
class Module_Poll_View_Client{
    
    public function answers($vars){
        $tpl = new Dante_Lib_Template();
        $tpl->_vars=$vars;
        $fileName = '../module/poll/template/client/answers.html';
        if (file_exists('template/default/poll/client/answers.html')) {
            $fileName = 'template/default/poll/client/answers.html';
        }
        return $tpl->draw($fileName);
    }
    
    public function pollBlock($vars){
        $tpl = new Dante_Lib_Template();
        $tpl->_vars=$vars;
        $fileName = '../module/poll/template/client/block.html';
        if (file_exists('template/default/poll/client/block.html')) {
            $fileName = 'template/default/poll/client/block.html';
        }
        return $tpl->draw($fileName);
    }
    
    public function pollFromVotes($vars){
        $tpl = new Dante_Lib_Template();
        $tpl->_vars=$vars;
        $fileName = '../module/poll/template/client/poll.html';
        if (file_exists('template/default/poll/client/poll.html')) {
            $fileName = 'template/default/poll/client/poll.html';
        }
        return $tpl->draw($fileName);
    }
    
}
?>