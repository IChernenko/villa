pollmanage={
    controller:'module.poll.controller.manage',
    
    //блок функций грида
    
    editSubmit:function(id, obj, action){
        if(!action){
            action='drawAjax';
        }
        pollmanage.performer(obj);
        data={
            controller  :pollmanage.controller,
            action      :action,
            id          :id
        }
        $.ajax({
            type: 'POST',
            data:data,
            url: '/ajax.php',
            dataType : "json",
            success: function (data){
                if(data.result==1){
                    handbooks.formSubmit();
                }else{
                    alert(data.message)
                }
            }
        });
    },
    changeLang: function(obj)
    {
        $('#edtPollForm').find('a, select').addClass('disabled');
        var id = $('#poll_id').val();
        if(!id) return false;
        
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            dataType: 'JSON',
            data: {
                controller: 'module.poll.controller.manage',
                action: 'drawForm',
                id: id,
                lang_id: $(obj).val()
            },
            success: function(data)
            {
                if(data.result == 1)
                    $('.modal-body').html(data.html);
                else
                    jAlert(data.message, 'Ошибка');
            }
        });
    },
    start:function(id, obj){
        pollmanage.performer(obj);
        pollmanage.editSubmit(id, obj, 'startPoll');
    },
    stop:function(id, obj){
        pollmanage.performer(obj);
        pollmanage.editSubmit(id, obj, 'stopPoll');        
    },
    cont:function(id, obj){
        pollmanage.performer(obj);
        pollmanage.editSubmit(id, obj, 'stopPoll');
        
    },
    del:function(id, obj){
        
        if(!confirm("Вы действительно хотите удалить этот опрос?"))
            return false;
        pollmanage.performer(obj);
        pollmanage.editSubmit(id, obj, 'delPoll');
        
    },
    editForm:function(id, obj){
        pollmanage.performer(obj);
        data={
            controller  :pollmanage.controller,
            action      :'drawForm',
            id          :id
        }
        $('#pollForm').arcticmodal({
            type: 'ajax',
            url: 'ajax.php',
            ajax: {
                type:'get',
                dataType:"json",
                q:1,
                data:data,
                success:function(data,el,responce){
                    data.body.html(el);
                    $(el).find('.modal-body').html(responce.html);                    
                }
            },
            afterClose: function(data, el) {                
                handbooks.formSubmit();
            }
        });
    },
    //обработка формы в модальном окне
    editPoll: function(obj, closed){
        
        pollmanage.performer(obj);
        var data=$('#edtPollForm').serialize();
        $.ajax({
            type: 'POST',
            data:data,
            url: '/ajax.php'+(!closed?'?returntype=1':''),
            dataType : "json",
            success: function (data){
                if(data.result==1){
                    if(closed){
                        $('#pollForm').arcticmodal('close');
                        handbooks.formSubmit();
                    }else{                        
                        $('#pollForm .modal-body').html(data.html);
                    }                    
                }else{
                    //alert(data.message)
                    console.log(data);
                }
            }
        });
        
    },    
    performer: function(obj){
        $(obj).removeClass('btn-success btn-info btn-normal btn-danger btn-warning').text('\u0412ыполняю');
    },
    delAnswer: function(id, obj){
        pollmanage.performer(obj);
        $('#answer_'+id).val('');
        pollmanage.editPoll(obj);
    },
    addAnswer: function(){
        
    }
}