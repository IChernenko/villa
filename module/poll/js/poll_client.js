pollclient={
    vote:function(poll_id, answer_id){
        data={
            controller: 'module.poll.controller.client',
            action:     'addVote',
            poll_id:    poll_id,
            answer_id:  answer_id
        }
        $.ajax({
            type: 'POST',
            data:data,
            url: '/ajax.php',
            dataType : "json",
            success: function (data){
                if(data.result==1){
                    $('#poll_block_'+poll_id+' .poll_answers').html(data.html)
                }
            }
        });
    }
    
}

