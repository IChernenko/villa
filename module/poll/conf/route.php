<?php

Dante_Lib_Router::addRule("/^\/manage\/poll\/$/", array(
    'controller' => 'Module_Poll_Controller_Manage'
));
Dante_Lib_Router::addRule("/\/poll\/$/", array(
    'controller' => 'Module_Poll_Controller_Client',
    'action'=>'draw'
));

?>