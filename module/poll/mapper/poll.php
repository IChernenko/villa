<?php
class Module_Poll_Mapper_Poll extends Module_Handbooksgenerator_Mapper_Base{
    
    /**
     * Используемые таблицы.
     */
    protected $_tableName           ='[%%]poll';
    protected $_tableNameLang       ='[%%]poll_lang';
    protected $_tableAnswers        ='[%%]poll_answers';
    protected $_tableAnswersLang    ='[%%]poll_answers_lang';
    protected $_tablePollStatus     ='[%%]poll_status';
    protected $_tablePollDivision   ='[%%]poll_division';
    protected $_tableDivision       ='[%%]division';
    /**
     * Возвращает массив опросов отфильтрованных для egrid
     * @param type $params
     * @return type
     */
    public function getPolls($params){
        //$this->debug=true;
        $join=array(
            "LEFT JOIN {$this->_tablePollStatus} as t2 ON t1.status=t2.id",
        );
        $cols=array(
            "t1.*",
            "t2.name as status_name"
        );
        
        return $this->getRowsByParams($params, $cols, $join);
    }
    
    public function getPollsClient($params, $lang = 1)
    {
        $join = array(
            "LEFT JOIN {$this->_tableNameLang} AS `t2` ON (`t2`.`poll_id` = `t1`.`id` AND `t2`.`lang_id` = {$lang})"
        );
            
        return $this->getRowsByParams($params, '*', $join);
    }
    /**
     * Возвращает массив опросов привязанных к заданной категории. Фильтрация происходит по URL в таблице division
     * @param string $division - url для фильтрации
     * @return array
     */
    public function getPollsByDivision($division, $lang = 1){
        if(!$division){
            $whereDivision =  "d.link IS NULL OR d.id = 3";
        }else{
            $whereDivision = "d.link='{$division}' OR d.link='/{$division}'";
        }
        $division = !$division?"   OR d.id=3 ":"='/{$division}'";
        $sql="SELECT * FROM {$this->_tablePollDivision} AS `pd` 
            LEFT JOIN {$this->_tableName} AS `p` ON `p`.`id` = `pd`.`poll_id`
            LEFT JOIN {$this->_tableNameLang} AS `pl` ON (`pl`.`poll_id` = `p`.`id` AND `pl`.`lang_id` = {$lang})
            LEFT JOIN {$this->_tableDivision} AS `d` ON `d`.`id` = `pd`.`division_id`
            WHERE ({$whereDivision}) AND `p`.`status` = 2";
            //echo $sql.'<br/>';
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $f=array();
        while($i=Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)){
            $f[$i['id']]=$i;
        };
        return $f;
    }
    /**
     * Возвращает массив свойств конкретного опроса
     * @param int $poll_id
     * @return array
     */
    public function getPoll($poll_id, $lang = 1)
    {
        $sql = "SELECT * FROM {$this->_tableName} AS `p`
                LEFT JOIN {$this->_tableNameLang} AS `pl` 
                ON (`pl`.`poll_id` = `p`.`id` AND `pl`.`lang_id` = {$lang})
                WHERE `p`.`id` = {$poll_id}";
                
        return Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
    }
    /**
     * Возвращает масив котегорий к которым привязан конкретный опрос
     * @param int $poll_id
     * @return array - массив категорий
     */
    public function getPollDivisions($poll_id){
        $sql="SELECT t1.id, t1.name, t1.link FROM {$this->_tableDivision} as t1 
            LEFT JOIN {$this->_tablePollDivision} as t2 ON t2.`division_id`=t1.id
            WHERE t2.poll_id={$poll_id}
        ";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $f=array();
        while($i=Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)){
            $f[$i['id']]=$i;
        };
        return $f;
    }
    /**
     * Возвращает  массив вопросов по конкретному опросу.
     * @param int $poll_id
     * @return array - массив вопросов
     */
    public function getAnswers($poll_id, $lang = 1)
    {
        $answers = array();
        
        $sql = "SELECT * FROM {$this->_tableAnswers} AS `a`
                LEFT JOIN {$this->_tableAnswersLang} AS `al` 
                ON (`al`.`ans_id` = `a`.`id` AND `al`.`lang_id` = {$lang})
                WHERE `a`.`poll_id` = {$poll_id}";
                
        if(!$r = Dante_Lib_SQL_DB::get_instance()->open($sql)) return $answers;
        
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
            $answers[$f['id']] = $f;
        
        return $answers;
    }
    
    
    public function getPollModel($id, $lang = 1)
    {
        $model = new Module_Poll_Model_Manage();
        $model->lang_id = $lang;
        
        if(!$id) return $model;
        
        $poll = $this->getPoll($id, $lang);
        $answers = $this->getAnswers($id, $lang);
        
        $model->id = $id;
        $model->query = $poll['query'];
        $model->query_lang = $poll['query_lang'];
        
        foreach($answers as $ansId => $answer)
        {
            $model->answer[$ansId] = $answer['answer'];
            $model->answer_lang[$ansId] = $answer['answer_lang'];
        }
        
        return $model;
    }
    /**
     * Запускаем|возобновляем активность  опроса. в случае востановления дата завершения опроса удаляется.
     * @param type $poll_id
     * @return boolean
     */
    public function start($poll_id){
        //Достанем данные по конкретному опросу
        $poll=$this->getPoll($poll_id);
        if(!isset($poll['id'])) return false;      
        //В зависимости от статуса ставим или не ставим дату
        $time=time();
        $set=array(
            "`status`=2"
        );                                 
        if($poll['status']==3){
            $set[]="`date_stop`=null";
        }else{
            $set[]="`date_start`='{$time}'";
            $set[]="`date_stop`=null";
        }
        $setStr=join(', ', $set);
        $sql="UPDATE {$this->_tableName} SET {$setStr} WHERE id={$poll_id}";
        Dante_Lib_SQL_DB::get_instance()->exec($sql);
        return true;     
    }
    /**
     * Останавливаем опрос.
     * @param int $poll_id
     */
     public function stop($poll_id){
        $table= new Dante_Lib_Orm_Table($this->_tableName);
        $table->date_stop=time();
        $table->status=3;
        $table->update(array('id'=>$poll_id));                
    }
    /**
     * Удаяем выбранный опрос из базы
     * @param int $poll_id
     */
    public function del($poll_id){
        $table= new Dante_Lib_Orm_Table($this->_tableName);
        $table->delete(array('id'=>$poll_id));
    }
    /**
     * Редактируем|добавляем опрос. Весь спектр параметров в присланной модели. 
     * @param Module_Poll_Model_Manage $model
     * @return int - идентификатор редактируемого|созданного опроса
     */
    public function editPoll(Module_Poll_Model_Manage $model)
    {
        //Добавляем/редактируем главную таблицу        
        $table= new Dante_Lib_Orm_Table($this->_tableName);
        $table->query = $model->query;
        if($model->id)          
            $table->update(array('id'=>$model->id));
        else
            $model->id = $table->insert();
        
        if($model->query_lang && $model->lang_id)
        {
            $tableLang = new Dante_Lib_Orm_Table($this->_tableNameLang);
            $tableLang->poll_id = $model->id;
            $tableLang->lang_id = $model->lang_id;
            $tableLang->query_lang = $model->query_lang;
            $tableLang->apply(array('poll_id'=>$model->id, 'lang_id'=>$model->lang_id));
        }
        
        //Добавляем/редактируем ответы
        $this->updateAnswers($model); 
       
        //Добавляем/редактируем категории
        $this->updateDivisions($model);
         
        return $model->id;
    }
    /**
     * Редактируем существующие вопросы и добавляем новые вопросы. Пустые воросы удаляем.
     * @param Module_Poll_Model_Manage $model
     * @param type $id
     * @return type
     */
    public function updateAnswers(Module_Poll_Model_Manage $model)
    {
        if($model->answer)
        {   
            foreach($model->answer as $key => $answer)
            {
                $table = new Dante_Lib_Orm_Table($this->_tableAnswers);
                if(!$answer){
                    //Пустые вопросы удаляем
                    $table->delete(array('id'=>$key));
                }else{
                    $table->answer = $answer;
                    $table->update(array('id'=>$key));
                } 
                
                if($model->answer_lang && $model->lang_id)
                {    
                    $tableLang = new Dante_Lib_Orm_Table($this->_tableAnswersLang);
                    if($model->answer_lang[$key])
                    {
                        $tableLang->ans_id = $key;
                        $tableLang->lang_id = $model->lang_id;
                        $tableLang->answer_lang = $model->answer_lang[$key];
                        $tableLang->apply(array('ans_id'=>$key, 'lang_id'=>$model->lang_id));
                    }
                    else
                        $tableLang->delete(array('ans_id'=>$key, 'lang_id'=>$model->lang_id));
                }
            }
        } 
        if($model->answer_new)
        {   
            //Добавляем новые записи
            foreach($model->answer_new as $key => $answer)
            {
                $table = new Dante_Lib_Orm_Table($this->_tableAnswers);
                $table->poll_id = $model->id;
                if(!$answer){
                    //Пустые вопросы пропускаем
                    continue;
                }else{
                    $table->answer = $answer;
                    $newId = $table->insert();
                }   
                
                if($model->answer_lang_new && $model->lang_id && $model->answer_lang_new[$key] && $newId)
                {
                    $tableLang = new Dante_Lib_Orm_Table($this->_tableAnswersLang);
                    $tableLang->ans_id = $newId;
                    $tableLang->lang_id = $model->lang_id;
                    $tableLang->answer_lang = $model->answer_lang_new[$key];
                    $tableLang->insert();
                }
            }
        }
        //Пересчитываем количество голосов
        $this->recalcVotes($model->id);
        return $model->id;
    }
    /**
     * Обновление записей по привязке ороса к категориям
     * @param Module_Poll_Model_Manage $model
     * @param int $id - id опроса
     * @return null
     */
    public function  updateDivisions(Module_Poll_Model_Manage $model){
        //Удаляем всё
        $table = new Dante_Lib_Orm_Table($this->_tablePollDivision);
        $table->delete(array('poll_id'=>$model->id));
        if(!$model->divisions)return;
        foreach ($model->divisions as $division_id){
            if(!(int)$division_id)continue;
            $sql="INSERT INTO `{$this->_tablePollDivision}` SET poll_id={$model->id}, division_id={$division_id}";
            //echo $sql;
            Dante_Lib_SQL_DB::get_instance()->open($sql);
        }
    }
    /**
     * Пересчитываем текущее количество голосов опроса.
     * @param int $id
     */
    public function recalcVotes($id){        
        $sql="UPDATE {$this->_tableName} SET `votes`=(SELECT sum(votes) as votes FROM `{$this->_tableAnswers}` WHERE poll_id={$id}) WHERE id={$id}";
        Dante_Lib_SQL_DB::get_instance()->open($sql);
    }
    /**
     * Добавляем один голос к конкретному ответу
     * @param int $poll_id
     * @param int $answer_id
     */
    public function addVote($poll_id, $answer_id){
        $sql="UPDATE {$this->_tableAnswers} SET `votes`=(`votes`+1) WHERE `id`={$answer_id} AND `poll_id`={$poll_id}";
        Dante_Lib_SQL_DB::get_instance()->open($sql);
        $this->recalcVotes($poll_id);
    }
}
?>