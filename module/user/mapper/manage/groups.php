<?php
class Module_User_Mapper_Manage_Groups extends Module_Handbooksgenerator_Mapper_Base 
{      
    protected $_tableName = '[%%]user_groups';
    protected $_groupsTableName = '[%%]groups';
    protected $_divisionTableName = '[%%]division';
    protected $_divisionGroupsTableName = '[%%]division_groups';
    
    /**
     * выбираем группы пользователей
     * @author Elrafir
     * @return array 
     */
    public function getGroups() {
        $orm = new Dante_Lib_Orm_Table($this->_groupsTableName);
        $orm->select_array(false);
        return $orm->getFields();        
    }
    /**
     * выбираем группу по ID
     * @author Elrafir
     * @return array 
     */
    public function getGroup($id=false) {
        if(!$id)return false;
        $orm = new Dante_Lib_Orm_Table($this->_groupsTableName);
        $orm->select(array('group_id'=>$id));
        
        $model = new Module_User_Model_Groups();
        $model->group_id = $orm->group_id;
        $model->group_name = $orm->group_name;
        $model->group_caption = $orm->group_caption;
        $model->is_system = $orm->is_system;
        
        return $model;
    }
    /**
     * Редактируем/добавляем запись в группе пользователей
     * @param Module_User_Model_Groups $model
     */
    public function apply(Module_User_Model_Groups $model){
        $orm = new Dante_Lib_Orm_Table($this->_groupsTableName);
        $orm->group_id = $model->group_id;
        $orm->group_caption = $model->group_caption;
        
        if($model->group_name) $orm->group_name = $model->group_name;
        $orm->apply();
    }
    
    public function del($id) {
        $table = new Dante_Lib_Orm_Table($this->_groupsTableName);
        $table->delete(array(
            'group_id' => $id
        ));
    }
    
    /**
     * определяем ид группы юзера
     * @param int $userId
     * @return int 
     */
    public function getUserGroupId($userId)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);

        $table->get()->where(array('user_id'=>$userId));
        $table->fetch();

        return $table->group_id;
    }
    /**
     * Добавление/апдейт принадлежности пользователя к группам
     * @param int $uid
     * @param array|string|false $groups
     */
    public function setUserGroup($uid, $groups=false){
        //Удаляем имеющиеся записи
        $sql = "DELETE FROM {$this->_tableName} WHERE user_id={$uid}";
        Dante_Lib_SQL_DB::get_instance()->exec($sql);
        //Герерируем инсерт и добавляем поля;
        $set = array();
        if(!$groups){
            return false;
        }elseif(!is_array($groups)){            
            $set[] = "({$uid}, {$groups})";
        }else{
            foreach($groups as $row){
                $set[]="({$uid}, {$row})";
            }
        }
        $values = join(', ',$set);
        $sql = "INSERT INTO {$this->_tableName} (`user_id`, `group_id`) VALUES {$values}";
        Dante_Lib_SQL_DB::get_instance()->exec($sql);
    }

    /**
    * Добавление группы для пользователя
    * @author Dorian
    */
    public function addGroup($uid, $gid) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->user_id = $uid;
        $table->group_id = $gid;
        return $table->insert();
    }
    
    /**
     * определяем раздел по урлу
     * @param string $url
     * @return mixed 
     */
    public function getDivisionByUrl($url)
    {
        //а есть ли запись для данной штуки ? 
        $table = new Dante_Lib_Orm_Table($this->_divisionTableName);
        if(!$table->exist(array(
            'link' => $url
        ))){
            return false;
        }
        
        $table = new Dante_Lib_Orm_Table($this->_divisionTableName);
        
        $table->get()->where(array('link'=>$url))
            ->limit(array(0, 1))->orderby('id desc');
        $table->fetch();

        return $table->id;
    }
}
?>