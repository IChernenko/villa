<?php

/**
 * Description of manage
 *
 * @author Mort
 */
class Module_User_Mapper_Manage_User extends Module_Handbooksgenerator_Mapper_Base {

    protected $_tableName = '[%%]users';
    protected $_userGroupsTableName = '[%%]user_groups';
    protected $_groupsTableName = '[%%]groups';
    protected $_userProfileTableName = '[%%]user_profile';

    /**
     * Выборка юзеров по параметрам.
     * @param type $params
     * @return type
     */
    public function getUsers($params) {

        $join   = array(
            "LEFT JOIN {$this->_userProfileTableName} as t2 ON t2.user_id = t1.id ",
            "LEFT JOIN {$this->_userGroupsTableName} as t3 ON t3.user_id = t1.id",
            "LEFT JOIN {$this->_groupsTableName} as t4 ON t3.group_id=t4.group_id"
        );
        $cols   =    array(
            "t1.*",
            "t2.*",
            "t3.group_id",
            "t4.group_caption"
        );
        return $this->getRowsByParams($params, $cols, $join);
    }
    
    protected function _instanceModel()
    {
        return new Module_User_Model();
    }

    public function getUser($uid) 
    {
        $model = $this->_instanceModel();
        if(!$uid) return $model;
        
        $sql = "SELECT `u`.*, `ug`.`group_id` 
                FROM {$this->_tableName} AS `u`
                LEFT JOIN {$this->_userGroupsTableName} AS `ug` ON `ug`.`user_id` = `u`.`id`
                WHERE `u`.`id` = {$uid}";
        $f = Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
        $model->id = $f['id'];
        $model->email = $f['email'];
        $model->password = $f['password'];
        $model->rating = $f['rating'];
        $model->login = $f['login'];
        $model->active = $f['is_active'];
        $model->group_id = $f['group_id'];
        
        $model = $this->_getProfile($uid, $model);
        
        return $model;
    }
    
    protected function _getProfile($uid, $model)
    {
        $table = new Dante_Lib_Orm_Table($this->_userProfileTableName);
        $table->select(array('user_id' => $uid));
        
        if($table->getNumRows() == 0) return $model;
        $model->fio = $table->fio;
        
        return $model;
    }

    /**
     * легкая выбрка групп
     * @return array
     */
    public function getGroups()
    {
        $sql = "select
                    *
                from ".$this->_groupsTableName;
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $types = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $types[$f['group_id']] = array(
                'id' => $f['group_id'],
                'name' => $f['group_caption']
            );
        }

        return $types;
    }

    public function apply($model)
    {
        $ormUser    = new Dante_Lib_Orm_Table($this->_tableName);
        $ormGroup   = new Dante_Lib_Orm_Table($this->_userGroupsTableName);
        //Добавляем контент
        $ormUser->login     = $model->login;
        $ormUser->email     = $model->email;
        if($model->password){
            $ormUser->password  = md5($model->password);
        }
        $ormUser->rating    = $model->rating;
        $ormUser->is_active = 1;
        //Обновляем основную таблицу
        if(!$model ->id){
            $model->id = $ormUser->insert();
        }else{
            $ormUser->update(array('id'=>$model->id));
        }

        //Таблица  профайла
        $this->_applyProfile($model);

        $cond = (array('user_id'=>$model->id));
        //Таблица принадлежности к группе
        $ormGroup->user_id = $model->id;
        $ormGroup->group_id = $model->group_id;
        $ormGroup->apply($cond);
        return $model->id;
    }

    protected function _applyProfile($model)
    {
        $tableObj = new Dante_Lib_Orm_Table($this->_userProfileTableName);
        $cond = array('user_id'=>$model->id);
        $tableObj->user_id = $model->id;
        $tableObj->fio     = $model->fio;
        $tableObj->apply($cond);
    }
}

?>
