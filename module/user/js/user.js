user = {
    controller: 'module.user.controller.manage.user',
    submit: function(obj){
        if(handbooks.buttonsDisabled())return false;
        vars =$("#editUserForm").serializeArray();
        url = 'ajax.php?controller='+user.controller+'&action=edit';
        $.ajax({
            type: 'POST',
            data:vars,
            url: url,
            dataType : "json",
            success: function (data){
                handbooks.buttonsEnabled();
                if(data.result==1){                    
                    $('#modalWindow').arcticmodal('close');
                    handbooks.formSubmit();
                    $('#modalWindow .modal-body').html(data.html);                                    
                }else{
                    alert(data.message)
                }
            }
        });
    },
    
    submitGroup: function(obj){
        if(handbooks.buttonsDisabled())return false;
        vars =$("#editUserForm").serializeArray();
        url = 'ajax.php?controller=module.user.controller.manage.groups&action=edit';
        $.ajax({
            type: 'POST',
            data:vars,
            url: url,
            dataType : "json",
            success: function (data){
                handbooks.buttonsEnabled();
                if(data.result==1)
                {                    
                    $('#modalWindow').arcticmodal('close');
                    handbooks.formSubmit();                                  
                }else{
                    alert(data.message)
                }
            }
        });
    }
    
    
}