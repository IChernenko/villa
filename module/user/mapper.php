<?php

/**
 * Маппер пользователя
 *
 * @author dorian
 */
class Module_User_Mapper 
{
    
    /**
     * Добавить пользователя
     * @param App_User_Model $user модель пользователя 
     */
    public function add(Module_User_Model $user) {
        $table = new Dante_Lib_Orm_Table('[%%]users');
        $table->email = $user->email;
        $table->password = md5($user->password);
        if ($user->login) {
            $table->login = $user->login;
        }
        $table->rating = $user->rating;
        $table->is_active = (int)$user->active;
        $user->id = (int)$table->insert();
        
        $tableGroups = new Dante_Lib_Orm_Table('[%%]user_groups');
        $tableGroups->user_id = $user->id;
        $tableGroups->group_id = $user->group_id;
        $tableGroups->insert();
        
        return $user->id;
    }

    /**
     * Получение модели пользователя
     * @return Module_User_Model || false;
     */
    public function get($uid) {
        $table = new Dante_Lib_Orm_Table('[%%]users');
        $table->select(array('id'=>$uid));

        if ($table->getNumRows() == 0) return false;

        $user = new Module_User_Model();
        $user->id = $table->id;
        $user->email = $table->email;
        $user->login = $table->login;
        $user->rating = $table->rating;
        if ($table->login) {
            $user->login = $table->login;
        }

        return $user;
    }
    
    public function getProfile($uid)
    {
        $sql = 'SELECT * FROM `[%%]user_profile` WHERE `user_id` = '.$uid;
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        if(!$r) return false;
        $profile = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r);
        
        return $profile;
    }
    
    public function saveProfile($profile)
    {
        $uid = $profile['user_id'];
        $table = new Dante_Lib_Orm_Table('[%%]user_profile');
        $r = $table->exist(array('user_id'=>$uid));
        foreach($profile as $key=>$value)
        {
            if($r && $key == 'user_id') continue;
            $table->$key = $value;
        }
        if($r) $table->update(array('user_id'=>$uid));
        else $table->insert();
        return true;
    }
    
    public function changeEmail($uid, $email)
    {
        $table = new Dante_Lib_Orm_Table('[%%]users');
        $table->email = $email;
        $table->update(array('id'=>$uid));
        return $email;
    }
    
    public function changePassword($uid, $cur, $new)
    {
        $table = new Dante_Lib_Orm_Table('[%%]users');
        $r = $table->exist(array('id'=>$uid, 'password'=>md5($cur)));
        if(!$r) return false;
        $table->password = md5($new);
        $table->update(array('id'=>$uid));
        return true;
    }

    public function activate($uid) {
        $user = Module_User_Dmo::dmo()->byId($uid)->fetch();
        if ($user->getNumRows() == 0) return false;
        $user->is_active = 1;
        $user->apply();
    }

    public function getUidByEmail($email) {
        $table = new Dante_Lib_Orm_Table('[%%]users');
        return $table->select(array('email'=>$email))->id;
    }
    
    public function getUidByLogin($login) {
        $table = new Dante_Lib_Orm_Table('[%%]users');
        return $table->select(array('login'=>$login))->id;
    }
    
    public function getNameByEmail($email)
    {
        $uid = $this->getUidByEmail($email);
        $table = new Dante_Lib_Orm_Table('[%%]user_profile');
        $r = $table->exist(array('user_id'=>$uid));
        if($r)
        {
            $nameRow = $table->select(array('user_id'=>$uid));
        }
        else return '';
        
        $userName = $nameRow->name.' '.$nameRow->surname;
        return trim($userName);
    }
    
    public function getPassByUid($uid)
    {
        $table = new Dante_Lib_Orm_Table('[%%]users');
        return $table->select(array('id'=>$uid))->password;
    }

    public function getEmailById($uid)
    {
        $table = new Dante_Lib_Orm_Table('[%%]users');
        return $table->select(array('id'=>$uid))->email;
    }
}

?>
