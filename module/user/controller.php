<?php

class Module_User_Controller extends Dante_Controller_Base
{
    protected function _instanceMapper()
    {
        return new Module_User_Mapper();
    }

    protected function _getProfileAction()
    {
        $mapper = $this->_instanceMapper();
        
        $uid = Dante_Helper_App::getUid();
        if(!$uid) return array(
            'result' => 0,
            'message' => 'Доступ только для залогиненных пользователей!'
        );
        
        $profile = $mapper->getProfile($uid);
        if(!$profile['user_id']) 
            $profile['user_id'] = $uid;        
        
        if($this->_getRequest()->get('full'))
            $profile['email'] = $mapper->get($uid)->email;
        
        if($this->_getRequest()->get('json'))
            return array(
                'result' => 1,
                'profile' => $profile
            );
        else return $profile;
    }
    
    protected function _saveProfileAction()
    {
        $profile = $this->_getRequest()->get('profile');
        $mapper = $this->_instanceMapper();
        
        $mapper->saveProfile($profile);
        
        $message = 'Ваши изменения сохранены!';
        if($this->_getRequest()->get('json'))
            return array(
                'result' => 1,
                'message' => $message
            );
        else return $message;
    }
    
    protected function _changeEmailAction()
    {
        $uid = $this->_getRequest()->get('user_id');
        $newEmail = $this->_getRequest()->get('email');
        
        $mapper = $this->_instanceMapper();
        $result = $mapper->changeEmail($uid, $newEmail);
        
        $message = 'Новый E-mail сохранен!';
        if($this->_getRequest()->get('json'))
            return array(
                'result' => 1,
                'message' => $message,
                'email' => $result
            );
        else return $message;
    }
    
    protected function _changePasswordAction()
    {
        $uid = $this->_getRequest()->get('user_id');
        $curPass = $this->_getRequest()->get('curPass');
        $newPass = $this->_getRequest()->get('newPass');
        
        $mapper = $this->_instanceMapper();
        $res = $mapper->changePassword($uid, $curPass, $newPass);
        if(!$res) 
            return array(
                'result' => 0,
                'message' => 'Текущий пароль введен неправильно!'
            );
        
        $message = 'Ваш пароль успешно изменен!';
        if($this->_getRequest()->get('json'))
            return array(
                'result' => 1,
                'message' => $message
            );
        else return $message;
    }
}

?>