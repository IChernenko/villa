<?php

$userMapper = new Module_User_Mapper();
$groupMapper = new Module_Group_Mapper();
$mapper = new Module_User_Mapper_Manage_Groups();

$uid = $userMapper->getUidByEmail('admin');
if (!$uid) throw new Exception("Cant find admin user");

$gid = $groupMapper->getByName('admin');
if (!$gid) throw new Exception("Cant find admin group");


$mapper->addGroup($uid, $gid);
//if (!$mapper->addGroup($uid, $gid)) throw new Exception("Cant add  user to admin group");