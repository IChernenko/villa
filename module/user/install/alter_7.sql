ALTER TABLE `[%%]groups` ADD `is_system` TINYINT(1) NOT NULL DEFAULT 0;

UPDATE `[%%]groups` SET `is_system` = 1 WHERE `group_name` IN('user', 'admin');