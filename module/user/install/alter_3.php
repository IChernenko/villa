<?php

$metaData = Dante_Lib_SQL_DB::get_instance()->getMetaData('[%%]users');

if (!$metaData->fieldExists('login')) {
    $sql = 'ALTER TABLE [%%]users ADD column login varchar(32)';
    Dante_Lib_SQL_DB::get_instance()->exec($sql);
}

?>
