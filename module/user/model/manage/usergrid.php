<?php

class Module_User_Model_Manage_Usergrid {
   
    public $sortable_rules=array(
        'name'=>'id',
        'type'=>'desc',
    );
    
    public $rows_in_page = 100;
    
    public $filter_rules=array(        
        array(
            'name'=>'id',
            'format'=>'`t1`.`%1$s` = "%2$s"'
            ),
        array(
            'name'=>'login',
            'format'=>'`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
            ),
        array(
            'name'=>'email',
            'format'=>'`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
            ),        
        array(
            'name'=>'rating',
            'format'=>'`t1`.`%1$s` = "%2$s"'
            ),
        array(
            'name'=>'group_id',
            'format'=>'`t3`.`group_id` = "%2$s"'
            ),
        
        );
}
?>