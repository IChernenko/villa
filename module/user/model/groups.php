<?php
class Module_User_Model_Groups{
    /**
     * id группы ... 
     * @var Int 
     */
    public $group_id;
    /**
     * Системное название группы ... 
     * @var string 
     */
    public $group_name;
    /**
     * Публичное название ... 
     * @var string 
     */
    public $group_caption;   
    
    public $is_system;
}
?>