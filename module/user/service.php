<?php



/**
 * Сервис по работе с пользователями
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_User_Service {
    
    /**
     * Регистрирует пользователя
     * @param type $params 
     */
    public function register($params) {
        $password = 123; //md5(time());
        
        $user = new Module_User_Model();
        $user->email = $params['email'];
        $user->password = $password;
        $userMapper = new Module_User_Mapper();
        if (!$userMapper->add($user)) throw new Exception('cant register user : '.$params['email']);
        
        $params['password'] = $password;
        
        $this->_notifyUser($params);


        return $user->id;
    }
    
    public function activate($uid) {
        $userMapper = new Module_User_Mapper();
        return $userMapper->activate($uid);
    }
    
    /**
     * Отправить уведомление пользователю об успешной регистрации на email.
     * @param type $params 
     */
    protected function _notifyUser($params) {
        // формируем шаблон письма
        $view = new Module_User_View_Emailnotify();
        $message = $view->draw($params);
        
        $subject = 'Регистрация успешна';
        $sender = new Module_Mail_Service_Sender();
        $sender->send($params['email'], $subject, $message);
        
    }
    
    public function getUidByEmail($email) {
        $userMapper = new Module_User_Mapper();
        return $userMapper->getUidByEmail($email);
    }
}

?>
