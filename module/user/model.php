<?php


/**
 * Модель пользователя
 *
 * @author dorian
 */
class Module_User_Model {
	
    public $id;

    public $email;

    public $password;
        
    public $rating = 1;
    
    public $login = false;
    
    public $active = false;
    
    public $group_id;
    
    public $fio;
}

?>