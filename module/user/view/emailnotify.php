<?php



/**
 * Description of emailnotify
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_User_View_Emailnotify {
    
    public function draw($params) {
        $tplFileName = '../module/user/template/registration_notify.html';

        $customTpl = Dante_Helper_App::getWsPath().'/user/registration_notify.html';
        if (file_exists($customTpl)) $tplFileName = $customTpl;
        

        $tpl = new Dante_Lib_Template();
        $tpl->params = $params;
        $tpl->draw($tplFileName);
        
        return true;
    }
}

?>
