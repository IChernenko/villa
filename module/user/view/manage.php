<?php
class Module_User_View_Manage
{
    public $groupList = array();
    
    public function drawForm($model)
    {
        $tpl = new Dante_Lib_Template();
        $tpl->model = $model;
        $tpl->groupList = $this->groupList;
        
        $fileName = '../module/user/template/form.html';
        $ws = Dante_Helper_App::getWsName();
        
        if (file_exists("template/{$ws}/user/form.html")) {
            $fileName = "template/{$ws}/user/form.html";
        }
        
        return $tpl->draw($fileName);
    }
    
    public function formGroup($model)
    {
        $tpl = new Dante_Lib_Template();
        $tpl->model = $model;
        
        $fileName = '../module/user/template/formgroups.html';
        $ws = Dante_Helper_App::getWsName();
        if (file_exists("template/{$ws}/user/formgroups.html")) {
            $fileName = "template/{$ws}/user/formgroups.html";
        }
        return $tpl->draw($fileName);
    }
}
?>