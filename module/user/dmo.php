<?php



/**
 * Description of dmo
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 * @author Elrafir <elrafir@gmail.com>
 */
class Module_User_Dmo extends Dante_Lib_Orm_Table{
    /**
     * Рабочая таблица в БД
     * @var string 
     */
    protected $_tableName = '[%%]users';
    
    /**
     *
     * @return Module_User_Dmo 
     */
    public static function dmo() {
        return self::getInstance();
    }
    /**
     * Ищем юзера по id
     * @param int $id
     * @return array|false
     */
    public function byId($id) {
        return $this->getByAttributes(array('id'=>$id));
    }
    /**
     * Ищем юзера по login
     * @param string $login
     * @return array|false
     */
    public function byLogin($login) {
        return $this->getByAttributes(array('login'=>$login));
    }
    /**
     * Ищем юзера по email
     * @param string $email
     * @return array|false
     */
    public function byEmail($email) {
        return $this->getByAttributes(array('email'=>$email));
    }
    /**
     * Фильтр по проверочному коду.
     * @param type $code
     * @return type
     */
    public function byCode($email, $code){
        return $this->getByAttribute(array('email'=>$email, 'restory_code'=>$code));
    }

    /**
     * Ищем юзера по login помимо строки с заданным id 
     * @param string $login
     * @param int $login
     * @return array|false
     */
    public function byLoginNotId($login, $id){
        $condition = array(
            'login'=>$login,
            'id'=>array(
                'operation'=>'<>',
                'value'=>$id
            )
        );
        return $this->getByAttributes($condition);
    }
    /**
     * Ищем юзера по email помимо строки с заданным id 
     * @param int $email
     * @param int $email
     * @return array|false
     */
    public function byEmailNotId($email, $id){
        $condition = array(
            'email'=>$email,
            'id'=>array(
                'operation'=>'<>',
                'value'=>$id
            )
        );
        return $this->getByAttributes($condition);
    }
    /**
     * Проверяет уникальность полей login, email в ситуациях с новой записью ил редактированию
     * @param object $model
     */
    public function isUniqueFields($model){
        if((int)$model->id){
            //Проверяем уникальность не принимая во внимание имеющуюсястроку с $id
            if($this->byLoginNotId($model->login, $model->id)->fetch()->getNumRows()>0){                
                return 'такой Логин уже используется';
            }
            if($this->byEmailNotId($model->email, $model->id)->fetch()->getNumRows()>0){                
                return 'такой email уже используется';
            }            
        }else{
            //Проверяем уникальность не принимая во внимание имеющуюсястроку с $id
            if($this->byLogin($model->login)->fetch()->getNumRows()>0){
                return 'такой Логин уже используется';
            }
            if($this->byEmail($model->email)->fetch()->getNumRows()>0){
                return 'такой email уже используется';
            }
        }
        return false;
    }
    
}

?>
