<?php
global $package;
$package = array(
    'version' => '7',
    'name' => 'module.user',
    'dependence' => array(
        'module.group',        
        'module.auth',
        'module.lang'
    ),
    'js' => array(
//        'ws' =>  array(
//            'admin' => array(
                '../module/user/js/user.js?1',                
//            )
//       )     
    ), 
);