<?php

$url = Dante_Controller_Front::getRequest()->get_requested_url();

Dante_Lib_Router::addRule("/manage\/user\//", array(
    'controller' => 'Module_User_Controller_Manage_User'
));
Dante_Lib_Router::addRule("/manage\/usergroups\//", array(
    'controller' => 'Module_User_Controller_Manage_Groups'    
));

?>