<?php
class Module_User_Controller_Manage_Groups extends Module_Handbooksgenerator_Controller_Base {
    
    
    protected function _init() {
        parent::_init();
        $this->_mapper = new Module_User_Mapper_Manage_Groups;
    }
      /**
     * Отображение в гриде списка групп пользователей
     * @return string - html грида
     */
    protected function _drawAction(){
        $helper = new Module_Division_Helper();
        $helper->checkAccess();
        
        $mapper = new Module_User_Mapper_Manage_Groups;        
        $rows = $mapper->getGroups();
        $this->formEnable('module.user.controller.manage.groups');
        $this->tableParams['style'] = array(
            'width' => '700px'
        );
        $this->caption = 'Управление группами пользователей';
        $this->thead = array(
            'titles'=>array(
                'ID',
                'Системное название',
                'Публичное название',
                'Функции'
            )
        );        
        foreach($rows as $row){
            $cur=array();
            $cur['group_id']        = $row['group_id'];
            $cur['group_name']      = $row['group_name'];
            $cur['group_caption']   = $row['group_caption'];
            $cur['buttons'] = array(
                'id'=>$row['group_id'],
                'type'=> $row['is_system'] ? array('edit') : array('edit', 'del'),                
            );
            $this->tbody[$row["group_id"]]= array(
                'content'=>$cur,                
            );
        }
        $this->tfoot=array(            
            array(
                'button_filter'=>array(
                    "type"=>array(
                        'edit'
                    ),                    
                )
             ),       
        );
        return $this->generate();
    }
    
    /**
     * Форма добавления/редактирования группы     
     * @return array from ajax - хтмл формы
     */
    protected function _drawFormAction(){
        
        $mapper = new Module_User_Mapper_Manage_Groups;
        $tpl    = new Module_User_View_Manage;
        $request = $this->_getRequest();        
        $id     = $request->get_validate('id', 'int');
        $model   = $mapper->getGroup($id);        
        $html   = $tpl->formGroup($model);
        return array(
            'result'=>1,
            'html'=>$html
        );
    }
    
    protected function _editAction(){
        $model      = new Module_User_Model_Groups;
        $mapper     = new Module_User_Mapper_Manage_Groups;
        $request    = $this->_getRequest();
        
        $model->group_id        = $request->get_validate('group_id', 'int');
        $model->group_name      = $request->get_validate('group_name', 'text');
        $model->group_caption   = $request->get_validate('group_caption', 'text');
        
        $mapper->apply($model);
        
        return array(
            'result' => 1
        );
    }   
}

?>