<?php

/**
 * Description of controller
 *
 * @author Elrafir
 */

class Module_User_Controller_Manage_User extends Module_Handbooksgenerator_Controller_Base 
{
    protected $_alias = "module.user.controller.manage.user";

    protected function _init() {
        parent::_init();
        $this->_mapper = new Module_User_Mapper_Manage_User();
    }
    
    protected function _instanceView()
    {
        return new Module_User_View_Manage();
    }
    
    protected function _instanceModel()
    {
        return new Module_User_Model_Manage_User();
    }

    /**
     * Отображение в гриде списка юзеров
     * @return string - html грида
     */
    protected function _drawAction()
    {
        $hotelHelper = new Module_Division_Helper();
        $hotelHelper->checkAccess();
        
        $model = $this->_instanceModel();
        $mapper = new Module_User_Mapper_Manage_User();       
        
        $params = $this->getParams($model);        
        $list = $mapper->getUsers($params);        
        $this->formEnable($this->_alias);
        
        $this->thead = array(
            $this->_genTitles(),
            $this->_genFilters()
        );
        $this->tbody = $this->_genRows($list['rows']);  
        
        $this->tfoot=array(
            array($this->paginator($list['count'], $params)),
            array(
                'button_filter'=>array(
                    "type"=>array(
                        'edit'
                    ),                    
                )
             ),       
        );
        
        return $this->generate();
    }
    
    /**
     * Форма добавления/редактирования пользователя     
     * @return array from ajax - хтмл формы
     */
    protected function _drawFormAction($model = false)
    {        
        $view = $this->_instanceView();
        $id = $this->_getRequest()->get_validate('id', 'int');  
        
        if(!$model) $model = $this->_mapper->getUser($id);        
        $view->groupList = $this->_getGroups();
        
        return array(
            'result' => 1,
            'html' => $view->drawForm($model)
        );
    }
    
    
    protected function _editAction() 
    {        
        $model = $this->_populateModel();
        //На лету проверяем нет ли совпадений по ключевым уникальным полям
        try
        {
            if($error = Module_User_Dmo::dmo()->isUniqueFields($model))
                throw new Exception($error);
                
            $this->_validate($model);
            
            if(!$this->_mapper->apply($model)) 
                throw new Exception('Что-то пошло не так. Обратитесь за помощью к администратору сайта.');
        }
        catch(Exception $e)
        {            
            return array(
               'result'=>0,
               'message'=> $e->getMessage()
            );
        }        
        return $this->_drawFormAction($model);
    }
    
    protected function _populateModel()
    {    
        $model = new Module_User_Model_Manage_User();    
        //Вбиваем присланные данные
        $model->id      = $this->_getRequest()->get_validate('id', 'int', false);
        $model->email   = $this->_getRequest()->get_validate('email', 'email', false);
        $model->login   = $this->_getRequest()->get_validate('login', 'text', false);
                
        if($password = $this->_getRequest()->get_validate('password', 'password'))
            $model->password = $password;
        
        $model->rating = $this->_getRequest()->get_validate('rating', 'int', false);
        $model->group_id = $this->_getRequest()->get_validate('group_id', 'int', 5);        
        $model->fio  = $this->_getRequest()->get_validate('fio', 'text', false);
        return $model;
    }


    protected function _genTitles()
    {          
        $titles = array(
            array(
                'content'=>"id",
                'sortable'  =>'id',
                'sortable_def' =>'desc',
                'params'=>array(
                    'style'=>array(
                        'width'=>'14px'
                    )
                )
            ),
            array(
                'content'   =>'Пользователь',
                'sortable'  =>'login',                
                'params'=>array(
                    'style'=>array(
                        'width'=>'100px'
                    )
                )
            ),
            array(
                'content'   =>'email',
                'sortable'  =>'email',
                'params'=>array(
                    'style'=>array(
                        'width'=>'100px'
                    )
                )
            ),
            array(
                'content'   =>'Рейтинг',
                'sortable'  =>'rating',
                'params'=>array(
                    'style'=>array(
                        'width'=>'132px'
                    )
                )
            ),
            array(
                'content'   =>'Группа',
                'sortable'  =>'group_name',
                'params'=>array(
                    'style'=>array(
                        'width'=>'132px'
                    )
                )
            ),
            array(
                'content'   =>'Функции',
                'params'=>array(
                    'style'=>array(
                        'width'=>'132px'
                    )
                )                
            ),
        );
        
        if(!Dante_Lib_Config::get('user.displayLogin'))
        {
            unset($titles[1]);
        }
        if(!Dante_Lib_Config::get('user.displayRating'))
        {
            unset($titles[3]);
        }
        
        return $titles;
    }
    
    protected function _genFilters()
    {        
        $request = $this->_getRequest();  
        $filters = array(
            '',
            "login"=>array(
                "type"=>"text",
                "value"=>$request->get_validate("login", "text"),
            ),            
            "email"=>array(
                "type"=>"text",
                "value"=>$request->get_validate("email", "text"),
            ),            
            "rating"=>array(
                "type"=>"text",
                "value"=>$request->get_validate("rating", "int"),
            ),
            'group_id'=>array(
                'type'=>'combobox',
                'items'=> $this->_getGroups(),
                'value'=> $request->get_validate('group_id', 'int')
            ),          
            array(
                "type"=>"button_filter"
            ),            
        );
        
        if(!Dante_Lib_Config::get('user.displayLogin'))
        {
            unset($filters['login']);
        }
        if(!Dante_Lib_Config::get('user.displayRating'))
        {
            unset($filters['rating']);
        }
        
        return $filters;
    }


    protected function _genRows($rows)
    {               
        $table = array();
       
        foreach($rows as $row)
        {           
            $cur = array();
            $cur['id'] = $row['id'];
            $cur['login'] = $row['login'];            
            $cur['email'] = $row['email'];            
            $cur['rating'] = $row['rating'];            
            $cur['group'] = $row['group_caption'];
            $cur['buttons'] = array(
                'id'=>$row['id'],
                'type'=>array('edit', 'del'),
                'content'=>$this->_buttonActive($row['id'],$row['is_active'])
            );            
            
            if(!Dante_Lib_Config::get('user.displayLogin'))
                unset($cur['login']);
            if(!Dante_Lib_Config::get('user.displayRating'))
                unset($cur['rating']);
            
            if($row['is_active'])
                $params['class']='success';
            else
                $params['class']='error';            
            
            $table[$row["id"]]= array(
                'content'=>$cur,
                'params'=>$params
            );
        }
        
        return $table;
    }
    
    protected function _getGroups()
    {
        $mapper = new Module_User_Mapper_Manage_Groups();
        $result = $mapper->getGroups();
        $groups = array();
        foreach($result as $val)
        {
            $groups[$val['group_id']] = $val['group_caption'];
        }
        return $groups;
    } 
    
    
    protected function _validate($model)
    {
        //если нет id то должен быть пароль        
        $required = array(
            'password'=> 'Пароль',
            'email'=> 'Email',    
        );        

        if($model->id) unset($required['password']);        
        foreach($required as $k=>$v){
            if(!$model->$k){
                throw new Exception("Поле {$v} введено не корректно");
            }
        }     
    } 
    
}
?>
