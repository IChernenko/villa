/**
 * Инициирование поиска
 */
function doSearch(inputId) {
    //debugger;
    if (inputId == undefined) {
        inputId = '#searchString';
    }

    var searchString = $(inputId).val();
    var url = encodeURIComponent(searchString);
    if (searchString.length == 0) return false;   
    
    location.assign('search/'+url);    
}

function doSearchEnt(event, inputId)
{
    if(event.which == 13) doSearch(inputId);
}