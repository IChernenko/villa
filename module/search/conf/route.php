<?php

$url = Dante_Controller_Front::getRequest()->get_requested_url();

Dante_Lib_Router::addRule("/^\/search\/([\wА-Яа-я\%\-\.\,\?\!]+)$/", array(
    'controller' => 'Module_Search_Controller',
    'action' => 'search'
));

Dante_Lib_Router::addRule("/^\/search\/([\wА-Яа-я\%\-\.\,\?\!]+)\/page(\d+)$/", array(
    'controller' => 'Module_Search_Controller',
    'action' => 'search'
));

?>