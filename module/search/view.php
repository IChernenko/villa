<?php
/**
 * View для отображения результатов поиска
 * 
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 * Date: 27.05.12
 * Time: 12:18
 * To change this template use File | Settings | File Templates.
 */
class Module_Search_View
{
    public $paginator = '';
    
    public function draw($model) {
        $tplFileName = '../module/search/template/search.html';

        $customTpl = 'template/'.Dante_Helper_App::getWsName().'/search/list.html';
        if (file_exists($customTpl)) 
        {
            $tplFileName = $customTpl;
        }

        $tpl = new Dante_Lib_Template();
        $tpl->paginator = $this->paginator;
        $tpl->request = $model->request;
        $tpl->items = $model->result;
        return $tpl->draw($tplFileName);
    }
}
