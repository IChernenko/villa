<?php

class Module_Search_Model
{
    /**
     * Запрос
     * @var string 
     */
    public $request;
    
    /**
     * Результат поиска
     * @var array 
     */
    public $result = false;
    
    /**
     * Лимит поиска(в случае использования пейджинатора)
     * @var boolean || string 
     */
    public $limit = false;
    
    /**
     * Общее кол-во найденых строк
     * @var int 
     */
    public $total = 0;
}

?>