<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of controller
 *
 * @author dorian
 */
class Module_Search_Controller extends Dante_Controller_Base{
    
    protected function _searchAction() {
        $model = new Module_Search_Model();        
        $view = new Module_Search_View();
        
        $model->request = urldecode($this->_getParam('%1'));
        
        if(Dante_Lib_Config::get('search.pagingEnable'))
        {
            $paginator = new Lib_Paginator_Driver();
            $page = (int)$this->_getParam('%2', 1);
            $paginator->link = str_replace('/page'.$this->_getParam('%2'), '', $this->_getParam('%0'));
            
            $itemsPerPage = Dante_Lib_Config::get('search.itemsPerPage');
            $model->limit = "LIMIT {$itemsPerPage} OFFSET ".((($page-1)*$itemsPerPage));            
        }       
              
        Dante_Lib_Observer_Helper::fireEvent('search', $model);
        
        if(Dante_Lib_Config::get('search.pagingEnable') && $model->total != 0)
        {            
            $paginator->draw($model->total, $page, $itemsPerPage);
            $view->paginator = $paginator->html;
        }
                
        $html = $view->draw($model);
        
        return $html;
    }
}

?>
