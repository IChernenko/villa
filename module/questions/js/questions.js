questions = {
    controller: "module.questions.manage.controller",
    setColors: function() {
        $('.reviewElement span').addClass('badge-success');
        $('.reviewElement').each(function() {
            var approved = $(this).attr('approved');
            if(approved == '0'){
                $(this).children('span').removeClass('badge-success');
                $(this).children('span').addClass('badge-important');
                var parent_id = jsphp.str_replace('reviewElement parent_', '', $(this).attr('class'));
                questions.setWarningColorsRecursively(parent_id);
            }
        });
    },
    
    setWarningColorsRecursively: function(parent_id) {
        if(parent_id==0){return;}
        $('#row_'+parent_id).children('span').removeClass('badge-success');
        $('#row_'+parent_id).children('span').removeClass('badge-important');
        $('#row_'+parent_id).children('span').addClass('badge-warning');
        
        var new_parent_id = jsphp.str_replace('reviewElement parent_', '', $('#row_'+parent_id).attr('class'));
        questions.setWarningColorsRecursively(new_parent_id);
    },
    
    format: function() {
        $('.reviewElement').hide();
        $('.parent_0').show();
        
        //стрелочки
        $('.reviewElement').each(function() {
            //добавляем красивую байду слева для деток
            $(this).children('sup').html('|');
            $(this).children('sup').last().html('|_');
            
            //добавляем плюсики
              var parent_id = jsphp.str_replace('reviewElement parent_', '', $(this).attr('class'));
              if(typeof($('#row_'+parent_id+' .icon-plus').get(0)) == "undefined"){
                $('#row_'+parent_id).html(
                    
                        $('#row_'+parent_id).html()+'<i class="icon-plus" onclick="questions.showChilds('+parent_id+')" style="cursor:pointer;"></i>' 
                    
                    );
                }
        });
        
        //расцветка
        questions.setColors();
        
        //разрешаем добавлять деток только на 1 ур
        $('.icon-download').hide();
        $('.parent_0 span .icon-download').show();
    },
    
    showChilds: function(parent_id) {
        $('.parent_'+parent_id).show();
        $('#row_'+parent_id+' .icon-plus').addClass('icon-minus');
        $('#row_'+parent_id+' .icon-plus').removeClass('icon-plus');
        
        $('#row_'+parent_id+' .icon-minus').attr('onclick', 'questions.hideChilds('+parent_id+')');
    },
    
    hideChilds: function(parent_id) {
        questions.hideRecursively($('.parent_'+parent_id));
        $('#row_'+parent_id+' .icon-minus').addClass('icon-plus');
        $('#row_'+parent_id+' .icon-minus').removeClass('icon-minus');
        
        $('#row_'+parent_id+' .icon-plus').attr('onclick', 'questions.showChilds('+parent_id+')');
    },
    
    hideRecursively: function(item){
        item.each(function() {
            $(this).hide();
            var new_parent_id = jsphp.str_replace('row_', '', $(this).attr('id'));
            questions.hideChilds(new_parent_id);
        });
    },
    
    makeApproved: function(id){
        $.ajax({
            type: "POST",
            url: '/ajax.php',
            data: {
                controller: questions.controller,
                action: "approve",
                id:id
            },
            success: function(data) {
                if (data.result == 1) {
                    $('#row_'+id).attr('approved', '1');
                    questions.setColors();
                }
            }
        });
    },
    
    makeDelete: function(id){
        $.ajax({
            type: "POST",
            url: '/ajax.php',
            data: {
                controller: questions.controller,
                action: "delete",
                id:id
            },
            success: function(data) {
                if (data.result == 1) {
                    $('#row_'+id).remove();
                    questions.setColors();
                }
            }
        });
    },
    
    makeAddChild:function(id){
        this.createEditDiv('add', id);
        var divOffset = {
            top:50,
            left:100
        };
        
        if(id != 0){
            divOffset = $('#row_'+id).offset();
        }
        
        $('#questionsEditDiv').css('top',  (30+divOffset.top)+'px');
        $('#questionsEditDiv').css('left', (30+divOffset.left)+'px');
        
        $('#row_edit_name').val('Администрация');
        $('#row_edit_email').val('');
        $('#row_edit_message').val('');
    },
    
    makeEdit:function(id){
        this.createEditDiv('edit', id);
        
        var divOffset = $('#row_'+id).offset();
        
        $('#questionsEditDiv').css('top',  (30+divOffset.top)+'px');
        $('#questionsEditDiv').css('left', (30+divOffset.left)+'px');
        
        var name = $('#row_'+id+' span #row_name').html();
        var email = $('#row_'+id+' span #row_email').html();
        var message = $('#row_'+id+' span #row_message').html();
        
        $('#row_edit_name').val(name);
        $('#row_edit_email').val(email);
        $('#row_edit_message').val(message);
    },
    
    saveTextEditForm:function(id){
        var id = $('#row_edit_id').val();
        var action = $('#row_edit_action').val();
        
        var name = $('#row_edit_name').val();
        var email = $('#row_edit_email').val();
        var message = $('#row_edit_message').val();
        
        $.ajax({
            type: "POST",
            url: '/ajax.php',
            data: {
                controller: questions.controller,
                action: action,
                id:id,
                
                name:name,
                email:email,
                message:message
            },
            success: function(data) {
                if (data.result == 1) {
                    if(action=='edit'){
                        $('#row_'+id+' span #row_name').html(name);
                        $('#row_'+id+' span #row_email').html(email);
                        $('#row_'+id+' span #row_message').html(message);

                        //подсветка текста
                        $('#row_'+id+' span').attr('data-original-title', message);
                    }
                    if(action=='add'){
                        jAlert('Запись была успешно добавлена. Для вступления изменений страница будет обновлена.', 'Успех', function() {
                            location.reload(); 
                          });
                    }
                    
                    questions.removeEditDiv();
                }
            }
        });
    },
    
    createEditDiv: function(action, id)
    {
        var div = document.createElement('div');
          div.setAttribute('id', 'questionsEditDiv');
          div.setAttribute('class', 'questionsEditDiv');
          div.style.position = "absolute";
          div.style.zIndex = "15";
          document.body.appendChild(div);
          
          var editHtml = '<label for="row_edit_name">'+
                        'Имя'+
                        '<font color="red">*</font>'+
                        ':'+
                        '</label>'+
                        '<input id="row_edit_name" type="text" style="width:300px;">'+
                        '<br>'+
                    
                        '<label for="row_edit_email">'+
                        'email'+
                        '<font color="red">*</font>'+
                        ':'+
                        '</label>'+
                        '<input id="row_edit_email" type="text" style="width:300px;">'+
                        '<br>'+
                    
                        '<label for="row_edit_message">'+
                        'Отзыв'+
                        '<font color="red">*</font>'+
                        ':'+
                        '</label>'+
                        '<textarea id="row_edit_message" style="width: 300px; height: 75px;" cols="80" rows="15"></textarea>'+
                        '<br>'+
                    '<center><input class="btn" type="button" onclick="questions.saveTextEditForm()" value="Сохранить">'+
                    '&nbsp&nbsp&nbsp&nbsp&nbsp'+
                    '<input class="btn" type="button" onclick="questions.removeEditDiv()" value="Закрыть форму"></center>'+
                    '';
                
        editHtml += '<input id="row_edit_action" type="text" style="display:none;" value="'+action+'">'+
                    '<input id="row_edit_id" type="text" style="display:none;" value="'+id+'">';
        
        $('#questionsEditDiv').html('<form class="well">'+editHtml+'</form>');
    },
    removeEditDiv: function()
    {
        $('#questionsEditDiv').remove();
    }
}