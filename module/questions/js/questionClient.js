$(document).ready(function(){
    $.ajax({
        type: "POST",
        url: "/ajax.php",
        data: {
            controller: "module.questions.controller",
            action: 'paging'
        },
        success: function(data)
        {
            // Если авторизованный пользователь - это администратор,
            // и ему выводится страница администрирования
            if(data.result == 0)
            {

            }
            // Если обычный пользователь,
            // и ему выводится страница вопросов
            else if(data.result == 1)
            {
                $("#prevQuestions").html(data['output']);
                $("#prevQuestions").append(data['paginator']);
                questionClient.addStyles();
                questionClient.addStylesPaginator();
                questionClient.setPage(data['page']);
//                if(data['auth'] == 1) questionClient.disableInputs();
            }

            // Режим отладки
            else if(data.result == 2) console.log(data);
        }
    });
});


questionClient = {
    
    answerLabel: '',
    
    //Операции с номером текущей страницы
    page: 1,
    
    setPage: function(pageNum)
    {
        this.page = pageNum;
    },
    
    getPage: function()
    {
        return this.page;
    },
    
    disableInputs: function()
    {
        $("#questions input").prop("disabled", true);
    },
    
    //Проверка заполненности полей
    checkValues: function()
    {
        $(".error").remove();
        var email = $("#questions_email").val();
        var message = $("#questions_message").val();

        if(!$("#questions_email").prop("disabled") && email == '')
        {
            $("#questions").append('<div class="error err-input">Введите E-mail!</div>');
            return false;
        }

        if(message == '')
        {
            $("#questions").append('<div class="error err-message">Введите вопрос!</div>');
            return false;
        }

        return true;
    },
    
    //Отправляем запрос на сервер
    sendQuestion: function()
    {
        var check = this.checkValues();
        if(!check) return false;

        jQuery.ajax({
            type: 'POST',
            url: '/ajax.php',
            data:
            {
                controller: "module.questions.controller",
                action: 'add',
                email: $("#questions_email").val(),
                message: $("#questions_message").val()
            },
            success: function(data)
            {
                if(data['result'] == 2) console.log(data['message']);//DEBUG
                if(data['result'] == 1)
                {
                    if(questionClient.getPage() != 1)
                    {
                        window.location.pathname = '/questions/';
                    }
                    $("#prevQuestions").html(data['output']);
                    $("#prevQuestions").append(data['paginator']);
                    questionClient.addStyles();
                    questionClient.addStylesPaginator();
                    questionClient.restoreDefaults();

                    jAlert('Спасибо за Ваш вопрос. Вопрос принят.', 'Ok');
                }
                else
                {
                    $("#questions").append('<div class="error err-input">'+data.message+'</div>');
                }
            }
        });
    },
    
    //Добавляем стили в вывод
    addStyles: function()
    {
        var rows = $("#prevQuestions").find("div");
        $(rows).each(function(){
            if(!$(this).hasClass("parent_0"))
                {
                    var name = $(this).find(".row-name");
                    $(name).css({"color":"#d28a24"});
                    var label = $(this).find(".row-message-label");
                    $(label).css({"display":"inline-block", "margin-left":"25px"});
                    $(label).html(questionClient.answerLabel+":&nbsp;");
                }
             var message = $(this).find(".row-message");
             var mLabel = $(this).find(".row-message-label");
             if($(message).width() == $(this).width())
                 {
                     var margin = $(mLabel).css("margin-left");
                     $(message).css("margin-left", margin);
                 }
        });
    },
    
    //Присваиваем стили пейджинатору
    addStylesPaginator: function()
    {
        $("div.paginatorDefault").css({
            "float":"left",
            "line-height":"25px",
            "margin-top":"30px",
            "text-align":"center",
            "width":"900px"
        });
        $("div.paginatorDefault .arrows").css({
            "display":"inline-block",
            "font": "bold 14px Arial",
            "height":"25px",
            "line-height":"25px",
            "padding":"0",
            "text-align":"center",
            "text-decoration":"none",
            "width":"25px"
        });
        $("div.paginatorDefault ul").css({
            "display":"inline-block",
            "margin":"0 10px"
        });
        $("div.paginatorDefault li").css({
            "display":"inline-block",
            "margin":"0 10px"
        });
        $("div.paginatorDefault .arrow-left").text("<<");
        $("div.paginatorDefault .arrow-right").text(">>");
        $("div.paginatorDefault a").css({"color":"black"});
        $("div.paginatorDefault button").css({"background":"none"});
    },
    
    //Возвращаем инпутам станд. значения
    restoreDefaults: function()
    {
        $("#questions_email").val('');
        $("#questions_password").val('');
        $("#questions_message").val('');
    }
}

//Вызов кнопкой
function sendQuestion()
{
    questionClient.sendQuestion();
}