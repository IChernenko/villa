<?php
global $package;
$package = array(
    'version' => '6',
    'name' => 'module.questions',
    'js' => array(
        '../module/questions/js/questions.js',        
        '../module/questions/js/questionClient.js'
        ),
    'dependence' => array(
        'module.division',
        'module.entity',
        'module.rating',
        'lib.jquery.validate',
    )
);