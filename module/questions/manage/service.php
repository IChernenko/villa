<?php

/**
 * Description of service
 *
 * @author Valkyria
 */
class Module_Questions_Manage_Service 
{
    public static function sendLetter(Module_Questions_Model $question, $answerText)
    {
        $view = new Module_Questions_Manage_View();
        $message = $view->drawMail($question, $answerText);
        
        $subject = "Ответ на Ваш вопрос";      
        
        $service = new Module_Mail_Service_Sender();
        $service->send($question->email, $subject, $message);
    }
}

?>
