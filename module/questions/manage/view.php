<?php

/**
 * Description of view
 *
 * @author Valkyria
 */
class Module_Questions_Manage_View
{
    public function drawMail(Module_Questions_Model $questionModel, $answerText)
    {
        $tpl = new Dante_Lib_Template();
        $filePath = '../module/questions/template/answer_mail.html';
        
        $questionModel->date = Dante_Helper_Date::converToDateType($questionModel->date, 3);
                
        $tpl->model = $questionModel;
        $tpl->answer = $answerText;
        $tpl->domain = Dante_Lib_Config::get('url.domain_main');     
        return $tpl->draw($filePath);
    }
}

?>
