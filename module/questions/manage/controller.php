<?php

/**
 * управление комментариями
 *
 * @author Mort
 */
class Module_Questions_Manage_Controller extends Lib_Jgrid_Controller_Base{
    
    /**
     * отрисовка стартового html
     */
    protected function _defaultAction()
    {
        $hotelHelper = new Module_Division_Helper();
        $hotelHelper->checkAccess();
        
        $mapper = new Module_Questions_Manage_Mapper();
        $comments = $mapper->getComments();
        
        
        //формируем массив для парентов
        $stringParams = '<div id="row_{id}" class="reviewElement parent_{parent_id}" approved="{approved}" style="height:35px;">
            {separator}
            <span class="badge" title="{message}">
                <font id="row_message" style="display:none;">{message}</font>
                <font id="row_name">{name}</font>
                &nbsp&nbsp&nbsp
                ( <font id="row_email">{email}</font> )
                &nbsp&nbsp&nbsp
                {date}
                &nbsp&nbsp&nbsp
                <i class="icon-download" onclick="questions.makeAddChild({id})" style="cursor:pointer;"></i>
                <i class="icon-pencil" onclick="questions.makeEdit({id})" style="cursor:pointer;"></i>
                <i class="icon-trash" onclick="questions.makeDelete({id})" style="cursor:pointer;"></i>
            </span>
            </div>
            <script>
                
            </script>';
        
        $libTree = new Component_Vcl_Tree();
        $drawTreeParams = array(
            'array' => $comments,
            'stringParams' => $stringParams,
            'parentKey' => 'parent_id',
            'separatorString' => '&nbsp&nbsp&nbsp&nbsp&nbsp<sup></sup>'
        );
        
        $commentsIdEdit = $libTree->drawTree($drawTreeParams, 0);
        $commentsIdEditString = implode("", $commentsIdEdit);
        
        $html = '';
        $html .= '<div style="width:100%;"><div style="margin: 0px auto; width:800px;">';
        $html .= '<center><h3> Вопрос - Ответ </h3></center><br>';
        $html .= '<center><button class="btn" type="button" onclick="questions.makeAddChild(0)">Добавить новый</button></center><br>';
        $html .= $commentsIdEditString;
        $html .= '</div></div>';
        $html .= '<script>questions.format();$(".reviewElement span").tooltip();</script>';
        
        return $html;
    }
    
    
    protected function _approveAction() {
        $id = $this->_getRequest()->get('id');
        $mapper = new Module_Questions_Manage_Mapper();
        $mapper->approve($id);
        
        return array(
            'result' => 1
        );
        
    }
    
    protected function _deleteAction() {
        $id = $this->_getRequest()->get('id');
        $mapper = new Module_Questions_Manage_Mapper();
        $mapper->del($id);
        
        return array(
            'result' => 1
        );
        
    }
    
    protected function _editAction() {
        
        $table = new Module_Questions_Model();
        $table->id = $this->_getRequest()->get('id');
        $table->date = time();
        $table->name = $this->_getRequest()->get('name');
        $table->email = $this->_getRequest()->get('email');
        $table->message = $this->_getRequest()->get('message');
        
        $mapper = new Module_Questions_Manage_Mapper();
        $mapper->update($table);
        
        return array(
            'result' => 1
        );
        
    }
    
    protected function _addAction() {
        
        $table = new Module_Questions_Model();
        $table->parent_id = $this->_getRequest()->get('id');
        $table->date = time();
        $table->name = $this->_getRequest()->get('name');
        $table->email = $this->_getRequest()->get('email');
        $table->message = $this->_getRequest()->get('message');
        
        $mapper = new Module_Questions_Manage_Mapper();
        $mapper->add($table);
        
        // Если это ответ на вопрос
        if($table->parent_id != 0)
        {
            $parentQuestion = $mapper->getById($table->parent_id);
            Module_Questions_Manage_Service::sendLetter($parentQuestion, $table->message);
        }
        
        return array(
            'result' => 1
        );
        
    }
}

?>
