<?php

/**
 * управление комментариями
 *
 * @author Mort
 */
class Module_Questions_Manage_Mapper {
    
    protected $_tableName = '[%%]questions';
    
    /**
     * выбираем комментарии
     * @return array 
     */
    public function getComments($params = array()) {
        $where = false;
        $limit = false;
        if(isset($params['where']))$where = $params['where'];
        if(isset($params['limit']))$limit = array($params['offset'], $params['limit']);
        $sql = Dante_Lib_Sql_Builder_Select::build($this->_tableName, '*', $where, false, $limit);
        //$sql = "select * from [%%]questions WHERE `email` = 'lysakalexandr42@gmail.com' limit 0 ,30";

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $comments = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $f['date'] = Dante_Helper_Date::converToDateType($f['date']);
            $comments[$f['id']] = $f;
        }
        
        return $comments;
    }
    
    public function countRows($params = array())
    {
        $where = '';
        $whereArr = array();
        
        if(isset($params['where']) && count($params['where']) > 0){
            foreach ($params['where'] as $key => $value) {
                if(is_array($value)){
                    $oper = 'IN';
                    $value = "(".  implode(',', $value).")";
                }else{
                    $oper = '=';
                    $value = "'".$value."'";
                }
                $whereArr[] = "`".$key."` ".$oper." ".$value;
            }
            $where = 'where '.  implode(',', $whereArr);
        }
        
        $sql = "SELECT COUNT(*) as 'count' FROM ".$this->_tableName." ".$where;
        $numRows = Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
        
        return $numRows['count'];
    }
    
    public function approve($id) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->id = $id;
        $table->approved = 1;
        
        $table->apply(array(
            'id' => $id
        ));
    }
    
    /**
     * добавляем комментарий
     * @param Module_Questions_Model $params 
     */
    public function add(Module_Questions_Model $params) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->date = $params->date;
        $table->name = $params->name;
        $table->email = $params->email;
        $table->message = $params->message;
        $table->parent_id = $params->parent_id;
        
        if($params->parent_id > 0){
            $table->approved = 1;
            $this->approve($params->parent_id);
        }
        
        $params->id = $table->insert();
        
        //теперь заполняем дерево
        $table->parents_tree = $params->parent_id.'.'.$params->id;
        if($table->parent_id != 0){
            $table->parents_tree = '0.'.$table->parents_tree;
        }
        $table->apply(array(
            'id' => $params->id
        ));
    }
    
    /**
     * обновление комментария
     * @param Module_Questions_Model $params 
     */
    public function update(Module_Questions_Model $params) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->id = $params->id;
        $table->date = $params->date;
        $table->name = $params->name;
        $table->email = $params->email;
        $table->message = $params->message;
        
        $table->apply(array(
            'id' => $params->id
        ));
    }
    
    /**
     * удаление комментария
     * @param int $id 
     */
    public function del($id) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->delete(array(
            'id' => $id
        ));
    }

    public function getNewQuestionsCount() 
    {
        $sql = "SELECT COUNT(*) AS `count` FROM {$this->_tableName} WHERE `approved` = 0";
        return Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'count');
    }
    
    /**
     *
     * @param type $id
     * @return \Module_Questions_Model 
     */
    public function getById($id)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->select(array('id' => $id));
        
        $model = new Module_Questions_Model();
        $model->id = $id;
        $model->date = $table->date;
        $model->name = $table->name;
        $model->email = $table->email;
        $model->message = $table->message;
        return $model;
    }
    
}

?>
