<?php

$url = Dante_Controller_Front::getRequest()->get_requested_url();

Dante_Lib_Router::addRule("/manage\/questions\//", array(
    'controller' => 'Module_Questions_Manage_Controller'
));

Dante_Lib_Router::addRule("/questions/", array(
    'controller' => 'Module_Questions_Controller'
));

Dante_Lib_Router::addRule("/questions\/page[0-9]+/", array(
    'controller' => 'Module_Questions_Controller'
));

?>