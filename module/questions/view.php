<?php
/**
 * View формы "вопрос-ответ"
 * 
 *  
 */

class Module_Questions_View
{
    public function draw($email)    
    {
        $tplFileName = '../module/questions/template/questions.html';
        $tpl = new Dante_Lib_Template();
        $tpl->email = $email;
        
        return $tpl->draw($tplFileName);
    }
    
    public function drawPrev($questions)
    {
        $stringParams = '<div id="row_{id}" 
            class="reviewElement parent_{parent_id}" approved="{approved}"">
            {separator}
                <span class="messageBlock" title="{message}">
                    <span class="row-name">{name}</span>
                    <span class="row-date">&nbsp({date})</span>
                    <br>
                    <span class="row-message-label">'.Module_Lang_Helper::translate('question').':&nbsp</span><span class="row-message">{message}</span>
                </span>
            </div>';
        
        $tree = new Component_Vcl_Tree();
        $drawTreeParams = array(
            'array' => $questions,
            'stringParams' => $stringParams,
            'parentKey' => 'parent_id',
            'separatorString' => '<span class="row-arrow">&nbsp;</span>'
        );
        
        $questionsIdEdit = $tree->drawTree($drawTreeParams, 0);
        $questionsIdEditString = implode("", $questionsIdEdit); 
        
        return $questionsIdEditString;
    }
}


?>