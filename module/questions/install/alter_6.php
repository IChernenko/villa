<?php

$label = new Module_Labels_Mapper();

$modelWrongMail = new Module_Labels_Model();
$modelWrongMail->sys_name = 'wrongEmail';
$modelWrongMail->lang_id = 1;
$modelWrongMail->name = 'Неверный E-mail!';

$label->add($modelWrongMail);

$modelNotReg = new Module_Labels_Model();
$modelNotReg->sys_name = 'emailNotRegistered';
$modelNotReg->lang_id = 1;
$modelNotReg->name = 'E-mail не зарегистрирован!';

$label->add($modelNotReg);

$modelWrongPassForm = new Module_Labels_Model();
$modelWrongPassForm->sys_name = 'wrongPasswordFormat';
$modelWrongPassForm->lang_id = 1;
$modelWrongPassForm->name = 'Неверный формат пароля!';

$label->add($modelWrongPassForm);

$modelWrongPass = new Module_Labels_Model();
$modelWrongPass->sys_name = 'wrongPassword';
$modelWrongPass->lang_id = 1;
$modelWrongPass->name = 'Неверный пароль!';

$label->add($modelWrongPass);

?>