<?php 

$label = new Module_Labels_Mapper();

$modelAddQ = new Module_Labels_Model();
$modelAddQ->sys_name = 'addQuestion';
$modelAddQ->lang_id = 1;
$modelAddQ->name = 'Задать вопрос';

$label->add($modelAddQ);

$modelEntMail = new Module_Labels_Model();
$modelEntMail->sys_name = 'enterEmail';
$modelEntMail->lang_id = 1;
$modelEntMail->name = 'Введите Ваш E-mail';

$label->add($modelEntMail);

$modelEntPass = new Module_Labels_Model();
$modelEntPass->sys_name = 'enterPassword';
$modelEntPass->lang_id = 1;
$modelEntPass->name = 'Введите Ваш пароль';

$label->add($modelEntPass);

$modelEntQ = new Module_Labels_Model();
$modelEntQ->sys_name = 'enterQuestion';
$modelEntQ->lang_id = 1;
$modelEntQ->name = 'Введите Ваш вопрос';

$label->add($modelEntQ);

?>