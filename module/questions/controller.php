<?php

/**
 * Контроллер блока вопрос-ответ
 * 
 * @author Valkyria 
 */

class Module_Questions_Controller extends Dante_Controller_Base{
    
    /**
     * Отрисовка формы
     */
    protected function _defaultAction()
    {
        $view = new Module_Questions_View;
        
        $email = '';
        if($uid = Dante_Helper_App::getUid())
        {
            $mapper = new Module_User_Mapper();
            $email = $mapper->get($uid)->email;
        }
        
        return $view->draw($email);
    }
    
    /**
     * Функция вызывается при каждой загрузке страницы и определяет,
     * нужно ли загрузить очередную страницу пейджинга
     * 
     * @return array
     */
    protected function _pagingAction()
    {
        $pageUrl = $_SERVER['HTTP_REFERER'];
        $pattern = '/page[0-9]+$/i';
        $matches = array();
        $email = '';
        $page = 1;
        $auth = false;
        $returnArray = array();
        $uid = Dante_Helper_App::getUid();
        $session = new Dante_Lib_Session;

        // Если пользователь авторизован
        if($uid)
        {
            $mapper = new Module_User_Mapper;
            $userModel = $mapper->get($uid);
            $email = $userModel->email;
            $auth = 1;

            // Администратор
            if($userModel->rating == 100)
            {
                $returnArray = array(
                    'result' => 0
                );
            }
            // Обычный пользователь
            else
            {
                if(preg_match($pattern, $pageUrl, $matches))
                    $page = str_replace('page', '', $matches[0]);

                $html = $this->_showAction($email, $page);
                $returnArray = array(
                    'result' => 1,
                    'paginator' => $html['paginator'],
                    'output' => $html['html'],
                    'page' => $page,
                    'auth' => $auth
                );
            }
        }
        // Если пользователь не авторизован (guest)
        else
        {
            if(preg_match($pattern, $pageUrl, $matches))
                $page = str_replace('page', '', $matches[0]);

            $html = $this->_showAction($email, $page);
            $returnArray = array(
                'result' => 1,
                'paginator' => $html['paginator'],
                'output' => $html['html'],
                'page' => $page,
                'auth' => $auth
            );
        }
        return $returnArray;
    }

    /**
     * Добавляем новый вопрос юзера. 
     * Назад отправляем ранее заданные вопросы
     * 
     * @return array 
     */

    protected function _addAction()
    {
        $uid = Dante_Helper_App::getUid();
        $session = new Dante_Lib_Session;

        if($uid)
        {
            $mapper = new Module_User_Mapper;
            $userModel = $mapper->get($uid);
            $email = $userModel->email;
        }
        else
        {
            $email = $this->_getRequest()->get('email');
            $sessionEmail = array(
                'email' => $email
            );
            $session->set('questions', $sessionEmail);

            $valid = $this->_validateAction($email);

            if(!$valid['result'])
            {
                return $valid;
            }
        }

        $message = $this->_getRequest()->get('message');

        if(strlen($message)>200)
        {
            $message = substr($message, 0, 200);
        }
        $userMapper = new Module_User_Mapper;
        $userName = $userMapper->getNameByEmail($email);

        $model = new Module_Questions_Model();
        $model->date = time();
        $model->name = $userName;
        $model->email = $email;
        $model->message = $message;
        $model->parent_id = 0;

        $mapper = new Module_Questions_Manage_Mapper();
        $mapper->add($model);

        $html = $this->_showAction($email);


        return array
        (
            'result' => 1,
            'paginator' => $html['paginator'],
            'output' => $html['html']
        );

        //DEBUG
        /*return array(
            'result' => 2,
            'message' => $html
        );*/
        //
    }

    
    /**
     * Валидируем данные (e-mail)
     * 
     * @param string $email
     * @return array
     */
    protected function _validateAction($email)
    {
        //$userMapper = new Module_User_Mapper;
        $validator = new Dante_Lib_Validator();

        if(!$validator->getValidate($email, 'email'))
            throw new Exception(Module_Lang_Mapper::translate('wrongEmail'));

        /*$uid = $userMapper->getUidByEmail($email);
        if(!$uid)
        {
            throw new Exception(Module_Lang_Mapper::translate('emailNotRegistered'));
        }*/

        // Если e-mail валидный
        return array(
            'result' => 1
        );

    }


    
    /**
     * Находим все заданные ранее вопросы и ответы на них 
     * Генерируем html
     * 
     * @param string $email
     * @param int $page
     * @return string 
     */
    protected function _showAction($email, $page = 1)
    {
        $mapper = new Module_Questions_Manage_Mapper();
             
        $limit = 5;
        $offset = ($page-1)*$limit;
        
        $params = array();
        if($email != '')
        {
            $params = array(
                'where' => array(
                    'email' => $email
                    ),
                'limit' => $limit,
                'offset' => $offset
                );
        }
        else
        {
            $params = array(
                'where' => array(
                    'parent_id' => 0
                    ),
                'limit' => $limit,
                'offset' => $offset
                );
        }
        
        $questions = $mapper->getComments($params);
        
        foreach($questions as &$value)
        {
            if(strlen($value['name']) == 0) $value['name'] = $value['email'];
        }
        unset($value); 
        
        $questionKeys = array_keys($questions);
        $params = array(
            'where' => array(
                'parent_id' => $questionKeys
            )
        );
        
        $answers = array();
        $answers = $mapper->getComments($params);
        if($answers)
        {
            $questions = array_merge($questions, $answers);
        }
        
        
        
        $view = new Module_Questions_View();       
        
        $html = $view->drawPrev($questions);
        
        if($email != '')
        {
            $params = array(
                'where' => array(
                    'email' => $email
                    )
                );
        }
        else
        {
            $params = array(
                'where' => array(
                    'parent_id' => 0
                    )
                );
        }
        $count = $mapper->countRows($params);
        if($count>50) $count = 50;
        
        $paginator = new Lib_Paginator_Driver();
        $paginator->link = '/questions';
        $paginatorHtml = $paginator->draw($count, $page, $limit);
        
        return array(
            'paginator' => $paginatorHtml,
            'html' => $html
        );
    }
    
    
}

?>