<?php

class Module_Slider_Manage_Service
{
    public function getSettings()
    {
        $dir = Module_Slider_Helper::getSettingsFolder();
        if(!is_dir($dir)) { //echo($dir); die();
            if (!@mkdir($dir, 0755)) throw new Exception("Не могу создать директорию $dir");
        }
        
        $filename = $dir.'slider.dat';
        
        if(!is_file($filename)) return false;
        
        $file = fopen($filename, 'r');            
        $settings = fread($file, filesize($filename));
        fclose($file);
        
        if(!$settings) return false;
        
        return json_decode($settings);
    }
    
    public function saveSettings($dataArr)
    {
        $data = json_encode($dataArr);
        
        $dir = Module_Slider_Helper::getSettingsFolder();
        if(!is_dir($dir)) mkdir($dir, 0755);
        
        $filename = $dir.'slider.dat';
        $file = fopen($filename, 'w');
        fwrite($file, $data);
        fclose($file);
    }
}

?>
