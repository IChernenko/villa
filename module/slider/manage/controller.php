<?php

class Module_Slider_Manage_Controller extends Module_Handbooksgenerator_Controller_Base
{
    protected function _drawAction()
    {
        $hotelHelper = new Module_Division_Helper();
        $hotelHelper->checkAccess();
        
        $mapper = new Module_Slider_Manage_Mapper();
        $imgRows = $mapper->getImages();
        
        $model = new Module_Slider_Model();
        $params = $this->getParams($model);
        
        $this->formEnable('module.slider.manage.controller');
        
        $this->caption = 'Управление слайдером';
        $this->_setHeader();
        $this->_setBody($imgRows['rows']);
        
        $footerButtons = array();
        if(Dante_Lib_Config::get('slider.allowSettingsEdit'))
            $footerButtons[] = '<a class="btn btn-info" onclick="slider_manage.openSettings();">Настройки...</a>';
        
        $footerButtons['button_filter'] = array(            
                            "type"=>array(
                                'edit'
                            ),                    
                        );
        
        $this->tfoot=array(
            array($this->paginator($imgRows['count'], $params)),
            $footerButtons
        );
        
        $html=$this->generate();        
        return $html;
        
    }
    
    protected function _setHeader()
    { 
        $title = array(
            array(
                'content' => "#",
                'params' => array(
                    'style' => array(
                        'width'=>'15px'
                    )
                )
            ),
            'Изображение',
            array(
                'content' =>'Имя файла',
                'sortable' =>'img_name',
                'sortable_def' =>'desc',
                'params' => array(
                    'style' => array(
                        'width' => '200px'
                    )
                )
            ),
            array(
                'content' =>'Описание',
                'params' => array(
                    'style' => array(
                        'width' => '350px'
                    )
                )
            ),
            array(
                'content' =>'URL',
                'params' => array(
                    'style' => array(
                        'width' => '300px'
                    )
                )
            ),
            array(
                'content' =>'Актив.'
            ),
            array(
                'content' =>'Функции',
                'params' => array(
                    'style' => array(
                        'width' => '120px'
                    )
                )                
            ),
        );
        
        if(!Dante_Lib_Config::get('slider.allowDescr'))
            unset($title[3]);
        
        if(!Dante_Lib_Config::get('slider.allowUrl'))
            unset($title[4]);
        
        $this->thead = array(
            $title
        );
    }
    
    protected function _setBody($imgRows)
    {          
        foreach($imgRows as $row){  
            $vars = array();
            $vars['id'] = $row['id'];
            $vars['img'] = 
                    '<img src="'.Module_Slider_Helper::getFolder().$row['img_name'].'" style="width: 400px;"/>';
            $vars['img_name'] = $row['img_name'];
            
            if(Dante_Lib_Config::get('slider.allowDescr'))
                $vars['title'] = $row['title'];

            if(Dante_Lib_Config::get('slider.allowUrl'))
                $vars['url'] = $row['url'];
            
            $vars['is_active'] = $row['is_active']?
                '<a class="btn btn-warning btn-mini" onclick="slider_manage.toggleActive('.$row['id'].', 0);">Деактивировать</a>':
                '<a class="btn btn-info btn-mini" onclick="slider_manage.toggleActive('.$row['id'].', 1);">Активировать</a>';
            
            $vars['buttons'] = array(
                'id'=>$row['id'],
                'type'=>array('edit', 'del'),                
            );
            $this->tbody[$row['id']] = $vars;
        }
    }
    
    protected function _imageUploadAction()
    {
        if(!isset($_FILES['file']))
            return array(
                'result' => 0,
                'message' => 'Нет файла для загрузки'
            );
                    
        $uploadDir = Module_Slider_Helper::getFolder();
        if (!is_dir($uploadDir)) mkdir($uploadDir, 0755, true);             

        $newName = substr(md5(time()), 0, 25);  

        $uploader = new Lib_JqueryUpload_Uploader();
        $uploader->globalKey = 'file';
        
        $image = $uploader->upload($uploadDir, $newName);  
        
        return array(
            'result' => 1,
            'image' => $image,
            'imageDir' => $uploadDir
        );
    } 
    
    
    protected function _editAction()
    {
        $mapper = new Module_Slider_Manage_Mapper();
        $model = new Module_Slider_Model();
        
        $imgNameUrl = $this->_getRequest()->get_validate('img_name', 'text');
        $imgName = trim((str_ireplace(Module_Slider_Helper::getFolder(), '', $imgNameUrl)), ' /');
        
        $isActive = $this->_getRequest()->get('is_active');
        if($isActive) $isActive = 1;
        
        $model->id = $this->_getRequest()->get_validate('id', 'int');
        $model->img_name = $imgName;
        $model->is_active = $isActive;
        $model->url = $this->_getRequest()->get('url');
        $model->title = $this->_getRequest()->get_validate('title', 'text');
        $model->title2 = $this->_getRequest()->get_validate('title2', 'text');
        $model->title3 = $this->_getRequest()->get_validate('title3', 'text');
        $model->lang_id = $this->_getRequest()->get_validate('lang_id', 'int');

        $edit = $mapper->imgEdit($model);
        if($this->_getRequest()->get('returntype') == 1){
            return $this->_drawFormAction($edit);
        }
        return array(
            'result' => 1,
            'id' => $edit
        );
    }


    protected function _drawFormAction($id = false)
    {        
        if(!$id) $id = $this->_getRequest()->get_validate('id', 'int');
        
        $mapper = new Module_Slider_Manage_Mapper();
        $img = $mapper->getImageById($id);
        
        if($img['is_active']) $img['is_active'] = 'checked';
                
        $img['src'] = Module_Slider_Helper::getFolder().$img['img_name'];
        
        $view = new Module_Slider_Manage_View();
        $html = $view->drawForm($img);
        return array(
            'result' => 1,
            'html' => $html
        );
    }    
    
    
    protected function _toggleActiveAction()
    {
        $id = $this->_getRequest()->get_validate('id', 'int');
        $active = $this->_getRequest()->get_validate('active', 'int');
        
        $mapper = new Module_Slider_Manage_Mapper();
        $mapper->toggleActive($id, $active);
        
        return array(
            'result' => 1
        );
    }
    
    protected function _delAction()
    {
        $mapper = new Module_Slider_Manage_Mapper();
        $id = $this->_getRequest()->get_validate('id', 'int');
        $mapper->del($id);
        
        return array(
           'result'=>1 
        );
    }
    
    protected function _drawSettingsAction()
    {
        if(!Dante_Lib_Config::get('slider.allowSettingsEdit'))
            return array(
                'result' => 0
            );
        
        $service = new Module_Slider_Manage_Service();
        $settings = $service->getSettings();
        
        $view = new Module_Slider_Manage_View();
        
        return array(
            'result' => 1,
            'html' => $view->drawSettings($settings)
        );
    }
    
    protected function _editSettingsAction()
    {
        $settings = $this->_getRequest()->get('settings');
        $dataArr = array();
        
        foreach($settings as $val)
        {
            if(!$val['value']) continue;
            $dataArr[$val['name']] = $val['value'];
        }
        
        $service = new Module_Slider_Manage_Service();
        $service->saveSettings($dataArr);
        
        return array(
            'result' => 1
        );
        
    }

    protected function _getLangDataAction() {
        $settings = $this->_getRequest()->get('settings');
        $dataArr = array();

        foreach($settings as $val) {
            if(!$val['value']) continue;
            $dataArr[$val['name']] = $val['value'];
        }

        $mapper = new Module_Slider_Manage_Mapper();
        $data = $mapper->getImageByLangId($dataArr['id'], $dataArr['lang_id']);

        return $data;
    }
    
}

?>