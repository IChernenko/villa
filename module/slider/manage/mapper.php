<?php

class Module_Slider_Manage_Mapper extends Module_Handbooksgenerator_Mapper_Base
{
    protected $_tableName = '[%%]slider';
    protected $_tableName_lang = '[%%]slider_lang';

    public function getImages()
    {
        return $this->getRowsByParams();
    }
    
    public function getImageById($id)
    {
        if(!$id) return false;
        $sql = 'SELECT * FROM `'.$this->_tableName.'` WHERE `id` = '.$id;
        $img = Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
        return $img;
    }

    public function getImageByLangId($id, $langId)
    {
//        if(!$id) return false;
        $sql = 'SELECT * FROM `'.$this->_tableName_lang.'` WHERE `slider_id` = '.$id .' AND `lang_id` = '. $langId;
        $img = Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
        return $img;
    }
    
    public function imgEdit($model)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table2 = new Dante_Lib_Orm_Table($this->_tableName_lang);

        $table->img_name = $model->img_name;
        $table->url = $model->url;
        $table->is_active = $model->is_active;

        if(!$model->id) {
            $id = $table->insert();
        }
        else {
            $table->update(array('id'=>$model->id));
            $id = $model->id;
        }

        $table2->lang_id = $model->lang_id;
        $table2->slider_id = $id;
        $table2->title = '';
        $table2->title2 = '';
        $table2->title3 = '';
        if ( $model->title ) {
            $table2->title = $model->title;
        }
        if ( $model->title2 ) {
            $table2->title2 = $model->title2;
        }
        if ( $model->title3 ) {
            $table2->title3 = $model->title3;
        }

        $table2->apply(array(
            'lang_id' => $model->lang_id,
            'slider_id' => $id
        ));
        
        return $id;
    }
    
    public function getAll()
    {
        $sql = "SELECT * FROM {$this->_tableName} WHERE `is_active` = 1";
        if(!$r = Dante_Lib_SQL_DB::get_instance()->open($sql)) return false;
        $list = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r))
        {
            $model = new Module_Slider_Model();
            $model->img_name = $f['img_name'];
            $model->id = $f['id'];

            $model->url = $f['url'];
            $list[$f['id']] = $model;
        }
        
        return $list;
    }
    
    public function toggleActive($id, $active)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->is_active = $active;
        $table->update(array('id' => $id));
    }
}

?>