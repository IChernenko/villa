<?php

class Module_Slider_Manage_View
{
    public function drawForm($vars = false)
    {
        $tplName = '../module/slider/manage/template/imgform.html';
        $tpl = new Dante_Lib_Template();
        $tpl->_vars = $vars;
        
        return $tpl->draw($tplName);
    }
    
    public function drawSettings($settings = false)
    {
        $tplName = '../module/slider/manage/template/settings.html';
        
        $customTpl = Dante_Helper_App::getWsPath().'/slider/settings.html';
        
        if (file_exists($customTpl)) {
            $tplName = $customTpl;
        }
        
        $tpl = new Dante_Lib_Template();
        $tpl->settings = $settings;
        
        return $tpl->draw($tplName);
    }
}

?>