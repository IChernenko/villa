<?php

class Module_Slider_View
{
    public function draw($list, $settings, $langData)
    {
        $tplFileName = '../module/slider/template/slider.html';

        $customTpl = Dante_Helper_App::getWsPath().'/slider/slider.html';
        
        if (file_exists($customTpl)) {
            $tplFileName = $customTpl;
        }

        $tpl = new Dante_Lib_Template();
        $tpl->list = $list;
        $tpl->langData = $langData;
        $tpl->settings = $settings;
        return $tpl->draw($tplFileName);
    }
}

?>
