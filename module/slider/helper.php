<?php

class Module_Slider_Helper
{
    public static function getFolder()
    {
        $app = Dante_Lib_Config::get('app.uploadFolder');
        $dir = Dante_Lib_Config::get('slider.dir');
        return "{$app}/{$dir}/";
    }
    
    public static function getSettingsFolder()
    {
        $app = Dante_Lib_Config::get('app.uploadFolder');
        $dir = Dante_Lib_Config::get('slider.settingsDir');
        return "{$app}/{$dir}/";
    }
}

?>
