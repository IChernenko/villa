<?php

$url = Dante_Controller_Front::getRequest()->get_requested_url();

Dante_Lib_Router::addRule("/manage\/slider\//", array(
    'controller' => 'Module_Slider_Manage_Controller'
));

Dante_Lib_Router::addRule("/manage\/slidersettings\//", array(
    'controller' => 'Module_Slider_Manage_Controller',
    'action' => 'drawSettings'
));

?>