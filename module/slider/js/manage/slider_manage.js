slider_manage = {
    controller: "module.slider.manage.controller",
    
    validate: function()
    {
        if(!$('#img_name').val())
        {
            alert('Ошибка! Выберите изображение!');
            return false;
        }
        return true;
    },    
    edit: function(obj, close)
    { 
        if(!this.validate()) return false;
        
        var data = $('#editSlide').serializeArray();
        data.push(
            {name: 'controller', value: this.controller},
            {name: 'action', value: 'edit'}
        );            
        
        $.ajax({
            type: 'POST',
            data: data,
            url: '/ajax.php',
            dataType : "json",
            success: function (data){
                if(data.result==1)
                {
                    $('#modalWindow').arcticmodal('close');                  
                }
                else alert(data.message);
            }
        });
        
    },    
    toggleActive: function(id, active)
    {
        var data = {
            controller: this.controller,
            action: 'toggleActive',
            id: id,
            active: active
        }
        $.ajax({
            url: "ajax.php",
            type: 'post',
            dataType: "json",
            data: data,
            success:function(data)
            {
                if(data.result == 1)
                {
                    handbooks.formSubmit();
                }
                else console.log(data);
            }
        });
    },
    openSettings: function()
    {
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {
                controller: 'module.slider.manage.controller',
                action: 'drawSettings'
            },
            success: function(data)
            {
                if(data.result == 1)
                {
                    $('#modalWindow .modal-body').html(data.html);
                    $('#modalWindow').arcticmodal();
                }
                else console.log(data);
            }
        });
    },
    editSettings: function()
    {
        var settings = $('#editSettings').serializeArray();
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {
                controller: 'module.slider.manage.controller',
                action: 'editSettings',
                settings: settings
            },
            success: function(data)
            {
                if(data.result == 1)
                {
                    jAlert('Изменения сохранены', 'Сообщения', function(){
                        $('#modalWindow').arcticmodal('close');
                    });                    
                }
                else console.log(data);
            }
        });
    },
    getLangData : function() {
        var settings = $('#editSlide').serializeArray();
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            data: {
                controller: 'module.slider.manage.controller',
                action: 'getLangData',
                settings: settings
            },
            success: function(data) {

                if ( data == false ) {
                   $('#title').val('');
                   $('#title2').val('');
                   $('#title3').val('');
                } else {
                    $('#title').val(data['title']);
                    $('#title2').val(data['title2']);
                    $('#title3').val(data['title3']);
                }
            }
        });
    }
};