<?php

Dante_Lib_Config::set('slider.dir', 'slider');

Dante_Lib_Config::set('slider.allowSettingsEdit', true);
Dante_Lib_Config::set('slider.allowDescr', true);
Dante_Lib_Config::set('slider.allowUrl', true);
Dante_Lib_Config::set('slider.settingsDir', 'settings');

?>