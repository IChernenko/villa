<?php

class Module_Slider_Controller extends Dante_Controller_Base
{
    protected function _defaultAction() 
    {
        $currentLang = Module_Lang_Helper::getCurrent();
        $langData = array();
        $mapper = new Module_Slider_Manage_Mapper();
        $view = new Module_Slider_View();
        
        $list = $mapper->getAll();
        if(!$list) return '';        
        
        if(Dante_Lib_Config::get('slider.allowSettingsEdit'))
        {
            $service = new Module_Slider_Manage_Service();
            $settings = $service->getSettings();
        }
        else
            $settings = false;

        foreach ( $list as $image_slider ) {
            $langArray = $mapper->getImageByLangId($image_slider->id, $currentLang);
            $langData[$image_slider->id] = $langArray;
        }
        
        return $view->draw($list, $settings, $langData);
    }
    
    protected function _drawAjaxAction()
    {
        return array(
            'result' => 1,
            'html' => $this->_defaultAction()
        );
    }
}

?>
