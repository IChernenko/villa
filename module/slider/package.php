<?php
global $package;
$package = array(
    'version' => '3',
    'name' => 'module.slider',
    'js' => array(
        '../module/slider/js/manage/slider_manage.js'
        ),
    'dependence' => array(
        'module.division',
        'module.handbooksgenerator',
        'lib.jqueryUpload',
    )
);