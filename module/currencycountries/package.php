<?php
global $package;
$package = array(
    'version' => '5',
    'name' => 'module.currencycountries',
    'dependence' => array(
        'lib.jgrid',
        'module.auth',
        'module.lang',
        'component.jalerts'
    ),
    'js' => array(
        '../module/currencycountries/js/currencycountries.js'
    )

);