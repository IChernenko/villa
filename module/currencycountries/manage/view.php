<?php
/**
 * Description of view
 *
 * @author Valkyria
 */
class Module_Currencycountries_Manage_View 
{
    public $langList;
    
    public $currencyList;
    
    public function drawForm($model)
    {
        $tpl = new Dante_Lib_Template();
        $filePath = '../module/currencycountries/manage/template/form.html';
        
        $tpl->langList = $this->langList;
        $tpl->cсyList = $this->currencyList;
        $tpl->model = $model;
        return $tpl->draw($filePath);
    }
}

?>
