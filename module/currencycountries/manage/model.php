<?php

/**
 * Description of callback
 *
 * @author Mort
 */
class Module_Currencycountries_Manage_Model 
{    
    public $sortable_rules = array(
        'name' => 'id',
        'type' => 'asc'
    );
    
    public $filter_rules = array(
        array(
            'name'=>'title',
            'format'=>'`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ),
        array(
            'name'=>'country_code',
            'format'=>'`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
        ),
        array(
            'name'=>'lang_id',
            'format'=>'`t1`.`%1$s` = "%2$s"'
        ),
        array(
            'name'=>'currency',
            'format'=>'`t1`.`%1$s` = "%2$s"'
        ),
    );
    
    public $id;

    public $title;

    public $country_code;
    
    public $lang_id;
    
    public $currency;
    
    public $disp_name;
    
    public $set_lang;
}

?>
