<?php
/**
 * Description of controller
 *
 * @author Valkyria
 */

class Module_Currencycountries_Manage_Controller extends Module_Handbooksgenerator_Controller_Base 
{   
    protected $_alias = 'module.currencycountries.manage.controller';


    protected function _init() 
    {
        parent::_init();
        $this->_mapper = new Module_Currencycountries_Manage_Mapper();
    }
    
    protected function _instanceView()
    {
        return new Module_Currencycountries_Manage_View();
    }
    
    protected function _instanceModel()
    {
        return new Module_Currencycountries_Manage_Model();
    }

    /**
     * отрисовка стартового html
     * @return string 
     */
    protected function _drawAction()
    {
        $model = $this->_instanceModel();
        $ccyMapper = new Module_Currency_Mapper();
        $langMapper = new Module_Lang_Mapper();
        
        $ccys = $ccyMapper->getList();
        $langs = $langMapper->getList();
        
        $ccyList = array();
        $lngList = array();
        foreach($ccys as $id => $currency)
        {
            $ccyList[$id] = $currency['title'];
        }
        foreach($langs as $id => $lang)
        {
            $lngList[$id] = $lang->name;
        }
        
        $params = $this->getParams($model);
        $list = $this->_mapper->getRowsByParams($params);
        
        $this->formEnable($this->_alias);
        
        $titles = $this->_getTitles();        
        $filters = $this->_genFilters($lngList, $ccyList);
        
        $this->thead = array($titles, $filters);
        
        $this->tbody = $this->_genRows($list['rows'], $lngList, $ccyList);
        
        $this->tfoot = array(
            array(
                $this->paginator($list['count'], $params)
            ),
            array(
                'button_filter' => array(
                    'type' => array('edit'),                    
                )
             )  
        );
        
        return $this->generate();
    }
    
    protected function _getTitles()
    {
        $titles = array(            
            array(
                'content' => 'ID',
                'sortable' => 'id'
            ),
            array(
                'content' => 'Название',
                'sortable' => 'title'
            ),
            array(
                'content' => 'Код страны',
                'sortable' => 'country_code'
            ),
            array(
                'content' => 'Язык',
                'sortable' => 'lang_id'
            ),
            array(
                'content' => 'Валюта',
                'sortable' => 'currency'
            ),
            'Функции'
        );
        
        return $titles;
    }
    
    protected function _genFilters($lngList, $ccyList)
    {
        $filters = array(
            NULL,
            'title' => array(
                'type' => 'text',
                'value' => $this->_getRequest()->get('title')
            ),
            'country_code' => array(
                'type' => 'text',
                'value' => $this->_getRequest()->get('country_code')
            ),
            'lang_id' => array(
                'type' => 'combobox',
                'items' => $lngList,
                'value' => $this->_getRequest()->get('lang_id')
            ),
            'currency' => array(
                'type' => 'combobox',
                'items' => $ccyList,
                'value' => $this->_getRequest()->get('currency')
            ),
            'button_filter'=>array(
                'type'=>'button_filter'
            )
        );
        
        return $filters;
    }
    
    protected function _genRows($rows, $lngList, $ccyList)
    {
        $table = array();
        foreach($rows as $row)
        {
            $cur = array();
            $cur['id'] = $row['id'];
            $cur['title'] = $row['title'];
            $cur['country_code'] = $row['country_code'];
            $cur['lang'] = $lngList[$row['lang_id']];
            $cur['currency'] = $ccyList[$row['currency']];     
            $cur['buttons'] = array(
                'id' => $row['id'],
                'type' => array('edit', 'del'),                
            );  
            $table[$cur['id']] = $cur;
        }
        
        return $table;
    }


    protected function _drawFormAction()
    {
        $id = $this->_getRequest()->get_validate('id', 'int', 0);   
        $lang = $this->_getRequest()->get_validate('set_lang', 'int', 1);
        
        $model = $this->_mapper->getById($id, $lang);
        
        $view = $this->_instanceView();
        $ccyMapper = new Module_Currency_Mapper();
        $langMapper = new Module_Lang_Mapper();
        
        $view->currencyList = $ccyMapper->getList();
        $view->langList = $langMapper->getList();
        
        return array(
            'result' => 1,
            'html' => $view->drawForm($model)
        );
    }

    protected function _editAction() 
    {
        $model = $this->_populateModel();
        $id = $this->_mapper->apply($model);
        
        return array(
            'result' => 1,
            'id' => $id
        );
    }
    
    protected function _populateModel()
    {
        $model = $this->_instanceModel();
        $model->id = $this->_getRequest()->get('id');
        $model->title = $this->_getRequest()->get('title');
        $model->country_code = $this->_getRequest()->get('country_code');
        $model->lang_id = $this->_getRequest()->get('lang_id');
        $model->currency = $this->_getRequest()->get('currency');
        $model->set_lang = $this->_getRequest()->get('set_lang');
        $model->disp_name = $this->_getRequest()->get('disp_name');
        $model->set_lang = $this->_getRequest()->get('set_lang');
        
        return $model;
    }
}

?>
