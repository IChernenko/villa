<?php

/**
 * Description of mapper
 *
 * @author Valkyria
 */
class Module_Currencycountries_Manage_Mapper extends Module_Handbooksgenerator_Mapper_Base
{    
    protected $_tableName = '[%%]currencycountries';
    protected $_tableNameLang = '[%%]currencycountries_lang';
    
    protected function _instanceModel()
    {
        return new Module_Currencycountries_Manage_Model();
    }

    public function getById($id, $lang = 1)
    {
        if(!$id) return $this->_instanceModel();
        
        $sql = "
          SELECT * FROM ".$this->_tableName." AS a, ".$this->_tableNameLang." AS b
          WHERE
              a.id =".$id." AND
              a.id=b.country_id AND
              b.set_lang = ".$lang."";

        $f = Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
        $f['set_lang'] = $lang;        
        
        return $this->_arrayToModel($f);
    }
    
    protected function _arrayToModel($f)
    {
        $model = $this->_instanceModel();
        
        $model->id = $f['id'];
        $model->title = $f['title'];
        $model->country_code = $f['country_code'];
        $model->lang_id = $f['lang_id'];
        $model->currency = $f['currency'];
        $model->disp_name = $f['disp_name'];
        $model->set_lang = $f['set_lang'];
        
        return $model;
    }

    public function apply($model)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->title = $model->title;
        $table->country_code = $model->country_code;
        $table->lang_id = $model->lang_id;
        $table->currency = $model->currency;
        
        if($model->id) $table->update(array('id' => $model->id));
        else $model->id = $table->insert();
        
        $tableLang = new Dante_Lib_Orm_Table($this->_tableNameLang);
        $tableLang->country_id = $model->id;
        $tableLang->disp_name = $model->disp_name;
        $tableLang->set_lang = $model->set_lang;
        
        $tableLang->apply(array(
            'country_id' => $model->id,
            'set_lang' => $model->set_lang
        ));
        
        return $model->id;
    }
    
    public function getNames()
    {
        $this->paginator = false;
        
        $params = array(
            'sort' => array(
                'name' => '`t1`.`title`',
                'type' => 'ASC'
            )
        );
        $rows = $this->getRowsByParams($params);
        $list = array();
        foreach($rows['rows'] as $id => $country)
        {
            $list[$id] = $country['title'];
        }
        
        $this->paginator = true;
        return $list;
    }

    public function getLangIdByCode($countryCode) {
        $sql = "SELECT lang_id FROM ".$this->_imagesTableName." where country_code='$countryCode'";
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r);
        return $f['lang_id'];
    }
}

?>
