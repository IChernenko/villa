<?php

/**
 * Description of mapper
 *
 * @author Valkyria
 */
class Module_Currencycountries_Mapper 
{    
    protected $_tableName = '[%%]currencycountries';
    protected $_tableNameLang = '[%%]currencycountries_lang';
    
    public function getIdByCode($code)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->select(array('country_code' => $code));
        if($table->getNumRows() == 0) return false;
        else return $table->id;
    }
    
    public function getList($lang = false)
    {
        if(!$lang) $lang = Module_Lang_Helper::getCurrent();
        
        $sql = "SELECT * FROM {$this->_tableName} AS `c`
                LEFT JOIN {$this->_tableNameLang} AS `cl`
                ON(`c`.`id` = `cl`.`country_id` AND `cl`.`set_lang` = {$lang})
                ORDER BY `cl`.`disp_name` ASC";

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $list = array();

        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) 
        {
            $list[$f['id']] = $f['disp_name'];
        }
        return $list;
    }
}

?>
