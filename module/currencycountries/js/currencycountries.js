currencycountries = 
{
    editSubmit: function(obj)
    {
        if(!this.validate()) return;
        
        if($(obj).hasClass('disabled')) return;
        $('.func').addClass('disabled');
        
        var data = $('#editCountry').serializeArray();
        $.ajax({
            type: 'POST',
            data: data,
            url: '/ajax.php',
            dataType : "json",
            success: function (data)
            {                                  
                $('.func').removeClass('disabled');
                if(data.result==1)
                {
                    $('#modelID').val(data.id);
                    jAlert('Изменения сохранены', 'Сообщение');
                }
                else jAlert(data.message, 'Ошибка');
            }
        });
    },
    
    validate: function()
    {
        var inputs = $('#editCountry').find('input, select').filter('.required');
        for(var i=0; i < inputs.length; i++)
        {
            if(!inputs.eq(i).val())
            {
                jAlert('Заполните все поля', 'Ошибка', function(){
                    inputs.eq(i).focus();
                });
                return false;
            }
        }
        
        return true;
    },

    changeLang: function(obj)
    {
        if($(obj).hasClass('disabled')) return;
        $('.func').addClass('disabled');
        
        var data = {
            controller: 'module.currencycountries.manage.controller',
            action: 'drawForm',
            id: $('#modelID').val(),
            set_lang: $(obj).val()
        }
        
        $.ajax({
            url: 'ajax.php',
            type: 'post',
            dataType: 'json',
            data: data,
            success:function(data)
            {
                if(data.result == 1)
                {
                    console.log(data);
                    $('.modal-body').html(data.html);
                }
                else console.log(data);
            }
        });
    }
}