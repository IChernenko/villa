ALTER TABLE [%%]currencycountries ADD `lang_id` INT( 11 ) UNSIGNED NOT NULL AFTER `title`;

UPDATE [%%]currencycountries SET `lang_id` = '1';

ALTER TABLE [%%]currencycountries ADD
      CONSTRAINT fk_currencycountries_lang_id
      FOREIGN KEY (`lang_id`)
      REFERENCES [%%]languages(id) ON DELETE CASCADE ON UPDATE CASCADE;