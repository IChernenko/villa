CREATE TABLE IF NOT EXISTS [%%]currencycountries (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `currency` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE [%%]currencycountries ADD
      CONSTRAINT fk_currencycountries_currency
      FOREIGN KEY (`currency`)
      REFERENCES [%%]currency(id) ON DELETE CASCADE ON UPDATE CASCADE;