CREATE TABLE IF NOT EXISTS [%%]currencycountries_lang (
  `country_id` INT(11) NOT NULL,
  `set_lang` INT(11) UNSIGNED NOT NULL,
  `disp_name` VARCHAR(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `[%%]currencycountries_lang` ADD CONSTRAINT `fk_currencycountries_lang_country_id`
    FOREIGN KEY (`country_id`) REFERENCES `[%%]currencycountries`(`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `[%%]currencycountries_lang` ADD CONSTRAINT `fk_currencycountries_lang_lang_id`
    FOREIGN KEY (`set_lang`) REFERENCES `[%%]languages`(`id`) 
    ON DELETE CASCADE ON UPDATE CASCADE;