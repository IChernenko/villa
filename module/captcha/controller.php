<?php

/**
 * Description of controller
 *
 * @author Valkyria
 */
class Module_Captcha_Controller extends Dante_Controller_Base
{
    protected function _defaultAction() 
    {
        $service = new Module_Captcha_Service();
        
        return array(
            'result' => 1,
            'captcha' => $service->showCaptchaImage()
        );
    }
}

?>
