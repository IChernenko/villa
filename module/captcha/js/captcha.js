captcha = {
    update: function()
    {
        $.ajax({
            type: 'POST',
            url: 'ajax.php',
            dataType: 'JSON',
            data: {
                controller: 'module.captcha.controller'
            },
            success: function(data)
            {
                $('#captchaMain').replaceWith(data.captcha)
            }
        });
    }
}

