<?php

/**
 * Description of view
 *
 * @author Valkyria
 */
class Module_Captcha_View 
{
    public function draw($imageSrc)
    {
        $html = '<div id="captchaMain">
            <img src="data:image/png;base64,'.$imageSrc.'"/>
            <input type="image" title="'.Module_Lang_Helper::translate('sys_refresh').'" src="../module/captcha/img/button.png"
                onclick="captcha.update()"/>
                </div>';
        
        return $html;
    }
}

?>
