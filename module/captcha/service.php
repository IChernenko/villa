<?php
/**
 * Сервис капчи
 * User: dam0rin
 * Date: 21.03.14
 * Time: 17:35 
 */
class Module_Captcha_Service 
{
    /**
    * Поля класса
    * 
    * @var $captchaImage (resource)- переменная хранящая GD-поток изображения.
    * @var $symbols (String) Строка, содержащая допустимые символы
    * для генерации изображения.
    */
    private $captchaImage = null;
    private $symbols = 'abcdefghijklmnopqrstuvwxyz';

    /**
    * Конструктор класса
    */
    function __construct() {
    }
    /**
    * Функция для генерации и отображения картинки в формате PNG
    */
    public function showCaptchaImage()
    {
        $this->captchaImage = imagecreatetruecolor(175, 50);

        $backgroundColor = imagecolorallocate($this->captchaImage, 255, 255, 255);
        $foregroundColor = imagecolorallocate($this->captchaImage, 32, 64, 160);

        imagefill($this->captchaImage, 0, 0, $backgroundColor);

        $fontFile = "./../module/captcha/fonts/font.ttf";
        $capcha = '';
        $x = 10;
        for ($j = 0; $j < 5; $j++) {
            $smb = $this->symbols[rand(0, 25)];
            $capcha .= $smb;

            $angle = rand(-5, 5);
            imagettftext(
                $this->captchaImage, 25, $angle, $x, 32, $foregroundColor, $fontFile, $smb);
        $x = $x + 30;
        }
        $_SESSION['captcha'] = $capcha;

        $dir = 'tmp/';
        if(!is_dir($dir)) mkdir($dir, 0755);
        
        $captchaFileName = $dir.'cap_'.time().'.png';
        imagepng($this->captchaImage, $captchaFileName);

        $image = file_get_contents($captchaFileName);
        unlink($captchaFileName);

        $imgSrc = base64_encode($image);
        $view = new Module_Captcha_View();
        
        return $view->draw($imgSrc);
    }
    /**
        * Функция для сравнения символов с картинки с введенными пользователем
        * @param  $cp
        * @return boolean
        */
    public function validateCaptcha($cp) 
    {	
        if ($cp == $_SESSION['captcha']) {
                return true;
        }
        //throw new Exception($_SESSION['captcha']);
        return false;
    }    
}

?>

