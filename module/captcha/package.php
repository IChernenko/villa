<?php
global $package;
$package = array(
    'version' => '1',
    'name' => 'module.captcha',
    'js' => array(
        '../module/captcha/js/captcha.js'
    ),
    'css' => array(
        '../module/captcha/css/captcha.css'
    )
);