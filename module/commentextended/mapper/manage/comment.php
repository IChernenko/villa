<?php

/**
 * управление комментариями
 *
 * @author Mort
 */
class Module_Commentextended_Mapper_Manage_Comment {
    
    protected $_tableName = '[%%]commentextended';
    
    /**
     * выбираем комментарии
     * @return array 
     */
    public function getComments() {
        $comments = array();
        
        $sql = "select
                    *
                from ".$this->_tableName." ORDER BY id DESC";

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $f['date'] = date("H:i:s d-m-Y", $f['date']); 
            $comments[$f['id']] = $f;
        }
        
        return $comments;
    }
    
    public function approve($id) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->id = $id;
        $table->approved = 1;
        
        $table->apply(array(
            'id' => $id
        ));
    }
    
    /**
     * добавляем комментарий
     * @param Module_Commentextended_Model_Comment $params 
     */
    public function add(Module_Commentextended_Model_Comment $params) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->date = $params->date;
        $table->name = $params->name;
        $table->email = $params->email;
        $table->message = $params->message;
        $table->parent_id = $params->parent_id;

        $params->id = $table->insert();
    }
    
    /**
     * обновление комментария
     * @param Module_Commentextended_Model_Comment $params 
     */
    public function update(Module_Commentextended_Model_Comment $params) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->id = $params->id;
        $table->date = $params->date;
        $table->name = $params->name;
        $table->email = $params->email;
        $table->message = $params->message;
        
        $table->apply(array(
            'id' => $params->id
        ));
    }
    
    /**
     * удаление комментария
     * @param int $id 
     */
    public function del($id) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->delete(array(
            'id' => $id
        ));
    }
    
    public function getNotApprovedCount() {
        $sql = "select id from ".$this->_tableName." WHERE approved = 0";

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $phones = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $phones[] = (int)$f['id'];
        }

        return count($phones);
    }
    
}

?>
