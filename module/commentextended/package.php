<?php
global $package;
$package = array(
    'version' => '3',
    'name' => 'module.commentextended',
    'js' => array('../module/commentextended/js/commentextended.js'),
    'dependence' => array(
        'module.division',
        'module.entity',
        'module.rating',
        'lib.jquery.validate'
    )
);