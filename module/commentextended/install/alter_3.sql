drop table if exists `[%%]commentextended`;

CREATE TABLE `[%%]commentextended` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(11),
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `message` text,
    `approved` tinyint(1) NOT NULL,
`parent_id` int(11) unsigned DEFAULT '0',
`parents_tree` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT="Комментарии";
