<?php
/**
 * управление комментариями
 *
 * @author Mort
 */
class Module_Commentextended_Model_Comment
{
    public $id;

    public $date;

    public $name;

    public $email;

    public $message;

    public $approved;

    public $parent_id;
    
    public $parents_tree;
}
