<?php

/**
 * управление комментариями
 *
 * @author Mort
 */
class Module_Commentextended_Controller_Manage_Comment extends Lib_Jgrid_Controller_Base{
    
    /**
     * отрисовка стартового html
     */
    protected function _defaultAction()
    {
        $hotelHelper = new Module_Division_Helper();
        $hotelHelper->checkAccess();
        
        $mapper = new Module_Commentextended_Mapper_Manage_Comment();
        $comments = $mapper->getComments();
        
        
        //формируем массив для парентов
        $stringParams = '<div id="row_{id}" class="reviewElement parent_{parent_id}" approved="{approved}" style="height:35px;">
            {separator}
            <span class="badge" title="{message}">
                <font id="row_message" style="display:none;">{message}</font>
                <font id="row_name">{name}</font>
                &nbsp&nbsp&nbsp
                ( <font id="row_email">{email}</font> )
                &nbsp&nbsp&nbsp
                {date}
                &nbsp&nbsp&nbsp
                <i class="icon-thumbs-up" onclick="commentextended.makeApproved({id})" style="cursor:pointer;"></i>
                <i class="icon-download" onclick="commentextended.makeAddChild({id})" style="cursor:pointer;"></i>
                <i class="icon-pencil" onclick="commentextended.makeEdit({id})" style="cursor:pointer;"></i>
                <i class="icon-trash" onclick="commentextended.makeDelete({id})" style="cursor:pointer;"></i>
            </span>
            </div>
            <script>
                
            </script>';
        
        $libTree = new Component_Vcl_Tree();
        $drawTreeParams = array(
            'array' => $comments,
            'stringParams' => $stringParams,
            'parentKey' => 'parent_id',
            'separatorString' => '&nbsp&nbsp&nbsp&nbsp&nbsp<sup></sup>'
        );
        
        $commentsIdEdit = $libTree->drawTree($drawTreeParams, 0);
        $commentsIdEditString = implode("", $commentsIdEdit);
        
        $html = '';
        $html .= '<div style="width:100%;"><div style="margin: 0px auto; width:800px;">';
        $html .= '<center><h3> Отзывы </h3></center><br>';
        $html .= '<center><button class="btn" type="button" onclick="commentextended.makeAddChild(0)">Добавить новый</button></center><br>';
        $html .= $commentsIdEditString;
        $html .= '</div></div>';
        $html .= '<script>commentextended.format();$(".reviewElement span").tooltip();</script>';
        
        return $html;
    }
    
    
    protected function _approveAction() {
        $id = $this->_getRequest()->get('id');
        $mapper = new Module_Commentextended_Mapper_Manage_Comment();
        $mapper->approve($id);
        
        return array(
            'result' => 1
        );
        
    }
    
    protected function _deleteAction() {
        $id = $this->_getRequest()->get('id');
        $mapper = new Module_Commentextended_Mapper_Manage_Comment();
        $mapper->del($id);
        
        return array(
            'result' => 1
        );
        
    }
    
    protected function _editAction() {
        
        $table = new Module_Commentextended_Model_Comment();
        $table->id = $this->_getRequest()->get('id');
        $table->date = time();
        $table->name = $this->_getRequest()->get('name');
        $table->email = $this->_getRequest()->get('email');
        $table->message = $this->_getRequest()->get('message');
        
        $mapper = new Module_Commentextended_Mapper_Manage_Comment();
        $mapper->update($table);
        
        return array(
            'result' => 1
        );
        
    }
    
    protected function _addAction() {
        
        $table = new Module_Commentextended_Model_Comment();
        $table->parent_id = $this->_getRequest()->get('id');
        $table->date = time();
        $table->name = $this->_getRequest()->get('name');
        $table->email = $this->_getRequest()->get('email');
        $table->message = $this->_getRequest()->get('message');
        
        $mapper = new Module_Commentextended_Mapper_Manage_Comment();
        $mapper->add($table);
        
        return array(
            'result' => 1
        );
        
    }
    
    protected function _checkNewCommentAction() {
        $mapper = new Module_Commentextended_Mapper_Manage_Comment();
        $count = $mapper->getNotApprovedCount();
        
        return array(
            'result' => 1,
            'count' => $count
        );
    }
    
}

?>
