--
-- Структура таблицы `publications_division`
--

DROP TABLE IF EXISTS `[%%]publications_division`;
CREATE TABLE IF NOT EXISTS `[%%]publications_division` (
  `publications_id` int(11) NOT NULL,
  `division_id` int(11) unsigned NOT NULL,
  KEY `publications_id` (`publications_id`),
  KEY `division_id` (`division_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Ограничения внешнего ключа таблицы `publications_division`
--
ALTER TABLE `[%%]publications_division`
  ADD CONSTRAINT `publications_division_ibfk_1` FOREIGN KEY (`publications_id`) REFERENCES `[%%]publications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `publications_division_ibfk_2` FOREIGN KEY (`division_id`) REFERENCES `[%%]division` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;