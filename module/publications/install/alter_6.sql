ALTER TABLE [%%]publications ADD column `uid` INT(11);

ALTER TABLE [%%]publications ADD
      CONSTRAINT fk_publications_uid
      FOREIGN KEY (`uid`)
      REFERENCES [%%]users(id) ON DELETE CASCADE ON UPDATE CASCADE;