CREATE TABLE IF NOT EXISTS [%%]publications (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='публикации' AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS [%%]publications_lang (
  `page_id` int(11) NOT NULL,
    `lang_id` int(11) NOT NULL,
  `page_title` varchar(20) NOT NULL,
    `page_content` text NOT NULL,
    `page_preview` text NOT NULL,
    `title` text NOT NULL,
    `keywords` text NOT NULL,
    `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='публикации' AUTO_INCREMENT=1 ;

ALTER TABLE [%%]publications_lang ADD
      CONSTRAINT fk_publications_lang_page_id
      FOREIGN KEY (`page_id`)
      REFERENCES [%%]publications(id) ON DELETE CASCADE ON UPDATE CASCADE;