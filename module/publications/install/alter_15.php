<?php

$labelMapper = new Module_Labels_Mapper();
$labelModel = new Module_Labels_Model();

$labelModel->lang_id = 1;
$labelModel->sys_name = 'pageNotFound';
$labelModel->name = 'Запрашиваемая страница не найдена';
$labelModel->is_system = 1;

$labelMapper->add($labelModel);

?>
