ALTER TABLE [%%]publications ADD column `division` INT(11) UNSIGNED NULL;

ALTER TABLE [%%]publications ADD
      CONSTRAINT fk_publications_division
      FOREIGN KEY (`division`)
      REFERENCES [%%]division(id) ON DELETE CASCADE ON UPDATE CASCADE;