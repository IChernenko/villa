DELETE FROM [%%]publications;
DELETE FROM [%%]publications_lang;
ALTER TABLE [%%]publications AUTO_INCREMENT =1;
ALTER TABLE [%%]publications_lang AUTO_INCREMENT =1;
INSERT INTO [%%]publications (`id`, `name`) VALUES
(1, 'Об отеле');
INSERT INTO [%%]publications_lang (`page_id`, `lang_id`, `page_title`, `page_content`, `page_preview`, `title`, `keywords`, `description`) VALUES
(1, 1, 'Об отеле', 'Об отеле...Детальное Описание...', 'asdf2', 'Об отеле', 'Об отеле - ключевые слова', 'Об отеле Описание');