<?php

global $package;
$package = array(
    'version' => '16',
    'name' => 'module.publications',
    'dependence' => array(
        'module.entity',
        'module.gallery',
        'module.auth',
        'module.lang',
        'module.labels',
        'lib.paginator',
        'ws'=>array(
            'admin'=>array(
                'lib.jquery.elrte',
                'lib.elfinder',
                'module.handbooksgenerator',
                'lib.jquery.elrte',
                'lib.jdatepicker',
                'lib.timepicker',    
                'lib.jquery.timepicker'
            ),            
        )
    ),
    'js' => array(
        'ws'=>  array(
            'admin'=>array(
                '../module/publications/js/manage/publications.js',
            )
        )
                
    ),
    'css'=>array(
        
        '../module/publications/css/manage/publications.css'        
    )
    
    
);
?>