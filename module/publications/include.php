<?php
Dante_Lib_Config::set('publication.manage.resize', true);
Dante_Lib_Config::set('publication.resizeWidth', 180);
Dante_Lib_Config::set('publication.resizeHeight', 160);
Dante_Lib_Config::set('publication.imgDir', 'media/publications/');
Dante_Lib_Config::set('publication.allowComments', false);
Dante_Lib_Config::set('publication.allowGallery', false);
Dante_Lib_Config::set('publication.allowPreview', false);
Dante_Lib_Config::set('publication.allowImage', true);

?>
