<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of division
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Publications_Dmo_Division extends Dante_Lib_Orm_Table{
    
    protected $_tableName = '[%%]publications_division';
    
    /**
     *
     * @param type Module_Publications_Dmo_Division 
     */
    public function byDivision($divisionId) {
        return $this->getByAttribute(array('division_id' => $divisionId));
    }
}

?>
