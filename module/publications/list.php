<?php



/**
 * Контроллер отображения списка публикаций
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Publications_List extends Dante_Controller_Base{
    
    protected function _defaultAction() {
        $divisionMapper = new Module_Division_Mapper();
        $divisionId = (int)$divisionMapper->getByName('homepage');
        if ($divisionId == 0) return 'Не задан раздел home';
        
        $publicationsMapper = new Module_Publications_Mapper();
        $list = $publicationsMapper->getList(array(
            'filter' =>array('division' => $divisionId)
        ));
        
        //var_dump($list);
        
        $tpl = new Dante_Lib_Template();
        $tpl->news = $list;
        return $tpl->draw('../module/publications/template/list.html');
    }
}
?>
