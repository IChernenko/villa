<?php

class Module_Publications_Manage_Controller extends Module_Handbooksgenerator_Controller_Base {
    protected function _defaultAction(){
        //return $this->_drawAction();
    }
    
    protected function _drawAction(){
        $hotelHelper = new Module_Division_Helper();
        $hotelHelper->checkAccess();
        
        $mapper     = new Module_Publications_Manage_Mapper;
        $model      = new Module_Publications_Manage_Modelgrid;      
        $params     = $this->getParams($model);
        $lang       = $this->_getRequest()->get_validate('lang_id', 'int', 1);
        $rows       = $mapper->getPublications($params, $lang);
        $this->formEnable('module.publications.manage.controller');
        $this->tableParams['class'][]='publicationTable';
        $this->caption='Управление публикациями (новости, страницы, статьи)';
        $this->_setHead();
        $this->_setBody($rows['rows']);
        $this->tfoot=array(
            array($this->paginator($rows['count'], $params)),
            array(
                'button_filter'=>array(
                    "type"=>array(
                        'edit'
                    ),                    
                )
             ),       
        );
        //Убираем колонку публикации
        if(Dante_Lib_Config::get('pulications.multidivision')){
            $this->_killColumnByNum(4);
        }else{
            $this->_killColumnByNum(5);
        }
        $html=$this->generate();        
        return $html;
    }
    
    protected function _drawAjaxAction(){
        return array(
            'result'=>1,
            'html'=>$this->_drawAction()
        );
    }
    
    protected function _drawFormAction($id=false){
        
        $divisions  = new Module_Division_Manage_Mapper;
        $mapper     = new Module_Publications_Manage_Mapper;
        $tpl        = new Module_Publications_Manage_View;
        
        if(!$id)
            $id     = $this->_getRequest()->get_validate('id', 'int');
        $lang       = $this->_getRequest()->get_validate('lang_id', 'int', 1);
               
        $vars           = array('id'=>'','uid'=>'', 'date'=>'', 'time'=>'');
        $publication    = $mapper->getPublication($id, $lang);
        $vars           = array_merge($vars, (array)$publication);
         
        $vars['date']   = Dante_Helper_Date::converToDateType($vars['date']);
        $vars['time']   = Dante_Helper_Date::converToDateType($vars['time'], false, 'H:i');
        $vars['multidivisions'] = Dante_Lib_Config::get('pulications.multidivision');
        $vars['languages']      = $this->_getLanguages();
        $vars['divisions']      = $divisions->getAvailableDivisions(array(1,2));
        $vars['divisions']      = $this->_combobox($vars['divisions'], 'division_id', array('value'=>'id', 'text'=>'name'), false, false, 3, 'id', 'parent_id', true);
        $vars['divisions_use']  = $mapper->getDivisions($vars['id']);       
        $html       = $tpl->form($vars);
        return array(
            'result'=>1,
            'html'=>$html
        );
    }
    
    /**
     * генерация формы редактирования картинок
     * @param type $id
     * @return type 
     */
    protected function _drawImgFormAction($id=false)
    {    
        if(!$id) $id = $this->_getRequest()->get('id');
        $entMapper = new Module_Entity_Mapper();
        $entType = $entMapper->getBySysName('publication')->id;
        $imgMapper = new Module_Gallery_Mapper();
        $vars['images'] = $imgMapper->getByEntity($entType, $id);
        $vars['id'] = $id;
        
        $view = new Module_Publications_Manage_View;      
        $html = $view->imgForm($vars);
        return array(
            'result'=>1,
            'html'=>$html
        );
    }
    
    /**
     * сохраняет изображение в базе данных
     * @return type 
     */
    protected function _saveGalleryAction()
    {
        $model = new Module_Gallery_Model;
        $entMapper = new Module_Entity_Mapper();
        
        $imgNameUrl = $this->_getRequest()->get_validate('url', 'text');
        $imgName = trim((str_ireplace(Dante_Lib_Config::get('publication.imgDir'), '', $imgNameUrl)), ' /');
        
        $entId = $this->_getRequest()->get('id');
        
        $model->entity_type_id = $entMapper->getBySysName('publication')->id;
        $model->entity_id = $entId;
        $model->file_name = $imgName;
        $model->adding_date = time();
        
        $galMapper = new Module_Gallery_Mapper();
        $galMapper->addFile($model);
        
        return $this->_drawImgFormAction($entId);
    }
    
    protected function _imgCommentAction()
    {
        $model = new Module_Gallery_Model;
        $model->id = $this->_getRequest()->get('id');
        $model->comment = $this->_getRequest()->get_validate('comment', 'text');
        $mapper = new Module_Gallery_Mapper();
        $mapper->addComment($model);
        return array(
            'result' => 1
        );
    }


    protected function _delImageAction()
    {
        $id = $this->_getRequest()->get('id');
        $mapper = new Module_Gallery_Mapper();
        $mapper->fileDelete($id);
        return array(
            'result' => 1
        );
    }

    /**
     * редактирование/добавление/удаление записей грида
     */
    protected function _editAction() {
        
        $mapper     = new Module_Publications_Manage_Mapper;
        $model      = new Module_Publications_Manage_Model;
        $lang       = (int)$this->_getRequest()->get('lang_id', 1);
        $id         = $this->_getRequest()->get_validate('id', 'int', 0);        
        $uid        = (int)Dante_Helper_App::getUid();
        
        $imgName    = false;
        //print_r($_REQUEST);

        $pageContent = $this->_getRequest()->get_validate('page_content', 'text', "");
        $pagePreview = $this->_getRequest()->get_validate('page_preview', 'text', "");

        /*if (!function_exists ('tidy_parse_string')) {
            throw new Exception('tidy not installed');
        }

        $parameters = array (
            'clean' => true,
            'drop-proprietary-attributes' => true,
            'drop-font-tags' => true,
            'drop-empty-paras' => true,
            'enclose-text'	=> true,
            'fix-backslash'	=> false,
            'force-output'	=> true,
            'hide-comments' => true,
            'indent'	=> false,
            'indent-spaces'	=> 4,
            'join-classes' => true,
            'join-styles' => true,
            'logical-emphasis' => true,
            'output-xhtml' => true,
            'merge-divs'	=> true,
            'show-body-only'	=> true,
            'word-2000'	=> true,
            'wrap'	=> 0,
        );

        $pageContent = tidy_parse_string ($pageContent, $parameters);
        tidy_clean_repair ($pageContent);
        $pageContent = tidy_get_output ($pageContent);*/
        //$pageContent = strip_tags($pageContent, '<p><b><a><i><u>');

        $cleaner = new Lib_Htmlcleaner_Htmlcleaner();
        //$pageContent = $cleaner->cleanup($pageContent);
        //$pageContent = str_replace('&nbsp;', ' ', $pageContent);

        if (strlen($pagePreview)>0) {
            //$pagePreview = $cleaner->cleanup($pagePreview);
            //$pagePreview = str_replace('&nbsp;', ' ', $pagePreview);
        }
        
        $model->id          = $id;
        $model->name        = $this->_getRequest()->get_validate('name', 'text');
        $model->url         = $this->_getRequest()->get_validate('publication_url', 'text');        
        $model->uid         = $uid;
        $model->date        = Dante_Helper_Date::strtotimef($this->_getRequest()->get_validate('date', 'date', NULL), 0);
        $model->time        = Dante_Helper_Date::strToTime($this->_getRequest()->get('time'), $model->date);
        if(!$model->date)$model->date=time();
        if(!$model->time)$model->time=time();
        $model->lang        = $lang;
        $model->image       = $this->_getRequest()->get_validate('image', 'text');
        $model->title       = $this->_getRequest()->get_validate('title', 'text', "");
        $model->description = $this->_getRequest()->get_validate('description', 'text', "");
        $model->keywords    = $this->_getRequest()->get_validate('keywords', 'text', "");
        $model->divisions   = $this->_getRequest()->get_validate('divisions', 'array');
        $model->division    = $this->_getRequest()->get_validate('division', 'int');
        $model->auth_only   = $this->_getRequest()->get_validate('auth_only', 'int', 0);
        $model->page_title      = $this->_getRequest()->get_validate('page_title', 'text', "");
        $model->page_preview    = $pagePreview;
        $model->page_content    = $pageContent;
      
        //подгоняем URL
        if(!$model->url) $model->url = $model->name;
        $urlText  = str_replace('.html', '', $model->url);
        $transUrl = Dante_Lib_Transliteration::transform($urlText);
        $model->url  = substr($transUrl, 0, 95).'.html';
        
        if(!$model->uid) $model->uid=$uid;
        //Ресайз и перенос картинки        
        if(Dante_Lib_Config::get('publication.manage.resize') && $model->image){
            $image = rawurldecode($model->image);
            $ext = strstr($image, '.');
            $newName = md5(time()).$ext;
            $dirname = dirname($image);
            $dir    = (substr($dirname,0,1)=='/'?substr($dirname,1):$dirname).'/';
            if($dirname && strlen($dirname)>2 && is_file(substr($image,1))){
                $name   = basename($image);
                $dstDir = Dante_Lib_Config::get("publication.imgDir");            
                $width  = Dante_Lib_Config::get("publication.resizeWidth");
                $height = Dante_Lib_Config::get("publication.resizeHeight");
                $model->image = $newName;
                Lib_Image_Helper::resize($dir, $name, $dstDir, $width, $height, $newName);
            }
        }       
        $edit_id=$mapper->edit($model);
        
        
        if($this->_getRequest()->get('returntype')==1){
            return $this->_drawFormAction($edit_id);
        }
        return array(
            'result'=>1,
            'id'=>$edit_id
        );       
    }
    
    protected function _delAction(){
        $mapper = new Module_Publications_Manage_Mapper();
        $id=$this->_getRequest()->get_validate('id', 'int');
        $mapper->del($id);
        
        return array(
           'result'=>1 
        );
    }

//    /**
//     * Ресайзит заданную картинку
//     * @param string $fileName 
//     * @author Dorian
//     */
//    protected function _resizeImage($fileName) {
//        $image = new Lib_Image_Driver();
//        $image->load($fileName);
//
//        $widthTemp = $image->getWidth();
//        $heightTemp = $image->getHeight();
//        
//        $resizeWidth = Dante_Lib_Config::get('publication.resizeWidth');
//        $resizeHeight = Dante_Lib_Config::get('publication.resizeHeight');
//
//        if($widthTemp > $resizeWidth) $image->resizeToWidth($resizeWidth);
//        elseif($heightTemp > $resizeHeight) $image->resizeToHeight($resizeHeight);
//
//        $image->save($fileName);
//        chmod($fileName, 0644);
//    }
    
    /**
     * загрузка картинки
     * @return boolean 
     */
//    protected function _uploadAction() {
//        $typeId = $this->_getRequest()->get('typeId');
//        
//
//        Dante_Lib_Log_Factory::getLogger()->debug('$typeId = '.$typeId);
//        
//        if(isset($_FILES['Filedata'])){
//            if (!is_dir(Dante_Lib_Config::get('app.uploadFolder'))) {
//                mkdir(Dante_Lib_Config::get('app.uploadFolder'), 0775, true);
//            }
//            
//            if (!is_dir(Dante_Lib_Config::get('app.uploadFolder').'/publications')) {
//                mkdir(Dante_Lib_Config::get('app.uploadFolder').'/publications');
//            }
//            
//            if (!is_dir(Dante_Lib_Config::get('app.uploadFolder').'/publications/logos')) {
//                mkdir(Dante_Lib_Config::get('app.uploadFolder').'/publications/logos');
//            }
//            
//            $uploadDir = Dante_Lib_Config::get('app.uploadFolder').'/publications/logos/';
//            
//            $uploader = new Component_Swfupload_Upload();
//            $fileName = $uploader->upload($uploadDir);
//            
//            if (Dante_Lib_Config::get('publication.manage.resize') === true) {
//                $this->_resizeImage($uploadDir.$fileName);
//            }
//                        
//            return $fileName;
//        }else{
//            return false;
//        }
//    }
    protected function _setHead(){        
        $division   = new Module_Division_Manage_Mapper;
        $mapper     = new Module_Publications_Manage_Mapper;
        
        $divisions  = $division->getAvailableDivisions(array(1,4,5));
        $divisions  = $this->_combobox($divisions, 'division', array('value'=>'id', 'text'=>'name'), false, true, 3);
        
        $userlist   = $mapper->getUsers();
        $userlist   = $this->_combobox($userlist, 'uid', array('value'=>'id', 'text'=>'login'));
        $title = array(
            array(
                'content'=>"№",
                'params'=>array(
                    'style'=>array(
                        'width'=>'14px'
                    )
                )
            ),
            array(
                'content'   =>'Название',
                'sortable'  =>'name',
                'sortable_def' =>'desc',
                'params'=>array(
                    'style'=>array(
                        'width'=>'130px'
                    )
                )
            ),
            array(
                'content'   =>'URL',
                'sortable'  =>'url',
                'params'=>array(
                    'style'=>array(
                        'width'=>'100px'
                    )
                )
            ),
            array(
                'content'   =>'Раздел',
                'sortable'  =>'division_name',
                'params'=>array(
                    'style'=>array(
                        'width'=>'60px'
                    )
                )
            ),
            array(
                'content'   =>'Категории',
                'sortable'  =>'division_id',
                'params'=>array(
                    'style'=>array(
                        'width'=>'100px'
                    )
                )
            ),
            
            array(
                'content'   =>'Дата добавления',
                'params'=>array(
                    'style'=>array(
                        'width'=>'114px'
                    )
                )                
            ),
            
            array(
                'content'   =>'Автор',
                'sortable'  =>'uid',
                'params'=>array(
                    'style'=>array(
                        'width'=>'132px'
                    )
                )
            ),
        );
        if(Dante_Lib_Config::get('publication.allowGallery'))
            $title[] = array(
                'content'   =>'Картинки',
                'params'=>array(
                    'style'=>array(
                        'width'=>'70px'
                    )
                )
            );
        
        $title[] = array(
                'content'   =>'Функции',
                'params'=>array(
                    'style'=>array(
                        'width'=>'132px'
                    )
                )                
            );
        
        $filters    = array(
            '',
            "name"=>array(
                "type"=>"text",
                "value"=>$this->_getRequest()->get("name"),
            ),
            
            "publication_url"=>array(
                "type"=>"text",
                "value"=>$this->_getRequest()->get("publication_url"),
            ),
            "division"=>$divisions,//$divisions,
            "division_id"=>'',//$divisions,
            "date"=>array(
                "type"=>"datepick",
                "value"=>$this->_getRequest()->get("date"),
            ),
            "uid"=>$userlist
        );
        if(Dante_Lib_Config::get('publication.allowGallery'))
            $filters[] = NULL;
        
        $filters[] =  array(
                "type"=>"button_filter"
            );
            
        
        $this->thead=array(
            $title,
            $filters
        );
    }
    protected function _setBody($rows){    
                
        $i=0;
        foreach($rows as $row){            
            $division = $this->_divisionFromBody($row['id']);            
            $i++;
            $cur=array();
            $cur['num']             = $i;
            $cur['name']            = $row['name'];
            $cur['url']             = $row['url'];
            $cur['division']        = $row['division_name'];
            $cur['page_title']      = $division;
            $cur['datetime']        = Dante_Helper_Date::converToDateType($row['time'], 7);            
            $cur['autor']           = $row['autor'];
            if(Dante_Lib_Config::get('publication.allowGallery'))
            {
                $cur['images'] = '<a class="btn btn-mini btn-info" 
                    style="width: 65px;" onclick="publications.imgEdit('.$row['id'].');">View/Edit</a>';
            }
            $cur['buttons'] = array(
                'id'=>$row['id'],
                'type'=>array('edit', 'del'),                
            );
            $this->tbody[$row["id"]]=$cur;
        }
    }
    protected function _divisionFromBody($id){
        $mapper     = new Module_Publications_Manage_Mapper;
        $rows       = $mapper->getDivisions($id);
        $div=array();
        foreach($rows as $row){
            $div[]="<div>".$row."</div>";
        }
        return join('',$div);
    }
    
    protected function _getLanguages(){
        $lang       = new Module_Lang_Mapper();
        $languages  = $lang->getList();
        $thisLang   = (int)$this->_getRequest()->get('lang_id', 1);
        foreach($languages as &$cur){
            $cur=(array)($cur);
        }
        $combobox   = $this->_combobox($languages, 'lang_id', array('value'=>'id', 'text'=>'name'), $thisLang, FALSE); 
        return $combobox;
    }

    protected function _combobox($vars, $name, $f, $selected=false, $add_null=true, $parentId=false , $childrenName='id', $parentName='parent_id', $returnOnlyItems=false){
        
        $selected=$selected?$selected:$this->_getRequest()->get($name);
        $combobox   = new Component_Vcl_Combobox;
        $combobox->addAttributeArray(array(
            'id'=>$name,
            'name'=>$name,
            'class'=>'combobox',
        ));
        $combobox->selected_value=$selected;
        if($add_null)$combobox->addItem('0', '');
        if($parentId){            
            $combobox->tree($childrenName, $parentName, $parentId, $f['value'], $f['text']);
            $combobox->_addItemsArrayTree($vars, $parentId);
            
        }else{
            foreach($vars as $cur){
                $combobox->addItem($cur[$f['value']], $cur[$f['text']]);
            }
        }
        if($returnOnlyItems) return $combobox->getItems ();
        return $combobox->draw();        
    }
}

?>
