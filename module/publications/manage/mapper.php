<?php

/**
 * Description of Publications
 *
 * @author Mort
 */
class Module_Publications_Manage_Mapper extends Module_Handbooksgenerator_Mapper_Base {
    
    protected $_tableName = '[%%]publications';
    protected $_tableNameLang = '[%%]publications_lang';
    protected $_tableDivision = '[%%]division';
    protected $_tablePublicationsDivision = "[%%]publications_division";
    protected $_tableUsers = "[%%]users";


    public function getPublications($params=false, $lang=1) {
        //$this->debug=true;
        $join=array(
            "left join {$this->_tablePublicationsDivision} as pd on pd.publications_id=t1.id",
            "left join {$this->_tableDivision} as d on t1.division=d.id",
            "left join {$this->_tableNameLang} as pl on (pl.page_id=t1.id and pl.lang_id={$lang})",
            "left join {$this->_tableUsers} as `u` on `u`.id=t1.uid",
        );
        
        $cols=array(
            't1.*',
            'd.name as division_name',
            'pl.*',
            'u.login as autor' 
        );
        $publications   = $this->getRowsByParams($params, $cols, $join);        
        
        
        return $publications;
    }
    
    public function getPublication($id=false, $lang=null){
        if(!$id)return null;
        $sql="SELECT 
                t1.*,
                pl.*,
                u.login
                
                FROM {$this->_tableName} as t1 ";
        $sql.=  "left join {$this->_tablePublicationsDivision} as pd on pd.publications_id=t1.id ".
                "left join {$this->_tableDivision} as d on d.id=pd.division_id ".
                "left join {$this->_tableNameLang} as pl on (pl.page_id=t1.id and pl.lang_id={$lang}) ".
                "left join {$this->_tableUsers} as `u` on `u`.id=t1.uid ".
                "WHERE t1.id={$id} ".
            "";
      
        $r = Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
        return $r;
    }

    public function getDivisions($id=FALSE){
        if(!$id)return FALSE;
        $sql="SELECT d.name, d.id from {$this->_tablePublicationsDivision} as pd
        LEFT JOIN {$this->_tableDivision} as d ON d.id=pd.division_id";
        
            $sql .=  " WHERE pd.publications_id={$id}";
        
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $result=array();
        while($f= Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)){
            $result[$f['id']]=$f['name'];
        }
        return $result;        
    }
    
    public function getUsers(){  
        $users = $this->getRowsByParams(array('tableName'=>$this->_tableUsers));
        return $users['rows'];        
    }   
    
    /**
     * удаление
     * @param int $id 
     */
    public function del($id) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->delete(array(
            'id' => $id
        ));
    }
    /**
     * Добавить/редактировать запись в таблице
     * @param Module_Publications_Manage_Model $model
     * @return int
     */
    public function edit(Module_Publications_Manage_Model $model){
        
        $orm = new Dante_Lib_Orm_Table($this->_tableName);
        $orm ->name     = $model->name;
        $orm ->url      = $model->url;
        $orm ->date     = $model->date;
        $orm ->time     = $model->time;
        $orm ->image    = $model->image;
        $orm ->division = $model->division;
        $orm->auth_only = $model->auth_only;
        if(!$model->uid)$model->uid=NULL;
        $orm ->uid      = $model->uid;       
        if(!$model ->id){
            $id = $orm->insert();            
        }else{
            $orm->update(array('id'=>$model->id));
            $id = $model->id;
        }
       
        //----------- langtable -----------
        $orm = new Dante_Lib_Orm_Table($this->_tableNameLang);
        
        $orm->page_id       = $id;
        $orm->lang_id       = $model->lang;
        $orm->page_title    = $model->page_title;
        $orm->page_content  = $model->page_content;
        $orm->page_preview  = $model->page_preview;
        $orm->title         = $model->title;
        $orm->keywords      = $model->keywords;
        $orm->description   = $model->description;
        
        $orm->delete(array('page_id'=>$id, 'lang_id'=>$orm->lang_id ));
        $orm->apply(array('page_id'=>$id, 'lang_id'=>$orm->lang_id ));        
        //echo mysql_error();
        $this->updateDivisions($id, $model->divisions);
        return $id;
    }
    
    public function updateDivisions($id, $divisions){
        $orm = new Dante_Lib_Orm_Table($this->_tablePublicationsDivision);
        $orm->delete(array('publications_id'=>$id));
        if(!$divisions)
            return;
        foreach($divisions as &$cur){
            if(!$cur=(int)$cur)
                continue;
            $cur="({$id}, {$cur})";
        }
        $divisions = join(', ', $divisions);
        $sql="INSERT INTO {$this->_tablePublicationsDivision} (`publications_id`, `division_id`) VALUES {$divisions}";
        Dante_Lib_SQL_DB::get_instance()->exec($sql);
    }  
    
}

?>
