<?php
class Module_Publications_Manage_Modelgrid extends Module_Publications_Manage_Model{
    public $sortable_rules=array(
        'name'=>'name',
        'type'=>'desc',
    );
    
    public $filter_rules=array(
        array(
            'name'=>'divisions_filter',
            'format'=>'`t1`.`id` IN "%2$s"'
            ),
        array(
            'name'=>'division',
            'format'=>'`t1`.`%1$s` = "%2$s"'
            ),
        array(
            'name'=>'name',
            'format'=>'`t1`.`%1$s` LIKE "%3$s%2$s%3$s"'
            ),
        array(
            'name'=>'publication_url',
            'format'=>'`t1`.`url` LIKE "%3$s%2$s%3$s"'
            ),
        array(
            'name'=>'date',
            'format'=>'date'
            ),
        array(
            'name'=>'uid',
            'format'=>'`t1`.`%1$s` = "%2$s"'
            ),
        );
       
}
?>