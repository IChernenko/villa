<?php

/**
 * Description of callback
 *
 * @author Mort
 */
class Module_Publications_Manage_Model {
    
    public $id;

    public $name;

    public $page_title;

    public $page_content;

    public $page_preview;

    public $title;

    public $keywords;

    public $description;
    
    public $url;
    
    public $date;

    public $humanDate;
    
    public $time;

    public $humanTime;
    
    public $image;
    
    public $type;
    
    public $uid;
    
    public $division;
    
    public $division_id;
    
    public $publication_id;
    
    public $lang;
    
    public $divisions;
    
    public $auth_only;
}

?>
