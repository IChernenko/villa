<?
class Module_Publications_Manage_View{
    
    function buttons($vars=false){
        $tpl = new Dante_Lib_Template();
        $tpl->_vars=$vars;
        $fileName = '../module/publications/manage/template/buttons.html';
        if (file_exists('template/default/manage/publications/buttons.html')) {
            $fileName = 'template/default/manage/publications/buttons.html';
        }
        return $tpl->draw($fileName);        
    }
    
    function form($vars=false){
        $tpl = new Dante_Lib_Template();
        $tpl->_vars=$vars;
        $fileName = '../module/publications/manage/template/form.html';
        if (file_exists('template/default/manage/publications/form.html')) {
            $fileName = 'template/default/manage/publications/form.html';
        }
        return $tpl->draw($fileName);        
    }
    public function prevContent($content){
        $tpl = new Dante_Lib_Template();
        $tpl->content=$content;
        $fileName = '../module/publications/manage/template/prevcontent.html';
        if (file_exists('template/default/manage/publications/prevcontent.html')) {
            $fileName = 'template/default/manage/publications/prevcontent.html';
        }
        return $tpl->draw($fileName);
    }
    
    public function imgForm($vars = false){
        $tpl = new Dante_Lib_Template();
        $tpl->_vars=$vars;
        $fileName = '../module/publications/manage/template/imgform.html';
        if (file_exists('template/default/manage/publications/imgform.html')) {
            $fileName = 'template/default/manage/publications/imgform.html';
        }
        return $tpl->draw($fileName);        
    }
    
}