<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 30.05.12
 * Time: 11:29
 * To change this template use File | Settings | File Templates.
 */
class Module_Publications_Controller extends Dante_Controller_Base
{
    protected function _getPageUrl() {
        
        $pageUrl = $this->_getParam('pageUrl');
        if ($pageUrl) return $pageUrl;
        
        $pageUrl = $this->_getParam('%0');
        if (!$pageUrl) $pageUrl = 'about.html';
        $pageUrl = explode("/", $pageUrl);
        return $pageUrl;
    }
    
    protected function _defaultAction() 
    {
        $pageUrl = $this->_getPageUrl();
        
        $lang = Module_Lang_Helper::getCurrent();

        $mapper = new Module_Publications_Mapper();
        if (is_array($pageUrl)) {
            $pageUrl = $pageUrl[count($pageUrl)-1];// временный хак!
        }
        
        $publication = $mapper->getByLink($pageUrl, $lang);                                     //ЗДЕСЬ
        
        //Может это архив ? Тогда отправляем в архивы.
        // test!!!!!!!!!!!
        if (!$publication) {
            
            // попробуем найти раздел
            $divisionMapper = new Module_Division_Mapper();
            $divisionId = (int)$divisionMapper->getByLink($pageUrl);
            //var_dump($pageUrl);
            if ($divisionId > 0) { // у нас есть такой раздел
                
                $params = array();
                $params['filter']['division'] = $divisionId;
                $params['maxWords'] = Dante_Lib_Config::get('news.previewMaxWords');
                
                
                $mapper = new Module_Publications_Mapper();
                $list = $mapper->getList($params, $lang);
                //var_dump($list);
                if (!$list) $list = array();
                
                // загрузить инфу по разделу
                $division = $divisionMapper->getById($divisionId);
                $view = new Module_Publications_View_List();
                $paginatop = new Lib_Paginator_Driver();
                $paginatop->link = $pageUrl;                
                $this->_setSeo($division->title, $division->description, $division->keywords);
                return $view->draw($list, $division).$paginatop->draw(count($list));
            }
        }
        else {
            $divisionMapper = new Module_Division_Mapper();
            $divisionId = (int)$divisionMapper->getByLink($pageUrl);
            $division = $divisionMapper->getById($divisionId);            
            $this->_setSeo($division->title, $division->description, $division->keywords);
        }       
        
        if (!$publication) return Module_Lang_Helper::translate('pageNotFound');         
                
        if(!Dante_Helper_App::getUid() && $publication->auth_only == 1)
            return 'Информация доступна только для зарегистрированных пользователей';
        
        $tpl = new Dante_Lib_Template();
        $tpl->publication = $publication;
        
        if(Dante_Lib_Config::get('publication.allowGallery'))
        {            
            $entMapper = new Module_Entity_Mapper();
            $gallMapper = new Module_Gallery_Mapper();
            $entType = $entMapper->getBySysName('publication')->id;
            $gallery = $gallMapper->getByEntity($entType, $publication->id); 
            $tpl->gallery = $gallery;
        }
        
        $wsName = Dante_Helper_App::getWsName(); 
        $tplFilePath = '../module/publications/template/detail.html';
        $custom = "template/$wsName/publications/detail.html";
        if (file_exists($custom)) $tplFilePath = $custom;
        
        return $tpl->draw($tplFilePath);
    }
    
    protected function _drawArhiveAction(){
        
        $pageUrl = $this->_getPageUrl();
        $lang = Module_Lang_Helper::getCurrent();
        
        $mapper = new Module_Publications_Mapper();
        $divisionMapper = new Module_Division_Mapper();
        $divisionId = (int)$divisionMapper->getByLink($pageUrl[1]);
        if ($divisionId > 0) 
        {
            $params = array();
            $params['filter']['division'] = $divisionId;
            // у нас есть такой раздел
            ##Собираем paginator
            $paginator= new Lib_Paginator_Driver();
            if(!isset($pageUrl[2]))
                $pageUrl[2]='page1';
            $pageSegment = $pageUrl[2];
            if(!$pageSegment) $pageSegment=1;            
            //номер страницы
            $page = str_replace('page', '', $pageSegment);
            
            //Количество записей всего
            $countRows=$mapper->getCountList($params, $lang);
            //Ссылка перед номером страницы.
            $paginator->link = str_replace('/page'.$page, '', $this->_getParam('%0'));            
            $paginator->draw($countRows, $page);            
            $params['limit'] = $paginator->limit;
            
            $list = $mapper->getList($params, $lang);                
            if (!$list) $list = array();          
            
            // загрузить инфу по разделу
            $division = $divisionMapper->getById($divisionId);             
            $view = new Module_Publications_View_List();
            $this->_setSeo($division->title, $division->description, $division->keywords);
            return $view->drawArhive($list, $division, $paginator->html);
        }
        
        return "";
    }

    /**
     * Отображение детализации новости.
     * 
     * Пример: example.com/$id.html 
     *  где $id = идентификатор публикации
     */
    protected function _detailAction() 
    {
        $pageUrl = $this->_getPageUrl();
        
        $pageId = (int)$this->_getParam('%1');
        
        $lang = Module_Lang_Helper::getCurrent();

        $mapper = new Module_Publications_Mapper();
        if ($pageId>0)
            $publication = $mapper->getById($pageId, $lang);
        else 
            $publication = $mapper->getByLink($pageUrl[1], $lang);
        
        if (!$publication) {
            $divisionMapper = new Module_Division_Mapper();
            $divisionId = (int)$divisionMapper->getByLink($pageUrl[1]);
            if ($divisionId > 0) { // у нас есть такой раздел
                $params = array();
                $params['filter']['division'] = $divisionId;
                $params['maxWords'] = Dante_Lib_Config::get('news.previewMaxWords');
                
                $mapper = new Module_Publications_Mapper();
                $list = $mapper->getList($params, $lang);
                //var_dump($list);
                if (!$list) $list = array();
                
                // загрузить инфу по разделу
                $division = $divisionMapper->getById($divisionId);
                $this->_setSeo($division->title, $division->description, $division->keywords);
                $view = new Module_Publications_View_List();
                return $view->draw($list, $division);
            }
        }
        
        if (!$publication) return $this->_defaultAction();        
        
        if(!Dante_Helper_App::getUid() && $publication->auth_only == 1)
            return 'Информация доступна только для зарегистрированных пользователей';
            
        $tpl = new Dante_Lib_Template();
        $tpl->publication = $publication;
        $this->_setSeo($publication->title, $publication->description, $publication->keywords);
        
        $entity = Module_Entity_Helper::getBySysName('publication');
        
        if (Dante_Lib_Config::get('publication.allowComments')) {
            $commentController = new Module_Comment_Controller_Comment();
            $tpl->comments = $commentController->draw($publication->id, $entity->id);
        }
        
        if(Dante_Lib_Config::get('publication.allowGallery'))
        {            
            $gallMapper = new Module_Gallery_Mapper();
            $gallery = $gallMapper->getByEntity($entity->id, $publication->id); 
            $tpl->gallery = $gallery;
        }
        
        $wsName = Dante_Helper_App::getWsName(); 
        $tplFilePath = '../module/publications/template/detail.html';
        $custom = "template/$wsName/publications/detail.html";
        if (file_exists($custom)) $tplFilePath = $custom;

        $tpl = $this->_beforeDetailRendered($tpl);

        return $tpl->draw($tplFilePath);
    }

    protected function _beforeDetailRendered(Dante_Lib_Template $tpl) {
        return $tpl;
    }

    /**
     * Определяем SEO странице
     * @param string $title
     * @param string $description
     * @param string $keywords
     */
    protected function _setSeo($title=false, $description=false, $keywords=false){
        
        if($title)Dante_Lib_Config::set('seo.draw.title', $title);
        if($description)Dante_Lib_Config::set('seo.draw.description', $description);
        if($keywords)Dante_Lib_Config::set('seo.draw.keywords', $keywords);
    }
}
