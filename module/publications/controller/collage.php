<?php



/**
 * Коллаж для публикаций
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Publications_Controller_Collage extends Dante_Controller_Base{
    
    protected function _defaultAction() {
        $url = Dante_Lib_Config::get('publication.url');
        $image = '';
        if ($url) {       
            $mapper = new Module_Publications_Mapper();
            $model = $mapper->getByLink($url, Module_Lang_Helper::getCurrent());
            if ($model) {
                $image = $model->image;
            }
        }
        $html = "<div id=\"contentImage\">";
        if ($image != '') {
            $html .= "<img src=\"media/publications/logos/{$image}\">";
        }
        $html .= "</div>";
        return $html;
    }
}

?>
