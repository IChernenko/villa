<?php



/**
 * Вьюха для списка публикаций
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Publications_View_List {
    
    public function draw($list, $division=false) {
        $tpl = new Dante_Lib_Template();
        $tpl->news = $list;
        $tpl->division = $division;
        $ws = Dante_Helper_App::getWsName();
        $fileName = '../module/publications/template/list.html';
        if (file_exists("template/{$ws}/publications/list.html")) {
            $fileName = "template/{$ws}/publications/list.html";
        }
        return $tpl->draw($fileName);
    }
    public function drawArhive($list, $division=false, $paginator=false) {
        $tpl = new Dante_Lib_Template();
        $tpl->news = $list;
        $tpl->division = $division;
        $tpl->paginator = $paginator;
        $ws = Dante_Helper_App::getWsName();
        $fileName = '../module/publications/template/listarhive.html';
        if (file_exists("template/{$ws}/publications/listarhive.html")) {
            $fileName = "template/{$ws}/publications/listarhive.html";
        }
        return $tpl->draw($fileName);
    }
}
?>
