<?php

// правила роутинга для публикаций

$url = Dante_Controller_Front::getRequest()->get_requested_url();

function publicationLangCallback($params) {
    Dante_Lib_Config::set('publication.url', $params['pageUrl']);
}

$publicationsControllerClassName = 'Module_Publications_Controller';

// Дадаим возможность в проекте перекрыть контроллер публикаций
if (file_exists('app/publications/controller.php')) {
    $publicationsControllerClassName = ucfirst(Dante_Helper_App::getProjectFolder()).'_App_Publications_Controller';
    //var_dump($publicationsControllerClassName); die();
}

//var_dump(Dante_Helper_App::getProjectFolder());

$langList = Module_Lang_Mapper::getCodesList();
if (preg_match_all("/^\/([a-z]{2,3})\/([a-zA-Z0-9\-\_]+)\.html$/", $url, $matches)) {
    $langCode = $matches[1][0];
    if (in_array($langCode, $langList)) {
        Dante_Lib_Router::addRule("/^\/([a-z]{2,3})\/([a-zA-Z0-9\-\_]+)\.html$/", array(
            'controller' => $publicationsControllerClassName,
            'params' => array('pageUrl'=>$matches[2][0].'.html'),
            'callback' => 'publicationLangCallback'
        ));
    }
}

/* deprecated!
Dante_Lib_Router::addRule('/^\/(\d+)\.html$/', array(
    'controller' => 'Module_Publications_Controller',
    'action' => 'detail',
    'params' => array('id'=>'%1')
));*/


Dante_Lib_Router::addRule("/^\/([0-9]+)\-(.*)\.html$/", array(
    'controller' => $publicationsControllerClassName,
    'action' => 'detail'
));



Dante_Lib_Router::addRule("/^\/([a-zA-Z0-9\-\_\:]+)\.html$/", array(
    'controller' => $publicationsControllerClassName,
    'action' => 'detail',
    'params' => array('url'=>'%1')
));

Dante_Lib_Router::addRule("/^\/(.*)\.html\/$/", array(
    'controller' => $publicationsControllerClassName,
    
));
Dante_Lib_Router::addRule("/^\/manage\/publications\/?.*$/", array(
    'controller' => 'Module_Publications_Manage_Controller',
    'action'=>'draw',
));
?>