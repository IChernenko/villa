<?php



/**
 * Description of helper
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Publications_Helper {
    
    public static function truncate($text, $maxWordsCount, $endSuffix=false) {
        $words = explode(' ', $text);
        if (count($words) > $maxWordsCount) {
            $text = '';
            for($i=0; $i<$maxWordsCount; $i++) {
                $text .= $words[$i].' ';
            }
        }
        
        return $text;
    }
}

?>
