<?php

class Module_Publications_Observer_Search implements Dante_Lib_Observer_Iobserver
{
    public function notify($model, $strEventType) 
    {
        $langId = Module_Lang_Helper::getCurrent();
        
        $sql = "select page_id from [%%]publications_lang where page_title like '%{$model->request}%' 
        or page_content like '%{$model->request}%'";
        
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        $pages = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
            $pages[] = (int)$f['page_id'];
        }
        
        if (count($pages) != 0) 
        {
            $pageIds = implode(',', $pages);

            $sql = "SELECT p.id, p.url, pl.page_title
                    FROM publications AS p LEFT JOIN publications_lang AS pl ON
                    (p.id = pl.page_id AND pl.lang_id = {$langId})
                    where p.id in ({$pageIds})";
            $items = array();
            $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
            while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) {
                $items[] = array(
                    'link' => $f['id'].'-'.$f['url'],
                    'title' => $f['page_title']
                );
            }

            $model->result['publications'] = $items;
        }
    }
}

?>
