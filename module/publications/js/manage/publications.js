publications = {
    controller:"module.publications.manage.controller",
    edit: function(obj, closed){        
        publications.performer(obj);
        $("textarea.editor").elrte('updateSource');
        var data=$('#editPublication').serialize(); //console.log(data); return 0;
        $.ajax({
            type: 'POST',
            data: data,
            url: '/ajax.php'+(!closed?'?returntype=1':''),
            dataType : "json",
            success: function (data){
                if(data.result==1)
                {
                    if(closed)
                    {
                        $('#modalWindow').arcticmodal('close');
                        handbooks.formSubmit();
                    }
                    else
                    {                        
                        $('#modalWindow .modal-body').html(data.html);
                    }                    
                }
                else
                {
                    //console.log(data);
                    alert(data.message);
                }
            }
        });
    },
    
    imgEdit: function(id)
    {
        if(handbooks.buttonsDisabled())return false;
        data={
            controller  : this.controller,
            action      :'drawImgForm',
            id          :id
        }
        $('#modalWindow').arcticmodal({
            type: 'ajax',
            url: 'ajax.php',
            ajax: {
                type:'get',
                dataType:"json",
                data:data,
                success:function(data,el,responce){
                    data.body.html(el);
                    $(el).find('.modal-body').html(responce.html);
                    handbooks.buttonsEnabled();
                }
            },
            afterClose: function(data, el) {                
                handbooks.formSubmit();
            }
        });
    },
    
    imgSaveGallery: function(url)
    {
        var data = {
            controller : this.controller,
            action :'saveGallery',
            id: $('[name=id]').val(),
            url: url
        };
        dante.ajaxContentLoadStart('td.download');
        $.ajax({
            type: 'POST',
            data: data,
            url: '/ajax.php',
            dataType : "json",
            success: function (data){
                if(data.result==1)
                {                
                    $('#modalWindow .modal-body').html(data.html);                   
                }
                else
                {
                    alert(data.message)
                }
            }
        });
    },
    
    imgDel: function(object, id)
    {
        var data = {
            controller : this.controller,
            action :'delImage',
            id: id
        }; 
        dante.ajaxContentLoadStart($(object).parent('td'));
        $.ajax({
            type: 'POST',
            data: data,
            url: '/ajax.php',
            dataType : "json",
            success: function (data){ 
                if(data.result==1)
                {                   
                    var tr = '#'+id;
                    $(tr).remove();
                }
                else
                {
                    alert(data.message)
                }
            }
        });
    },
    
    addComment: function(object, id)
    {
        this.disableBtn(object);
        var comment = $(object).prev('textarea').val();
        if(!comment) 
        {
            alert('Вы не ввели комментарий!');
            return false;
        }
        var data = {
            controller : this.controller,
            action :'imgComment',
            id: id,
            comment: comment
        };
        dante.ajaxContentLoadStart($(object).next('p'));
        $.ajax({
            type: 'POST',
            data: data,
            url: '/ajax.php',
            dataType : "json",
            success: function (data){
                publications.enableBtn(object);
                if(data.result==1)
                {
                    $(object).next('p').text('Ваш комментарий сохранен!');
                }
                else
                {
                    alert(data.message)
                }
            }
        });
    },
    
    editEnd: function()
    {
        $('#modalWindow').arcticmodal('close');
    },
    
    disableBtn: function(object)
    {
        $(object).prop({'disabled':true});
    },
    
    enableBtn: function(object)
    {
        $(object).prop({'disabled':false});
    },
    
    performer: function(obj){
        $(obj).removeClass('btn-success btn-info btn-normal btn-danger btn-warning').text('\u0412ыполняю');
    },
    
    displayImages: function (id){
        //document.getElementById('imagesEditForm').innerHTML = ''+
        //'Изображения:'+
               // '<div id="images_html"></div>'+
                //'<br><br><br>'+
                //'Загрузить новое изображение:<br>'+
                //'<div id="uploadButton"></div>'+
                //'<div id="status"></div>'+
        //'';
        
        var typeId = id;

        swfUploadUrl = hostUrl+"/ajax.php?action=upload&controller=module.publications.manage.controller&q=1&ctype=html&typeId="+typeId;

        params = {
            uploadUrl:swfUploadUrl,
            uploadButton: "../../../../component/swfupload/button.png",
            uploadComplete: publications.uploadCallBack
            //typeId: typeId
        }
        delete swfu;

        initSwfUpload(params);
    },
    changeLang:function(){
        data={            
            lang_id:$('#lang_id').val(),
            id:$('#publication_id').val(),
            controller:"module.publications.manage.controller",
            action:'drawForm'
        }
        $.ajax({
            type: 'POST',
            data:data,
            url: '/ajax.php'+(!closed?'?returntype=1':''),
            dataType : "json",
            success: function (data){
                if(data.result==1){
                    if(closed){
                        $('#modalWindow').arcticmodal('close');
                        handbooks.formSubmit();
                    }else{                        
                        $('#modalWindow .modal-body').html(data.html);
                    }                    
                }else{
                    alert(data.message)
                }
            }
        });
    },
    load_elfinder:function (id) {
        
        $('<div id="myelfinder"></div>').dialogelfinder({
            url: 'ajax.php?controller=lib.elfinder.controller&folder=publications',
            commandsOptions: {
              getfile: {
                oncomplete: 'destroy' // destroy elFinder after file selection
              }
            },
            getFileCallback: function(url){
                $('#'+id).val(url);
                $('#prev_'+id).attr({src:url});
            } // pass callback to file manager
          });
        return false;
    },
    load_elfinder_gallery:function (id) {
        
        $('<div id="myelfinder"></div>').dialogelfinder({
            url: 'ajax.php?controller=lib.elfinder.controller&folder=publications',
            commandsOptions: {
              getfile: {
                oncomplete: 'destroy' // destroy elFinder after file selection
              }
            },
            getFileCallback: function(url){
                publications.imgSaveGallery(url);
            } // pass callback to file manager
          });
        return false;
    }       
}