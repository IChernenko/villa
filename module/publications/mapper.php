<?php
/**
 * Маппер публикаций
 * 
 * @author dorian
 */
class Module_Publications_Mapper
{
    protected $_tableName = '[%%]publications';
    protected $_tableNameLang = '[%%]publications_lang';

    /**
     * Получение публикации по имени
     * @param string $name имя страницы
     * @param int $lang код языка
     * @return Module_Publications_Manage_Model
     */
    public function getByName($name, $lang) {
        $sql = "select
                    p.name,
                    p.auth_only,
                    pl.page_id,
                    pl.page_title,
                    pl.page_content,
                    p.image
                from [%%]publications as p
                left join [%%]publications_lang as pl on (pl.page_id = p.id and pl.lang_id = {$lang})
                where p.name = '{$name}'";

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        if (Dante_Lib_SQL_DB::get_instance()->getNumRows($r) == 0) return false;

        $f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r);

        $model = new Module_Publications_Manage_Model();
        $model->name = $f['name'];
        $model->auth_only = $f['auth_only'];
        $model->id = (int)$f['page_id'];
        $model->page_title = $f['page_title'];
        $model->page_content = $f['page_content'];

        return $model;
    }
    
    public function getByLink($link, $lang) {
        $sql = "select
                    p.name,
                    p.auth_only,
                    pl.page_id,
                    pl.page_title,
                    pl.page_content,
                    p.image,
                    pl.title as seoTitle,
                    pl.keywords as seoKeywords,
                    pl.description as seoDescription
                from {$this->_tableName} as p
                left join {$this->_tableNameLang} as pl on (pl.page_id = p.id and pl.lang_id = {$lang})
                where p.url = '{$link}' AND `pl`.`page_title` != '' AND `pl`.`page_content` != ''";
                //echo($sql);
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        if (Dante_Lib_SQL_DB::get_instance()->getNumRows($r) == 0) return false;
        
        $f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r);

        $model = new Module_Publications_Manage_Model();
        $model->name = $f['name'];
        $model->id = (int)$f['page_id'];
        $model->auth_only = $f['auth_only'];
        $model->page_title = $f['page_title'];
        $model->page_content = $f['page_content'];
        $model->image = $f['image'];
        $model->title = $f['seoTitle'];
        $model->keywords = $f['seoKeywords'];
        $model->description = $f['seoDescription'];
        return $model;
    }
    
    public function getById($id, $lang, $params=array()) {
        $sql = "select
                    p.name,
                    p.auth_only,
                    pl.page_id,
                    pl.page_title,
                    pl.page_content,
                    p.uid,
                    pl.title as seoTitle,
                    pl.keywords as seoKeywords,
                    pl.description as seoDescription
                from [%%]publications as p
                left join [%%]publications_lang as pl on (pl.page_id = p.id and pl.lang_id = {$lang})
                WHERE `p`.`id` = {$id} AND `pl`.`page_title` != '' AND `pl`.`page_content` != ''";
                        
        // добавим фильтрацию если она нужна        
        if (isset($params['filter'])) 
        {        
            if(count($params['filter'] == 1)) $sql .= ' AND ';
            $sql .= Dante_Lib_Sql_Builder_Base::buildConditions($params['filter'], false);
        }                 

        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        if (Dante_Lib_SQL_DB::get_instance()->getNumRows($r) == 0) return false;

        $f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r);

        $model = new Module_Publications_Manage_Model();
        $model->name = $f['name'];
        $model->id = (int)$f['page_id'];
        $model->auth_only = $f['auth_only'];
        $model->page_title = $f['page_title'];
        
        $model->page_content = $f['page_content'];
        $model->uid = (int)$f['uid'];
        $model->title = $f['seoTitle'];
        $model->keywords = $f['seoKeywords'];
        $model->description = $f['seoDescription'];

        return $model;
    }
    
    

    /**
     * Добавление публикации
     * @param Module_Publications_Manage_Model $publication 
     */
    public function add(Module_Publications_Manage_Model $publication, $langId = 1) {
        $dmo = new Module_Publications_Dmo();
        if ($publication->type) $dmo->publication_type = $publication->type;
        if ($publication->uid) $dmo->uid = $publication->uid;
        if ($publication->url) $dmo->url = $publication->url;
        if ($publication->division) $dmo->division = $publication->division;
        $dmo->name = '';
        $dmo->date = time();
        $dmo->time = 0;
        $publicationId = (int)$dmo->insert();
        if ($publicationId == 0) throw new Exception("cant insert publication : ".$dmo->getSql());
        
        $langDmo = new Module_Publications_Dmo_Lang();
        $langDmo->page_id = $publicationId;
        $langDmo->lang_id = $langId;
        $langDmo->page_title = $publication->page_title;
        $langDmo->page_content = $publication->page_content;
        $langDmo->insert();

        return $publicationId;
    }
    
    public function update(Module_Publications_Manage_Model $publication, $langId = 1) {
        $dmo = new Module_Publications_Dmo();
        if ($publication->type) $dmo->publication_type = $publication->type;
        if ($publication->uid) $dmo->uid = $publication->uid;
        if ($publication->url) $dmo->url = $publication->url;
        $dmo->id = $publication->id;
        $dmo->date = time();
        $dmo->apply();
        
        $langDmo = new Module_Publications_Dmo_Lang();
        $langDmo->page_title = $publication->page_title;
        $langDmo->page_content = $publication->page_content;
        $langDmo->apply(array(
            'page_id' => $publication->id,
            'lang_id' => $langId
        ));
    }
    
    /**
     * Получение списка публикаций
     * 
     * @param type $params
     * @param type $lang
     * @return array of Module_Publications_Manage_Model 
     */
    public function getList($params = array(), $lang = 1) {
        $sql = "select
                    p.id,
                    p.name,
                    p.image,    
                    p.url,
                    p.date,
                    p.time,
                    p.uid,
                    p.division,
                    pl.page_id,
                    pl.page_title,
                    pl.page_content,
                    pl.page_preview
                from {$this->_tableName} as p
                left join {$this->_tableNameLang} as pl on (pl.page_id = p.id and pl.lang_id = {$lang}) 
                WHERE `pl`.`page_title` != '' AND `pl`.`page_content` != ''";
                
        // добавим фильтрацию если она нужна        
        if (isset($params['filter'])) 
        {        
            if(count($params['filter'] == 1)) $sql .= ' AND ';
            $sql .= Dante_Lib_Sql_Builder_Base::buildConditions($params['filter'], false);
        }         
        
        if(!Dante_Helper_App::getUid())
            $sql .= " AND `p`.`auth_only` = 0";
                
        $sql .= " order by p.date desc";
        if(isset($params['limit'])){
            $sql .= ' '.$params['limit'];
        }
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        if (Dante_Lib_SQL_DB::get_instance()->getNumRows($r) == 0) return false;

        $list = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) 
        {
            if (isset($params['maxWords']))
                $f['page_content'] = Module_Publications_Helper::truncate($f['page_content'], $params['maxWords']);
            
            $model = new Module_Publications_Manage_Model();
            $model->id              = $f['id'];
            $model->name            = $f['name'];
            $model->page_title      = $f['page_title'];
            $model->page_content    = $f['page_content'];
            $model->page_preview    = $f['page_preview'];
            if ($f['image'] != '') {
                $model->image = Dante_Lib_Config::get("publication.imgDir").$f['image'];
            }

            $model->url             = $f['url'];
            $model->date            = $f['date'];
            $model->humanDate       = Dante_Helper_Date::converToDateType($f['date']);
            $model->time            = $f['time'];
            $model->humanTime       = Dante_Helper_Date::converToDateType($f['time'], false, 'H:i');
            $model->division        = (int)$f['division'];
            $model->uid             = (int)$f['uid'];

            $list[] = $model;
        }
        
        return $list;
    }
    /**
     * КОличество записей по выбранному фильтру
     * @param type $params
     */
    public function getCountList($params, $lang)
    {
        $sql = "SELECT COUNT(*) AS `count` FROM {$this->_tableName} AS `p`
                LEFT JOIN {$this->_tableNameLang} AS `pl` ON (`pl`.`page_id` = `p`.`id` and `pl`.`lang_id` = {$lang}) 
                WHERE `pl`.`page_title` != '' AND `pl`.`page_content` != ''";
                
        if (isset($params['filter']))     
        {
            if(count($params['filter'] == 1)) $sql .= ' AND ';
            $sql .= Dante_Lib_Sql_Builder_Base::buildConditions($params['filter'], false);
        }
        
        return Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'count');        
    }
    public function getByUid($uid, $lang = 1) {
        return $this->getList(array(
            'filter' => array('uid' => $uid)
        ), $lang);
    }
    
    
}
