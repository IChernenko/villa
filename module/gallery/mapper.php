<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of mapper
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Gallery_Mapper {
    
    protected $_tableName = '[%%]gallery';
    
    public function getByUid($uid) {
        return Module_Gallery_Dmo::dmo()->byUid($uid)->fetchAll('id');
    }
    
    public function getByEntity($entType, $entId)
    {
        return Module_Gallery_Dmo::dmo()->byEntity($entType, $entId)->fetchAll('id');
    }
    
    public function getFileName($id)
    {
        return Module_Gallery_Dmo::dmo()->byId($id)->fetch()->file_name;
    }


    public function addFile($model)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        
        $table->entity_type_id = $model->entity_type_id;
        $table->entity_id = $model->entity_id;
        $table->uid = $model->uid;
        $table->file_name = $model->file_name;
        $table->adding_date = $model->adding_date;
        
        $id = $table->insert();
        
        return $id;
    }
    
    public function addComment($model)
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->comment = $model->comment;
        
        $table->update(array('id'=>$model->id));
        return $model->id;
    }


    public function fileDelete($id) 
    {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->delete(array('id' => $id));
    }
}

?>
