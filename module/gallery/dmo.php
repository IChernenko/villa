<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dmo
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Gallery_Dmo extends Dante_Lib_Orm_Table{
    
    protected $_tableName = '[%%]gallery';
    
    /**
     *
     * @return Module_Gallery_Dmo 
     */
    public static function dmo() {
        return self::getInstance();
    }


    public function byId($id) {
        return $this->getByAttributes(array('id'=>$id));
    }
    
    public function byUid($id) {
        return $this->getByAttributes(array('uid'=>$id));
    }
    
    public function byEntity($entType, $entId)
    {
        return $this->getByAttributes(array('entity_type_id'=>$entType, 'entity_id'=>$entId));
    }
    
    public function del($id) 
    {
        return $this->delete($id);
    }
    
    
}

?>
