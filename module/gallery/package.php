<?php
global $package;
$package = array(
    'version' => '2',
    'name' => 'module.gallery',
    'dependence' => array(
        'module.entity',
        'lib.lightbox'
    ),
    'js' => array(
        '../module/gallery/js/gallery.js'
    )
);