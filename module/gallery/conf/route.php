<?php
$url = Dante_Controller_Front::getRequest()->get_requested_url();

Dante_Lib_Router::addRule('/^\/manage\/gallery\/$/', array(
    'controller' => 'Module_Gallery_Manage_Controller'
));