<?php



/**
 * Контроллер управления галереей
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Gallery_Manage_Controller extends Dante_Controller_Base{
    
    protected function _defaultAction() {
        $uid = (int)Dante_Helper_App::getUid();
        if ($uid == 0) throw new Exception("неверный вызов");
        
        $tpl = new Dante_Lib_Template();
        
        $mapper = new Module_Gallery_Mapper();
        $items = $mapper->getByUid($uid);
        
        
        
        $tpl->items = $items;
        return $tpl->draw('../module/gallery/template/manage.html');
    }
    
    /**
     * Сохранение файла
     * @return type 
     */
    protected function _uploadAction() {
        $uid = (int)Dante_Helper_App::getUid();
        if ($uid == 0) throw new Exception("неверный вызов");
        
        if(!isset($_FILES['Filedata'])) throw new Exception("нечего загружать");
        
        $uploadFolder = Dante_Lib_Config::get('app.uploadFolder');
        
        
        
        $uploadDir = "{$uploadFolder}/{$uid}/gallery";
        if (!is_dir($uploadDir)) {
            mkdir($uploadDir, 0755, true);
            //@chmod($uploadDir, 0755);
        }

        $uploader = new Component_Swfupload_Upload();
        $fileName = $uploader->upload($uploadDir.'/');

        $ext = pathinfo($fileName, PATHINFO_EXTENSION);

        //ресайз начало
        $newname = $uploadDir.'/'.$fileName;
        $image = new Lib_Image_Driver();
        $image->load($newname);

        $widthTemp = $image->getWidth();
        $heightTemp = $image->getHeight();

        $maxWidth = Dante_Lib_Config::get('gallery.maxImageWidth');

        if($widthTemp > $maxWidth){
            Dante_Lib_Log_Factory::getLogger()->debug("resize to width : $maxWidth");
            $image->resizeToWidth($maxWidth);
        }elseif($heightTemp > Dante_Lib_Config::get('gallery.maxImageHeight')){
            $image->resizeToHeight(Dante_Lib_Config::get('gallery.maxImageHeight'));
            Dante_Lib_Log_Factory::getLogger()->debug("resize to height");
        }

        Dante_Lib_Log_Factory::getLogger()->debug("before save $newname");
        $fileName = time().'.'.$ext;
        $newname = $uploadDir.'/'.$fileName;
        $image->save($newname);
        
        // сохраним картинку
        $dmo = new Module_Gallery_Dmo();
        $dmo->uid = $uid;
        $dmo->file_name = $fileName;
        $dmo->adding_date = time();
        $id = $dmo->insert();
        
        //ресайз конец
        
        return array(
            'result' => 1,
            'uid' => $uid,
            'file' => $fileName,
            'id' => $id
        );
    }
    
    protected function _removeAction() {
        $uid = (int)Dante_Helper_App::getUid();
        if ($uid == 0) throw new Exception("неверный вызов");
        
        $id = (int)$this->_getRequest()->get('id');
        $dmo = new Module_Gallery_Dmo();
        $dmo->delete(array('id'=>$id, 'uid'=>$uid));
        return array(
            'result' => 1,
            'id' => $id
        );
    }
}

?>
