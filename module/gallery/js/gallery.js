gallery = {
    init: function() {
        var swfUploadUrl = "/ajax.php?action=upload&controller=module.gallery.manage.controller&q=1&ctype=json";

        var params = {
            buttonPlaceholderId:    'btnGalleryUpload',
            uploadUrl:              swfUploadUrl,
            uploadButton:           "../../../../component/swfupload/button.png",
            uploadSuccess: function(obj, data)    {

                data =  JSON.parse(data);
                if (data.result == 1) {
                    gallery.add('media/'+data.uid+'/gallery/'+data.file, data.id);
                }
            }
        }
        initSwfUpload(params);
    },
    
    add: function(img, id) {
        $("#gallery_items").append('<li id="gallery_item_'+id+'"><a href="#"><img src="'+img+'" class="img-polaroid"></a><br/><button class="btn btn-danger" onclick="javascript:gallery.remove('+id+');">Удалить</button></li>');
    },
    
    remove: function(id) {
        $('#gallery_item_'+id).fadeOut();
        $.ajax({
            type: "POST",
            url: '/ajax.php',
            data: {
                controller: "module.gallery.manage.controller",
                action: "remove",
                id: id
            },
            success: function(data) {
                if (data.result == 1) {
                    if (data.id > 0) $('#gallery_item_'+data.id).remove();
                }
            }
        });
    }
}