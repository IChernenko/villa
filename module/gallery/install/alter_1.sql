drop table if exists [%%]gallery;
CREATE TABLE [%%]gallery (
  `id`                  int(11) NOT NULL AUTO_INCREMENT,
  `entity_type_id`      int(11) UNSIGNED,
  `entity_id`           int(11),
  `uid`                 int(11),
  `file_name`           varchar(32),
  `adding_date`         int(11),  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 comment="галерея изображений";

ALTER TABLE [%%]gallery ADD
      CONSTRAINT fk_gallery_entity
      FOREIGN KEY (`entity_type_id`)
      REFERENCES [%%]entity(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE [%%]gallery ADD
      CONSTRAINT fk_gallery_uid
      FOREIGN KEY (`uid`)
      REFERENCES [%%]users(id) ON DELETE CASCADE ON UPDATE CASCADE;
