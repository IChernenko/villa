<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of controller
 *
 * @author Sergey Suzdaltsev <sergey.suzdaltsev@gmail.com>
 */
class Module_Gallery_Controller extends Dante_Controller_Base{
    
    protected function _defaultAction() {
        $uid = (int)Dante_Helper_App::getUid();
        if (!$uid) {
            $login = Radast_App_Helper::getLoginByPath();
            $userMapper = new Module_User_Mapper();
            $uid = $userMapper->getUidByLogin($login);
        }
        //if ($uid == 0) return 'Страница доступна только залогиненому пользователю.';
        
        $tpl = new Dante_Lib_Template();
        
        $mapper = new Module_Gallery_Mapper();
        $items = $mapper->getByUid($uid);
        
        
        
        $tpl->items = $items;
        return $tpl->draw('../module/gallery/template/list.html');
    }
}

?>
