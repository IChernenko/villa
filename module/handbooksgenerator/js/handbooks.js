/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var handbooks={
    id:false,
    url:'ajax.php',
    controller:false,
    action:'drawAjax',
    formid:'editHandbookForm',
    submitFlag:false,
    formSubmit:function(gridid, obj){
        if(handbooks.buttonsDisabled())return false;
        if(handbooks.is_object(gridid)){
            obj=gridid;
            gridid='';
        }else if(!gridid || gridid===undefined){
            gridid='';
        }else{
            gridid='#'+gridid;
        }
        var defText=$(obj).text();
        $(obj).addClass('btn-warning').text('Loading...');
        
        
        var objid=gridid+' #'+handbooks.formid;
        data=$(objid).serializeArray();
        if(!$(objid+' input').is('[name=action]')){
            data.push(                
                {name:'action', value:handbooks.action}
            );
        }
        if(!$(objid+' input').is('[name=controller]')){
            data.push(                
                {name:'controller', value:handbooks.controller}
            );
        }
        if(!$(objid+' input').is('[name=id]') && handbooks.id){
            data.push(                
                {name:'id', value:handbooks.id}
            );
        }
        $.ajax({
            url:handbooks.url,
            type:'post',
            dataType:"json",
            q:1,
            data:data,
            success:function(data){
                handbooks.buttonsEnabled();
                if(data.result==1){
                    grid=$(objid).parent().html(data.html);
                    $(obj).removeClass('btn-warning').text(defText);
                    var value = jQuery( "input" ).val();
                    if ( value == ( "/manage/currencytowns/") ) {
                    tsorter.create('handbookTable');
                    }
                }else{
                    jAlert(data.message, 'Ошибка');
                }
                if(data.eval){
                   eval(data.eval);
                }
            }
        })
        
    },
    sortable:function(column, type, obj){        
        var form=$(obj).parent().parent().parent().parent().parent().parent();
        form.find('#sortColumn').val(column);
        form.find('#sortType').val(type);
        handbooks.formid=form.attr('id');
        handbooks.formSubmit();
    },
    clearFilters:function(obj){
        z=$(obj).parent().parent().parent().find('input, textarea, select');
        z.val('');
        handbooks.formSubmit();
    },
    changeCheckbox:function(obj){
        n=$("#checkAll:checked").length;
        if(n>0){
            attr=true;
        }else{
            attr=false;
        } 
        
        $('.editCheckbox:enabled').attr('checked',attr)
    },
    is_object:function(mixed_var){
        if (Object.prototype.toString.call(mixed_var) === '[object Array]') {
        return false;
      }
        return mixed_var !== null && typeof mixed_var == 'object';
    },
    edit:function(id){
        if(handbooks.buttonsDisabled())return false;
        data={
            controller  : handbooks.controller,
            action      : 'drawForm',
            id          : id,
            page_url    : handbooks.page_url
        }
        $('#modalWindow').arcticmodal({
            type: 'ajax',
            url: 'ajax.php',
            ajax: {
                type:'get',
                dataType:"json",
                q:1,
                data:data,
                success:function(data,el,responce){
                    data.body.html(el);
                    $(el).find('.modal-body').html(responce.html);
                    handbooks.buttonsEnabled();
                }
            },
            afterClose: function(data, el) {                
                handbooks.formSubmit();
            }
        });
    },
    
    del:function(id)
    {
        if(handbooks.buttonsDisabled())return false;
        jConfirm('Вы действительно хотите удалить запись без возможности восстановления?', 'Подтверждение', 
        function(result){
            if(!result) 
            {
                handbooks.buttonsEnabled();
                return false;
            }
            else
            {
                $('#gridRefresh').addClass('btn-warning').text('Loading...');
                data={
                    controller  :handbooks.controller,
                    action      :'del',
                    id          :id,
                    page_url    :$('#page_url').val()
                }
                $.ajax({
                    url:handbooks.url,
                    type:'post',
                    dataType:"json",
                    q:1,
                    data:data,
                    success:function(data){
                        handbooks.buttonsEnabled();
                        if(data.result==1){
                            handbooks.formSubmit();
                        }else{                    
                            jAlert(data.message, 'Ошибка');
                        }                
                    }
                })
            }
        });
    },
    active:function(id, value){
        if(handbooks.buttonsDisabled())return false;              
        data={
            controller  :handbooks.controller,
            action      :'active',
            id          :id,
            value       :value
        }
        $.ajax({
            url:handbooks.url,
            type:'post',
            dataType:"json",
            q:1,
            data:data,
            success:function(data){
                handbooks.buttonsEnabled();
                if(data.result==1){
                    handbooks.formSubmit();
                }else{                    
                    jAlert(data.message, 'Ошибка');
                }                
            }
        })
    },    
    buttonsDisabled:function(){        
        if(handbooks.submitFlag)return true;
        handbooks.submitFlag=true;
        $('#gridRefresh').addClass('btn-warning').text('Loading...');  
        $('.btn').addClass('disabled');
        return false;
    },
    buttonsEnabled:function(){
        handbooks.submitFlag=false;
        $('#gridRefresh').removeClass('btn-warning').text('Обновить');
        $('.btn').removeClass('disabled');
    }
}