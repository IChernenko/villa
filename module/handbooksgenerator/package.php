<?php
global $package;
$package = array(
    'version' => '2',
    'name'=>'module.handbooksgenerator',
    'dependence' => array(
        'lib.paginator',
        'lib.jquery.arcticmodal',
        'component.jalerts',
        'module.lang',
        'module.labels'
    ),
    'js' => array(
        '../module/handbooksgenerator/js/handbooks.js',
        //'../module/handbooksgenerator/js/webforms2.js',
        
    ),
    'css' => array(
        '../module/handbooksgenerator/css/handbooks.css',
        
    )
);