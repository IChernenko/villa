<?php
/**
 * Генератор таблиц для справочников и не только
 * @author Elrafir
 * Полное описание с примерами немного позже добавлю
 */
abstract class Module_Handbooksgenerator_Controller_Base extends Dante_Controller_Base
{
    /**
     * Массив с параметрами доступа, true - если они не заданы, false - право доступа отсутствует вообще.
     * @var array || boolean
     */
    protected $_access = NULL;

    /**
     * Язык лейблов. Для админки - дефолтный из конфига, для клиента - текущий
     * @var int
     */
    protected $_lang = 1;

    /**
     * Массив лейблов
     * @var array
     */
    protected $_labels = NULL;

    /**
     * Маппер класса наследника.
     * @var type
     */
    protected $_mapper = false;

    /**
     * Заглавие таблицы с параметрами.
     * @var mixed
     */
    public $tableParams=false;
    /**
     * Перечень инициализируемого JS-функционала
     * @var array
     */
    protected $_eval=array();
    /**
     * Количество ячеек в строке , если false то по умолчанию назначается количество из первого обрабатываемого тега thead|tbody|tfoot присланного в в виде массива. При этом учитывается размер массива + colspan указанный впараметрах
     * @var int|false
     */
    public $cell_in_row=false;

    /**
     * Содержимое тега <thead></thead>
     * @var string|mixed
     */
    public $thead='';

    /**
     * Содержимое тега <tbody></tbody>
     * @var string|mixed
     */
    public $tbody=false;
    /**
     * Содержимое тега <tfoot></tfoot>
     * @var string|mixed
     */
    public $tfoot='';

    /**
     * Содержимое тега <caption><caption>
     * @var string
     */
    public $caption='';

    /**
     * Несёт информацию о добавлении формы в грид. Назначается методом formEnable
     * @var array
     */
    protected $_gridform=false;
    /**
     *  Данные по  сортировке в gride. Обрабатываются жинамически и используются пи генерации html для соответствующих инпутов
     *  @var false|string название поля по которому будет сортировка производится
     */
    protected $_sortable=array(
        'column'=>false,
        'type'=>'asc'
    );
    /**
     * Определяет будет ли открыватся модальноеокно на странице. Если да то будет добавлен контейнер для него.
     * @var boolean
     */
    public $modal=false;
    /**
     * Номера клонок которые нужно проигнорировать и не выводить
     * @var int порядковый номер колонки
     */
    public $kill=array();
    /**
     * Устанавливаем дефолтные данные.
     */
    protected function _init() {
        parent::_init();
        $this->clear();
    }

    protected abstract function _drawAction();

    /**
     * Стандартный очень часто используемый метод для контроллеров наследников.
     * При обращении к контроллеру без указания action используется _drawAction
     * @return string|mixed
     */
    protected function _defaultAction(){
        $this->_setAccess();
        return $this->_drawAction();
    }
    /**
     * Стандартный очень часто используемый метод для контроллеров наследников.
     * КОнвертация отображения грида в вариант для ajax запросов
     * @return array from ajax
     */
    protected function _drawAjaxAction(){
        $url = $this->_getRequest()->get('page_url');
        $this->_setAccess($url);
        try{
            return array(
                'result'=>1,
                'html'=>$this->_drawAction()
            );
        }catch(Exception $e){
            return array(
               'result'=>0,
               'message'=> $e->getMessage()
            );
        }
    }



    protected function _setAccess($url = false)
    {
        if(is_null($this->_access))
        {
            $helper = new Module_Division_Helper();
            $this->_access = $helper->checkAccess($url);
            $this->formEnable(array(
                'page_url'=>$url?$url:$this->_getRequest()->get_requested_url()
            ));
        }
    }

    protected function _setLang()
    {
        $ws = Dante_Helper_App::getWsName();
        if($ws == 'admin')
        {
            $code = Dante_Lib_Config::get('ws.admin.lang');
            if($code)
            {
                $this->_lang = Module_Lang_Mapper::getByCode($code);
                Dante_Lib_Session::set('lang', $this->_lang);
            }
        }
        else
        {
            $this->_lang = Module_Lang_Helper::getCurrent();
        }

        $this->_setLabels();
    }

    protected function _setLabels()
    {
        $this->_labels = array(
            'add' => Module_Lang_Helper::translate('sys_add'),
            'del' => Module_Lang_Helper::translate('sys_del'),
            'edit' => Module_Lang_Helper::translate('sys_edit'),
            'activ' => Module_Lang_Helper::translate('sys_activ'),
            'deactiv' => Module_Lang_Helper::translate('sys_deactiv'),
            'sortAsc' => Module_Lang_Helper::translate('sys_sortAsc'),
            'sortDesc' => Module_Lang_Helper::translate('sys_sortDesc'),
            'all' => Module_Lang_Helper::translate('sys_all'),
            'filter' => Module_Lang_Helper::translate('sys_filter'),
            'months' => array(
                'jan' => Module_Lang_Helper::translate('sys_jan'),
                'feb' => Module_Lang_Helper::translate('sys_feb'),
                'mar' => Module_Lang_Helper::translate('sys_mar'),
                'apr' => Module_Lang_Helper::translate('sys_apr'),
                'may' => Module_Lang_Helper::translate('sys_may'),
                'jun' => Module_Lang_Helper::translate('sys_jun'),
                'jul' => Module_Lang_Helper::translate('sys_jul'),
                'aug' => Module_Lang_Helper::translate('sys_aug'),
                'sep' => Module_Lang_Helper::translate('sys_sep'),
                'oct' => Module_Lang_Helper::translate('sys_oct'),
                'nov' => Module_Lang_Helper::translate('sys_nov'),
                'dec' => Module_Lang_Helper::translate('sys_dec')
            ),
            'days' => array(
                'mon' => Module_Lang_Helper::translate('sys_mon'),
                'tue' => Module_Lang_Helper::translate('sys_tue'),
                'wed' => Module_Lang_Helper::translate('sys_wed'),
                'thu' => Module_Lang_Helper::translate('sys_thu'),
                'fri' => Module_Lang_Helper::translate('sys_fri'),
                'sat' => Module_Lang_Helper::translate('sys_sat'),
                'sun' => Module_Lang_Helper::translate('sys_sun')
            )
        );
    }

    /**
     *
     * @param string|array $params - массив свойств формы грида
     * Если переменная строковая она присваевается полю controller
     * Если контроллер не указан вообще, то форма не добавляется
     * Возможные значения
     * controller   = контроллер вида module.handbooksgenerator.controller.base
     * action       = экшн в контроллере (НЕ экшн формы). По умолчанию drawAjax
     * url          = url обработчика на сервере. По умолчанию ajax.php
     * page_url     = url страницы. Нужен для правильной работы защиты доступа
     * id           = не стандартный id формыю По умолчанию #editHandbookForm
     * name         = name формы. По умолчанию отсутсвует
     * @return type
     */
    public function formEnable($params){
        if(is_string($params)){
            $this->_gridform['controller']=$params;
            return;
        }
        foreach($params as $k=>$v){
            $this->_gridform[$k]=$v;
        }
    }

    /**
     *
     * @param string|mixed $tbody
     * @param string|mixed $tableParams
     * @param string|mixed $thead
     * @param string|mixed $tfoot
     * @param string $caption - необязательная строка загдавия таблицы . Если текст передан то выводится в формате <caption>$caption</caption>
     * @return string - возвращает сгенерированный HTML с таблицей стравочником.
     */
    public function generate()
    {
        if($this->_labels == NULL) $this->_setLang();

        $tpl = new Module_Handbooksgenerator_View_Table();

        $html['strParams']=$this->_setParams($this->tableParams);

        $html['caption']=$this->_getRows($this->caption);
        if(!isset($this->thead['tag'])){
            $tag='th';
        }else{
            $tag='td';
        }
        $html['thead']      =  $this->_getRows($this->thead, $tag);
        $html['tbody']      =  $this->_getRows($this->tbody);
        $html['tfoot']      =  $this->_getRows($this->tfoot);
        $html['form']       =  $this->_gridform;
        $html['gridSortable']= $this->_sortable;
        $eval=false;
        if(isset($this->_eval) && $this->_eval){
            $eval=implode(';', $this->_eval);
        }
        $html['eval']=$eval;
        $result=$tpl->draw($html);
        if($this->modal){
            $modal = new Lib_Jquery_Arcticmodal_Controller;
            $result.=$modal->draw('modalWindow');
        }
        $this->clear();
        return $result;
    }

    /**
     * Предустановленные типы параметров style, class, id. Остальные параметры будут перечисленны в формате key='val1 val2 ...'
     * @param array|string $params -параметры
     * @return string
     */
    protected function _setParams($params){
        $str=array();
        if($params){
            if(is_array($params)){
                //передали массив
                foreach ($params as $keyStr => $valueStr) {
                    $val='';
                    if(is_array($valueStr)){
                        foreach($valueStr as $keyParam=>$valueParam){
                            switch($keyStr){
                                case 'style':
                                    $val.=$keyParam.":".$valueParam.";";
                                    break;
                                case 'class':
                                    $val.=" $valueParam";
                                    break;
                                default :
                                    $val.=" $valueParam";
                                    break;
                            }
                        }
                    }else{
                        //Строка в значении
                        $val=$valueStr;
                    }
                    if($keyStr=='eval'){
                        $this->_eval[]=$valueStr;
                    }else{
                        $str[]=$keyStr.'="'.$val.'"';
                    }
                }
            }else{
                //Передали строку
                $str[]=$params;
            }
            return ' '.join(' ',$str);
        }
        return false;
    }

    protected function _getRows($rows, $tag='td'){
        if(is_array($rows) && array_key_exists('rows', $rows) && array_key_exists('count', $rows) && array_key_exists('rowCount', $rows)){
            $rows=$rows['rows'];
        }
        $str=array();
        //Массив или строка ?
        if(is_array($rows)){
            if(isset($rows['content'])){
                $cur=$rows['content'];
            }else{
                $cur=$rows;
            }
            foreach($cur as $key=>$row){
                $text='<tr ';

                if(isset($row['params']) && $row['params']){
                    $text.=$this->_setParams($row['params']);
                }
                $text.='>';
                if(isset($row['content'])){
                    $text.=$this->_getCell($row['content'], $tag);
                }else{
                    $text.=$this->_getCell($row, $tag);
                }
                $text.='</tr>';
                $str[]=$text;
            }
        }else{
            $str[]=$rows;
        }
        return join('',$str);
    }

    /**
     *
     * @param string|array $cell
     * @param string|false $tag тег для thead? по умолчанию th
     * @return string html-код вставляемый между <tr></tr>
     */
    protected function _getCell($cell, $tag='td')
    {
        $str=array();
        if(is_array($cell)){
            foreach($cell as $k=>$v){
                if($v===false){
                    unset($cell[$k]);
                }
            }
            $cell_in_row=count($cell);
            if(!$this->cell_in_row){
                  $this->cell_in_row=$cell_in_row;
            }

            $i=1;
            $count=count($cell);
            foreach($cell as $key=>$cur){
                //Если этот номер в списке не выводимых то пропускаем эту колонку
                if(in_array($i, $this->kill)){
                    $i++;
                    continue;
                }
                //Контент либо предустановленный тип либо строка.
                if(is_array($cur)){
                    if($count==$i){
                        if($i<$this->cell_in_row){
                            $cur['params']['colspan']=$this->cell_in_row-$i+1-(count($this->kill));
                        }
                    }
                     ## Сортировки
                    if(isset($cur['sortable'])){
                       $this->_getSortable($cur);
                    }
                    ## тег
                    $text='<'.$tag;
                    if(isset($cur['params']) && $cur['params']){
                        $text.=$this->_setParams($cur['params']);
                    }
                    $text.='>';
                    $text.=$this->_parseByPattern($cur, $key);
                    if(isset($cur['content']) && $cur['content']){
                        $text.=$cur['content'];
                    }

                    $text.="</$tag>";
                }else{
                    if($count==$i){
                        if($i<$this->cell_in_row){
                            $z=array();
                            $z['params']['colspan']=$this->cell_in_row-$i+1-(count($this->kill));

                        }

                    }

                    $text='<'.$tag.' ';
                    if(isset($z['params']) && $z['params']){
                        $text.=$this->_setParams($z['params']);
                        unset($z);
                    }
                    $text.='>'.$cur.'</'.$tag.'>';
                }
                $str[]=$text;
                if($i==$this->cell_in_row){
                    break;
                }
                $i++;
            }
        }else{
            $str[]=$cell;
        }
        return join('',$str);
    }

    protected function _getSortable(&$cur)
    {
        if($this->_labels == NULL) $this->_setLang();

        $sortColumn=  $this->_getRequest()->get_validate('sortColumn', 'text', false);
        $sortType=  $this->_getRequest()->get_validate('sortType', 'text', false, 4);
        $successAsc='';
        $successDesc='';
        //Сверяем не это ли поле сортируемым
        if($sortColumn && $cur['sortable']==$sortColumn){
            $this->_sortable['column']=$cur['sortable'];
            if($sortType=='desc'){
                $this->_sortable['type']='desc';
                $successAsc='';
                $successDesc='btn-success';
            }else{
                $this->_sortable['type']='asc';
                $successAsc='btn-success';
                $successDesc='';
            }
        }else{
            //нет переменных -> смотрим не является это поле дефотным, если true то добавляем в дефолт этого класса
            if(isset($cur['sortable_def']) && !$sortColumn){
                $this->_sortable['column']=$cur['sortable'];
                if($cur['sortable_def']=='desc'){
                    $this->_sortable['type']='desc';
                    $successAsc='';
                    $successDesc='btn-warning';
                }else{
                    $this->_sortable['type']='asc';
                    $successAsc='btn-warning';
                    $successDesc='';
                }
            }
        }
        //$cur['params']['class'][]='sortable';
        $cur['content']=
            '<div class="sortable">'
                .$cur['content']
                .'<div class="sortable-arrow btn  btn-mini '.$successAsc.' asc" title="'.$this->_labels['sortAsc'].'" 
                    onclick="handbooks.sortable(\''.$cur['sortable'].'\', \'asc\', this)"><span class="icon-arrow-up"></span></div>'
                .'<div class="sortable-arrow btn  btn-mini '.$successDesc.' desc"  title="'.$this->_labels['sortDesc'].'" 
                    onclick="handbooks.sortable(\''.$cur['sortable'].'\', \'desc\', this)"><span class="icon-arrow-down"></span></div>'
            .'</div>'
            ;
    }

    protected function _parseByPattern($cur, $name=false)
    {
        if($this->_labels == NULL) $this->_setLang();

        if(!isset($cur['type'])){
            if(!isset($cur['value']))
                return null;
            $cur['type']='text';
        }
        if($cur['type']=='add'){
            $cur['type']='edit';
        }
        if($cur['type']=='combobox'){
            $cur['type']='select';
        }

        $value=(isset($cur['value'])?$cur['value']:'');
        if(is_array($cur['type'])){
            $html='';
            foreach($cur['type'] as $type){
                $vars=$cur;
                $vars['type']=$type;
                $html.=$this->_parseByPattern($vars, $name);
            }
            return $html;
        }

        $months = "'".implode("','", $this->_labels['months'])."'";
        $days = "'".implode("','", $this->_labels['days'])."'";

        switch ($cur['type']){
            case 'text':
                    return "<input type='text' class='handbooks' onChange='handbooks.formSubmit()' name='{$name}' value='{$value}'>";
                break;
            case 'datepick':
                    $this->_eval[]="
                        $('#{$name}').datepicker(
                            {
                                dateFormat:'dd/mm/yy',
                                firstDay: 1,
                                changeMonth: true,
                                changeYear: true,        
                                monthNamesShort: [{$months}],
                                dayNamesMin: [{$days}]
                            }
                        )";
                    return "<input type='text' class='handbooks' name='{$name}' id='{$name}' value='{$value}'>";
                break;
            case 'monthpick':
                    $this->_eval[]="
                        $('#{$name}').monthpicker({
                            monthNames: [{$months}]
                        });";
                    return "<input type='text' class='handbooks' name='{$name}' id='{$name}' value='{$value}'>";
                break;
            case 'select':
                $combobox = new Component_Vcl_Combobox();
                $attr=array(
                    'name'=>$name,
                    'onchange'=>'handbooks.formSubmit()'
                );
                $combobox->addAttributeArray($attr);
                $combobox->selected_value=$value;
                $combobox->addItem(0, '---');
                foreach ($cur['items'] as $k=>$v){
                    $combobox->addItem($k, $v);
                }
                return $combobox->draw();
                break;
            case 'checkAll':
                    return '<label for="checkAll"><input type="checkbox" class="handbooks" id="checkAll" onchange="handbooks.changeCheckbox()">Все</label>';
                break;
            case 'button_filter':
                    return '                            
                        <button type="button" onclick="handbooks.formSubmit()" class="btn btn-warning btn-small">'.$this->_labels['filter'].'</button>
                        <button type="button" onclick="handbooks.clearFilters(this)" class="btn btn-link btn-small">X</button>
                        ';
                break;
            case 'edit':
                $this->modal=true;
                //если не указан id то вернём кнопку добавления
                if(isset($cur['id']) && ($cur['id']=(int)$cur['id'])){
                    return '<a class="btn btn-mini btn-info optional" onclick="handbooks.edit('.$cur['id'].')">'.$this->_labels['edit'].'</a>';
                }else{
                    return '<a class="btn btn-success pull-right" onclick="handbooks.edit(false)">'.$this->_labels['add'].'</a>';
                }
                break;
            case 'del':
                if(isset($cur['id']) && ($cur['id']=(int)$cur['id'])){
                    return '<a class="btn btn-mini btn-danger btnDel" title="'.$this->_labels['del'].'" onclick="handbooks.del('.$cur['id'].')">X</a>';
                }
                return null;
                break;
            case 'active':
                if(isset($cur['id']) && ($cur['id']=(int)$cur['id'])){
                    return $this->_buttonActive($cur['id'], $cur['is_active']);
                }
                return null;
                break;
            default :
                return null;
        }

    }
    /**
     * Метод для подготовки параметров запроса данных из БД
     *
     * @param object $model - принимаемые данные $model->search_rules, $model->filter_rules
     * @return array
     */
    protected function getParams($model=false){
        if(!isset($model->rows_in_page))
            $params['rows_in_page'] = $this->_getRequest()->get_validate('rows_in_page', 'int', 15);
        else
        {
            $params['rows_in_page'] = $this->_getRequest()->get_validate('rows_in_page', 'int', $model->rows_in_page);
            $params['rows_in_page_custom'] = $model->rows_in_page;
        }
        $params['page']=  $this->_getRequest()->get_validate('paginator_page', 'int','1');
        $search=false;
        if(isset($model->search_rules)){
            foreach($model->search_rules as $row){
                if($cur=$this->_getRequest()->get_validate($row['name'])){
                    if($row['format']==='date'){
                        $date = explode('/', $cur);
                        $date= mktime(0, 0, 0, $date[1], $date[0], $date[2]);
                        $params['where'][]=" `$cur`='$date'";
                    }else{
                        $params['where'][]=sprintf($row['format'], $row['name'], trim($cur),'%');
                    }
                    $search=true;
                }
            }
        }
        if(isset($model->filter_rules)){
            if(!$search){
                foreach($model->filter_rules as $row){
                    if($cur=$this->_getRequest()->get_validate($row['name'])){
                        if(isset($row['format']) && $row['format']=='date'){
                            $date = explode('/', $cur);
                            if(!isset($date[2])) array_unshift($date, '1');
                            $date= mktime(0, 0, 0, $date[1], $date[0], $date[2]);

                            $params['where'][]=" `".$row['name']."`='$date' ";
                        }else{
                            $params['where'][]=sprintf($row['format'], $row['name'], trim($cur),'%');
                        }
                        $search=true;
                    }elseif(isset($row['format_default']) && $row['format_default']){
                        $row['format_default'] = str_replace('%time%', time(), $row['format_default']);
                        $params['where'][]=sprintf($row['format_default'], $row['name'], trim($cur),'%');
                    }
                }
            }
        }
        if(isset($model->sortable_rules['name'])){
            if(!isset($model->sortable_rules['type'])){
                $model->sortable_rules['type']='asc';
            }
            $params['sort']['name']=$this->_getRequest()->get_validate('sortColumn', 'text', $model->sortable_rules['name']);
            $params['sort']['type']=$this->_getRequest()->get_validate('sortType', 'text', $model->sortable_rules['type']);
        }

        return $params;
    }
    /**
     * Пейджинатор грида. Использует Dante_Lib_Paginator.
     * @param int $count -общее количество записей.
     * @param array('page'=>num , 'rows_in_page'=>num) $params - параметры
     * @return string - html пейджинатора
     */
    public function paginator($count, $params){
        $paginator = new Lib_Paginator_Driver();
        $page       = $params['page'];
        $rowInPage  = $params['rows_in_page'];

        if(isset($params['rows_in_page_custom']))
            $paginator->addRowsInPageSelectItem($params['rows_in_page_custom']);

        $functionJS = "handbooks.formSubmit()";
        return $paginator->draw($count, $page, $rowInPage, $functionJS);
    }

    /**
     * Сброс всех значений и параметров и фильтров на дефолтные
     */
    public function clear(){
        $this->caption=false;
        $this->thead=false;
        $this->tbody=false;
        $this->tfoot=false;
        $this->_eval=false;
        $this->cell_in_row=false;
        $this->_gridform=array(
            'controller'=>false,
            'action'=>'drawAjax',
            'url'=>'ajax.php',
            'page_url'=>false,
            'id'=>'editHandbookForm',
            'name'=>false
        );
        $this->_sortable=false;
        $this->tableParams=array(
            'id'=>'handbookTable',
            'class'=>array(
                'table'=>'table',
                'handbook'=>'handbook',
                'table-bordered'=>'table-bordered',
                'table-hover'=>'table-hover',
                'table-condensed'=>'table-condensed'
            )
        );

    }
    /**
     * Убираем колонку из таблицы по порядковому номеру
     * @param type $num
     */
    protected function _killColumnByNum($num){
        $this->kill[] = $num;
    }
    /**
     * Шаблок кнопки активации.
     * @param int $id   id обрабатываемой записи
     * @param int $value зекущее значение.
     * @return string html с кнопкой
     */
    protected function _buttonActive($id, $value=0)
    {
        if($this->_labels == NULL) $this->_setLang();

        if($value){
            $button = '<button type="button" title="'.$this->_labels['deactiv'].'" class="btn btn-mini btn-warning switchActive" 
            onclick="handbooks.active('.$id.', 0)">-</button>';
        }else{
            $button = '<button type="button" title="'.$this->_labels['activ'].'" class="btn btn-mini btn-success switchActive" 
            onclick="handbooks.active('.$id.', 1)">+</button>';
        }
        return $button;
    }
    /**
     * Удаление записи по id
     * @param type $id
     */
   protected function _delAction(){
        $id     = $this->_getRequest()->get_validate('id', 'int');
        $this->_mapper->del($id);
        return array(
           'result'=>1
        );
    }

    /**
     * Активация/деактивация -  стандартный экшн для использования дочерними классами
     * @return array from ajax
     */
    protected function _activeAction(){
        $id     = $this->_getRequest()->get_validate('id', 'int');
        $value  = $this->_getRequest()->get_validate('value', 'int');
        $value  = $value?1:0;
        $this->_mapper->setActive($id, $value);
        return array(
            'result'=>1
        );
    }

}
?>