<?php
class Module_Handbooksgenerator_View_Paginator{
   public function draw($vars){
        $tpl = new Dante_Lib_Template();
        $tpl->vars=$vars;
        $fileName = '../module/handbooksgenerator/template/paginator.html';
        if (file_exists('template/handbooksgenerator/paginator.html')) {
            $fileName = 'template/handbooksgenerator/paginator.html';
        }
        
        return $tpl->draw($fileName);
    }
}