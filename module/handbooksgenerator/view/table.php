<?php
class Module_Handbooksgenerator_View_Table{
    public function draw($vars){
        $tpl = new Dante_Lib_Template();
        $tpl->_vars=$vars;
        $fileName = '../module/handbooksgenerator/template/table.html';
        if (file_exists('template/handbooksgenerator/table.html')) {
            $fileName = 'template/handbooksgenerator/table.html';
        }
        
        return $tpl->draw($fileName);
    }
}
