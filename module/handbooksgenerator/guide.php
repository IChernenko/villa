<?
class Module_Handbooksgenerator_Guide extends Module_Handbooksgenerator_Controller_Base{
    protected function _defaultAction() { 
        
    

    
       $html='
        <style>
           span.code{
            color: #C09853;
            border: 1px solid #FBEED5;
            background-color: #FCF8E3;
            font-style: italic;
           }
            h5{
                text-transform: uppercase;
                font-style: italic;
                color:#006;
            }
            div.normal{
                white-space:normal;
            }
            div.normal td{
                 white-space:pre;
            }
        </style>
    <div style="white-space:pre; text-align:left; width:940px;">
        Документация по справочнику handbooksgenerator
<h5>0.Общее описание</h5>
    
<h5>I. Инициализация:</h5>
        Справочник наследует базовый контроллер, то есть  инициализация происходит методом наследования handbooksgenerator

        пример:

<pre>class Example_Controller extends Module_Handbooksgenerator_Controller_Base{

}</pre>
<pre>class Example_Mapper extends Module_Handbooksgenerator_Mapper_Handbooks{

}</pre>

<h5>II. Генерирование таблицы</h5>
        Таблица на выходе генерится из свойств:
        а). 
        <b>$this->tableParams</b> Заглавие таблицы с параметрами а формате EGRID. По умолчанию имеет свойства:
<pre>array(
    "id"=>"handbookTable",
    "class"=>array(
        "table", "handbook", "table-bordered", "table-hover"
    ),
);</pre>        <b>$this->thead</b> - массив в формате egrid , на выходе получим часть таблицы в теге &lt;thead  &gt;&lt;/thead  &gt;
        <b>$this->tbody</b> - массив в формате egrid , на выходе получим часть таблицы в теге &lt;tbody  &gt;&lt;/tbody  &gt;
        <b>$this->tfoot</b> - массив в формате egrid , на выходе получим часть таблицы в теге &lt;tfoot  &gt;&lt;/tfoot  &gt;
        <b>$this->caption</b> - Содержимое тега &lt;caption  &gt;&lt;/caption  &gt;
        *все свойста не обязательны.

<h5>III. Используемые методы.</h5>
        <b>$this->formEnable($params);</b> 
            Заклчение таблицы в тег &lt;form&gt;&lt;/form&gt; . По умолчанию форма не добавляется. Если формы нет вообще то фильтры и сортировки работать не будут.
                   
            $params - массив свойств формы грида
            Если переменная строковая она присваевается элементу <b>$params["<span class="code">controller</span>"]</b>;
            Если контроллер не указан вообще, то форма не добавляется
            Возможные значения <span class="code">$params</span> для <span class="code">$this->formEnable($params)</span>';
            $this->caption='Возможные значения ';
            $this->thead=array(
                'title'=>array(
                    '№',
                    'Ключ',
                    'Описание'
                )
            );
            $this->tbody=array(
                array(
                    1,
                    '<b>controller</b>',
                    'контроллер вида <span class="code">example.controller</span>. обязательный параметр для активации формы.<br>
                    '
                ),
                array(
                    2,
                    '<b>action</b>',
                    'экшн в контроллере (НЕ экшн формы). По умолчанию drawAjax'
                ),
                array(
                    3,
                    '<b>url</b>',
                    '<b>url</b> файла обработчика на сервере. По умолчанию ajax.php.'
                ),
                array(
                    3,
                    '<b>id</b>',
                    'не стандартный id формы По умолчанию #editHandbookForm '
                ),
                array(
                    3,
                    '<b>name</b>',
                    'name формы. По умолчанию отсутсвует'
                ),
            );
            $html.='<div class="normal">'. $this->generate().'</div>';
            
            $html.='        <b>$this->generate();</b>
            Генерция html готового грида. Использует уже внесенные данные. 
            После генерацции рубит все хвосты и устанавливает везде дефолтные значения, чтоб можно было новый грид создавать.

        <b>$this->paginator($count, $params);</b>        
            Пейджинатор грида. Использует Dante_Lib_Paginator.
            @param int $count - общее количество записей.
            @param array("page"=>num , "rows_in_page"=>num) $params - текущая страница и требуемое количество строк в таблице
            @return string - html пейджинатора
       



        <b>$this->getParams($model)</b>
            Подготовка параметров для выборки данных. Использует "толстую, умную" модель.
            Возвращает массив вида Array ( [rows_in_page] => 15 [page] => 1 [where] => Array ( [0] => `t1`.`status` = "1" ) [sort] => Array ( [name] => id [type] => desc ) );
        
        <b>$mapper->getRowsByParams($params=false, $cols="*",  $join=false)</b>
            Достаёт данные по заданной таблице или нескольким таблицам соединённым JOIN.
            <span class="code">$cols</span> = Строка или массив перечисляющий поля выборки.
            <span class="code">$join</span> = Строка или массив перечисляющий JOIN других таблиц. Это поле фактически не имеет строих ограничений.
                        Его можно использовать не по прямому назначению и передать через него часть строки SQL-запроса, находящуюся между FROM и WHERE
            <span class="code">$params</span> = Массив несущий в себе любые из обрабатываемых параметров.';
            $this->caption='Возможные ключи:';
            $this->thead=array(
                'title'=>array(
                    'Название ключа',
                    'Описание'
                )
            );
            $this->tbody=array(
                array(
                    '$params["<span class="code">tableName</span>"]',
                    'название таблицы указанной в поле FROM. Если не задан то по дефолту используется свойство маппера <span class="code">$this->_tableName</span>'
                ),
                array(
                    '$params["<span class="code">where</span>"]',
                    'массив или строка с фильтрами выборки'
                ),
                array(
                    '$params["<span class="code">sort</span>"]["<span class="code">name</span>"]',
                    'Название поля по которому идёт сортировка.'
                ),
                array(
                    '$params["<span class="code">sort</span>"]["<span class="code">type</span>"]',
                    'Направление сортировки.'
                ),
                array(
                    '$params["<span class="code">rows_in_page</span>"]',
                    'Максимальное количество строк при выборке. Добавляется в LIMIT. Если задано то пейджинатор считается включенным'
                ),
                array(
                    '$params["<span class="code">page</span>"]',
                    'Выбранная страница пейджинтора. Используется для расчёта OFFSET'
                ),
            );
            $this->tfoot=array(
                'field'=>array(
                    'Все из выше перечисленных параметров могут быть сгенерированы методом <b>$this->getParams($model)</b> или добавлены Вашими методами.'
                )
            );
            
            $html.='<div class="normal">'. $this->generate().'</div>';
                
$html.='<h5>IV. Наполнение контентом:</h5>.
    Значения в полях может быть трёх типов:
        - Строка. Самый простой из типов. Не сопровождается параметрами.
        - Массив с параметрами в формате EGRID: стили, классы, id , любые атрибуты, выполняемый JS , ну и конечно сам контент.
                Использование смотрите в разделе VI. ФОРМАТ ЗАПОЛНЕНИЯ EGRID.
        - Предустановленный паттерн. Паттерны используются для контента в ячейке. Смотрите таблицу ниже.
    
<h5>V. Предустановленные патерны и  методы их вызова.</h5>
    В предустановленных паттернах на данный момент есть:';
    $this->tableParams['style']['white-space']='pre';
    $this->formEnable(array('controller'=>'module.handbooksgenerator.guide', 'id'=>'ff'));
    $this->thead=array(
        'title'=>(
                array(
                    '#',
                    'тип',
                    'описание',
                    'пример кода вызова',
                    'demo',
                )
            )
        );
    $this->tbody=array(
        array(
            1,
            '<b>text</b>',
            '<span class="code">&lt;input type="text" name="attrName" id="attrName" onChange="handbooks.formSubmit()" &gt;</span>
поле вызывающее сабмит формы при событии onChange
Применяется в основном для фильтров и поиска ТЕКСТА',
'<code>"attrName"=>array(
    "type"=>"text" - указывать необязательно
    "value"=>$this->_getRequest()->get("attrName")
),</code>',
            "attrName"=>array(
                "type"=>"text",
                "value"=>$this->_getRequest()->get_validate("attrName", 'int' ,'значени по умолчанию'),
            )
            
        ),
        array(
            2,
            '<b>datepick</b>',
            '<span class="code">&lt;input type="text" name="attrName" id="attrName" onChange="handbooks.formSubmit()" &gt;</span>
поле вызывающее сабмит формы при событии onChange
при фокусировке вызывает окно datepick
Применяется в основном для фильтров и поиска ПО ДАТАМ',
'<code>"attrName"=>array(
    "type"=>"datepick" - указывать необязательно
    "value"=>$this->_getRequest()->get("attrName")
),</code>',
            "attrName2"=>array(
                "type"=>"datepick",
                "value"=>$this->_getRequest()->get("attrName2", 'значени по умолчанию'),
            )
            
        ),
        array(
            3,
            '<b>checkAll</b>',
            '<span class="code">&lt;input type="checkbox" id="checkAll" onchange="handbooks.changeCheckbox()" &gt;</span>
checkbox отвечающий за выделение ВСЕХ чекбоксов текущего грида.
'           ,
'<code>...,
    array(
       "type"=>"checkAll"
    ),
...</code>',
            array(
                "type"=>"checkAll"
             )   
        ),
        array(
            4,
            '<b>button_filter</b>',
            'Кнопки сабмита формы и функциональная ссылка для очистки фильтров'
            ,
'<code>...,
    array(
       "type"=>"button_filter"
    ),
...</code>',
            array(
                "type"=>"button_filter"
             ),
        ),
        array(
            5,
            '<b>select</b>',
            'предусмотрена. но не реализована. Пока используем комбобокс своими силаси',
        ),
        array(
            6,
            '<b>sortable<br>sortable_def</b>',
'Кнопки сортировки по конкретному столбцу.
 Учитывает все фильтры и пейджнатор
 Используются можно в любом месте.
 В основном использую в тайтле колонок
 Можно так же расположить рядом с инпутами фильтров.
 Так же строку тайтла можно разметить в футере',
'<code>...,
    array(
        "content"=>"Любой контент",
        "sortable"=>"nameField",
</code> Если это поле дофолтной сорировки то добавляем:
        <code>"sortable_def"=>"desc",</code> по умолчанию ASC
    ),
<code>...,</code>',
            array(
                'content'=>'Любой контент',
                'sortable'=>'nameField',
                'sortable_def'=>'',
            ), 
        )
        ,
        
        
    );
    $this->tfoot=array(
        'rows'=>array(
            array(
                'content'=>'По этому разделу пока всё',
                'params'=>array(
                    'style'=>'text-align:center'
                    )
            ),
                       
        )

    );
    $html.='<div class="normal">'.$this->generate()."</div>";   
    $html.='
<h5>VI. Формат заполнения <font color=red>egrid</font>.</h5>
Формат местами запутанный, но довольно гибкий.
Форматированию подлежат все основные части таблицы , кроме , разче что тега caption
Любой элемент этого формата может быть как строкой, так и массивом с набором свойств и контентом.
    <b>1. Формат строковой.</b>
        отличия в использовании для разных тегов:
        <span class="code">table</span> -  можетбыть назначено методом $this->tableParams. Строка при назначении заменяет все атрибуты таблицы. Не рекомендуется киспользованию.
        <span class="code">thead, tbody, tfoot</span> - Замещает контент. При  это контет должен быть HTML c tr-ами и т.д.
        <span class="code">tr</span> - Замещает контент.
        <span class="code">td, th</span>
 -----------------------------------------       

<h5>VII. Использование толстой модели.</h5>
Толстая модель используется методом <b>$this->getParams($model);</b>
Если Вы не пользуетесь предустановленным фунционалом как то <i>пейджинация грида, фильтры, поиск, сортировка</i> то оно Вам и не нужно.
Изначально я пользовался только толстой моделью, но поскольку это не политкорректно то показываю схему из последнего использования обоих моделей.
файлы 
<b>productsgrid.php</b> толстая модель 
<pre>
class Exemple_Modulegrid{    
    public $sortable_rules=array(
        "name"=>"`id`",
        "type"=>"asc",
    );
    public $filter_rules=array(        
        array(
            "name"=>"name",
            "format"=>"`t1`.`%1$s` LIKE \'%3$s%2$s%3$s\'"
            ),
        array(
            "name"=>"id",
            "format"=>"`t1`.`%1$s` = \'%2$s\'"
            ),
    public $search_rules=array(
        array(
            "name"=>\'desc\',
            \'format\'=>\'`t1`.`%1$s` = "%2$s"\'
            ),
    );
}
</pre>
<b>products.php</b> тонкая модель
<pre>
class Exemple_Module extends Exemple_Modulegrid{    
    /**
     * @var int 
     */
    public $id;
    /**
     * @var string 
     */
    public $name;
    /**
     * @var string 
     */
    public $desc;
}
</pre>';
    $this->tableParams['id']='exempleModel';
    $this->caption='Таблица свойств толстой модели';
    $this->thead=array(
        array(
            '№',
            'Название метода',
            'Предназначение',
            'Пример кода'
        ),
        'fields'=>'sss'
    );
    $this->tbody=array(
        array(
            1,
            '<b>$sortable_rules<b>',
'Метод указывает дефолтное значение как именно сортировать строки при выборке данных из таблицы.
Свойство не является обязательным
Свойство учитывается только если нет данных по сортировке в реквесте.
'           ,
'<code>public $sortable_rules=array(
    "name"=>"`id`",
    "type"=>"asc",
);</code>'           
        ),
        array(
            2,
            '<b>$filter_rules<b>',
'Метод указывет какие поля и каким образом обрабатывать для фильтрации.
Фильтрация имеет НАКОПИТЕЛЬНЫЙ тип и может проходить сразу по нескольким полям
Фильтрация НЕ РАБОТАЕТ если добавлен хоть один фильтр из етода $search_rules

<b>"name"=>"id"</b> - название поля по которому будет проходить поиск данных в $_REQUEST
<b>"format"=>"`t1`.`%1$s` = \'%2$s\'"</b> - Алгоритм поиска в БД по днному полю. Подставляются ние описанные флаги.
<b>"format_default"=>"`t1`.`%1$s` > \'5\'"</b> - Дефолтная фильтрация.
Используется если по данному полю нет $_REQUEST, но нужен базовый фильтр.
Например нав в обчной выборке без фильтров нужно всё кроме строки с `id`=1.

флаги подстановки
     - %1$s -название поля
     - %2$s -запрашиваемая строка фильтра
     - %3$s - SQL-символ % для правила поиска совпадений
t1  = универсальный псевдоним таблицы указанной в FROM. Назначаяется автоматически при генерции запроса.

t2, t3, tn ... псевдонимы join-таблиц. Можно называть их как угодно, на Ваш вкус.
     '      ,
'<code>
 public $filter_rules=array(        
    array(
        "name"=>"name",
        "format"=>"`t1`.`%1$s` LIKE \'%3$s%2$s%3$s\'"
        ),
    array(
        "name"=>"id",
        "format"=>"`t1`.`%1$s` = \'%2$s\'"
        "format_default"=>"`t1`.`%1$s` > "5"",
        ),
    array(
        "name"=>"user_name",
        "format"=>"`t2`.`%1$s` = \'%2$s\'"
        ),
</code>',
            
        ),
        array(
            3,
            '<b>$search_rules</b>',
'Правила строгого поиска.
 Идентичен <b>$filter_rules </b>, но отличаетсятем что имеет высший приоритет перед ним.
Если выполняется хоть одно правило $search_rules то ВСЕ фильтры $filter_rules не буду приняты во внимание'
            ,
'<code>
public $search_rules=array(
    array(
        "name"=>"identifier",
        "format"=>\'`t1`.`%1$s` = "%2$s"\'
        ),
);
</code>',
        )
    );
    $this->tfoot=array();
    $html.='<div class="normal">'. $this->generate().'</div>';

$html.='<h5>VIII.Пример выборки строк.</h5>
    

<h5>X. Использование:</h5>
           Самый простой пример:
           $this->tbody=array(
                array(1,2,3),
                array(11,22,33),
                
            );
            return $this->generate();
            выдаст';
            $this->tbody=array(
                array(1,2,3),
                array(11,22),
                
            );
            $html.='<div class="normal">'. $this->generate().'</div>';
            $html.='Пример с использованием всех тегов таблицы:
            $this->caption="пример полной таблицы";
            $this->thead=array(
                array("колонка 1", "колонка 1", "колонка 1"),         
            );
            $this->tbody=array(
                array(1,2,3),
                array(1,2),
            );
            $this->tfoot=array(
                array("все строк столько-то"),
            );
            return $this->generate();
            ';
            $this->caption="пример полной таблицы";
            $this->thead=array(
                array("колонка 1", "колонка 1", "колонка 1"),         
            );
            $this->tbody=array(
                array(1,2,3),
                array(1,2),
            );
            $this->tfoot=array(
                array("все строк столько-то"),
            );
            $html.='<div class="normal">'. $this->generate().'</div>';
            
    return $html;
    }
    
}
?>
