<?php
/**
 * @author Elrafir 
 * 
 */
class Module_Handbooksgenerator_Mapper_Base
{
    public $hasPrimary = true;
    /**
     * Метка для дебага
     * @var bool 
     */
    public $debug=false;
    /**
     * Количество записей на странице при включенном пейджинторе
     * @var int 
     */
    public $rows_in_page=15;
    /**
     * Номер выбранной страницы при включенном пейджинаторе
     * @var int 
     */
    public $page=1;
    /**
     * Переключатель пейджинатора - по умолчанию включен.
     * @var bool 
     */
    public $paginator=true;
    
    
    /**
     * 
     * @param type $params
     * @param type $cols
     * @param type $join
     * @return type
     */
    public function getRowsByParams($params=false, $cols="*",  $join=false){
        if(isset($params['rows_in_page'])){
            $this->rows_in_page=$params['rows_in_page'];            
        }
        if(isset($params['sort'])){
            $sort_by="order by {$params['sort']['name']} {$params['sort']['type']}";
        }else{
            $sort_by=false;
        }
        if(isset($params['page'])){
            $this->paginator=true;
            $this->page=$params['page'];
        }
        if(!isset($params['tableName'])){
            $params['tableName']=$this->_tableName;
        }
        if($this->paginator){
            $limit = ' LIMIT '.$this->rows_in_page.' OFFSET '.(($this->page-1)*$this->rows_in_page);
        }else{
            
            $limit='';
        }
        $order = '';
        
        if(isset($params['where']) && $params['where']){
            
           if(!is_array($params["where"])){
                $params["where"]=array($params["where"]);
            }
            $where = implode(" AND ", $params["where"]);
        }else{
            $where = '1';
        }
        //echo $where;
        if($join){
            if(!is_array($join)){
                $join=array($join);
            }
            $join=join(' ', $join);
        }
        if(isset($cols)){
            if(!is_array($cols)){
                $cols=array($cols);
            }
            $cols=join(', ',$cols);
        }else{
           $cols='*';
        }
        
        //сперва определяем кол-во строк в общем        
        $sql = "select count(*) as count from {$params['tableName']} as t1 {$join} where {$where}"; 
        $this->_debug($sql);
        $r_count = Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
        
        //теперь выгребаем нужные строки
        $sql = "select {$cols} from {$params['tableName']} as t1 {$join} where {$where} {$order} {$sort_by} {$limit}";
        $this->_debug($sql);
        $r = Dante_Lib_SQL_DB::get_instance()->open($sql);
        
        $rowsArray = array();
        while($f = Dante_Lib_SQL_DB::get_instance()->fetch_assoc($r)) 
        {
            if($this->hasPrimary)
            {
                $key = each($f);
                $rowsArray[$key['value']] = $f;
            }
            else
                $rowsArray[] = $f;
        }
        
        return array(
            'rowCount' => $r_count['count'],
            'rows' => $rowsArray,
            'count' => $r_count['count']
        );
    }
    
    protected function _debug($var){
        if($this->debug){
            echo '<pre>';
            if(is_string($var)){
                echo (str_replace('[%%]', '', $var));
            }else{
                var_dump($var);
            }
            echo '</pre>';           
            
        }
    }
    ##### Стандартные функции... удаление/активация
    

    public function setActive($id, $value, $columnName = 'is_active'){        
        if(!$id) return false;        
        $orm = new Dante_Lib_Orm_Table($this->_tableName);
        $orm->$columnName = $value;
        $orm->update(array('id'=>$id));
        return true;
    }     
    public function del($id) {
        $table = new Dante_Lib_Orm_Table($this->_tableName);
        $table->delete(array(
            'id' => $id
        ));
    }
}