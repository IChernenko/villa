<?php

$lang = Module_Lang_Mapper::getByCode('rus');
if(!$lang) $lang = 1;

$labels = array(
    'sys_add' => 'Добавить',
    'sys_del' => 'Удалить',
    'sys_edit' => 'Ред.',
    'sys_activ' => 'Активировать',
    'sys_deactiv' => 'Деактивировать',
    'sys_save' => 'Сохранить',
    'sys_sortAsc' => 'Сортировать по возрастанию',
    'sys_sortDesc' => 'Сортировать по убыванию',
    'sys_all' => 'Все',
    'sys_filter' => 'Фильтр',
    'sys_refresh' => 'Обновить',
    'sys_jan' => 'Янв',
    'sys_feb' => 'Фев',
    'sys_mar' => 'Мар',
    'sys_apr' => 'Апр',
    'sys_may' => 'Май',
    'sys_jun' => 'Июн',
    'sys_jul' => 'Июл',
    'sys_aug' => 'Авг',
    'sys_sep' => 'Сен',
    'sys_oct' => 'Окт',
    'sys_nov' => 'Ноя',
    'sys_dec' => 'Дек',
    'sys_mon' => 'Пн',
    'sys_tue' => 'Вт',
    'sys_wed' => 'Ср',
    'sys_thu' => 'Чт',
    'sys_fri' => 'Пт',
    'sys_sat' => 'Сб',
    'sys_sun' => 'Вс',
);

$sql = "SELECT `id` FROM `[%%]labels` ORDER BY `id` DESC LIMIT 1";
$id = Dante_Lib_SQL_DB::get_instance()->fetchField($sql, 'id');

$i = $id+1;

foreach($labels as $label => $value)
{
    $sql = "INSERT INTO `[%%]labels` SET `id` = {$i}, `sys_name` = '{$label}', `is_system` = 1";
    Dante_Lib_SQL_DB::get_instance()->exec($sql);
    
    $sql = "INSERT INTO `[%%]labels_lang` SET `id` = {$i}, `lang_id` = {$lang}, `name` = '{$value}'";
    Dante_Lib_SQL_DB::get_instance()->exec($sql);
    $i++;
}

?>
