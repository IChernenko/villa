function questionPostAnswer() {
    var questionIndex = $('#questionIndex').val();
    if (!questionIndex) return false;

    questionIndex++;

    $.ajax({
        type: "POST",
        url: '/ajax.php',
        data: {
            controller: "module.questionary.controller.question",
            action: 'draw',
            index: questionIndex,
            group: $('#questionGroup').val(),
            answer: $('#questionAnswer').val()
        },
        success: function(data) {
            if (data.result == 1) {
                $("#questionary").html(data.template);
            }
        }
    });
}