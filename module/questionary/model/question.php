<?php
/**
 * Модель вопроса
 * User: dorian
 * Date: 21.04.12
 * Time: 14:14
 * To change this template use File | Settings | File Templates.
 */
class Module_Questionary_Model_Question
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var int
     */
    public $index;

    public $answer;

    public $answers = array();

    public $type = 1;

    public $group;
}
