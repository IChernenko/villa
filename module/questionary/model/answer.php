<?php
/**
 * Модель Ответ на вопрос
 * User: dorian
 * Date: 23.04.12
 * Time: 19:40
 * To change this template use File | Settings | File Templates.
 */
class Module_Questionary_Model_Answer
{
    /**
     * @var int
     */
    public $id;

    public $questionId;

    /**
     * @var string
     */
    public $name;

    /**
     * @var int
     */
    public $order;

    public $value;
}