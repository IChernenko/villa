<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 23.04.12
 * Time: 18:48
 * To change this template use File | Settings | File Templates.
 */

$mapper = new Module_Questionary_Mapper_Question();
/*
Вы когда-нибудь сидели на диете?

- Да, но безуспешно – 1 очко.
- Пару, раз пробовала, и потом бросала – 2 очка.
- Нет – 3 очка.
*/
$model = new Module_Questionary_Model_Question();
$model->name = 'Сколько вам лет';
$model->index = 1;

$answer = new Module_Questionary_Model_Answer();
$answer->name = '15-20';
$answer->order = 1;
$model->answers[] = $answer;

$answer = new Module_Questionary_Model_Answer();
$answer->name = '21-25';
$answer->order = 2;
$model->answers[] = $answer;

$mapper->add($model);


// Вопрос 2
$model = new Module_Questionary_Model_Question();
$model->name = 'Как часто вы кушаете?';
$model->index = 2;

$answer = new Module_Questionary_Model_Answer();
$answer->name = '2 раза в день';
$answer->order = 1;
$model->answers[] = $answer;

$answer = new Module_Questionary_Model_Answer();
$answer->name = '3 раза в день';
$answer->order = 2;
$model->answers[] = $answer;

$mapper->add($model);
