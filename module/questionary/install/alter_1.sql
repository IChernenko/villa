drop table if exists `[%%]question_group`;
CREATE TABLE `[%%]question_group` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `uid` int(11) unsigned NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT="Группы вопросов";

ALTER TABLE `[%%]question_group` ADD
      CONSTRAINT fk_question_group_parent_id
      FOREIGN  KEY (parent_id)
      REFERENCES [%%]question_group(id) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE `[%%]question_type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `sys_name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT="Типы вопросов";

insert into [%%]question_type (name, sys_name) values ("Список", "list");

CREATE TABLE `[%%]question` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `type_id` int(11) unsigned NOT NULL,
  `group_id` int(11) unsigned NOT NULL,
  `draw_order` tinyint(2),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT="Вопросы";

ALTER TABLE [%%]question ADD
      CONSTRAINT fk_question_type_id
      FOREIGN  KEY (type_id)
      REFERENCES [%%]question_type(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE [%%]question ADD
      CONSTRAINT fk_question_group_id
      FOREIGN  KEY (group_id)
      REFERENCES [%%]question_group(id) ON DELETE CASCADE ON UPDATE CASCADE;


CREATE TABLE `[%%]question_list_answers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `question_id` int(11) unsigned NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `draw_order` tinyint(2),
  `value` tinyint(2),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT="Ответы на вопросы типа список";

ALTER TABLE [%%]question_list_answers ADD
      CONSTRAINT fk_question_list_answers_question_id
      FOREIGN  KEY (question_id)
      REFERENCES [%%]question(id) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE `[%%]question_results` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `question_group_id` int(11) unsigned NOT NULL,
  `ball_from` tinyint(2),
  `ball_to` tinyint(2),
  `description` TEXT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT="Результаты опроса";

ALTER TABLE [%%]question_results ADD
      CONSTRAINT fk_question_results_question_group_id
      FOREIGN  KEY (question_group_id)
      REFERENCES [%%]question_group(id) ON DELETE CASCADE ON UPDATE CASCADE;
