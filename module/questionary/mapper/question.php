<?php
/**
 * Маппер для загрузки вопросов
 * User: dorian
 * Date: 23.04.12
 * Time: 18:36
 * To change this template use File | Settings | File Templates.
 */
class Module_Questionary_Mapper_Question
{
    public function add(Module_Questionary_Model_Question $question) {
        $table = new Dante_Lib_Orm_Table('[%%]question');
        $table->name = $question->name;
        $table->draw_order = $question->index;
        $table->type_id = (int)$question->type;
        $table->group_id = (int)$question->group;
        $question->id = $table->insert();

        $this->_addAnswers($question->answers, $question->id);
    }

    protected function _addAnswers($answers, $questionId) {
        foreach($answers as $answer) {
            $this->_addAnswer($answer, $questionId);
        }
    }

    /**
     * Добавление вопроса
     * @param Module_Questionary_Model_Answer $answer
     * @param int $questionId
     */
    protected function _addAnswer(Module_Questionary_Model_Answer $answer, $questionId) {
        $table = new Dante_Lib_Orm_Table('[%%]question_list_answers');
        $table->question_id = (int)$questionId;
        $table->name = $answer->name;
        $table->draw_order = (int)$answer->order;
        $table->value = (int)$answer->value;
        $answer->id = $table->insert();
    }

    public function getCount($groupId) {
        $table = new Dante_Lib_Orm_Table('[%%]question');
        $table->count()->where(array('group_id'=>$groupId));
        $table->fetch();
        return (int)$table->count;
    }

    public function get($index=1, $groupId=1) {
        $table = new Dante_Lib_Orm_Table('[%%]question');
        $table->select(
            array(
                'draw_order' => $index,
                'group_id' => $groupId
            )
        );

        if ($table->getNumRows() == 0) return false;

        $model = new Module_Questionary_Model_Question();
        $model->id = $table->id;

        $model->name = $table->name;
        $model->index = $table->draw_order;
        $model->type = (int)$table->type_id;
        $model->group = (int)$table->group_id;
        $model->answers = $this->_getAnswers($model->id);

        return $model;
    }

    protected function _getAnswers($questionId) {
        $table = new Dante_Lib_Orm_Table('[%%]question_list_answers');
        $table->get()->where(array('question_id' => $questionId))->orderby('draw_order');
        $table->open();

        $answers = array();
        while($f = $table->fetchRecord()) {
            $answer = new Module_Questionary_Model_Answer();
            $answer->id = (int)$f['id'];
            $answer->name = $f['name'];
            $answer->order = (int)$f['draw_order'];
            $answer->value = (int)$f['value'];
            $answers[] = $answer;
        }

        return $answers;
    }
}
