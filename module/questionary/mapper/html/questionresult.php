<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dorian
 * Date: 14.05.12
 * Time: 10:33
 * To change this template use File | Settings | File Templates.
 */
class Module_Questionary_Mapper_Html_Questionresult
{
    public function draw($id) {
        $fileName = "page/dieta/{$id}.html";
        if (!file_exists($fileName)) {
            return 'wrong call';
        }

        return file_get_contents($fileName);
    }
}
