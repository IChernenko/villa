<?php
/**
 * Маппер групп вопросов
 * User: dorian
 * Date: 25.04.12
 * Time: 16:47
 * To change this template use File | Settings | File Templates.
 */
class Module_Questionary_Mapper_Group
{
    public function add(Module_Questionary_Model_Group $model) {
        $table = new Dante_Lib_Orm_Table('[%%]question_group');
        //$table->id = (int)$model->id;
        $table->name = $model->name;

        if ($model->parent)
            $table->parent_id = (int)$model->parent;

        if ($model->uid)
            $table->uid = (int)$model->uid;

        if ($model->description)
            $table->description = $model->description;
        $model->id = $table->insert();
    }

    public function clearAll() {
        $sql = 'delete from [%%]question_group';
        return Dante_Lib_SQL_DB::get_instance()->exec($sql);
    }

    /**
     * @return bool|Module_Questionary_Model_Group
     */
    public function getDefault() {
        $table = new Dante_Lib_Orm_Table('[%%]question_group');
        $table->select(array('name'=>'default'));
        if ($table->getNumRows()==0) return false;

        $model = new Module_Questionary_Model_Group();
        $model->id = (int)$table->id;
        $model->name = $table->name;
        $model->parent = (int)$table->parent_id;
        $model->uid = (int)$table->uid;
        $model->description = $table->description;
        return $model;
    }
}
