<?php
/**
 * Маппер для ответов
 * User: dorian
 * Date: 25.04.12
 * Time: 14:46
 * To change this template use File | Settings | File Templates.
 */
class Module_Questionary_Mapper_Answer
{
    /**
     * Получить ответ на вопрос по $answerId
     * @param $id
     * @return bool|Module_Questionary_Model_Answer
     */
    public function get($id) {
        $table = new Dante_Lib_Orm_Table('[%%]question_list_answers');
        $table->select(array('id' => $id));

        if ($table->getNumRows() == 0) return false;

        $model = new Module_Questionary_Model_Answer();
        $model->id = (int)$table->id;

        $model->name = $table->name;
        $model->order = $table->draw_order;
        $model->questionId = (int)$table->question_id;
        $model->value = $table->value;

        return $model;
    }
}
