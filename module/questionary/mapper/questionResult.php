<?php
/**
 * Маппер результатов опросника
 * User: dorian
 * Date: 26.04.12
 * Time: 22:21
 * To change this template use File | Settings | File Templates.
 */
class Module_Questionary_Mapper_QuestionResult
{
    protected $_tableName;

    public function add(Module_Questionary_Model_QuestionResult $model) {

        //$metaData = Dante_Lib_SQL_DB::get_instance()->getMetaData($this->_tableName);
        $table = new Dante_Lib_Orm_Table('[%%]question_results');
        $table->question_group_id = (int)$model->group;
        $table->ball_from = (int)$model->ballFrom;
        $table->ball_to = (int)$model->ballTo;
        $table->description = $model->description;
        $model->id = $table->insert();
    }

    public function getByBall($ballFrom, $ballTo) {
        $sql = "select * from [%%]question_results where ball_from <= {$ballFrom} and ball_to >= {$ballTo}";
        //echo($sql);
        $f = Dante_Lib_SQL_DB::get_instance()->open_and_fetch($sql);
        if (count($f) == 0) return false;

        $model = new Module_Questionary_Model_QuestionResult();
        $model->id = (int)$f['id'];
        $model->group = (int)$f['question_group_id'];
        $model->ballFrom = (int)$f['ball_from'];
        $model->ballTo = (int)$f['ball_to'];
        $model->description = $f['description'];

        return $model;
    }
}
