<?php
/**
 * Контроллер опросника
 * User: dorian
 * Date: 24.04.12
 * Time: 1:19
 * To change this template use File | Settings | File Templates.
 */
class Module_Questionary_Controller_Question extends Dante_Controller_Base {

    protected function _defaultAction() {

        //$service = new Dieta_Service_A1_Sms();
        //return $service->draw();

        //$controller = new Module_A1_Controller();
        //return $controller->run();

        $mapper = new Module_Questionary_Mapper_Group();

        //var_dump($this->_getRequest());
        $index = (int)$this->_getRequest()->get('index', 1);
        $group = $this->_getRequest()->get('group', $mapper->getDefault()->id);
        $answerId = (int)$this->_getRequest()->get('answer');
        if ($answerId > 0) $this->_saveAnswer($answerId);

        // если это первый вопрос
        if ($index == 1) {
            Dante_Lib_Session::set('answer', 0);

            // убираем информацию об оплате через смс
            //Dante_Lib_Session::remove('smsCode');
            //Dante_Lib_Session::remove('accessCode');
        }

        $mapper = new Module_Questionary_Mapper_Question();

        $questionCount = $mapper->getCount($group);

        // мы ответили на все вопросы
        if ($index > $questionCount) {
            return $this->_testEnded();
        }

        $question = $mapper->get($index, $group);

        $view = new Module_Questionary_View_Question();
        return $view->draw($question);
    }

    /**
     * Запускается когда тест пройден до конца
     * @return mixed|string
     */
    protected function _testEnded() {

        $helper = new Module_A1_Helper_Sms();
        if ($helper->validate()) {
            return 'Для просмотра результатов теста пройдите по <a href="/test/result/">ссылке</a>';
        }

        // отображаем форму оплаты
        $service = new Dieta_Service_A1_Sms();
        return '<div id="paymentForm">'.$service->draw().'</div>';
    }

    protected function _drawAction() {
        return array(
            'result' => 1,
            'template' => $this->_defaultAction()
        );
    }

    /**
     * Вывод результатов тестирования
     * @return bool|Module_Questionary_Model_QuestionResult
     */
    protected function _drawResultAction() {

        $helper = new Module_A1_Helper_Sms();
        if (!$helper->validate()) {
            return 'У вас нет доступа к данной странице. Оплатите стоимость результата тестирования';
        }

        $resultMapper = new Module_Questionary_Mapper_QuestionResult();
        $ball = (int)Dante_Lib_Session::get('answer');
        $result = $resultMapper->getByBall($ball, $ball);
        if ($result->id == 0) return 'Скорее всего вы еще не завершили давать ответы на все вопросы';

        $htmlMapper = new Module_Questionary_Mapper_Html_Questionresult();
        return $htmlMapper->draw($result->id);

        return $result->description;
    }

    /**
     * Сохраняет ответы пользователя.
     * @param $answerId
     */
    protected function _saveAnswer($answerId) {
        $mapper = new Module_Questionary_Mapper_Answer();
        $answer = $mapper->get($answerId);

        Dante_Lib_Session::set('answer', (int)Dante_Lib_Session::get('answer')+$answer->value);
    }
}