<?php

// роуты для опросника
$url = Dante_Controller_Front::getRequest()->get_requested_url();

Dante_Lib_Router::addRule("/test\/result\//", array(
    'controller' => 'Module_Questionary_Controller_Question',
    'action' => 'drawResult'
));