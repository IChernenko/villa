<?php
/**
 * View для вопроса
 * User: dorian
 * Date: 24.04.12
 * Time: 1:29
 * To change this template use File | Settings | File Templates.
 */
class Module_Questionary_View_Question
{
    public function draw(Module_Questionary_Model_Question $question) {
        $tplFileName = '../module/questionary/template/question.html';

        $customTpl = Dante_Helper_App::getWsPath().'/questionary/question.html';
        
        if (file_exists($customTpl)) {
            $tplFileName = $customTpl;
        }

        $tpl = new Dante_Lib_Template();
        $tpl->question = $question;
        return $tpl->draw($tplFileName);
    }
}
